<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function test_method($temp = '')
    {
        $cash = ( 0 + STR_REPLACE(',', '', $temp));
       	IF($cash>1000000000000){ 
      		return ROUND(($cash/1000000000000),2).'T';
        }
        ELSEIF($cash>1000000000){ 
  			return ROUND(($cash/1000000000),2).'B';
     	}
     	ELSEIF($cash>1000000){ 
    		return ROUND(($cash/1000000),2).'M';
        }
        ELSEIF($cash>1000){ 
     		return ROUND(($cash/1000),2).'K';
     	}
        else{
            return $cash;
        }
    	//$metrics_result[0]['TotalSales'] = $cash;
    }   
}