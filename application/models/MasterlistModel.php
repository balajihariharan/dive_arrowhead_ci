<?php
class MasterlistModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }


    public function get_competitor_model()
    { 

        $debug = 'MasterlistModel :  get_competitor_model : ' . "CALL dive_get_master_list_competitor()"; 
        $query      = $this->db->query("CALL dive_get_master_list_competitor()");
        $result     = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }

    public  function get_competitor_category($competitorid){  
         $debug = 'MasterlistModel : get_competitor_category : ' . "CALL sp_getcompetitor_categoryid(".$competitorid.")"; 
        $result_query = $this->db->query("CALL sp_getcompetitor_categoryid(".$competitorid.")");
        $query = $result_query->result();
        log_debug($debug);
        return $query;
    }
    public function Masterlist_details_model($param1,$param2){
        /*$param1 = '"'.implode(',',$param1).'"';
        $param2 = '"'.implode(',',$param2).'"';*/
          $debug = 'MasterlistModel : Masterlist_details_model : ' . "CALL dive_get_master_list_details(".$param1.",".$param2.")"; 
        $result_query = $this->db->query("CALL dive_get_master_list_details(".$param1.",".$param2.")");
        $query_result = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query_result;
    }



    public function get_metrics_model(){
        $debug = 'MasterlistModel : get_metrics_model : ' . "CALL dive_masterlist_on_load_header()"; 
        $query      = $this->db->query("CALL dive_masterlist_on_load_header()");
        $result     = $query->result_array();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }


     public function get_item_detail_model($param1,$param2){
        /*$param1 = '"'.implode(',',$param1).'"';
        $param2 = '"'.implode(',',$param2).'"';*/
        $debug = 'MasterlistModel : get_item_detail_model : ' . "CALL dive_get_master_list_header(".$param1.",".$param2.")"; 
        $query = $this->db->query("CALL dive_get_master_list_header(".$param1.",".$param2.")");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result ;
    }

     public function search_partno_model(){
        $debug = 'MasterlistModel : search_partno_model : ' . "Select distinct a. partno 
        from dive_my_part_master a
        inner join dive_map_mypart_ebaylisting b
        on b.partno = a.partno
        where b.item_id <> ''"; 
        $query = $this->db->query("Select distinct a. partno 
        from dive_my_part_master a
        inner join dive_map_mypart_ebaylisting b
        on b.partno = a.partno
        where b.item_id <> ''");   
        log_debug($debug);
        return $query->result();
    }

    public function competitor_mypartnumber_update_model($param1,$param2,$param3,$username,$roleid){
         $debug = 'MasterlistModel : competitor_mypartnumber_update_model : ' . "CALL sp_mapmypartnumber_insert('".$param1."','".$param2."','".$param3."','".$username."','".$roleid."')"; 
         $query = $this->db->query("CALL sp_mapmypartnumber_insert('".$param1."','".$param2."','".$param3."','".$username."','".$roleid."')");           
        if($query->conn_id->affected_rows >= 0){
            $resultquery = array('compid'=>$param1,'mypartno'=>$param2,'itemid'=>$param3);
        }
        else{
            $resultquery = 'Failure';
        }
        log_debug($debug);
        return $resultquery;
    }

    public function remove_from_masterlist_model($itemid,$compid){
        //$id = array();
        foreach($itemid as $value) 
        {
             $debug = 'MasterlistModel : remove_from_masterlist_model : ' . "CALL dive_masterlist_remove_item('". $value['value']."')"; 
             $query = $this->db->query("CALL dive_masterlist_remove_item('". $value['value']."','".$compid."')");
           // $id[] = $value['value'];
        }
       // $ids = implode(',',$id);

      /*  $query = $this->db->query("CALL dive_masterlist_remove_item('".$ids."')");
        if($query->conn_id->affected_rows >= 1){
            $result = $ids;
        }
        else{
            $result= "Failure";
        }*/
        log_debug($debug);
        return $query;
    }

    public function getcompletelist(){
        $debug = 'MasterlistModel : getcompletelist : ' . "CALL dive_get_master_list_details('ALL','ALL')";  
        $result_query = $this->db->query("CALL dive_get_master_list_details('ALL','ALL')");
        $query_result = $result_query->result();
        log_debug($debug);
        return $query_result;
    }

   public function get_title(){
    $id = $this->session->userdata('compid');
        $debug = 'MasterlistModel : get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id; 
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }
}
?>