<?php
class Manage_User_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database('default');
		 $this->load->helper('log4php');
	}
	public function display()
	{
		  $debug = 'Manage_User_Model: display : ' . "CALL dive_admin_manage_user_role()";
		$ndata = $this->db->query("CALL dive_admin_manage_user_role()");
    	$result = $ndata->result_array();
    	$ndata->next_result();
	    $ndata->free_result();
	    log_debug($debug);    
	    return  $result;
    }
	public function index()
 	{
 		$debug = 'Manage_User_Model: index : ' . "CALL dive_admin_manage_user_select()";
 		$data = $this->db->query("CALL dive_admin_manage_user_select()");
  		$result = $data->result_array();
  		$data->next_result();
	    $data->free_result(); 
	    log_debug($debug);     
  		return $result;
	}

	

	public function insert_mod($userid,$username,$loginid,$emailid,$password,$roleid,$username1)
   {
   	  $debug = 'Manage_User_Model: insert_mod : ' . "CALL dive_admin_manageusers_insert('".$userid."','".$username."','".$loginid."','".$emailid."','".$password."','".$roleid."',
      	'".$username1."')";
      $this->db->query("CALL dive_admin_manageusers_insert('".$userid."','".$username."','".$loginid."','".$emailid."','".$password."','".$roleid."',
      	'".$username1."')");
      	log_debug($debug); 
       return true; 
	}
	public function edit_select($roleid)
	{
		$debug = 'Manage_User_Model: edit_select : ' . "CALL dive_admin_manage_user_edit_select(".$roleid.")()";
		$data=$this->db->query("CALL dive_admin_manage_user_edit_select(".$roleid.")");
		$result=$data->result_array();
		log_debug($debug);
		return $result;
	}
	public function get_userid()
	{
		$debug = 'Manage_User_Model: get_userid : ' . "CALL CALL dive_admin_manage_user_get_userid()";
		$data=$this->db->query("CALL dive_admin_manage_user_get_userid()");
		$result=$data->result_array();
		log_debug($debug);
		return $result;
	}
	public function delete($userid)
	{
		$debug = 'Manage_User_Model: delete : ' . "CALL dive_admin_manage_user_delete(".$userid.")";
		$data=$this->db->query("CALL dive_admin_manage_user_delete(".$userid.")");
		log_debug($debug);
		return true;
	}
	
    public function get_title(){
    $id = $this->session->userdata('compid');
        $debug = 'Manage_User_Model: get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }
}
?>