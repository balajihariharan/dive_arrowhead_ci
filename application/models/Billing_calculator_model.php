<?php 
class Billing_calculator_model extends CI_Model
{
	public function __construct(){
  		parent::__construct();
  		$this->load->database('default');
 	}	
  	
  	  public function get_rolename(){
    $debug = 'Billing_calculator_model : get_rolename : ' . "CALL dive_billing_calculator_get()";
    $query=$this->db->query(" CALL dive_billing_calculator_get()"); 
    $result = $query->result_array();
    $query->next_result();
    $query->free_result();
    log_debug($debug);
    return $result;

    }
    
    public function save($compwith,$no_of_comp,$id){
    $debug = 'Billing_calculator_model : save : ' . "CALL dive_billing_calculator('".$compwith."','".$no_of_comp."','".$id."')";
    $query=$this->db->query("CALL dive_billing_calculator1('".$compwith."','".$no_of_comp."','".$id."')");
    $result = $query->result_array();
    log_debug($debug);
    return $result;

    }
   
       public function get_title(){
       $id = $this->session->userdata('compid');
       $debug = 'Billing_calculator_model : get_title: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

}
?>
