<?php
class ReportsModel extends CI_Model
{

        public function __construct(){
        ini_set('memory_limit', '256M');
        $this->load->helper('log4php');
    }

    public function get_reports_competitorcombo(){
            $debug = 'ReportsModel: get_reports_competitorcombo : ' . "CALL dive_get_report_data";
            $query = $this->db->query('CALL dive_get_report_data');
            $resultquery = $query->result();
            $query->next_result();
            $query->free_result();
            log_debug($debug);
            return $resultquery;
    }

    public function report_generate_model($param1,$param2){
            /*echo 'CALL dive_get_weekly_report('.$param1.','.$param2.')';
            exit;*/
            $debug = 'ReportsModel: report_generate_model : ' . "CALL dive_get_weekly_report('.$param1.','.$param2.')";
            $query = $this->db->query("CALL dive_get_weekly_report('".$param1."','".$param2."')");
            $resultquery = $query->result_array();
            $query->next_result();
            $query->free_result();
            log_debug($debug);
            return $resultquery;
    }

    public function get_title(){
    $id = $this->session->userdata('compid');
        $debug = 'ReportsModel: get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }
}
?>