<?php

class API_Run_Tracker_model extends CI_Model 
{
	
	   public function __construct(){
         ini_set('memory_limit', '256M');
         $this->load->helper('log4php');

    }
   public  function cron_state ($runfrm,$runto){
    	   $debug = ' API_Run_Tracker_model : cron_state : ' . "CALL cron_run_status('".$runfrm."','".$runto."')";
         $result_query = $this->db->query("CALL cron_run_status('".$runfrm."','".$runto."')");
         $query = $result_query->result();
         $result_query->next_result();
	       $result_query->free_result();    
         log_debug($debug); 
         return $query;
    }

   public  function Run_status ($c_id,$runfrm,$runto){
        	$debug = ' API_Run_Tracker_model :  Run_status  : ' . "CALL dive_competitor_run_status('".$c_id."','".$runfrm."','".$runto."')";
        	$data=	$this->db->query( "CALL dive_competitor_run_status('".$c_id."','".$runfrm."','".$runto."')");
          log_debug($debug);     
          return $data->result();
    }

  public  function e_mail (){
          $debug = ' API_Run_Tracker_model : e_mail: ' . "CALL dive_email()";
          $data=  $this->db->query( "CALL dive_email()");
          log_debug($debug); 
          return $data->result_array();
    }

    
     public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = ' API_Run_Tracker_model :get_screenname: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug); 
        return $result;

    }
}
?>
   