<?php

class MyParts_Model extends CI_Model
{
	public function __construct() 
	{
     	parent::__construct();
     	$this->load->database('default');
     	  $this->load->helper('log4php');
  	}
	public function index($compid)
    {
        $debug = ' : MyParts_Model : index ' . "CALL dive_config_myparts_select(".$compid.")";
        $res = $this->db->query("CALL dive_config_myparts_select(".$compid.")");
        $result= $res->result();
        log_debug($debug);
        return $result;
    }
	public function save($partno,$sku,$cost_price,$freight,$ebay_fees,$margin,$my_price_ebay,$MSprice,$username,$roleid)
	{
		set_time_limit(6000);
		$debug = ' :MyParts_Model :save ' . "CALL dive_config_myparts_save('".$partno."','".$sku."','".$cost_price."','".$freight."','".$ebay_fees."','".$margin."','".$my_price_ebay."',".$MSprice.",'".$username."','".$roleid."')";
		$this->db->query("CALL dive_config_myparts_save('".$partno."','".$sku."','".$cost_price."','".$freight."','".$ebay_fees."','".$margin."','".$my_price_ebay."',".$MSprice.",'".$username."','".$roleid."')"); 
		log_debug($debug);
		return true;
	}

	public function savenewpart($mypart,$sku,$CostPrice,$Freight,$Fees,$margin,$username,$roleid)
	{
		$debug = ' : MyParts_Model : savenewpart' . "CALL dive_config_myparts_savenewpartno('".$mypart."','".$sku."','".$CostPrice."','".$Freight."','".$Fees."','".$margin."',".$username."','".$roleid."')";
		$res = $this->db->query("CALL dive_config_myparts_savenewpartno('".$mypart."','".$sku."','".$CostPrice."','".$Freight."','".$Fees."','".$margin."','".$username."','".$roleid."')");
		if($res->current_row >= 0){
            $result = "success";
        }
        else{
            $result= "Failure";
        }
		//$result= $res->result();
		log_debug($debug);
		return $result;
		
	}
   public function get_margin(){
    	$debug = ' : MyParts_Model :  get_margin ' . "SELECT default_margin  FROM `dive_config_meta_data`;";
        $query=$this->db->query("SELECT default_margin  FROM `dive_config_meta_data`;");
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

    public function get_title(){
    	$id = $this->session->userdata('compid');
        $debug = ' : MyParts_Model :  get_screenname ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

    public function margincountsavemodel($margincount){
            $query = $this->db->query("CALL dive_margincount_save_sp('".$margincount."')");
            /*echo  "CALL dive_pagecountebaysettings_save_sp('".$pagecount."')";
            exit;*/
            $debug = 'MyParts_Model : margincountsavemodel : ' . "CALL dive_margincount_save_sp('".$margincount."')";
            $result = $query;
            log_debug($debug);
            return $result;
  }
  public function getstorename(){
        $debug = ' MyParts_Model : getstorename: ' . "CALL dive_multistore_getstorename()";
        $result_query = $this->db->query("CALL dive_multistore_getstorename()");
        $query = $result_query->result_array();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;     
    }

}
?>