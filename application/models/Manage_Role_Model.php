<?php

class Manage_Role_Model extends CI_Model
{
	public function __construct() 
	{
     	parent::__construct();
     	$this->load->database('default');
     	 $this->load->helper('log4php');
  	}
	public function index()
	{
		$debug = 'Manage_Role_Model: index : ' . "CALL dive_admin_manage_role_select()";
		$res = $this->db->query("CALL dive_admin_manage_role_select()");
		$result= $res->result_array();
		$res->next_result();
        $res->free_result();
        log_debug($debug);
        return $result;
	}
	public function edit_select($roleid)
	{
		$debug = 'Manage_Role_Model: edit_select : ' . "CALL dive_admin_manage_role_edit_select(".$roleid.")";
		$data=$this->db->query("CALL dive_admin_manage_role_edit_select(".$roleid.")");
		$result=$data->result_array();
		log_debug($debug);
		return $result;
	}
	public function save($roleid,$rolename,$roledesc,$username)
	{
		$debug = 'Manage_Role_Model: save : ' . "CALL dive_admin_manage_role_edit_update('".$roleid."','".$rolename."','".$roledesc."','".$username."')";
		$data=$this->db->query("CALL dive_admin_manage_role_edit_update('".$roleid."','".$rolename."','".$roledesc."','".$username."')");
		log_debug($debug);	
		return true;
	}
	public function delete($roleid)
	{
	    $debug = 'Manage_Role_Model: delete : ' . "CALL dive_admin_manage_role_delete(".$roleid.")";
        $data=$this->db->query("CALL dive_admin_manage_role_delete(".$roleid.")");
        log_debug($debug);	
		return true;
	}
	public function get_roleid()
	{
	    $debug = 'Manage_Role_Model: get_roleid : ' . "CALL dive_admin_manage_role_get_roleid()";
        $data=$this->db->query("CALL dive_admin_manage_role_get_roleid()");
		$result=$data->result_array();   
		 $data->next_result();
        $data->free_result();
        log_debug($debug);
		return $result;
	}
	
       public function get_title(){
	    $id = $this->session->userdata('compid');
	    $debug = 'Manage_Role_Model: get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }
}