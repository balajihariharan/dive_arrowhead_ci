<?php
class Amz_ConfigExploreModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }

    public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'Amz_ConfigExploreModel :  get_title: ' . "SELECT SellerName ,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

    public function LoadData(){
        $CI = &get_instance();
        $this->db3 = $CI->load->database('db3', TRUE);
        $data = $this->db3->query("SELECT * from dive_amz_config_explore;");
        $result = $data->result();
        return $result;
    }
    public function save($ASIN,$Title,$SKU,$ListPrice,$CurrentPrice,$Keywords){
        $CI = &get_instance();
        set_time_limit(6000);
        $this->db3 = $CI->load->database('db3', TRUE);
        $this->db3->query("CALL dive_amz_config_explore_importsave ('".$ASIN."','".$Title."','".$SKU."','".$ListPrice."','".$CurrentPrice."','".$Keywords."')");
        return true;
    }  
    public function SaveSearch($ASIN,$Keywords){
        $CI = &get_instance();
        set_time_limit(6000);
        $this->db3 = $CI->load->database('db3', TRUE);
        $this->db3->query("CALL dive_amz_config_explore_SaveSearch('".$ASIN."','".$Keywords."')");
        return true;
    }  
   }
?>