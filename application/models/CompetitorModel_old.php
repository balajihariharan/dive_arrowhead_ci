<?php

class CompetitorModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
    }

    public  function get_competitor(){
        $query = $this->db->query("CALL dive_comp_analysis_tab2and3_competitornameonly()");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function exclusion_competitor_name($name){
        $query = $this->db->query("CALL dive_excl_competitor_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;
    }
	
	public function exclusion_general_lookup(){
		$query = $this->db->query("CALL dive_excl_general_dropdown()");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;
	}
	
	public function search_part_number($name){
		$query = $this->db->query("CALL dive_excl_part_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;
	}
	
	public function search_category_name($name){
		$query = $this->db->query("CALL dive_excl_category_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;
	}
	
	public function search_brand_name($name){
		$query = $this->db->query("CALL dive_excl_brand_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;
	}
	
     public  function get_competitor_category($competitorid){
        $id = '"'.implode(',',$competitorid).'"';
        $result_query = $this->db->query("CALL sp_getcompetitor_categoryid(".$id.")");
        $query = $result_query->result();
        return $query;
    }

    public function get_tab1_categorycombo(){
        $result_query = $this->db->query("CALL dive_comp_analysis_tab1categorycombo()");
        $query = $result_query->result();
        return $query;
    }

    public function get_competitor_listings($param1,$param2,$param3 = "",$param4 = ""){
        $inputparam = '"'.implode(',',$param1).'"';
        $inputparam2 = '"'.$param2.'"';
        $query = $this->db->query("CALL dive_comp_analysis_tab1_list($inputparam,$inputparam2,$param3,$param4)");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        $secondquery = $this->db->query("CALL dive_comp_analysis_tab1_sublist($inputparam,$inputparam2)");
        $query2 = $secondquery->result();
        $secondquery->next_result();
        $secondquery->free_result();
        $query3 = array();
        if(count($query1) >= 1){
            foreach($query1 as $mainkey => $queryvalue){
                foreach($query2 as $subkey => $query1value){
                    if($queryvalue->MyID == $query1value->MyID){
                        $query3[$queryvalue->MyID][] = $query1value;
                    }
                }
            }
        }
        $result = array('first'=>$query1,'second'=>$query3);
        return $result;
    }

    public function metrics_model(){
        $query = $this->db->query("CALL dive_comp_anslysis_metrics()");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

   public function get_competitor_category_showitem($tabs,$competitorid,$categoryid){
        $competitorid_split = implode (",",$competitorid);
        $categoryid_split = implode (",",$categoryid);
        $querystring = 'CALL dive_comp_analysis_tab2_3("'.$categoryid_split.'","'.$competitorid_split.'","'.$tabs.'")';
        $result_query = $this->db->query($querystring);
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        return $query;
    }

    public function get_exclusion(){
        $query = $this->db->query("CALL dive_comp_analysis_exclusion('','')");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function exclusionlist_update_model($param1,$param2){
        $query = $this->db->query("CALL dive_comp_analysis_exclusion('".$param1."',".$param2.")");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
    }
	
	public  function save_exclusion($exclusion_id, $exclusion_name, $general_selected_value, $location_name, $active_only, $rated_name, $shipping_name,
		$parts_selected_value, $category_selected_value, $brand_selected_value, $title_selected_value, $competitor_selected_value) {
       /* echo 'CALL dive_exclusion_save("'.$exclusion_id.'","'.$exclusion_name.'","'.$general_selected_value.'","'.$location_name.'", "'.$active_only.'","'.$rated_name.'","'.$shipping_name.'", "'.$parts_selected_value.'","'.$category_selected_value.'","'.$brand_selected_value.'","'.$title_selected_value.'","'.$competitor_selected_value.'")';
        exit;*/
		$selqry = 'CALL dive_exclusion_save("'.$exclusion_id.'","'.$exclusion_name.'","'.$general_selected_value.'","'.$location_name.'", "'.$active_only.'","'.$rated_name.'","'.$shipping_name.'", "'.$parts_selected_value.'","'.$category_selected_value.'","'.$brand_selected_value.'","'.$title_selected_value.'","'.$competitor_selected_value.'")';
        $result_query = $this->db->query($selqry);
        if($result_query->conn_id->affected_rows >= 1 || $result_query->conn_id->affected_rows == 0){
            return $query= 'success';  
        }
        else{
            return $query= 'failure';
        }
		
		
    }
	
	public function fetch_exclusion($param){
		$query = $this->db->query("CALL dive_exclusion_get_details('".$param."')");
        $query1 = $query->result();
		$query->next_result();
        $query->free_result();
		return $query1;
	}

    public function competitor_mypartnumber_update_model($param1,$param2,$param3,$param4){
        $query = $this->db->query("CALL dive_comp_analysis_map_mypart('".$param1."','".$param3."','".$param2."','".$param4."')");         
        if($query->conn_id->affected_rows >= 1){
            $resuletquery = array('itemid'=>$param2,'partnumber'=>$param3);
        }
        else{
            $resuletquery = 'Failure';
        }
        return $resuletquery;
    }

    public function dive_comp_analysis_item_sales_trend_model($param1,$param2,$param3,$param4){
        $query = $this->db->query("CALL dive_comp_analysis_item_sales_trend('".$param1."','".$param2."','".$param3."','".$param4."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;
    }
	public function interchange_part_model(){
        $query = $this->db->query("CALL dive_interchange_onload_partdetails()");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        return $query1;   
    }
    
    public function search_partno_model($partno){
        $query = $this->db->query("SELECT distinct partno FROM dive_my_part_master WHERE partno LIKE '%".$partno."%' ");
        return $query->result();
    }

    public function get_delete_filter($id){
         $table = array('dive_exclusion_list');
        $this->db->where('exclusion_id', $id);
        $result = $this->db->delete($table);
        return $result;
       
    }
}
?>