<?php
class Manage_Access_Model extends CI_model{
    public function __construct() {
      parent::__construct();
      $this->load->database('default');
       $this->load->helper('log4php');
    }
  public function get_rolename(){
    $debug = 'Manage_Access_Model :  get_rolename: ' . "CALL dive_admin_role_access_get_role() ";
    $query=$this->db->query("CALL dive_admin_role_access_get_role()");
    $result = $query->result_array();
    $query->next_result();
    $query->free_result();
    log_debug($debug);
    return $result;
  }
  public function get_screendata($roleid){
    $debug = 'Manage_Access_Model :  get_screendata: ' . "CAll dive_admin_role_access_get('".$roleid."')";
    $query=$this->db->query("CAll dive_admin_role_access_get('".$roleid."')");
    $result=$query->result_array();
    log_debug($debug);
    return $result;
  }

   public function save_model(){
    $debug = 'Manage_Access_Model :  save_model: ' . "select * from dive_screen_master";
    $query = $this->db->query("select * from dive_screen_master");
    $response = $query->result();
     $query->next_result();
        $query->free_result();
        log_debug($debug);
    return $response;
  }

  public function get_save_screendata($param1,$param2,$param3,$param4){
    foreach($param2 as $paramvalue){
      $debug = 'Manage_Access_Model :  get_save_screendata: ' . "CAll dive_admin_role_access_save(".$param1.",".$paramvalue.",0,'".$param4."')";
      $query=$this->db->query("CAll dive_admin_role_access_save(".$param1.",".$paramvalue.",0,'".$param4."')");      
    }
  
    foreach($param3 as $paralue){
      $debug = 'Manage_Access_Model :  get_save_screendata: ' . "CAll dive_admin_role_access_save(".$param1.",".$paralue.",1,'".$param4."')";

      $query=$this->db->query("CAll dive_admin_role_access_save(".$param1.",".$paralue.",1,'".$param4."')");      
    }
    log_debug($debug);
  }
    public function get_title(){
    $id = $this->session->userdata('compid');
    $debug = 'Manage_Access_Model :  get_title: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }
}
?>