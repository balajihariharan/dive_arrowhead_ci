<?php

class CategoryReport_Model extends CI_Model
{
      	public function __construct() 
      	{
             	  parent::__construct();
             	  $this->load->database('default');
              $this->load->helper('log4php');
        }

      	public function index()
      	{
              $debug = 'CategoryReport_Model: index : ' . "CALL dive_catwise_report_get_category_master()";
          		$res = $this->db->query("CALL dive_catwise_report_get_category_master()");
          		$result= $res->result_array();
          		$res->next_result();
              $res->free_result();
              log_debug($debug);
              return $result;
      	}

	      public function getdata($week,$year,$catname)
      	{
              $debug = 'CategoryReport_Model: getdata : ' . "CALL dive_catwise_report_category_get_data_master(".$week.",".$year.",'".$catname."')";
          		$res = $this->db->query("CALL dive_catwise_report_category_get_data_master(".$week.",".$year.",'".$catname."')");
          		$result= $res->result_array();
          		$res->next_result();
              $res->free_result();
              log_debug($debug);
              return $result;
      	}
      	
          public function get_title()
        {
              $id = $this->session->userdata('compid');
              $debug = 'CategoryReport_Model: get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
              $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
              $result = $query->result();
              log_debug($debug);
              return $result;
        }
}
?>