<?php
class AmazonResearchModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }

    public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'RawlistModel :  get_title: ' . "SELECT SellerName ,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

    public function pagecountmodel(){
        $query = $this->db->query("SELECT pagecount FROM dive_config_meta_data");
        $res = $query->result_array();
        return $res;
    } 
}
?>