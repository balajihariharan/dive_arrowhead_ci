<?php

class Declining_Sales_Model extends CI_Model 
{
	
	public function __construct(){
		ini_set('memory_limit', '256M');
		$this->load->helper('log4php');
	}

   	public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = ' Declining_Sales_Model :get_screenname: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug); 
        return $result;
    }

    public function get_declining_sales($param1){
    	$debug = ' Declining_Sales_Model :get_screenname: ' . "CALL dive_get_declining_sales_detail(".$param1.")";
    	$query = $this->db->query("CALL dive_get_declining_sales_detail(".$param1.")");
    	$result = $query->result();
        log_debug($debug); 
        return $result;
    }
    public function get_tab1_categorycombo(){
        $debug = ' Declining_Sales_Model :    get_tab1_categorycombo: ' . "CALL dive_comp_analysis_tab1categorycombo()";
        $result_query = $this->db->query("CALL dive_comp_analysis_tab1categorycombo()");
        $query = $result_query->result_array();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;
   
      
    }
}