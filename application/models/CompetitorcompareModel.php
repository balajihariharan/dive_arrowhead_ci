<?php
class CompetitorcompareModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }

     public function get_competitor() {
        //echo "CALL dive_get_competitors(".$checkboxval."')";exit;
        $debug = 'CompetitorcompareModel : get_competitor : ' . "CALL dive_get_competitor_compare_competitor()";
        $data = $this->db->query("CALL dive_get_competitor_compare_competitor()");
        $result = $data->result();
        log_debug($debug);
        return $result;
    }
    public function get_competitor_compare_result_sell($competitor,$radio){
         $debug = 'CompetitorcompareModel : get_competitor_compare_result_sell : ' . "CALL dive_competitor_compare_select(".$competitor.", ".$radio.")";
        $data = $this->db->query("CALL dive_competitor_compare_select(".$competitor.", ".$radio.")");
        $result = $data->result();
        log_debug($debug);
        return $result;
    }
     public function get_competitor_compare_result_dontsell($competitor,$radio){
         $debug = 'CompetitorcompareModel : get_competitor_compare_result_dontsell : ' . "CALL dive_competitor_compare_select(".$competitor.", ".$radio.")";
        $data = $this->db->query("CALL dive_competitor_compare_select(".$competitor.", ".$radio.")");
        $result = $data->result();
        log_debug($debug);
      /*  echo "<pre>";
        print_r($result);
        exit;*/
        return $result;
    }

 }
