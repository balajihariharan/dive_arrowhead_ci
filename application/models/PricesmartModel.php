<?php
class PricesmartModel extends CI_Model
{

       public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');

    }

   public  function get_price_list(){   
        $debug = 'PricesmartModel: get_price_list : ' . "CALL dive_psmart_rules_list()";
        $result_query = $this->db->query("CALL dive_psmart_rules_list()");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;
    }

    public  function get_price_list_global(){ 
      $debug = 'PricesmartModel: get_price_list_global : ' . "CALL dive_psmart_rules_list_global()";  
        $result_query = $this->db->query("CALL dive_psmart_rules_list_global()");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        return $query;
    }

    public function get_item_detail($flag,$compid){
        $debug = 'PricesmartModel: get_item_detail : ' . "CALL psmart_get_price_recomendation('".$flag."',".$compid.")";
        $result_query = $this->db->query("CALL psmart_get_price_recomendation('".$flag."',".$compid.")");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;
    }

    public function get_comp_cat_name($param = ''){
             $debug = 'PricesmartModel: get_comp_cat_name : ' . "CALL psmart_prule_comp_cat_load('".$param."')";
            $result_query = $this->db->query("CALL psmart_prule_comp_cat_load('".$param."')");
            $query = $result_query->result();
            $result_query->next_result();
            $result_query->free_result();
            log_debug($debug);
            return $query; 

    }
    
    public function save_pricesmart($param1,$param2,$param3,$param4,$param5,$param6,$param7,$param8,$param9,$param10,$param11,$username,$roleid){

        $result_query = $this->db->query('CALL psmart_prules_save_header("'.$param1.'","'.$param2.'","'.$param3.'","'.$param4.'","'.$param5.'","'.$param6.'","'.$param7.'","'.$param8.'","'.$param9.'","'.$username.'","'.$roleid.'")');
        $debug = 'PricesmartModel: save_pricesmart : ' . 'CALL psmart_prules_save_header("'.$param1.'","'.$param2.'","'.$param3.'","'.$param4.'","'.$param5.'","'.$param6.'","'.$param7.'","'.$param8.'","'.$param9.'","'.$username.'","'.$roleid.'")';
        $result_query = $this->db->query('CALL psmart_prules_save_comp_categ_map("'.$param1.'","'.$param10.'","'.$param11.'","'.$username.'","'.$roleid.'")');
        $debug = 'PricesmartModel: save_pricesmart : ' . 'CALL psmart_prules_save_comp_categ_map("'.$param1.'","'.$param10.'","'.$param11.'","'.$username.'","'.$roleid.'")';
        log_debug($debug);
    }

    public function get_globaldata($param1){
         $debug = 'PricesmartModel: get_globaldata : ' . 'CALL psmart_prule_get_rule_details("'.$param1.'")';
        $result_query = $this->db->query('CALL psmart_prule_get_rule_details("'.$param1.'")');
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
          log_debug($debug);
        return $query;   
    }


    public function get_catid($param1){
        $this->db->where('rule_id',$param1);
        //$this->db->where('rule_id','1498053652');
        $result = $this->db->get('psmart_price_rules_details_comp_categ');
        $result = $result->result();
        return $result;
    }

    public function save($itemid,$rprice,$username,$roleid){
        set_time_limit(6000);
          $debug = 'PricesmartModel: save : ' . 'CALL psmart_save("'.$itemid.'","'.$rprice.'","'.$username.'","'.$roleid.'")';
        $this->db->query('CALL psmart_save("'.$itemid.'","'.$rprice.'","'.$username.'","'.$roleid.'")');
        log_debug($debug);
        return true;
    }

    /*public function imp_save($itemid,$partno,$title,$sku,$myprice,$comp_high,$comp_low,$cprice,$freight,$fees,$rprice){
        set_time_limit(6000);
        $this->db->query('CALL psmart_import_save("'.$itemid.'""'.$rprice.'")');
        return true;
    }
*/
    public function price_ruleid_delete($param){
        $tables = array('psmart_price_rules_list', 'psmart_price_rules_details_comp_categ', 'psmart_price_rules_details');
        $this->db->where('rule_id', $param);
        $result = $this->db->delete($tables);
        return $result;
    }

    public function price_checked_updata($ruleid,$flag,$username,$roleid)
    {   $debug = 'PricesmartModel: price_checked_updata : ' . 'CALL dive_psmart_rules_list_update("'.$ruleid.'","'.$flag.'","'.$username.'","'.$roleid.'")';
        $this->db->query('CALL dive_psmart_rules_list_update("'.$ruleid.'","'.$flag.'","'.$username.'","'.$roleid.'")');
         log_debug($debug);
        return true;
    }

    public function exportchecked($itemid)
    {   $debug = 'PricesmartModel: exportchecked : ' . 'CALL dive_psmart_export_selected("'.$itemid.'")';
        $result_query = $this->db->query('CALL dive_psmart_export_selected("'.$itemid.'")');
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;
    }    

    public function price_rule_save($ruleid,$rulehierarchy,$activeflag,$username,$roleid){
        foreach($ruleid as $key=>$rulevalue){
            $ruleid_val = $rulevalue;
            $hierarchy_val = $rulehierarchy[$key];
            $activeflag_val = $activeflag[$key];
             $debug = 'PricesmartModel: price_rule_save : ' . "CALL dive_price_rules_update_list('" . $ruleid_val."', ".$hierarchy_val .", ". $activeflag_val.",'" . $username."', ".$roleid .")";
            $result_query = $this->db->query("CALL dive_price_rules_update_list('" . $ruleid_val."', ".$hierarchy_val .", ". $activeflag_val.",'" . $username."', ".$roleid .")");
        }
         log_debug($debug);
    }


    public function get_salestrends($itemid){
         $debug = 'PricesmartModel: get_salestrends : ' . 'CALL dive_get_sales_trend_pricesmart("'.$itemid.'")';
        $result_query = $this->db->query('CALL dive_get_sales_trend_pricesmart("'.$itemid.'")');
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;
    }
 
    public function get_title(){
       
         $id = $this->session->userdata('compid');
           $debug = 'PricesmartModel: get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;

    }

    public function get_week(){
        $result_query = $this->db->query('CALL psmart_get_sales_trend_week()');
        $debug = 'PricesmartModel: get_week : ' . 'CALL psmart_get_sales_trend_week()';
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;
    }

    public function get_ebaysettings_detail(){

        $query = $this->db->query("SELECT * FROM dive_ebaysettings WHERE active_flag = 1");
        $debug = 'PricesmartModel: get_ebaysettings_detail : ' . "SELECT * FROM dive_ebaysettings WHERE active_flag = 1";
        $result = $query->result_array();
        log_debug($debug);
        return $result;
    }

    public function updateprice($itemid,$startprice){

        $debug = 'PricesmartModel: updateprice : ' . 'CALL dive_pricesmart_ebayprice_update("'.$itemid.'","'.$startprice.'")';
        $result_query = $this->db->query('CALL dive_pricesmart_ebayprice_update("'.$itemid.'","'.$startprice.'")');
        log_debug($debug);
        return $result_query;
    }
    public function lastrundate(){
        $query = $this->db->query("CALL dive_psmart_get_lastrundate;");
        $debug = 'PricesmartModel: lastrundate : ' . " CALL dive_psmart_get_lastrundate;";
        $result = $query->result();
        /*echo"<pre>";
        print_r($result);
        exit;*/
        log_debug($debug);
        return $result;
    }
     public function psmrt_edit_result_model($itemid,$compid){
        $debug = ' PricesmartModel : psmrt_edit_result_model: ' . "CALL dive_psmart_config_auto_rawlist_get(".$itemid.",".$compid.")";
        $result_query = $this->db->query("CALL dive_psmart_config_auto_rawlist_get(".$itemid.",".$compid.")");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;     
    }
}
?>
