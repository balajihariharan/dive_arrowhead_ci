<?php

class ConfigCBR_Model extends CI_Model 
{
	
	public function __construct()
  {
    ini_set('memory_limit', '256M');
  }

  public function GetData()
  {
    $debug = 'ConfigCBR_Model : GetData : ' . "CALL dive_config_cbr_get_data()";
    $query = $this->db->query("CALL dive_config_cbr_get_data()");
    $result = $query->result();
    $query->next_result();
    $query->free_result();
    log_debug($debug);       
    return $result;
  }

  public function GetDataHeader()
  {
     $debug = 'ConfigCBR_Model : GetDataHeader : ' . "CALL dive_config_cbr_get_header()";
    $query = $this->db->query("CALL dive_config_cbr_get_header()");
    $result = $query->result_array();
    $query->next_result();
    $query->free_result();
    log_debug($debug);    
    return $result;
  }

  public function Save($compname,$incdate,$username,$roleid)
  {
    $debug = 'ConfigCBR_Model : Save : ' . "CALL dive_config_cbr_save_new('".$compname."','".$incdate."','".$username."','".$roleid."')";
    $result_query = $this->db->query("CALL dive_config_cbr_save_new('".$compname."','".$incdate."','".$username."','".$roleid."')");
    log_debug($debug);
    return true;
  }

  public function Checkbox_Update($param1,$param2,$username,$roleid)
  {
     $debug = 'ConfigCBR_Model : Checkbox_Update : ' . "CALL dive_config_cbr_save_flag('".$param1."',".$param2.",'".$username."','".$roleid."')";
      $query = $this->db->query("CALL dive_config_cbr_save_flag('".$param1."',".$param2.",'".$username."','".$roleid."')");
      log_debug($debug);
      return true;
  }    

  public function Delete($param1)
  {
      $debug = 'ConfigCBR_Model : Delete : ' . "CALL dive_config_cbr_delete('".$param1."')";
      $query = $this->db->query("CALL dive_config_cbr_delete('".$param1."')");
      log_debug($debug);
      return true;

  }   

   public function Refresh()
  {
      $query = $this->db->query("CALL dive_config_cbr_listing_process()");
      return true;

  } 
  
  public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'ConfigCBR_Model : get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }   
}

?>