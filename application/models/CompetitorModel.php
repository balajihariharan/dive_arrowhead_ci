<?php

class CompetitorModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
          $this->load->helper('log4php');
    }

      public  function get_competitor(){
        $debug = 'CompetitorModel :  get_competitor: ' . "CALL dive_comp_analysis_tab2and3_competitornameonly()";
        $query = $this->db->query("CALL dive_comp_analysis_tab2and3_competitornameonly()");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
         log_debug($debug);
        return $result;
    }

    public function exclusion_competitor_name($name){
        $debug = 'CompetitorModel :  exclusion_competitor_name: ' . "CALL dive_excl_competitor_lookup('".$name."')";
         $query = $this->db->query("CALL dive_excl_competitor_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;
    }
    
    public function exclusion_general_lookup(){
        $debug = 'CompetitorModel :  exclusion_general_lookup: ' . "CALL dive_excl_general_dropdown()";
        $query = $this->db->query("CALL dive_excl_general_dropdown()");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;
    }
    
    public function search_part_number($name){
        $debug = 'CompetitorModel :   search_part_number: ' . "CALL dive_excl_part_lookup('".$name."')";
        $query = $this->db->query("CALL dive_excl_part_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;

         
    }
    
    public function search_category_name($name){
        $debug = 'CompetitorModel :   search_category_name: ' . "CALL dive_excl_category_lookup('".$name."')";
        $query = $this->db->query("CALL dive_excl_category_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;
    }
    
    public function search_brand_name($name){
        $debug = 'CompetitorModel :    search_brand_name: ' . "CALL dive_excl_brand_lookup('".$name."')";
        $query = $this->db->query("CALL dive_excl_brand_lookup('".$name."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
         log_debug($debug);
        return $query1;
    }
    
     public  function get_competitor_category($competitorid){
        $id = '"'.implode(',',$competitorid).'"';
        $debug = 'CompetitorModel :    search_brand_name: ' . "CALL sp_getcompetitor_categoryid(".$id.")";
        $result_query = $this->db->query("CALL sp_getcompetitor_categoryid(".$id.")");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        return $query;
    }

    public function get_tab1_categorycombo(){
        $debug = 'CompetitorModel :    get_tab1_categorycombo: ' . "CALL dive_comp_analysis_tab1categorycombo()";
        $result_query = $this->db->query("CALL dive_comp_analysis_tab1categorycombo()");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;
   
      
    }

    public function get_competitor_listings($param1,$param2,$param3 = "",$param4 = ""){
        $inputparam = '"'.implode(',',$param1).'"';
        $inputparam2 = '"'.$param2.'"';
        $debug_first = 'CompetitorModel :    get_competitor_listings: ' . "CALL dive_comp_analysis_tab1_list($inputparam,$inputparam2,$param3,$param4)";
        $query = $this->db->query("CALL dive_comp_analysis_tab1_list($inputparam,$inputparam2,$param3,$param4)");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        $debug_second = 'CompetitorModel :    get_competitor_listings: ' . "CALL dive_comp_analysis_tab1_sublist($inputparam,$inputparam2)";
        $secondquery = $this->db->query("CALL dive_comp_analysis_tab1_sublist($inputparam,$inputparam2)");
        $query2 = $secondquery->result();
        $secondquery->next_result();
        $secondquery->free_result();
        $query3 = array();
        if(count($query1) >= 1){
            foreach($query1 as $mainkey => $queryvalue){
                foreach($query2 as $subkey => $query1value){
                    if($queryvalue->MyID == $query1value->MyID){
                        $query3[$queryvalue->MyID][] = $query1value;
                    }
                }
            }
        }
        $result = array('first'=>$query1,'second'=>$query3);
          log_debug($debug_first);
          log_debug($debug_second);
        return $result;
    }

    public function metrics_model(){
        $debug = 'CompetitorModel :    metrics_model: ' . "CALL dive_comp_anslysis_metrics()";
        $query = $this->db->query("CALL dive_comp_anslysis_metrics()");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }

   public function get_competitor_category_showitem($tabs,$competitorid,$categoryid){
        $competitorid_split = implode (",",$competitorid);
        $categoryid_split = implode (",",$categoryid);
        $querystring = 'CALL dive_comp_analysis_tab2_3("'.$categoryid_split.'","'.$competitorid_split.'","'.$tabs.'")';
         $debug = 'CompetitorModel :    get_competitor_category_showitem: ' . 'CALL dive_comp_analysis_tab2_3("'.$categoryid_split.'","'.$competitorid_split.'","'.$tabs.'")';
        $result_query = $this->db->query($querystring);
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
       log_debug($debug);
        return $query;
    }

    public function get_exclusion(){
        $debug = 'CompetitorModel :  get_exclusion: ' . "CALL dive_comp_analysis_exclusion('','')";
        $query = $this->db->query("CALL dive_comp_analysis_exclusion('','')");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }

    public function exclusionlist_update_model($param1,$param2){
        $debug = 'CompetitorModel : exclusionlist_update_model: ' . "CALL dive_comp_analysis_exclusion('".$param1."',".$param2.")";
        $query = $this->db->query("CALL dive_comp_analysis_exclusion('".$param1."',".$param2.")");
        //$query1 = $query->result();
        if($query->conn_id->affected_rows == 1)
        {
                echo "succes";
        }
        else{
            echo "Failed";
        }
        log_debug($debug);
        //$query->next_result();
        //$query->free_result();
    }
    
    public  function save_exclusion($exclusion_id, $exclusion_name, $general_selected_value, $location_name, $active_only, $rated_name, $shipping_name,
        $parts_selected_value, $category_selected_value, $brand_selected_value, $title_selected_value, $competitor_selected_value,$username,$roleid) {
        /*echo 'CALL dive_exclusion_save("'.$exclusion_id.'","'.$exclusion_name.'","'.$general_selected_value.'","'.$location_name.'", "'.$active_only.'","'.$rated_name.'","'.$shipping_name.'", "'.$parts_selected_value.'","'.$category_selected_value.'","'.$brand_selected_value.'","'.$title_selected_value.'","'.$competitor_selected_value.'")';
        exit;*/
          $debug = 'CompetitorModel : save_exclusion: ' . 'CALL dive_exclusion_save("'.$exclusion_id.'","'.$exclusion_name.'","'.$general_selected_value.'","'.$location_name.'", "'.$active_only.'","'.$rated_name.'","'.$shipping_name.'", "'.$parts_selected_value.'","'.$category_selected_value.'","'.$brand_selected_value.'","'.$title_selected_value.'","'.$competitor_selected_value.'","'.$username.'","'.$roleid.'")';
        $selqry = 'CALL dive_exclusion_save("'.$exclusion_id.'","'.$exclusion_name.'","'.$general_selected_value.'","'.$location_name.'", "'.$active_only.'","'.$rated_name.'","'.$shipping_name.'", "'.$parts_selected_value.'","'.$category_selected_value.'","'.$brand_selected_value.'","'.$title_selected_value.'","'.$competitor_selected_value.'","'.$username.'","'.$roleid.'")';
        $result_query = $this->db->query($selqry);
        if($result_query->conn_id->affected_rows >= 1 || $result_query->conn_id->affected_rows == 0){
            return $query= 'success';  
             log_debug($debug); 
        }
        else{
            return $query= 'failure';
             log_debug($debug); 
        }
        
        
    }
    
    public function fetch_exclusion($param){
        $debug = 'CompetitorModel :  fetch_exclusion: ' . "CALL dive_exclusion_get_details('".$param."')";
        $query = $this->db->query("CALL dive_exclusion_get_details('".$param."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;
    }

    public function competitor_mypartnumber_update_model($param1,$param2,$param3,$param4,$username,$roleid){
        $debug = 'CompetitorModel :  competitor_mypartnumber_update_model: ' . "CALL dive_comp_analysis_map_mypart
            ('".$param1."','".$param3."','".$param2."','".$param4."','".$username."',".$roleid.")";
        $query = $this->db->query("CALL dive_comp_analysis_map_mypart
            ('".$param1."','".$param3."','".$param2."','".$param4."','".$username."',".$roleid.")");         
        if($query->conn_id->affected_rows >= 1){
            $resuletquery = array('itemid'=>$param2,'partnumber'=>$param3);
        }
        else{
            $resuletquery = 'Failure';
        }
         log_debug($debug);
        return $resuletquery;
    }

    public function dive_comp_analysis_item_sales_trend_model($param1,$param2,$param3,$param4){
         $debug = 'CompetitorModel :  dive_comp_analysis_item_sales_trend_model: ' . "CALL dive_comp_analysis_item_sales_trend('".$param1."','".$param2."','".$param3."','".$param4."')";
        $query = $this->db->query("CALL dive_comp_analysis_item_sales_trend('".$param1."','".$param2."','".$param3."','".$param4."')");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
         log_debug($debug);
        return $query1;
    }
    public function interchange_part_model(){
        $debug = ' CompetitorModel  : get_rawlist_competitorcombo : ' . "CALL dive_interchange_onload_partdetails()";
        $query = $this->db->query("CALL dive_interchange_onload_partdetails()");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;   
    }
    
    public function search_partno_model(){
         $debug = 'CompetitorModel :  search_partno_model: ' . "Select distinct a. partno 
        from dive_my_part_master a
        inner join dive_map_mypart_ebaylisting b
        on b.partno = a.partno
        where b.item_id <> ''";
        $query = $this->db->query("Select distinct a. partno 
        from dive_my_part_master a
        inner join dive_map_mypart_ebaylisting b
        on b.partno = a.partno
        where b.item_id <> ''");
        log_debug($debug);
        return $query->result();
    }

    public function get_delete_filter($id){
         $table = array('dive_exclusion_list');
        $this->db->where('exclusion_id', $id);
        $result = $this->db->delete($table);
        return $result;
       
    }
     
    
    public function get_title(){

        $id = $this->session->userdata('compid');
        $debug = 'CompetitorModel : get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id; 
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

}
?>