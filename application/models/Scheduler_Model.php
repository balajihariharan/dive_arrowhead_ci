<?php 
class Scheduler_Model extends CI_Model
{
	public function __construct(){
  		parent::__construct();
  		$this->load->database('default');
       $this->load->helper('log4php');
 	}	
  	
  	  public function get_time($name)
  	 {
      $debug = 'Scheduler_Model  : get_time : ' . "CALL dive_scheduler_get('".$name."')";
    	$query = $this->db->query("CALL dive_scheduler_get('".$name."')");
    	$result = $query->result_array();
      log_debug($debug);
    	return $result;
	}

  public function tasksched()
    {
      $debug = 'Scheduler_Model  : tasksched : ' . "CALL dive_taskschedulder()";
      $query = $this->db->query("CALL dive_taskschedulder()");
      $result = $query->row();
      $query->next_result();
      $query->free_result();    
      log_debug($debug);
      return $result;
   }
	
	public function dive_scheduler_save( $services, $daily,$weekly,$monthly,$start_from,$sun, $mon, $tue,$wed,$thu,$fri,$sat,$schetime,$fromdate,$username, $roleid)
   	{ 

   /*   echo "CALL dive_scheduler_save('".$services."','".$daily."','".$weekly."','".$monthly."','".$start_from."','".$sun."','".$mon."','".$tue."','".$wed."','".$thu."','".$fri."','".$sat."','".$schetime."','".$fromdate."')";exit;*/
       $debug = 'Scheduler_Model  : dive_scheduler_save : ' . "CALL dive_scheduler_save('".$services."','".$daily."','".$weekly."','".$monthly."','".$start_from."','".$sun."','".$mon."','".$tue."','".$wed."','".$thu."','".$fri."','".$sat."','".$schetime."','".$fromdate."','".$username."','".$roleid."')";
  		$this->db->query("CALL dive_scheduler_save('".$services."','".$daily."','".$weekly."','".$monthly."','".$start_from."','".$sun."','".$mon."','".$tue."','".$wed."','".$thu."','".$fri."','".$sat."','".$schetime."','".$fromdate."','".$username."','".$roleid."')");
       log_debug($debug);
 	}
  
 
    public function get_title(){
    $id = $this->session->userdata('compid');
    $debug = 'Scheduler_Model  : get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
         log_debug($debug);

        return $result;
    }
}
?>
