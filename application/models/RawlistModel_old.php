<?php
class RawlistModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }

    public function get_rawlist_competitorcombo() {
        //echo "CALL dive_get_competitors(1)";exit;
          $debug = 'RawlistModel: get_rawlist_competitorcombo : ' . "CALL dive_get_raw_list_competitors(1)";
        $data = $this->db->query("CALL dive_get_raw_list_competitors(1)");
        $result = $data->result();
        log_debug($debug);
        $data->next_result();
        $data->free_result();
        return $result;
    }
    public function get_competitor($checkboxval, $checkboxkey) {
        //echo "CALL dive_get_competitors(".$checkboxval."')";exit;
        $debug = 'RawlistModel : get_competitor : ' . "CALL dive_get_raw_list_competitors(".$checkboxval.")";
        $data = $this->db->query("CALL dive_get_raw_list_competitors(".$checkboxval.")");
        $result = $data->result();
        log_debug($debug);
        return $result;
    }
    
    public  function get_competitor_category($competitorid, $checkboxval, $checkboxkey){   
        //echo "CALL dive_rawlist_load_category(".$competitorid.", ".$checkboxval.")";exit;
        $debug = 'RawlistModel : get_competitor_category : ' . "CALL dive_rawlist_load_category(".$competitorid.", ".$checkboxval.")";
        $result_query = $this->db->query("CALL dive_rawlist_load_category(".$competitorid.", ".$checkboxval.")");
        $query = $result_query->result();
        log_debug($debug);
        return $query;
      
    }

    public function rawlist_details_model($param1,$param2,$param3,$param4){
       /* $param1 = '"'.implode(',',$param1).'"';
        $param2 = '"'.implode(',',$param2).'"';*/
        //echo "CALL dive_get_raw_list_details(".$param1.",".$param2.",".$param3.")";exit;
        $debug = 'RawlistModel : rawlist_details_model : ' . "CALL dive_get_raw_list_details(".$param1.",".$param2.",".$param3.",'".$param4."')";
        $result_query = $this->db->query("CALL dive_get_raw_list_details(".$param1.",".$param2.",".$param3.",'".$param4."')");
       
          $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;
    }

    public function search_partno_model(){
         $debug = 'RawlistModel : search_partno_model: ' . "Select distinct a. partno 
        from dive_my_part_master a
        inner join dive_map_mypart_ebaylisting b
        on b.partno = a.partno
        where b.item_id <> ''";
        /*$query = $this->db->query("SELECT distinct partno FROM dive_my_part_master WHERE partno LIKE '%".$partno."%' ");*/
        $query = $this->db->query("Select distinct a. partno 
        from dive_my_part_master a
        inner join dive_map_mypart_ebaylisting b
        on b.partno = a.partno
        where b.item_id <> ''");
        return $query->result();
    }

     public function raw_item_detail_model($param1,$param2,$param3,$param4){
        /*$param1 = '"'.implode(',',$param1).'"';
        $param2 = '"'.implode(',',$param2).'"';*/
        //echo "CALL dive_get_raw_list_header(".$param1.",".$param2.",".$param3.")";exit;
        $debug = 'RawlistModel : raw_item_detail_model : ' . "CALL dive_get_raw_list_header(".$param1.",".$param2.",".$param3.",'".$param4."')";
        $query = $this->db->query("CALL dive_get_raw_list_header(".$param1.",".$param2.",".$param3.",'".$param4."')");
        $result = $query->result_array();
        log_debug($debug);
        $query->next_result();
        $query->free_result();
        return $result ;
    }

    

    public function competitor_mypartnumber_update_model($param1,$param2,$param3,$username,$roleid){
       // echo "CALL sp_mapmypartnumber_insert('".$param1."','".$param2."','".$param3."')";
         $debug = 'RawlistModel :  competitor_mypartnumber_update_model : ' . "CALL sp_mapmypartnumber_insert('".$param1."','".$param2."','".$param3."','".$username."',
         '".$roleid."')";
         $query = $this->db->query("CALL sp_mapmypartnumber_insert('".$param1."','".$param2."','".$param3."','".$username."','".$roleid."')");         
        if($query->conn_id->affected_rows >= 0){
            $resultquery = array('compid'=>$param1,'mypartno'=>$param2,'itemid'=>$param3);
        }
        else{
            $resultquery = 'Failure';
        }
        log_debug($debug);
        return $resultquery;
    }

    public function add_to_masterlist_model($param1,$param2,$username,$roleid){
        foreach($param2 as $value)
        {
             $debug = 'RawlistModel : add_to_masterlist_model : ' . "CALL dive_rawlist_add_masterlist('".$param1."','".$value['value']."','".$username."','".$roleid."')";
            $query = $this->db->query("CALL dive_rawlist_add_masterlist('".$param1."','".$value['value']."','".$username."','".$roleid."')");
        }
    
        if($query->current_row >= 0){
            $result = array('compid'=>$param1,'itemid'=>$value['value']);
        }
        else{
            $result= "Failure";
        }
        log_debug($debug);
        return $result ;
    }

    
    public function rawlist_search_details($param1,$param2) {
        $debug = 'RawlistModel : rawlist_search_details : ' . "CALL dive_ebay_search_details('".$param1."','".$param2."')";
        $query = $this->db->query("CALL dive_ebay_search_details('".$param1."','".$param2."')");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result ;
    }

    public function rawlist_search_header($param1,$param2) {
         $debug = 'RawlistModel : rawlist_search_header: ' . "CALL dive_ebay_search_header('".$param1."','".$param2."')";
        $query = $this->db->query("CALL dive_ebay_search_header('".$param1."','".$param2."')");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result ;
    }
    public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'RawlistModel :  get_title: ' . "SELECT SellerName ,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

    public function checkebaydetails($param1,$param2){
        /*echo "CALL checkebaydetails('".$param1."','".$param2."')";*/
        $query=$this->db->query("CALL dive_rawlist_checkebaydetails('".$param1."','".$param2."')");
        $debug = 'RawlistModel :  checkebaydetails: ' . "CALL dive_rawlist_checkebaydetails('".$param1."','".$param2."')";
        $result = $query->result_array();
        log_debug($debug);
        $query->next_result();
        $query->free_result();
        return $result;
    }
    public function pagecountmodel(){
        $query = $this->db->query("SELECT pagecount FROM dive_config_meta_data");
        $res = $query->result_array();
       /* echo "<pre>";
        print_r($res);
        exit;*/
        return $res;
    }
 
}

?>