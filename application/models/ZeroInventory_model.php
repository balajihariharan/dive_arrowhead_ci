<?php

class ZeroInventory_model extends CI_Model 
{
	
	public function __construct()
  {
    ini_set('memory_limit', '256M');
  }

  public function GetData($param1)
  {
   set_time_limit(6000);
    $debug = 'ZeroInventory_model : GetData : ' . "CALL dive_ZeroInventory_get_data(".$param1.")";
    $query = $this->db->query("CALL dive_ZeroInventory_get_data(".$param1.")");
    $result = $query->result();
    
    $query->next_result();
    $query->free_result();
    log_debug($debug);       
    return $result;

  }
   public function get_tab1_categorycombo(){
        $debug = 'ZeroInventory_model :    get_tab1_categorycombo: ' . "CALL dive_comp_analysis_tab1categorycombo()";
        $result_query = $this->db->query("CALL dive_comp_analysis_tab1categorycombo()");
        $query = $result_query->result_array();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;
   
      
    }
   

 public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'ZeroInventory_model : get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }  
  
}

?>