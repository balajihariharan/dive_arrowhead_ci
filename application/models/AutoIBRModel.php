<?php
class AutoIBRModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }

    public  function get_competitor_category()
    {   
        $debug = 'AutoIBRModel :  get_competitor_category : ' . "CALL dive_IBR_category()";
        $result_query = $this->db->query("CALL dive_IBR_category()");
        $query = $result_query->result();
        log_debug($debug);
        return $query;
    }

    public function get_title(){
            $id = $this->session->userdata('compid');
            $debug = 'AutoIBRModel :  get_title: ' . "SELECT SellerName ,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id;
            $query=$this->db->query("SELECT SellerName,ShowMasked FROM dive_config_meta_data  WHERE CompID = ".$id);
            $result = $query->result();
            log_debug($debug);
            return $result;
        }
    public  function getcategorydetails($data)
    {   
        $debug = 'AutoIBRModel :  getcategorydetails : ' . "CALL dive_IBR_category('".$data."')";
        $result_query = $this->db->query("CALL dive_AutoIBR_getcategorydetails('".$data."')");
        $query = $result_query->result();
        log_debug($debug);
        return $query;
    }
    public function save($ItemID,$RecommendedPrice,$ApproveFlag){
        set_time_limit(6000);
        $debug = 'AutoIBRModel: save : ' . "CALL dive_AutoIBR_save(".$ItemID.",".$RecommendedPrice.",".$ApproveFlag.")";
        $this->db->query("CALL dive_AutoIBR_save(".$ItemID.",".$RecommendedPrice.",".$ApproveFlag.")");
        log_debug($debug);
        return true;
    }
    //geturl($ItemID)
    public function geturl($ItemID){
        set_time_limit(6000);
        $debug = 'AutoIBRModel: save : ' . "CALL dive_AutoIBR_geturl(".$ItemID.")";
        $query = $this->db->query("CALL dive_AutoIBR_geturl(".$ItemID.")");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }

 
}

?>