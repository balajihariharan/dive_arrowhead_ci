<?php

class Price_Update_History_model extends CI_Model 
{
	
	 public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');

    }

    public  function price_history_model($runfrm,$runto){
        $debug = ' Price_Update_History_model : get_price_history : ' . "CALL dive_psmart_price_update_history('".$runfrm."','".$runto."')";
         $result_query = $this->db->query("CALL dive_psmart_price_update_history('".$runfrm."','".$runto."')");
         $query = $result_query->result();
         $result_query->next_result();
         $result_query->free_result();    
         log_debug($debug); 
         return $query;
    }
  
    public function get_screenname(){
      $debug = ' Price_Update_History_model :get_screenname: ' . "SELECT screenname FROM dive_screen_master  WHERE screenid = 29";
      $query=$this->db->query("SELECT screenname FROM dive_screen_master  WHERE screenid = 29");
      $result = $query->result();
      log_debug($debug); 
      return $result;
    }

    public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = ' Price_Update_History_model :get_screenname: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug); 
        return $result;
    }
 

}
?>
   