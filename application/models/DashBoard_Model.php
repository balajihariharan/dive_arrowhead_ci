<?php

class DashBoard_Model extends CI_Model
{
	public function __construct() 
	{
     	parent::__construct();
     	$this->load->database('default');
     	 $this->load->helper('log4php');
  	}

	public function getdata($week,$year)
	{    
		$debug = 'DashBoard_Model : getdata : ' . "CALL dive_dashboard_get_data_master(".$week.",".$year.")";
		$res = $this->db->query("CALL dive_dashboard_get_data_master(".$week.",".$year.")");
		$result= $res->result_array();
		$res->next_result();
        $res->free_result();
        log_debug($debug);
        return $result;
	}

	public function getdatadrill2($week,$year,$compid)
	{   
		$debug = 'DashBoard_Model : getdatadrill2 : ' . "CALL dive_dashboard_get_data_level2_master(".$week.",".$year.",".$compid.")";
		$res = $this->db->query("CALL dive_dashboard_get_data_level2_master(".$week.",".$year.",".$compid.")");
		$result= $res->result_array();
		$res->next_result();
        $res->free_result();
        log_debug($debug);
        return $result;
	}
	  public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'DashBoard_Model : get_title : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }
     

	public function getdata_complete($week,$year)
	{
		$debug = 'DashBoard_Model : getdata_complete : ' . "CALL dive_dashboard_get_data(".$week.",".$year.")";
		$res = $this->db->query("CALL dive_dashboard_get_data(".$week.",".$year.")");
		$result= $res->result_array();
		$res->next_result();
        $res->free_result();
        log_debug($debug);
        return $result;
	}

	  public function getdatadrill2_complete($week,$year,$compid)
	{
		$debug = 'DashBoard_Model :getdatadrill2_complete : ' . "CALL dive_dashboard_get_data_level2(".$week.",".$year.",".$compid.")";
		$res = $this->db->query("CALL dive_dashboard_get_data_level2(".$week.",".$year.",".$compid.")");
		$result= $res->result_array();
		$res->next_result();
        $res->free_result();
        log_debug($debug);
        return $result;
	}
}
?>