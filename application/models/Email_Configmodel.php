<?php 
	class Email_Configmodel extends CI_Model
	{
	 
		public function __construct() {
			parent::__construct();
			$this->load->database('default');
			$this->load->helper('log4php');
		}
		public function email_config_get() {
			$debug = ' Email_Configmodel : email_config_get : ' . "CALL dive_email_config_get()";
			$query = $this->db->query("CALL dive_email_config_get()");
			$result = $query->result_array();
			$query->next_result();
			$query->free_result();
			log_debug($debug);
			return $result;
		}
		public function dive_email_config_insert ($smtpserver, $username, $password, $formmail, $to, $cc, $bcc, $roleid) { 
			$debug = ' Email_Configmodel : dive_email_config_insert : ' . "CALL dive_email_config_insert ('".$smtpserver."', '".$username."', '".$password."', '".$formmail."', '".$to."',
			'".$cc."', '".$bcc."', '".$roleid."')";
			$this->db->query( "CALL dive_email_config_insert('".$smtpserver."','".$username."','".$password."','".$formmail."','".$to."','".$cc."','".$bcc."','".$roleid."')");
			log_debug($debug);
			return true;
		}

		public function get_title() {
			$id = $this->session->userdata('compid');
			$debug = ' Email_Configmodel : get_title: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
			$query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
			$result = $query->result();
			log_debug($debug);
			return $result;
		}

	}
?>
