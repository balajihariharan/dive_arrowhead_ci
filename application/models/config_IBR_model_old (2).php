
<?php
class config_IBR_model extends CI_Model
{

      public function __construct(){
        ini_set('memory_limit', '256M');
        $this->load->helper('log4php');
        ini_set('max_execution_time', 0);
    } 
   /* public function get_competitor($compname) 
        {
            $debug = 'config_IBR_model : get_competitor : ' . "CALL dive_ibr_compname('%".$compname."%')";
            $data = $this->db->query("CALL dive_ibr_compname('%".$compname."%')");
            $result = $data->result();
             log_debug($debug);
            return $result;
        }
    public function get_datas()
        {
             $debug = 'config_IBR_model :  get_datas : ' . "CALL dive_configIBR_getdata()";
            $query = $this->db->query("CALL dive_configIBR_getdata()");
            $result = $query->result_array();
             log_debug($debug);
            return $result;
        }

    public  function get_competitor_category($competitor)
        {   
        $debug = 'config_IBR_model :  get_competitor_category : ' . "CALL dive_IBR_category('".$competitor."')";
        $result_query = $this->db->query("CALL dive_IBR_category('".$competitor."')");
        $query = $result_query->result();
        log_debug($debug);
        return $query;
        }


    public  function get_ibr_insert($compname,$firsttop, $secondtop,$allitems, $listitems,$selectedcat, $f1_flag,$s_flag,$list_flag,$cat_flag,$username,$roleid)
    { 
       $debug = 'config_IBR_model :  get_ibr_insert: ' . "CALL dive_ibr_insert_data('".$compname."','".$firsttop."','".$secondtop."','".$allitems."','".$listitems."',
            '".$selectedcat."','".$f1_flag."','".$s_flag."','".$list_flag."','".$cat_flag."','".$username."','".$roleid."')";  
       $result_query = $this->db->query("CALL dive_ibr_insert_data('".$compname."','".$firsttop."','".$secondtop."','".$allitems."','".$listitems."',
            '".$selectedcat."','".$f1_flag."','".$s_flag."','".$list_flag."','".$cat_flag."','".$username."','".$roleid."')");
        if( $result_query)
            return 1;
        else
            return 0;
        log_debug($debug);
    }*/

    public function get_title(){
        $id = $this->session->userdata('compid');
        $debug = 'config_IBR_model :  get_competitor_category : ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

     public  function config_ibr_filter_model()
        {   
        $debug = 'config_IBR_model :  config_ibr_filter : ' . "CALL config_ibr_filter('".$competitor."')";
        $result_query = $this->db->query("CALL dive_IBR_category('".$competitor."')");
        $query = $result_query->result();
        log_debug($debug);
        return $query;
        }
     public function search_category_name(){
        $debug = 'config_IBR_model :   search_category_name: ' . "CALL dive_configibr_category_lookup()";
        $query = $this->db->query("CALL dive_configibr_category_lookup()");
        $query1 = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $query1;
    }              
    public function save($ItemID,$Keywords_AND ,$Keywords_OR ,$Exclude_keywords ,$Category_Include,$Category_Exclude,$my_price,
          $Percent,$Priceform_from,$Priceform_to,$Conditions,$Shipping,$Location,$handlingtime,$Sellers_Include,$Sellers_Exclude,$Sort_order,$ebay_url,$crawler_url)
    {
        set_time_limit(6000);
        $result_query = $this->db->query("CALL dive_config_ibr_save('".$ItemID."','".$Keywords_AND."','".$Keywords_OR."','".$Exclude_keywords."','".$Category_Include."','".$Category_Exclude."','".$my_price."','".$Percent."','".$Priceform_from."','".$Priceform_to."','".$Conditions."','".$Shipping."','".$Location."','".$handlingtime."'".$Sellers_Include."','".$Sellers_Exclude."','".$Sort_order."','".$ebay_url."','".$crawler_url."')"); 
        return $result_query;
    }

     public function get_categorycombo(){
        $debug = ' config_IBR_model : get_categorycombo: ' . "CALL dive_comp_analysis_tab1categorycombo()";
        $result_query = $this->db->query("CALL dive_comp_analysis_tab1categorycombo()");
        $query = $result_query->result_array();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;     
    }
     public function categorycombo_result_model($categoryid){
        $debug = ' config_IBR_model : categorycombo_result_model: ' . "CALL dive_config_auto_rawlist_get_data(".$categoryid.")";
        $result_query = $this->db->query("CALL dive_config_auto_rawlist_get_data(".$categoryid.")");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;     
    }

    public function skucombo_result($sku){
        $debug = ' config_IBR_model : skucombo_result: ' . "CALL dive_config_auto_rawlist_get_data_forsku(".$sku.")";
        $result_query = $this->db->query("CALL dive_config_auto_rawlist_get_data_forsku('%".$sku."%')");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;     
    }

    public function unprocessedcombo_result(){
        $debug = ' config_IBR_model : unprocessedcombo_result: ' . "CALL dive_config_auto_rawlist_get_data_unprocessed()";
        $result_query = $this->db->query("CALL dive_config_auto_rawlist_get_data_unprocessed()");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;     
    }

    public function changeditems_result(){
        $debug = ' config_IBR_model : skucombo_result: ' . "CALL dive_config_auto_rawlist_get_changeditems_result()";
        $result_query = $this->db->query("CALL dive_config_auto_rawlist_get_changeditems_result()");
        $query = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $query;     
    }
/*     public function editpopup_result_model($itemid){
        $debug = ' config_IBR_model : categorycombo_result_model: ' . "CALL dive_config_auto_rawlist_edit_filter(".$itemid.")";
        $result_query = $this->db->query("CALL dive_config_auto_rawlist_edit_filter(".$itemid.")");
        $query = $result_query->result_array();
        $result_query->next_result();
        $result_query->free_result();
         log_debug($debug);
        return $query;     
    }*/

    public function save_key_search_model($itemid,$sku,$keywords_and,$keywords_or,$keywords_exclude,$splitcategory_include,$splitcategory_exclude,$myprice,$percentage,$from_price,$to_price,$condition,$shipping,$location,$handlingtime,$TopRated,$sellerinclude,$sellerexclude,$sortorder,$ebay_url,$username,$crawlerurl,$def_flag,$def_cat_id,$Titleincludeflag,$UpdateFlag)
    {
          
        $debug = ' config_IBR_model : save_key_search_model: ' . "CALL dive_auto_raw_list_save('".$itemid."','".$sku."','".$keywords_and."','".$keywords_or."','".$keywords_exclude."','".$splitcategory_include."','".$splitcategory_exclude."','".$myprice."','".$percentage."','".$from_price."','".$to_price."','".$condition."','".$shipping."','".$location."','".$handlingtime."','".$TopRated."','".$sellerinclude."','".$sellerexclude."','".$sortorder."','".$ebay_url."','".$username."','".$crawlerurl."','".$def_flag."','".$def_cat_id."',".$Titleincludeflag.",".$UpdateFlag.")";
        $result_query = $this->db->query("CALL dive_auto_raw_list_save('".$itemid."','".$sku."','".$keywords_and."','".$keywords_or."','".$keywords_exclude."','".$splitcategory_include."','".$splitcategory_exclude."',".$myprice.",'".$percentage."','".$from_price."','".$to_price."','".$condition."',".$shipping.",".$location.",'".$handlingtime."','".$TopRated."','".$sellerinclude."','".$sellerexclude."','".$sortorder."','".$ebay_url."','".$username."','".$crawlerurl."','".$def_flag."','".$def_cat_id."',".$Titleincludeflag.",".$UpdateFlag.")");
         log_debug($debug);
            
    }

    public function delete_filter_model($itemid){
        $debug ='config_IBR_model : delete_filter_model:'."DELETE FROM dive_config_ibr_filter WHERE ItemID=".$itemid;
        $result_query = $this->db->query("DELETE FROM dive_config_ibr_filter WHERE ItemID=".$itemid);
        log_debug($debug);
        return $result_query;
    }
    public function flag_update_model($itemid,$flag){
        $debug ='config_IBR_model : delete_filter_model:'."UPDATE dive_config_ibr_filter SET  Active_flag =".$flag." WHERE ItemID =".$itemid;
        $result_query = $this->db->query("UPDATE dive_config_ibr_filter SET  Active_flag =".$flag." WHERE ItemID =".$itemid);
        log_debug($debug);
        return $result_query;
    }
    public function ipn_meta_data_model(){
        $debug ='config_IBR_model : ipn_meta_data_model:'."SELECT * FROM dive_ipn_meta_data";
        $result_query = $this->db->query("SELECT * FROM dive_ipn_meta_data");
        $result = $result_query->result();
        log_debug($debug);
        return $result;
    }
    public function get_cron_status_checker(){
        $debug ='config_IBR_model : get_cron_status_checker:'."SELECT * FROM config_ibr_cron_run_status ORDER BY itemid DESC LIMIT 1";
        $result_query = $this->db->query("SELECT * FROM config_ibr_cron_run_status ORDER BY itemid DESC LIMIT 1");
        $result = $result_query->result();
        log_debug($debug);
        return $result;
    }
    public function get_api_save_exclude($myitemid,$itemId,$sellerUserName,$title,$exclude_flag,$username){
         $debug = ' config_IBR_model : get_api_save_exclude: ' . "CALL dive_config_ibr_save_excludes(".$myitemid.",".$itemId.",'".$sellerUserName."','".$title."','".$exclude_flag."','".$username."')";
        $result_query = $this->db->query("CALL dive_config_ibr_save_excludes(".$myitemid.",".$itemId.",'".$sellerUserName."','".$title."','".$exclude_flag."','".$username."')");
        log_debug($debug);
    }

    public function config_ibr_cron_run_status_tablecheck(){
        $debug ='config_IBR_model : config_ibr_cron_run_status_tablecheck:'."SELECT progress,total FROM config_ibr_cron_run_status ORDER BY Ended DESC LIMIT 0,1";
        $result_query = $this->db->query("SELECT progress,total FROM config_ibr_cron_run_status ORDER BY Ended DESC LIMIT 0,1");
        $result = $result_query->result();
        log_debug($debug);
        return $result;
    }
    public function api_search_non_exclude($guid,$my_itemid,$handlingtimeflag,$topratedflag){
     $debug ='config_IBR_model : api_search_non_exclude:'."CALL dive_get_api_search_no_exclude('".$guid."',".$my_itemid.",".$handlingtimeflag.",".$topratedflag.")";
     $result_query = $this->db->query("CALL dive_get_api_search_no_exclude('".$guid."',".$my_itemid.",".$handlingtimeflag.",".$topratedflag.")");
     $query = $result_query->result();
     $result_query->next_result();
     $result_query->free_result();
     log_debug($debug);
     return $query;
    }
    public function api_search_exclude($my_itemid){
        $debug ='config_IBR_model : api_search_exclude:'."CALL dive_get_api_search_exclude(".$my_itemid.")";
        $result_query = $this->db->query("CALL dive_get_api_search_exclude(".$my_itemid.")");
        $result = $result_query->result();
         $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $result;
    }
    public function reset_exclude_def_keyword($my_itemid){
        $debug ='config_IBR_model : reset_exclude_def_keyword:'."CALL dive_config_ibr_def_keyword_get(".$my_itemid.")";
        $result_query = $this->db->query("CALL dive_config_ibr_def_keyword_get(".$my_itemid.")");
        $result = $result_query->result();
         $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
        return $result;
    }
    
}
    ?>