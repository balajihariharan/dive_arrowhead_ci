<?php
class SalestrendsModel extends CI_Model
{
    

    public function __construct(){
        ini_set('memory_limit', '256M');
        $this->load->helper('log4php');
    }

    public function get_competitor_model()
    {    
         $debug = 'SalestrendsModel : get_competitor_model : ' . "CALL dive_get_master_list_competitor";
        $result_query = $this->db->query("CALL dive_get_master_list_competitor");
        $result = $result_query->result();
        log_debug($debug);
        $result_query->next_result();
        $result_query->free_result();
        return $result;
    }
    public  function get_competitor_category($competitorid){
        $debug = 'Salestrends :  get_competitor_category : ' . "CALL sp_getcompetitor_categoryid(".$competitorid.")";   
        $query= $this->db->query("CALL sp_getcompetitor_categoryid(".$competitorid.")");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }

    public function get_salestrends_detail_model($param1,$param2,$param3){
    /*	$param1 	= implode(',',$param1);
    	$param2 	= implode(',',$param2);*/
    	$param4 	= '@error_msg';
        $debug = 'Salestrends : get_salestrends_detail_model : ' . "CALL dive_get_sales_trend_details('".$param1."','".$param2."','".$param3."',".$param4.")";   
    	$query = $this->db->query("CALL dive_get_sales_trend_details('".$param1."','".$param2."','".$param3."',".$param4.")");
    	$result = $query->result();
       	$query->next_result();
        $query->free_result();
        log_debug($debug);
    	return $result;
    }

    public function get_totsal_detail_model($param1,$param2,$param3){
    /*	$param1 	= implode(',',$param1);
    	$param2 	= implode(',',$param2);*/
    	$param4 	= '@error_msg';
        $debug = 'Salestrends :  get_totsal_detail_model: ' . "CALL dive_get_sales_trend_header('".$param1."','".$param2."','".$param3."',".$param4.")";   
    	$query = $this->db->query("CALL dive_get_sales_trend_header('".$param1."','".$param2."','".$param3."',".$param4.")");
    	$result = $query->result_array();
    	$query->next_result();
        $query->free_result();
        log_debug($debug);
    	return $result;
    }

    public function get_salestrends_dashboard($param1,$param2,$param3){
        $param4     = '@error_msg';
        $debug = 'Salestrends :   get_salestrends_dashboard: ' . "CALL dive_get_sales_trend_dashboard('".$param1."','".$param2."','".$param3."',".$param4.")";  
        $query = $this->db->query("CALL dive_get_sales_trend_dashboard('".$param1."','".$param2."','".$param3."',".$param4.")");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }  

    public function get_totsal_detail_dashboard($param1,$param2,$param3){
    /*  $param1     = implode(',',$param1);
        $param2     = implode(',',$param2);*/
        $param4     = '@error_msg';
        $debug = 'Salestrends :   get_totsal_detail_dashboard: ' . "CALL dive_get_sales_trend_header_dashboard('".$param1."','".$param2."','".$param3."',".$param4.")";
        $query = $this->db->query("CALL dive_get_sales_trend_header_dashboard('".$param1."','".$param2."','".$param3."',".$param4.")");
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }


    public function get_salestrends_dashboard2($param1,$param2){
        $debug = 'Salestrends :   get_salestrends_dashboard2: ' . "CALL dive_get_sales_trend_dashboard2(".$param1.",".$param2.")";
        $query = $this->db->query("CALL dive_get_sales_trend_dashboard2(".$param1.",".$param2.")");
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        log_debug($debug);
        return $result;
    }  

       public function get_title(){
     
    	$id = $this->session->userdata('compid');
        $debug = 'Salestrends :    get_title: ' . "SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id;
        $query=$this->db->query("SELECT SellerName FROM dive_config_meta_data  WHERE CompID = ".$id);
        $result = $query->result();
        log_debug($debug);
        return $result;
    }

}
