<?php

class MenuModel extends CI_Model
{

    public function __construct(){
        ini_set('memory_limit', '256M');
         $this->load->helper('log4php');
    }

    public  function get_menulist(){
        $roleid = $this->session->userdata('roleid');
        //echo "CALL dive_load_MenuList('".$roleid."')";exit;

		$result_query = $this->db->query("CALL dive_load_MenuList('".$roleid."')");
        $debug = 'MenuModel: get_menulist : ' . "CALL dive_load_MenuList('".$roleid."')";
		$returnquery  = $result_query->result();
        $result_query->next_result();
        $result_query->free_result();
        log_debug($debug);
		return $returnquery;
    }
	  public  function get_submenulist($sendval){
		$submenuquery = $this->db->query("CALL dive_get_submenu('".$sendval."')");
          $debug = 'MenuModel: get_submenulist : ' . "CALL dive_get_submenu('".$sendval."')";
		$returnquery  = $submenuquery->result();
        $submenuquery->next_result();
        $submenuquery->free_result();
        log_debug($debug);
        return $returnquery;
    }
}
?>