<?php
/*  � 2013 eBay Inc., All Rights Reserved */ 
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
    //show all errors - useful whilst developing
    error_reporting(E_ALL);

    // these keys can be obtained by registering at http://developer.ebay.com
    
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = '34ca8179-d2c8-48d2-b0a8-b86397a533b5';   // these prod keys are different from sandbox keys
        $appID = 'panneerS-dbelectr-PRD-2bffbb2e5-d9c6840d';
        $certID = 'PRD-bffbb2e5c6db-bf3b-4691-b31d-a168';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**ttYRWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wJloWhAZSFoQ2dj6x9nY+seQ**BXsDAA**AAMAAA**DMnGqJVnQVE4E31Etax+5jalKU3AvQolSoaPW1nxwdn/R/aTqpnp2Q0agb20DFnt+0gS3FjwNUl8P/PRHWkK0th1+IZiDD3CLKT256EA+m6yjMQLrgtk/eutshR+7VvH8cZ1ulaQuOb5KDWPuVF5r7PlQAqAH4GgG7glDVgfz9htEQ2SuOaFtCKVYZPbVpyI322ikO5ux7zM6GsR4qD6CwyQJ8hnkR0yWomrJYdtgap6LuZF3jeNW50tjo4rX8Fz44GK/3zFpYjpx2gGUeSdVaPGNJymeyymh7hVixOdbA0UklOkii9Wh5Gh9bulFwW3u6YAhzgyIWWNIRHtCrPOYlboDmuOQGp1nwrJIXFsS9398EEqqmnwj0FUI2fHxnjFCyiLohv4yTsIpAaSHAjlue3DxlmK8F8NcBc5OSj9CMXdo9BR91WLbt92Xwz0+uBHTcvBjrqgA3RqPV4EjaNtJgnPxG0KqhM377Hyo7AElG5CAGsnnd3f49u6PajRBsUBwfBl5/sZbRT2ZGsZzv7qxE+dczxzEs8IgmzAX5A59tktQp85qrsgIWVk+J9B1DaTq7Ga7NtNy33DUo6foLzZ0x8L+0/pVZulVVJlxAkxBorPW4lCEGHclftdihdFpuLPjKVtZPpoeAECtdZqloV5TCcIdATLvvR/jeK2ZiYf9fZMEG9Du2KIVNBm3BhhDhy7mSgRuRp/FLX8/yxIGCrpn4wQi+xf2Az27sBwifR+GTbAerwT2wvgRYLNS2Mlv1u2';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
    
    
?>