<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AmazonConfig
{
    public $parameters = array();

    public function __construct() 
    {
        $this->secretKey = 'cnnIMtObMnz6WDntJNS/IomZCqPCa7vMehEtf5eG';
        $this->parameters['AWSAccessKeyId']     = 'AKIAI4JCEII377NPZOJQ';
        $this->parameters['MarketplaceId'] = 'ATVPDKIKX0DER';
        $this->parameters['SellerId']           = 'A3I4NLEIJJQG7M';
        $this->parameters['SignatureMethod']    = 'HmacSHA256';
        $this->parameters['SignatureVersion']   = '2';
    }

    public function calculateStringToSign(array $parameters)
    {
        $stringToSign  = 'POST'."\n";
        $stringToSign .= 'mws.amazonservices.com'."\n";
        $stringToSign .= '/Products/2011-10-01'."\n";
        //uksort($parameters, 'strcmp');
        $stringToSign .= $this->getParameterString($parameters);
        return $stringToSign;
    }

    public function getParameterString(array $parameters)
    {
        $url = array();
        uksort($parameters, 'strcmp');
        foreach ($parameters as $key => $val) {
            $key = $this->EncodeURL($key);
            $val = $this->EncodeURL($val);
            $url[] = "{$key}={$val}";
        }
        $parameterString = implode('&', $url);
        return $parameterString;
    }

    public function calculateSignature($stringToSign)
    {
        $signature = hash_hmac("sha256", $stringToSign, $this->secretKey, true);
        return urlencode(base64_encode($signature));
    }

    public function EncodeURL($string)
    {
        return str_replace("%7E", "~", rawurlencode($string));
    }

    public function getTimestamp()
    {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    }

    public function GetSearchKeywordResult($query)
    {
        $this->parameters['Action'] = 'ListMatchingProducts';
        $this->parameters['Version'] = '2011-10-01';
        $this->parameters['Timestamp'] = $this->getTimestamp();
        $this->parameters['Query'] = $query;

        $request = "https://mws.amazonservices.com/Products/2011-10-01?";
        $request .= $this->getParameterString($this->parameters) . "&Signature=" . $this->calculateSignature($this->calculateStringToSign($this->parameters));
        $allHeadersStr[0] = 'Content-Type: application/xml; charset=utf-8';
       
        //initialise a CURL session
        $connection = curl_init();

        //Set Endpoint URL 
        curl_setopt($connection, CURLOPT_URL, $request);

        //Set Method as POST; Bydefault it is Get, So it is OK if you do not set it if the call is Get call
        curl_setopt($connection, CURLOPT_POST, 1);

        //Set it to return the transfer as a string from curl_exec
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);

        //Stop CURL from verifying the peer's certificate
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($connection, CURLOPT_PORT, 443);
        curl_setopt($connection, CURLOPT_USERAGENT, 'APA-DIVE');
        
        //Send the Request
        $response = curl_exec($connection);
        $xmlstr = str_replace("ns2:","",$response);
        $simpleXMLElement = simplexml_load_string($xmlstr);
        $SearchResults = $simpleXMLElement->ListMatchingProductsResult->Products;
        
        return $SearchResults;
    }

    public function GetPriceResult($query){  
    
        foreach ($query as $key => $value) {
           $ASIN_List ="ASINList.ASIN.".($key+1)."";
           $ASIN_val = $value;   
           $this->parameters[$ASIN_List] = $ASIN_val;
    }
        $this->parameters['Action'] = 'GetCompetitivePricingForASIN';
        $this->parameters['Version'] = '2011-10-01';
        $this->parameters['Timestamp'] = $this->getTimestamp();
        

        $request = "https://mws.amazonservices.com/Products/2011-10-01?";
        $request .= $this->getParameterString($this->parameters) . "&Signature=" . $this->calculateSignature($this->calculateStringToSign($this->parameters));
        $allHeadersStr[0] = 'Content-Type: application/xml; charset=utf-8';
       
        //initialise a CURL session
        $connection = curl_init();

        //Set Endpoint URL 
        curl_setopt($connection, CURLOPT_URL, $request);

        //Set Method as POST; Bydefault it is Get, So it is OK if you do not set it if the call is Get call
        curl_setopt($connection, CURLOPT_POST, 1);

        //Set it to return the transfer as a string from curl_exec
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);

        //Stop CURL from verifying the peer's certificate
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($connection, CURLOPT_PORT, 443);
        curl_setopt($connection, CURLOPT_USERAGENT, 'APA-DIVE');
        
        //Send the Request
        $response = curl_exec($connection);
        $simpleXMLElement = simplexml_load_string($response);

        return $simpleXMLElement;
    }
}
?>