<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Keysnew {
  public function keys_function() {
   
   error_reporting(E_ALL);
    // these keys can be obtained by registering at http://developer.ebay.com
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = '7f17c78d-17ee-4de7-9f8f-3e29a31b3a2a';   // these prod keys are different from sandbox keys
        $appID = 'Hemanand-Hemanand-PRD-80902ebf9-255f7197';
        $certID = 'PRD-0902ebf9dd1b-635d-4623-900a-8f21';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**2ooBWQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ACkYqgDZSFpA6dj6x9nY+seQ**l7cDAA**AAMAAA**TOFfxCPuXGii1toBxrwIeD0Ptpb3YmLR4L/xaneZA4dJ1ow1H5fRAAn+Db06aXylNaJDJaVf02LEbzqZpraK4pIdfecOpWDXG7fF+9B33ACaosFqzbwG5A5xnAqU7Ugcarp43tSKwRL4fNGgODOrH97v5qhipdnPHB7c1D07AxLaWVtEewWB7xx+S7/LjJTaBkHvdtGgwokXIy7haE6b6FdDs1i+egFlJCf3QOxNdLZB037YAH+DllWnbBPY2mscplYwHIE2BqQ3ikU7T8nUQtqT0uJ7x6q14WUkzqUDu6EIvM/woWJI55bL7qsxJYyHGgt7kGoD299HxR3KienrtxgD0xM2CC7lh4SyJRXa5XTh8dpuxoM3/emBtt7cdzT3aR1iAsgIdSrGkwStCgG2t7nu994irghNGHtfHLHAAsssS4Y2JhMj2GfGQIFbD9w9OquMtn3exQJdzo0v6Qy18VzsnOXjfIogJjqxtjgHwXVTYmq4pM3MvTLG/p4roy5QBFT5cbgvQ5+uchaVKJpkI2a6E8TZ29H8cEVV2W/MqXarnyj5iwAtfU4ANdENVSLBdS41hLArgtH6faZn+EmFjieOuA3w2IEVetQ6/9dTlmOUTDHq4mDh7aNgcwNOP3KJ0p8GunMH1aO1/Uktpz0q491XBPZA0oyIFx51BsGu7UNiItRtwQa7ntFrxhVlvD7wq2qFPnOg9FcX3XlHtP9j/t5vW+/sHDn47c2Cq0fjnmm7VzcJr9ovbLx7uSIMXuuI';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
  }
}