<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class screenname{
         
 	  var $CI;
    public function __construct()
    {
            $this->CI =& get_instance();
            $this->CI->load->database();
    }

              public function getscreen(){
              if($this->CI->session->userdata['loginid']){
              $roleid = $this->CI->session->userdata('roleid');
              $result_query = $this->CI->db->query("CALL dive_load_MenuList('".$roleid."')");
              $mainmenu = $result_query->result();
              $result_query->next_result();
              $result_query->free_result();
              $zeroflag_query = $this->CI->db->query("select * from dive_screen_master where flag='n'");
              $zeroflag = $zeroflag_query->result();
              //$mainmenu['zeroflag'] = $zeroflag;
              $zeroflag_name = array();
              foreach($zeroflag as $key => $value){
                $zeroflag_name[] = $value->screenname;
              }
              $zeroflag_url = array();
              foreach($zeroflag as $key => $value){
                $zeroflag_url[] = $value->url;
              }
              $unique = array();
              foreach($mainmenu as $mainkey => $mainvalue){
                $unique[] = $mainvalue;
              }
              $selfunique = array();
              $selfunique1 = array();
              $icon = array();
              foreach($unique as $value){
                if($value->MenuID == $value->submenuparentid){
                  $selfunique[$value->submenuitem] = $value->MenuItem;
                }
                else{
                  $selfunique1[] = $value->MenuItem;
                }
              }
              $resultvalue   = [];
              $unique = array_unique($selfunique);
              foreach($unique as $search) {
                $found = array_keys($selfunique, $search);
                if(count($found) > 1) {
                  $resultvalue[$search] = $found;
                }
                else{
                $resultvalue[$search] = $found;
                }
              }
            $resultvalue1 = array();
            $resultvalue2 = array();
            foreach($selfunique1 as $value){
              foreach($resultvalue as $val){
                if(!in_array($value, $val))
                {
                  $resultvalue1[$value] = $value;
                }
                if(in_array($value, $val))
                {
                  $resultvalue2[$value] = $value;
                }
              }
            }
            $result = array_diff($resultvalue1,$resultvalue2);

            $mainmenu_data['menu_data']   = array_merge($resultvalue,$result);
           
            $icononly = array();
            $urlonly = array();
            foreach($mainmenu as $meinkey => $value){
              foreach($mainmenu_data['menu_data'] as $subkey => $subvalue){
                $icononly[$value->MenuItem] = $value->Icon;
                $urlonly[$value->submenuitem]  = $value->submenuurl;    
              }
            }
            $mainmenu_data['icononly']  = $icononly;
            $mainmenu_data['url']   = $urlonly;
            $mainmenu_data['zeroflag_name']  = $zeroflag_name;
            $mainmenu_data['zeroflag_url']   = $zeroflag_url;
            return $mainmenu_data;
      }else
      {
       redirect('/Login/index','refresh'); 
      }
    }
  }
     