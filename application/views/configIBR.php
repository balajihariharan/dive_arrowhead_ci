<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
     <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>
        <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/x2js/1.2.0/xml2json.min.js"></script> -->
</head>
<body class="main" ng-controller="myController" ng-cloak>
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src="<?php echo base_url().'assets/';?>img/loader.gif" width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="images/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="images/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="images/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="images/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->

    <div>
       <!--  <script>
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script> -->
        <style type="text/css">

.label_inner_general_three{
        font-weight: 600;
    display: inline-block;
    margin: 15px 0 0 0;
    vertical-align: top;
}
            .flt_left{
                float: left;
            }

            .ex_in_main{
                float: left;
                margin: 0 0 0 40px;
            }

            .ex_in_main p{
                margin: 0 0 10px 0;
                font-weight:600;
            }

          
             .pagi_master li{
                list-style-type: none;
                display: inline-block;
                margin: 0 3px;
                background: #5D85B2;
                color: #fff;
                
                border-radius: 3px;
                cursor: pointer;
                    }

                    .pagi_master li a{
                            color: #fff;
                text-decoration: none;
                padding: 4px;
                    }
                .pagination ul>.active>a, .pagination ul>.active>span {
                color: #999999;
                cursor: default;
                }
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }
                .closecrawler {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }
             .closecrawler:hover,
                .closecrawler:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

                    .temp_upload{
            padding: 6px 30px 6px 10px;
            margin: 0% 11% 0% --228%;
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            font-family: sans-serif, Verdana, Geneva;
            text-align: right;
            display: inline-block;
            color: #fff;
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
            font-size: 13px;
        }

         .viewicon{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/edit.png') no-repeat scroll 5px center;
                    padding: 18px 6px 0px 28px;
                    }
            .viewicon2{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>assets/img/delete_tbl.png') no-repeat scroll 16px center;
                    padding: 18px 6px 0px 28px;
            }
            .pop_pad_all_add_new .channel_main{
                width:93%;
                margin: 0 auto;
                display: block;
                overflow: hidden;
            }
            .label_inner_general{
                font-weight: 600;
                width: 260px;
            }
            .pop_pad_all_add_new{
                padding: 20px;

            }
            .save_icon_manage{
                float: none;
            }

            .legendclass{
                color: #2C3E50;
                font-weight: bold;  
                margin: 0 5px;
                padding: 0 10px;
            }

            .fieldsetclass{
                /*border-color: #2C3E50;*/
                padding: 8px 10px;
                margin: 8px 0;
                border-radius: 2px;
                border: 1px solid #cccccc8a;
                box-shadow: 0px 1px 6px rgba(199, 199, 199, 0.54);
            }

            /*.filterclass{
                background: #dee7f8 none repeat scroll 0 0;
                border: 1px solid #cad8f3;
                border-radius: 6px;
                padding: 0 5px;
                float: left;
                font-size: 11px;
            }*/
            .filterclass{
                margin: 6px 0;
                padding: 0 5px;
                font-size: 13px;
            }
           /* .filterclass span{
                background: #dee7f8 none repeat scroll 0 0;
            }*/
            .filterclass p{
            background: #dee7f8 none repeat scroll 0 0;
            display: inline-block;
            border-radius: 6px;
             padding: 0 5px;
            }
            .filterclass strong{
                font-weight: 600;
            }

            .bold{
                font-weight: bold;
            }
            #searchResult_main{
             border: 1px solid #aaa;
            overflow-y: auto;
            width: 205px;
            position: absolute;
            margin-left: 83px;
            padding-bottom: 0px;
            min-height: 10px;
            max-height: 150px;
            }

            #searchResult_main li{
             background: white;
             padding: 4px;
            cursor: pointer;
                list-style: none;
                border-bottom: 1px solid #ededed;
                font-size: 14px;
            }

            #searchResult_main li:hover{
                background: #f1f1f1;
            }
            #searchResult{
                border: 1px solid #aaa;
                overflow-y: auto;
                width: 205px;
                position: absolute;
                /*margin-left: 365px;*/
                margin-left: 83px;
                padding-bottom: 0px;
                min-height: 10px;
                max-height: 150px;
                margin-top: 0px;
            }

            #searchResult li{
             background: white;
             padding: 4px;
            cursor: pointer;
                list-style: none;
                border-bottom: 1px solid #ededed;
                font-size: 14px;
            }

            #searchResult li:hover{
                background: #f1f1f1;
            }
            .temp_upload{     
            padding: 6px 30px 6px 10px;     
            margin: 0% 11% 0% -26%;     
            border: none;       
            -webkit-border-radius: 3px;     
            -moz-border-radius: 3px;        
            border-radius: 3px;     
            cursor: pointer;        
            font-family: sans-serif, Verdana, Geneva;       
            text-align: right;      
            display: inline-block;      
            color: #fff;        
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);      
            font-size: 13px;        
        }       
         .temp_upload_new{      
            padding: 6px 30px 6px 10px;     
           margin: 5% 1% 0% 0%;     
            border: none;       
            -webkit-border-radius: 3px;     
            -moz-border-radius: 3px;        
            border-radius: 3px;     
            cursor: pointer;        
            font-family: sans-serif, Verdana, Geneva;       
            text-align: right;      
            display: inline-block;      
            color: #fff;        
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);      
            font-size: 13px;        
        }     
        .cron_upload{     
            padding: 6px;     
            margin: 0% 11% 0% -26%;     
            border: none;       
            -webkit-border-radius: 3px;     
            -moz-border-radius: 3px;        
            border-radius: 3px;     
            cursor: pointer;        
            font-family: sans-serif, Verdana, Geneva;       
            text-align: right;      
            display: inline-block;      
            color: #fff; 
            background: linear-gradient(#009e47, #00713b);      
            /*background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);*/      
            font-size: 13px;        
        }   
        .close1 {       
            color: #aaa;        
            float: right;       
            font-size: 28px;        
            font-weight: bold;      
        }       
                .close1:hover,      
                .close1:focus {     
                    color: #2d8bd5;     
                    text-decoration: none;      
                    cursor: pointer;        
                }    



                .close2 {       
            color: #aaa;        
            float: right;       
            font-size: 28px;        
            font-weight: bold;      
        }       
                .close2:hover,      
                .close2:focus {     
                    color: #2d8bd5;     
                    text-decoration: none;      
                    cursor: pointer;        
                }    

                #my_file {      
                display: none;      
            }
            .new_plan_list_view p {
                    line-height: 27px;
            }
            /**************/

            .form_head{
                padding: 0px 0px 10px 10px;
                margin-bottom: 10px;
            }
            .run_cron_start{
                float: left;
                margin-top: 10px;
                color: #06a206;
                font-size: 20px;
                font-weight: 600;
            }
            .run_cron_start p{
                display: inline-block;
            }
            .run_cron_status{
                float: left;
            }
            .run_cron_status ul li{
                float: left;
                padding: 0 30px;
                /*margin-top: 13px;*/
            }
            .run_cron_status ul li span.run_cron_sta_value{
                display: block;
                padding: 10px 0;
            }
            .run_cron_status ul li span.run_cron_sta_header{
                font-size: 18px;
                 font-weight: 600;
                 color: #2678b9;
            }
            .tooltip {
                position: relative;
                display: inline-block;
            }

            .tooltip .tooltiptext {
                visibility: hidden;
                width: 120px;
                background-color: #2c3e50;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px 0;
                position: absolute;
                z-index: 1;
                top: 2px;
                left: 57px;
            }

            .tooltip .tooltiptext::after {
                content: "";
                position: absolute;
                top: 50%;
                right: 100%;
                margin-top: -5px;
                border-width: 5px;
                border-style: solid;
                border-color: transparent #2c3e50 transparent transparent;
            }
            .tooltip:hover .tooltiptext {
                visibility: visible;
            }

           .run_cron_start .tooltip .tooltiptext{
            left: 28px;
            font-size: 15px;
            font-weight: 500;
            top:0;
           }

           .blink-infinite {
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
    -webkit-animation-name: blink;
    -moz-animation-name: blink;
    -o-animation-name: blink;
    animation-name: blink;
    -webkit-animation-timing-function: linear;
    -moz-animation-timing-function: linear;
    -o-animation-timing-function: linear;
    animation-timing-function: linear;
    -webkit-animation-duration: 1s;
    -moz-animation-duration: 1s;
    -o-animation-duration: 1s;
    animation-duration: 1s;
}
.out-of-stock {
    background: rgba(255,0,0,1);
    padding: 3px 6px!important;
    color: #fff;
    border-radius: 4px;
    float: right;
    font-size: 9px;
}
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                   <?php echo "<h2>".$screen."</h2>" ;?>
                </div>
            </div>
        </div>
             <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
                <div class="content_main_all">
                    <div class="form_head">
                        <div id="run_cron" class="run_cron_start" style="display: none;"><p>Process Scheduled Successfully</p>
                            <!-- <div class="tooltip">
                                    <img style="vertical-align: middle;" src="<?php echo base_url();?>assets/img/refresh.png" ng-click="cron_status()">
                                    <span class="tooltiptext">Refresh</span>
                                </div> -->
                        </div>
                        <div class="run_cron_status" style="display: none;">
                            <ul>
                                <li><span class="run_cron_sta_header">Start Date</span>
                                    <span class="run_cron_sta_value">{{cron_sta_startdate}}</span>
                                </li>
                                <li>
                                    <span class="run_cron_sta_header">End Date</span>
                                    <span class="run_cron_sta_value">{{cron_sta_enddate}}</span>
                                </li>

                                <li>
                                    <span class="run_cron_sta_header">Status</span>
                                    <span class="run_cron_sta_value">{{cron_sta_progress}} out of {{cron_sta_total}}</span>
                                </li>
                               <!--  <li class="tooltip">
                                    <img style="margin-top: 9px;" src="<?php echo base_url();?>assets/img/refresh.png" ng-click="cron_status()">
                                    <span class="tooltiptext">Refresh</span>
                                </li> -->
                            </ul>
                            
                        </div>
                        <div id="run_cron_end"  class="run_cron_start" style="display: none;"><p>Process Completed Successfully</p></div>
                        <form>
                            
                              <div class="button_right">
                                    <a href="#" class="last_menu_icon">
                                                <input type="button" class="temp_upload" ng-click="import()" value="Bulk Import" />
                                    </a>
                                </div>
                                <input type="file" id="my_file" file-reader="parts"/>

                                <div class="button_right" id="exportsel">
                                    
                                     <li class="submenu">
                                            <a class="last_menu_icon" href="#">
                                                <input type="button" class="button_download" value="Export" style="margin-right: 57px;"/>
                                            </a>
                                            <ul class="level2">
                                                <li>
                                                    <a href="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_AutoRawlist.csv">As CSV</a>
                                                </li>
                                                <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel()">As Excel</a></li>
                                            </ul>
                                        </li>
                                </div>   
                                   
                                   <div class="button_right">
                                                <input type="button" class="button_download" ng-click="Bulkexport()" value="Bulk Export" style="margin-right: 57px;"/>
                                            </div>

                                <div class="button_right">

                                     <input type="button" class="cron_upload" ng-click="Cron()" value="Start Run" />
                                </div>    
                        </form>
                    </div>
                    <div class="lable_normal" id = "storeinput">StoreName<span style="color:red; font-size:16px;"></span></div>
                        <div class="lable_normal"><select name="category[]"  ng-options="::substore.Sellername for substore in Storename track by ::substore.CompID" ng-model="StoreName" id="store" class="select" ng-change="get_categoryforstore()" style="display: inline-block; float: none;">
                            <option value="" >-Select-</option>
                        </select>
                    </div>
                    <input type="radio" class="radiobt"  id="category" name="selecttype"  ng-click="selected(1)" checked>
                        <div class="lable_normal" id = "catinput">Category Details <span style="color:red; font-size:16px;"></span></div>
                        <div class="lable_normal"><select name="category[]"  ng-options="::subcat.cat for subcat in categorycomboload track by ::subcat.Categoryid" ng-model="categorylist" id="popupcloseevent" class="select" ng-change="get_category_result()" style="display: inline-block; float: none;">
                            <option value="">-Select-</option>
                        </select>
                    </div>

                         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                         <input type="radio" class="radiobt"  id="sku" name="selecttype"  ng-click="selected(2)">
                       <div class="lable_normal" id ="skuinput">SKU <span style="color:red; font-size:16px;"></span></div>
                        <div class="lable_normal"><input id ="Skuinput" ng-model="Skuinput" ></div>

                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<div class="lable_normal"><input id="buttoninput" ng-model="Skuinput" ng-click="get_sku_result(Skuinput)" class="cron_upload" type="button" value="Go" /></div> 

                         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                         <input type="radio" class="radiobt"  id="unprocessed" name="selecttype"  ng-click="selected(3)">
                       <div class="lable_normal" id ="skuinput">UnProcessed Items <span style="color:red; font-size:16px;"></span></div>

                         <!-- <input type="radio" class="radiobt"  id="sku" name="selecttype"  ng-click="selected(3) ;get_changeditems_result() "> -->
                         <!-- <div class="lable_normal">Modified Items</div> -->
                          
                       <!--  <input type="hidden" name="api_hidden_id" id="api_hidden_id" ng-model="api_hidden_id" value="<?php echo $GUID; ?>" /> -->

                         <!-- <select  name="category[]"  ng-options="::subcat.cat for subcat in categorycomboload track by ::subcat.Categoryid" ng-model="category" id="category" class="select">
                             <option value="">-Select-</option>
                             </select> -->
                             <div>
                                <div class="error_msg_all" style="display: none;" id="emsg_id">No Data to display</div>
                            </div>
                            <div>
                                <div class="error_msg_all" style="display: none;" id="selectstorename">Please select Store name</div>
                            </div>
                            <div>
                                <div class="error_msg_all" style="display: none;" id="selectsku">Please fill the SKU Details</div>
                            </div>
                            <div class="itm_lst"><p >Total Item Listed:</p><p style="font-size: 20px; color: #00886d;">{{totalitems}}</p></div>
                        <div class="plan_list_view new_plan_list_view">
                            <table cellspacing="0" border="1" class="table price_smart_grid_new">
                                <thead>
                                    <tr>
                                        <th class="curser_pt" ng-click="sortBy('storename')">StoreName</th>
                                        <th class="curser_pt" ng-click="sortBy('itemid')">ItemID</th>
                                        <th class="curser_pt" ng-click="sortBy('sku')">SKU</th>
                                        <!-- <th class="curser_pt" ng-click="sortBy('Category')">Category</th> -->
                                        <th class="curser_pt" ng-click="sortBy('title')">Title</th>
                                        <!-- <th class="curser_pt" ng-click="sortBy('filters')">Filter</th> -->
                                        
                                        <th class="curser_pt" ng-click="sortBy('keywords')" style="width: 18%;">Search Keyword</th>
                                        <th  >Active</th>
                                        <th  >Action</th>
                                    </tr>
                                   <tr class="tab1table">
                                       <td>
                                            <div class="search"><input type="text" ng-model="storenamesearch" ng-change="storesearch()" class="inputsearch"></div>
                                       </td>
                                       <td>
                                            <div class="search"><input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch"></div>
                                       </td>
                                       <td>
                                            <div class="search">
                                         <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch">
                                       </div>
                                       </td>
                                    <!--    <td>
                                        <div class="search">
                                         <input type="text" ng-model="Categoryquery" ng-change="Categorysearch()" class="inputsearch">
                                      </div>
                                       </td> -->
                                       <td>
                                      <div class="search">
                                        <input type="text" ng-model="titlequery" ng-change="mytitlesearch()" class="inputsearch">
                                      </div> 
                                       </td>
                                      <!--  <td>
                                            <div class="search">
                                               <input type="text" ng-model="Filtersquery" ng-change="filtersearch()" class="inputsearch" placeholder="Filter">
                                            </div> 
                                        </td>-->
                                        <td>
                                            <!-- <div class="search">
                                               <input type="text" ng-model="SearchKeywordquery" ng-change="keywordsearch()" class="inputsearch" placeholder="Search Keyword">
                                            </div> -->
                                        </td>
                                        <td></td>
                                         <td></td>
                                      </tr>
                                </thead>
                        <tbody>
                           <!-- <tr ng-repeat="data in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse"> -->
                            <tr ng-repeat="data in pagedItems[currentPage] | filter:search ">
                                <td>{{data.storename}}</td>
                                <td>{{data.itemid}}</td>
                                <td>{{data.sku}}</td>
                                <!-- <td>{{data.category}}</td> -->
                                <td>
                                     <a ng-click="redirectToLink($event)" element="{{data.itemid}}" style="cursor: pointer;">{{data.title}}
                                    </a>
                                </td>
                                <td>
                                <p ng-if="data.Keywords_OR_validate || data.Active_flag==1 "><span style="font-weight:bold;padding-left: 0px;">OR</span> :{{data.Keywords_OR}}</p><p ng-if="!data.Keywords_OR_validate"></p><br>
                                <p ng-if="data.Keywords_AND"><span style="font-weight:bold;padding-left: 0px;">AND</span> :{{data.Keywords_AND}}</p><p ng-if="!data.Keywords_AND"></p><br>
                                <p ng-if="data.Exclude_key_validate || data.Active_flag==1 "><span style="font-weight:bold;padding-left: 0px;">Exclude Keywords</span>:{{data.Exclude_keywords}}</p><p ng-if="!data.Exclude_key_validate"></p>  
                                </td>
                                <td style="text-align: center;">
                                    <input type="checkbox" name="temp" ng-checked="data.Active_flag==1" ng-click="flagupdate($event)" class="field_chk ex_list_checkbox" data-value="{{data.itemid}}"/>
                                </td>
                                <!--  <td ng-if="data.activeflag=='1'" style="text-align: center;">
                                    <input type="checkbox" name="temp" ng-click="flagupdate($event)" class="field_chk ex_list_checkbox" data-value="{{data.itemid}}" checked/>
                                </td> -->
                                    <td>
                                        <table class="table_inner">
                                        <tbody>
                                        <tr>
                                            <td ng-click="editpopup($event)" dataitemid="{{data.itemid}}" datacategoryid="{{data.categoryid}}" datasku="{{data.sku}}" datacategory="{{data.category}}" datatitle="{{data.title}}" datasearchkeyword="{{data.SearchKeyword}}" dataprice="{{data.price}}" dataCategory_Exclude="{{data.Category_Exclude}}" dataCategory_Include="{{data.Category_Include}}" dataSellers_Exclude="{{data.Sellers_Exclude}}" dataSellers_Include="{{data.Sellers_Include}}" dataConditions="{{data.Conditions}}" dataExclude_keywords="{{data.Exclude_keywords}}" dataKeywords_AND="{{data.Keywords_AND}}" dataKeywords_OR="{{data.Keywords_OR}}" dataKeywords_OR_validate="{{data.Keywords_OR_validate}}" dataLocation="{{data.Location}}" dataPriceform_from="{{data.Priceform_from}}" dataPriceform_to="{{data.Priceform_to}}" dataShipping="{{data.Shipping}}" datahandlingtime ="{{data.HandlingTime}}" dataTitleincludeflag ="{{data.Titleincludeflag}}" dataTopRated ="{{data.TopRated}}" dataSort_order="{{data.Sort_order}}" datacrawler_url="{{data.crawler_url}}" dataebay_url="{{data.ebay_url}}" datamy_price="{{data.my_price}}" dataPercent="{{data.Percent}}" data_mpn="{{data.MPN}}" data_ipn="{{data.IPN}}" data_opn="{{data.OPN}}" data_compid="{{data.compid}}" dataAppID = "{{data.AppID}}"
                                            style="cursor:pointer; text-align:justify;" class="viewicon" >
                                            </td>
                                            <td ng-click="deletepopup($event)" dataitemid="{{data.itemid}}" class="viewicon2"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                            <!-- pagination ui part -->
                                <div class="pagination pull-right pagi_master">
                                    <ul>
                                        <li ng-class="{disabled: currentPage == 0}">
                                            <a href ng-click="prevPage()">« Prev</a>
                                        </li>
                                        <li ng-repeat="n in pagenos | limitTo:5"
                                            ng-class="{active: n == currentPage}"
                                        ng-click="setPage()">
                                            <a href ng-bind="n + 1">1</a>
                                        </li>
                                        <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                            <a href ng-click="nextPage()">Next »</a>
                                        </li>
                                    </ul>
                                </div>
                         <!-- pagination ui part end -->

                        </div>

                        </div>
                    </div>
                </div>
                
        </div>
        
<div id="Pnopopup2" class="modal">
    <div class="pop_map_my_part conf_auto_rawlist">          
        <div class="pop_container">
            <div class="heading_pop_main">
                <h4>Edit</h4>
                <span class="close" ng-click="popupclose()">&times;</span>
            </div>
            <div class ="pop_pad_all_add_new" style="color: #666;">

                <fieldset class="fieldsetclass">
                    <legend class="legendclass" align="center">Item Specifics</legend>
                    <div>
                        <table class="pop_item_spec">
                            <tr>
                                <td style="font-weight: bold;">Item ID</td>
                                <td ng-bind="currentitemid" style="color: chocolate;font-weight: bold;"></td>
                                <td style="font-weight: bold;">SKU</td>
                                <td ng-bind="currentsku"></td>
                            </tr>
                            <!-- <tr>
                                <td style="font-weight: bold;">SKU</td>
                                <td ng-bind="currentsku"></td>
                            </tr> -->
                            <tr>
                                <td style="font-weight: bold;">Category</td>
                                <td ng-bind="currentcatid"></td>
                                <td style="font-weight: bold;">Title</td>
                                <td><a href="https://www.ebay.com/itm/{{currentitemid}}" target="blank">{{currenttitle}}</a>
                                </td>
                            </tr>
                            <!-- <tr>
                                <td style="font-weight: bold;">Title</td>
                                <td><a href="https://www.ebay.com/itm/{{currentitemid}}" target="blank">{{currenttitle}}</a></td>
                            </tr> -->
                        </table>
                        <label class="title-inc"><input type="checkbox" id="title_inc" ng-click="keywrd_length_check()" name="title_inc" ng-model="title_inc" ng-true-value="'1'" ng-false-value="'0'">Title Include</label>
                    </div>
                </fieldset>
            
                 <div id="edit_mp_ip_opn" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #3966ad; border-radius: 5px; color: white;padding: 8px 0px 8px 0px;">MPN,OPN,IPN Not Available...</p></center>

                </div>
                 <div class="left_fieldset">
                    <fieldset class="fieldsetclass popup_keyword ">
                        <legend class="legendclass">Keywords</legend>
                        <div>

                            <label for="exckeyword">
                                OR (Any Words Any Order) :
                                <!-- ng-model="keywordsAND" -->
                                <span class="key-length"><p class="key-length key-blink" ng-if="keywrd_length < 0">Maximum Length is 300</p>{{keywrd_length}} left</span>
                            <textarea class="autosizeinput" id="orinput" width="100%" ng-model="keywordsor" value="" ng-change="keywrd_length_check();changemade()"></textarea>
                                <!-- <input type="text" style="width:100%; margin-top: 10px;" maxlength="300" /> -->
                            </label>
                            
                            <label for="exckeyword">
                                AND (All Words Any Order) : 
                                <!-- ng-model="keywordsOR" -->
                               <!--  <input type="text" class="input_box_full" id="andinput" ng-change="keywrd_length_check()" ng-model="keywordsand" value=""/> -->
                            <textarea class="autosizeinput"  width="100%" id="andinput" ng-change="keywrd_length_check();changemade()" ng-model="keywordsand" value=""></textarea>
                            </label>
                        </div>
                           <!--  <label class="exckeyword" for="exckeyword"> -->
                             <label  for="exckeyword">
                             Exclude KeyWords : 
                                <!-- <input type="text" name="exckeyword" ng-change="keywrd_length_check()" ng-model="keywordsmore" maxlength="300" value=""/> -->
                            <textarea class="autosizeinput"  width="100%" name="exckeyword" ng-change="keywrd_length_check();changemade()" ng-model="keywordsmore" maxlength="300" value=""></textarea>
                                (Comma Separated)
                            </label>
                            <!-- <input type="submit"  style="padding: 7px 24px 5px 6px;" ng-click="reset()" ng-disabled="reset_disable" class="button_search" value="Reset"> -->
                            <div>
                                <input type="submit"  style="padding: 7px 24px 5px 6px;" ng-click="reset()" ng-disabled="reset_disable" class="button_search" value="Reset">
                            </div>
                        </fieldset>
                     <div id="mp_ip_opn" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #3966ad; border-radius: 5px; font-size:14px; color: white;padding: 8px 0px 8px 0px;">MPN,OPN,IPN Not Available...<span>Do You Want to set Previous Keyword  <button class="mp_ip_yes" type="submit" ng-click="set_yes()">Yes</button><button class="mp_ip_no" type="submit" ng-click="set_none()">No</button> </span></p></center>

                    </div>


                    <fieldset class="fieldsetclass ship_loc">
                      <legend class="legendclass">Shipping & Location</legend>
                        <label for="freeship">
                            <input  type="checkbox" id="freeship" name="freeship" ng-model="freeshiping" ng-change="changemade()" element="Free Shipping" ng-true-value="'Free Shipping'" ng-false-value="''" value="1">Free</label>
                        
                        <label for="location">
                            <input  type="checkbox" id="location" name="location" element="Only US"  ng-model="location" ng-change="changemade()" ng-true-value="'Only US'" ng-false-value="''" value="1">Only US</label>

                        <label for="handlingtime">
                           <input  type="checkbox" id="handling" name="handlingtime" element="HandlingTime"  ng-model="handling" ng-change="changemade()" value="1" ng-true-value="'HandlingTime'" ng-false-value="''" >HT<=5D</label>
                            <br/>
                    </fieldset>
                    <fieldset class="fieldsetclass sort_order">
                      <legend class="legendclass">Sort Order</legend>
                            <label>Sort Order :</label>
                            <!-- $event,this.options[this.selectedIndex].getAttribute('itemvalue') -->
                                <select style="color: #505050;" ng-model="sortorder"  ng-change="sortorderchange();changemade()" class="sortorderselect">
                                    <option value="0">--Select--</option>
                                    <option itemvalue="Best Match" value="12">Best Match</option>
                                    <option itemvalue="Time: ending soonest" value="1">Time: ending soonest</option>
                                    <option itemvalue="Time: newly listed" value="10">Time: newly listed</option>
                                    <option itemvalue="Price + Shipping: lowest first" value="15">Price + Shipping: lowest first</option>
                                    <option itemvalue="Price + Shipping: highest first" value="16">Price + Shipping: highest first</option>
                                    <option itemvalue="Distance: nearest first" value="7">Distance: nearest first</option>
                                </select>
                    </fieldset>

                    </div>
                    <div class="right_fieldset">
                         <fieldset class="fieldsetclass"> 
                            <legend class="legendclass">Category</legend>
                            <div>
                                <div class="cate_left">
                                <label>Exclude</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="loadcategory" id="normal_search_inc" ng-change="partnosearch_main();changemade()" ng-model="partnofirst"  value="" autocomplete="off" >

                                 <ul id='searchResult_main' style="display: none;">
                                                        <li ng-click='setValue_main(result.category,result.CategoryID)' ng-repeat="result in suggestcat | filter : partnofirst" id="cat_inc" value="{{result.CategoryID}}" ><p ng-click="get_category(result.category,result.CategoryID,'Exclude')">{{result.category}}</p></li>
                                 </ul>
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="cate_right">
                                <label>Include Only</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="loadcategory" id="normal_search_exc" ng-change="partnosearch();changemade()" ng-model="partno"  value="" autocomplete="off" > 
                                     <ul id='searchResult' style="display: none;">
                                                        <li ng-click='setValue(result.category,result.CategoryID)' ng-repeat="result in suggestcat | filter : partno" id="cat_exc"  value="{{result.CategoryID}}" ><p ng-click="get_category(result.category,result.CategoryID,'Include')">{{result.category}}</p></li>
                                    </ul>
                                
                            </div>
                            </div>
                        </fieldset>
                        <fieldset class="fieldsetclass popup_price">
                            <legend class="legendclass">Price</legend>
                                <label for='price' >Percentage</label>
                                ± <input type='text' value="" id="percent"  style="min-width:5%; max-width: 8%" ng-model="percent"  ng-change="percentchange();changemade()" style=""/> <span style="font-size: 14px; font-weight: 600;">% of My Price [{{currentprice}}] </span><!-- {{currentprice}} -->
                                <!-- <br><br> -->
                                <label for='price' class="from_price">From</label> 
                                $ <input type='text' ng-model="minus" style="min-width:5%; max-width: 8%" name="minus"/> To 
                                $ <input type='text' ng-model="plus" style="min-width:5%; max-width: 8%" name="plus"/>   <!-- <input ng-click="clearpercent();" type="button" value="Clear"> -->
                        </fieldset>
                        <fieldset class="fieldsetclass">
                            <legend class="legendclass">Condition</legend>
                            <label for="connew">
                                <input type="checkbox" id="con_New" name="condition" ng-model="condition" ng-change="changemade()" ng-true-value="'3'" ng-click="conditionchange($event)" element="New" ng-false-value="''" value="3" ng-checked="isChecked">
                                New
                            </label>
                            <label for="conused">
                                <input type="checkbox" id="con_Used" name="condition" ng-model="condition" ng-change="changemade()" ng-true-value="'4'" ng-false-value="''" ng-click="conditionchange($event)" element="Used" value="4" ng-checked="isChecked">
                                Used
                            </label>
                            <!-- <label for="remanu">
                                <input type="checkbox" id="con_Remanufactured" name="condition" ng-model="condition" ng-true-value="'Remanufactured'" ng-false-value="''" ng-click="conditionchange($event)" element="Remanufactured" value="2" >
                                Remanufactured
                            </label>
                            <label for="conused">
                                <input type="checkbox" id="con_parts" name="condition" ng-model="condition" ng-true-value="'For Parts Or Not Working'" ng-false-value="''" ng-click="conditionchange($event)" element="For Parts Or Not Working" value="1" >
                                For Parts Or Not Working
                            </label> -->
                            <label for="connc">
                                <input type="checkbox" id="con_notspecified" name="condition" ng-model="condition" ng-change="changemade()" ng-true-value="'10'" ng-false-value="''" ng-click="conditionchange($event)" element="Not Specified" value="10" ng-checked="isChecked">
                                Not Specified
                            </label>
                        </fieldset>
                    </div>
                <div class="left_fieldset">
                    
                </div>
                <div class="right_fieldset">
                    <fieldset class="fieldsetclass">
                        <legend class="legendclass">Sellers</legend>
                            <div>
                                <label style="display: block; margin-bottom: 7px;">
                                    Specific Sellers (Enter Seller's User Id)
                                </label>
                                <div class="cate_left">
                                <label for='includeseller'>Include Only</label>
                                <input type='text' id="comp_inc" ng-blur="get_competitor(comp_inc,'Include')" ng-model="comp_inc" ng-change="changemade()" value="" />
                                </div>
                                <div class="cate_right">
                                <label for='excludeseller'>Exclude</label> 
                                <input type='text' id="comp_exc" ng-blur="get_competitor(comp_exc,'Exclude')" ng-model="comp_exc" ng-change="changemade()" value=""/>
                                <div class="cate_right">
                                <label for="toprate">
                                    <input  type="checkbox" id="toprate" name="toprate" ng-model="TopRated" ng-change="changemade()" element="TopRated" ng-true-value="'TopRated'" ng-false-value="''" value="1">Top Rated Seller</label>
                                </div>
                                <br/>
                            </div>
                    </fieldset>

                </div>
                <div class="left_fieldset">
                    
                </div>
                <div class="full_fieldset">
                    <fieldset class="fieldsetclass">
                        <legend class="legendclass" style="text-align:center;">Applied Filters</legend>
                        <div>
                            <!--  Forklifts,Solenoids -->
                            <li class="filterclass" ng-if="keywordsor != ''"><p><span class="bold" ng-if="keywordsor != ''">OR : </span>{{keywordsor}}
                                <span ng-click="eventclose($event)" datalabel="keywordsor" datavalue="{{keywordsor}}" ng-if="keywordsor != ''" style="cursor:pointer">&nbsp;&nbsp;x</span></p>
                            </li>
                            
                            <li class="filterclass" ng-if="keywordsand != ''"><p><span class="bold" ng-if="keywordsand != ''">AND : </span>{{keywordsand}}
                                <span ng-click="eventclose($event)" datalabel="keywordsand" datavalue="{{keywordsmore}}" ng-if="keywordsand != ''" style="cursor:pointer">&nbsp;&nbsp;x</span></p>
                            </li>
                            
                            <li class="filterclass" ng-if="keywordsmore != ''"><p><span class="bold" ng-if="keywordsmore != ''">KeyWord : </span>{{keywordsmore}}
                                <span ng-click="eventclose($event)" datalabel="keywords" datavalue="{{keywordsmore}}" ng-if="keywordsmore != ''" style="cursor:pointer">&nbsp;&nbsp;x</span></p>
                            </li>
                            
                            <!-- ng-hide="resultlabellength == 0 || resultlabellength == NULL" -->
                            <li class="filterclass" ng-model="resultlabellength"><p>
                                <!-- ng-hide="resultlabellength == 0 || resultlabellength == NULL" -->
                                <span class="bold" ng-model="resultlabellength">Category : </span>
                                <span ng-repeat="(key,catvalue) in resultcatlabel" cate-value="{{catvalue.id}}">{{catvalue.name}}<span ng-click="eventclose($event)" datalabel="category" datavalue="{{catvalue.id}}" style="cursor:pointer">&nbsp;x&nbsp;&nbsp;</span></span></p>
                            </li>
                            
                            <!-- <span ng-show="!$last">,</span>  ng-hide="pricevalue == 0"-->
                            <!--     -->
                            <li class="filterclass" ng-model="pricevalue" ng-hide="pricevalue == 0"><p><span class="bold" ng-hide="pricevalue == 0" ng-model="pricevalue">Price : </span><strong>From </strong>${{minus | currency : ''}} <strong>To</strong> ${{plus | currency : ''}}
                                <span ng-click="eventclose($event)" datalabel="pricelabel" style="cursor:pointer">&nbsp;&nbsp;x</span><p>
                            </li>
                            
                             <li class="filterclass" ng-if="resultcondition != NULL" ng-if="resultcondition != ''"><p><span class="bold" ng-if="resultcondition != ''">Condition :</span>{{resultcondition}}
                                <span ng-click="eventclose($event)" datalabel="conditionlabel" ng-if="resultcondition != ''" style="cursor:pointer">x</span></p>
                            </li>
                            
                            <li class="filterclass" ng-if="freeshiping || location || handling != NULL"><p>
                                <span class="bold" ng-if="freeshiping == 'Free Shipping' || location == 'Only US' || handling=='HandlingTime'">
                                    Shipping &amp; Location :</span> {{freeshiping}}
                                    <span ng-click="eventclose($event)" datalabel="freeshiping" ng-if="freeshiping == 'Free Shipping'" style="cursor:pointer">x</span> 
                                    {{location}}
                                    <span ng-click="eventclose($event)" datalabel="location" ng-if="location == 'Only US'" style="cursor:pointer">x</span>
                                    {{handling}}
                                    <span ng-click="eventclose($event)" datalabel="Handling" ng-if="handling=='HandlingTime'" style="cursor:pointer">x</span>
                                </p>
                            </li>
                            <li class="filterclass" ng-if="TopRated != NULL"><p>
                                <span class="bold" ng-if="TopRated=='TopRated'">
                                   Seller Type:</span> 
                                    {{TopRated}}
                                    <span ng-click="eventclose($event)" datalabel="TopRated" ng-if="TopRated=='TopRated'" style="cursor:pointer">x</span>
                                </p>
                            </li>
                            
                             <li class="filterclass" ng-model="resultcomplabellength" ng-hide="resultcomplabellength == 0"><p>
                                <span class="bold" ng-model="resultcomplabellength" ng-hide="resultcomplabellength == 0">Sellers :</span>
                                <span ng-repeat="(catkey,catvalue) in resultcomplabel" comp-value="{{catvalue.key}}">{{catvalue.key}}<span ng-click="eventclose($event)" datalabel="seller" datavalue="{{catvalue.key_id}}" style="cursor:pointer">&nbsp;x&nbsp;&nbsp;</span></span></p>
                            </li>

                            <li class="filterclass" ng-hide="resultsortorder == NULL"><p><span class="bold" ng-hide="resultsortorder == NULL">Sort Order :</span>{{resultsortorder}}
                                <span ng-click="eventclose($event)" datalabel="resultorder" datavalue="{{resultsortorder}}" style="cursor:pointer">x</span></p>
                            </li> 
                        </div>
                    </fieldset>  
                </div>
                    <div id="filtr_msg" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #1f6f1d;color: white; border-radius:5px;padding: 8px 0px 8px 0px;">Applied Filters Saved Successfully</p></center>
                    </div>
                        <!-- <hr size="2"> -->
                         <div class="conf_button_right" align="center">
                            <span>
                                <button type="submit" id="crwlbtn" style="padding: 7px 24px 5px 6px;" ng-click="crawlersearch()" class="button_search">API Search</button>
                            </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <span>
                                <button type="submit" id="testbtn" style="padding: 7px 24px 5px 6px;" ng-click="testsearch('')" class="button_search">eBay Search</button>
                            </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                           <span>
                               <button type="submit" style="padding: 7px 24px 5px 6px;"  ng-click="submititems()" class="button_add">Save Search</button>
                            </span>
                            <!-- <a href="">Clear options&nbsp;</a> -->
                        </div>

                </div>
            </div>
        </div>
    </div>

            <!--  import popup -->
                                <div id="popupform1">
                                    <div id="Pnopopup1" class="modal">

                                            <div class="pop_map_my_part">
                                               
                                                 <div class="pop_container" style="height: 191px;">
                                                        <div class="heading_pop_main">
                                                            <h4>

                                                                Import Config IBR #
                                                            </h4>
                                                             <span class="close1">&times;</span>
                                                        </div>
                                                       <div class="pop_pad_all">
                                                           <div style="color:#009e47; text-align:center; font-size:17px">{{message}}</div>
                                                           <div style="color:#ff3333; text-align:center; font-size:17px">{{errormessage}}</div>
                                                        </div>

                                                        <div class="button_center">
                                                            <input type="hidden" id="ftype"/>
                                                        
                                                        <input type="text" id="fname" class="input_med" readonly/>
                                                        <button type="submit" id="popsavemain" class="temp_upload_new" ng-click="upload()">Upload</button><span style="font-size:16px; font-weight:bold;">(.csv)
                                                        </div>
                                                        <div class="channel_main">
                                                           
                                                        </div>
                                                        
                                                        <div class="button_center padding_bottom_15">
                                                        <button type="submit" id="save_imp" class="button_download" ng-click="save_imp()">Save</button>
                                                        </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>

                    <div id="confirmpopup1">
                    <div id="confirmpnpopup1" class="modal">

                            <div class="pop_map_my_part">
                               
                                 <div class="pop_container" style="padding: 13px;">
                                        <div class="heading_pop_main">
                                            <h4>
                                                Confirm
                                            </h4>
                                             <span class="close2" style="padding-top: 2%;">&times;</span>
                                        </div>
                                       <div class="pop_pad_all">
                                           <div style="color:#009e47; text-align:center; font-size:17px">{{message}}</div>
                                           <div style="color:#ff3333; text-align:center; font-size:17px">{{errormessage}}</div>
                                        </div>

                                        <div class="channel_main" style="text-align: center;font-family: varela round;margin: 6px 0 27px 0;">
                                            There are <span style="color:red;font-weight: bold;">{{currentskucount}} Listings</span>  having this SKU.
                                            <br><br>
                                            Do you want to update these filters for all Items having this SKU ? 
                                        </div>
                                        <div class="button_center padding_bottom_15">
                                        <button type="submit" class="button_add" style="padding: 6px 13px 6px 10px;background: no-repeat right -1px #00763C;" ng-click="savefilters(0)">Update Current Listing</button>
                                        <button type="submit" class="button_add" style="padding: 6px 13px 6px 10px;background: no-repeat right -1px #00763C;" ng-click="savefilters(1)">Update All</button>
                                        </div>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <div id="savechange">
                    <div id="savechange1" class="modal">

                            <div class="pop_map_my_part">
                               
                                 <div class="pop_container" style="padding: 13px;">
                                        <div class="heading_pop_main">
                                            <h4>
                                                Confirm
                                            </h4>
                                             <!-- <span class="close3" style="padding-top: 2%;">&times;</span> -->
                                        </div>
                                      

                                        <div class="channel_main" style="text-align: center;font-family: varela round;margin: 6px 0 27px 0;">
                                            You have unsaved changes. Do you want to save them?
                                        </div>
                                        <div class="button_center padding_bottom_15">
                                        <button type="submit" class="button_add" style="padding: 6px 13px 6px 10px;background: no-repeat right -1px #00763C;" ng-click="submititems()">Yes</button>
                                        <button type="submit" class="button_add" style="padding: 6px 13px 6px 10px;background: no-repeat right -1px #00763C;" ng-click="savechangeclose()">No</button>
                                        </div>
                                </div>  
                            </div>
                        </div>
                    </div>

   
    
    <?php $this->load->view('footer.php'); ?> 
     <div id="Pnopopup" class="modal">
    <div class="pop_map_my_part" style="margin:5% auto;width:75%;">          
        <div class="pop_container">
            <div class="heading_pop_main">
                <h4>API Search Results -</h4>
                <h4>{{currentitemid}}</h4>
                <span class="closecrawler" id="close2" ng-click="exclude_check()">&times;</span>
                <span></span>
                <span><img src="{{pictureurl}}" style="max-width: 35px; border: 1px solid #c7e9ff; vertical-align: middle;"></span>
            </div>
            <div class ="pop_pad_all_add_new" style="color: #666;">
                <!-- <table cellspacing="0" border="1" class="table api_search_result" style="width:30%;">
                    <thead>
                        <th>My ItemID</th>
                        <th>Images</th>
                    </thead>
                    <tr>
                        <td width="50%">{{currentitemid}}</td>
                        <td style="text-align: center;"><img src="{{pictureurl}}" style="max-width: 60px;"></td>
                    </tr>
                </table> -->
               <!--  <span style="font-weight: bold;">Image : <img src="{{pictureurl}}" style="max-width: 60px;"></span>
                <span style="color:#f44336">{{currentkeyword}}</span> -->

            <div id="tabs">
                <ul>
                    <li class="tabonebutton"><a href="#tabs-1">API Search</a></li>
                    <li class="tabtwobutton"><a href="#tabs-2">Show Exclude</a></li>
                </ul>
                 <div id="tabs-1">
                <div class="plan_list_view">
                 <table ng-if="comp_analysis_map !=''" cellspacing="0" border="1" class="table price_smart_grid_new api_search_table" style="margin-bottom: 10px;">
                    <thead>
                        <tr>
                            <th>sel</th>
                            <th>Image</th>
                            <th>Seller</th>
                            <th>Title</th>
                            <th class="curser_pt" ng-click="sortBy4('CurrentPrice')">Current Price</th>
                            <th class="curser_pt" ng-click="sortBy4('ShippingPrice')">Shipping Price</th>
                            <th class="curser_pt" ng-click="sortBy4('TotalPrice')">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat ="data in comp_analysis_map | orderBy:propertyName4:reverse4">
                            <td><input type="checkbox" name="api_checkbox_ta1" id="{{data.itemId}}" ng-checked="data.flag == 1" ng-disabled="data.flag == 1"></td>
                            <td width="50"><img src="<?php echo base_url().'assets/';?>/img/No_Image_Available.gif" style="max-width: 60px;"></td>
                            <td ng-if="data.MultiShipFlag == 1">{{data.sellerUserName}}
                                <div class="out-of-stock">Multi-Ship</div>
                            </td>
                            <td ng-if="data.MultiShipFlag != 1">{{data.sellerUserName}}</td>
                            <td><a ng-click="redirectToLink($event)" element="{{data.itemId}}" style="cursor: pointer;">{{data.title}}</a></td>
                            <td>{{data.CurrentPrice}}</td>
                            <td>{{data.ShippingPrice}}</td>
                            <td>{{data.TotalPrice}}</td>
                        </tr>
                    </tbody>
                </table>
               <table cellspacing="0" border="1" class="table price_smart_grid_new api_search_table">
                    <thead>
                        <tr>
                            <th>sel</th>
                            <th>Image</th>
                            <th>Seller</th>
                            <th>Title</th>
                            <th class="curser_pt" ng-click="sortBy2('CurrentPrice')">Current Price</th>
                            <th class="curser_pt" ng-click="sortBy2('ShippingPrice')">Shipping Price</th>
                            <th class="curser_pt" ng-click="sortBy2('TotalPrice')">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat ="data in crawlurl | orderBy:propertyName2:reverse2">
                             <td><input type="checkbox" name="api_checkbox" element="{{data.itemId}}" id="{{data.itemId}}" ng-checked="data.excludeflag == 1" ng-model="api_checkbox[data.itemId]" ng-click="Api_checked($event)"></td>
                            <td width="50"><img src="{{data.galleryURL}}" style="max-width: 60px;"></td>
                            <td ng-if="data.MultiShipFlag == 1">{{data.sellerUserName}}
                                <div class="out-of-stock">Multi-Ship</div>
                            </td>
                            <td ng-if="data.MultiShipFlag != 1">{{data.sellerUserName}}</td>
                            <td><a ng-click="redirectToLink($event)" element="{{data.itemId}}" style="cursor: pointer;">{{data.title}}</a></td>
                            <td>{{data.CurrentPrice}}</td>
                            <td ng-if="data.MultiShipFlag == 1">TBD</td>
                            <td ng-if="data.MultiShipFlag != 1">{{data.ShippingPrice}}</td>
                            <td>{{data.TotalPrice}}</td>
                        </tr>
                    </tbody>
                </table>
                 <div id="exc_msg" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #c31d15;color: white; border-radius:5px;width: 100%;">Excluded Successfully</p></center>
                    </div>
                <div align="center" style="margin-top:10px;">
                    <span><button type="submit" id="" style="padding: 7px 24px 5px 6px;"  class="button_Exclude" ng-click="Api_save_exclude()">Exclude Items</button></span>
                </div> 
            </div>  
            </div> 
             <div id="tabs-2" style="display: none;">
                <div class="plan_list_view">
               <table cellspacing="0" border="1" class="table price_smart_grid_new api_search_table">
                    <thead>
                        <tr>
                            <th>sel</th>
                            <th>Image</th>
                            <th>Seller</th>
                            <th>Title</th>
                            <th class="curser_pt" ng-click="sortBy3('CurrentPrice')">Current Price</th>
                            <th class="curser_pt" ng-click="sortBy3('ShippingPrice')">Shipping Price</th>
                            <th class="curser_pt" ng-click="sortBy3('TotalPrice')">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat ="data in crawlurl_exclude | orderBy:propertyName3:reverse3">
                             <td><input type="checkbox" name="api_checkbox_tab2" element="{{data.itemId}}" id="{{data.itemId}}" ng-checked="data.excludeflag == 1" ng-model="api_checkbox_tab2[data.itemId]" ng-click="Api_checked_tab2($event)"></td>
                            <td width="50"><img src="{{data.galleryURL}}" style="max-width: 60px;"></td>
                            <td ng-if="data.MultiShipFlag == 1">{{data.sellerUserName}}
                                <div class="out-of-stock">Multi-Ship</div>
                            </td>
                            <td ng-if="data.MultiShipFlag != 1">{{data.sellerUserName}}</td>
                            <td><a ng-click="redirectToLink($event)" element="{{data.itemId}}" style="cursor: pointer;">{{data.title}}</a></td>
                            <td>{{data.CurrentPrice}}</td>
                            <td ng-if="data.MultiShipFlag == 1">TBD</td>
                            <td ng-if="data.MultiShipFlag != 1">{{data.ShippingPrice}}</td>
                            <td>{{data.TotalPrice}}</td>
                        </tr>
                    </tbody>
                </table>
                 <div id="exc_msg_tab2" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #1f6f1d;color: white; border-radius:5px;width: 100%;">Saved Successfully</p></center>
                    </div>
                <div align="center" style="margin-top:10px;">
                    <span><button type="submit" id="" style="padding: 7px 24px 5px 6px;"  class="button_add" ng-click="Api_save_exclude_tab2()">Include Items</button></span>
                </div> 
            </div>  
            </div>        
        </div>
    </div>
</div>




    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
<!-- <script>
$('#testbtn').click(function(e){    
      if ($('#andinput').val() == "" || $('#orinput').val() == "") {
                 $("#msg_id").css("display","block");
                 var and_input=document.getElementById('andinput').value;
                   if(and_input == ""){
                        document.getElementById('andinput').style.borderColor = "red";
                        return false;
                    }
                    else
                    {
                       document.getElementById('andinput').style.borderColor = "";
                    }

                  var or_input=document.getElementById('orinput').value;
                   if(or_input == ""){
                        document.getElementById('orinput').style.borderColor = "red";
                        return false;
                    }  
                    e.preventDefault();
                  
     
            }
            else
            {   
                $("#msg_id").css("display","none");
                document.getElementById('andinput').style.borderColor = "";
                document.getElementById('orinput').style.borderColor = "";   
            }
        });


</script> -->
<script type="text/javascript">
    $( document ).ready(function() {
         console.log( "ready!" );
        $("#Skuinput").css("display","none");
        $("#buttoninput").css("display","none");
        $("#emsg_id").css("display","none");
});
</script>
    <script>

        var sortingOrder = 'sku';
        var mainresultvalue =[];
        var app = angular.module('myApp',['ngCsv']);
        app.controller('myController',function($scope,$http,$filter,$attrs,$window,$attrs){
        $scope.sortingOrder = sortingOrder;
        $scope.reverse = false;
        $scope.filteredItems = [];
        $scope.groupedItems = [];
        $scope.itemsPerPage = 25;
        $scope.pagedItems = [];
        $scope.pagenos = [];
        $scope.catresult = '';
        $scope.currentPage = 0;
        $scope.maincompetitorvalue =[];
        $scope.resultcomplabel = [];
        $scope.api_checkbox    = [];
        $scope.api_checkbox_tab2   = [];
        $scope.final_data = [];
        $scope.final_data_tab2 = [];
        $scope.totalitems ='';
        $scope.count = 0;
        $scope.AppID =''

            $http.get("<?php echo base_url();?>index.php/ConfigIBR/getstorename")
                           .success(function (response) {
                            $scope.isLoading =false;
                               $scope.Storename = response;
                               $scope.Storename.push({CompID: "00", Sellername: "Show All Store"});
                              })

           $scope.changemade = function() {
           $scope.count++;
           };
            //redirect to ebay
              $scope.redirectToLink = function (obj) {
                        var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                        $window.open(url, '_blank','width=1200,height=600"');
                    }; 

            // search keyword charatcer length
            $scope.keywrd_length_check =function(){
                var k_or = $scope.keywordsor.replace(/\s/g,',%20');
                var k_and = $scope.keywordsand;
                var k_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                if($scope.title_inc == true){
                    var incl_title = $scope.currenttitle.replace(/\s/g,',%20');
                     incl_title ='('+incl_title+')';
                }
                else{
                    var incl_title = '';
                }
                var keyword_overall= '('+k_or+')'+k_and+'%20'+incl_title+'%20-'+k_more.replace(/\s/g,'%20');
                  console.log(keyword_overall);
                $scope.keywrd_length = 300 - (keyword_overall.length);
                console.log($scope.keywrd_length);
                //parseFloat
            }


            $scope.crawldata = 'data';
            $scope.crawlersearch = function(crawlerbutton){
                $scope.currentguid = '';
                $http({                     
                     method  : 'POST',
                     url     : '<?php echo base_url();?>index.php/ConfigIBR/getGUID',
                     data    : '',
                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                })
                .success(function(response) {
                    $scope.currentguid = response.replace('\r\n','');
                //var sendgui = Math.floor(new Date().valueOf() / 1000);
                //var sendgui = $('#api_hidden_id').val();
                var sendgui = $scope.currentguid;
                $scope.keyword_crawler = '';               
                $scope.key_exclude_crawler = '';
                $scope.categryid_crawler ='';
                $scope.highprice_crawler ='';
                $scope.condition_crawler = '';
                $scope.freeshipping_crawler ='';
                $scope.location_crawler ='';
                $scope.competitor_crawler ='';
                $scope.sortorder_crawler = '';
                $scope.Sellers_Include_exclude ='';
                $scope.excluderesultebay ='';
                $scope.includeresultebay ='';
                var includeebay = '';
                var excludeebay = '';
                var my_item_id = $scope.currentitemid;
                $k =0;
                $s =0;
                     
                 if($scope.title_inc == true){
                    $scope.include_title = $scope.currenttitle.replace(/\s/g,',%20');
                    $scope.include_title ='('+$scope.include_title+')';
                   }
                   else{
                    $scope.include_title = '';
                   }
                 //keywords condition
                // or,exclude | and,exclude | or | and 
                 if($scope.keywordsor !='' && $scope.keywordsand !='' && $scope.keywordsmore !=''){
                    $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_or_more = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawl_and = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = '('+$scope.keyword_or_more+')'+$scope.keyword_crawl_and+'%20'+$scope.include_title+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');
                }
                else if($scope.keywordsor !='' && $scope.keywordsand !=''){
                     $scope.keyword_or_more = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawl_and = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = '('+$scope.keyword_or_more+')'+$scope.keyword_crawl_and+'%20'+$scope.include_title;
                }
                else if($scope.keywordsor !='' && $scope.keywordsmore !=''){
                    $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_or_more = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawler = '('+$scope.keyword_or_more+')'+'%20'+$scope.include_title+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');
                }
                 else if($scope.keywordsand !='' && $scope.keywordsmore !=''){
                    $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_and_more = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = $scope.keyword_and_more+'%20'+$scope.include_title+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');
                }
                else if($scope.keywordsor !=''){
                    $scope.keyword_crawl_or = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawler = '('+$scope.keyword_crawl_or+')'+'%20'+$scope.include_title;
                }
                 else if($scope.keywordsand !=''){
                    $scope.keyword_crawl_and = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = $scope.keyword_crawl_and+'%20'+$scope.include_title;
                }
                /*if($scope.keywordsor !='' && $scope.keywordsmore !=''){
                    $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_or_more = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawler = '('+$scope.keyword_or_more+')'+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');

                }
                 else if($scope.keywordsand !='' && $scope.keywordsmore !=''){
                    $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_and_more = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = $scope.keyword_and_more+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');
                }
                else if($scope.keywordsor !=''){
                    $scope.keyword_crawl_or = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawler = '('+$scope.keyword_crawl_or+')';
                }
                 else if($scope.keywordsand !=''){
                    $scope.keyword_crawl_and = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = $scope.keyword_crawl_and;
                }*/
                console.log($scope.keyword_crawler);

                 
                //console.log($scope.condition_crawler);
                  //both price
                 if($scope.minus !=''&& $scope.plus !=''){
                    
                    $scope.highprice_crawler = '&itemFilter('+$k+').name=MinPrice&itemFilter('+$k+').value='+$scope.minus+'&itemFilter('+$k+').paramName=Currency&itemFilter('+$k+').paramValue=USD&itemFilter('+(++$k)+').name=MaxPrice&itemFilter('+$k+').value='+$scope.plus+'&itemFilter('+$k+').paramName=Currency&itemFilter('+$k+').paramValue=USD';
                    $s++;
                }
                 //low price
                if($scope.minus !=''&& $scope.plus ==''){
                    $scope.highprice_crawler = '&itemFilter.name=MinPrice&itemFilter.value='+$scope.minus+'&itemFilter.paramName=Currency&itemFilter.paramValue=USD';
                }


                //high price
                if($scope.plus !='' && $scope.minus ==''){
                   $scope.highprice_crawler = '&itemFilter.name=MaxPrice&itemFilter.value='+$scope.plus+'&itemFilter.paramName=Currency&itemFilter.paramValue=USD';
                }

                 //free shipping
                if($scope.freeshiping == undefined ){
                    $scope.freeshipping_crawler ='' ;
                }
                else if($scope.freeshiping !=''){
                    $scope.freeshipping_crawler = '&itemFilter.name=FreeShippingOnly&itemFilter.value=true';
                }

                 //sort order
                if($scope.resultsortordervalue !=''){
                    var sort_ord_val =  {"12":"BestMatch","1":"EndTimeSoonest","10":"StartTimeNewest",
                                            "15":"PricePlusShippingLowest","16":"PricePlusShippingHighest","7":"DistanceNearest"};
                      var sotorder  = sort_ord_val[$scope.resultsortordervalue];                
                      //console.log(sotorder);
                     $scope.sortorder_crawler = '&sortOrder='+sotorder;
                }
                //condition
                if($scope.condition == 0){
                    $scope.condition_crawler = '';
                }
                else if($scope.condition !=''){
                    $scope.condition_crawler ="";
                    if($s>0){
                           if($scope.condition == 3) $scope.condition_crawler = '&itemFilter('+(++$k)+').name=Condition&itemFilter('+($k)+').value='+1000;
                              else if($scope.condition == 4) $scope.condition_crawler = '&itemFilter('+(++$k)+').name=Condition&itemFilter('+($k)+').value='+3000;
                                   else if($scope.condition == 10) $scope.condition_crawler = '&itemFilter('+(++$k)+').name=Condition&itemFilter('+($k)+').value='+'';
                    $s++;
                    }
                    else{
                         if($scope.condition == 3) $scope.condition_crawler = '&itemFilter('+$k+').name=Condition&itemFilter('+$k+').value='+1000;
                             else if($scope.condition == 4) $scope.condition_crawler = '&itemFilter('+$k+').name=Condition&itemFilter('+$k+').value='+3000;
                                  else if($scope.condition == 10) $scope.condition_crawler = '&itemFilter('+$k+').name=Condition&itemFilter('+$k+').value='+'';
                    $s++;
                    }
                    
                }

                 //category include only   
                if($scope.resultcatlabel !=''){
                    var resultcategoryinclude = [];
                    var resultcategoryexclude = [];
                    var resultcatforebay = [];
                    for(val in $scope.resultcatlabel){
                        var sortvalue = $scope.resultcatlabel[val].name;
                        if(sortvalue[0] == '!' && sortvalue[1] == '='){
                            resultcategoryexclude.push(val);     
                        }
                        else{
                            resultcategoryinclude.push(val);
                        }
                    }
                   
                    //include category
                    if(resultcategoryinclude!=''){
                        if($s>0){
                        var include_mainurl  = '&itemFilter('+(++$k)+').name=Category%20';}
                        else{
                        var include_mainurl  = '&itemFilter('+$k+').name=Category%20'; }

                        //var include_mainurl = '&itemFilter(0).name=Category%20';
                        $j = 0;
                        for(key in resultcategoryinclude){
                            include_mainurl += '&itemFilter('+$k+').value('+$j+')='+resultcategoryinclude[key];
                            $j++;
                        }
                        $s++;
                     }
                    //exclude catergory
                    if(resultcategoryexclude!=''){
                        
                        if($s>0) {
                        var exclude_mainurl = '&itemFilter('+(++$k)+').name=ExcludeCategory%20';}
                        else {
                        var exclude_mainurl = '&itemFilter('+$k+').name=ExcludeCategory%20';}

                            $i = 0;
                            for(key in resultcategoryexclude){
                                exclude_mainurl += '&itemFilter('+$k+').value('+$i+')='+resultcategoryexclude[key];
                                $i++;
                            }
                            $s++;
                     }
                    if(resultcategoryexclude.length == 0){exclude_mainurl = '';}
                    if(resultcategoryinclude.length == 0){include_mainurl = '';}
                }

                 //competitor include and exclude
                 if($scope.resultcomplabel !=''){
                    var includeebay = [];
                    var excludeebay = [];
                    for(val in $scope.resultcomplabel){
                        var keysplitvalue = $scope.resultcomplabel[val].key;
                        if(keysplitvalue[0] == '!' && keysplitvalue[1] == '='){
                            excludeebay.push($scope.resultcomplabel[val].key_id);
                        }
                        else{
                            includeebay.push($scope.resultcomplabel[val].key_id);   
                        }
                    }
                    //var exclude_comp_mainurl = '&itemFilter(0).name=ExcludeSeller';
                    //var include_comp_mainurl = '&itemFilter(0).name=Seller%20';

                   
                    //include seller        
                    if(includeebay!= ''){
                             if($s>0){
                                var include_comp_mainurl = '&itemFilter('+(++$k)+').name=Seller%20';}
                             else{
                                var include_comp_mainurl = '&itemFilter('+$k+').name=Seller%20';}
                             $j = 0;
                                for(key in includeebay){
                                    include_comp_mainurl += '&itemFilter('+$k+').value('+$j+')='+includeebay[key];
                                    $j++;
                                }
                                $s++;
                        } 
                  //exclude seller
                    if(excludeebay!= ''){
                        if($s>0) {
                        var exclude_comp_mainurl = '&itemFilter('+(++$k)+').name=ExcludeSeller';}
                        else{
                            var exclude_comp_mainurl = '&itemFilter('+$k+').name=ExcludeSeller';}
                        $i = 0;
                            for(key in excludeebay){
                                exclude_comp_mainurl += '&itemFilter('+$k+').value('+$i+')='+excludeebay[key];
                                $i++;
                            }
                            $s++;
                     }
                }

                //location
                if($scope.location == undefined){
                    $scope.location_crawler = '';
                }
                else if($scope.location != ''){
                    ++$k;
                    $scope.location_crawler = '&itemFilter('+$k+').name=LocatedIn&itemFilter('+$k+').value=US';
                }

                if(exclude_comp_mainurl == '' || exclude_comp_mainurl == undefined || excludeebay.length == 0){exclude_comp_mainurl = '';}
                if(include_comp_mainurl == '' || include_comp_mainurl == undefined || includeebay.length == 0){include_comp_mainurl = '';}

               $scope.crawlerurl = 'http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME='+$scope.AppID+'&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&outputSelector(0)=SellerInfo&outputSelector(1)=StoreInfo&paginationInput.pageNumber='+$scope.per_page_num+'&paginationInput.entriesPerPage='+$scope.per_page_data+'&keywords='+$scope.keyword_crawler+$scope.highprice_crawler+$scope.condition_crawler+$scope.freeshipping_crawler+include_mainurl+exclude_mainurl+exclude_comp_mainurl+include_comp_mainurl+$scope.location_crawler+$scope.sortorder_crawler;
               $scope.crawlerurl = $scope.crawlerurl.replace(/  | /g,'');
                   console.log($scope.crawlerurl); 
                    
               //if($scope.crawldata == 'data'){
                 if(crawlerbutton == undefined){
                    $scope.isLoading =true;
                    $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/ConfigIBR/crawler_url_search',
                         data    : {'url' : $scope.crawlerurl,'myitemid':my_item_id,
                                    'guid':sendgui,'handlingtime':$scope.handling,'toprated':$scope.TopRated},
                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                            $scope.crawlurl = response.Non_Exclude_res;
                            $scope.crawlurl_exclude = response.Exclude_res;
                             $scope.comp_analysis_map = response.comp_analysis_mapped_items;
                            //console.log(response);
                             $scope.crawlurl.forEach(function(part){
                                 if(part.excludeflag == 1){
                                    $scope.api_checkbox[part.itemId] = true;
                                }  
                             })
                              $scope.crawlurl_exclude.forEach(function(part){
                                 if(part.excludeflag == 1){
                                    $scope.api_checkbox_tab2[part.itemId] = true;
                                }                               
                             })
                           
                            $scope.isLoading = false;
                            $('#Pnopopup').show();
                            $scope.propertyName2 = 'CurrentPrice';
                            $scope.reverse2 = true;
                             $scope.propertyName3 = 'CurrentPrice';
                            $scope.reverse3 = true;

                          $scope.sortBy2 = function(propertyName2) {
                            $scope.reverse2 = ($scope.propertyName2 === propertyName2) ? !$scope.reverse2 : false;
                            $scope.propertyName2 = propertyName2;
                          };
                          $scope.sortBy3 = function(propertyName3) {
                            $scope.reverse3 = ($scope.propertyName3 === propertyName3) ? !$scope.reverse3 : false;
                            $scope.propertyName3 = propertyName3;
                          };
                          $scope.sortBy4 = function(propertyName4) {
                            $scope.reverse4 = ($scope.propertyName4 === propertyName4) ? !$scope.reverse4 : false;
                            $scope.propertyName4 = propertyName4;
                          };
                          });
                  }
                });
               
              
                //$window.open($scope.crawlerurl, '_blank','width=1200,height=600"');


            }
            $scope.selected =function(data){
                if (data == 1){
                    $scope.pagedItems ='';
                    $scope.currentPage =0;
                    $scope.skuinput ='';
                     $("#popupcloseevent").css("display","block");
                     $("#Skuinput").css("display","none");
                     $("#buttoninput").css("display","none");
                     $("#emsg_id").css("display","none");
                     $("#selectstorename").css("display","none");
                }
                else if(data ==2){
                    if($scope.categorycomboload == undefined){
                     $("#selectstorename").css("display","block");
                    }
                     $scope.pagedItems ='';
                      $scope.currentPage = 0;
                       $scope.categorylist ='';
                    $("#popupcloseevent").css("display","none");
                    $("#Skuinput").css("display","block");
                    $("#buttoninput").css("display","block");
                    $("#emsg_id").css("display","none");
               }
               else if(data==3){
                    $scope.pagedItems ='';
                    $scope.currentPage ='';
                    $scope.skuinput ='';
                    $scope.categorylist ='';
                    $("#popupcloseevent").css("display","none");
                    $("#Skuinput").css("display","none");
                    $("#buttoninput").css("display","none");
                    $("#emsg_id").css("display","none");
                    $("#selectstorename").css("display","none");
                    //console.log($scope.categorylist.Categoryid);
                     if($scope.StoreName.CompID !='' && $scope.StoreName.CompID != undefined){
                     $scope.isLoading =true;
                            $http({
                                    method: 'POST',
                                    data    : {'CompID' : $scope.StoreName.CompID},
                                    url     : '<?php echo base_url();?>index.php/configIBR/unprocessedcombo_result',
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                  })
                       .success(function (data) {
                        $scope.isLoading =false;
                        $scope.populate(data);
                     })
                   }

               } 
            }

            $scope.testsearch = function(savebutton){ 
                //$scope.keywordsor = '';
                //$scope.keywordsand = '';
                $scope.keyword_test = '';               
                $scope.AND_OR_id ='';
                $scope.key_exclude_test = '';
                $scope.categryid_test ='';
                $scope.lowprice_test = '';
                $scope.highprice_test ='';
                $scope.condition_test = '';
                $scope.freeshipping_test ='';
                $scope.location_test ='';
                $scope.competitor_test ='';
                $scope.sortorder_test = '';
                $scope.Sellers_Include_exclude ='';
                $scope.excluderesultebay ='';
                $scope.includeresultebay ='';
               
               if($scope.title_inc == true){
                $scope.include_title = $scope.currenttitle.replace(/\s/g,',%20');
                $scope.include_title ='('+$scope.include_title+')';
               }
               else{
                $scope.include_title = '';
               }
               //console.log($scope.include_title);
                //keywords condition
                if($scope.keywordsor !='' && $scope.keywordsand !=''){
                    $scope.key_OR = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_test = '('+$scope.key_OR+')'+$scope.keywordsand.replace(/\s/g, '+')+'%20'+$scope.include_title;
                }
                else if($scope.keywordsor !='' ){
                    $scope.key_OR = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_test = '('+$scope.key_OR+')'+'%20'+$scope.include_title;
                }
                else if($scope.keywordsand !=''){
                     $scope.keyword_test = $scope.keywordsand.replace(/\s/g, '+')+'%20'+$scope.include_title;
                }
               /* if($scope.keywordsor !=''){
                    $scope.key_OR = $scope.keywordsor;
                    $scope.keyword_test = $scope.key_OR.replace(/\s/g, '+');
                    $scope.AND_OR_id =2;
                }
                 else if($scope.keywordsand !=''){
                    $scope.key_AND = $scope.keywordsand;
                    $scope.keyword_test = $scope.key_AND.replace(/\s/g, '+');
                    $scope.AND_OR_id =1;
                }*/
               
               /*  if($scope.keywordsand !='' || $scope.keywordsor !=''){
                    $scope.key_AND = $scope.keywordsand+' '+$scope.keywordsor;
                    $scope.keyword_test = $scope.key_AND.replace(/\s/g, '+');
                    $scope.AND_OR_id =4;
                }*/

                //keywords condition exclude
                if($scope.keywordsmore !=''){
                    $scope.keyword_more_test = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' ');
                     $scope.key_exclude_test = $scope.keyword_more_test;
                } 

                //category include only
                if($scope.resultcatlabel !=''){
                    var resultcatforebay = [];
                    for(val in $scope.resultcatlabel){
                        var sortvalue = $scope.resultcatlabel[val].name;
                        if(sortvalue[0] != '!' && sortvalue[1] != '='){
                            resultcatforebay.push(val);   
                        }
                    }
                    $scope.categryid_test = resultcatforebay.toString();
                }

                //low price
                if($scope.minus !=''){
                    $scope.lowprice_test = $scope.minus;
                }

                //high price
                if($scope.plus !=''){
                     $scope.highprice_test = $scope.plus;
                }

                //condition
                if($scope.condition == 0){
                    $scope.condition_test = '';
                }
                else if($scope.condition !=''){
                     $scope.condition_test = $scope.condition;
                }
                
               // console.log($scope.condition+'ebay');
                //free shipping
                if($scope.freeshiping == undefined ){
                    $scope.freeshipping_test = 0;
                }
                else if($scope.freeshiping !=''){
                    $scope.freeshipping_test = 1;
                }

                //console.log($scope.location);
                //location
                if($scope.location == undefined){
                      $scope.location_test = 0;
                }
                else if($scope.location != ''){
                      $scope.location_test = 1;
                }
                //console.log($scope.location_test);

                //competitor include and exclude
                 if($scope.resultcomplabel !=''){
                    var includeebay = [];
                    var excludeebay = [];
                    for(val in $scope.resultcomplabel){
                        var keysplitvalue = $scope.resultcomplabel[val].key;
                        if(keysplitvalue[0] == '!' && keysplitvalue[1] == '='){
                            excludeebay.push($scope.resultcomplabel[val].key_id);
                        }
                        else{
                            includeebay.push($scope.resultcomplabel[val].key_id);   
                        }
                    }
                    $scope.includeresultebay = includeebay.toString();
                    $scope.excluderesultebay = excludeebay.toString();
                }

                if($scope.includeresultebay != ''){
                    $scope.Sellers_Include_exclude ='&_saslop=1&_sasl='+$scope.includeresultebay; 
                }
                else if($scope.excluderesultebay != ''){
                    $scope.Sellers_Include_exclude ='&_saslop=2&_sasl='+$scope.excluderesultebay; 
                }


                //sort order
                if($scope.resultsortordervalue !=''){
                     $scope.sortorder_test = $scope.resultsortordervalue;
                }

                //url formation
                $scope.url = 'https://www.ebay.com/sch/i.html?_nkw='+$scope.keyword_test+'&_ex_kw='+$scope.key_exclude_test+'&_udlo='+$scope.lowprice_test+'&_udhi='+$scope.highprice_test+'&LH_ItemCondition='+$scope.condition_test+'&&LH_PrefLoc='+$scope.location_test+'&LH_FS='+$scope.freeshipping_test+'&_sop='+$scope.sortorder_test+'&_sacat='+$scope.categryid_test+$scope.Sellers_Include_exclude;
               /* $scope.url = 'https://www.ebay.com/sch/i.html?_nkw='+$scope.keyword_test+'&_in_kw='+$scope.AND_OR_id+'&_ex_kw='+$scope.key_exclude_test+'&_udlo='+$scope.lowprice_test+'&_udhi='+$scope.highprice_test+'&LH_ItemCondition='+$scope.condition_test+'&&LH_PrefLoc='+$scope.location_test+'&LH_FS='+$scope.freeshipping_test+'&_sop='+$scope.sortorder_test+'&_sacat='+$scope.categryid_test+$scope.Sellers_Include_exclude;*/
              console.log($scope.url);
                if(savebutton == ''){    
                    $window.open($scope.url, '_blank','width=1200,height=600"');
                }
            }
            $scope.upload = function(){
              document.getElementById('my_file').click();
                    };
              $scope.get_sku_result =function(sku){
                 if(sku == undefined){
                     $("#selectsku").css("display","block");
                    }
                console.log(sku);
                 
                  if($scope.StoreName.CompID !='' && $scope.StoreName.CompID != undefined && sku!='' && sku!=undefined){
                    $("#selectsku").css("display","none");
                    $scope.isLoading =true;
                        $http({
                                method: 'POST',
                                url     : '<?php echo base_url();?>index.php/ConfigIBR/skucombo_result',
                                data    : {'SKU' : sku,'CompID' : $scope.StoreName.CompID},
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                              })
                        .success(function (data) {
                            $scope.isLoading = false;
                            $scope.populate(data);
                       });
                   }
             }
             $scope.get_changeditems_result =function(){
                console.log('working');
                $scope.isLoading =true;
                        $http({
                                method: 'POST',
                                url     : '<?php echo base_url();?>index.php/ConfigIBR/changeditems_result',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                              })
                  
                        .success(function (data) {
                            $scope.isLoading = false;
                            $scope.populate(data);
                    });
             }
         
         

            $scope.get_categoryforstore =function(){
                $("#selectstorename").css("display","none");
                console.log($scope.StoreName.CompID);
                $scope.categorylist= '';
                $scope.pagedItems ='';
                $scope.currentPage =0;
                $scope.skuinput ='';
                //$scope.isLoading =true;
                //return false;
                if($scope.StoreName.CompID !='' && $scope.StoreName.CompID != undefined){
                    $scope.isLoading = true;
                        $http({
                                method: 'POST',
                                url     : '<?php echo base_url();?>index.php/ConfigIBR/getcategory',
                                data    : {'CompID' : $scope.StoreName.CompID},
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                              })
                   .success(function (response) {
                    $scope.isLoading =false;
                     $scope.categorycomboload = response;
                     console.log($scope.categorycomboload);
                 })
               }
            }
            $scope.get_category_result =function(){
                console.log($scope.categorylist.Categoryid);
                if($scope.categorylist.Categoryid != '' && $scope.StoreName.CompID !='' && $scope.StoreName.CompID != undefined ){
                     $scope.isLoading =true;
                            $http({
                                    method: 'POST',
                                    url     : '<?php echo base_url();?>index.php/ConfigIBR/categorycombo_result',
                                    data    : {'category' : $scope.categorylist.Categoryid,'CompID' : $scope.StoreName.CompID},
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                  })
                       .success(function (data) {
                        $scope.isLoading =false;
                        $scope.populate(data);
                     })
               }
            }
             $scope.populate =function(data){
                    $scope.catresult = data;
                    $scope.totalitems =  $scope.catresult.length;
                    console.log(data);
                    if($scope.catresult == '')
                    {
                        $("#emsg_id").css("display","block");
                        return false;
                    }
                    //return false;
                    //$scope.def_cat_id = data[0].categoryid;
                    $scope.pictureurl = data[0].pictureurl;

                    //export excel
                    $scope.parts =$scope.catresult;  
                    $scope.parts.forEach(function (part) {
                        part.flag='N';
                    });


                    var exportdata=[];
                    var exportdata_exc = [];

                    exportdata.push ({'ItemID' : 'ItemID#','Keywords_AND' : 'Keywords_AND','Keywords_OR' : 'Keywords_OR','Exclude_keywords' : 'Exclude_keywords','Category_Include' : 'Category_Include','Category_Exclude' : 'Category_Exclude','my_price' : 'my_price','Percent' : 'Percent','Priceform_from' : 'Priceform_from','Priceform_to' : 'Priceform_to','Condition' : 'Condition','Shipping' : 'Shipping','Location' : 'Location','Sellers_Include' : 'Sellers_Include','Sellers_Exclude' : 'Sellers_Exclude','Sort_order' : 'Sort_order'});       

                    $scope.parts.forEach(function (part) {
                        exportdata.push({'ItemID' : part.itemid,'Keywords_AND' : part.Keywords_AND,'Keywords_OR' : part.Keywords_OR,'Exclude_keywords' : part.Exclude_keywords,'Category_Include' : part.Category_Include,'Category_Exclude' : part.Category_Exclude,'my_price':part.my_price,'Percent' : part.Percent,'Priceform_from' : part.Priceform_from,'Priceform_to' : part.Priceform_to,'Conditions' : part.Conditions,'Shipping' : part.Shipping,'Location' : part.Location,'Sellers_Include' : part.Sellers_Include,'Sellers_Exclude' : part.Sellers_Exclude,'Sort_order' : part.Sort_order});
                        
                        exportdata_exc.push({'ItemID' : part.itemid,'Keywords_AND' : part.Keywords_AND,'Keywords_OR' : part.Keywords_OR,'Exclude_keywords' : part.Exclude_keywords,'Category_Include' : part.Category_Include,'Category_Exclude' : part.Category_Exclude,'my_price':part.my_price,'Percent' : part.Percent,'Priceform_from' : part.Priceform_from,'Priceform_to' : part.Priceform_to,'Conditions' : part.Conditions,'Shipping' : part.Shipping,'Location' : part.Location,'Sellers_Include' : part.Sellers_Include,'Sellers_Exclude' : part.Sellers_Exclude,'Sort_order' : part.Sort_order});
                    });
                       $scope.exportdata_excsheettwo = [
                                                    {'ItemID':'format:number|Decimal:0|positiveNumber','Keywords_AND':'if multiple seperated by |','Keywords_OR':'if multiple seperated by |','Exclude_keywords':'if multiple seperated by |',' Category_Include':' must be category id &&if multiple seperated by |',' Category_Exclude':' must be category id &&if multiple seperated by |', 'Percent':'', 'my_price':'','Condition' : 'New','Shipping' : 'TRUE/FALSE','Location' : 'us','Sellers_Include':'if multiple seperated by |', 'Sellers_Exclude':'if multiple seperated by |','Sort Order' : 'Best Match - 12'},
                                                    {'ItemID':'','Keywords_AND':'','Condition' : 'Used','Sort Order' : 'Time: ending soonest - 1','FreeShipping' : '','Location' : ''},
                                                    {'ItemID':'','Keywords_AND':'','Condition' : 'Not Specified','Sort Order' : 'Time: newly listed - 10','FreeShipping' : '','Location' : ''},
                                                    {'ItemID':'','Keywords_AND':'','Condition' : '','Sort Order'      : 'Price + Shipping: lowest first - 15','FreeShipping' : '','FreeShipping' : ''},
                                                    {'ItemID':'','Keywords_AND':'','Condition' : '','Sort Order' : 'Price + Shipping: highest first - 16','FreeShipping' : '','Location' : ''},
                                                    {'ItemID':'','Keywords_AND':'','Condition' : '','Sort Order' : 'Distance: nearest first - 7','FreeShipping' : '','Location' : ''}
                                                    
                                                    ];

                   $scope.ForCSVExport = exportdata;
                   $scope.ForExcelExport = exportdata_exc;
                   //export excel end
                    //console.log($scope.catresult);
                                                 //pagination part start 
                        // ********************
                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            $scope.storesearch = function () {
                                    $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                    for(var attr in item) {
                                    if(attr == "storename") {
                                        if (searchMatch(item[attr], $scope.storenamesearch)  > -1){
                                            return true;
                                        }
                                      }
                                    }
                                    return false;
                                    });
                                    // take care of the sorting order
                                    if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                    }
                                    $scope.currentPage = 0;
                                    // now group by pages
                                    $scope.groupToPages();
                                };    

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                      for(var attr in item) {
                                        if(attr == "itemid") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };


                              $scope.myskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                    for(var attr in item) {
                                    if(attr == "sku") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.Categorysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Category") {
                                            if (searchMatch(item[attr], $scope.Categoryquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             $scope.mytitlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                    for(var attr in item) {
                                     if(attr == "title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                            
                             /*$scope.filtersearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Filters") {
                                            if (searchMatch(item[attr], $scope.Filtersquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };keywordsearch
                              $scope.keywordsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.catresult, function (item) {
                                    for(var attr in item) {
                                     if(attr == "keywords") {
                                            if (searchMatch(item[attr], $scope.SearchKeywordquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };*/

                            
                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};
                    
                   
            }
               

            //suggest category combo for include  
                 $scope.setValue_main = function(index,arg){
                    $('#normal_search_inc').val(index);  
                    $('#searchResult_main').hide(); 
                    $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {}); 
                 }

                  $scope.partnosearch_main = function () {
                    if($scope.partnofirst !=''){
                        $('#searchResult_main').show();
                    }
                    else{
                         $('#searchResult_main').hide();
                    }
                      
                       /* $scope.filteredItems1 = $filter('filter')($scope.suggestcat, function (item) {
                              for(var attr in item) {
                                if(attr == "category") {
                                    if (searchMatch(item[attr], $scope.partnofirst)  > -1){
                                        return true;
                                    }
                                  }
                                }
                            return false;
                    });*/
                };

                 //import popup     
                    $scope.import = function(){     
                        //console.log('hi');      
                        $scope.message = '';        
                        $scope.errormessage = '';       
                        $("#popupform1").css("display", "block");       
                        $("#Pnopopup1").css("display", "block");        
                    };


               
            //suggest category combo for exclude
                $scope.setValue = function(index){
                    $('#normal_search_exc').val(index);  
                    $('#searchResult').hide();  
                    $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});
                 }

                  $scope.partnosearch = function () {
                    if($scope.partno !=''){
                         $('#searchResult').show();
                    }
                    else{
                         $('#searchResult').hide();
                    }
                   
                               /* $scope.filteredItems1 = $filter('filter')($scope.suggestcat, function (item) {
                                      for(var attr in item) {
                                        if(attr == "category") {
                                            if (searchMatch(item[attr], $scope.partno)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });*/
                };


            //price calculation start
                $scope.price = $scope.currentprice;
                $scope.percentchange = function(){
                    if($scope.percent){
                      $scope.pricevalue = 1;
                      var currentprice  = $scope.currentprice;
                      var min_price     = $scope.currentprice / 100 * $scope.percent;
                      $scope.minus      = (parseFloat(currentprice)-parseFloat(min_price)).toFixed(2);
                      $scope.plus       = (parseFloat(currentprice)+parseFloat(min_price)).toFixed(2);
                    }
                    else{
                        $scope.minus = '';
                        $scope.plus = '';
                        $scope.pricevalue = 0;
                    }
                };


             // category label start     
                $scope.categorylabel       = [];
                $scope.get_category     = function(catname,catid,cattype){
                    if(cattype == 'Exclude'){
                        $scope.categorylabel[catid] = '!='+catname;
                    }
                    else{
                        $scope.categorylabel[catid] = catname;
                    }
                    angular.forEach($scope.categorylabel, function(value, key){
                        mainresultvalue[key] = value;
                    });
                   /* $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});*/
                    $scope.resultlabellength = 1;
                }

            // competitor label start 
            $scope.competitorlabel  =[];
            $scope.get_competitor =function(compname,comptype){
                if((comptype == 'Include' || comptype == 'Exclude') && compname != ''){
                    if(compname.indexOf(',') > -1){
                        if(comptype == 'Include'){
                            var mycomp = compname.split(',');
                            for(mycompkey in mycomp){
                                $scope.maincompetitorvalue[mycomp[mycompkey]] = mycomp[mycompkey];
                            }
                        }
                        else{
                            var mycomp = compname.split(',');
                            for(mycompkey in mycomp){
                                $scope.maincompetitorvalue[mycomp[mycompkey]] = '!='+mycomp[mycompkey];
                            }
                        }                     
                    }
                    else{
                        if(comptype == 'Include'){
                            //console.log($scope.maincompetitorvalue);
                            $scope.maincompetitorvalue[compname] = compname; 
                        }
                        else{
                            $scope.maincompetitorvalue[compname] = '!='+compname;
                        }
                    }

                        var compresponse = [];
                        for(key in $scope.maincompetitorvalue){
                            if(key != ''){
                                compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                            }
                        }
                        $scope.resultcomplabel = compresponse;
                        $scope.resultcomplabellength = 1;
                }
            }

            $scope.eventclose = function(firstevent){

                     if(firstevent.target.attributes.datalabel.value == 'keywordsand'){
                        $scope.keywordsand = '';
                    }
                     if(firstevent.target.attributes.datalabel.value == 'keywordsor'){
                        $scope.keywordsor = '';
                    }
                    if(firstevent.target.attributes.datalabel.value == 'keywords'){
                        $scope.keywordsmore = '';
                    }
                    if(firstevent.target.attributes.datalabel.value == 'category'){
                        var index = mainresultvalue.indexOf(firstevent.target.attributes.datavalue.value);
                        //console.log(mainresultvalue);
                        if (index == -1) {
                            delete mainresultvalue[firstevent.target.attributes.datavalue.value];
                            $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});

                            if(Object.keys($scope.resultcatlabel).length == 0){
                                $scope.resultlabellength = 0;
                            }
                        }
                        //console.log(mainresultvalue);
                    }

                    if(firstevent.target.attributes.datalabel.value == 'pricelabel'){
                        $scope.percent = '';
                        $scope.minus = '';
                        $scope.plus = '';
                        $scope.pricevalue = 0;
                    }

                    if(firstevent.target.attributes.datalabel.value == 'conditionlabel'){
                        $scope.resultcondition = '';
                        $scope.resultconditionvalue = '';
                        $scope.condition = false;
                    }
                    if(firstevent.target.attributes.datalabel.value == 'freeshiping'){
                        $scope.freeshiping = '';
                    }
                    if(firstevent.target.attributes.datalabel.value == 'location'){
                        $scope.location = '';
                    }
                    if(firstevent.target.attributes.datalabel.value =='Handling'){
                        $scope.handling = '';
                    }
                    if(firstevent.target.attributes.datalabel.value =='TopRated'){
                        $scope.TopRated = '';
                    }

                    if(firstevent.target.attributes.datalabel.value == 'resultorder'){
                        $scope.resultsortorder      = '';
                        $scope.resultsortordervalue = 0;
                        $scope.resultsortorderlabel = 0;
                       // $scope.sortorder = 0;
                    }
                    if(firstevent.target.attributes.datalabel.value == 'seller'){
                        $scope.resultindex = $scope.maincompetitorvalue.indexOf(firstevent.target.attributes.datavalue.value);
                        if ($scope.resultindex == -1) {
                            delete $scope.maincompetitorvalue[firstevent.target.attributes.datavalue.value];
                            var compresponse = [];
                            for(key in $scope.maincompetitorvalue){
                                if(key != ''){
                                    compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                                }
                            }

                            $scope.resultcomplabel = compresponse;
                            //console.log($scope.maincompetitorvalue);
                            $scope.comp_inc = '';
                            $scope.comp_exc = '';
                            if(compresponse.length == 0){
                                $scope.resultcomplabellength = 1;
                            }
                        }
                    }
                    
                }


                $scope.resultconditionvalue = '';
                $scope.resultcondition = '';
                $scope.conditionchange = function(obj){
                     if(obj.target.checked){
                    $scope.resultcondition = obj.target.attributes.element.value;
                    $scope.resultconditionvalue = obj.target.attributes.value.value;
                     }      
                    else{       
                        $scope.resultcondition = '';        
                        $scope.resultconditionvalue = '';    
                    }
                }

                $scope.resultsortordervalue = '';
                $scope.sortorderchange = function(obj){
                    if($(".sortorderselect").find(':selected')){
                        $scope.resultsortorderlabel = 1;
                        $scope.resultsortorder = $(".sortorderselect").find(':selected').attr('itemvalue');
                        $scope.resultsortordervalue = $scope.sortorder;
                    }
                    else{
                        $scope.resultsortorder = '';
                        $scope.resultsortordervalue = '';

                    }
                    
                }
                //exclude competitor in API Search
                //for tab1
                var api_dummy =[];
                $scope.Api_checked = function(obj){
                    var item = obj.target.attributes.element.value;
                    if($scope.api_checkbox[item]){
                        api_dummy[item] = item;
                    }
                    else{
                        delete api_dummy[item];
                    }
                    var api_temp = "";
                    for(i in api_dummy){
                        api_temp += api_dummy[i] + "," ;
                       
                    }
                     //console.log(api_temp);
                    
                }
                //for tab2
                var api_dummy_tab2 =[];
                $scope.Api_checked_tab2 = function(obj){
                    var item_tab2 = obj.target.attributes.element.value;
                    if($scope.api_checkbox_tab2[item_tab2]){
                        api_dummy_tab2[item_tab2] = item_tab2;
                    }
                    else{
                        delete api_dummy_tab2[item_tab2];
                    }
                    var api_temp_tab2 = "";
                    for(i in api_dummy_tab2){
                        api_temp_tab2 += api_dummy_tab2[i] + "," ;
                       
                    }
                     //console.log(api_temp_tab2);
                    
                }

                 $scope.$watch('crawlurl',function(newvalue,oldvalue){
                    $scope.final_data = newvalue;
                    //console.log($scope.final_data);
                 },true)

                $scope.$watch('crawlurl_exclude',function(newvalue,oldvalue){
                    $scope.final_data_tab2 = newvalue;
                   // console.log($scope.final_data_tab2);
                 },true)
                 

                $scope.Api_save_exclude = function(){
                    var myitemid = $scope.currentitemid;
                    var final_data1 = [];
                   $scope.final_data.forEach(function(part){
                        if($scope.api_checkbox[part.itemId] == true){
                             final_data1.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':1});
                        }
                        if($scope.api_checkbox[part.itemId] == false){
                            final_data1.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':0});
                        }
                   })

                   //console.log(final_data1);

                        $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/ConfigIBR/api_save_exclude',
                                data    : { 'myitemid' : myitemid ,'data' : final_data1},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(data) {
                                $scope.isLoading = false;
                                $('#exc_msg').show();
                                    //alert("Successfully Updated");
                                })
                            .catch(function (err) {
                                var msg =err.status;
                                 //Error Log To DB
                                 var screenname = '<?php echo $screen;?>';
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                               
                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                         data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                 .success(function(response) {
                                                //return true;
                                              });
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                         $('#Pnopopup2').css('display','none'); 
                                          $('#Pnopopup').css('display','none'); 
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                });
                }
              
                $scope.Api_save_exclude_tab2 = function(){
                    console.log('trigger_tab2');
                    var myitemid_tab2 = $scope.currentitemid;
                    var final_data1_tab2 = [];
                   $scope.final_data_tab2.forEach(function(part){
                        if($scope.api_checkbox_tab2[part.itemId] == true){
                             final_data1_tab2.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':1});
                        }
                        if($scope.api_checkbox_tab2[part.itemId] == false){
                            final_data1_tab2.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':0});
                        }
                   })

                   //console.log(final_data1_tab2);

                        $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/ConfigIBR/api_save_exclude',
                                data    : { 'myitemid' : myitemid_tab2 ,'data' : final_data1_tab2},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(data) {
                                $scope.isLoading = false;
                                $('#exc_msg_tab2').show();
                                    //alert("Successfully Updated");
                                })
                            .catch(function (err) {
                                var msg =err.status;
                                 //Error Log To DB
                                 var screenname = '<?php echo $screen;?>';
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                               
                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                         data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                 .success(function(response) {
                                                //return true;
                                              });
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                         $('#Pnopopup2').css('display','none'); 
                                          $('#Pnopopup').css('display','none'); 
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                });
                }

                $scope.submititems = function(){
                    $('#savechange').css('display','none'); 
                    $('#savechange1').css('display','none');
                     $scope.count = 0;
                    $scope.isLoading =true;
                     $http({
                        method : 'POST',
                        url    : '<?php echo base_url(); ?>index.php/ConfigIBR/getSKUCount',
                        data   : {  'sku':$scope.currentsku},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                     })
                    .success(function(data){
                        $scope.isLoading =false;
                        $scope.currentskucount = data;
                        if ($scope.currentskucount > 1)
                        {
                            $('#confirmpopup1').css('display','block'); 
                            $('#confirmpnpopup1').css('display','block'); 
                        }

                        else
                        {
                            $scope.savefilters(0);
                        }
                    })
                    .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                    });
                };
                    
                    

                 $scope.savefilters = function(flag){
                     console.log($scope.owncompid);
                    $('#confirmpopup1').css('display','none'); 
                    $('#confirmpnpopup1').css('display','none'); 
                    console.log($scope.def_current_catid); 
                
                    $scope.crawldata = 'nodata';
                    $scope.AppID = 'ToBeReplaced';
                    $scope.crawlersearch('crawlerbutton');
                    $scope.testsearch('savebutton');
                    $scope.isLoading =true;
                    $scope.default_flag = 1;
                    if($scope.condition == ''){
                        $scope.resultconditionvalue = '';
                    }
                    $scope.resultconditionvalue = $scope.condition;
                    setTimeout(function() { 
                    //return false;
                     $http({
                        method : 'POST',
                        url    : '<?php echo base_url(); ?>index.php/ConfigIBR/save_key_search',
                        data   : {  'itemid':$scope.currentitemid,
                                    'sku' : $scope.currentsku,
                                    'keywords_and':$scope.keywordsand,
                                    'keywords_or':$scope.keywordsor,
                                    'keywords_exclude':$scope.keywordsmore,
                                    'catagory_id':$scope.resultcatlabel,
                                    'myprice':$scope.currentprice,
                                    'percentage':$scope.percent,
                                    'from_price':$scope.minus,
                                    'to_price':$scope.plus,
                                    'condition':$scope.resultconditionvalue,
                                    'shipping':$scope.freeshiping,
                                    'location':$scope.location,
                                    'handlingtime':$scope.handling,
                                    'toprated':$scope.TopRated,
                                    'comptitor':$scope.resultcomplabel,
                                    'sortorder':$scope.resultsortordervalue,
                                    'eBay_url':$scope.url,
                                    'crawlerurl':$scope.crawlerurl,
                                    'flag':$scope.default_flag,
                                    'catagoryid':$scope.def_current_catid,
                                    'titleinclude':$scope.title_inc,
                                    'updateflag' : flag,
                                    'Compid':$scope.owncompid
                                    },
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                     })
                    .success(function(data){
                        $scope.isLoading =false;
                        $scope.catresult.forEach(function (part) {
                              //console.log(part);
                                        if (part.itemid === $scope.currentitemid){
                                            console.log(part);
                                            part.Active_flag  = 1;  
                                            part.Keywords_AND = $scope.keywordsand;
                                            part.Keywords_OR = $scope.keywordsor;
                                            part.Exclude_keywords =$scope.keywordsmore;

                                            part.Keywords_OR_validate = $scope.keywordsor;
                                            part.Exclude_key_validate =$scope.keywordsmore;

                                               
                                        }
                         });
                        //console.log($scope.catresult);

                        //$scope.get_category_result();
                        $('#filtr_msg').css('display','block');
                       //$scope.popupclose();

                    })
                    .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
                    }, 5000);
                }


                $scope.redirectlink = function(keyword){
                    var key = keyword.replace(" ","+");
                    var url ="https://www.ebay.com/sch/i.html?&_nkw="+key;
                    $window.open(url,'_blank','width=1200,height=600');
                };
                   

                    $scope.redirectlink = function(keyword){
                        var key = keyword.replace(" ","+");
                        var url ="https://www.ebay.com/sch/i.html?&_nkw="+key;
                        $window.open(url,'_blank','width=1200,height=600');
                    };

                   
                $scope.popupclose = function(){
                    
                   if($scope.count != 0){
                    $("#Pnopopup2").css('display','block'); 
                    $('#savechange').css('display','block'); 
                    $('#savechange1').css('display','block');
                    }else{
                     $("#Pnopopup2").hide('slow'); 
                     $("#popup").hide('slow'); 
                     $('#filtr_msg').hide('slow');
                     }
                    }
                    
                    $scope.savechangeclose = function(){
                    $scope.count = 0;
                    $('#savechange').css('display','none'); 
                    $('#savechange1').css('display','none');
                    $("#Pnopopup2").hide('slow'); 
                    }
                
                $scope.flagupdate =function(evt){
                    $scope.isLoading =true;
                    $scope.flag_itemid = evt.target.getAttribute('data-value');

                    var flag_val ='';
                    if(evt.target.checked){
                      var flag_val = 1;  
                    }
                    else{
                      var flag_val = 0;  
                    }
                    //console.log(flag_val);
                    //console.log($scope.flag_itemid);
                      $http({
                        method : 'POST',
                        url    : '<?php echo base_url(); ?>index.php/ConfigIBR/flag_update_filter',
                        data   : {'itemid':$scope.flag_itemid,'flag':flag_val},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                     })
                    .success(function(data){
                        $scope.isLoading =false;
                          //success msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                 data    : 6
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });

                    })
                    .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
                }

                $scope.deletepopup =function(evt){
                    $scope.isLoading =true;
                    $scope.deleted_itemid = evt.target.getAttribute('dataitemid');

                      $http({
                        method : 'POST',
                        url    : '<?php echo base_url(); ?>index.php/ConfigIBR/delete_filter',
                        data   : {  'itemid':$scope.deleted_itemid },
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                     })
                    .success(function(data){
                        $scope.isLoading =false;
                        //$scope.get_category_result();
                        $scope.catresult.forEach(function (part) {
                              //console.log(part);
                                        if (part.itemid === $scope.deleted_itemid){
                                            console.log(part);
                                            part.Active_flag  = 0;  
                                            part.Keywords_AND = '';
                                            part.Keywords_OR = '';
                                            part.Exclude_keywords ='';
                                            part.Keywords_OR_validate ='';
                                            part.Exclude_key_validate ='';                                               
                                        }
                         });
                          //success msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                 data    : 10
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });

                    })
                    .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
                }

         // Cron Run functionality start
                $scope.Cron = function(){
                    $('#run_cron').show();
                     $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/ConfigIBR/auto_raw_list_cron'
                            })
                                 .success(function(response) {    

                                });
                }

               /* $scope.cron_status = function(){
                 $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/configIBR/cron_status_checker'
                        })
                             .success(function(response) {
                                console.log(response);
                             if(response.length == 0){
                                $("#run_cron > p").text("Please Wait..."); 
                                $scope.cron_sta_startdate = '';
                                $scope.cron_sta_enddate = '';
                                $scope.cron_sta_progress = '';
                                $scope.cron_sta_total = '';
                                $scope.cron_sta_status = '';
                             }
                             else{
                                $scope.cron_sta_startdate = response[0].start;
                                $scope.cron_sta_enddate = response[0].Ended;
                                $scope.cron_sta_progress = response[0].progress;
                                $scope.cron_sta_total = response[0].total;
                                $scope.cron_sta_status = response[0].status_field;
                                //console.log($scope.cron_sta_status);
                                if($scope.cron_sta_total == $scope.cron_sta_progress){
                                     $('#run_cron').hide();
                                    $('.run_cron_status').hide();
                                    $('#run_cron_end').show();
                                    setTimeout(function() {
                                        $("#run_cron_end").hide('blind', {}, 500)
                                    }, 10000);
                                }
                                else{
                                    $('#run_cron').hide();
                                    $('.run_cron_status').show();
                                }
                             }    
                        });
                }*/
       // Cron Run functionality end

        $scope.keywordprevious = '';
        $scope.keywordspreviousand = '';
        $scope.keywordspreviousmore = '';
        $scope.set_yes = function(){
             //$scope.isLoading = true;
             $scope.keywordsor = $scope.s_or;
             $scope.keywordsand = $scope.s_and;
             $scope.keywordsmore = $scope.s_more;
             if($scope.keywordprevious != ''){
                $scope.keywordsor = $scope.keywordprevious;
                $scope.keywordsand = $scope.keywordspreviousand;
                $scope.keywordsmore = $scope.keywordspreviousmore;
             };
              if($scope.keywordsor == '' && $scope.keywordsand == '' && $scope.keywordsmore == '' ){
                 $('#edit_rest_mp_ip').show(); 
                  $('#mp_ip_opn').hide(); 
              }
             $('#mp_ip_opn').hide();  
        }
         $scope.set_none = function(){
            // $scope.isLoading = true;
            $scope.keywordprevious = $scope.s_or;
            $scope.keywordspreviousand = $scope.s_and;
            $scope.keywordspreviousmore = $scope.s_more;
             $scope.keywordsor = '';
             $scope.keywordsand = '';
             $scope.keywordsmore = '';
             $('#mp_ip_opn').hide();
        }
          //reset button
        $scope.reset = function(){
             $scope.isLoading = true;
              $scope.s_or = $scope.keywordsor;
             $scope.s_and = $scope.keywordsand;
             $scope.s_more = $scope.keywordsmore;
             $('#filtr_msg').hide(); 
             $http({                     
                     method  : 'POST',
                     url     : '<?php echo base_url();?>index.php/ConfigIBR/editpopup_api_search',
                     data    : {'itemid' : $scope.currentitemid},
                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                        console.log(response);
                    $scope.isLoading = false;
                    $scope.keywrd_mpn =response['sel_result'][0]['MPN'] == null ? '' : response['sel_result'][0]['MPN'];
                    $scope.keywrd_ipn =response['sel_result'][0]['IPN'] == null ? '' : response['sel_result'][0]['IPN'];
                    $scope.keywrd_opn =response['sel_result'][0]['OPN'] == null ? '' : response['sel_result'][0]['OPN'];
                    $scope.keywrd_def =response['def_keyword_res'][0]['Default_Keywords'] == null ? '' : response['def_keyword_res'][0]['Default_Keywords'];
                    $scope.keywrd_exc =response['def_keyword_res'][0]['Exclude_Keywords'] == null ? '' : response['def_keyword_res'][0]['Exclude_Keywords'];
                    if($scope.keywrd_mpn == '' && $scope.keywrd_ipn == '' &&  $scope.keywrd_opn == ''){
                        $('#mp_ip_opn').show();
                        $('#edit_mp_ip_opn').hide();
                    }
                    $scope.keywrd_mpn_opn_ipn =$scope.keywrd_mpn+' '+$scope.keywrd_ipn.trim()+' '+$scope.keywrd_opn;
                    $scope.keywordsor =$scope.keywrd_mpn_opn_ipn.replace(/,/g,"");
                     $scope.keywordsand = $scope.keywrd_def;
                   // $scope.keywordsmore = '';
                    $scope.keywordsmore = $scope.keywrd_exc;
                    $scope.keywrd_length_check();
                });
         }
         //get exclude  count
         $scope.exclude_check = function(){
            $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/ConfigIBR/crawler_exclude_test',
                         data    : {'myitemid':$scope.currentitemid},
                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                        //console.log(response);
                        $scope.per_page_data = response.pagedata;
                        $scope.per_page_num = response.pagenumber;

                        if($scope.per_page_data >= 1){
                        $scope.per_page_data = $scope.per_page_data;
                        $scope.per_page_num = $scope.per_page_num;
                        }
                        else{
                            $scope.per_page_data =10;
                            $scope.per_page_num = $scope.per_page_num;
                        }
                        console.log($scope.per_page_data);
                    })
         }
           //edit popup
          $scope.editpopup = function(evt) {
             $scope.reset_disable =false;
             $('#edit_mp_ip_opn').hide();
            $scope.isLoading = true;
                $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/ConfigIBR/crawler_exclude_test',
                         data    : {'myitemid':evt.target.getAttribute('dataitemid')},
                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                        //console.log(response);
                        $scope.per_page_data = response.pagedata;
                        $scope.per_page_num = response.pagenumber;

                        if($scope.per_page_data >= 1){
                        $scope.per_page_data = $scope.per_page_data;
                        $scope.per_page_num = $scope.per_page_num;
                        }
                        else{
                            $scope.per_page_data =10;
                            $scope.per_page_num = $scope.per_page_num;
                        }
                        console.log($scope.per_page_data);
                    })
          if(evt.target.getAttribute('dataKeywords_OR_validate') == '' && evt.target.getAttribute('dataKeywords_AND')== ''){
                 $http({                     
                     method  : 'POST',
                     url     : '<?php echo base_url();?>index.php/ConfigIBR/editpopup_api_search',
                     data    : {'itemid' : evt.target.getAttribute('dataitemid')},
                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                         console.log(response);
                    $scope.isLoading = false;
                    $scope.keywrd_mpn =response['sel_result'][0]['MPN'] == null ? '' : response['sel_result'][0]['MPN'];
                    $scope.keywrd_ipn =response['sel_result'][0]['IPN'] == null ? '' : response['sel_result'][0]['IPN'];
                    $scope.keywrd_opn =response['sel_result'][0]['OPN'] == null ? '' : response['sel_result'][0]['OPN'];
                    $scope.keywrd_def =response['def_keyword_res'][0]['Default_Keywords'] == null ? '' : response['def_keyword_res'][0]['Default_Keywords'];
                     $scope.keywrd_exc =response['def_keyword_res'][0]['Exclude_Keywords'] == null ? '' : response['def_keyword_res'][0]['Exclude_Keywords'];
                    if($scope.keywrd_mpn == '' && $scope.keywrd_ipn == '' &&  $scope.keywrd_opn == ''){
                        $('#edit_mp_ip_opn').show();
                        $('#mp_ip_opn').hide();
                       $scope.reset_disable =true;
                    }
                    $scope.keywrd_mpn_opn_ipn =$scope.keywrd_mpn+' '+$scope.keywrd_ipn.trim()+' '+$scope.keywrd_opn;
                    $scope.keywordsor = $scope.keywrd_mpn_opn_ipn.replace(/,/g,"");
                    $scope.comp_inc = '';
                    $scope.comp_exc = '';
                    if(evt.target.getAttribute('dataPercent') == '' || evt.target.getAttribute('dataPercent') == 0){
                        $scope.pricevalue = 0;
                        $scope.percent  ='';
                       }
                       else if(evt.target.getAttribute('dataPercent') != ''){
                        $scope.percent  = evt.target.getAttribute('dataPercent');
                        $scope.pricevalue = 1;
                       }
                       if(evt.target.getAttribute('dataPriceform_from') == 0){
                         $scope.minus  ='';
                       }
                        else{
                          $scope.minus  = evt.target.getAttribute('dataPriceform_from');
                        }
                        if(evt.target.getAttribute('dataPriceform_to') == 0){
                         $scope.plus  ='';
                        }
                        else{
                             $scope.plus  = evt.target.getAttribute('dataPriceform_to');
                        }
                        //keywords editmode mpn/opn/ipn or keywordAND
                    
                       $("#Pnopopup2").css("display", "block");
                       var conditionvalues = {"3":"New","4":"Used","10":"Not Specified"}; 
                       var shippingval = {"1":"Free Shipping"};
                       var locationval = {"1":"Only US"}; 
                        var handlingval = {"1":"HandlingTime"}; 
                       var topval     = {"1":"TopRated"};
                       var sortorderval =  {"12":"Best Match","1":"Time: ending soonest","10":"Time: newly listed",
                                            "15":"Price + Shipping: lowest first","16":"Price + Shipping: highest first","7":"Distance: nearest first"};
                       mainresultvalue = [];
                       $scope.maincompetitorvalue = [];
                       $scope.currentitemid = evt.target.getAttribute('dataitemid');
                       $scope.currentsku    = evt.target.getAttribute('datasku');
                       $scope.currentcatid  = evt.target.getAttribute('datacategory');
                       $scope.def_current_catid  = evt.target.getAttribute('datacategoryid');
                       console.log($scope.def_current_catid);
                       $scope.currenttitle  = evt.target.getAttribute('datatitle');
                       $scope.currentprice  = evt.target.getAttribute('dataprice');
                      $scope.keywordsand = $scope.keywrd_def;
                       //$scope.keywordsand  = evt.target.getAttribute('dataKeywords_AND');
                       $scope.keywordsmore  = evt.target.getAttribute('dataExclude_keywords');
                       $scope.AppID = evt.target.getAttribute('dataAppID');
                       //$scope.resultcondition  = conditionvalues[evt.target.getAttribute('dataConditions')];
                       //$scope.condition  = evt.target.getAttribute('dataConditions');
                       $scope.resultcondition =conditionvalues[evt.target.getAttribute('dataConditions')== 0 ? 3 : evt.target.getAttribute('dataConditions')] ;
                       $scope.condition  = evt.target.getAttribute('dataConditions');
                       
                       if($scope.condition == 0){
                         $scope.condition = '3';
                        }
                       if($scope.condition == 10){
                        $scope.condition = '10';
                        }
                       if($scope.condition == 3){
                         $scope.condition = '3';
                        }
                        if($scope.condition == 4){
                         $scope.condition = '4';
                        }
                        $scope.keywrd_length_check();
                       $scope.freeshiping  = shippingval[evt.target.getAttribute('dataShipping')];
                       $scope.location  = locationval[evt.target.getAttribute('dataLocation')];
                       $scope.handling   =  handlingval[evt.target.getAttribute('datahandlingtime')];
                       $scope.TopRated   = topval[evt.target.getAttribute('dataTopRated')];
                       $scope.title_inc   = evt.target.getAttribute('dataTitleincludeflag');
                       $scope.owncompid =evt.target.getAttribute('data_compid');
                       
                       //$scope.sortorder  = evt.target.getAttribute('dataSort_order');
                       //$scope.resultsortorder =sortorderval[evt.target.getAttribute('dataSort_order')];
                        $scope.resultsortorder =sortorderval[evt.target.getAttribute('dataSort_order')] ? sortorderval[evt.target.getAttribute('dataSort_order')] : sortorderval[15];
                       $scope.sortorder =evt.target.getAttribute('dataSort_order') == 0 ? 15 : evt.target.getAttribute('dataSort_order');
                       $scope.resultsortordervalue =$scope.sortorder; 
                       //console.log($scope.sortorder) ;
                       $scope.cat_include  = evt.target.getAttribute('dataCategory_Include').split(',');
                       $scope.cat_exclude  = evt.target.getAttribute('dataCategory_Exclude').split(',');
                       $scope.comp_include  = evt.target.getAttribute('dataSellers_Include').split(',');
                       $scope.comp_exclude  = evt.target.getAttribute('dataSellers_Exclude').split(',');

                       //category & competitor label pushing in edit mode
                        if($scope.cat_include != '' || $scope.cat_exclude != ''){
                            $scope.categorymain = [];
                             for(catid in $scope.suggestcat){
                                $scope.categorymain[$scope.suggestcat[catid].CategoryID] = $scope.suggestcat[catid].category;
                           }
                                                 
                           for(catinid in $scope.cat_include){
                                    mainresultvalue[$scope.cat_include[catinid]] = $scope.categorymain[$scope.cat_include[catinid]];
                           }
                           for(catinid in $scope.cat_exclude){
                                    mainresultvalue[$scope.cat_exclude[catinid]] = '!='+$scope.categorymain[$scope.cat_exclude[catinid]];
                           } 
                       }

                       /*console.log($scope.comp_include);
                       console.log($scope.comp_exclude);*/
                       //*****************************
                      if($scope.comp_include != '' || $scope.comp_exclude != '' ){
                               for(catinid in $scope.comp_include){
                                    if($scope.comp_include[catinid] != ''){
                                        $scope.maincompetitorvalue[$scope.comp_include[catinid]] = $scope.comp_include[catinid];
                                    }
                               }
                                for(catinid in $scope.comp_exclude){
                                    if($scope.comp_exclude[catinid] != ''){
                                        $scope.maincompetitorvalue[$scope.comp_exclude[catinid]] = '!='+$scope.comp_exclude[catinid];
                                    }
                               }
                       }

                        $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});
                        var compresponse = [];
                        for(key in $scope.maincompetitorvalue){
                            compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                        }
                        $scope.resultcomplabel = compresponse;  
                        if(compresponse.length == 0){
                            $scope.resultcomplabellength = 0;
                        }  
                 });
            } // if condition end
            else if(evt.target.getAttribute('dataKeywords_AND') != ''|| evt.target.getAttribute('dataKeywords_OR_validate') != ''){
                $scope.isLoading = false;
                $scope.keywordsor  = evt.target.getAttribute('dataKeywords_OR');
                 $scope.comp_inc = '';
                    $scope.comp_exc = '';
                    if(evt.target.getAttribute('dataPercent') == '' || evt.target.getAttribute('dataPercent') == 0){
                        $scope.pricevalue = 0;
                        $scope.percent  ='';
                       }
                       else if(evt.target.getAttribute('dataPercent') != ''){
                        $scope.percent  = evt.target.getAttribute('dataPercent');
                        $scope.pricevalue = 1;
                       }
                       if(evt.target.getAttribute('dataPriceform_from') == 0){
                         $scope.minus  ='';
                       }
                        else{
                          $scope.minus  = evt.target.getAttribute('dataPriceform_from');
                        }
                        if(evt.target.getAttribute('dataPriceform_to') == 0){
                         $scope.plus  ='';
                        }
                        else{
                             $scope.plus  = evt.target.getAttribute('dataPriceform_to');
                        }
                        //keywords editmode mpn/opn/ipn or keywordAND
                    
                       $("#Pnopopup2").css("display", "block");
                       var conditionvalues = {"3":"New","4":"Used","10":"Not Specified"}; 
                       var shippingval = {"1":"Free Shipping"};
                       var locationval = {"1":"Only US"}; 
                       var handlingval = {"1":"HandlingTime"};
                        var topval     = {"1":"TopRated"};
                       var sortorderval =  {"12":"Best Match","1":"Time: ending soonest","10":"Time: newly listed",
                                            "15":"Price + Shipping: lowest first","16":"Price + Shipping: highest first","7":"Distance: nearest first"};
                       mainresultvalue = [];
                       $scope.maincompetitorvalue = [];
                       $scope.currentitemid = evt.target.getAttribute('dataitemid');
                       $scope.currentsku    = evt.target.getAttribute('datasku');
                       $scope.currentcatid  = evt.target.getAttribute('datacategory');
                       $scope.def_current_catid  = evt.target.getAttribute('datacategoryid');
                       console.log($scope.def_current_catid);
                       $scope.currenttitle  = evt.target.getAttribute('datatitle');
                       $scope.currentprice  = evt.target.getAttribute('dataprice');
                       $scope.keywordsand  = evt.target.getAttribute('dataKeywords_AND');
                       $scope.keywordsmore  = evt.target.getAttribute('dataExclude_keywords');
                       $scope.AppID = evt.target.getAttribute('dataAppID');
                        $scope.owncompid =evt.target.getAttribute('data_compid');
                       //$scope.resultcondition  = conditionvalues[evt.target.getAttribute('dataConditions')];
                       //$scope.condition  = evt.target.getAttribute('dataConditions');
                        $scope.resultcondition =conditionvalues[evt.target.getAttribute('dataConditions')];
                       $scope.condition  = evt.target.getAttribute('dataConditions');
                       
                       if($scope.condition == 0){
                         $scope.condition = '0';
                        }
                       if($scope.condition == 10){
                        $scope.condition = '10';
                        }
                       if($scope.condition == 3){
                         $scope.condition = '3';
                        }
                        if($scope.condition == 4){
                         $scope.condition = '4';
                        }
                        //console.log($scope.condition+'else')
                       $scope.freeshiping = shippingval[evt.target.getAttribute('dataShipping')];
                       $scope.location    = locationval[evt.target.getAttribute('dataLocation')];
                       $scope.handling    =  handlingval[evt.target.getAttribute('datahandlingtime')];
                       $scope.TopRated    = topval[evt.target.getAttribute('dataTopRated')];
                       $scope.title_inc   = evt.target.getAttribute('dataTitleincludeflag');
                       //$scope.sortorder  = evt.target.getAttribute('dataSort_order');
                       //$scope.resultsortorder =sortorderval[evt.target.getAttribute('dataSort_order')];
                        $scope.resultsortorder =sortorderval[evt.target.getAttribute('dataSort_order')] ? sortorderval[evt.target.getAttribute('dataSort_order')] : sortorderval[15];
                       $scope.sortorder =evt.target.getAttribute('dataSort_order') == 0 ? 15 : evt.target.getAttribute('dataSort_order');
                       $scope.resultsortordervalue =$scope.sortorder; 
                       //console.log($scope.sortorder) ;
                       $scope.cat_include  = evt.target.getAttribute('dataCategory_Include').split(',');
                       $scope.cat_exclude  = evt.target.getAttribute('dataCategory_Exclude').split(',');
                       $scope.comp_include  = evt.target.getAttribute('dataSellers_Include').split(',');
                       $scope.comp_exclude  = evt.target.getAttribute('dataSellers_Exclude').split(',');
                       $scope.keywrd_length_check();
                       //category & competitor label pushing in edit mode
                        if($scope.cat_include != '' || $scope.cat_exclude != ''){
                            $scope.categorymain = [];
                             for(catid in $scope.suggestcat){
                                $scope.categorymain[$scope.suggestcat[catid].CategoryID] = $scope.suggestcat[catid].category;
                           }
                                                 
                           for(catinid in $scope.cat_include){
                                    mainresultvalue[$scope.cat_include[catinid]] = $scope.categorymain[$scope.cat_include[catinid]];
                           }
                           for(catinid in $scope.cat_exclude){
                                    mainresultvalue[$scope.cat_exclude[catinid]] = '!='+$scope.categorymain[$scope.cat_exclude[catinid]];
                           } 
                       }

                       /*console.log($scope.comp_include);
                       console.log($scope.comp_exclude);*/
                       //*****************************
                      if($scope.comp_include != '' || $scope.comp_exclude != '' ){
                               for(catinid in $scope.comp_include){
                                    if($scope.comp_include[catinid] != ''){
                                        $scope.maincompetitorvalue[$scope.comp_include[catinid]] = $scope.comp_include[catinid];
                                    }
                               }
                                for(catinid in $scope.comp_exclude){
                                    if($scope.comp_exclude[catinid] != ''){
                                        $scope.maincompetitorvalue[$scope.comp_exclude[catinid]] = '!='+$scope.comp_exclude[catinid];
                                    }
                               }
                       }

                        $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});
                        var compresponse = [];
                        for(key in $scope.maincompetitorvalue){
                            compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                        }
                        $scope.resultcomplabel = compresponse;  
                        if(compresponse.length == 0){
                            $scope.resultcomplabellength = 0;
                        }  
            }  // else if condition end               
        }
                    $scope.exportDataExcel = function () {
                        /*var mystyle = {
                            sheetid: 'Config Auto Rawlist',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };*/

                        var data1 = $scope.ForExcelExport;
                        var data2 = $scope.exportdata_excsheettwo;
                        var opts = [{sheetid:'DIVE_Config_Auto_Rawlist',header:true,column: { style: 'background:#a8adc4' },},{sheetid:'Instruction',header:false,column: { style: 'background:#a8adc4'},}];
                        var res = alasql('SELECT  * INTO XLSX("DIVE_Config_Auto_Rawlist.xls",?) FROM ?',
                                        [opts,[data1,data2]]);

                            //alasql('SELECT * INTO XLS("DIVE_Config_Auto_Rawlist.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                };

                $scope.Bulkexport = function(){
                         $http({
                                    method: 'POST',
                                    url     : '<?php echo base_url();?>index.php/ConfigIBR/bulk_export',
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                  })
                        .success(function (data) {
                        console.log(data);
                        $scope.isLoading =false;
                        var mystyle = {
                            sheetid: 'Bulk export',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                        };
                            alasql('SELECT * INTO XLS("DIVE_ BulkExport.xls",?) FROM ?',[mystyle, data]);
                        
                     })
                      
                }
                $scope.save_imp = function(){
                    var type = document.getElementById('ftype').value;
                    //console.log(type);
                        //|| type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                if (type == 'application/vnd.ms-excel')
                    { 

                        //console.log("importvalue");
                        //console.log($scope.parts);

                        $scope.isLoading = true; //Loader Image
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/ConfigIBR/imp_save',
                                    data    : { 'New_Content' : $scope.parts},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                })
                                .success(function(data) {
                                    $scope.isLoading = false;
                                                 $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg_id').html(response);
                                                    $('.err_modal1').css('display','block');
                                                    $('#msg1').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                                    $("#popupform").css("display", "none");
                                    $("#Pnopopup").css("display", "none");
                                    //location.reload(true);
                                        //$scope.message = "Successfully Updated";
                                })
                                .catch(function(err) {
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                var msg = err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                })

                    }
                    else
                    {
                       $scope.message = '';
                       $scope.errormessage = 'You are trying to Upload a Non-CSV File. Try again!!';
                    }
                    };
           

        })

  //directive for import excel
        app.directive('fileReader', function($timeout) {
                return {
                scope: {
                fileReader:"="
                },
                link: function(scope, element) {               
                $(element).on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                var name = files[0].name;
                var type = files[0].type;
                if (files.length) {
                var r = new FileReader();
                r.onload = function(e) {
                var contents = e.target.result;
               
                var resultvalue = [];
                
                angular.forEach(contents.split('\n'), function(value, key) {
                        var result = value.split(",");
                          resultvalue.push({'ItemID' : result[0],'Keywords_AND' : result[1],'Keywords_OR' : result[2], 'Exclude_keywords' :  result[3], 'Category_Include' :  result[4], 'Category_Exclude' :  result[5],'Percent' : result[6],'my_price': result[7], 'Priceform_from' :  result[8], 'Priceform_to' :  result[9], 'Conditions' :  result[10], 'Shipping' :  result[11],'Location' :  result[12], 'Sellers_Include' :  result[13], 'Sellers_Exclude' :  result[14], 'Sort_order' :  result[15]});
                          });
                    // console.log('import');
                     //console.log(resultvalue);
                        
                        scope.$apply(function () {
                        document.getElementById('fname').value = name;
                        document.getElementById('ftype').value = type;
                        scope.fileReader = resultvalue;                    
                    });
                    };
                r.readAsText(files[0]);
                };
              });
            }
          };
      });



    </script>
        <script>
            //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
       </script> 

       <script>
        $(document).on('click','#close2',function(){
          $("#Pnopopup").hide(); 
           $("#popup").hide(); 
            $('#exc_msg').hide();
            $('#exc_msg_tab2').hide();
         //location.reload(true); 
        });    
 </script>
 <!-- <script>       
    $(document).on('click','.close',function(){     
          $("#Pnopopup2").hide();       
        });         
        
</script>   -->     
<script>        
          $(document).on('click','.close1',function(){      
          var name = document.getElementById('fname').value;        
           if (name == '')      
          {     
            $("#Pnopopup1").css("display", "none");         
          }     
          else      
          {     
            location.reload(true);      
            $("#Pnopopup1").css("display", "none");     
          }         
        });     
          //new popup msg close button      
           $(document).on('click','.destroy',function(){        
          $(".err_modal").css("display", "none");       
          //location.reload(true);        
      });       
    $(document).on('click','.destroy2',function(){      
    $(".err_modal").css("display", "none");             
    });     

    

</script> 

<script>        
          $(document).on('click','.close2',function(){      
            $("#confirmpopup1").css("display", "none");         
            $("#confirmpnpopup1").css("display", "none");     
        });     
</script> 
<script>
     $(function () {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
        Pusher.logToConsole = true;
        var pusher = new Pusher('26e79100d1fea27244e6', {
          encrypted: true
        });
        var channel = pusher.subscribe('schedulerprogressarrowhead');
        channel.bind('myevent', function(data) {          
            document.getElementById("run_cron").value = "";
            document.getElementById("run_cron").innerHTML = data.message;
        });
        Pusher.logToConsole = true;
        var pusher = new Pusher('26e79100d1fea27244e6', {
          encrypted: true
        });
        var channel = pusher.subscribe('mynewchannelarrowhead');
        channel.bind('eventarrow', function(data) {
            document.getElementById("run_cron").value = "";
            document.getElementById("run_cron").innerHTML = "Schedular Process Completed Successfully.";
            setTimeout(function () {
                document.getElementById('run_cron').style.display='none';
            }, 10000);
        });
</script>
</body>
</html>
                        
                        


           
           
        
 
            
