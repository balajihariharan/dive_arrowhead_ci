<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>


    </title>
             <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon"/> 
    <!-- <link rel="stylesheet" href="<?php echo base_url().'assets/';?>Romaine/css/style.css"/>   -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/jquery-ui.css"/>
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/popup.css"/>
    <!-- <link rel="stylesheet" href="<?php echo  base_url();?>assets/price_css/style.css" /> --> 
    <link rel="stylesheet" href="<?php echo  base_url();?>assets/css/style_romaine.css" />
  <!--   <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">    --> 
    <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>   
   <!--  <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script> -->
    <!-- <script src="http://netsh.pp.ua/upwork-demo/1/js/typeahead.js"></script>  -->
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>    
    <!--  <script src="https://js.pusher.com/4.1/pusher.min.js"></script> --><!--pusher notification -->
</head>
<style>

 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
    .pagination ul>.active>a, .pagination ul>.active>span {
    color: #999999;
    cursor: default;
    }
  
</style> 
<body class="main" ng-controller="postController" ng-cloak>
<?php


    function getGUID() {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid,20,12)
                .chr(125);// "}"
            return $uuid;
        }
    }
    $result = getGUID();
    $resultval = str_replace('{','',$result);
    $GUID = str_replace('}','',$resultval);
// echo $GUID;

    $datetime = date('Y-m-d H:i:s');
    $time = strtotime($datetime);
?>
<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
     <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
<style type="text/css">
    div .ms-options{
        width:280%;
    }
/*.status_bar td:nth-child(7),.status_bar th:nth-child(7){
    border:none;
    border-bottom:1px solid #e1e1e1;
    border-top:1px solid #e1e1e1;
    text-align:right;
    width:5px;
    min-width:5px;
    max-width:5px;
    font-size:13px;
    padding:0 2px !important;
}
.status_bar td:nth-child(8),.status_bar th:nth-child(8){
    text-align:right;
}*/

.slimtable-page-btn {
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    padding: 0 4px;
    border-radius: 3px;
    cursor:pointer;
}
.slimtable-page-btn:hover {
    background:#187ed5; 
}
.slimtable-page-btn.active {
    background:#187ed5; 
}
.slimtable-paging-btnsdiv{
    float:right;
}
/*.status_bar tr, .status_bar th, .status_bar td{
    width: 10%;
}*/

/*.status_bar thead th:nth-child(6){
    min-width: 600px;
    width: 600px;
    max-width: 600px;
}
.status_bar thead th:nth-child(2){
    min-width: 150px;
    width: 150px;
    max-width: 150px;
}
.status_bar thead th:nth-child(4){
    min-width: 150px;
    width: 150px;
    max-width: 150px;
}*/

ul,li { margin:0; padding:0; list-style:none;}
.label { color:#000; font-size:16px;}
.field1{
    display: inline-block;
    float: left;
    padding: 0 10px 0 0px;
}
.shwpopup
{
    display:none;
    width: 50%;
    margin: 50%;
    position: fixed;
    z-index: 1;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
    left: 0;
    top: 0;
}
.field{
    width:auto;
}
.ckeckclass
{
    margin:2px;
}
.divTable{
    float: left;
    width: 70px;
    height: 40px;
    border-right: 1px solid #e1e1e1;
    padding-top: 28px;
    padding-left: 7px;
    
}
.lastdivTable{
    float: left;
    width: 70px;
    padding-top: 28px;
    padding-left: 7px;
    
}
.paging-nav {
  text-align: right;
  padding-top: 2px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #5D85B2;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

tfoot
{
text-align: center !important;
display: table-row-group !important;
}

.paging-nav,
#tblrawresult {
 
  margin: 0 auto;
  font-family: Arial, sans-serif;
}

.select {
    padding: 2px;
    float: right;
    width: 155px;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}

.selectpage {
    padding: 2px;
    width: 76;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}



/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 10% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 30%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: #fff;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #2d8bd5;
    text-decoration: none;
    cursor: pointer;
}
.itemIDCell
{
    margin:0;
}

@media only screen and (max-width: 1500px) {
    
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }



/**********autocomplete suggestion***************/
#searchResult{
 /*list-style: none;
 display :block;
 padding: 0.1%;
 width: 6.9%;
 position: absolute;
 margin: 0;
 margin-left: 6.7%*/
   /* padding: 0;
    border: 1px solid black;
    max-height: 7.5em;
    overflow-y: auto;
    width: 8.1%;
    position: absolute;
    margin-left: 8.2%;*/
        border: 1px solid #aaa;
    overflow-y: auto;
    width: 172px;
    position: absolute;
    margin-left: 143px;
    padding-bottom: 0px;
    min-height: 10px;
    max-height: 150px;
}

#searchResult li{
 background: white;
 padding: 4px;
cursor: pointer;
    list-style: none;
    border-bottom: 1px solid #ededed;
}

#searchResult li:hover{
    background: #f1f1f1;
}

.centerclass{
  text-align: center;
  vertical-align: middle;
    height: 30px;
    min-width: 150px;
    line-height: 20px;
}

.gridimage{
  max-width: 100px;
  max-height: 100px;
}
/**************/

/*#competitorname option[value='? string:91 ?']{
    display: none;
}*/

</style>
    <div class="container header_bg_clr posi_rela">
        <div class="wrapper">
            <div class="header_main">
                <div class="head_menu">
             <?php echo "<h2>".$screen."</h2>" ;?><!-- <span style="padding-left:40%; color:red; font-size:14px"></span> -->
                </div>
            </div>
        </div>
    </div>  
    <div id="msg" style="display: none;"></div>
             
    </div>
     <!-- error handling popup-->
          <div class="progrss_confirm" >
         <div id="success" class="err_modal1" style="display: none;">
             <div class="alert_box" class="popup" class="modal-content" >
                     <div class="fleld-alert success"> 

                           <div class="head_icon_green" style="background: #1376AF none repeat scroll">
                              <img src="<?php echo base_url();?>/assets/img/success_icon_blue.png"  alt="Logout" title="Logout" class="destroy" />
                          </div>

                            <div class="alert alert-warning" style="padding: 0% 0px;">
                            <!-- <strong  class="msgstatic content_dynamic" style="font-size: 19px;padding:0px 0px">Searching eBay for Competitors for the Keyword</strong> -->
                             <strong  class="msgprogress" style="font-size: 19px;padding:0px 0px"></strong>
                            <strong class="msgprogressitem" style="font-size: 19px;padding:0px 0px"></strong>
                            <strong  class="filterprogress" style="font-size: 19px;padding:0px 0px"></strong>
                           <!--  <strong class="msgprogress"></strong> -->
                           <!-- <button >Cancel</button> -->
                           <div class="alert_ok_btn butn_hide" id="cancel" style="display:none">
                              <button ng-click="cancelfun()" class="btn_ok_sty_green" style="background-color:rgba(179, 0, 0, 0.84);padding: 6px 25px;left: 40%;" >Stop</button>
                            </div>
                             <p></p>
                            </div>
                                
                                    

                          <!--  <div class="alert_ok_btn butn_hide">
                              <button class="btn_ok_sty_green clck" ng-model="msghide" style="margin-left: -57px;">Yes</button>
                            </div>
                            <div class="alert_ok_btn butn_hide">
                              <button class="btn_ok_sty_green nonclck" style="margin: -16px 0 0px 26px;background-color:#b30000;left: 46%;">No</button>
                            </div> -->

                           </div>
                      </div>
                   </div>
                   </div>
                   <!-- -->
                 <!-- error handling popup end-->   
    <div class="container">
        <div class="wrapper">
        <div class="content_main_all">
            <div class="form_head" style="padding: 0px 0px 6px 5px;">
              <form id="searchform" class="raw_list_sec_form" ng-submit="submitForm()">
                <div class="" style="width: auto;float: left;margin-right: 1.33%;">
                    <div class="lable_normal_sec" >
                        Search For <span style="color:red; font-size:16px;">*</span>  : 
                    <input type="textbox" name="searchamazon" id="searchamazon" ng-click="displayvalidation()"
                   size="40" ng-model="searchamazon" value="" placeholder="Enter Keyword/Title/ASIN/PartNumber">
                   </div>
                </div>
              <button value="Search" id="srchbt" name="srchbt" class="button_search" type="submit" ng-click="SearchKeyword()">Search</button>
                
     <center> <div class="error_msg_all" style="display:none" id="ebay_msg_id">Please Enter Keyword/Title/ASIN/PartNumber </div></center>  
            </form>
     
            <div class="button_right">

                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="export_button" ng-click="exportDataExcel()" value="Export As Excel"/>
                            </a>
                        </li>
                </div>
    </div>
             <form ng-submit="addtomasterlist()" id="addform">
                <div class="plan_list_view" style="padding: 0px 0 0px;"> 
            <div class="itm_lst">
               <p>Total Items Listed :</p>
               <p style="font-size: 20px; color: #00886d;">{{resultcount}}</p>
            </div>
        </div>

        <table cellspacing="0" border="1" class="status_bar" >
        <thead>
          <tr>
            <th>Image</th>
            <th class="curser_pt centerclass" ng-click="sortBy('Competitor')">Competitor</th>
            <th class="curser_pt centerclass" ng-click="sortBy('ASIN')">ASIN</th>
            <th class="curser_pt centerclass" ng-click="sortBy('PartNo')">Part Number</th>
            <th class="curser_pt centerclass" ng-click="sortBy('Title')">Title</th>
            <th class="curser_pt centerclass" ng-click="sortBy('Category')">Category</th>
            <th class="curser_pt centerclass" ng-click="sortBy('Model')">Brand</th>
            <th class="curser_pt centerclass" ng-click="sortBy('ListPrice')">List Price (In $)</th>
          </tr>
        </thead>
        <tr ng-repeat="item in result | orderBy:orderByField:reverseSort">
            <td><img src="{{item.ImageURL}}" class="gridimage" /></td>
            <td>{{item.Competitor}}</td>
            <td>{{item.ASIN}}</td>
            <td>{{item.PartNo}}</td>
            <td><a ng-click="redirectToLink($event)" element="{{item.ASIN}}" style="cursor: pointer;" >{{item.Title}}</a></td>
            </td>
            <td>{{item.Category}}</td>
            <td>{{item.Model}}</td>
            <td>{{item.ListPrice}}</td>
        </tr>
        </table>
      </div>  
      <?php $this->load->view('footer.php'); ?>
 </body>
</html>

<script>

            var sortingOrder = 'Competitor';
            var postApp     = angular.module('postApp', ['ngSanitize', 'ngCsv']);

            postApp.controller('postController', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 25;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.currentPage = 0;
                $scope.searchresult = [];
                
                
                //Click the button to open an new browser window that is 1200px wide and 600px tall
                $scope.redirectToLink = function(obj){
                var url ="http://www.amazon.com/dp/" +obj.target.attributes.element.value;
                $window.open(url,'_blank','width=1200,height=600')
                };


                //category loading function
                $scope.SearchKeyword = function()
                {
                    if ($scope.searchamazon == null || $scope.searchamazon == '')
                    {
                      $("#ebay_msg_id").css("display","block");
                      $scope.result = '';
                      $scope.filteredItems = '';
                      $scope.resultcount = 0;
                      return false;
                    }
                    $scope.isLoading = true;
                    $http({                     
                    method  : 'POST',
                    url     : '<?php echo base_url();?>index.php/AmazonResearch/SearchAmazon',
                    data    : {'Keyword': $scope.searchamazon},
                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                      $scope.isLoading = false; //Loader Image+
                      $scope.result = response;
                      console.log($scope.result);
                      $scope.isLoading = false;
                      $scope.resultcount = $scope.result.ItemCount;
                      $scope.ForExcelExport =  '';


                        var exportdata_exc = [];
                            if($scope.result != undefined)
                            {   
                                angular.forEach($scope.result, function(part)
                               {
                                    exportdata_exc.push({'ImageURL' : part.ImageURL,'Competitor' : part.Competitor,'ASIN' : "'"+part.ASIN,'My Part#' : part.PartNo,'Title' : part.Title,'Category' : part.Category,'Brand' : part.Model,'List Price (In $)' : part.ListPrice });
                                    
                                });             
                            }
                     $scope.ForExcelExport = exportdata_exc; 

                      $scope.sortBy = function(propertyName) 
                      {
                          $scope.sortKey = propertyName;   //set the sortKey to the param passed
                          $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                          $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                      };
                      


                    });
                    $scope.exportDataExcel = function () {
                             var mystyle = {
                                sheetid: 'Amazon Search',
                                headers: true,
                                column: { style: 'background:#a8adc4' },
                              };
                              alasql('SELECT * INTO XLS("DIVE_AmazonSearch.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                      };
                };

                $scope.displayvalidation = function()
                {
                    $("#ebay_msg_id").css("display","none");
                }
          });
        </script>

        <script type="text/javascript">
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );        

    </script>
 