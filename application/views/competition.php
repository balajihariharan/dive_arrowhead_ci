<!DOCTYPE html>
<html ng-app="postApp">
<head><title> 
   <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    
</head>
<style type="text/css">
.status_bar tbody tr:last-child {
    background: #ccc none repeat scroll 0 0 !important;
    border-color: red;
    border-bottom-width: 4px;
    border-bottom-style: solid;
}

/**********autocomplete suggestion***************/
#searchResult_main{
 /*list-style: none;
 display :block;
 padding: 0.1%;
 width: 6.9%;
 position: absolute;
 margin: 0;
 margin-left: 6.7%*/
   /* padding: 0;
    border: 1px solid black;
    max-height: 7.5em;
    overflow-y: auto;
    width: 8.1%;
    position: absolute;
    margin-left: 8.2%;*/
        border: 1px solid #aaa;
    overflow-y: auto;
    width: 147px;
    position: absolute;
    margin-left: 392px;
    padding-bottom: 0px;
    min-height: 10px;
    max-height: 150px;
}

#searchResult_main li{
 background: white;
 padding: 4px;
cursor: pointer;
    list-style: none;
    border-bottom: 1px solid #ededed;
}

#searchResult_main li:hover{
    background: #f1f1f1;
}
/**************/



</style>
<link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
<body ng-controller="postController" ng-cloak>
<?php $this->load->view('header.php'); ?>

    <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
   
    <!-- spinner old place -->
    <div class="container header_bg_clr">
        <div class="wrapper">
        
        <div class="head_menu">
           <div class="header_main">
               <?php echo "<h2>".$screen."</h2>" ;?>
            </div>
                <div class="unique_cate"><span>Total Listings :</span><?php if(count($metrics)>=1){echo $metrics[0]['TotalListings'];}    ?></div>
                <div class="total_sales"><span>Total SKUs :</span><?php if(count($metrics)>=1){echo $metrics[0]['TotalSku'];}?></div>
                <div class="unique_sku"><span>Total Sales :</span><?php if(count($metrics)>=1){echo "$". $metrics[0]['TotalSales'];}?></div>
                <div class="total_items"><span>Total Categories :</span><?php if(count($metrics)>=1){echo $metrics[0]['TotalCategory'];}?></div>
        </div>
        </div>
    </div>
    <div id="msg" style="display: none;"></div>
        <div class="container" >
            <div class="wrapper">
            <div class="content_main_all">
                <div id="tabs">
                    <ul>
                        <li class="tabonebutton"><a href="#tabs-1">Show My Items</a></li>
                        <li class="tabtwobutton"><a href="#tabs-2">Show Items That I don't Sell</a></li>
                        <li class="tabthreebutton"><a href="#tabs-3">All</a></li>
                        <button id="btnExport" class="export_button">Export to Excel</button>
                        <button id="btnExport1" class="export_button" style="display:none;">Export to Excel</button>
                        <button id="btnExport2" class="export_button" style="display:none;">Export to Excel</button>

                        <li id="tab4"><input type="checkbox" ng-model="iamlowest" id="iamlowest" ng-click="showlowest()" ng-true-value="1" ng-false-value="0"/><b class="check_box_text">&nbsp;I am the Lowest &nbsp;&nbsp;&nbsp;</b></li>
                        <li id="tab5"><input type="checkbox" ng-model="iamnotlowest" id="iamlowest1" ng-click="showlowest()" ng-true-value="1" ng-false-value="0"><b class="check_box_text">&nbsp;I am not the Lowest</b></li>
                    </ul>
                    <div id="tabs-1">
                            <form id="tab1categorymypartform" ng-submit="category_mypart_submit()">
                                <div class="field_head">
                                    <div class="lable_normal">Category Details <span style="color:red; font-size:16px;">*</span>  : </div>
                                    <select name="langOptfirst[]" id="langOptfirst" multiple name="competitornamefirst[]" ng-change="ngcompetitorfirst()" class="chooseallcategoryfirst" ng-model="competitornamefirst">
                                    <!-- <option value="all" class="multi_drop">All</option> -->
                                    <?php if(count($category_combo) >= 1){ 
                                      foreach($category_combo as $competitorkey => $competitorvalue){
                                      ?>
                                    <option class="submulti_drop" value="<?php echo $competitorvalue->Categoryid;?>"><?php echo $competitorvalue->cat;?></option>
                                    <?php } 
                                    }?>
                                    </select>
                                    &nbsp;&nbsp;
                                    <label>My Partnumber</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="loadcategory" id="normal_search" ng-change="partnosearch()" ng-model="searchpart.partnofirst" value="" autocomplete="off" >

                                            <ul id='searchResult_main' style="display: none">
                                                    <li ng-click='setValue(result.partno)' ng-repeat="result in suggest_partno | filter : searchpart.partnofirst" >{{ result.partno }}</li>
                                            </ul>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                               
                                                    


                                    <button type="submit" id="showing" class="button_search_tab1" value="">Show</button>
                                </div>
                            </form>
                            <center><div class="addbtn error_msg_all" style=" text-align:center; display:none">Please Select the category</div></center>
                    <!-- <button id="btnExport" style="background-color: #034f8b;
                        border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;">Export to Excel</button> -->
                        <div class="plan_list_view" id="table_wrapper">
                            <p id="metricslabel" style="float: right;padding: 0 10px;margin-right: 10px;border-radius: 6px;">Total Items :<span id="metricsval" style="font-size: 26px; color: #00886d;">{{metricsval}}</span></p>
                            <!-- style="position:absolute;top: 4.3%;width: 10%;" -->
                            <table class="research status_bar" cellspacing="0" border="1">
                                <thead>
                                    <tr>
                                        <td class="curser_pt" ng-click="sortBy('MyID')"><span>Item ID</span></td>
                                        <td class="curser_pt" ng-click="sortBy('MySKU')"><span>SKU</span></td>
                                        <td class="curser_pt" ng-click="sortBy('mypartno')"><span>My Part #</span></td>
                                        <td class="curser_pt" ng-click="sortBy('MyTitle')"><span>Title</span></td>
                                        <td class="curser_pt" ng-click="sortBy('MyCategory')"><span>Category</span></td>
                                        <td class="curser_pt" ng-click="sortBy('MyPrice')"><span>My Price(in $)</span></td> 
                                        <td class="curser_pt" ng-click="sortBy('MyQuantitySold')"><span>Qty.Sold (Inc)</span></td>
                                        <td class="curser_pt" ng-click="sortBy('lastweek_Qtysold')"><span>Qty.Sold (Last Wk)</span></td>
                                        <td class="curser_pt" ng-click="sortBy('CompCount')"><span>No.of Comp</span></td> 
                                        <td class="curser_pt" ng-click="sortBy('CompLowPrice')"><span>Comp.Price (L in $)</span></td>
                                        <td class="curser_pt" ng-click="sortBy('CompHighPrice')"><span>Comp.Price (H in $)</span></td>
                                    </tr>
                                    <!--  <tr class="tab1table">
                                        <td><input type="text" class="inputsearch" placeholder="Item ID" ng-model="search.MyID"></td>
                                        <td><input type="text" class="inputsearch" placeholder="SKU" ng-model="search.MySKU"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Part No" ng-model="search.mypartno"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Title" ng-model="search.MyTitle"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Category" ng-model="search.MyCategory"></td>
                                        <td><input type="text" class="inputsearch" placeholder="CurrentPrice" ng-model="search.MyPrice"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Qty Sold (Since Inception)" ng-model="search.MyQuantitySold"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Qty Sold (Last Week)" ng-model="search.lastweek_Qtysold"></td>
                                        <td><input type="text" class="inputsearch" placeholder="No.of Comp" ng-model="search.CompCount"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Low Comp Price" ng-model="search.CompLowPrice"></td>                                    
                                       <td><input type="text" class="inputsearch" placeholder="High Comp Price" ng-model="search.CompHighPrice"></td>                                      
                                      </tr> -->

                                       <tr class="tab1table">
                                        <td><input type="text" class="inputsearch" ng-model="search.MyID"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.MySKU"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.mypartno"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.MyTitle"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.MyCategory"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.MyPrice"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.MyQuantitySold"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.lastweek_Qtysold"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.CompCount"></td>
                                        <td><input type="text" class="inputsearch" ng-model="search.CompLowPrice"></td>
                                       <td><input type="text" class="inputsearch" ng-model="search.CompHighPrice"></td>                                      
                                      </tr>
                                </thead>
                                 <!-- <tbody ng-repeat="firstitem in firsttabitem | filter:search | orderBy:propertyName:reverse"> -->
                                <tbody ng-repeat="firstitem in firsttabitem | filter:search | orderBy:sortKey:reverse">
                                <tr class="accordion">
                                    <td > 
                                    <i class="heart_beat article">
                                    <span><img class="plusandminus" ng-click="goCats = !goCats" ng-show="!goCats" data-value="{{firstitem.MyID}}" src="<?php echo base_url().'assets/';?>img/plus_icon1.png" /></span>
                                    <span><img class="plusandminus" ng-click="goCats = !goCats" ng-show="goCats" data-value="{{firstitem.MyID}}" src="<?php echo base_url().'assets/';?>img/minus_icon1.png" /></span>
                                    </i>
                                    <p class="mysalestrends1" ng-click="mysalestrends($event)" datacompid="{{firstitem.MyID}}" datasku="{{firstitem.MySKU}}" dataname="<?php echo $this->session->userdata('compid'); ?>" datalist="main" style="text-decoration: underline;color: #0094DE;cursor:pointer">{{firstitem.MyID}}</p>
                                    </td>
                                    <td>{{firstitem.MySKU}}</td>
                                    <td data-value="{{firstitem.MyID}}" class="mymodalmaintable" id="myupdatepartno{{firstitem.mypartno}}" my-partnumber="{{firstitem.mypartno}}"><a href="#" style="color: #0094DE;" id="updatedpartnumber{{firstitem.MyID}}">{{firstitem.mypartno}}</a></td>
                                    <td><a ng-click="redirectToLink($event)" element="{{firstitem.MyID}}" style="text-decoration: none;color: #0094DE;cursor:pointer;">{{firstitem.MyTitle}}</a></td>
                                    <td>{{firstitem.MyCategory}}</td>
                                    <td class="alignment">{{firstitem.MyPrice}}</td>
                                    <td class="alignment">{{firstitem.MyQuantitySold}}</td>
                                    <td class="alignment">{{firstitem.lastweek_Qtysold}}</td>
                                    <td>{{firstitem.CompCount}}</td>
                                    <td>{{firstitem.CompLowPrice}}</td>
                                    <td>{{firstitem.CompHighPrice}}</td>
                                </tr>
                                       <tr class="table_inner_head_acc tab1subtable" ng-show="goCats" id="{{firstitem.MyID}}">
                                            <th class="curser_pt" ng-click="sortBy('CompetitorItemID1')" style="color:#fff;">Item ID</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorSKU1')" style="color:#fff;">SKU</th>
                                            <th class="curser_pt" ng-click="sortBy('mypartno1')" style="color:#fff;">My Part #</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorTitle1')" style="color:#fff;">Title</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorCategory1')" style="color:#fff;">Category</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorPrice1')" style="color:#fff;">Current Price(in $)</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorQuantitySold1')" style="color:#fff;">Qty.Sold (Inc)</th>
                                            <th class="curser_pt" ng-click="sortBy('lastweek_Qtysold1')" style="color:#fff;">Qty.Sold (Last Wk)</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorName1')" colspan="4" style="color:#fff;">Competitor Name</th>
                                        </tr>
                                         <!-- <tr class="expandrow" ng-show="goCats" ng-repeat="firstinsideitem in firsttabinsideitem[firstitem.MyID] | orderBy:propertyName:reverse" id="{{firstitem.MyID1}}_child"> -->
                                        <tr ng-show="goCats" ng-repeat="firstinsideitem in firsttabinsideitem[firstitem.MyID] | orderBy:sortKey:reverse" id="{{firstitem.MyID1}}_child">
                                             <td class="mysalestrends" ng-click="mysalestrends($event)" datacompid="{{firstinsideitem.CompetitorItemID1}}" datasku="{{firstinsideitem.CompetitorSKU1}}" dataname="{{firstinsideitem.CompetitorName1}}" datalist="sublist" style="text-decoration: underline;color: #0094DE;cursor:pointer">{{firstinsideitem.CompetitorItemID1}}</td>
                                             <td>{{firstinsideitem.CompetitorSKU1}}</td>
                                             <td data-value="{{firstinsideitem.CompetitorItemID1}}" class="mymodalsubtable" id="myupdatepartno{{firstinsideitem.mypartno1}}" my-partnumber="{{firstinsideitem.mypartno1}}" my-competitorname="{{firstinsideitem.CompetitorName1}}"><a href="#" style="color: #0094DE;" id="updatedsubpartnumber{{firstinsideitem.CompetitorItemID1}}">{{firstinsideitem.mypartno1}}</a></td>
                                             <!-- href="http://www.ebay.com/itm/{{firstinsideitem.CompetitorItemID1}}" -->
                                             <td><a ng-click="redirectToLink($event)" element="{{firstinsideitem.CompetitorItemID1}}" style="text-decoration: none;color: #0094DE;cursor:pointer;">{{firstinsideitem.CompetitorTitle1}}</a></td>
                                             <td>{{firstinsideitem.CompetitorCategory1}}</td>
                                             <td class="alignment">{{firstinsideitem.CompetitorPrice1}}</td>
                                             <td class="alignment">{{firstinsideitem.CompetitorQuantitySold1}}</td>
                                             <td class="alignment">{{firstinsideitem.lastweek_Qtysold1}}</td>
                                             <td colspan="4">{{firstinsideitem.CompetitorName1}}</td>
                                        </tr>
                                        <!-- <span class="expandrow">____________________________________________________________</span> -->
                                </tbody>
                                <tr ng-show="result"><td colspan="11"><h1><center>{{result}}</center></h1></td></tr>
                                
                            </table>
                            </div>
                                <!-- <pagination total-items="totalItems" ng-model="currentPage"  max-size="5" boundary-links="true" items-per-page="numPerPage" class="pagination-sm"></pagination> -->
                                <nav class="pageination" style="margin-left:90%;">
                                  <div class="prev">
                                    <<
                                  </div>
                                  <div class="current-page">
                                  </div>
                                  <div class="next">
                                  >>
                                  </div>
                                </nav>
                        </div>

                    <div id="tabs-2" ng-cloak>
                         <div class="form_style">
                            <form id="competitorform" ng-submit="submitForm()">
                                    <div class="field_head">
                                        <div class="lable_normal">Competitor Details <span style="color:red; font-size:16px;">*</span>  : </div>
                                        <select name="langOpt[]" id="langOpt" multiple name="competitorname[]" class="chooseallcategory" ng-model="competitorname" ng-change="ngcompetitor()">
                                            <?php if(count($data) >= 1){ 
                                              foreach($data as $competitorkey => $competitorvalue){
                                              ?>
                                            <option class="submulti_drop" value="<?php echo $data[$competitorkey]->CompID;?>"><?php echo $data[$competitorkey]->CompetitorName;?></option>
                                            <?php } 
                                            }?>
                                        </select>

                                    </div>
                                    <div class="field_head secondselect">
                                        <div class="lable_normal">
                                            Category Details <span style="color:red; font-size:16px;">*</span>  : 
                                        </div>
                                        <select ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" id="langOpt1" multiple name="competitorcategory[]" ng-model="competitorcategory"></select>
                                        <!-- <select name="langOpt1[]" id="langOpt1" multiple name="competitorcategory[]" ng-model="competitorcategory">
                                         <option ng-repeat="subcat in subcategories" ng-if="!!subcat" ng-bind-html="::subcat.cat" ng-value="::subcat.Categoryid">
                                          </option>
                                        </select> -->
                                    </div>
                                    <div class="field_head">
                                        <button type="submit"  id="search2" class="button_search" value="">Search</button>
                                    </div>
                                </form>  
                                </div>
                                 <center><div class="addbtn2 error_msg_all" style=" text-align:center; display:none">Please Select the Competitor and Category</div></center>
                         <div class="plan_list_view" id="table_wrapper1">
                            <table class="research status_bar_inner" cellspacing="0" border="1">
                                <thead>
                                    <tr class="tab2tablemain">
                                        <td class="curser_pt" ng-click="sortBy('SellerName')"><span >SellerName</span></td>
                                        <td class="curser_pt" ng-click="sortBy('ITemID')"><span >Item ID</span></td>
                                        <td class="curser_pt"  ng-click="sortBy('SKU')"><span>SKU</span></td>
                                        <td class="curser_pt" ng-click="sortBy('Title')"><span >Title</span></td>
                                        <td class="curser_pt" ng-click="sortBy('Category')"><span >Category</span></td> 
                                        <td class="curser_pt" ng-click="sortBy('Price')"><span >CurrentPrice</span></td>
                                        <td class="curser_pt" ng-click="sortBy('QuantitySold')"><span >QuantitySold</span></td>
                                    </tr>
                                     <tr class="tab2table">
                                        <td><input type="text" class="inputsearch" placeholder="SellerName" ng-model="search.SellerName"></td>
                                        <td><input type="text" class="inputsearch" placeholder="ITemID" ng-model="search.ITemID"></td>
                                        <td><input type="text" class="inputsearch" placeholder="SKU" ng-model="search.SKU"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Title" ng-model="search.Title"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Category" ng-model="search.Category"></td>
                                        <td><input type="text" class="inputsearch" placeholder="CurrentPrice" ng-model="search.Price"></td>
                                        <td><input type="text" class="inputsearch" placeholder="QuantitySold" ng-model="search.QuantitySold"></td>
                                      </tr>
                                </thead>
                                <tbody>
                                   <!-- <tr ng-repeat="item in items | filter:search | orderBy:propertyName:reverse" class="col s12"> -->
                                   <tr ng-repeat="item in items | filter:search | orderBy:sortKey:reverse" class="col s12">
                                        <td class="col s2"><p data-ng-bind="item.SellerName"></p></td>
                                        <td class="col s2"><p data-ng-bind="item.ITemID"></p></td>
                                        <td class="col s2"><p data-ng-bind="item.SKU"></p></td>
                                        <td class="col s2"><a ng-click="redirectToLink($event)" element="{{item.ITemID}}" style="text-decoration: none;color: #0094DE;cursor:pointer;">{{item.Title}}</a></td>
                                        <td class="col s2"><p data-ng-bind="item.Category"></p></td>
                                        <td class="col s2"><p data-ng-bind="item.Price"></p></td>
                                        <td class="col s2"><p data-ng-bind="item.QuantitySold"></p></td>
                                    </tr>
                                </tbody>
                                <tr ng-show="secondresult"><td colspan="11"><h1><center>{{secondresult}}</center></h1></td></tr>
                            </table>

                    </div>
                    </div>  
                    <div id="tabs-3" ng-cloak>
                        <!-- implement -->
                            <div class="form_style">
                            <form ng-submit="submitFormtab3()">
                                    <div class="field_head">
                                        <div class="lable_normal">Competitor Details <span style="color:red; font-size:16px;">*</span>  : </div>

                                        <select name="langOpt2[]" id="langOpt2" multiple name="competitorname1[]" ng-change="ngcompetitor1()" class="chooseallcategory1" ng-model="competitorname1">
                                            <!-- <option class="multi_drop" value="all">All</option> -->
                                            <?php if(count($data) >= 1){ 
                                              foreach($data as $competitorkey => $competitorvalue){
                                              ?>
                                            <option value="<?php echo $data[$competitorkey]->CompID;?>"><?php echo $data[$competitorkey]->CompetitorName;?></option>
                                            <?php } 
                                            }?>
                                        </select>

                                    </div>
                                    <div class="field_head">
                                        <div class="lable_normal">
                                            Category Details <span style="color:red; font-size:16px;">*</span>  : 
                                        </div>
                                        <select ng-options="::subcat3.cat for subcat3 in subcategoriestab track by ::subcat3.Categoryid" id="langOpt3" multiple name="competitorcategory[]" ng-model="competitorcategory3"></select>
                                        
                                        <!-- <select name="langOpt3[]" id="langOpt3" multiple name="competitorcategory[]" ng-model="competitorcategory3">
                                          <option ng-repeat="subcat3 in subcategoriestab" ng-bind-html="subcat3.cat" ng-value="{{subcat3.Categoryid}}">
                                          {{subcat3.cat}}
                                          </option>
                                        </select> -->

                                    </div>
                                    <div class="field_head">
                                        <button type="submit" id="search3"  class="button_search" value="">Search</button>
                                    </div>
                                </form>
                                </div>
                                 <center><div class="addbtn3 error_msg_all" style=" text-align:center; display:none">Please Select the Competitor and Category</div></center>

                            <div class="plan_list_view" id="table_wrapper2">
                            <table id="tableToExporttab3" class="research status_bar_inner" cellspacing="0" border="1">
                                    <thead>
                                    <tr class="tab3tablemain">
                                        <td class="curser_pt" ng-click="sortBy('SellerName')"><span >SellerName</span></td>
                                        <td class="curser_pt" ng-click="sortBy('ITemID')"><span >Item ID</span></td>
                                        <td class="curser_pt" ng-click="sortBy('SKU')"><span >SKU</span></td>
                                        <td class="curser_pt" ng-click="sortBy('Title')"><span >Title</span></td>
                                        <td class="curser_pt" ng-click="sortBy('Category')"><span >Category</span></td> 
                                        <td class="curser_pt" ng-click="sortBy('Price')"><span >CurrentPrice</span></td>
                                        <td class="curser_pt" ng-click="sortBy('QuantitySold')"><span >QuantitySold</span></td>
                                    </tr>
                                     <tr class="tab3table">
                                        <td><input type="text" class="inputsearch" placeholder="SellerName" ng-model="search.SellerName"></td>
                                        <td><input type="text" class="inputsearch" placeholder="ITemID" ng-model="search.ITemID"></td>
                                        <td><input type="text" class="inputsearch" placeholder="SKU" ng-model="search.SKU"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Title" ng-model="search.Title"></td>
                                        <td><input type="text" class="inputsearch" placeholder="Category" ng-model="search.Category"></td>
                                        <td><input type="text" class="inputsearch" placeholder="CurrentPrice" ng-model="search.Price"></td>
                                        <td><input type="text" class="inputsearch" placeholder="QuantitySold" ng-model="search.QuantitySold"></td>
                                      </tr>
                                </thead>
                                <tbody>
                                   <tr ng-repeat="tabitem in tabitems | filter:search | orderBy:sortKey:reverse" class="col s12">
                                        <td class="col s2"><p data-ng-bind="tabitem.SellerName"></p></td>
                                        <td class="col s2"><p data-ng-bind="tabitem.ITemID"></p></td>
                                        <td class="col s2"><p data-ng-bind="tabitem.SKU"></p></td>
                                        <td class="col s2"><a ng-click="redirectToLink($event)" element="{{tabitem.ITemID}}" style="text-decoration: none;color: #0094DE;cursor:pointer;">{{tabitem.Title}}</a></td>
                                        <td class="col s2"><p data-ng-bind="tabitem.Category"></p></td>
                                        <td class="col s2"><p data-ng-bind="tabitem.Price"></p></td>
                                        <td class="col s2"><p data-ng-bind="tabitem.QuantitySold"></p></td>
                                    </tr>
                                </tbody>
                                <tr ng-show="thirdresult"><td colspan="11"><h1><center>{{thirdresult}}</center></h1></td></tr>
                            </table>
                            <!-- <div class="next_prev_haed">
                                <table class="next_prev">
                                    <tbody>
                                        <tr>
                                            <td colspan="8">
                                            <td><a>Page</a></td>
                                            <td><input class="input_page" readonly="readonly" type="text"></td>
                                            <td>of 10</td>
                                            <td><a>Show Rows of:</a></td>
                                            <td>
                                                <select class="select_nav">
                                                    <option selected="selected" value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="40">40</option>
                                                    <option value="80">80</option>
                                                    <option value="160">160</option>

                                                </select>
                                            </td>
                                            <td><a><img src="<?php echo base_url().'assets/';?>img/previous.png"></a></td>
                                            <td><a><img src="<?php echo base_url().'assets/';?>img/left.png"></a></td>
                                            <td><a><img src="<?php echo base_url().'assets/';?>img/right.png"></a></td>
                                            <td><a><img src="<?php echo base_url().'assets/';?>img/next.png"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> -->
                    </div>
                    </div>
                </div>
                </div>
               </div>
                </div>
           <?php $this->load->view('footer.php'); ?>
         
        <!-- popup start -->    
        <!-- ********************************** -->
                                    <form id="popupform" ng-submit="submitmainForm()">
                                        <div id="Pnopopup" class="modal">
                                               <div class="pop_map_my_part">
                                               
                                                 <!-- <center><div class="successupdate" style="display:none;">Update Successfully</div></center> -->
                                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                                  <center><div class="pnum" style="display:none;">Please Enter Part Number</div></center>
                                                 <div class="pop_container">
                                                            <div class="heading_pop_main">
                                                                <h4>
                                                                    Map my Part #
                                                                </h4>
                                                                 <span id="myPnopopup{{firstitem.MyID}}" class="close">&times;</span>
                                                            </div>
                                                            <div class="pop_pad_all">
                                                                <div class="field_12">
                                                                    <div class="lable_normal">
                                                                        <label class="normal_label">Item ID</label>
                                                                        <label id="displayitemid"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="field_12">
                                                                                    <div class="lable_normal">
                                                                                      <label class="normal_label">My Part Number </label>
                                                            <input type="hidden" name="maincompetitorname" ng-model="mainpopup.maincompetitorname" value="main"/>
                                                            <input type="text" id="runpartnumber" name="maincompitemid" ng-model="searchpart.partno" ng-change="popuppartnosearch()" value="" autocomplete="off" />
                                                            
                                                            <ul id='searchResult1' >
                                                                    <li ng-click='popupsetValue(presult.partno)' ng-repeat="presult in suggest_partno | filter : searchpart.partno" >{{ presult.partno }}</li>
                                                            </ul>



                                                            <input type="hidden" name="mainmypartnumber" ng-model="mainpopup.maincompetitoritemid" id="displayitemidhidden" value=""/>
                                                            <input type="hidden" name="mainlist" ng-model="mainpopup.mainlist" value="main"/>
                                                            <input type="hidden" name="part" id="mainpartnumber" ng-model="mainpopup.part" value=""/>
                                                                    </div>  
                                                                </div>
                                                                <div class="button_center">
                                                                    <img style="display:none;" class="ajaxloader" src='<?php echo base_url().'assets/';?>img/ajax-loader.gif'/>
                                                                    <button type="submit" id="popsavemain" class="button_upload">Save</button>
                                                                </div>
                                                            </div>
                                                </div>
                                               </div>
                                            </div>
                                    </form>

                                    <form id="popupsubform" ng-submit="submitsubForm()">
                                        <div id="Pnopopupsub" class="modal">
                                               <div class="pop_map_my_part">
                                               
                                                <!--  <center><div class="successupdate" style="display:none;">Update Successfully</div></center> -->
                                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                                  <center><div class="pnumsub" style="display:none;">Please Enter Part Number</div></center>
                                                 <div class="pop_container">
                                                            <div class="heading_pop_main">
                                                                <h4>
                                                                    Map my Part #
                                                                </h4>
                                                                 <span id="myPnopopupsubclose" class="myPnopopupsubclose">&times;</span>
                                                            </div>
                                                            <div class="pop_pad_all"">
                                                                <div class="field_12">
                                                                    <div class="lable_normal">
                                                                        <label class="normal_label">Item ID</label>
                                                                        <label id="displayitemidsub"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="field_12">
                                                                                    <div class="lable_normal">
                                                                                      <label class="normal_label">My Part Number <span style="color:red; font-size:16px;">*</span></label>
                                                            <input type="hidden" name="subcompetitorname" id="competitornamesublist" ng-model="subpopup.subcompetitorname"/>
                                                            <input type="text" id="runpartnumbersub" name="subcompitemid" ng-model="searchpart.partno" ng-change="subpopuppartnosearch()" value="" autocomplete="off"/>
                                                            
                                                            <ul id='searchResult2'>
                                                                    <li ng-click='subpopupsetValue(subresult.partno)' ng-repeat="subresult in suggest_partno | filter : searchpart.partno" >{{ subresult.partno }}</li>
                                                                    
                                                            </ul>

                                                            <input type="hidden" name="submypartnumber" ng-model="subpopup.subcompetitoritemid" id="displayitemidhiddensub" value=""/>
                                                            <input type="hidden" name="sublist" ng-model="subpopup.sublist" value="sublist"/>
                                                                    </div>  
                                                                </div>
                                                                <div class="button_center">
                                                                    <img style="display:none;" class="ajaxloader" src='<?php echo base_url().'assets/';?>img/ajax-loader.gif'/>
                                                                    <button type="submit" id="popsavesub" class="button_upload">Save</button>
                                                                </div>
                                                            </div>
                                                </div>
                                               </div>
                                            </div>
                                    </form>

                                    <!-- Item id based sales trends -->
                                   <div id="Pnopopupsalestrends" ng-if="Pnopopupsalestrends" class="modal" style="display:block;">
                                   <!-- <input type="checkbox" ng-model="checkboxModel.value2" class="iamnotlowest" ng-click="iamnotlowest()" ng-true-value="1" ng-false-value="0"> -->
                                               <div class="modal-content" style="display:block; width:80%">
                                               <div class="lable_normal table_top_form">
                                               
                                               <input id="showvalues" ng-model="checkboxModel.showvalues" ng-click="salestrendsshowvalues()" type="checkbox">Show values</div>
                                                <span id="myPnopopupsubclose" ng-click="myPnopopupsubclose()" class="myPnopopupsubclose">&times;</span>
                                                 <center><div class="successupdate" style="display:none;">Update Successfully</div></center>
                                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                                 <center><div id="NoRecord" style="display:none;">No Record Found</div></center>
                                                    <table id="MainContent_GV" cellspacing="0" border="1" class="status_bar">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:8%;">Competitor ID #</th>
                                                            <th style="width:8%;">Competitor SKU</th>
                                                            <th style="width:6%;">Current Price(in $)</th>
                                                            <th style="width:6%; min-width:0px;">Qty. Sold (5 wk)</th>
                                                            <th style="width:6%; min-width:100px;">Total Sales (5 wk in $)</th>
                                                            <th style="width:20%;">5 wk Price (In $)</th>
                                                            <th style="width:20%;">5 wk Qty.Sold</th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                           <tr>
                                                                <td>
                                                                    <div class="search"></div>
                                                                </td>
                                                                <td>
                                                                    <div class="search"></div>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                <em>W{{weekno5}}</em>
                                                                <em>W{{weekno4}}</em>
                                                                <em>W{{weekno3}}</em>
                                                                <em>W{{weekno2}}</em>
                                                                <em>W{{weekno1}}</em>
                                                                </td>
                                                                <td>
                                                                <em>W{{weekno5}}</em>
                                                                <em>W{{weekno4}}</em>
                                                                <em>W{{weekno3}}</em>
                                                                <em>W{{weekno2}}</em>
                                                                <em>W{{weekno1}}</em>
                                                                </td>
                                                         </tr>
                                                         </tfoot>
                                                            <tbody>
                                                            <tr style="display: table-row;" ng-repeat="sales in salestrendsdata">
                                                                <td>{{sales.itemid}}</td>
                                                                <td>{{sales.sku}}</td>
                                                                <td class="alignment">{{sales.price}}</td>
                                                                <td class="alignment">
                                                                <div class="tbl_txt">
                                                                 <div class="price_text">{{sales.qtysold}}</div>
                                                                </div>
                                                                </td>
                                                                <td class="alignment">
                                                                    <div class="tbl_txt">
                                                                        <div class="price_text" style="margin-right:10%">{{sales.totalsales}}</div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                <div class="tbl_img">
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'u'" class="price_up"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'u'" class="price_up"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'u'" class="price_up"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'u'" class="price_up"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'u'" class="price_up"></div>
                                                                </div>
                                                                <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                                        <div class="price_text">{{sales.pricevw1}}</div>
                                                                        <div class="price_text">{{sales.pricevw2}}</div>                              
                                                                        <div class="price_text">{{sales.pricevw3}}</div>
                                                                        <div class="price_text">{{sales.pricevw4}}</div>
                                                                        <div class="price_text">{{sales.pricevw5}}</div>
                                                                </div>
                                                                </td>
                                                                <td>
                                                                <div class="tbl_img">
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'u'" class="price_up"></div>

                                                                        <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                                            <div class="price_text">{{sales.qtysoldvw1}}</div>
                                                                            <div class="price_text">{{sales.qtysoldvw2}}</div>                              
                                                                            <div class="price_text">{{sales.qtysoldvw3}}</div>
                                                                            <div class="price_text">{{sales.qtysoldvw4}}</div>
                                                                            <div class="price_text">{{sales.qtysoldvw5}}</div>
                                                                        </div>
                                                                </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                               </div>
                                            </div>
                        <!-- Item id based sales trends end -->

        <!-- popup end -->

  <script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script>

        <script>
             $(function () {
                $("#tabs").tabs();
            });
        </script>
        <script >
             $('#showing').click(function(e){ 
            if ($('#langOptfirst').val() == "" ) {
                    var langOptfirst=document.getElementById('langOptfirst').value;
                   if(langOptfirst == ""){
                         $('.addbtn').css('display','inline-block');
                        return false;
                        e.preventDefault();
                      }
             }  
            else
            {  $('.addbtn').css('display','none');
            }
               });

             $('#search2').click(function(e){ 
                    if ($('#langOpt').val() == "" || $('#langOpt1').val() == "" ) {
                        
                            var langOpt=document.getElementById('langOpt').value;
                           if(langOpt == ""){
                                 $('.addbtn2').css('display','inline-block');
                                return false;
                                e.preventDefault();
                              } 
                              var langOpt1=document.getElementById('langOpt1').value;
                               if(langOpt1 == ""){
                                     $('.addbtn2').css('display','inline-block');
                                    return false;
                                    e.preventDefault();
                                  }
                     }  
                    else
                    {  
                        $('.addbtn2').css('display','none');
                    }
               });

        $('#search3').click(function(e){ 
                    if ($('#langOpt2').val() == "" || $('#langOpt3').val() == "" ) {
                        
                            var langOpt2=document.getElementById('langOpt2').value;
                           if(langOpt2 == ""){
                                 $('.addbtn3').css('display','inline-block');
                                return false;
                                e.preventDefault();
                              } 
                              var langOpt3=document.getElementById('langOpt3').value;
                               if(langOpt3 == ""){
                                     $('.addbtn3').css('display','inline-block');
                                    return false;
                                    e.preventDefault();
                                  }
                     }  
                    else
                    {  
                        $('.addbtn3').css('display','none');
                    }
               });

        </script>
        
        <script>
            $('#langOptfirst').multiselect({
                columns: 1,
                placeholder: 'Select Category',
                selectAll: true
            });
            $('#langOpt').multiselect({
                columns: 1,
                placeholder: 'Select Competitor',
                selectAll: true
            });
            $('#langOpt1').multiselect({
                columns: 1,
                placeholder: 'Select category',
                selectAll: true
            });

            $('#langOpt2').multiselect({
                columns: 1,
                placeholder: 'Select category',
                selectAll: true
            });
            $('#langOpt3').multiselect({
                columns: 1,
                placeholder: 'Select category',
                selectAll: true
            });

        </script>
        <script>
          /*  var postApp     = angular.module('postApp', ['ngSanitize','ui.bootstrap','ngResource']);*/
            var postApp     = angular.module('postApp', []);
            /*postApp.factory('Excel',function($window){
                var uri     ='data:application/vnd.ms-excel;base64,',
                template    ='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
                    base64  =function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
                    format  =function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
                    return {
                             tableToExcel:function(tableId,worksheetName){
                              var table=$(tableId),
                                ctx={worksheet:worksheetName,table:table.html()},
                                href=uri+base64(format(template,ctx));
                             return href;
                           }
                    };
            });
*/
            postApp.controller('postController', function($scope, $http, $timeout,$window,$attrs,$filter) {

                Date.prototype.getWeek = function() {
                    var onejan = new Date(this.getFullYear(), 0, 1);
                    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                }
                var weekNumber = (new Date()).getWeek();
                var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                var now = new Date();
                $scope.weekno1 = weekNumber; 
                $scope.weekno2 = weekNumber-1;
                $scope.weekno3 = weekNumber-2;
                $scope.weekno4 = weekNumber-3;
                $scope.weekno5 = weekNumber-4;
                this.pageLoaded  = true;
                $scope.isLoading = false;
                $scope.goCats = false;
                $scope.parseInt = parseInt;
                $scope.currentPage = 1;


                  $http.get("<?php echo base_url();?>index.php/Welcome/search_partno")
                               .then(function (response) {
                                   $scope.suggest_partno = response.data;
                                   $scope.filteredItems1 = $scope.suggest_partno;
                                   console.log($scope.suggest_partno);
                                  })

                    $scope.redirectToLink = function (obj) {
                        var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                        $window.open(url, '_blank','width=1200,height=600"');
                    };


                 $scope.category_mypart_submit = function() {
                    $('#searchResult_main').css('display','none');
                    var lowest;
                    var notlowest;
                    if($scope.iamlowest){lowest = 1;}else{ lowest = 0; }
                    if($scope.iamnotlowest){notlowest = 1;}else{ notlowest = 0; }
                        if($scope.result != ''){
                            $scope.result = '';
                        }
                        var competitorcategory = $scope.competitornamefirst.map(function(item){
                            return item.competitornamefirst;
                        });

                        $scope.isLoading = true;
                        $http({                     
                            method  : 'POST',
                            url     : '<?php echo base_url();?>index.php/Filters/index_get',
                            data    : {'categoryids':$scope.competitornamefirst,'mypartno':$('#normal_search').val(),'iamlowestfirst':lowest,'iamlowestsecond':notlowest},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .success(function(response) {
                            $scope.isLoading = false;
                            $scope.firsttabitem             = response.first;
                            $scope.firsttabinsideitem       = response.second;
                            $scope.metricsval   =   $scope.firsttabitem.length;    
                             if($scope.firsttabitem.length > 0){
                                $scope.firsttabitem  = data;
                            }else{
                                $scope.result    = 'No Records found';
                            }
                            //console.log( $scope.firsttabinsideitem);return false;                        
                       })
                        .catch(function (err) {
                                  var msg =err.status;
                          
                                     //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                            //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');                  
                                    $scope.isLoading = false;
                                });
                        })

                        .finally(function () {
                            $scope.isLoading = false;
                        }); 
                };


                /*$http.get("<?php echo base_url();?>Welcome/index_get")
                    .success(function (response) {
                        $scope.firsttabitem       = response.first;
                        $scope.firsttabinsideitem = {};
                        angular.forEach(response.first, function(value, key){
                            angular.forEach(response.second, function(insidevalue, insidekey){
                                if(value.MyID == insidekey){
                                    $scope.firsttabinsideitem[value.MyID] = insidevalue;
                                }
                            });       
                        });
                        $scope.search   = {};
                        $scope.isLoading = false;
                });*/

                    $scope.salestrendsshowvalues = function(){
                        if(event.target.checked == true){
                            $scope.salestrendsvalues = true;
                        }
                        else{
                            $scope.salestrendsvalues = false;
                        }
                    }

                    $scope.myPnopopupsubclose = function(){
                        $scope.Pnopopupsalestrends = false;
                    }

                    $scope.mysalestrends = function(obj){
                        $scope.isLoading = true;
                        var datacompid = obj.target.attributes.datacompid.value;
                        var datasku    = obj.target.attributes.datasku.value;
                        var dataname   = obj.target.attributes.dataname.value;
                        var datalist   = obj.target.attributes.datalist.value;
                         $http({                     
                            method  : 'POST',
                            url     : '<?php echo base_url();?>index.php/Filters/salestrends',
                            data    : {'datacompid':datacompid,'datasku':datasku,'dataname':dataname,'datalist':datalist},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .success(function(response) {
                            if(response !== 'failure'){
                            $scope.salestrendsdata       = response;
                            $scope.Pnopopupsalestrends = true;
                            $scope.isLoading = false;
                            }
                            else
                            {
                                //console.log('hi');
                               //alert('No Records Found');

                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                         data    : 8
                                        })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('#Pnopopup').css('display','none');
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                  
                                                $scope.isLoading = false;
                                            })
                                                         
                           }
                        })
                            .catch(function (err) {
                                var msg = err.status;
                                 
                                      //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });


                                 //failure msg
                                    $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                      data    : msg
                                    })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
                            .finally(function () {
                            $scope.isLoading = false;
                        });
                    }


                    /*$scope.iamlowest = function (event){
                        console.log(angular.element(document.getElementById('iamlowest')).val());
                    }*/


                $scope.showlowest = function () {

                    if(angular.isUndefined($scope.competitornamefirst)){
                        alert('Select atleast one category');
                        return false;
                    }
                    var competitorcategory = $scope.competitornamefirst.map(function(item){
                        return item.competitornamefirst;
                    });
                    $scope.isLoading = true;
                    $http({                     
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/Filters/iamlowest',
                        data    : {'categoryids':$scope.competitornamefirst,'mypartno':$('#normal_search').val(),'iamlowestfirst':$scope.iamlowest,'iamlowestsecond':$scope.iamnotlowest},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(response) {
                        $scope.isLoading = false;
                        $scope.firsttabitem             = response.first;
                        $scope.firsttabinsideitem3       = response.second;
                        $scope.metricsval   =   $scope.firsttabitem.length;                            

                    }).catch(function (err) {
                        
                          var msg = err.status;
                                      //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });

                        //failure msg
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                    }).finally(function () {
                        $scope.isLoading = false;
                    });
                }


               /* $scope.sortBy = function(propertyName) {
                  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                  $scope.propertyName = propertyName;
                };*/

                 $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                //$scope.groupToPages();
                            };


                $scope.submitmainForm = function() {
                    
                    $('.successupdate').hide();
                    $('.failureupdate').hide();
                    $('.pnum').hide();

                    var partarray ='';
                    angular.forEach($scope.suggest_partno, function(data){       
                     if(data.partno == $('#runpartnumber').val())
                     {
                       partarray = data.partno;
                     } 
                    });  
                    console.log(partarray);
                    if ($('#runpartnumber').val() == '')
                    {
                        partarray ='';
                    }
                    if (partarray.length == 0 && $('#runpartnumber').val() != '')
                     {    
                       console.log('not matched');
                       partarray = '';
                            //return false;
                               $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_configcbr',
                                      data    : 003
                                    })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                        $("#Pnopopup").css("display", "none");                 
                                        $scope.isLoading = false;
                                    });
                      }    

                   
                    //$('#displayitemidhidden td:nth-child(3)');
                    if (partarray.length != 0 || $('#runpartnumber').val()== '')
                    {
                          $scope.isLoading = true;
                      $http({                     
                      method  : 'POST',
                      url     : '<?php echo base_url();?>index.php/Filters/competitor_mypartnumber_update',
                      data    :  angular.element('#popupform').serializeArray(),
                      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                     })
                      .success(function(data) {
                             $scope.isLoading = false;
                             if(data != 'Failure'){
                                $('#runpartnumber').val('');
                                 $(".button_search_tab1").trigger( "click" );
                                $('#updatedpartnumber'+data.itemid).html(data.partnumber);
                                //$('.successupdate').show();
                                         $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                         data    : 3
                                        })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('#Pnopopup').css('display','none');
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                  
                                                $scope.isLoading = false;
                                            })
                             } 
                             else{
                                $('#updatedpartnumber').val();
                                $('.failureupdate').show(); 
                             }
                      })
                        .catch(function (err) {
                              var msg = err.status;
                                      //Error Log To DB
                                 var screenname = '<?php echo $screen;?>';
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                               
                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                         data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                 .success(function(response) {
                                                return true;
                                              });

                            //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('#Pnopopup').css('display','none');
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                            //$scope.isLoading = false;
                            //$('.failureupdate').show();
                      });
                    }
                };


                 $scope.submitsubForm = function() {
                    //$('.successupdate').hide();
                    $('.failureupdate').hide();
                    
                    var partarraysub ='';
                    angular.forEach($scope.suggest_partno, function(data){       
                     if(data.partno == $('#runpartnumbersub').val())
                     {
                       partarraysub = data.partno;
                     } 
                    });  
                    console.log(partarraysub);
                    if ($('#runpartnumbersub').val() == '')
                    {
                        partarraysub ='';
                    }
                    if (partarraysub.length == 0 && $('#runpartnumbersub').val() != '')
                     {    
                       console.log('not matched');
                       partarraysub = '';
                            //return false;
                               $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_partno_map',
                                      data    : 003
                                    })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                        $("#Pnopopupsub").css("display", "none");                 
                                        $scope.isLoading = false;
                                    });
                      }  
                    if (partarraysub.length != 0 || $('#runpartnumbersub').val()== '')
                    {
                     $scope.isLoading = true;
                      $http({                     
                      method  : 'POST',
                      url     : '<?php echo base_url();?>index.php/Filters/competitor_mypartnumber_update',
                      data    :  angular.element('#popupsubform').serializeArray(),
                      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                     })
                      .success(function(data) {
                             $scope.isLoading = false; 
                              if(data != 'Failure'){
                                $('#runpartnumbersub').val('');
                                $(".button_search_tab1").trigger( "click" );
                                $('#updatedsubpartnumber'+data.itemid).html(data.partnumber);
                                //$('.successupdate').show();
                                     $http({                     
                                             method  : 'POST',
                                             url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                             data    : 3
                                            })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('#Pnopopupsub').css('display','none');
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                             } 
                             else{
                                $('#updatedsubpartnumber').val();
                                $('.failureupdate').show(); 
                             }
                      })
                        .catch(function (err) {
                                    var msg = err.status;
                                      //Error Log To DB
                                     var screenname = '<?php echo $screen;?>';
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                                   
                                     $http({                     
                                             method  : 'POST',
                                             url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                             headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                          })
                                     .success(function(response) {
                                        return true;
                                      });

                             
                            //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('#Pnopopupsub').css('display','none');
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                      });
                    }
                };


            /*********Tab2***********/
                $scope.submitForm = function() {
                //if($scope.secondresult == ''){
                    $scope.secondresult = '';
                //}
                var competitorcategory = $scope.competitorcategory.map(function(item){
                    return item.Categoryid;
                });
                $scope.isLoading = true;
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Filters/competitor_category_listing_showitem',
                  data    : {'tabs':'tab2','competitor':$scope.competitorname,'category':competitorcategory},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                     $scope.isLoading = false;
                    $scope.items  = data;
                    if($scope.items.length > 0){
                        $scope.items  = data;
                    }else{
                        $scope.secondresult    = 'No Records found';
                    }

                    $scope.search = {};
                  })
                    .catch(function (err) {
                          //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                          

                        //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                    //$scope.secondresult    = 'Please Try Again'; 
                    // $scope.isLoading = false;
                  })
                   .finally(function () {
                      $scope.isLoading = false;
                    });
                };


                /*********Tab3***********/
                $scope.submitFormtab3 = function() {
                //if($scope.thirdresult == ''){
                    $scope.thirdresult = '';
                //}
                  var competitorcategory = $scope.competitorcategory3.map(function(item){
                    return item.Categoryid;
                 });
                  $scope.isLoading = true;
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Filters/competitor_category_listing_showitem',
                  data    : {'tabs':'tab3','competitor':$scope.competitorname1,'category':competitorcategory},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                     $scope.isLoading = false;
                    $scope.tabitems = data;
                    if($scope.tabitems.length > 0){
                         $scope.tabitems = data;
                    }
                    else{
                         $scope.thirdresult    = 'No Records found';
                    }
                    $scope.search   = {};
                  })
                   .catch(function (err) {
                    var msg = err.status;
                     //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                    //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');                  
                                    $scope.isLoading = false;
                                });
                     //$scope.isLoading = false;
                  })
                   .finally(function () {
                        $scope.isLoading = false;
                    });
                };


                var timeoutInstance;
                $scope.ngcompetitor = function() {
                if(timeoutInstance){
                    clearTimeout(timeoutInstance);
                }
                timeoutInstance = setTimeout(function(){
                    $scope.isLoading = true;
                    $http({                     
                    method  : 'POST',
                    url     : '<?php echo base_url();?>index.php/Filters/competitor_category',
                    data    : {'competitor':$scope.competitorname},
                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    $scope.subcategories = data;
                    $scope.isLoading = false;
                    $timeout(function(){
                        angular.element('#langOpt1').multiselect('reload');
                    },1500);
                  })
                  .catch(function (err) {
                     var msg =err.status;
                       //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                    //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                     //$scope.isLoading = false;
                  });
                },600);
                };


                //search part no in showmyitem window 
              /*  $scope.searchpartno =function() {
                    if($scope.partnofirst == ''){
                        $scope.searchresultload = false;
                        return false;
                    }
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/Welcome/search_partno',
                        data    : {'searchpartno':$scope.partnofirst},
                        header  : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                  .success(function(data) {
                    $scope.searchresultload = true;
                    $scope.searchresult = data;
                  });
                }*/

                // Set value to search box
                $scope.setValue = function(index){
                    $('#normal_search').val(index);  
                    //$scope.searchresultload = true;
                    $('#searchResult_main').hide();  
                 }

                  $scope.partnosearch = function () {
                  $('#searchResult_main').show();  
                                $scope.filteredItems1 = $filter('filter')($scope.suggest_partno, function (item) {
                                      for(var attr in item) {
                                        if(attr == "PartNo") {
                                            if (searchMatch(item[attr], $scope.searchpart.partnofirst)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                };


                //search part no in popup window 
               /* $scope.popupsearchpartno =function() {
                    if($scope.popuppartno == ''){
                        $scope.searchresultload1 = false;
                        return false;
                    }
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/Welcome/search_partno',
                        data    : {'searchpartno':$scope.popuppartno},
                        header  : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                  .success(function(data) {
                    $scope.searchresultload1 = true;
                    $scope.popupsearchresult = data;
                  });
                }*/

                 // Set value to search box
                $scope.popupsetValue = function(index){
                    $('#runpartnumber').val(index);  
                    //$scope.searchresultload = true;
                    $('#searchResult1').hide(); 
                 }

                  $scope.popuppartnosearch = function () {
                  $('#searchResult1').show();  
                                $scope.filteredItems1 = $filter('filter')($scope.suggest_partno, function (item) {
                                      for(var attr in item) {
                                        if(attr == "PartNo") {
                                            if (searchMatch(item[attr], $scope.searchpart.partno)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                };


                  //search part no in subpopup window 
              /*  $scope.subpopupsearchpartno =function() {
                    if($scope.subpopuppartno == ''){
                        $scope.searchresultload2 = false;
                        return false;
                    }
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/Welcome/search_partno',
                        data    : {'searchpartno':$scope.subpopuppartno},
                        header  : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                  .success(function(data) {
                    $scope.searchresultload2 = true;
                    $scope.subpopupsearchresult = data;
                  });
                }*/

                 // Set value to search box
                $scope.subpopupsetValue = function(index){
                    $('#runpartnumbersub').val(index);  
                    //$scope.searchresultload = true;
                    $('#searchResult2').hide();  
                 }

                  $scope.subpopuppartnosearch = function () {
                  $('#searchResult2').show();  
                                $scope.filteredItems1 = $filter('filter')($scope.suggest_partno, function (item) {
                                      for(var attr in item) {
                                        if(attr == "PartNo") {
                                            if (searchMatch(item[attr], $scope.searchpart.partno)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                };

               /* $scope.sortBy = function(propertyName) {
                  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                  $scope.propertyName = propertyName;
                };*/

                var timeoutInstance1;
                $scope.ngcompetitor1 = function() {
                 if(timeoutInstance1){
                    clearTimeout(timeoutInstance1);
                  }
                 timeoutInstance1 = setTimeout(function(){
                    $scope.isLoading=true;
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Filters/competitor_category',
                  data    : {'competitor':$scope.competitorname1},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    
                    $scope.subcategoriestab = data;
                     $scope.isLoading = false;
                         $timeout(function(){
                        angular.element('#langOpt3').multiselect('reload');
                    },1500);
                  })
                   .catch(function (err) {
                     var msg =err.status;
                       //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                    //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                  });
               },600);
                };

                /*$scope.sortBy = function(propertyName) {
                  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                  $scope.propertyName = propertyName;
                };*/


                $scope.exportToExcel=function(tableId){
                    var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
                    $timeout(function(){
                        location.href=exportHref;
                    },100);
                };
            });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.third').addClass('active');
        });
        $(document).on('click','.second',function(){
            $('.second').addClass('active');
            $('.third').removeClass('active');
            $('.first').removeClass('active');
            $('.fourth').removeClass('active');
        });
        $(document).on('click','.first',function(){
            $('.first').addClass('active');
            $('.second').removeClass('active');
            $('.third').removeClass('active');
            $('.fourth').removeClass('active');
            $('.fifth').removeClass('active');
        });
        $(document).on('click','.third',function(){
            $('.first').removeClass('active');
            $('.second').removeClass('active');
            $('.third').addClass('active');
            $('.fourth').removeClass('active');
            $('.fifth').removeClass('active');
        });
        $(document).on('click','.fourth',function(){
            $('.first').removeClass('active');
            $('.second').removeClass('active');
            $('.third').removeClass('active');
            $('.fourth').addClass('active');
            $('.fifth').removeClass('active');
        });
        $(document).on('click','.fifth',function(){
            $('.first').removeClass('active');
            $('.second').removeClass('active');
            $('.third').removeClass('active');
            $('.fourth').removeClass('active');
            $('.fifth').addClass('active');
            $('.fifth').removeClass('active');
        });
        </script>
        <script>
        var articleLimit = 10;
        var currentPage = 1;
        var $currentPage = $('.current-page');
        var $articles = $('.article');
        var totalArticles = $articles.length;
        var totalPages = (totalArticles / articleLimit);

        $articles.hide();
        $currentPage.html(currentPage);
        changePage();
        $('.next').on('click', function(){
            currentPage++;
          if (currentPage > totalPages) {
            currentPage = 1;
          }
          changePage();
        });
        $('.prev').on('click', function(){
            currentPage--;
          if (currentPage < 1) {
            currentPage = totalPages;
          }
          changePage();
        });
        function changePage() {
            $articles.hide();
          $currentPage.html(currentPage);
          $($articles[currentPage * articleLimit - 1]).show();
          $($articles[currentPage * articleLimit - 2]).show();
          $($articles[currentPage * articleLimit - 3]).show();
          $($articles[currentPage * articleLimit - 4]).show();
          $($articles[currentPage * articleLimit - 5]).show();
          $($articles[currentPage * articleLimit - 6]).show();
          $($articles[currentPage * articleLimit - 7]).show();
          $($articles[currentPage * articleLimit - 8]).show();
          $($articles[currentPage * articleLimit - 9]).show();
          $($articles[currentPage * articleLimit - 10]).show();
        }
        </script>
        <script  type="text/javascript">
        $(document).ready(function() {

        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + "-"  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

          $("#btnExport").click(function(e) {
            e.preventDefault();
            $('.expandrow').remove();
            $('.tab1table').remove();
            $('.tab1subtable').remove();
            $('.plusandminus').remove();
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'DIVE_MYITEMS_'+datetime+'.xls';
            a.click();
            location.reload();
          });

        
          $("#btnExport1").click(function(e) {
            e.preventDefault();
            $('.tabtwohide').css('display','block');
            $('.tab2table').remove();
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper1');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            var a = document.createElement('a');
            $('.tab2tablemain').after('<tr class="tab2table"><td><input type="text" class="inputsearch" placeholder="SellerName" ng-model="search.SellerName"></td><td><input type="text" class="inputsearch" placeholder="ITemID" ng-model="search.ITemID"></td><td><input type="text" class="inputsearch" placeholder="SKU" ng-model="search.SKU"></td><td><input type="text" class="inputsearch" placeholder="Title" ng-model="search.Title"></td><td><input type="text" class="inputsearch" placeholder="Category" ng-model="search.Category"></td><td><input type="text" class="inputsearch" placeholder="CurrentPrice" ng-model="search.CurrentPrice"></td><td><input type="text" class="inputsearch" placeholder="QuantitySold" ng-model="search.QuantitySold"></td></tr>');
            a.href = data_type + ', ' + table_html;
            a.download = 'DIVE_MYITEMS_IDONT_SELL'+datetime+'.xls';
            a.click();
          });

          $("#btnExport2").click(function(e) {
            e.preventDefault();
            $('.tabtwohide').css('display','block');
            $('.tab3table').remove();
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper2');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            var a = document.createElement('a');
            $('.tab3tablemain').after('<tr class="tab3table"><td><input type="text" class="inputsearch" placeholder="SellerName" ng-model="search.SellerName"></td><td><input type="text" class="inputsearch" placeholder="ITemID" ng-model="search.ITemID"></td><td><input type="text" class="inputsearch" placeholder="SKU" ng-model="search.SKU"></td><td><input type="text" class="inputsearch" placeholder="Title" ng-model="search.Title"></td><td><input type="text" class="inputsearch" placeholder="Category" ng-model="search.Category"></td><td><input type="text" class="inputsearch" placeholder="CurrentPrice" ng-model="search.CurrentPrice"></td><td><input type="text" class="inputsearch" placeholder="QuantitySold" ng-model="search.QuantitySold"></td></tr>');
            a.href = data_type + ', ' + table_html;
            a.download = 'DIVE_MYITEMS_ALL_'+datetime+'.xls';
            a.click();
            $('.tabtwohide').css('display','none');
          });
        });
        $(document).on('click','.tabonebutton',function(){
            $('#tab4').show();
            $('#tab5').show();
            $('#btnExport1').css('display','none');
            $('#btnExport2').css('display','none');
            $('#btnExport').css('display','block');
        });
        $(document).on('click','.tabtwobutton',function(){
            $('#tab4').hide();
            $('#tab5').hide();
            $('#btnExport1').css('display','block');
            $('#btnExport2').css('display','none');
            $('#btnExport').css('display','none');
        });
        $(document).on('click','.tabthreebutton',function(){
            $('#tab4').hide();
            $('#tab5').hide();
            $('#btnExport1').css('display','none');
            $('#btnExport').css('display','none');
            $('#btnExport2').css('display','block');
        });
    </script>
<script>
    $(document).ready(function() {
        $(document).on('click','.mymodalmaintable',function(){
            var datavalue = $(this).attr("data-value");
            var partno = $(this).attr('my-partnumber');
             $('#searchResult1').hide();
            $('#runpartnumber').val(partno);  
            $("#Pnopopup").css("display", "block");
            $('#displayitemid').text(datavalue);
            $('#displayitemidhidden').val(datavalue);
            $('#mainpartnumber').val(partno);
            //$('#searchResult1').css("display", "none");

        });

        $(document).on('click','.mymodalsubtable',function(){
            var datavalue = $(this).attr("data-value");
            var partno = $(this).attr('my-partnumber');
            var name = $(this).attr('my-competitorname');
             $('#searchResult2').hide();
            $('#runpartnumbersub').val(partno);  
            $("#Pnopopupsub").css("display", "block");
            $('#competitornamesublist').val(name);
            $('#displayitemidsub').text(datavalue);
            $('#displayitemidhiddensub').val(datavalue);
             //$('#searchResult2').css("display", "none");
        });


        $(document).on('click','.close',function(){
          var idvalue = $(this).attr("id");
          $("#Pnopopup").css("display", "none");
          $('.successupdate').hide();
          $('.failureupdate').hide(); 
          $('#runpartnumber').val('');
          $('.pnum').css("display", "none");

        });

        $(document).on('click','.myPnopopupsubclose',function(){
          $("#Pnopopupsub").css("display", "none");
          $('.successupdate').hide();
          $('.failureupdate').hide(); 
          $('#runpartnumbersub').val(''); 
          $('.pnum').css("display", "none");    
        });
            //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
            $(document).on('click','.destroy2',function(){
          $(".err_modal").css("display", "none");
          $("#Pnopopup").css("display", "block");   
        });
              $(document).on('click','.destroy3',function(){
          $(".err_modal").css("display", "none");
          $("#Pnopopupsub").css("display", "block");   
        });
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>
    
</body>
</html>
        