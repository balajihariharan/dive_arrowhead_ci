<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title> <?php echo $title[0]->SellerName;?> : <?php echo $screen;?> </title>

<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
<script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
</head>

<body class="main" ng-controller="postController" ng-cloak>

<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

   <!-- <div class="container head_bg"> -->
  <?php $this->load->view('header.php'); ?>
  
   
<style>
 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
          .pagination ul>.active>a, .pagination ul>.active>span {
    color: #999999;
    cursor: default;
  }
</style> 

<style type="text/css">


.status_bar td:nth-child(7){
  text-align:left;
}

.paging-nav {
  text-align: right;
  padding-top: 2px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #5D85B2;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}
.status_bar th:nth-child(9) {
    min-width: 280px;
    width: 280px;
}
.status_bar th:nth-child(10) {
    min-width: 280px;
    width: 280px;
}

tfoot
{
  text-align: center !important;
  display: table-row-group !important;
}
.price_text {
   width: 16px;
   height: 17px;
   display: inline-block;
   padding: 0 16px;
}
.select {
    padding: 2px;
    float: right;
    width: 155px;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}

.paging-nav,
#MainContent_GV {
 
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }

</style>
  
  <div class="container header_bg_clr">
        <div class="wrapper">
         <div class="head_menu">
            <div class="header_main">
                 <?php echo "<h2>".$screen."</h2>" ;?>
            </div>
            </div>
      </div>
      </div>
               <div id="msg" style="display: none;"></div>

       <div class="container">
        <div class="wrapper">
      <div class="content_main_all">
                             <form>
                             <div class="field">
                             <div class="lable_normal" >
                             Category Details  <span style="color:red; font-size:16px;">*</span>  : 
                             </div>
                             <select  name="category[]"  ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" ng-model="category" id="category" class="select" ng-change="selectWeek()">
                             <option value="">-Select-</option>
                             </select>
                             </div>
                             </form>
                                    <!-- export excel button -->
       
 
    <div class="container">
        <div class="wrapper">
            <div class="form_head">
                    <div class="button_right">
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                              <input type="button" style="margin-right: 22px;" class="export_button" ng-click="exportDataExcel()" value="Export to Excel"/>
                            </a>
                      </li>
        </div>
            </div>

        </div>
        </div>
       
           
                <div class="plan_list_view" style="overflow: auto;">
               
        <!-- onchange="showval();" -->
                        <div class="lable_normal table_top_form">
                         <input id="showvalues" ng-model="checkboxModel.showvalues" ng-click="salestrendsshowvalues()" type="checkbox">
                         Show values</div>

                          <div class="itm_lst" >
                                          <div class="itm_lst">
                                               <p>Items Count:</p>
                                               <img src="<?php echo base_url().'assets/';?>img/list.png">
                                               <p style="font-size: 20px; color: #00886d;">{{declining_total_count}}</p>
                                          </div>
                                           
                          </div>         

                  <table cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
              
                                <th class="curser_pt" ng-click="sortBy('ItemID')">ItemID</th>
                                <th class="curser_pt" ng-click="sortBy('SKU')" >SKU</th>                                        
                                <th class="curser_pt" ng-click="sortBy('mypartno')">My Part#</th>
                                <th class="curser_pt" ng-click="sortBy('Category')">Category</th> 
                                <th ng-click="sortBy('Title')">Title</th>
                                <th class="curser_pt" ng-click="sortBy('quantity')">Quantity </th>
                                <th class="curser_pt" ng-click="sortBy('price')">Price (In $)</th>
                               <!--  <th class="curser_pt" ng-click="sortBy('pricevw1')">5 wk Price (In $)</th> -->
                                <th class="curser_pt" ng-click="sortBy('qtysold_inc')">Qty Sold (Inc)</th>
                                 <th class="curser_pt" ng-click="sortBy('qtysoldvw1')">5 wk Qty.Sold</th>   
                            </tr>

                        </thead>
                         <tfoot>
                          <tr>
                                    <td>
                                      <div class="search"><input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch"></div>
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch">
                                       </div>  
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="partquery" ng-change="mypartsearch()" class="inputsearch">
                                       </div>    
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="Categoryquery" ng-change="Categorysearch()" class="inputsearch">
                                      </div>    
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="titlequery" ng-change="mytitlesearch()" class="inputsearch">
                                      </div>    
                                    </td>
                                    <td></td>                                   
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="pricequery" ng-change="pricesearch()" class="inputsearch">
                                      </div>    
                                    </td>
                                    <td></td>  
                                    
                                    <td>
                                                    <em style="padding: 10px;" title="W-5">W<?php echo date("W")-5; ?></em>
                                                    <em style="padding: 10px;" title="W-4">W<?php echo date("W")-4; ?></em>
                                                    <em style="padding: 10px;" title="W-3">W<?php echo date("W")-3; ?></em>
                                                    <em style="padding: 10px;" title="W-2">W<?php echo date("W")-2; ?></em>
                                                    <em style="padding: 10px;" title="W-1">W<?php echo date("W")-1; ?></em>
                                    </td>
                                  <!--   <td ng-repeat="totsal2 in totalsales">
                                                    <em title="{{totsal2.wk1dt}}">W-5</em>
                                                    <em title="{{totsal2.wk2dt}}">W-5</em>
                                                    <em title="{{totsal2.wk3dt}}">W-5</em>
                                                    <em title="{{totsal2.wk4dt}}">W-5</em>
                                                    <em title="{{totsal2.wk5dt}}">W-5</em>
                                    </td> -->
                         </tr>
                        </tfoot>   
                     <tbody>
                     <!--  <tr ng-repeat="sales in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse"> -->
                      <tr ng-repeat ="sales in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                <td>{{sales.ItemID}}</td>
                                <td>{{sales.SKU}}</td>
                                <td>{{sales.mypartno}}</td>
                                <td>{{sales.Category}}</td>
                                <td>
                                     <a ng-click="redirectToLink($event)" element="{{sales.ItemID}}" style="cursor: pointer;">{{sales.Title}}
                                      </a>
                                </td>

                                <td>{{sales.quantity}}</td>
                                 <td>{{sales.price}}</td>
                                <td>{{sales.qtysold_inc}}</td>

                                <td>
                                <div class="tbl_img">
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == null" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'n'" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'e'" class="price_equal"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'd'" class="price_down"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'u'" class="price_up"></div>
                                        
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == null" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'n'" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'e'" class="price_equal"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'd'" class="price_down"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'u'" class="price_up"></div>
                                        
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == null" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'n'" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'e'" class="price_equal"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'd'" class="price_down"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'u'" class="price_up"></div>
                                        
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == null" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'n'" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'e'" class="price_equal"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'd'" class="price_down"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'u'" class="price_up"></div>
                                        
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == null" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'n'" class="price_wrong"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'e'" class="price_equal"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'd'" class="price_down"></div>
                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'u'" class="price_up"></div>

                                        <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                            <div class="price_text">{{sales.qtysoldvw5}}</div>
                                            <div class="price_text">{{sales.qtysoldvw4}}</div>                              
                                            <div class="price_text">{{sales.qtysoldvw3}}</div>
                                            <div class="price_text">{{sales.qtysoldvw2}}</div>
                                            <div class="price_text">{{sales.qtysoldvw1}}</div>
                                        </div>
                                </div>
                                </td>

                            </tr>
                        </tbody>  
                   </table>
              </div>
                                              

                               <!-- pagination ui part -->
                      <div class="pagination pull-right pagi_master">
                          <ul>
                              <li ng-class="{disabled: currentPage == 0}">
                                  <a href ng-click="prevPage()">« Prev</a>
                              </li>
                              <li ng-repeat="n in pagenos | limitTo:5"
                                  ng-class="{active: n == currentPage}"
                              ng-click="setPage()">
                                  <a href ng-bind="n + 1">1</a>
                              </li>
                              <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                  <a href ng-click="nextPage()">Next »</a>
                              </li>
                          </ul>
                      </div>
            </div>
                               
          </div>
       </div>
       </div>
       </div>


                         <?php $this->load->view('footer.php'); ?>
  </body>
</html>
 <script src= "https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>  


<script>
$(function() {
$('#customWeekPicker').datepick({ 
    renderer: $.datepick.weekOfYearRenderer, 
    calculateWeek: customWeek, 
    firstDay: 1, 
    showOtherMonths: true, 
    showTrigger: '#calImg'
  }); 
 
function customWeek(date) { 
    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1; 
}
});

      $('#txtDate').datepicker({
        showWeek: true,
        firstDay: 1,
       onSelect: function(dateText) {        
           $("#txtWeek").val($.datepicker.iso8601Week(new Date(dateText)));
           $("#selectedDate").val(dateText);
       }
    });

</script>
<script>
        $('#compcat').click(function(e){ 
              if ($('#competitorname').val() != ""){
                  document.getElementById('competitorname').style.borderColor = "green"; 
              }
        });
         $('#customWeekPicker').click(function(e){ 
              if ($('#compcat').val() != ""){
                  document.getElementById('compcat').style.borderColor = "green"; 
              }
        });

$('#btnsearch').click(function(e){ 
     if ($('#competitorname').val() == "" || $('#compcat').val() == "" || $('#customWeekPicker').val() == "") {
                 var competitorname=document.getElementById('competitorname').value;
                   if(competitorname == ""){
                        document.getElementById('competitorname').style.borderColor = "red";
                       // return false;
                    }
                    else
                    {
                      document.getElementById('competitorname').style.borderColor = "green";
                    }

                  var compcat=document.getElementById('compcat').value;
                   if(compcat == ""){
                        document.getElementById('compcat').style.borderColor = "red";
                       // return false;
                    }
                    else
                     {
                           document.getElementById('compcat').style.borderColor = "green";  
                     }
                     var customWeekPicker=document.getElementById('customWeekPicker').value;
                   if(customWeekPicker == ""){
                        document.getElementById('customWeekPicker').style.borderColor = "red";
                        return false;
                    }    
                  
                    e.preventDefault();
                  
     
            }
            else
            {
                document.getElementById('competitorname').style.borderColor = "green";
                document.getElementById('compcat').style.borderColor = "green";  
                 document.getElementById('customWeekPicker').style.borderColor = "green";  
              
            }
        });
  //new popup msg close button
   $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
</script>
<script type="text/javascript">
  $('#btnsearch').click(function(e){ 
               
                    if ($('#competitorname').val() == "" || $('#compcat').val() == "" || $('#customWeekPicker').val() == "") {
                        $("#msg_id").css("display","block");
                        e.preventDefault();
                        return false;
                    }
                    else
                    {
                         $("#msg_id").css("display","none");
                    }
                });
</script>
 <script>
   var sortingOrder = 'ItemID';
   var weekfive =  '<?php echo date("W")-5; ?>';
   var weekfour =  '<?php echo date("W")-4; ?>';
   var weekthree =  '<?php echo date("W")-3; ?>';
   var weektwo =  '<?php echo date("W")-2; ?>';
   var weekone =  '<?php echo date("W")-1; ?>';
   var postApp     = angular.module('postApp', []);
    postApp.controller('postController', function($scope, $http,$timeout,$window,$filter) {

        $scope.sortingOrder = sortingOrder;
        $scope.reverse = false;
        $scope.filteredItems = [];
        $scope.groupedItems = [];
        $scope.itemsPerPage = 25;
        $scope.pagedItems = [];
        $scope.pagenos = [];
        $scope.declining_sales = '';
        $scope.currentPage = 0;
        //$scope.isLoading = true; //Loader Image
                           $http({   

                              method  : 'POST',
                              url     : '<?php echo base_url();?>index.php/Declining_Sales/getcategory',
                              data    : {'data' : '' },
                              headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                             })

                                .success(function(data) {

                                   
                                     $scope.subcategories = data;
                                     console.log($scope.subcategories);
                                      }).catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
                     
                      //$http.get("<?php echo base_url();?>index.php/Declining_Sales/Declining_Sales_detail")
                      $scope.selectWeek = function(){
                      $scope.isLoading = true;
                      var category = $scope.category.Categoryid;
                      console.log($scope.category.Categoryid);
                                  $http({
                                    method: 'POST',
                                    url     : '<?php echo base_url();?>index.php/Declining_Sales/Declining_Sales_detail',
                                    data    : {'category' : $scope.category.Categoryid},
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                      })
                               .success(function (response) {
                                  //console.log(response);
                                   $scope.declining_sales = response;
                                   $scope.declining_total_count =$scope.declining_sales.length;
                                   var exportdata_exc = [];

                   $scope.declining_sales.forEach(function (part) {                     
                            exportdata_exc.push({'ItemID' : part.ItemID,
                                                 'SKU':part.SKU,
                                                 'Title' : part.Title,
                                                 'MyPartNo' : part.mypartno,
                                                 'Category' : part.Category,
                                                 'Quantity' : part.quantity,
                                                 'Price (In $)': part.price,
                                                 'Qty Sold (Inc)':part.qtysold_inc,
                                                 ['Week'+weekfive]:part.qtysoldvw5,
                                                 ['Week'+weekfour]:part.qtysoldvw4,
                                                 ['Week'+weekthree]:part.qtysoldvw3,
                                                 ['Week'+weektwo]:part.qtysoldvw2,
                                                 ['Week'+weekone]:part.qtysoldvw1
                                               });

                        });
                                   $scope.ForExcelExport = exportdata_exc;
                                      //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.declining_sales, function (item) {
                                      for(var attr in item) {
                                        if(attr == "ItemID") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.myskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.declining_sales, function (item) {
                                    for(var attr in item) {
                                    if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.mypartsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.declining_sales, function (item) {
                                    for(var attr in item) {
                                     if(attr == "mypartno") {
                                            if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.Categorysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.declining_sales, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Category") {
                                            if (searchMatch(item[attr], $scope.Categoryquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             $scope.mytitlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.declining_sales, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.pricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.declining_sales, function (item) {
                                    for(var attr in item) {
                                     if(attr == "price") {
                                            if (searchMatch(item[attr], $scope.pricequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                            
                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};
                            $scope.isLoading = false; //Loader Image 

                    $scope.exportDataExcel = function () {
                        var mystyle = {
                            sheetid: 'Declining Sales Reports',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_Declining_Sales_Reports.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    };
                    
                })
                .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })

               $scope.redirectToLink = function (obj) {
                        var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                        $window.open(url, '_blank','width=1200,height=600"');
                    }; 
                    }   

             

    });
</script>
<script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>