<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
             <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
<body class="main" ng-app="myApp" ng-controller="myCtrl" ng-cloak>
    <?php $this->load->view('header.php'); ?>
    <div>
     <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
       
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
            .viewicon{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/edit.png') no-repeat scroll 5px center;
                    padding: 18px 6px 0px 28px;
                    }
            .viewicon2{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/delete_tbl.png') no-repeat scroll 53px center;
                    padding: 18px 6px 0px 28px;
            }
            .usagelimit{
                    margin: 0px 100px 0 0;
            }
            
            .table_main_new {
                width:100%;
                /*margin:50px 200px;*/
            }
            .table_main_new thead{
                background:#EEEEEE;
                
            }
           

            .table_main_new tbody tr td:last-child{
                 text-align: right;   
            }
            .table_main_new tbody tr td{
                color:#3e3e3e;
                font-size:14px;
                text-align:left ;
                text-transform: none;
                line-height:20px;
            }

            .table_main_new_access th:nth-child(1) {
                width:450px;
            }
            .table_main_new_access th:nth-child(2) {
                border-right:none;
            }

           .table_main_new thead tr th {
                    color: #fff;
                    font-size: 15px;
                    background: #2D8BD5;
                    text-align: left;
                    padding: 10px;
                    line-height: 18px;
            }
            .viewicon2 {
                cursor: pointer !important;
                background: rgba(0, 0, 0, 0) url(<?php echo base_url();?>assets/img/delete_tbl.png) no-repeat scroll 42px center;
                padding: 18px 6px 0px 28px;
            }

            .cronrun {
                float: right;
                margin-right: 5px;
                color: #333;
                font-size: 19px;
                padding: 7px 5px 7px 28px;
                background: url(<?php echo base_url();?>assets/img/cal.jpg) no-repeat 5px center #f8f8f8;
                box-shadow: 1px 0px 5px #B5B5B5;
            }
            
            .table_top_form_new {
                   font-size: 18px;
                    width: 230px;
                    height: 20px;
                    padding: 3px 0px 0px 13px;
                    float: left;
                    padding-bottom: 2px;
                    vertical-align: bottom;
                    margin: 20px 0 0px 0px;
            }

            .condition{
                font-size: 19px;
                color: red;
            }

            .refreshimage{
                    padding: 0px 0px 0px 42px;
            }
        </style>
       
                <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="head_menu">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
                    <div class="total_items"><span>CBR Active Competitors : </span>{{ActiveComp}}</div>
                    <div class="total_sales"><span>No. of Competitors : </span>{{TotalComp}}</div>
                    <!-- <div class="cronrun"><span>Upcoming Cron Run Date : </span>
                    <?php 
                            $date = new DateTime();
                            $date->modify('next sunday');
                            echo $date->format('Y-m-d');
                    ?>
                    </div>  -->
                </div>
            </div>
        </div>
        <div class="container">

            <div class="wrapper">
                <div class="content_main_all">
                  <div class="button_right" >
                        <button value="Search" class="button_add" ng-click="newcomp()">Add New Competitor</button>
                  </div>   

                        <div class="content_main">
                        <div style="margin-left: 1px;width: 558px;" id="div1_head" class="lable_normal table_top_form">
                            <input type="radio" id="route1" name="route" ng-click="showroute(0)" checked/>
                            <label for="radio12">Show All</label>
                            <input type="radio" id="route2" name="route" ng-click="showroute(1)"/>
                            <label for="radio11">Show CBR Only</label>
                            <input type="radio" id="route3" name="route" ng-click="showroute(2)"/>
                            <label for="radio12">Show IBR Only</label>
                            <input type="radio" id="active" name="route" ng-click="showactive()"/>
                            <label for="radio12">Show Active Only</label>
                        </div>
                        <div style="margin-left: 1px;width: 700px;background-color: none;" id="div1_head" class="lable_normal table_top_form_new">
                            <span class="condition">*</span>- Listing Data is based on Last Run</span> 
                            <span class="refreshimage"><img ng-click="refreshdata()" style="cursor: pointer;" src="<?php echo base_url();?>assets/img/refresh.png">  Click Refresh to Update Data</span> 
                        </div>

                            <div class="table_main_new">
                                <div class="left_heading">
                                </div>
                                <div id="msg" style="display: none;"></div>
                                <table cellspacing="0" cellpadding="0" border="2" style="width:100%;" cellspacing="0" align="center">
                                    <colgroup span="2"></colgroup>
                                    <colgroup span="2"></colgroup>
                                    <thead>
                                        <tr>
                                            <th class="curser_pt" ng-click="sortBy('CompID')" style="text-align: center; vertical-align: middle;" rowspan="2">Competitor Id #</th>
                                            <th class="curser_pt" ng-click="sortBy('CompetitorName')" style="text-align: center; vertical-align: middle;" rowspan="2">Competitor Name</th>
                                            <th class="curser_pt" ng-click="sortBy('Route')" style="text-align: center; vertical-align: middle;" rowspan="2">Route</th>
                                            <th class="curser_pt" ng-click="sortBy('InceptionDate')" style="text-align: center; vertical-align: middle;" rowspan="2">Inception Date</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Listing<soan class="condition">*</soan></th>
                                            <th class="curser_pt" ng-click="sortBy('FirstListingDate')" style="text-align: center; vertical-align: middle;" rowspan="2">First Listing Date</th>
                                            <th style="text-align: center; vertical-align: middle !important;" rowspan="2">Active</th>
                                            <th style="text-align: center; vertical-align: middle !important;" rowspan="2">Delete</th>
                                        </tr>
                                        <tr>
                                            <th style="text-align: center;" ng-click="sortBy('TotalListing')" class="curser_pt" scope="col">Total Listing</th>
                                            <th style="text-align: center;" ng-click="sortBy('ActiveListing')" class="curser_pt" scope="col">Active Listing</th>
                                        </tr> 
                                        <tr>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text" ng-model="compidquery" ng-change="compidsearch()" class="inputsearch" placeholder="Competitor Id"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="compnamequery" ng-change="compnamesearch()" class="inputsearch" placeholder="Competitior Name"/>
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="inceptiondatequery" ng-change="inceptiondatesearch()" class="inputsearch" placeholder="Inception Date"/>
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="firstlistingdatequery" ng-change="firstlistingdatesearch()" class="inputsearch" placeholder="First Listing Date"/>
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr> 
                                    </thead>

                                    <tbody>
                                    <tr ng-repeat="part in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                        <td style="text-align: center;width: 124px;">{{part.CompID}}</td>
                                        <td>{{part.CompetitorName}}</td>
                                        <td style="text-align: center;">{{part.Route}}</td>
                                        <td style="text-align: center;">{{part.InceptionDate}}</td>
                                        <td style="text-align: center;">{{part.TotalListing}}</td>
                                        <td style="text-align: center;">{{part.ActiveListing}}</td>
                                        <td style="text-align: center;">{{part.FirstListingDate}}</td>
                                        <td ng-if="part.ProcessFlag == 1" style="text-align: center;">
                                            <input type="checkbox" id="{{part.CompID}}" ng-click="savecheckbox(part.CompID)" checked>
                                        </td>
                                        <td ng-if="part.ProcessFlag == 0" style="text-align: center;">
                                            <input type="checkbox" id="{{part.CompID}}" ng-click="savecheckbox(part.CompID)">
                                        </td>
                                       <td ng-click="delete(part.CompID)" class="viewicon2"></td>
                                    </tr>
                                    </tbody>

                                </table>
                                </div>
                                <div class="pagination pull-right pagi_master">
                                    <ul>
                                        <li ng-class="{disabled: currentPage == 0}">
                                            <a href ng-click="prevPage()">« Prev</a>
                                        </li>
                                        <li ng-repeat="n in pagenos | limitTo:5"
                                            ng-class="{active: n == currentPage}"
                                            ng-click="setPage()">
                                            <a href ng-bind="n + 1">1</a>
                                        </li>
                                        <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                            <a href ng-click="nextPage()">Next »</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        </div>
   <?php $this->load->view('footer.php'); ?>
   <div id="Pnopopup2" class="modal">
                            <div class="pop_map_my_part">
                               
                                 <div class="pop_container">
                                            <div class="heading_pop_main">
                                                <h4>
                                                   Add New Competitor #
                                                </h4>
                                                 <span class="close">&times;</span>
                                            </div>
                                    <div class ="pop_pad_all_add_new">
                                    <form id="saveme" ng-submit= "savecomp()">
                                    <div class="channel_main" style="margin: 0px 0px 9px 31px;">
                                        <label class="label_inner_general" >Competitor Name <span style="color:red; font-size:16px;">*</span> </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id="compname"  ng-model="compname" value="" required />
                                        </p>
                                    </div>
 
                                    <div class="save_icon_manage">
                                        <input type="submit"  value="Save" class="button_add_part_save" style="margin: -15px 0px 0px 165px;">
                                    </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
    <script src="<?php  base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

</script>

<script >
 var sortingOrder = 'CompID';
 var app = angular.module('myApp', []);
            app.controller('myCtrl', function($scope,$http,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 20;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.compdata = '';
                $scope.currentPage = 0;
                $scope.route = 0;
                $scope.isLoading = true;
                $http.get("GetData")
                .then(function (response) 
                {
                    $scope.isLoading = false;
                    $scope.compdata = response.data.result;
                    $scope.headers = response.data.header;
                    console.log($scope.compdata);
                    console.log($scope.headers);

                    $scope.TotalComp = $scope.headers[0]['TotalComp'];
                    $scope.ActiveComp = $scope.headers[0]['ActiveComp'];

                                //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.compidsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "CompID") {
                                            if (searchMatch(item[attr], $scope.compidquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.compnamesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                    if(attr == "CompetitorName") {
                                            if (searchMatch(item[attr], $scope.compnamequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.inceptiondatesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "InceptionDate") {
                                            if (searchMatch(item[attr], $scope.inceptiondatequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.firstlistingdatesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "FirstListingDate") {
                                            if (searchMatch(item[attr], $scope.firstlistingdatequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.compidsearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end

                        $scope.showroute = function(route){
                        $scope.route=route;
                        if(route == 0){
                            $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Route") {
                                            if (searchMatch(item[attr], '')  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                            $scope.groupToPages();
                        }
                        else if(route == 1){
                            $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Route") {
                                            if (searchMatch(item[attr], 'CBR')  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                        }
                        else{
                            $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Route") {
                                            if (searchMatch(item[attr], 'IBR')  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                        }
                    };

                    $scope.showactive = function(){
                        $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                            for(var attr in item) {
                             if(attr == "ProcessFlag") {
                                    if (searchMatch(item[attr], 1)  > -1){
                                        return true;
                                    }
                                  }
                                }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };
                });
      
                    $scope.newcomp = function(){
                        $("#Pnopopup2").css("display", "block");
                    };  

                    $scope.refreshdata = function(){
                        $scope.isLoading = true;   

                        $http({                     
                            method  : 'POST',
                            url     : '<?php echo  base_url();?>index.php/ConfigCBR/Refresh',
                            })
                        .success(function(data) {
                            $scope.isLoading = false;
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                    data    : 3,
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'}
                                })
                                .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');  
                                    $("#Pnopopup2").css('display','none');                
                                })
                        })
                        .catch(function(err) {
                            $scope.isLoading = false;
                            var msg =err.status;
                            //Error Log To DB
                             var screenname = '<?php echo $screen;?>';
                             var StatusCode = err.status;
                             var StatusText = err.statusText;
                             var ErrorHtml  = err.data; 
                           
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                data    : msg
                            })
                            .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block'); 
                                $("#Pnopopup2").css('display','none');                 
                            });
                        })
                    };

                    $scope.savecomp = function() {
                        var compname = $('#compname').val();
                        $scope.isLoading = true;   

                        $http({                     
                            method  : 'POST',
                            url     : '<?php echo  base_url();?>index.php/ConfigCBR/Save',
                            data    : {'Compname':compname},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                        .success(function(data) {
                            $scope.isLoading = false;
                            if (data != 0 && data != 1)
                            {
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg_config_cbr',
                                    data    : {'msgstatus' : data,'compname' : compname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'}
                                })
                                .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');  
                                    $("#Pnopopup2").css('display','none');                
                                })
                            }
                            else if(data == 0) 
                            {
                                $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_configcbr',
                                data    : 1
                                })
                                .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    $("#Pnopopup2").css('display','none');                 
                                })
                            }
                            else
                            {
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_configcbr',
                                    data    : 2
                                })
                                .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');  
                                    $("#Pnopopup2").css('display','none');                
                                })
                            }
                        })

                        .catch(function(err) {
                            $scope.isLoading = false;
                            var msg =err.status;
                            //Error Log To DB
                             var screenname = '<?php echo $screen;?>';
                             var StatusCode = err.status;
                             var StatusText = err.statusText;
                             var ErrorHtml  = err.data; 
                           
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                data    : msg
                            })
                            .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block'); 
                                $("#Pnopopup2").css('display','none');                 
                            });
                        })
                    }

                    $scope.savecheckbox = function(compid){
                        var tem= "#"+compid;
                        if ($(tem).prop("checked")==true)
                        { 
                            $key = 1;                            
                        }
                        else
                        {
                            $key = 0;
                        }
                        $scope.isLoading = true;
                        $http({                     
                            method  : 'POST',
                            url     : '<?php echo  base_url();?>index.php/ConfigCBR/Checkbox_Update',
                            data    : {'compid':compid,'key':$key},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .success(function(data) {
                            $scope.isLoading = false;
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                    data    : 6
                            })
                            .success(function(response) {
                                    $scope.isLoading = false;
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');  
                                    $("#Pnopopup2").css('display','none');                
                            })
                        })
                        .catch(function(err) {
                            $scope.isLoading = false;
                            var msg =err.status;
                            //Error Log To DB
                             var screenname = '<?php echo $screen;?>';
                             var StatusCode = err.status;
                             var StatusText = err.statusText;
                             var ErrorHtml  = err.data; 
                           
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                data    : msg
                            })
                            .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block'); 
                                $("#Pnopopup2").css('display','none');                 
                            })
                        })
                    }

                    $scope.delete = function(compid){
                        $scope.isLoading = true;
                        $http({                     
                            method  : 'POST',
                            url     : '<?php echo  base_url();?>index.php/ConfigCBR/Delete',
                            data    : {'compid':compid},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .success(function(data) {
                            $scope.isLoading = false;
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                    data    : 4
                            })
                            .success(function(response) {
                                    $scope.isLoading = false;
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');  
                                    $("#Pnopopup2").css('display','none');                
                            })
                        })
                        .catch(function(err) {
                            $scope.isLoading = false;
                            var msg =err.status;
                              //Error Log To DB
                             var screenname = '<?php echo $screen;?>';
                             var StatusCode = err.status;
                             var StatusText = err.statusText;
                             var ErrorHtml  = err.data; 
                           
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                data    : msg
                            })
                            .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block'); 
                                $("#Pnopopup2").css('display','none');                 
                            })
                        })
                    }
            });
</script>
<script>
    $(document).on('click','.close',function(){
        $("#Pnopopup2").hide(); 
        $("#popup").hide(); 
    });    
 </script>
 <script>
    $(document).on('click','.destroy',function(){
        $(".err_modal").css("display", "none");
        location.reload(true);
    });
     $(document).on('click','.destroy2',function(){
        $(".err_modal").css("display", "none");
    });
</script>
</body>
</html>
