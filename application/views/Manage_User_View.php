<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
             <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
        
    </title>
   
</head>
<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
<body class="main">
    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <div>
      <!--   <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script> -->
        <style type="text/css">
        .edit_type
        {
            text-decoration: none;
            color: #1376AF;
            cursor: pointer;
            background-color: #fff;
            border: 1px solid #ccc;
            height: 27px;
            width: 85px;
            font-size: 14px;
        }
         .delete_type
        {
            text-decoration: none;
            color: #1376AF;
            cursor: pointer;
            background-color: #fff;
            border: 1px solid #ccc;
            height: 27px;
            width: 115px;
            font-size: 14px;
        }
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
             .edit_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 85px;
                font-size: 14px;
            }

             .delete_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 90px;
                font-size: 14px;
            }


            .buttonSave{
                padding: 6px 30px 6px 10px;
                margin: -28% 8% 0% 493%;
                border: none;
                border-radius: 3px;
                cursor: pointer;
                font-family: sans-serif, Verdana, Geneva;
                text-align: right;
                display: inline-block;
                color: #fff;
                vertical-align: top;
                background: url(../img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
                font-size: 13px;

            }
            
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                     <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
                 <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
                
                    <div class="content_main_all">
                        
                        <!--<div class="channel_main">
                            <div class="channel_all">
                                <label>Channel</label>
                                <select class="select_med">
                                    <option>PMT</option>
                                    <option>ebay</option>
                                    <option>Amazon</option>
                                    <option>3dcart</option>
                                    <option>Magento</option>
                                    <option>Big Commerce</option>
                                </select>
                                <span class="amazon_drop"><img src="images/amazon_logo.png"></span>
                            </div>
                           
                        </div>-->

                        <div class="pricing_menu_manage">
                            <ul>
                                <li>
                            <input type="submit" value="Add User" class="button_add rawlistdatasubmit" id ="adduser"/>
                                </li>
                            </ul>
                        </div>
                        <div class="table_main">
                            <table style="width:100%;" border="1" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>User Name</th>
                                        <th>Login ID</th>
                                        <th>Email ID</th>
                                        <th>Role Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php 
                                    echo '<tbody>';
                                    foreach ($id as $value)
                                    {
                                       echo '<tr>
                                        <td>'.$value['userid'].'</td>
                                        <td>'.$value['username'].'</td>
                                        <td>'.$value['loginid'].'</td>
                                        <td>'.$value['emailid'].'</td>
                                        <td>'.$value['rlname'].'</td>
                                        <td><input type="submit" class="edit_type" id="'.$value['userid'].'" value="Edit"/>
                                        <input type="submit" class="delete_type" id="'.$value['userid'].'" value="Delete"/></td>
                                        </tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                 </div>


                     
                        <div id="Pnopopup" class="modal">
                            <div class="pop_map_my_part">
                               
                                 <center><div class="successupdate" style="display:none;">Update Successfully</div></center>
                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                 <div class="pop_container">
                                            <div class="heading_pop_main">
                                                <h4>
                                                    User Updates
                                                </h4>
                                                 <span class="close">&times;</span>
                                            </div>
                                    <div class ="pop_pad_all_add_new">
                                    <div class="channel_main">
                                        <label class="label_inner_general">User ID</label>
                                            <p class="drop_down_med1">
                                                <input type="text" id="userid" class="input_med" name="userid" value="" readonly/>
                                            </p>
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">User Name</label>
                                        <p class="drop_down_med1">
                                            <input class="input_med" id="username" name="username" value="" />
                                        </p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Login ID</label>
                                        <p class="drop_down_med1">
                                            <input class="input_med" id="loginid" name="loginid" value=""  />
                                        </p>
                                        <p class="resutnresponse"></p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Email ID</label>
                                        <p class="drop_down_med1">
                                            <input class="input_med" id="emailid" name="emailid" value="" type="Email" />
                                        </p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Password</label>
                                        <p class="drop_down_med1">
                                            <input class="input_med" id= "password" name= "password"  type ="password" value="" />
                                        </p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Role Name</label>
                                        <p class="drop_down_med1">
                                            <select class="drop_down_med" id="rname" >
                                            <option value="" > - Select - </option>
                                            <?php
                                            foreach ($role as $value) {
                                                echo '<option  value="'.$value['roleid'].'">'.$value['rolename'].'</option>';
                                            }
                                            ?>
                                        </select>
                                        </p>
                                    </div>
                                    <div class="save_icon_manage">
                                        <input type="submit" value="Save" id="save" class="button_add_part_save  rawlistdatasubmit edit ">
                                    </div>
                                </div>
                            </div>

                        </div>
                       
            </div>
        </div>
        
        <footer>
            <div class="container">
                <div class="footer_main">
                    <div class="wrapper">
                        <div class="footer_logo">
                            <img src="<?php echo base_url();?>assets/img/footer_logo_dive.png">
                        </div>
                        <p class="flt_left margin_top_10">
                            <a href="#" id="lnkEmailAddress">Powered By APA Engineering</a>
                            <img src="<?php echo base_url();?>assets/img/apa_logo.png" class="apa_footer">
                            <a>Copyrights &copy; 2017</a>
                        </p>
                        <p class="flt_right margin_top_10">
                            <a><img src="<?php echo base_url();?>assets/img/twitter.png"></a>
                            <a><img src="<?php echo base_url();?>assets/img/facebook.png"></a>
                            <a><img src="<?php echo base_url();?>assets/img/mail_footer.png"></a>
                        </p>
                    </div>
                </div>
            </div>
    </div>
    </footer>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>

    <script>
        $(".edit_type").click(function()
        {
            var id = $(this).attr('id');
            $.ajax({

                type: 'POST',
                url: 'edit',
                data: { id : id },
                success : roleedit
                ,
                error : function(xhr){
                             var msg =xhr.status;
                             var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data : {'xhrstatus':msg},
                            
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
       var roleedit = function (data){
                    var roledata = $.parseJSON(data);
                    $("#Pnopopup").css("display", "block");
                    $("#Pnopopup").css("display", "block");
                    document.getElementById('userid').value = roledata.result.userid; 
                    document.getElementById('username').value = roledata.result.username;
                    document.getElementById('loginid').value = roledata.result.loginid;
                    document.getElementById('emailid').value = roledata.result.emailid;
                    document.getElementById('rname').value = roledata.result.roleid;
        }
</script>
<script>

           $(document).on('click','.edit',function(){

             if ( $('#username').val() == ""||$('#loginid').val() == ""||$('#emailid').val() == ""||$('#password').val() == ""||$('#rname').val() == ""   ) {
                
                        var username=document.getElementById('username').value;
                       if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            /*return false;*/
                             }
                        else {
                           document.getElementById('username').style.borderColor = ""; 
                        }



                        var loginid=document.getElementById('loginid') .value;
                       if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            //return false;
                             }
                         else {
                           document.getElementById('loginid').style.borderColor = ""; 
                        }

                        if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            //return false;
                             }



                        var emailid=document.getElementById('emailid').value;
                       if(emailid == ""){
                            document.getElementById('emailid').style.borderColor = "red";
                            //return false;
                             }
                        else {
                           document.getElementById('emailid').style.borderColor = ""; 
                        }
                         if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            //return false;
                             }
                              if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            //return false;
                             }




                        var password=document.getElementById('password').value;
                       if(password == ""){
                            document.getElementById('password').style.borderColor = "red";
                           // return false;
                             }
                        else {
                           document.getElementById('password').style.borderColor = ""; 
                        } if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            //return false;
                             }
                              if(emailid == ""){
                            document.getElementById('emailid').style.borderColor = "red";
                            //return false;
                             }
                              if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                           // return false;
                             }



                        var rname=document.getElementById('rname').value;
                       if(rname == ""){
                            document.getElementById('rname').style.borderColor = "red";
                            return false;
                             }
                        else {
                           document.getElementById('rname').style.borderColor = ""; 
                        }
                         if(password == ""){
                            document.getElementById('password').style.borderColor = "red";
                            return false;
                             } if(emailid == ""){
                            document.getElementById('emailid').style.borderColor = "red";
                            return false;
                             } if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            return false;
                             } if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            return false;
                             }
                        e.preventDefault();
                }
            else{
                     
                     document.getElementById('username').style.borderColor = ""; 
                     document.getElementById('loginid').style.borderColor = "";
                     document.getElementById('emailid').style.borderColor = "";
                     document.getElementById('password').style.borderColor = "";
                     document.getElementById('rname').style.borderColor = "";
                }
            var uid = $('#userid').val();
            var uname = $('#username').val();
            var lid = $('#loginid').val();
            var eid = $('#emailid').val();
            var pass = $('#password').val();
            var rname = $('#rname').val();
            $.ajax({
                type: 'POST',
                url: 'insert',
                data: { uid : uid, 
                        uname : uname, 
                        lid : lid,
                        eid : eid,        
                        pass : pass,
                        rname : rname},
                success : function (data) {
                    //$('.successupdate').show();
                    //location.reload(true);
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                              data:{'msgstatus':3},
                                success: function(response){
                                     $('#Pnopopup').css('display','none');
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                },
                error : function (xhr){
                     var msg =xhr.status;
                     var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    //$('.failureupdate').show();
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data: {'xhrstatus':msg},
                                success: function(response){
                                     $('#Pnopopup').css('display','none');
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
        });
        });
        
</script>
<script>
        $("#adduser").click(function()
        { 
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: 'adduser',
                data: { id : id },
                success : userid,
                /*error : function(err){
                    alert('Oops Something Wrong');
                }*/
                 error : function(xhr){
                 var msg =xhr.status;
                 var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
       var userid = function (data){
                    var userdata = $.parseJSON(data);
                    $("#popupform").css("display", "block");
                    $("#Pnopopup").css("display", "block");
                    document.getElementById('userid').value = userdata.result.userid; 
                    document.getElementById('username').value = "";
                    document.getElementById('loginid').value = "";
                    document.getElementById('emailid').value = "";
                    document.getElementById('password').value = "";
                    document.getElementById('rname').value = "";
            }
</script>
<script>
        $(".delete_type").click(function()
        {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: 'delete',
                data: { id : id },
                success : function(){
                //alert("Deleted Record Successfully");
                //location.reload(true);
                  $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                             data: {'msgstatus':4},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                },
                error : function(xhr){
                     var msg =xhr.status;
                     var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    //alert('Oops Something Wrong');
                      $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
        /*$(document).on('blur','#loginid',function(){
            var id = $(this).val();
            $.ajax({
                type: 'POST',
                //url: '<?php echo base_url();?>Manage_User/user_exists',
                url: '<?php echo base_url();?>Manage_User/sample',
                data: { loginid : id },
                success : function(data){
                    alert("hi");

                    console.log(data);
                    //if(data == "success"){
                       // $('.resutnresponse').html('<span>User Already Exists</span>');
                    //}
                },
                error : function(err){
                    alert('Please Enter correct Data');
                }
            });
        });*/
</script>
<script>
    $(document).on('click','.close',function(){
          $("#Pnopopup").hide(); 
        });    

     //new popup msg close button
   $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            location.reload(true);
        });
</script>
</body>
</html>       
            