<!-- error handling popup-->
<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<body class="main" ng-controller="postController" ng-cloak>
    <?php $this->load->view('header.php'); ?>
    <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
      
    <style type="text/css">
     .pagi_master li{
                    list-style-type: none;
        display: inline-block;
        margin: 0 3px;
        background: #5D85B2;
        color: #fff;
        
        border-radius: 3px;
        cursor: pointer;
     }

     .pagi_master li a{
        color: #fff;
        text-decoration: none;
        padding: 4px;
        }
        .pagination ul>.active>a, .pagination ul>.active>span {
        color: #999999;
        cursor: default;
        }
        .alpha{
            "background-color" : "lightblue";
        }

        .tooltip .tooltiptext {
            visibility: hidden;
            width: auto;
            background-color: #2C3E50;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
        }
        div .ms-options{
            width:280%;
        }

        .slimtable-page-btn {
            display: inline-block;
            margin: 0 3px;
            background: #5D85B2;
            color: #fff;
            padding: 0 4px;
            border-radius: 3px;
            cursor:pointer;
        }
        .slimtable-page-btn:hover {
            background:#187ed5; 
        }
        .slimtable-page-btn.active {
            background:#187ed5; 
        }
        .slimtable-paging-btnsdiv{
            float:right;
        }

        ul,li { margin:0; padding:0; list-style:none;}
        .label { color:#000; font-size:16px;}
        .field1{
            display: inline-block;
            float: left;
            padding: 0 10px 0 0px;
        }
        .shwpopup
        {
            display:none;
            width: 50%;
            margin: 50%;
            position: fixed;
            z-index: 1;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.4);
            left: 0;
            top: 0;
        }
        .field{
            width:auto;
        }
        .ckeckclass
        {
            margin:2px;
        }
        .divTable{
            float: left;
            width: 70px;
            height: 40px;
            border-right: 1px solid #e1e1e1;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .lastdivTable{
            float: left;
            width: 70px;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .paging-nav {
          text-align: right;
          padding-top: 2px;
        }

        .paging-nav a {
          margin: auto 1px;
          text-decoration: none;
          display: inline-block;
          padding: 1px 7px;
          background: #5D85B2;
          color: white;
          border-radius: 3px;
        }

        .paging-nav .selected-page {
          background: #187ed5;
          font-weight: bold;
        }

        tfoot
        {
        text-align: center !important;
        display: table-row-group !important;
        }

        .paging-nav,
        #tblrawresult {
         
          margin: 0 auto;
          font-family: Arial, sans-serif;
        }

        .select {
            padding: 2px;
            float: right;
            width: 155px;
            border: 1px solid #bebcbc;
            border-radius: 2px;
            font-size: 12px;
            height: 29px !important;
            margin-bottom: 10px;
            color: #505050;
            background-color: #fff;
            font-family: sans-serif, Verdana, Geneva !important;
        }

        .selectpage {
            padding: 2px;
            width: 76;
            border: 1px solid #bebcbc;
            border-radius: 2px;
            font-size: 12px;
            height: 29px !important;
            margin-bottom: 10px;
            color: #505050;
            background-color: #fff;
            font-family: sans-serif, Verdana, Geneva !important;
        }



        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 10% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 30%; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        .close {
            color: #fff;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #2d8bd5;
            text-decoration: none;
            cursor: pointer;
        }
        .itemIDCell
        {
            margin:0;
        }

        @media only screen and (max-width: 1500px) {
            
        }

        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                  display: none !important;
                }



        /**********autocomplete suggestion***************/
        #searchResult{
           border: 1px solid #aaa;
            overflow-y: auto;
            width: 172px;
            position: absolute;
            margin-left: 143px;
            padding-bottom: 0px;
            min-height: 10px;
            max-height: 150px;
        }

        #searchResult li{
         background: white;
         padding: 4px;
        cursor: pointer;
            list-style: none;
            border-bottom: 1px solid #ededed;
        }

        #searchResult li:hover{
            background: #f1f1f1;
        }
    </style>
        <div class="container header_bg_clr posi_rela">
            <div class="wrapper">
                <div class="header_main">
                    <div class="head_menu">
                 <?php echo "<h2>".$screen."</h2>" ;?><!-- <span style="padding-left:40%; color:red; font-size:14px"></span> -->
                    </div>
                </div>
            </div>
        </div>  
        <div id="msg" style="display: none;"></div>
                 
        </div> 
        

<div class="container">
 <div class="wrapper">
   <div class="content_main_all">
     <div class="itm_lst">
       <p>Auto Rawlist Search Count:</p>
       <p style="font-size: 20px; color: #00886d;">{{totalcount}}</p>
     </div>
       <div class="form_head">
         <div class="field">
           <div class="lable_normal" >
               Category Details  <span style="color:red; font-size:16px;">*</span>  : 
           </div>
             <select  name="category[]"  ng-options="::subcat.subCategory for subcat in subcategories track by ::subcat.Categoryid" ng-model="competitorcategory" id="compcat" class="select"  ng-change ="getcategorydetails()">
             <option value="">-None-</option>
             </select>    
           </div><br/>
            <div class="plan_list_view">
              <div class="clear"></div>
                 <div class="lable_normal table_top_form"><input id="ckbCheckAll" type="checkbox" ng-click="globalcheck()" ng-model="haschecked" />
                    Select All - Across Pages</div>
                      <table cellspacing="0" border="1" class="table price_smart_grid_new">
                         <thead>
                            <tr>
                                <th rowspan="2" style=" width: 2%;">Approve</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('ItemID')" style="width: 6%;"><center>ItemID</center></th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('SKU')" style=" width: 6%;"><center>SKU</center></th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('Title')" style="width: 40%"><center>Title</center></th>
                                <th colspan="5" ><center>Price (in $)</center></th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('Profit')" style=" width: 6%;">Projected Profit (in $)</th>
                                <th rowspan="2" style=" width: 6%;"><center>Action</center></th>
                            </tr>
                            <tr>
                                <th class="curser_pt" ng-click="sortBy('CurrentPrice')" style="background-color: #2d8bd5;width: 6%;">Current</th> 
                                <th class="curser_pt" ng-click="sortBy('costprice')" style="background-color: #2d8bd5;width: 6%;">Cost</th>
                                <th class="curser_pt" ng-click="sortBy('freight')" style="background-color: #2d8bd5;width: 6%;">Freight</th>
                                <th class="curser_pt" ng-click="sortBy('CompLowPrice')" style="background-color: #2d8bd5;width: 6%;">Comp Lowest</th>
                                <th class="curser_pt" ng-click="sortBy('RecommendedPrice')" style="background-color: #2d8bd5;width: 4%;">Recommended</th>
                            </tr>
                            <tr>
                              <td>
                                 <input id="selectall" type="checkbox"  ng-click="selectallfunc()"  ng-model="selectall[currentPage]" class="ckeckclass" />
                               </td>
                               <td>
                                  <div class="search">
                                     <input class="inputsearch" type="text" ng-model="ItemID" ng-change="mysearch()" class="inputsearch" placeholder="ItemID"/>
                                  </div>
                               </td>
                               <td>
                                  <div class="search">
                                     <input class="inputsearch" type="text"  ng-model="SKU" ng-change="SKUsearch()" class="inputsearch" placeholder="SKU"/>
                                  </div>
                              </td>
                              <td>
                                 <div class="search">
                                     <input class="inputsearch" type="text"  ng-model="Title" ng-change="Titlesearch()" class="inputsearch" placeholder="Title"/>
                                  </div>
                              </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr> 
                        </thead>
                         <tbody>
                             <tr ng-repeat="part in pagedItems[currentPage] |  orderBy:sortKey:reverse | filter:search ">
                                 <!--  <tr ng-repeat="part in compdata"> -->
                                 <td id="{{part.ItemID}}">
                                     <input type="checkbox" element="{{part.ItemID}}" ng-checked="selectall[currentPage] || selectlist[part.ItemID] || checked ||part.ApproveFlag == 1" id="{{part.ItemID}}" ng-model="selectlist[part.ItemID]" ng-click="selectfunc($event)">
                                 </td>
                                 <td class="tooltip">{{part.ItemID}}<span class="tooltiptext ">Heavy Duty Starter Fits Hitachi Excavator EX400</span>
                                 </td>
                                 <td class="tooltip">{{part.SKU}}<span class="tooltiptext ">Heavy Duty Starter Fits Hitachi Excavator EX400</span></td>
                                 <td><a ng-click="redirectToLink($event)" element="{{part.ItemID}}" style="cursor: pointer;" class="">{{part.Title}}</a></td>
                                 <td>{{part.CurrentPrice}}</td>
                                <td>{{part.costprice}}</td>
                                <td>{{part.freight}}</td>
                                <td><a href="http://www.ebay.com/itm/{{part.CompItemID}}" target="_blank">{{part.CompLowPrice}}</a></td>
                                <td><input style=" text-align: center; font-weight: bold; font-size: 15px;" type="text" class="input_table editable_input_blck" ng-model="part.RecommendedPrice" ng-click="change(part.ItemID)"/></td>
                                <td>{{part.Profit}}</td>
                                <td ng-click="redirectToLink_more($event)"><a style="cursor: pointer;" class=""><img  element="{{part.ItemID}}" style="padding-left:35%" src="<?php echo base_url();?>/assets/img/eye_icon.png"></td>
                            </tr>
                        </tbody>
                        </table>
                           <div style="text-align:center;">
                                <input class="button_add" value="Save/Approve"  ng-click="save()" type="submit" style="margin: 14px 10px 0 0;"/>
                           </div>
                           </div>
                           </div>
                             <div class="pagination pull-right pagi_master">
                                    <ul>
                                        <li ng-class="{disabled: currentPage == 0}">
                                            <a href ng-click="prevPage()">« Prev</a>
                                        </li>
                                        <li ng-repeat="n in pagenos | limitTo:5"
                                            ng-class="{active: n == currentPage}"
                                            ng-click="setPage()">
                                            <a href ng-bind="n + 1">1</a>
                                        </li>
                                        <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                            <a href ng-click="nextPage()">Next »</a>
                                        </li>
                                    </ul>
                              </div>
                        </div>
                     </div>
                </div> 
            </div>
     </div>
</div>
<?php $this->load->view('footer.php'); ?>

<script>
            var sortingOrder = 'ItemID';
            var postApp      = angular.module('postApp', []);
            var screenname   = '<?php echo $screen;?>';

            postApp.controller('postController', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder  = sortingOrder;
                $scope.reverse       = false;
                $scope.filteredItems = [];
                $scope.groupedItems  = [];
                $scope.itemsPerPage  = 5;
                $scope.pagedItems    = [];
                $scope.pagenos       = [];
                $scope.compdata      = '';
                $scope.selectlist    = [];
                $scope.finaldata     = [];
                $scope.currentPage   = 0;
                $scope.route         = 0;
                $scope.isLoading     = true;

                                 $http.get("<?php echo base_url();?>index.php/AutoIBR/competitor_category")
                               .success(function (response) {
                                    $scope.subcategories = response;
                                    $scope.isLoading = false;
                               })
                
                //Click the button to open an new browser window that is 1200px wide and 600px tall
                $scope.redirectToLink = function(obj){
                var url ="http://www.ebay.com/itm/" +obj.target.attributes.element.value;
                $window.open(url,'_blank','width=1200,height=600')
                };

                $scope.redirectToLink_more = function(obj){
                    var ItemID =obj.target.attributes.element.value;
                 $http({
                        method:'post',
                        url:'<?php echo base_url();?>index.php/AutoIBR/gotoebay',
                        data:{'ItemID':ItemID},
                        headers:{'content-type': 'application/x-www-form-urlencoded'}
                 })
                  .success(function(data) {
                                    $scope.isLoading = false;
                                    var url = data;
                                    $window.open (url,'_blank','width=1200,height=600')
                    })
                   .catch(function (err) {
                                        var msg =err.status;
                                         //Error Log To DB
                                         var screenname = '<?php echo $screen;?>';
                                         var StatusCode = err.status;
                                         var StatusText = err.statusText;
                                         var ErrorHtml  = err.data; 
                                       
                                         $http({                     
                                                 method  : 'POST',
                                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                              })
                                         .success(function(response) {
                                                        return true;
                                                      });
                                        //failure msg
                                        $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                          data    : msg
                                        })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                  
                                                $scope.isLoading = false;
                                            });
                                        });
                }

                $scope.getcategorydetails= function()
                {   
                    $scope.compdata   =''; 
                    $scope.pagedItems = '';
                    $scope.isLoading  = true;
                    var Categoryid    = $scope.competitorcategory.Categoryid;
                     $http({
                        method:'post',
                        url:'<?php echo base_url();?>index.php/AutoIBR/getcategorydetails',
                        data:{'Categoryid':Categoryid},
                        headers:{'content-type': 'application/x-www-form-urlencoded'}
                     })
                    .success(function(data) {
                            $scope.isLoading  = false;
                            $scope.compdata   = data.result;
                            $scope.totalcount = $scope.compdata.length;
                            $scope.compdata.forEach(function(part){
                                part.flag='N';
                            });
                            $scope.compdata.forEach(function(part){
                                if(part.ApproveFlag == 1)
                                {
                                  $scope.selectlist[part.ItemID] = true;
                                }
                            });
                                                   //pagination part start 
                        // *********************

                             $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch  = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "ItemID") {
                                            if (searchMatch(item[attr], $scope.ItemID)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                            $scope.SKUsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                    if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.SKU)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              
                           $scope.Titlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.Title)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                            //CurrentPricesearch
                          $scope.CurrentPricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.CurrentPrice)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                     
                         
                            // *****************finish the filtered items*****************
                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.compdata.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                           //pagination part end
                            $scope.search = {};
                            $scope.showLoader = false; //Loader Image

                          
                           //global check for all checkboxes function 
                           $scope.globalcheck = function(){
                                    if($scope.haschecked){
                                            var length = $scope.pagedItems.length;
                                            $scope.compdata.forEach(function (part) {
                                                $scope.selectlist[part.ItemID] = true;
                                            });
                                            for (var i=0; i<=length ; i++)
                                            {
                                                $scope.selectall[i] = false;
                                            }
                                     }
                                     else{
                                            var length = $scope.pagedItems.length;
                                            // $scope.selectlist='';
                                            $scope.compdata.forEach(function (part) {
                                                $scope.selectlist[part.ItemID] = false;
                                            });
                                            for (var i=0; i<=length ; i++)
                                            {
                                                $scope.selectall[i] = false;
                                            }
                                        }
                                    //$('#selpage').prop('checked',false);
                                }
                           var selectallpg = [];
                           $scope.selectallfunc = function(){
                                    if($("#selectall").is(":checked"))
                                    {
                                        angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                                            $scope.selectlist[value.ItemID] = true;
                                            selectallfun1(value.ItemID);
                                        });
                                    }
                                    else{
                                        angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                                            $scope.selectlist[value.ItemID] = false;
                                            selectallfun2(value.ItemID);
                                        });
                                    }
                                    if (selectallpg[$scope.currentPage] != $scope.currentPage)
                                    {
                                        selectallpg[$scope.currentPage] = $scope.currentPage;
                                    }
                                    else
                                    {
                                         selectallpg[$scope.currentPage] = $scope.currentPage;
                                    }
                                    
                           }

                           var selectallfun1 = function(id){
                                if (dummy[id] == id)
                                {
                                    delete dummy[id];
                                    dummy[id]=id ;
                                }
                                else
                                {
                                    dummy[id]=id ;
                                }
                                var temp = "";
                                for (var i in dummy) {
                                    temp += dummy[i] + ", ";
                                }
                               
                             }

                            var selectallfun2 = function(id){
                                delete dummy[id];
                                var temp = "";
                                for (var i in dummy) {
                                       temp += dummy[i] + ", ";
                                }
                               
                            };
                            //single checkbox select
                             var dummy = [];
                            $scope.selectfunc = function(obj){
                                $('#selectall').prop('checked',false);
                                $('#ckbCheckAll').prop('checked',false);
                                var id = obj.target.attributes.element.value;
                                if ($scope.selectlist[id])
                                {
                                    dummy[id]=id ; 
                                }
                                else
                                {
                                   delete dummy[id];
                                }
                                var temp = "";
                                for (var i in dummy) {
                                       temp += dummy[i] + ", ";
                                }
                                                
                            }
                            //flag update
                            $scope.change = function(index){
                                //return false;
                                $scope.compdata.forEach(function (part) {
                                        if (part.ItemID == index)
                                        {
                                            part.flag='U';
                                        }                                  
                                    });
                            };
                           //
                          $scope.$watch('compdata', function(newvalue,oldvalue) {
                            //console.log(newvalue);
                            $scope.finaldata = newvalue;
                          }, true);


                          $scope.save = function(){
                                /*console.log($scope.selectlist['270820681374']);
                                console.log(typeof($scope.selectlist));*/
                                var finaldata1=[];
                                
                                $scope.finaldata.forEach(function (part) {
                                        if ($scope.selectlist[part.ItemID] == true)
                                        {    part.flag ='N';
                                            finaldata1.push({'ItemID': part.ItemID,'RecommendedPrice':part.RecommendedPrice,'ApproveFlag':1});    
                                        }
                                        
                                });
                                 $scope.finaldata.forEach(function (part) {
                                        if ($scope.selectlist[part.ItemID] == false)
                                        {    part.flag ='N';
                                            finaldata1.push({'ItemID': part.ItemID,'RecommendedPrice':part.RecommendedPrice,'ApproveFlag':0});    
                                        }
                                        
                                });
                                $scope.finaldata.forEach(function (part) {
                                    if (part.flag=='U')
                                    {
                                        finaldata1.push({'ItemID': part.ItemID,'RecommendedPrice':part.RecommendedPrice,'ApproveFlag':0});    
                                    }
                                });
                                console.log(finaldata1);
                         
                                    $http({                     
                                        method  : 'POST',
                                        url     : '<?php echo base_url();?>index.php/AutoIBR/save',
                                        data    : { 'data' : finaldata1},
                                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                    })
                                    .success(function(data) {
                                        $scope.isLoading = false;
                                            //alert("Successfully Updated");
                                                        $http({                     
                                                             method  : 'POST',
                                                             url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                             data    : 3
                                                            })
                                                         .success(function(response) {
                                                            $('#msg').html(response);
                                                            $('.err_modal').css('display','block');
                                                            $('#msg').css('display','block');                  
                                                            $scope.isLoading = false;
                                                        })
                                            // location.reload(true);
                                        })
                                    .catch(function (err) {
                                        var msg =err.status;
                                         //Error Log To DB
                                         var screenname = '<?php echo $screen;?>';
                                         var StatusCode = err.status;
                                         var StatusText = err.statusText;
                                         var ErrorHtml  = err.data; 
                                       
                                         $http({                     
                                                 method  : 'POST',
                                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                              })
                                         .success(function(response) {
                                                        return true;
                                                      });
                                        //failure msg
                                        $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                          data    : msg
                                        })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                  
                                                $scope.isLoading = false;
                                            });
                                        });
                            };

                   })

                     .catch(function(err) {
                                $scope.isLoading = false;
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                        $("#Pnopopup2").css('display','none');                 
                                        
                                    });
                                })

                    }

});
 </script>
 <script>
    $(document).on('click','.close',function(){
          $("#Pnopopup2").hide(); 
           $("#popup").hide(); 
         //location.reload(true); 
        });    
 </script>
 <script>
          //new popup msg close button
           $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
          //location.reload(true);
      });
</script>

    