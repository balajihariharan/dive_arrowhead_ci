
<div class="plan_list_view" id="div2" style="overflow: auto;">
            <div class="wrapper">
                    <div class="content_main_all">
                    <div class="display_inline_blk">
                        <span class="week">Week : {{week}}</span>
                        <span class="week">Competitor Name : {{compname}}</span>
                        </div>
                        <p class="drop_down_med1">
                                <!-- <img src="<?php echo base_url();?>/assets/img/back.jpg"> -->
                                <input type="button" class="back_button" ng-click="Back1()" value="Back"/>
                        </p>
                        <div class="button_right">       
                                <input type="button" class="export_buttonnew" value="Export As Excel" ng-click="exportDataDrill1()"/>
                        </div>
                        <br><br>
                        <div style="margin-left: 1px;width: 431px;" id="div2_head" class="lable_normal table_top_form">
                            <input type="radio" id="radio_lvl2_11" name="category_lvl2" ng-click="showcat(0)" checked/>
                            <label for="radio_lvl2_11">Market Share - Master List</label>
                            <input type="radio" id="radio_lvl2_12" name="category_lvl2" ng-click="showcat(1)"/>
                            <label for="radio_lvl2_12">Market Share - Competitor</label>
                        </div>
                        <div class="table_main_new"  id="exportable2">
                                <table style="width:100%;" border="2" cellpadding="0" cellspacing="0" align="center">
                                    <col>
                                          <colgroup span="2"></colgroup>
                                          <colgroup span="2"></colgroup>
                                              <thead>
                                          <tr>
                                            <th class="curser_pt" style="text-align: center; vertical-align: middle !important;" rowspan="2" ng-click="sortBy('Category')">Category</th>
                                            <th class="curser_pt" style="text-align: center; vertical-align: middle !important;" rowspan="2" ng-click="sortBy('MappedItems')">Items<br>Mapped</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">7 Days Sales (In $)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">7 Days % Of Total (In %)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Listing</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Sold [7 days]</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">% of Listing Sold (In %)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Avg. Sale Price (In $)</th>
                                          </tr>
                                          <tr>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDaysSales')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDaysSales_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDAYPercentOFTOTAL')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDAYPercentOFTOTAL_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('Listing')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('Listing_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SoldSevendays')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SoldSevendays_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('PercentofListingSold')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('PercentofListingSold_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('AvgSalePrice')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('AvgSalePrice_lw')">Week {{week-1}}</th>
                                      </tr>
                                      </thead>
                                <tbody>
                                <!-- id="myid_{{item.CategoryId}}" -->
                                <tr ng-repeat="item in reporttwodata | orderBy:sortKey:reverse">
                                    <td class="curser_pt_new tooltip" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}" scope="row" ng-click="ReportThree($event)" data-id="{{item.CategoryId}}" data-value="{{item.Category}}">
                                    <img src="<?php echo base_url();?>/assets/img/eye_icon.png">&nbsp;&nbsp;&nbsp;&nbsp;{{item.Category}}
                                    <br><span class="tooltiptext">({{item.SubCategory}})</span>
                                    </td>
                                    <td style="text-align: center;">{{item.MappedItems}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDaysSales | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDaysSales_lw | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDAYPercentOFTOTAL | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDAYPercentOFTOTAL_lw | currency:''}}</td>                                   
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.Listing}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.Listing_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SoldSevendays}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SoldSevendays_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.PercentofListingSold | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.PercentofListingSold_lw | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.AvgSalePrice}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.AvgSalePrice_lw}}</td>
                                </tr>
                                </tbody>
                            </table><br><br>
                            <div class="display_inline_blk align_center">
                                <p ng-if="reporttwodata.length == 0" class="nodata">No Data</p>
                            </div>
                        </div>
                    </div>
            </div>
        </div>