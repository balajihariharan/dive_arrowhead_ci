<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
             <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<style type="text/css">
    .linkbuttoncss {
    -webkit-transition-duration: 0.4s; 
    transition-duration: 0.4s;
    color: white;
    background-color: cadetblue;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
    font-family: verdana;
    border-radius: 10px;
 }

.linkbuttoncss:hover {
    background-color: green;
    color: white;
}
</style>
<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
<body class="main">
    <?php $this->load->view('header.php'); ?>
    <div>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                     <?php echo "<h2>".$screen."</h2>" ;?>
                </div>
                 <div class="pricing_menu_manage amazonlogodiv">
                    <img class="amazonlogo" src="<?php echo base_url().'assets/';?>img/amz1.png"/>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="wrapper">
                    <div class="content_main_all">

                    <div style="color:#2C3E50;font-weight: bold;">
                                    <div class="channel_main">
                                        <label class="label_inner_general">Seller ID</label>
                                            <p class="drop_down_med1">
                                                <input type="text" id="sellerid" size="45" class="" name="sellerid" value=""/>
                                            </p>
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Access Key ID</label>
                                        <p class="drop_down_med1">
                                            <input type="text" class="" size="45" id="awskeyid" name="awskeyid" value="" />
                                        </p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">MWSAuthToken</label>
                                        <p class="drop_down_med1">
                                            <input class="" size="45" id="authtoken" name="authtoken" value=""  />
                                        </p>
                                        
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Market Place ID</label>
                                        <p class="drop_down_med1">
                                            <input class="" size="45" id="mplaceid" name="mplaceid" value="" type="text" />
                                        </p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general">Secret Key</label>
                                        <p class="drop_down_med1">
                                            <input class="" size="45" id="mplaceid" name="mplaceid" value="" type="text" />
                                        </p>
                                    </div>
                                  <div style="margin-left: 8%;margin-top: 1%;">
                                    <label class="linkbuttoncss">
                                        <img style="margin-right: 5px;" src="<?php echo base_url();?>assets/img/sync.png"/>Link Account
                                    </label>
                                </div>
            </div>
        </div>
        
        <footer>
            <div class="container">
                <div class="footer_main">
                    <div class="wrapper">
                        <div class="footer_logo">
                            <img src="<?php echo base_url();?>assets/img/footer_logo_dive.png">
                        </div>
                        <p class="flt_left margin_top_10">
                            <a href="#" id="lnkEmailAddress">Powered By APA Engineering</a>
                            <img src="<?php echo base_url();?>assets/img/apa_logo.png" class="apa_footer">
                            <a>Copyrights &copy; 2017</a>
                        </p>
                        <p class="flt_right margin_top_10">
                            <a><img src="<?php echo base_url();?>assets/img/twitter.png"></a>
                            <a><img src="<?php echo base_url();?>assets/img/facebook.png"></a>
                            <a><img src="<?php echo base_url();?>assets/img/mail_footer.png"></a>
                        </p>
                    </div>
                </div>
            </div>
    </div>
    </footer>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>

    <script>
        $(".edit_type").click(function()
        {
            var id = $(this).attr('id');
            $.ajax({

                type: 'POST',
                url: 'edit',
                data: { id : id },
                success : roleedit
                ,
                error : function(xhr){
                             var msg =xhr.status;
                             var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data : {'xhrstatus':msg},
                            
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
       var roleedit = function (data){
                    var roledata = $.parseJSON(data);
                    $("#Pnopopup").css("display", "block");
                    $("#Pnopopup").css("display", "block");
                    document.getElementById('userid').value = roledata.result.userid; 
                    document.getElementById('username').value = roledata.result.username;
                    document.getElementById('loginid').value = roledata.result.loginid;
                    document.getElementById('emailid').value = roledata.result.emailid;
                    document.getElementById('rname').value = roledata.result.roleid;
        }
</script>
<script>

           $(document).on('click','.edit',function(){

             if ( $('#username').val() == ""||$('#loginid').val() == ""||$('#emailid').val() == ""||$('#password').val() == ""||$('#rname').val() == ""   ) {
                
                        var username=document.getElementById('username').value;
                       if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            /*return false;*/
                             }
                        else {
                           document.getElementById('username').style.borderColor = ""; 
                        }



                        var loginid=document.getElementById('loginid') .value;
                       if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            //return false;
                             }
                         else {
                           document.getElementById('loginid').style.borderColor = ""; 
                        }

                        if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            //return false;
                             }



                        var emailid=document.getElementById('emailid').value;
                       if(emailid == ""){
                            document.getElementById('emailid').style.borderColor = "red";
                            //return false;
                             }
                        else {
                           document.getElementById('emailid').style.borderColor = ""; 
                        }
                         if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            //return false;
                             }
                              if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            //return false;
                             }




                        var password=document.getElementById('password').value;
                       if(password == ""){
                            document.getElementById('password').style.borderColor = "red";
                           // return false;
                             }
                        else {
                           document.getElementById('password').style.borderColor = ""; 
                        } if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            //return false;
                             }
                              if(emailid == ""){
                            document.getElementById('emailid').style.borderColor = "red";
                            //return false;
                             }
                              if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                           // return false;
                             }



                        var rname=document.getElementById('rname').value;
                       if(rname == ""){
                            document.getElementById('rname').style.borderColor = "red";
                            return false;
                             }
                        else {
                           document.getElementById('rname').style.borderColor = ""; 
                        }
                         if(password == ""){
                            document.getElementById('password').style.borderColor = "red";
                            return false;
                             } if(emailid == ""){
                            document.getElementById('emailid').style.borderColor = "red";
                            return false;
                             } if(loginid == ""){
                            document.getElementById('loginid').style.borderColor = "red";
                            return false;
                             } if(username == ""){
                            document.getElementById('username').style.borderColor = "red";
                            return false;
                             }
                        e.preventDefault();
                }
            else{
                     
                     document.getElementById('username').style.borderColor = ""; 
                     document.getElementById('loginid').style.borderColor = "";
                     document.getElementById('emailid').style.borderColor = "";
                     document.getElementById('password').style.borderColor = "";
                     document.getElementById('rname').style.borderColor = "";
                }
            var uid = $('#userid').val();
            var uname = $('#username').val();
            var lid = $('#loginid').val();
            var eid = $('#emailid').val();
            var pass = $('#password').val();
            var rname = $('#rname').val();
            $.ajax({
                type: 'POST',
                url: 'insert',
                data: { uid : uid, 
                        uname : uname, 
                        lid : lid,
                        eid : eid,        
                        pass : pass,
                        rname : rname},
                success : function (data) {
                    //$('.successupdate').show();
                    //location.reload(true);
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                              data:{'msgstatus':3},
                                success: function(response){
                                     $('#Pnopopup').css('display','none');
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                },
                error : function (xhr){
                     var msg =xhr.status;
                     var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    //$('.failureupdate').show();
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data: {'xhrstatus':msg},
                                success: function(response){
                                     $('#Pnopopup').css('display','none');
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
        });
        });
        
</script>
<script>
        $("#adduser").click(function()
        { 
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: 'adduser',
                data: { id : id },
                success : userid,
                /*error : function(err){
                    alert('Oops Something Wrong');
                }*/
                 error : function(xhr){
                 var msg =xhr.status;
                 var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
       var userid = function (data){
                    var userdata = $.parseJSON(data);
                    $("#popupform").css("display", "block");
                    $("#Pnopopup").css("display", "block");
                    document.getElementById('userid').value = userdata.result.userid; 
                    document.getElementById('username').value = "";
                    document.getElementById('loginid').value = "";
                    document.getElementById('emailid').value = "";
                    document.getElementById('password').value = "";
                    document.getElementById('rname').value = "";
            }
</script>
<script>
        $(".delete_type").click(function()
        {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: 'delete',
                data: { id : id },
                success : function(){
                //alert("Deleted Record Successfully");
                //location.reload(true);
                  $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                             data: {'msgstatus':4},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                },
                error : function(xhr){
                     var msg =xhr.status;
                     var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    //alert('Oops Something Wrong');
                      $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
        /*$(document).on('blur','#loginid',function(){
            var id = $(this).val();
            $.ajax({
                type: 'POST',
                //url: '<?php echo base_url();?>Manage_User/user_exists',
                url: '<?php echo base_url();?>Manage_User/sample',
                data: { loginid : id },
                success : function(data){
                    alert("hi");

                    console.log(data);
                    //if(data == "success"){
                       // $('.resutnresponse').html('<span>User Already Exists</span>');
                    //}
                },
                error : function(err){
                    alert('Please Enter correct Data');
                }
            });
        });*/
</script>
<script>
    $(document).on('click','.close',function(){
          $("#Pnopopup").hide(); 
        });    

     //new popup msg close button
   $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            location.reload(true);
        });
</script>
</body>
</html>       
            