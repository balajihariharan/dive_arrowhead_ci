<!DOCTYPE html>
<html>
<head>
   <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
          <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<body class="main">
    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="img/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="img/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="img/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="img/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->
      <div>
        <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
        </style>
        <div id="msg" style="display: none;"></div>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="wrapper">
                <div class="content_main_all">
                    <form id="savedata" method="post">
                        <div class="content_main">
                                <div class="channel_main">
                                    <div class="channel_all">
                                        <label>Role Name <span style="color:red; font-size:16px;">*</span> :</label>
                                         <select class="select_med" name="selectrole" id="selectmed" >
                                            <?php 
                                                foreach ($role as $value)
                                                {                                       
                                                  echo '<option id="'.$value['roleid'].'" value="'.$value['roleid'].'">'.$value['rolename'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                   
                                </div>
 <center> <div style="color:red; display:none" id="msg_id">Please select the Role Name</div> </center>
                                <div class="table_main_access">
                                    
                                    
                                    <span class="returnresponse">
                                        

                                    </span>

                                </div>
                                <div class="pricing_menu_left">
                                    <input type="submit" id="save" value="Save" class="button_add rawlistdatasubmit">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        
         <?php $this->load->view('footer.php'); ?>
    </div>
 
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );
            $(document).on('click','.destroy',function(){
            $(".err_modal").css("display", "none");

            
        });

        });
    </script>
    <script>
    var responseparent = [];
        $(".select_med").change(function() {
            var id = $(this).children(":selected").attr("id");
            if(id!=0)
            {
                $('.returnresponse'). html('');
             $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>Manage_Access/get_screendata',
                data: {roleid : id },
                success : function(response){
                    var rawjson = $.parseJSON(response);
                    var result = '<div class="lable_normal table_top_form_access">';
                                  result +=      ' <input type="checkbox" id="ckbCheckAll">';
                                    result +=    'Select All' ;
                                 result +=   ' </div>';
                     result += '<table style="width:500px" border="1" cellpadding="0" cellspacing="0" id="screen"><thead><tr><th>Screen Name</th><th>Access</th></tr></thead><tbody>';           
                    $.each(rawjson,function(key,value){
                        responseparent.push(value);
                        result += '<tr><td><input type="hidden" value="'+value.screenid+'"/>'+value.screenname+'</td>';
                        if(value.active_flag == 0){
                            result += '<td><input type="hidden" value="'+value.screenname+'" name="selected_screen['+value.screenid+']"/><input type="checkbox" id="'+value.screenid+'" class="screenselect" name="active_flag['+value.screenid+']" value="'+value.screenname+'" data-parentid="'+value.parentid+'" data-screenid="'+value.screenid+'"/></td></tr>';
                        } 
                        else{
                            //class="check_small"
                            result += '<td><input type="hidden" value="'+value.screenname+'" name="selected_screen['+value.screenid+']"/><input type="checkbox" id="'+value.screenid+'" class="screenselect" name="active_flag['+value.screenid+']" value="'+value.screenname+'" data-parentid="'+value.parentid+'" data-screenid="'+value.screenid+'" checked/></td></tr>';
                        }
                    });
                    result += '</tbody></table>';
                    $('.returnresponse').html(result);
                },
                error : function(xhr){
                             var msg =xhr.status;
                             var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data : {'xhrstatus':msg},
                            
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
             });
            }
            else{
                document.getElementById('screen').style.display = "none";
            }        
          });

        //parent menu checkbox
        $(document).on('click','.screenselect',function(){
            var mainscreenid = $(this).attr('data-screenid');
            if(this.checked == false){
               $.each(responseparent,function(key,value){
                    if(value.parentid == mainscreenid){
                        $('#'+value.screenid).prop('checked', false);
                    }
               });
            }
            if(this.checked == true){
               $.each(responseparent,function(key,value){
                    if(value.parentid == mainscreenid){
                        $('#'+value.screenid).prop('checked', true);
                    }
               });
            }

        });


        //select All checkbox
        $(document).on('click','#ckbCheckAll',function(){
            if(this.checked == true){
                $('.screenselect').prop('checked', true);        
            }
            else{
                $('.screenselect').prop('checked', false);
            }
        });
    </script>
    <script>
           $(".select_med").change(function() {
             var id =  $('#selectmed').val();
              if(id=='0')
                {
                    $("#msg_id").css("display","block");
                        //document.getElementById('selectmed').style.borderColor = "red";
                        //e.preventDefault();
                        return false;                   
                         }
                    else
                    { $("#msg_id").css("display","none");
                       //document.getElementById('selectmed').style.borderColor = ""; 
                    }   

                    });  
   
        $("#save").click(function(event){  
        var id =  $('#selectmed').val();
            if(id=='0')
                {
                    $("#msg_id").css("display","block");
                        document.getElementById('selectmed').style.borderColor = "red";
                        //e.preventDefault();
                        return false;                   
                         }
                    else
                    { $("#msg_id").css("display","none");
                       document.getElementById('selectmed').style.borderColor = ""; 
                    }   

            event.preventDefault();
            var demo = $("#savedata").serialize();
            $.ajax({
                type: "POST",
                url:'<?php echo base_url();?>index.php/Manage_Access/save',
                data:$("#savedata").serialize(),
                success: function(response){
                    $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                            data : {'msgstatus':3},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                },
                error : function(xhr){
                var msg =xhr.status;
                var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                 $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data : {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
</script>
</script>
</body>
</html>