<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title> <?php echo $title[0]->SellerName;?> : <?php echo $screen;?> </title>

<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script> 
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>  
    <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script>  
    <script src="<?php echo base_url().'assets/';?>js/jquery.plugin.min.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery.datepick.js"></script>  
    <script src="<?php echo base_url().'assets/';?>js/jquery.datepick.ext.js"></script> 
    <script src="<?php echo base_url().'assets/';?>js/jquery.multiselect.js"></script> 

    
</head>

<body class="main" ng-controller="postController" ng-cloak>

<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

   <!-- <div class="container head_bg"> -->
  <?php $this->load->view('header.php'); ?>
  
   
<style>
 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
          .pagination ul>.active>a, .pagination ul>.active>span {
    color: #999999;
    cursor: default;
  }
</style> 

<style type="text/css">

.status_bar td:nth-child(5){
  text-align:right;
}
.status_bar td:nth-child(7){
  text-align:right;
}

.paging-nav {
  text-align: right;
  padding-top: 2px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #5D85B2;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}
.status_bar th:nth-child(8) {
    min-width: 280px;
    width: 280px;
}
.status_bar th:nth-child(9) {
    min-width: 280px;
    width: 280px;
}

tfoot
{
  text-align: center !important;
  display: table-row-group !important;
}
.price_text {
   width: 16px;
   height: 17px;
   display: inline-block;
   padding: 0 16px;
}
.select {
    padding: 2px;
    float: right;
    width: 155px;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}

.paging-nav,
#MainContent_GV {
 
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }

</style>
  
  <div class="container header_bg_clr">
        <div class="wrapper">
         <div class="head_menu">
            <div class="header_main">
                 <?php echo "<h2>".$screen."</h2>" ;?>
            </div>
            </div>
      </div>
      </div>
               <div id="msg" style="display: none;"></div>

       <div class="container">
        <div class="wrapper">
      <div class="content_main_all">
  <form id="salestrendsform" ng-submit="submitForm()" >

    <div class="container">
        <div class="wrapper">
            <div class="form_head">
                <div class="field">
                    <div class="lable_normal">
                        Competitor Details <span style="color:red; font-size:16px;">*</span>  : 
                    </div>
                            <select name="competitorname[]" ng-change="ngcompetitor()" class="select" ng-model="competitorname" id="competitorname">
                                  <option value="">-Select-</option>
                                  <?php if(count($comptitornameandid) >= 1){
                                    foreach($comptitornameandid as $value){ ?>
                                      <option value="<?php echo $value->CompID;?>"><?php echo $value->CompetitorName;?></option>  
                                  <?php }
                                  } ?>
                          </select> 
                </div>
                <div class="field">
                    <div class="lable_normal" >
                        Category Details <span style="color:red; font-size:16px;">*</span>  : 
                    </div>

                     <select  name="category[]"  ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" ng-model="competitorcategory" id="compcat" class="select">
                            <option value="">-None-</option>
                      </select>

                </div>
                <div class="field">
                    <div class="lable_normal">
                        Select End week <span style="color:red; font-size:16px;">*</span>  : 
                    </div>
                      <input placeholder="Select starting week" type="text" class="input mydate" ng-model="customWeekPicker" name="customWeekPicker" id="customWeekPicker" value="">
                </div>

                <div class="field">
                <button value="Search" class="button_search" id="btnsearch" type="submit">Search</button>
                 
                </div>
        
         
            </div>

        </div>
        </div>
</form>       

              <center> <div  class="error_msg_all" style=" display:none" id="msg_id">Please select Competitor,Category & End Week</div> </center>  
                <div class="plan_list_view" style="overflow: auto;">
               
        <!-- onchange="showval();" -->
                        <div class="lable_normal table_top_form">
                         <input id="showvalues" ng-model="checkboxModel.showvalues" ng-click="salestrendsshowvalues()" type="checkbox">
                         Show values</div>

                          <div class="itm_lst" ng-repeat="totsal in totalsales">
                                          <div class="itm_lst">
                                               <p>Total item listed:</p>
                                               <img src="<?php echo base_url().'assets/';?>img/list.png">
                                               <p style="font-size: 20px; color: #00886d;">{{totsal.TotalListings}}</p>
                                          </div>
                                           <div class="itm_lst">
                                               <p>Total Sales Since Inception:</p>
                                                <img src="<?php echo base_url().'assets/';?>img/doller.png">
                                               <p style="font-size: 20px; color: #00886d;">{{totsal.TotalSales}}</p>
                                          </div>
                                           
                          </div>         

                  <table cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
              
                                <th class="curser_pt" ng-click="sortBy('itemid')">Competitor ID #</th>
                                <th class="curser_pt" ng-click="sortBy('sku')" >Competitor SKU</th>         
                                <th ng-click="sortBy('mypartno')">My Part #</th>
                                <th class="curser_pt" ng-click="sortBy('title')">Title</th>    
                                <th class="curser_pt" ng-click="sortBy('price')">Current Price (In $)</th>
                                <th class="curser_pt" ng-click="sortBy('qtysold')">Qty. Sold (5 wk)</th>
                                <th class="curser_pt" ng-click="sortBy('totalsales')">Total Sales (5 wk) (In $)</th>
                                <th class="curser_pt" ng-click="sortBy('pricevw1')">5 wk Price (In $)</th>
                                <th class="curser_pt" ng-click="sortBy('qtysoldvw1')">5 wk Qty.Sold</th>  
                            </tr>

                        </thead>
                         <tfoot>
                          <tr>
                                    <td>
                                      <div class="search"><input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch"></div>
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch">
                                       </div>  
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="partquery" ng-change="mypartsearch()" class="inputsearch">
                                       </div>    
                                    </td>
                                    <td>
                                       <div class="search">
                                         <input type="text" ng-model="titlequery" ng-change="mytitlesearch()" class="inputsearch">
                                      </div>    
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td ng-repeat="totsal1 in totalsales">
                                                    <em title="{{totsal1.wk1dt}}">{{totsal1.wkno1}}</em>
                                                    <em title="{{totsal1.wk2dt}}">{{totsal1.wkno2}}</em>
                                                    <em title="{{totsal1.wk3dt}}">{{totsal1.wkno3}}</em>
                                                    <em title="{{totsal1.wk4dt}}">{{totsal1.wkno4}}</em>
                                                    <em title="{{totsal1.wk5dt}}">{{totsal1.wkno5}}</em>
                                    </td>
                                    <td ng-repeat="totsal2 in totalsales">
                                                    <em title="{{totsal2.wk1dt}}">{{totsal2.wkno1}}</em>
                                                    <em title="{{totsal2.wk2dt}}">{{totsal2.wkno2}}</em>
                                                    <em title="{{totsal2.wk3dt}}">{{totsal2.wkno3}}</em>
                                                    <em title="{{totsal2.wk4dt}}">{{totsal2.wkno4}}</em>
                                                    <em title="{{totsal2.wk5dt}}">{{totsal2.wkno5}}</em>
                                    </td>
                         </tr>
                        </tfoot>   
                                                     <tbody>
                                                      <tr ng-repeat="sales in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                                          <!--   <tr  ng-repeat="sales in saledata | filter:search | orderBy:propertyName:reverse" > -->
                                                                <td>{{sales.itemid}}</td>
                                                                <td>{{sales.sku}}</td>
                                                                <td>{{sales.mypartno}}</td>

                                                                <td>
                                                                     <a ng-click="redirectToLink($event)" element="{{sales.itemid}}" style="cursor: pointer;">{{sales.title}}
                                                                      </a>
                                                                </td>

                                                                <td class="alignment">{{sales.price}}</td>

                                                                <td class="alignment">
                                                                      <div class="tbl_txt">
                                                                            <div class="price_text">{{sales.qtysold}}</div>
                                                                      </div>
                                                                </td>

                                                                <td class="alignment">
                                                                    <div class="tbl_txt">
                                                                        <div class="price_text" style="margin-right:10%">{{sales.totalsales}}</div>
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                <div class="tbl_img">
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'u'" class="price_up"></div>

                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'u'" class="price_up"></div>

                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'u'" class="price_up"></div>

                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'u'" class="price_up"></div>

                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'u'" class="price_up"></div>
                                                                </div>
                                                                <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                                        <div class="price_text">{{sales.pricevw1}}</div>
                                                                        <div class="price_text">{{sales.pricevw2}}</div>                              
                                                                        <div class="price_text">{{sales.pricevw3}}</div>
                                                                        <div class="price_text">{{sales.pricevw4}}</div>
                                                                        <div class="price_text">{{sales.pricevw5}}</div>
                                                                </div>
                                                                </td>

                                                                <td>
                                                                <div class="tbl_img">
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'u'" class="price_up"></div>
                                                                        
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == null" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'n'" class="price_wrong"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'e'" class="price_equal"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'd'" class="price_down"></div>
                                                                        <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'u'" class="price_up"></div>

                                                                        <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                                            <div class="price_text">{{sales.qtysoldvw1}}</div>
                                                                            <div class="price_text">{{sales.qtysoldvw2}}</div>                              
                                                                            <div class="price_text">{{sales.qtysoldvw3}}</div>
                                                                            <div class="price_text">{{sales.qtysoldvw4}}</div>
                                                                            <div class="price_text">{{sales.qtysoldvw5}}</div>
                                                                        </div>
                                                                </div>
                                                                </td>

                                                            </tr>
                                                        </tbody>  
                                                   </table>
                                              </div>
                                              

                                                 <!-- pagination ui part -->
                                        <div class="pagination pull-right pagi_master">
                                            <ul>
                                                <li ng-class="{disabled: currentPage == 0}">
                                                    <a href ng-click="prevPage()">« Prev</a>
                                                </li>
                                                <li ng-repeat="n in pagenos | limitTo:5"
                                                    ng-class="{active: n == currentPage}"
                                                ng-click="setPage()">
                                                    <a href ng-bind="n + 1">1</a>
                                                </li>
                                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                                    <a href ng-click="nextPage()">Next »</a>
                                                </li>
                                            </ul>
                                        </div>
                              </div>
                               
                                                 </div>
                   </div>
                   </div>
                   </div>


                                  





                         <?php $this->load->view('footer.php'); ?>
                        </body>
</html>
 <script src= "https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>  


<script>
$(function() {
$('#customWeekPicker').datepick({ 
    renderer: $.datepick.weekOfYearRenderer, 
    calculateWeek: customWeek, 
    firstDay: 1, 
    showOtherMonths: true, 
    showTrigger: '#calImg'
  }); 
 
function customWeek(date) { 
    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1; 
}
});

      $('#txtDate').datepicker({
        showWeek: true,
        firstDay: 1,
       onSelect: function(dateText) {        
           $("#txtWeek").val($.datepicker.iso8601Week(new Date(dateText)));
           $("#selectedDate").val(dateText);
       }
    });

</script>
<script>
        $('#compcat').click(function(e){ 
              if ($('#competitorname').val() != ""){
                  document.getElementById('competitorname').style.borderColor = "green"; 
              }
        });
         $('#customWeekPicker').click(function(e){ 
              if ($('#compcat').val() != ""){
                  document.getElementById('compcat').style.borderColor = "green"; 
              }
        });

$('#btnsearch').click(function(e){ 
     if ($('#competitorname').val() == "" || $('#compcat').val() == "" || $('#customWeekPicker').val() == "") {
                 var competitorname=document.getElementById('competitorname').value;
                   if(competitorname == ""){
                        document.getElementById('competitorname').style.borderColor = "red";
                       // return false;
                    }
                    else
                    {
                      document.getElementById('competitorname').style.borderColor = "green";
                    }

                  var compcat=document.getElementById('compcat').value;
                   if(compcat == ""){
                        document.getElementById('compcat').style.borderColor = "red";
                       // return false;
                    }
                    else
                     {
                           document.getElementById('compcat').style.borderColor = "green";  
                     }
                     var customWeekPicker=document.getElementById('customWeekPicker').value;
                   if(customWeekPicker == ""){
                        document.getElementById('customWeekPicker').style.borderColor = "red";
                        return false;
                    }    
                  
                    e.preventDefault();
                  
     
            }
            else
            {
                document.getElementById('competitorname').style.borderColor = "green";
                document.getElementById('compcat').style.borderColor = "green";  
                 document.getElementById('customWeekPicker').style.borderColor = "green";  
              
            }
        });
  //new popup msg close button
   $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
</script>
<script type="text/javascript">
  $('#btnsearch').click(function(e){ 
               
                    if ($('#competitorname').val() == "" || $('#compcat').val() == "" || $('#customWeekPicker').val() == "") {
                        $("#msg_id").css("display","block");
                        e.preventDefault();
                        return false;
                    }
                    else
                    {
                         $("#msg_id").css("display","none");
                    }
                });
</script>

 <script>
   var sortingOrder = 'itemid';
   var postApp     = angular.module('postApp', []);
    postApp.controller('postController', function($scope, $http,$timeout,$window,$filter) {
                    $scope.sortingOrder = sortingOrder;
                    $scope.reverse = false;
                    $scope.filteredItems = [];
                    $scope.groupedItems = [];
                    $scope.itemsPerPage = 25;
                    $scope.pagedItems = [];
                    $scope.pagenos = [];
                    $scope.saledata = '';
                    $scope.currentPage = 0;

              /* $scope.sortBy = function(propertyName) {
                            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                            $scope.propertyName = propertyName;
                          };*/

            //Click the button to open an new browser window that is 1200px wide and 600px tall
              $scope.redirectToLink = function (obj) {
                       var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                    };


              //salestrends show values
             $scope.salestrendsshowvalues = function(){
                        if(event.target.checked == true){
                            $scope.salestrendsvalues = true;
                        }
                        else{
                            $scope.salestrendsvalues = false;
                        }
                    };


                //category loading function
              $scope.ngcompetitor = function() {
                  $scope.isLoading = true; //Loader Image
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Salestrends/competitor_category',
                  data    : {'competitor':$scope.competitorname},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                     $scope.isLoading = false; //Loader Image
                    $scope.subcategories = data;
                    $timeout(function(){
                        angular.element('#langOpt1').multiselect('reload');
                    },1500);
                  })
                   .catch(function (err) {
                    var msg = err.status;
                      //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                    //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                     //$scope.isLoading = false;
                  });
                };


                //masterlist competitor search function
                $scope.submitForm = function() {
                $('.spinner').show();
                var selectdate = $('.mydate').val();
                  $scope.isLoading = true; //Loader Image
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Salestrends/salestrends_detail_control',
                  data    : {'competitor':$scope.competitorname,'category':$scope.competitorcategory.Categoryid,'reportdate':selectdate},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {

                    $scope.saledata  = data.salestrenddetail;
                     $scope.totalsales  = data.totsaldetail;

                    /* console.log($scope.saledata);
                     console.log('**************');
                     console.log($scope.totalsales);*/
                     
                                       //pagination part start 
                        // *********************
                               $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };

                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                             // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
                                    for(var attr in item) {
                                       /* if (searchMatch(item[attr], $scope.query))
                                            return true;*/
                                           if(attr == "itemid") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                            
                             $scope.myskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
                                    for(var attr in item) {       
                                            if(attr == "sku") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.mypartsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
                                    for(var attr in item) {
                                            if(attr == "mypartno") {
                                            if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.mytitlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
                                    for(var attr in item) {                                   
                                             if(attr == "title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             // *****************finish the filtered items*****************

                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end


                    $scope.search = {};
                    $scope.isLoading = false; //Loader Image
                  })
                    .catch(function (err) {
                      var msg = err.status;
                       //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                    //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                     //$scope.isLoading = false;
                  })
                   .finally(function () {
                     $('.spinner').hide();
                    });
                };

    });
</script>
<script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>