<!DOCTYPE html>
<html ng-app="RunApp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
          <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
</head>
<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
<body class="main" ng-controller="RunController">
 <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
    <div class="logo">
                    <a href='<?php echo base_url();?>DashBoard/index'> <img height="50px" src="<?php echo base_url().'assets/';?>img/logo.png"></a>
                </div>
    <div>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <h2 style="background: #ffbf00;">Run <?php echo $screen;?> Recommendation</h2>
                    <span style="padding-left:40%; color:green; text-align: center;font-size: 25px;font-weight: bold;">{{message}}</span>
                </div>
            </div>
        </div>
        <form action=""><center>
        <input type="radio" name="Raw" value="auto"><strong>Automatic Rawlist</strong>
        <input type="radio" name="Raw" value="manual"><strong>Manual Rawlist </strong><br></center>
        </form>
        <br><br>
        <center><button style="color:white; background: #00763C; border-radius: 3px;padding:7px  45px 7px 42px;" ng-click="RunRec()">Start</button>&nbsp;&nbsp;<strong>Click here to start <?php echo $screen;?> Recommendation</strong></center>
</body>
<script>
    var RunApp = angular.module('RunApp', []);
    RunApp.controller('RunController',function($scope,$http,$timeout){
            $scope.RunRec = function(){
                $scope.message = 'Process Started Successfully';
                $scope.isLoading = true;
                alert('Do not exit this page until process gets over !!!');
                $http({                     
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/RunRecommendPrice/runfunc'
                })
                .then(function(response) {
                    $scope.isLoading = false;
                    $scope.message = 'Process Ended Successfully';           
                });
            }
    });
</script>
</html>