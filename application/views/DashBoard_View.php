<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
       <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    <link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_romaine.css" />
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script> 
    <script type="text/javascript" src="<?php echo  base_url();?>assets/demo/events.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/calendar.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/datePicker.js"></script>
    <script src="https://rawgithub.com/eligrey/FileSaver.js/master/FileSaver.js"></script>
</head>
<body class="main" ng-controller="postController" ng-cloak>
    <!-- <div class="container head_bg"> -->
  <?php $this->load->view('header.php'); ?>
  <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
  <div id="msg" style="display: none;"></div>
    <div>
        <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
    /*        .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }


            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }*/
.vertical_cen{
    vertical-align: middle;
}


            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            .edit_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 85px;
                font-size: 14px;
            }

             .delete_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 90px;
                font-size: 14px;
            }
            .table_main_new {
                width:100%;
                /*margin:50px 200px;*/
            }
            .table_main_new thead{
                background:#EEEEEE;
                
            }
           

            .table_main_new tbody tr td:last-child{
                 text-align: right;   
            }
            .table_main_new tbody tr td{
                color:#3e3e3e;
                font-size:14px;
                text-align:left ;
                text-transform:capitalize;
                line-height:20px;
            }

            .table_main_new_access th:nth-child(1) {
                width:450px;
            }
            .table_main_new_access th:nth-child(2) {
                border-right:none;
            }

               .table_main_new thead tr th {
                    color: #fff;
                    font-size: 15px;
                    background: #2D8BD5;
                    text-align: left;
                    padding: 10px;
                    line-height: 20px;
                }



                .nodata{
                  /*  margin: 0 660px;*/
                    font-weight: bold;
                    color: black;
                    font-size: 25px;         
                }

                .back_button {
                    padding: 5px 31px 6px 24px;
                    float: right;
                    margin: -8px -2px -9px 8px;
                    font-size: 15px !important;
                    border: none;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    cursor: pointer;
                    font-family: sans-serif, Verdana, Geneva;
                    text-align: right;
                    display: inline-block;
                    color: #fff;
                    vertical-align: top;
                    background-color: #00886D;
                }

                .export_buttonnew {
                    padding: 6px 30px 7px 10px;
                    float: right;
                    margin: -11px 9px 0 0;
                    font-size: 15px !important;
                    border: none;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    cursor: pointer;
                    font-family: sans-serif, Verdana, Geneva;
                    text-align: right;
                    display: inline-block;
                    color: #fff;
                    vertical-align: top;
                    background: url('<?php echo base_url();?>assets/img/export_data1.png') no-repeat right 1px #00886D;
                }
 
                .curser_pt_new {
                    cursor: pointer;
                    padding : 0 20px;
                    cursor: pointer !important;
                    }

                    .viewicon{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/eye_icon.png') no-repeat scroll 5px center;
                    padding: 3px 6px 0px 30px;
                    }

                    .trendicon{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/trend_icon.png') no-repeat scroll 5px center;
                    padding: 3px 6px 0px 30px;
                    }

                .report2{
                    padding : 0 20px;
                }

                .price_text {
                   width: 16px;
                   height: 17px;
                   display: inline-block;
                   padding: 0 16px;
                }

                .back_button_rep3 {
                    padding: 5px 31px 6px 24px;
                    float: left;
                    margin:20px 0px;
                    font-size: 15px !important;
                    border: none;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    cursor: pointer;
                    font-family: sans-serif, Verdana, Geneva;
                    text-align: right;
                    display: inline-block;
                    color: #fff;
                    vertical-align: top;
                    background-color: #00886D;
                }

                .status_bar th:nth-child(8) {
                    min-width: 280px;
                    width: 280px;
                }

                    .status_bar th:nth-child(9) {
                    min-width: 280px;
                    width: 280px;
                }

                .export_buttonnew {
                    padding: 6px 30px 7px 10px;
                    float: right;
                    margin: 0px 4px 0px 0px;
                    font-size: 15px !important;
                    border: none;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    cursor: pointer;
                    font-family: sans-serif, Verdana, Geneva;
                    text-align: right;
                    display: inline-block;
                    color: #fff;
                    vertical-align: top;
                    background: url('<?php echo base_url();?>assets/img/export_data1.png') no-repeat right 1px #00886D;
                }

                .tooltip .tooltiptext {
                    visibility: hidden;
                    width: 247px;
                    background-color: #2C3E50;
                    color: #fff;
                    text-align: center;
                    border-radius: 6px;
                    padding: 0px 0px 2px 0px;
                    position: absolute;
                }

                .tooltip:hover .tooltiptext {
                    visibility: visible;
                }

                .nodata2 {
                   text-align: center;
                    font-weight: bold;
                    color: black;
                    font-size: 25px;
                }

    </style>

        <div class="plan_list_view" id="div1" style="overflow: auto;">

            <div class="wrapper">
             <div class="header_main">
             <?php echo "<h2>".$screen."</h2>" ;?></div>
             <span style="padding-left:40%; color:red; font-size:14px"></span>
               

                    <div class="content_main_all">
                    <div class="display_inline_blk">

                        <span class="week">Week : {{week}}</span>

                        </div>
                        <div class="drop_down_med1">
                                <input class="date date-1" value="" id="myWeek" placeholder="YYYY-MM-DD" style="border-radius: 3px; padding: 4px 0px 4px 3px; width: 100px;color:black; font-weight: bold;" /> 
                                <!-- background: #5D85B2; border: none;-->
                     <span style="color:red; font-size:16px;">*</span>  : 
      <input type="button" class="button_search_tab1" id= "display_week" ng-click="GetData()" value="Display"/>
      <center> <div style=" display:none;margin-left: 448px" class="error_msg_all" id="msg_id">Please Select a Week</div> </center> 
                        </div>
                        
                        <div class="button_right">       
                                <input type="button" class="export_buttonnew" value="Export As Excel" ng-click="exportData()"/>
                        </div>
                        <br><br>
                        <!-- <input type="radio" name="category" ng-click="showcat(0)" checked/>Show My Category
                        <input type="radio" name="category" ng-click="showcat(1)"/>Show All Category
                        <br><br> -->
                        <div style="margin-left: 1px;width: 431px;" id="div1_head" class="lable_normal table_top_form">
                            <input type="radio" id="radio11" name="category" ng-click="showcat(0)" checked/>
                            <label for="radio11">Market Share - Master List</label>
                            <input type="radio" id="radio12" name="category" ng-click="showcat(1)"/>
                            <label for="radio12">Market Share - Competitor</label>
                        </div>
                        <div class="table_main_new"  id="exportable1">
                                <table style="width:100%;" border="2" cellpadding="0" cellspacing="0" align="center">
                                    <col>
                                          <colgroup span="2"></colgroup>
                                          <colgroup span="2"></colgroup>
                                              <thead>
                                          <tr>
                                            <th class="curser_pt" style="text-align: center; vertical-align: middle !important;" rowspan="2" ng-click="sortBy('Company')">Company</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">7 Days Sales (In $)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">7 Days % Of Total (In %) </th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Listing</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Sold [7 days]</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">% of Listing Sold (In %)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Avg. Sale Price (In $)</th>
                                          </tr>
                                          <tr>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDaysSales')" id="myselector">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDaysSales_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDAYPercentOFTOTAL')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDAYPercentOFTOTAL_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('Listing')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('Listing_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SoldSevendays')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SoldSevendays_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('PercentofListingSold')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('PercentofListingSold_lw')">Week {{week-1}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('AvgSalePrice')">Week {{week}}</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('AvgSalePrice_lw')">Week {{week-1}}</th>
                                      </tr>
                                      </thead>
                                <tbody>

                                <tr ng-repeat="item in dashboarddata | orderBy:sortKey:reverse">

                                    <td class="curser_pt_new" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}" scope="row" ng-click="ReportTwo($event)" data-id="{{item.CompId}}" data-value="{{item.Company}}">
                                    <img ng-click="ReportTwo($event)" data-id="{{item.CompId}}" data-value="{{item.Company}}" src="<?php echo base_url();?>/assets/img/eye_icon.png">&nbsp;&nbsp;&nbsp;&nbsp;{{item.Company}}
                                    </td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDaysSales | currency:''}} </td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDaysSales_lw | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDAYPercentOFTOTAL | currency:''}}</td>
                                     <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDAYPercentOFTOTAL_lw | currency:''}}</td>                          
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.Listing}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.Listing_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SoldSevendays}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SoldSevendays_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.PercentofListingSold | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.PercentofListingSold_lw | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.AvgSalePrice | currency:''}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.AvgSalePrice_lw | currency:''}}</td>

                                </tr>
                                </tbody>
                            </table><br><br>
                            <div class="display_inline_blk align_center">
                            <p ng-if="dashboarddata.length == 0" class="nodata">No Data</p>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
         <?php $this->load->view('DashBoardDrill2_View.php'); ?>
         <?php $this->load->view('DashBoardDrill3_View.php'); ?>
         <?php $this->load->view('footer.php'); ?>

<script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script >
  $('#myWeek').click(function(e){ 
    if ($('#myWeek').val() != "") {
                  document.getElementById('myWeek'). style.borderColor = "";
                  $("#msg_id").css("display","none");
             }  
             });         
                    

    $('#display_week').click(function(e){ 
      if ($('#myWeek').val() == "") {
                 var myWeek=document.getElementById('myWeek').value;
                   if(myWeek == ""){
                        document.getElementById('myWeek'). style.borderColor = "red";
                        $("#msg_id").css("display","block");
                        return false;
                    }
                    else
                     {
                       document.getElementById('myWeek').style.borderColor = ""; 
                    }

                    e.preventDefault();
            }
     else
                {   
                    $("#msg_id").css("display","none");
                    document.getElementById('myWeek').style.borderColor = "";
            }
        });
</script>
<script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
</script>

<script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script>
<script src="https://rawgithub.com/eligrey/FileSaver.js/master/FileSaver.js"></script>

<script>
Date.prototype.getWeek = function (dowOffset) {
    dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0;
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset;
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};

var cur_date = new Date();
var week = cur_date.getWeek();
var year = cur_date.getFullYear();
var month = cur_date.getMonth();
var mnth = month+1;
var day = cur_date.getDate();
var drilldate = year+'-'+mnth+'-'+day;
var sortingOrder = 'itemid';
var postApp = angular.module('postApp', []);
    postApp.controller('postController',function($scope,$http,$timeout,$filter,$window){
                $scope.isLoading = true;
                $scope.sortingOrder = sortingOrder;
                $scope.reporttwodata = [];
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 25;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.saledata = '';
                $scope.currentPage = 0;
                $scope.year = year;
                $scope.drilldate = drilldate;
                document.getElementById('div2').style.display = "none";
                document.getElementById('div3').style.display = "none";
                document.getElementById('div2_head').style.display = "none";
                document.getElementById('div3_head').style.display = "none";
                $scope.week = week-1;
                $("#radio11").prop('checked', true);
                $http({                     
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/DashBoard/DashBoard_data',
                        data    : { 'year':year,'week':  $scope.week },
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .then(function(response) {
                        $scope.isLoading = false;
                        $timeout(function() {   
                                angular.element('#myselector').triggerHandler('click');
                            }, 0);
                        $scope.dashboarddata = response.data.dashboard_category;
                        console.log($scope.dashboarddata);
                    });


                  
                $scope.sesdata = '<?php echo $this->session->userdata('compid')?>';

                $scope.sortBy = function(propertyName) {
                    $scope.sortKey = propertyName;
                    $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                    $scope.pagedItems = ""; 
                    $scope.groupToPages();
                };


                $scope.GetData = function(){
                    document.getElementById('div2').style.display = "none";
                    document.getElementById('div3').style.display = "none";
                    $scope.isLoading = true;
                    var x = document.getElementById("myWeek").value;
                    if (x!='')
                    {
                        var year = x.slice(0,4);
                        var cur_date = new Date(x);
                        var week = cur_date.getWeek();
                        $scope.week = week;
                        $scope.year = year;
                        $scope.drilldate = x;
                        if ($scope.viewflag != 1)
                        {
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/DashBoard/DashBoard_data',
                                data    : { 'year':year,'week': week},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .then(function(response) {
                                $scope.isLoading = false;
                                $("#radio11").prop('checked', true);
                                $("#radio_lvl2_11").prop('checked', true);
                                $("#radio_lvl3_11").prop('checked', true);
                                $scope.dashboarddata = response.data.dashboard_category;
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                console.log($scope.dashboarddata);
                                document.getElementById('div1').style.display = "block";
                                document.getElementById('div1_head').style.display = "block";
                            });
                        }
                        else
                        {
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/DashBoard/DashBoard_data2',
                                data    : { 'year':year,'week': week},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(response) {
                                $scope.isLoading = false;
                                $("#radio12").prop('checked', true);
                                $("#radio_lvl2_12").prop('checked', true);
                                $("#radio_lvl3_12").prop('checked', true);
                                $scope.dashboarddata = response.dashboard_category;
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                console.log($scope.dashboarddata);
                                document.getElementById('div1').style.display = "block";
                                document.getElementById('div1_head').style.display = "block";
                            })
                            .catch(function (err) {
                        
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        });
                        }
                    }
                    else
                    {
                        var cur_date = new Date();
                        var week = cur_date.getWeek();
                        var year = cur_date.getFullYear();
                        var month = cur_date.getMonth();
                        var mnth = month+1;
                        var day = cur_date.getDate();
                        $scope.week = week-1;
                        $scope.year = year;
                        //$scope.drilldate = day;
                        if ($scope.viewflag != 1)
                        {
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/DashBoard/DashBoard_data',
                                data    : { 'year':year,'week': week-1},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .then(function(response) {
                                $scope.isLoading = false;
                                 $("#radio11").prop('checked', true);
                                 $("#radio_lvl2_11").prop('checked', true);
                                 $("#radio_lvl3_11").prop('checked', true);
                                $scope.dashboarddata = response.data.dashboard_category;
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                console.log($scope.dashboarddata);
                                document.getElementById('div1').style.display = "block";
                                document.getElementById('div1_head').style.display = "block";
                            });
                        }
                        else
                        {
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/DashBoard/DashBoard_data2',
                                data    : { 'year':year,'week': week-1},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(response) {
                                $scope.isLoading = false;
                                $("#radio12").prop('checked', true);
                                $("#radio_lvl2_12").prop('checked', true);
                                $("#radio_lvl3_12").prop('checked', true);
                                console.log(response.dashboard_category);
                                $scope.dashboarddata = response.dashboard_category;
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                                console.log($scope.dashboarddata);
                                document.getElementById('div1').style.display = "block";
                                document.getElementById('div1_head').style.display = "block";
                            })
                            .catch(function (err) {
                        
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        });
                        }
                    }
                };

                $scope.exportData = function () {
                    var blob = new Blob([document.getElementById('exportable1').innerHTML], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    });
                    saveAs(blob, "MSR_WEEK_"+$scope.week+".xls");
                }; 

                 $scope.exportDataDrill1 = function () {
                    var blob = new Blob([document.getElementById('exportable2').innerHTML], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    });
                    saveAs(blob, "MSR_"+$scope.compname+"_WEEK_"+$scope.week+".xls");
                };

                $scope.exportDataDrill2 = function () {
                    var blob = new Blob([document.getElementById('exportable3').innerHTML], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    });
                    saveAs(blob, "MSR_"+$scope.compname+"_"+$scope.catname+".xls");
                };

                $scope.ReportTwo = function(e){
                    $scope.isLoading = true;
                    $scope.reporttwodata = '';
                    document.getElementById('div1').style.display = "none";
                    document.getElementById('div3').style.display = "none";
                    document.getElementById('div1_head').style.display = "none";
                    document.getElementById('div3_head').style.display = "none";
                    var compid = e.target.getAttribute('data-id');
                    $scope.compid = compid;
                    $scope.compname = e.target.getAttribute('data-value');
                    document.getElementById('div2').style.display = "block";
                    document.getElementById('div2_head').style.display = "block";
                    if ($scope.viewflag != 1)
                    {
                        $http({
                            method  : 'POST',
                            url     : '<?php echo base_url();?>index.php/DashBoard/ReportTwo',
                            data    : { 'year':$scope.year,'week': $scope.week,'compid':$scope.compid},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .then(function(response) {

                            $scope.isLoading = false;
                            console.log('if1suc');
                            $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                            $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                            $scope.reporttwodata = response.data.dashboard_category;
                            console.log($scope.reporttwodata);
                        })
                        .catch(function (err) {
                        console.log('if1fail');
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        });
                    }
                    else
                    {
                        $http({
                            method  : 'POST',
                            url     : '<?php echo base_url();?>index.php/DashBoard/ReportTwo2',
                            data    : { 'year':$scope.year,'week': $scope.week,'compid':$scope.compid},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .then(function(response) {
                            $scope.isLoading = false;
                            console.log('if2suc');
                            $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                            $timeout(function() {
                                    angular.element('#myselector').triggerHandler('click');
                                }, 0);
                            $scope.reporttwodata = response.data.dashboard_category;
                            console.log($scope.reporttwodata);
                        })
                        .catch(function (err) {
                        console.log('if2fail');
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        });
                    }
                }

                $scope.ReportThree = function(e){
                    $scope.isLoading = true;
                    var catid = e.target.getAttribute('data-id');
                    $scope.catname = e.target.getAttribute('data-value');
                    $scope.catid= e.target.getAttribute('data-id');
                    $("#radio1").prop('checked', true);
                    $("#radio2").prop('checked', false);
                    if (catid == null){
                        $scope.isLoading = false;
                        return false;
                    }
                    document.getElementById('div1').style.display = "none";
                    document.getElementById('div2').style.display = "none";
                    document.getElementById('div1_head').style.display = "none";
                    document.getElementById('div2_head').style.display = "none";
                    document.getElementById('div3').style.display = "block";
                    document.getElementById('div3_head').style.display = "block";
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/DashBoard/ReportThree',
                        data    : { 'compid':$scope.compid, 'catid' : catid, 'date' : $scope.drilldate},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .then(function(response) {
                    		//setTimeout(function(){
                        		$scope.isLoading = false;
                    		//},20000);
                        	$scope.saledata = response.data.dashboard_drill_data;
                        	$scope.totalsales = response.data.dashboard_drill_data_header;
                        	$scope.pagedItems = [];



                        	//*****************
	                        for (var i = 0; i < $scope.saledata.length; i++) {
	                            if (i % $scope.itemsPerPage === 0) {
	                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.saledata[i] ];
	                                } else {
	                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.saledata[i]);
	                            }
	                         }

	                        var searchMatch = function (haystack, needle) {
	                            if (!needle) {
	                                return true;
	                            }
	                             if(haystack !== null){
	                                return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
	                            }
	                        };

	                        $scope.mysearch = function () {
	                            $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
	                                for(var attr in item) {
	                                       if(attr == "itemid") {
	                                        if (searchMatch(item[attr], $scope.query)  > -1){
	                                            return true;
	                                        }
	                                    }
	                                }
	                                return false;
	                            });
	                            // take care of the sorting order
	                            if ($scope.sortingOrder !== '') {
	                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	                            }
	                            $scope.currentPage = 0;
	                            // now group by pages
	                            $scope.groupToPages();
	                        };
                        
	                        $scope.myskusearch = function () {
	                            $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
	                                for(var attr in item) {       
	                                        if(attr == "sku") {
	                                        if (searchMatch(item[attr], $scope.skuquery)  > -1){
	                                            return true;
	                                        }
	                                    }

	                                }
	                                return false;
	                            });
	                            // take care of the sorting order
	                            if ($scope.sortingOrder !== '') {
	                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	                            }
	                            $scope.currentPage = 0;
	                            // now group by pages
	                            $scope.groupToPages();
	                        };

	                        $scope.mypartsearch = function () {
	                            $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
	                                for(var attr in item) {
	                                        if(attr == "mypartno") {
	                                        if (searchMatch(item[attr], $scope.partquery)  > -1){
	                                            return true;
	                                        }
	                                    }
	                                }
	                                return false;
	                            });
	                            // take care of the sorting order
	                            if ($scope.sortingOrder !== '') {
	                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	                            }
	                            $scope.currentPage = 0;
	                            // now group by pages
	                            $scope.groupToPages();
	                        };

	                        $scope.mytitlesearch = function () {
	                            $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
	                                for(var attr in item) {                                   
	                                         if(attr == "title") {
	                                        if (searchMatch(item[attr], $scope.titlequery)  > -1){
	                                            return true;
	                                        }
	                                    }
	                                }
	                                return false;
	                            });
	                            // take care of the sorting order
	                            if ($scope.sortingOrder !== '') {
	                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	                            }
	                            $scope.currentPage = 0;
	                            // now group by pages
	                            $scope.groupToPages();
	                        };
                        
	                        $scope.groupToPages = function () {
	                        $scope.pagedItems = [];
	                        for (var i = 0; i < $scope.filteredItems.length; i++) {
	                            if (i % $scope.itemsPerPage === 0) {
	                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
	                                } else {
	                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
	                            }
	                            }
	                        };
                        
	                        $scope.range = function (start, end) {
	                            var ret = [];
	                            if (!end) {
	                                end = start;
	                                start = 0;
	                            }
	                            for (var i = start; i < end; i++) {
	                                ret.push(i);
	                            }
	                            $scope.pagenos = ret;
	                            return ret;
	                        };
                        
	                        $scope.prevPage = function () {
	                            if ($scope.currentPage > 0) {
	                                $scope.currentPage--;
	                            }
	                        };
                        
	                        $scope.nextPage = function () {
	                            if ($scope.currentPage < $scope.pagedItems.length - 1) {
	                                $scope.currentPage++;
	                            }
	                        };
                        
	                        $scope.setPage = function () {
	                            $scope.currentPage = this.n;
	                        };

	                        $scope.$watch('currentPage', function(pno,oldno){
	                          if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
	                            var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
	                            $scope.range(start, $scope.pagedItems.length);
	                          }
	                        });

	                        $scope.range($scope.pagedItems.length);
	                        $scope.mysearch();

	                        $scope.exportDataDrill3 = function () {
	                            var exportdata_exc = [];    
	                           
	                            $scope.ForExcelExport = exportdata_exc;

	                            var mystyle = {
	                            sheetid: 'My Parts',
	                            headers: true,
	                            column: { style: 'background:#a8adc4' },
	                            };
	                            alasql('SELECT * INTO XLS("DIVE_MyParts.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
	                        };

	                        $scope.showitem = function(flag){
	                            if (flag == 1)
	                            {
	                                document.getElementById('metrics').style.display = "none";
	                                $scope.filteredItems = $filter('filter')($scope.saledata, function (item) {
	                                            for(var attr in item) {
	                                                   if(attr == "itemid_map_flag") {
	                                                    if (searchMatch(item[attr], 1)  > -1){
	                                                        return true;
	                                                    }
	                                                }
	                                            }
	                                            return false;
	                                        });
	                                        // take care of the sorting order
	                                        if ($scope.sortingOrder !== '') {
	                                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	                                        }
	                                        $scope.currentPage = 0;
	                                        // now group by pages
	                                        $scope.groupToPages();
	                            }
	                            else
	                            {
	                                document.getElementById('metrics').style.display = "block";
	                                $scope.filteredItems = $filter('filter')($scope.saledata);
	                                if ($scope.sortingOrder !== '') {
	                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
	                                }
	                                $scope.currentPage = 0;
	                                $scope.groupToPages();
	                            }
	                        };
	                })
                   .catch(function (err) {
                        
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        });
                }

                $scope.Back1 = function(){
                    document.getElementById('div1').style.display = "block";
                    document.getElementById('div2').style.display = "none";
                    document.getElementById('div3').style.display = "none";  
                    document.getElementById('div1_head').style.display = "block";
                    document.getElementById('div2_head').style.display = "none";
                    document.getElementById('div3_head').style.display = "none";
                }

                $scope.Back2 = function(){
            		document.getElementById('div1').style.display = "none";
                	document.getElementById('div3').style.display = "none";
                	document.getElementById('div2').style.display = "block";
                	/*document.getElementById('page').style.display = "none";
                    */
                    document.getElementById('div1_head').style.display = "none";
                    document.getElementById('div3_head').style.display = "none";
                    document.getElementById('div2_head').style.display = "block";
                }

                $scope.salestrendsshowvalues = function(){
                            if(event.target.checked == true){
                                $scope.salestrendsvalues = true;
                            }
                            else{
                                $scope.salestrendsvalues = false;
                            }
                };    

                $scope.redirectToLink = function (obj) {
                       var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                };



                $scope.ReportFour = function(e){
                    $scope.isLoading = true;
                    var itemid = e.target.getAttribute('data-id');
                    var catid = e.target.getAttribute('data-value');
                    document.getElementById('div1').style.display = "none";
                    document.getElementById('div2').style.display = "none";             
                    document.getElementById('div3').style.display = "block";
                    
                    
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/DashBoard/ReportFour',
                        data    : { 'itemid' : itemid,'date' : $scope.drilldate},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .then(function(response) {
                        $scope.isLoading = false;
                        $scope.saledata_inner = response.data.dashboard_drill_data;
                        document.getElementById('Pnopopupsalestrends').style.display = "block";
                        console.log(response.data.dashboard_drill_data);
                    })
                    .catch(function (err) {
                        
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        });
                    
                };

                $scope.showcat = function(flag){
                    if (flag==1){
                        $scope.viewflag = 1;    
                    }
                    else{
                        $scope.viewflag = 0;
                    }
                    $scope.GetData();
                }
    });
</script>
<script>
    $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
        });
    $(document).on('click','.myPnopopupsubclose',function(){
        $("#Pnopopupsalestrends").css("display", "none");
    });
</script>
</html>