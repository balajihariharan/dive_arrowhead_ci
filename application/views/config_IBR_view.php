<!DOCTYPE html>
<html ng-app="IBRapp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
   
<body  class="main" ng-controller="IBR" ng-cloak>
    <?php $this->load->view('header.php'); ?>
    <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
 
      <div>
        
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

                 .itemIDCell {
                       margin: 0;
                  }
                 [ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak\
               {
                    display:none!important;
                }
                           
                /**********autocomplete suggestion***************/
                #searchResult{
                 /*list-style: none;
                 display :block;
                 padding: 0.1%;
                 width: 7.9%;
                 position: absolute;
                 margin: 0;
                 margin-left: 5.7%*/
                 /* padding: 0;
                    border: 1px solid black;
                    max-height: 7.5em;
                    overflow-y: auto;
                    width: 8.1%;
                    position: absolute;
                    margin-left: 7.1%;*/
                         border: 1px solid #aaa;
                    overflow-y: auto;
                    width: 164px;
                    position: absolute;
                    margin-left: 123px;
                    padding-bottom: 0px;
                    min-height: 10px;
                    max-height: 150px;
                }

                #searchResult li{
                 background: white;
                 padding: 4px;
                cursor: pointer;
                 list-style: none;
                    border-bottom: 1px solid #ededed;
                }
                #searchResult li:hover{
                    background: #f1f1f1;
                }
                /**************/

                #option1 option[value="? undefined:undefined ?"]{
                    display: none  ;
                }
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                   <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>

                <div id="msg" style="display: none;"></div>
      <form id="savedata" ng-submit="save_data()" method="post">  
        <div class="container">
            <div class="wrapper">
                
                <div class="content_main_all">
                    <div class="channel_main">
                        <div class="channel_all">
                            <label class="lable_normal">My Store User ID <span style="color:red; font-size:16px;">*</span> :</label>
                            <input type="text" ng-click= "flag()" name="CompName" class="input" id="searchcomp" ng-value="comptid"  ng-change="searchcomp() " ng-model="searchcomp1"  autocomplete="off" />
                            <ul id='searchResult'  ng-if="searchresultload">
                               <li ng-click='setValue($index)'   ng-repeat="search in renderselectvalue" >{{search.CompetitorName}}</li>
                            </ul>  

                        </div>
                  <div class="error_msg_all"  style="margin-right: 45px; display:none" id="Comp">Enter Competitor Name </div>
                    </div>
             
<div class="field_set">
     <fieldset>
            <legend class="legent_sty">Run IBR for</legend>
                <p class="input_field">
                    <input type="radio" class="check_box" name="selected" id="top_since_inc_flag"  ng-model="myradiocheck" ng-click="submitResult($event)"  ng-checked = "top_since_inc_flag == 1" value=""/>
                    <label class="label_inner_general">Top</label>
                    <input type="text" id="top1" name="top1" class="input_text" ng-model="top_since_inc">
                    <span> with alteast 1 Quantity Sold (since inc)</span>
                </p>

                <p class="input_field">
                    <input type="radio" class="check_box" name="selected" id="top_last_5_weeks_flag"  ng-click="submitResult($event)"  ng-checked = "top_last_5_weeks_flag == 1"  ng-value="top_last_5_weeks_flag" />
                    <label class="label_inner_general">Top</label>
                    <input type="text" id="top2" name="top2" class="input_text" ng-model="top_last_5_weeks">
                    <span> with alteast 1 Quantity Sold (Last 5 Weeks)</span>
                </p>

                <p class="input_field">
                    <input type="radio" class="check_box" ng-model="all_myitems" name="selected"  ng-click="submitResult($event)" ng-checked = "all_items ==1"  id="all_items" ng-value="all_items"/>
                    <label class="label_inner_general">All Items</label>
                </p>
                    
                <p class="input_field">
                    <input type="radio" class="check_box" value="" name="selected" id="list_of_items_flag"  ng-click="submitResult($event)" ng-checked ="list_of_items_flag==1" ng-value="list_of_items_flag"/>
                    <label class="label_inner_general">List of Items</label>
                    <textarea  name="list" ng-model="list_of_items" class="textarea"></textarea> 
                    
                </p>       
                   
                <p class="drop_down">
                 <input type="radio" class="check_box" name="selected" ng-click="submitResult($event)" ng-value="Categories_flag" ng-checked="true" ng-if="Categories_flag==1"  ng-model ="categories" id="Categories_flag" value="" />
                 <input type="radio" class="check_box" name="selected" ng-click="submitResult($event)" ng-value="Categories_flag"
                 ng-checked="false" ng-if="Categories_flag!==1"  ng-model ="categories" id="Categories_flag" value="" /> 

                    <label class="label_inner_general">Categories</label>
                    <select  ng-model="selected_category"   class="drop_down_med margin_top_10" id=option1>
                        <option value="" ng-selected="selected" >-select-</option>
                        <option  id ="options" ng-value="{{subcat.categoryid}}"   ng-model="loadedcategories"   ng-selected="subcat.subCategory == loadedcategories" ng-repeat="subcat in subcategories">{{subcat.subCategory}}</option>    
                    </select>
                </p>
                 <p>
                <p class="save_icon_manage">
                   <input type="submit" class="button_add rawlistdatasubmit" value="Save">
                </p>
                 <div class="error_msg_all" style=" display:none" id="msging_id">Please choose any one of option to configure IBR</div>
                 <div class="error_msg_all" style=" display:none" id="msg_id">Please Enter or Select the details in selected option</div>


 </fieldset>
 </div>
</div>
</div>
        
        </div>
        </form>
        
    
    
           <?php $this->load->view('footer.php'); ?>
    </div>
<script src="<?php echo  base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });


    </script>
    <script>
    var IBRapp = angular.module('IBRapp',[])
    IBRapp.controller('IBR',function($scope,$http){
                                 $http({
                                    method: 'POST',
                                    url: '<?php echo base_url();?>index.php/config_IBR/get_data',
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).then(function (response) {
                                    top_since_inc_flag_post         =response.data[0].top_since_inc_flag;
                                    top_last_5_weeks_flag_post      =response.data[0].top_last_5_weeks_flag;
                                    list_of_items_flag_post         =response.data[0].list_of_items_flag;
                                    Categories_flag_post            = response.data[0].categories_flag;
                                    all_items_post                  = response.data[0].all_items;
                                    $scope.top_since_inc_flag       = response.data[0].top_since_inc_flag;
                                    $scope.top_last_5_weeks_flag    = response.data[0].top_last_5_weeks_flag;
                                    $scope.list_of_items_flag       = response.data[0].list_of_items_flag;
                                    $scope.categories_flag          = response.data[0].categories_flag;
                                    $scope.searchcomp1              = response.data[0].CompetitorName;
                                    $scope.top_since_inc            = response.data[0].top_since_inc;
                                    $scope.top_last_5_weeks         = response.data[0].top_last_5_weeks;
                                    $scope.all_items                = response.data[0].all_items;
                                    $scope.list_of_items            = response.data[0].list_of_items;
                                    $scope.loadedcategories         = response.data[0].category;
                                    selected_category_input         = response.data[0].categories;
                                    $scope.ngcompetitorkey();
                                    });  


                                    $scope.flag = function(){
                                    $scope.top_since_inc            = '';
                                    $scope.top_last_5_weeks         =''; 
                                    $scope.all_items                ='';
                                    $scope.list_of_items            = '';
                                    $scope.loadedcategories         = '';
                                    //$scope.loadedcategories         = 0;
                                    $scope.top_since_inc_flag       = 0;
                                    $scope.top_last_5_weeks_flag    = 0;
                                    $scope.all_items                = 0;
                                    $scope.list_of_items_flag       = 0;
                                    $scope.categories_flag          = 0;
                                    top_since_inc_flag_post         = 0;
                                    top_last_5_weeks_flag_post      = 0;
                                    all_items_post                  = 0;
                                    list_of_items_flag_post         = 0;
                                    Categories_flag_post            = 0;    
                                    selected_category_input         = 0;   
                                  
                                    } 

                                    $scope.ngcompetitorkey = function(){
                                         $http({                     
                                          method  : 'POST',
                                          url     : '<?php echo base_url();?>index.php/config_IBR/competitor_category',
                                          data    : {'competitor':$scope.searchcomp1},
                                          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                         })
                                          .success(function(data) {
                                            $scope.subcategories = data;
                                                    
                                          });
                                    }

                                    $scope.searchcomp = function() {
                                         if($scope.searchcomp1 == ''){
                                                $scope.searchresultload = false;
                                                return false;
                                            }
                                        $http({
                                                method  : 'POST',
                                                url     : '<?php echo base_url();?>index.php/config_IBR/compload',
                                                data    : {'comp': $scope.searchcomp1},
                                                header  : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                            })
                                          .success(function(data) {
                                             $scope.selected_category='';
                                             $scope.searchresultload = true;
                                             $scope.searchresult = data;
                                             var selectvalue=[];
                                                        angular.forEach(data, function(value, key){
                                                            selectvalue.push({'compid' :value.CompID,'CompetitorName':value.CompetitorName});
                                                       });
                                                        if(selectvalue.length == 0){
                                                            $scope.searchresultload = false;
                                                        }
                                                        if($scope.searchcomp1 == ''){
                                                             $scope.searchresultload = false;
                                                        }
                                                        if($scope.searchcomp1 != ''){
                                                            $scope.renderselectvalue = selectvalue;
                                                             $scope.ngcompetitorkey();   

                                                        }

                                          });  
                                            $scope.setValue = function(index){
                                                
                                            
                                                $scope.searchcomp1 = $scope.renderselectvalue[index].CompetitorName;
                                                $scope.ngcompetitorkey();  
                                                $scope.comptid = '';
                                                $scope.comptid = $scope.renderselectvalue[index].compid; 
                                                $scope.searchresultload = false;

                                               }  

                                        }
                                                
                                                 var selected_category_input = '';
                                                $scope.$watch('selected_category',function(newvalue,oldvalue){
                                                    selected_category_input = newvalue;

                                                });
                                                var top_since_inc_flag_post = '';
                                                var top_last_5_weeks_flag_post = '';
                                                var all_items_post = '';
                                                var list_of_items_flag_post = '';
                                                var Categories_flag_post = '';    
                                                var oldcategories = '';
                                                $scope.$watch('subcategories',function(newvalue,oldvalue){
                                                    oldcategories = oldvalue;
                                                });
                                                var ids = '';
                                                $scope.submitResult = function(evt) {
                                                     ids = evt.target.getAttribute('id');
                                                     if(ids == 'top_since_inc_flag'){
                                                        top_since_inc_flag_post = 1;
                                                        top_last_5_weeks_flag_post = 0;
                                                        all_items_post=0;
                                                        list_of_items_flag_post=0;
                                                        Categories_flag_post=0;
                                                        selected_category_input = '';
                                                        all_items_post = '';
                                                        $scope.top_last_5_weeks = '';
                                                        $scope.list_of_items = '';
                                                        $scope.subcategories = '';
                                                        $scope.top_since_inc = '';
                                                     }
                                                     if(ids == 'top_last_5_weeks_flag'){
                                                        top_since_inc_flag_post = 0;
                                                        top_last_5_weeks_flag_post = 1;
                                                        all_items_post=0;
                                                        list_of_items_flag_post=0;
                                                        Categories_flag_post=0;
                                                        selected_category_input = '';
                                                        $scope.top_last_5_weeks = '';
                                                        all_items_post = '';
                                                        $scope.top_since_inc = '';
                                                        $scope.list_of_items = '';
                                                        $scope.subcategories = '';
                                                         }
                                                     if(ids == 'all_items'){
                                                        top_since_inc_flag_post = 0;
                                                        top_last_5_weeks_flag_post = 0;
                                                        all_items_post=1;
                                                        list_of_items_flag_post=0;
                                                        Categories_flag_post=0;
                                                        selected_category_input = '';
                                                        $scope.top_since_inc = '';
                                                        $scope.top_last_5_weeks = '';
                                                        $scope.list_of_items = '';
                                                        $scope.subcategories = '';
                                                     }
                                                     if(ids == 'list_of_items_flag'){
                                                        top_since_inc_flag_post = 0;
                                                        top_last_5_weeks_flag_post = 0;
                                                        all_items_post=0;
                                                        list_of_items_flag_post=1;
                                                        Categories_flag_post=0;
                                                        all_items_post = '';
                                                        selected_category_input = '';
                                                        $scope.top_since_inc = '';
                                                        $scope.top_last_5_weeks = '';
                                                        $scope.subcategories = '';
                                                     }
                                                     if(ids == 'Categories_flag'){

                                                        $scope.ngcompetitorkey();
                                                        $scope.loadedcategories='';
                                                        selected_category_input = '';   
                                                        top_since_inc_flag_post = 0;
                                                        top_last_5_weeks_flag_post = 0;
                                                        all_items_post=0;
                                                        list_of_items_flag_post=0;
                                                        Categories_flag_post=1;
                                                        all_items_post = '';
                                                        $scope.top_since_inc = '';
                                                        $scope.top_last_5_weeks = '';
                                                        $scope.list_of_items = '';
                                                        
                                                     }
                                                }

                                            $scope.save_data = function() {
                                                var comp_name = $scope.searchcomp1;
                                                //competitor name validation;
                                                if( comp_name == null|| comp_name =="")
                                                {    $("#Comp").css("display","block");
                                                    //alert ('Enter Competitor Name');
                                                     document.getElementById('searchcomp').style.borderColor = "red";
                                                    return false;

                                                }
                                                else
                                                {    document.getElementById('searchcomp').style.borderColor = "";
                                                     $("#Comp").css("display","none");

                                                }
                                                //validation;
                                                if((top_since_inc_flag_post!= 1)&&(top_last_5_weeks_flag_post != 1)&&(all_items_post!= 1)
                                                   &&(list_of_items_flag_post!= 1)&&(Categories_flag_post!= 1) )
                                                {     $("#msging_id").css("display","block");
                                                    //alert('Please choose any one of option to configure IBR');
                                                    return false;
                                                }
                                                else
                                                {
                                                    $("#msging_id").css("display","none");
                                                }

                                                var top_since_inc_input = $scope.top_since_inc;
                                                var top_last_5_weeks_input = $scope.top_last_5_weeks;
                                                var all_myitems_input = $scope.all_myitems
                                                var list_of_items_input = $scope.list_of_items;

                                                //validation;
                                                if((top_since_inc_input==0) || (top_since_inc_input == null)){
                                                   if((top_last_5_weeks_input==0) || (top_last_5_weeks_input== null)){
                                                    if(all_items_post!= 1){
                                                          if((list_of_items_input=='')|| (list_of_items_input==null)){
                                                            console.log(selected_category_input);
                                                                 if((selected_category_input== null) || (selected_category_input== 0)){
                                                                      console.log('innerloop');
                                                                     console.log(selected_category_input);
                                                                    $("#msg_id").css("display","block");
                                                                     //alert('Please enter or select the details in selected option');
                                                                      return false;
                                                             
                                               }}}}}
                                                if(((top_since_inc_input==0) || (top_since_inc_input == null))||((top_last_5_weeks_input==0) || (top_last_5_weeks_input== null)) || (all_items_post!= 1) || 
                                                    ((list_of_items_input=='')|| (list_of_items_input==null)) || ((selected_category_input== null) || (selected_category_input== 0)))
                                                {
                                                   $("#msg_id").css("display","none"); 
                                                }
                                             
                                                                  
                                                $http({
                                                        method: 'POST',
                                                        url: '<?php echo base_url();?>index.php/config_IBR/save',
                                                        data: {  
                                                                'compname':comp_name,
                                                                'firsttop':top_since_inc_input,
                                                                'secondtop':top_last_5_weeks_input,
                                                                'allitems':all_items_post,
                                                                'listitems':list_of_items_input,
                                                                'selectedcat':selected_category_input,
                                                                'firsttop_flag':top_since_inc_flag_post,
                                                                'secondtop_flag':top_last_5_weeks_flag_post,
                                                                'listitems_flag': list_of_items_flag_post,
                                                                'Categories_flag':Categories_flag_post
                                                            },
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                                    }).success(function(response) {
                                                        //alert('updated successfully');
                                                        $http({                     
                                                                method  : 'POST',
                                                                url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                                data    : 3
                                                            })
                                                             .success(function(response) {
                                                                $('#msg').html(response);
                                                                $('.err_modal').css('display','block');
                                                                $('#msg').css('display','block');                  
                                                                $scope.isLoading = false;
                                                            });
                                                    })
                                                        //alert('error');
                                                        .catch(function (err) {
                                                              var msg =err.status;
                                                        //failure msg
                                                                 $http({                     
                                                                    method  : 'POST',
                                                                    url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                                                      data    : msg
                                                                 })
                                                                         .success(function(response) {
                                                                            $('#msg').html(response);
                                                                            $('.err_modal').css('display','block');
                                                                            $('#msg').css('display','block');                  
                                                                            $scope.isLoading = false;
                                                                        });
                                                         //$scope.isLoading = false;
                                                      });
                                                   
                                            }



                         });

</script>
<script>
    //new popup msg close button
   $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
</script>
</body>
</html>
