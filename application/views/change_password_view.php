<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <meta charset="utf-8"/>
    <title>Romaine Electric : Change Password</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_romaine.css" />
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
 </head>
<style>
     .err_modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
</style>
<body ng-controller="myController" ng-cloak>

<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>


<div class="container head_bg">
        <div class="head">
                <div class="logo">
                    <a href='<?php echo base_url();?>DashBoard/index'> <img src="<?php echo base_url().'assets/';?>img/dive_logo_small.jpg"></a>
                </div>
                <div class="nav nav_bg">
                <ul>
                   
                </ul>
        </div>  
        </div>
    </div>
    <div id="msg" style="display: none;"></div>
    <div class="bg">
        <div class="container">
            <div class="wrapper">
                <div class="login_main" id="login_main">
                	<div>
	                    <h1 style="font-size: 18px; color: #ec3a3a; font-family: 'Open Sans', sans-serif;">Change Password</h1>
	                    <form ng-submit="passwrd_submit()">
                        <p style="padding: 8px 0; font-family: sans-serif;    font-size: 19px; color: #000;">Password : </p>
	                     	<div class="ps_main">
	                            <input name="Password1" type="Password" placeholder="Password" ng-model="passwrd1" class="ps_admin" required/>
	                        </div>
                        <p style="padding: 8px 0; font-family: sans-serif;    font-size: 19px; color: #000;">Confirm Password :</p>
                            <div class="ps_main">
                                 <input name="Password2" type="Password" placeholder="Password" ng-model="passwrd2" class="ps_admin" required/>
                            </div>
                           <input type="hidden" name="login" id="login_id" value="<?php echo $this->session->userdata('loginid'); ?>">
	                        <div>
	                            <button type="submit" class="submit_btn" style="background: #26a69a;">Submit</button>
	                        </div>
	                     </form>
                   	</div>
                </div>
            </div>
        </div>
    </div>
         <script src="<?php echo base_url().'assets/';?>js/jquery-1.11.3.min.js"></script>
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script>
    	 var app = angular.module('myApp',[]);
        app.controller('myController',function($scope,$http){
            
        	$scope.passwrd_submit = function(){
                var pass1 = $scope.passwrd1;
                var pass2 = $scope.passwrd2;
                var sess_loginid = $('#login_id').val();
                console.log(sess_loginid);
                if (pass1 != pass2) {
                    alert("Passwords Do not match");
                    return false;
                }
                else {
                    $scope.isLoading =true;
                    vars = window.location.href;
                    var id = vars.split("/");
                        if(sess_loginid == ''){
                            var loginid =id[7];
                        }
                        else{
                            var loginid =sess_loginid;
                            //console.log(loginid);
                        }
        		$http({
        			method : 'POST',
        			url    :'<?php echo base_url();?>index.php/ChangePassword/change_password',
        			data   : {'passwrd':$scope.passwrd2,'loginid':loginid},
        			headers:{'Content-Type': 'application/x-www-form-urlencoded'}
        			})
        		.success(function(data){
        			$scope.passresponse = data;
                    $scope.isLoading =false;

                        $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                             data    : 9
                            })
                        .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block');                  
                                $scope.isLoading = false;
                        });
        		})
                 .catch(function (err) {
                                  var msg =err.status;
                             //failure msg(db error)
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');                  
                                    $scope.isLoading = false;
                                });
                 });    

                 }
        	}

        });

    </script>
     <script>
            //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            // location.reload(true);
            window.location.replace('<?php echo base_url();?>');
        });
       </script>

</body>
</html>