<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> 

            <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    
    <link rel="stylesheet" href="<?php echo  base_url();?>assets/css/style_romaine.css" /> 
    
    <link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
    
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/jquery-ui.css"/>

    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/popup.css"/>
 <!--     <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">   -->
  <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
  <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script> 
  <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script>
  <!-- <script src="<?php echo base_url().'assets/';?>js/jquery.multiselect.js"></script>  -->
  <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script> 
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>
  
</head>
<body class="main" ng-controller="postController" ng-cloak>


<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

     <!-- <div class="container head_bg"> -->
   <?php $this->load->view('header.php'); ?>



  <style>
    .button_rightnew {
    float: right;
    padding: 10px 10px 0;
}
</style>
    <div class="container header_bg_clr">
        <div class="wrapper">
        <div class="head_menu">
            <div class="header_main">
                <?php echo "<h2>".$screen."</h2>" ;?>
                <span class="error_msg_head"></span>
                </div>
                <div class="no_competitor"><span>No. of Competitors :</span><?php if(count($metrics)>=1){echo $metrics[0]['NoofComp'];} ?></div>
                <div class="unique_sku"><span>Unique SKU :</span><?php if(count($metrics)>=1){echo $metrics[0]['UniqueSKU'];} ?></div>
                <div class="unique_cate"><span>Unique Categories :</span><?php if(count($metrics)>=1){echo $metrics[0]['UniqueCategories'];} ?></div>
                <div class="total_items"><span>TotalItems :</span><?php if(count($metrics)>=1){echo $metrics[0]['TotalItems'];} ?></div>
                <div  class="total_sales"><span>TotalSales :</span><?php if(count($metrics)>=1){echo "$". $metrics[0]['TotalSales'];} ?></div>
                
            </div>
        </div>
    </div>  
    

     <div class="container">
        <div class="wrapper">
        <div class="content_main_all">
        <div class="form_head">
        <form id="masterlistform" class="raw_list_sec_form" ng-submit="submitForm()">
            
                <div class="field">
                    <div class="lable_normal">
                        Competitor Details <span style="color:red; font-size:16px;">*</span>  : 
                    </div>
              <!--           <select name="langOptfirst[]" id="langOptfirst" multiple name="competitorname[]" ng-change="ngcompetitor()" class="chooseallcategory" ng-model="competitorname">                    
                        <?php if(count($comptitornameandid) >= 1){
                            foreach($comptitornameandid as $value){ ?>
                                <option value="<?php echo $value->CompID;?>"><?php echo $value->CompetitorName;?></option>  
                        <?php   }
                        } ?>                            
                        </select>  -->

                        <select name="competitorname[]" ng-change="ngcompetitor()" class="select" ng-model="competitorname" id="competitorname">
                            <option value="">-Select-</option>
                            <?php if(count($comptitornameandid) >= 1){
                              foreach($comptitornameandid as $value){ ?>
                                <option value="<?php echo $value->CompID;?>"><?php echo $value->CompetitorName;?></option>  
                            <?php }
                            } ?>
                      </select> 
                </div>
                
                <div class="field">
                    <div class="lable_normal" >
                        Category Details <span style="color:red; font-size:16px;">*</span>  : 
                    </div>
                  <!--    <select ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" id="langOpt1" multiple name="category[]" ng-model="competitorcategory"></select> -->

                     <select  name="category[]"  ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" ng-model="competitorcategory" id="compcat" class="select">
                      <option value="">-None-</option>
                      </select> 
                </div>
   
                <div class="field">
                <button value="Search" class="button_search" id="btnsearch" type="submit">Search</button>
                </div>

           
               </form>
          <center>   <div  class="error_msg_all"  style=" display:none" id="msg_id">Please select Competitor & Category</div> </center>
               <div class="button_rightnew">
                        <a class="last_menu_icon" href="#">
                        <input type="button" class="export_button" ng-click="exportDataExcelComp()" value="Export Complete Master List"/>
                        </a>

                            <ul class="level2">
                                <!-- <li ><a herf="#" class="last_menu_icon" ng-csv="ForCSVExport1" filename="DIVE_Complete_MasterList.csv">As CSV<a/></li>    

                                <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel1()">As Excel</a></li> -->

                               <!--  <li><a ng-href="<?php echo base_url();?>Masterlist/export" class="last_menu_icon">As Pdf<a/></li> -->

                            </ul>
                        </li>
                </div>

               <div class="button_right">
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="export_button" ng-click="exportDataExcel()" value="Export"/>
                            </a>

                            <ul class="level2">
                                <!-- <li ><a herf="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_MasterList.csv">As CSV<a/></li>    

                                <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel()">As Excel</a></li> -->

                                <!-- <li><a ng-href="<?php echo base_url();?>Masterlist/export" class="last_menu_icon">As Pdf<a/></li> -->
                            </ul>
                        </li>
                </div>



  </div>




    <br/>
    <div style="color:#009e47; text-align:center; font-size:20px">{{message}}</div>
     <div id="msg" style="display: none;"></div>
      <center><div class="addbtn" style="color:red; text-align:center; display:none">Please Select Atleast One</div></center>
        <!-- <form ng-submit="removefrommasterlist()"  id="removeform" style="min-height:800px;"> -->
            <form ng-submit="removefrommasterlist()"  id="removeform">
                <div class="plan_list_view">
                    <div class="clear"></div>

                    <div class="lable_normal table_top_form"><input id="ckbCheckAll" type="checkbox" ng-model="checked"  />
                    Select All - Across Pages</div>

                        <div class="itm_lst" ng-repeat="incitem in inceptitem">
                            <div class="itm_lst">
                                 <p>Unique Competitor SKU:</p>
                                 <p style="font-size: 20px; color: #00886d;">{{incitem.TotalSku}}</p>
                            </div>
                             <div class="itm_lst">
                                 <p>Total item listed:</p>
                                 <p style="font-size: 20px; color: #00886d;">{{incitem.TotalListings}}</p>
                            </div>
                             <div class="itm_lst"> 
                                 <p>Total Sales Since Inception:</p>
                                 <p style="font-size: 20px; color: #00886d;">${{incitem.TotalSales}}</p>
                            </div>
                            <div ng-if="incitem.InceptionDate" class="itm_lst">
                                 <p>Inception On:</p>
                                 <p style="font-size: 20px; color: #00886d;">{{incitem.InceptionDate}}</p>
                            </div>
                             <div ng-if="!incitem.InceptionDate" class="itm_lst">
                                 <p>Inception On:</p>
                                 <p style="font-size: 20px; color: #00886d;">N/A</p>
                            </div>
                        </div>
                        
                    
                    
                    <table cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
                                <th>Sel.</th>
                                <th class="curser_pt" ng-click="sortBy('CompetitorName')">Competitor</th>
                                <th class="curser_pt" ng-click="sortBy('Category')">Category</th>
                                <th class="curser_pt" ng-click="sortBy('ItemID')">Item ID</th>
                                <th ng-click="sortBy('mypartno')">My Part #</th>
                                <th class="curser_pt" ng-click="sortBy('SKU')">SKU</th>
                                <th class="curser_pt" ng-click="sortBy('Title')">Title</th>
                                <th class="curser_pt" ng-click="sortBy('price')">Price (in $)</th>
                                <th class="curser_pt" ng-click="sortBy('ListingDate')">Listing Date</th>
                                <th class="curser_pt" ng-click="sortBy('Qtysold')">Qty.Sold (Inc)</th>
                                <th class="curser_pt" ng-click="sortBy('lastweekQtysold')">Qty.Sold (Last Wk)</th>
                                
                            </tr>

                        </thead>

                        <tfoot>
                            <tr>
                            <td><input id="selpage" type="checkbox" ng-click="selectpage()" class="ckeckclass" ng-model="haschecked" /></td>
                                <!-- <td><input id="selectAll" type="checkbox" ng-model="checked" /></td> -->
                                <td>
                                  <div class="search"><input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch"></div>
                                </td>
                                 <td>
                                    <div class="search">
                                   <input type="text" ng-model="categoryquery" ng-change="categorysearch()" class="inputsearch">
                                   </div>  
                                </td>
                                <td> 
                                   <div class="search">
                                   <input type="text" ng-model="itemquery" ng-change="myitemsearch()" class="inputsearch">
                                   </div>
                                </td>
                                <td>
                                     <div class="search">
                                    <input type="text" ng-model="partquery" ng-change="mypartsearch()" class="inputsearch">
                                    </div>  
                                </td>
                                <td>
                                    <div class="search">
                                    <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch">
                                    </div>   
                                </td>
                              
                                <td>
                                    <div class="search">
                                        <input type="text" ng-model="titlequery" ng-change="mytitlesearch()" class="inputsearch">
                                    </div>   
                                </td>
                               
                                <td>
                                    <div class="search">
                                    <input type="text" ng-model="pricequery" ng-change="mypricesearch()" class="inputsearch">
                                    </div>   
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            
                            </tr>
                       </tfoot>   
                      
                               <!--  <tr ng-repeat="items in masterlistitem | filter:search | orderBy:propertyName:reverse" > -->
                                  <tr ng-repeat="items in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                    <td class="removeitem_{{items.ItemID}}"><input type="checkbox"  ng-model="items.selected" ng-checked="checked" value="{{items.ItemID}}"  name="removefrommasterlist[]" id="{{items.ItemID}}" class="ckeckclass" ></td>
                                    <td class="removeitem_{{items.ItemID}}">{{items.CompetitorName}}</td>
                                    <td class="removeitem_{{items.ItemID}}">{{items.Category}}</td>
                                    <td class="removeitem_{{items.ItemID}}">{{items.ItemID}}</td>

                                    <td ng-if="items.mypartno" class="mymodalmaintable removeitem_{{items.ItemID}}" data-value="{{items.ItemID}}" my-partnumber="{{items.mypartno}}" my-compid="{{items.compid}}" id="mypartnumber{{items.mypartno}}" ><a href="#" style="color: #0094DE;" id="updatedpartnumber{{items.ItemID}}" >{{items.mypartno}}</a></td>
                                    <td ng-if="!items.mypartno" class="mymodalmaintable removeitem_{{items.ItemID}}" data-value="{{items.ItemID}}" my-partnumber="{{items.mypartno}}" my-compid="{{items.compid}}" id="mypartnumber{{items.mypartno}}" ><a href="#" style="color: #0094DE;" id="updatedpartnumber{{items.ItemID}}" >N/A</a></td>

                                    <td class="removeitem_{{items.ItemID}}">{{items.SKU}}</td>
                                    <td class="removeitem_{{items.ItemID}}">
                                    <a ng-click="redirectToLink($event)" element="{{items.ItemID}}" style="cursor: pointer;">{{items.Title}}
                                    </a>
                                    </td>
                                    <td class="removeitem_{{items.ItemID}}">{{items.price}}</td>
                                    <td class="removeitem_{{items.ItemID}}">{{items.ListingDate}}</td>
                                    <td class="removeitem_{{items.ItemID}}">
                                      <!-- <span ng-if="items.CompetitorName.includes('IBR') && !items.Qtysold" >N/A</span>
                                    <span ng-if="items.CompetitorName.includes('CBR') && !items.Qtysold" >{{items.Qtysold}}</span>
                                    <span ng-if="items.Qtysold" > -->{{items.Qtysold}}<!-- </span> --></td>
                                    <td class="removeitem_{{items.ItemID}}">
                                       <span ng-if="items.CompetitorName.includes('[IBR]') && !items.lastweekQtysold" >N/A<br></span>
                                        <span ng-if="items.CompetitorName.includes('[CBR]') && !items.lastweekQtysold" >{{items.lastweekQtysold}}</span>
                                        <span ng-if="items.lastweekQtysold" >{{items.lastweekQtysold}}</span></td>
                                    
                                </tr>

                           
                    </table>
                        
                </div>

                 <!-- pagination ui part -->
                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>
                        </div>
                
                <div style="text-align:center;">
                <input class="button_delete" value="Remove from Master List" type="submit"   />
                </div>

            </div>

            </form>


        
        
        

 <!-- popup start -->    
        <!-- ********************************** -->
                                    <form id="popupform" ng-submit="submitmainForm()">
                                        <div id="Pnopopup" class="modal">
                                               <div class="pop_map_my_part">
                                                
                                                <!--  <center><div class="successupdate" style="display:none;">Update Successfully</div></center> -->
                                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                                   <center><div class="pnum" style="display:none;">Please Enter Part Number</div></center>
                                                 <div class="pop_container">
                                                            <div class="heading_pop_main">
                                                                <h4>
                                                                    Map my Part #
                                                                </h4>
                                                                <span id="myPnopopup{{items.ItemID}}" class="close">&times;</span>
                                                            </div>
                                                            <div class="pop_pad_all">
                                                                <div class="field_12">
                                                                    <div class="lable_normal">
                                                                        <label class="normal_label">Item ID</label>
                                                                        <label id="displayitemid"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="field_12">
                                                                                    <div class="lable_normal">
                                                                                      <label class="normal_label">My Part Number </label>
                                                            <input type="hidden" id="displaycompidhidden" name="maincompetitorid" ng-model="mainpopup.maincompetitorid" value=""/>
                                                          <!--   <input type="text" id="runpartnumber" name="maincompitemid" ng-model="mainpopup.maincompetitorpartnumber" 
                                                            value=""/> -->
                                                              <input type="text" id="runpartnumber" name="maincompitemid" ng-change="partnosearch()" ng-model="searchpart.partno" value="" autocomplete="off"/>
                                                           <!--  <input type="text" id="runpartnumber" ng-model="searchpart"/>  ng-if="searchresultload"-->


                                                           <ul id='searchResult'>
                                                          <li ng-click='setValue(result.partno)' ng-repeat="result in suggest_partno | filter : searchpart" >{{ result.partno }}</li>
                                                           </ul>


                                                            <input type="hidden" name="mainmypartnumber" ng-model="mainpopup.maincompetitoritemid" id="displayitemidhidden" value=""/>
                                                            
                                                           
                                                                    </div>  
                                                                </div>
                                                                <div class="button_center">
                                                                    <img style="display:none;" class="ajaxloader" src='<?php echo base_url().'assets/';?>img/ajax-loader.gif'/>
                                                                    <button type="submit" id="popsavemain" class="button_upload">Save</button>
                                                                </div>
                                                            </div>
                                                </div>
                                               </div>
                                            </div>
                                    </form>

            </div>
        </div>
</div>
                        
               <?php $this->load->view('footer.php'); ?>
                                
                            
           </body>
</html>


<script>
    $(document).ready(function() {
        $(document).on('click','.mymodalmaintable',function(){
            console.log('hi');
            var datavalue = $(this).attr("data-value");
            var partno = $(this).attr('my-partnumber');
            var compid =$(this).attr('my-compid');
            $('#runpartnumber').val(partno);  
            $("#Pnopopup").css("display", "block");
            $('#searchResult').hide();
            $('#displayitemid').text(datavalue);
            $('#displayitemidhidden').val(datavalue);
            $('#displaycompidhidden').val(compid);
            /*$('#mainpartnumber').val(partno);*/
            

        });

          $(document).on('click','.close',function(){
          var idvalue = $(this).attr("id");
          $("#Pnopopup").css("display", "none");
          $('.pnum').css("display", "none");
           $('#searchResult').css("display", "none");
          $('.successupdate').hide();
          $('.failureupdate').hide(); 
          $('#runpartnumber').val('');     
        });
    });
  $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");  
        });
  $(document).on('click','.destroy2',function(){
          $(".err_modal").css("display", "none");
          $("#Pnopopup").css("display", "block");   
         });
 </script>


 <script>
      $('#compcat').click(function(e){ 
      if ($('#competitorname').val() != ""){
          document.getElementById('competitorname').style.borderColor = ""; 
          }
         });

$('#btnsearch').click(function(e){ 
      
      if ($('#competitorname').val() == "" || $('#compcat').val() == "") {
                 $("#msg_id").css("display","block");
                 var competitorname=document.getElementById('competitorname').value;
                   if(competitorname == ""){
                        document.getElementById('competitorname').style.borderColor = "red";
                       // return false;
                    }
                    else
                    {
                       document.getElementById('competitorname').style.borderColor = "";
                    }

                  var compcat=document.getElementById('compcat').value;
                   if(compcat == ""){
                        document.getElementById('compcat').style.borderColor = "red";
                        return false;
                    }  
                   /* $("#msg_id").css("display","block");
                    $('.addbtn').css('display','none');
                    $("#ebay_msg_id").css("display","none");
                    $("#success_msg").css("display","none");*/
                    e.preventDefault();
                  
     
            }
            else
            {   $("#msg_id").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                document.getElementById('compcat').style.borderColor = "";   
                 /* $("#msg_id").css("display","none");
                    $('.addbtn').css('display','none');
                    $("#ebay_msg_id").css("display","none");
                    $("#success_msg").css("display","none");*/
            }
        });


</script>


 <script>
        var exportdata=[];
        
         
            var sortingOrder = 'CompetitorName';
            var postApp     = angular.module('postApp', ['ngSanitize', 'ngCsv']);
            postApp.controller('postController', function($scope, $http, $timeout,$window,$filter) {
                    $scope.sortingOrder = sortingOrder;
                    $scope.reverse = false;
                    $scope.filteredItems = [];
                    $scope.groupedItems = [];
                    $scope.itemsPerPage = 25;
                    $scope.pagedItems = [];
                    $scope.pagenos = [];
                    $scope.masterlistitem = '';
                    $scope.currentPage = 0;



            //Click the button to open an new browser window that is 1200px wide and 600px tall
                 $scope.redirectToLink = function (obj) {
                       var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                    };



                //popup save button function
                 var timeoutinstance;
                $scope.submitmainForm = function() {
                    $('.failureupdate').hide();
                    $('.pnum').hide();
                  
                  var partarray =[];
                   angular.forEach($scope.suggest_partno, function(data){       
                        if(data.partno == $('#runpartnumber').val()){
                         partarray = data.partno;
                         } 
                   });  
                       if (partarray.length == 0) {    
                             console.log('not matched');
                              partarray = [];
                        //return false;
                           $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_configcbr',
                                  data    : 003
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    $("#Pnopopup").css("display", "none");                 
                                    $scope.isLoading = false;
                                });
                  }    
                 if (partarray.length != 0)
                 {
                  $scope.isLoading = true; //Loader Image
                     $scope.isLoading = true; //Loader Image
                      $http({                     
                      method  : 'POST',
                      url     : '<?php echo base_url();?>index.php/Masterlist/competitor_mypartnumber_update',
                      data    :  {'items':angular.element('#popupform').serializeArray(),'partnum':partarray},
                      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                     })
                      .success(function(data) {
                             $scope.isLoading = false;
                             if(data != 'Failure'){
                                $('#runpartnumber').val('');
                                $(".button_search").trigger( "click" );
                                $('#updatedpartnumber'+data.itemid).html(data.mypartno);
                               // $('.successupdate').show();
                                   $http({                     
                                             method  : 'POST',
                                             url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                             data    : 3
                                            })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('#Pnopopup').css('display','none');
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                  
                                                $scope.isLoading = false;
                                            })
                             } 
                             else{
                                $('#updatedpartnumber').val();
                                $('.failureupdate').show(); 
                             }
                      })

                        .catch(function (err) {

                        var msg = err.status;
                         //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });

                         //failure msg
                            $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data    : msg
                            })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    $("#Pnopopup").css("display", "none");                 
                                    $scope.isLoading = false;
                                });
                    })  
                    } 
                };
              


                //category loading function
                $scope.ngcompetitor = function() {
                      $scope.isLoading = true; //Loader Image
                      $scope.inceptitem = '';
                      $scope.pagedItems = [];
                    $scope.masterlistitem  = '';
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Masterlist/competitor_category',
                  data    : {'competitor':$scope.competitorname},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                      $scope.isLoading = false; //Loader Image
                    $scope.subcategories = data['competitor_category_result'];
                    $scope.suggest_partno = data['partno_result'];
                   $scope.filteredItems1 = $scope.suggest_partno; 
                    
                  })
                   .catch(function (err) {
                          var msg = err.status;
                           //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                         
                        })
                };


                 //search part no in popup window 
                $scope.partnosearch = function () {
                  $('#searchResult').show();  
                               $scope.filteredItems1 = $filter('filter')($scope.suggest_partno, function (item) {
                                    for(var attr in item) {
                                       if(attr == "PartNo") {
                                           if (searchMatch(item[attr], $scope.searchpart.partno)  > -1){ 
                                     return true;
                                           }
                                          }
                                       }
                                   return false;
                               });
                };
                // Set value to search box
                $scope.setValue = function(index){
                 $('#runpartnumber').val(index);  
                    //$scope.searchresultload = true;
                    $('#searchResult').hide();                    
                 }
                 

                //masterlist competitor search function
                $scope.submitForm = function() {
                $scope.message = '';
                $('.spinner').show();
                $scope.isLoading = true; //Loader Image
                $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Masterlist/masterlist_details_control',
                  data    : {'competitor':$scope.competitorname,'category':$scope.competitorcategory.Categoryid},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    $('.spinner').hide();
                     $scope.isLoading = false; //Loader Image
                     var exportdata_exc = [];
                      exportdata.push ({'CompetitorName' : 'Competitor','Category':'Category','ItemID' : 'Item ID','mypartno' : 'My Part#','SKU' : 'SKU','Title' : 'Title','price' : 'Price (in $)','ListingDate':'Listing Date','Qtysold':'Qty.Sold (Inc)','lastweekQtysold':'Qty.Sold (Last Wk)'});
                    $scope.inceptitem = data.itemdetail;
                    $scope.masterlistitem  = data.masterdetail;
                    $scope.masterlistitem.forEach(function (part) {
                        if (part.mypartno != '')
                        {
                            exportdata.push({'CompetitorName' : part.CompetitorName,'Category':part.Category,'ItemID' : "'"+part.ItemID,'mypartno' : 'N/A','SKU' : part.SKU,'Title' : part.Title,'price' : part.price,'ListingDate': part.ListingDate,'Qtysold':part.Qtysold,'lastweekQtysold':part.lastweekQtysold});
                            exportdata_exc.push({'Competitor' : part.CompetitorName,'Category':part.Category,'Item ID' : "'"+part.ItemID,'My Part#' : 'N/A','SKU' : part.SKU,'Title' : part.Title,'Price (in $)' : part.price,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.Qtysold,'Qty.Sold (Last Wk)':part.lastweekQtysold});
                        }
                        else
                        {
                             exportdata_exc.push({'Competitor' : part.CompetitorName,'Category':part.Category,'Item ID' : "'"+part.ItemID,'My Part#' : part.mypartno,'SKU' : part.SKU,'Title' : part.Title,'Price (in $)' : part.price,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.Qtysold,'Qty.Sold (Last Wk)':part.lastweekQtysold});
                        }

                        });
                       
                       $scope.ForCSVExport = exportdata;
                       $scope.ForExcelExport = exportdata_exc;
                    
                     //pagination part start 
                        // *********************
 
                            $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };

                            var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                if(haystack !== null){                                       
                                        return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                };

                             // *****************init the filtered items*****************
                           $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                           if(attr == "CompetitorName") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.categorysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                        
                                        if(attr == "Category") {
                                            if (searchMatch(item[attr], $scope.categoryquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.myitemsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                       
                                        if (attr == "ItemID") {
                                            if (searchMatch(item[attr], $scope.itemquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.mypartsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                      
                                        if(attr == "mypartno") {
                                            if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.myskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                      
                                        if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.mytitlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                       
                                         if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.mypricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.masterlistitem, function (item) {
                                    for(var attr in item) {
                                       
                                        if(attr == "price") {
                                            if (searchMatch(item[attr], $scope.pricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             // *****************finish the filtered items*****************


                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                     $('#ckbCheckAll').trigger('click');
                                      $('#ckbCheckAll').trigger('click');
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                     $('#ckbCheckAll').trigger('click');
                                      $('#ckbCheckAll').trigger('click');
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                                $('#selpage').prop('checked',false);
                                 
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end

                    $scope.search = {};
                    $scope.isLoading = false; //Loader Image

                    $scope.exportDataExcel = function () {
                        var mystyle = {
                            sheetid: 'Master List',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_MasterList.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    };

                  })
                     .catch(function (err) {
                       var msg =err.status;
                        //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                     
                                        $scope.isLoading = false;
                                    });
                         
                        })
                   .finally(function () {
                     $('.spinner').hide();
                    });
                }; 


                  $scope.selectpage = function(){
                    angular.forEach($scope.masterlistitem, function(value, key){
                            /*console.log(value);*/
                              if($scope.haschecked){
                                $('#'+value.ItemID).prop('checked',true);
                            }
                            else
                            {
                                 $('#'+value.ItemID).prop('checked',false);
                            }
                            
                           
                    });
                };

            //remove from masterlist function
                 $scope.removefrommasterlist =function(){

                     if($('input[type=checkbox]:checked').length == 0){
                          //  if($('.ckeckclass').prop("checked") == false){
                                 $('.addbtn').css('display','inline-block');
                                  $("#msg_id").css("display","none");
                                 return false;
                            }
                            else{
                                $('.addbtn').css('display','none');
                            }
                                 // $scope.message = false;
                                     $scope.isLoading = true; //Loader Image
                                       $http({                     
                                          method  : 'POST',
                                          url     : '<?php echo base_url();?>index.php/Masterlist/remove_from_masterlist',
                                          data    : {'itemid':angular.element('#removeform').serializeArray(),'competitor':$scope.competitorname},
                                          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                         })
                                          .success(function(data) {
                                             $scope.isLoading = false; //Loader Image
                                            if(data !== 'failure')
                                            {
                                                $scope.string = data;
                                                $(".button_search").trigger( "click" );
                                                //$scope.message="Items Successfully Removed.";
                                                $http({                     
                                                    method  : 'POST',
                                                    url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                    data    : 2
                                                  })
                                                    .success(function(response) {
                                                        $('#msg').html(response);
                                                        $('.err_modal').css('display','block');
                                                        $('#msg').css('display','block');                  
                                                        $scope.isLoading = false;
                                                    });
                                            }
                                          })
                                            .catch(function (err) {
                                             var msg =err.status;
                                              //Error Log To DB
                                             var screenname = '<?php echo $screen;?>';
                                             var StatusCode = err.status;
                                             var StatusText = err.statusText;
                                             var ErrorHtml  = err.data; 
                                           
                                             $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                                  })
                                             .success(function(response) {
                                                            return true;
                                                          });
                                               //failure msg
                                                  $http({                     
                                                   method  : 'POST',
                                                   url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                                   data    : msg
                                                  })
                                                   .success(function(response) {
                                                      $('#msg').html(response);
                                                      $('.err_modal').css('display','block');
                                                      $('#msg').css('display','block');                     
                                                      $scope.isLoading = false;
                                                  });
                                       
                                          })
                                          
                         };
            
                    $scope.exportDataExcelComp = function () {
                        var exportdataComp = [];
                        $scope.isLoading = true;
                        $http.get("exportexcelcomplete")
                        .then(function (response) 
                        {
                           $scope.masterlistitem  = response.data;
                           console.log($scope.masterlistitem);
                           $scope.masterlistitem.forEach(function (part) {
                                if (part.mypartno != '')
                                {
                                    exportdataComp.push({'Competitor' : part.CompetitorName,'Category':part.Category,'Item ID' : "'"+part.ItemID,'My Part#' : 'N/A','SKU' : part.SKU,'Title' : part.Title,'Price (in $)' : part.price,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.Qtysold,'Qty.Sold (Last Wk)':part.lastweekQtysold});
                                }
                                else
                                {
                                     exportdataComp.push({'Competitor' : part.CompetitorName,'Category':part.Category,'Item ID' : "'"+part.ItemID,'My Part#' : part.mypartno,'SKU' : part.SKU,'Title' : part.Title,'Price (in $)' : part.price,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.Qtysold,'Qty.Sold (Last Wk)':part.lastweekQtysold});
                                }
                            });
                       
                           $scope.ForExcelExportComp = exportdataComp;
                            var mystyle = {
                                sheetid: 'Complete Master List',
                                headers: true,
                                column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_Complete_MasterList.xls",?) FROM ?',[mystyle, $scope.ForExcelExportComp]);
                            $scope.isLoading = false;
                        });       
                    };
 
});
</script>
<script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>