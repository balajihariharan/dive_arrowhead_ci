<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title> <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
     
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>



</head>
<body class="main" ng-app="myApp" ng-controller="myCtrl" ng-cloak>
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="images/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="images/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="images/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="images/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->

    <div>
       <!--  <script>
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script> -->
        <style type="text/css">
             .pagi_master li{
                list-style-type: none;
                display: inline-block;
                margin: 0 3px;
                background: #5D85B2;
                color: #fff;
                
                border-radius: 3px;
                cursor: pointer;
                    }

                    .pagi_master li a{
                            color: #fff;
                text-decoration: none;
                padding: 4px;
                    }
                .pagination ul>.active>a, .pagination ul>.active>span {
                color: #999999;
                cursor: default;
                }
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            .curser_pt2 {
                cursor: pointer !important;
                background: url('<?php echo base_url();?>assets/img/sort_icon.png') no-repeat 5px center;
                padding: 0 20px;
            }


        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
             <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
                <div class="content_main_all">
                    <div class="form_head">
                        <form ng-submit="competitorsearch()">
                        <div class="field">
                            <div class="lable_normal">
                               Run Date From <span style="color:red; font-size:16px;">*</span> :

                            </div>
                            <p class="drop_down_med1">
                                <input type="text" class="date date-1"  id="runfrm" ng-model="fromdate" name="fromdate" placeholder="YYYY-MM-DD " />
                            </p>
                        </div>
                        <div class="field">
                            <div class="lable_normal">
                               Run Date To  <span style="color:red; font-size:16px;">*</span> :
                            </div>
                            <p class="drop_down_med1">
                                <input type="text" class="date date-1"  id="runto" ng-model="todate" name="todate" placeholder="YYYY-MM-DD"  />
                            </p>
                        </div>
                        <!-- id="btnsearch" -->
                        <div class="field1 margin_top_8"><input type="submit" class="button_search" id="btnsearch" name="btnsearch" value="Search"/></div>
                          
                    </form>
                    </div>
                    <center> <div style="color:red; display:none" id="msg_id">Please select the Date</div> </center> 
                    <div class="button_right" >
                                        <button value="Search" class="send_to_email" ng-click="email()">Send in Email</button>
                         </div>   
                        <div class="plan_list_view">
                            <table cellspacing="0" border="1" class="table price_smart_grid_new">
                                <col>
                                          <colgroup span="2"></colgroup>
                                          <colgroup span="2"></colgroup>
                                <thead>
                                    <tr>
                                        <th class="curser_pt" rowspan="2" scope="colgroup" ng-click="sortBy('CompetitorID')">Competitor</th>
                                        <th class="curser_pt" rowspan="2" scope="colgroup"  ng-click="sortBy('recordDate')">Run Date</th>
                                        <th style="text-align: center;" colspan="4" scope="colgroup">Last Run</th>
                                        <th class="curser_pt" rowspan="2" scope="colgroup" ng-click="sortBy('TotalCalls')">Total Calls</th>
                                        <th class="curser_pt" rowspan="2" scope="colgroup" ng-click="sortBy('RecordCount')">Record Count</th>
                                        <th class="curser_pt" rowspan="2" scope="colgroup" ng-click="sortBy('Error')">Error</th>
                                        <th class="curser_pt" rowspan="2" scope="colgroup" ng-click="sortBy('CronStatus')">Cron Status</th>
                                    </tr>
                                    <tr>
                                        <th style="background: #2D8BD5;text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('StartDate')">Start Date</th>
                                        <th style="background: #2D8BD5;text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('EndDate')">End Date</th>
                                        <th style="background: #2D8BD5;text-align: center;width: 7%;" class="curser_pt" scope="col" ng-click="sortBy('LastPageNo')">Last Page No</th>
                                        <th style="background: #2D8BD5;text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('TotalPage')">Total Page</th>
                                    </tr>
                                    <tr>
                                       <td><div class="search">
                                       <input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch" placeholder="Competitor Name" >
                                        </div></td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>

                                    </tr>
                                </thead>
                                    <tbody ng-repeat="comp in pagedItems[currentPage]">
                                    <tr>
                                        <td ng-click="competitorsids(comp.CompetitorID)" data-value="{{comp.CompetitorID}}" style="cursor:pointer">{{comp.CompetitorID}}</td>
                                        <td style="text-align: center">{{comp.recordDate}}</td>
                                        <td style="text-align: center">{{comp.StartDate}}</td>
                                        <td style="text-align: center">{{comp.EndDate}}</td>
                                        <td style="text-align: center;">{{comp.LastPageNo}}</td>
                                        <td style="text-align: center;">{{comp.TotalPage }}</td>
                                        <td style="text-align: center;">{{comp.TotalCalls}}</td>
                                        <td style="text-align: center;">{{comp.RecordCount}}</td>
                                        <td style="text-align: center;">{{comp.Error}}</td>
                                        <td style="text-align: center;">{{comp.CronStatus}}</td>
                                    </tr>
                                    </tbody>
                            </table>

                            <div class="pagination pull-right pagi_master">
                                <ul>
                                    <li ng-class="{disabled: currentPage == 0}">
                                        <a href ng-click="prevPage()">« Prev</a>
                                    </li>
                                    <li ng-repeat="n in pagenos | limitTo:5"
                                        ng-class="{active: n == currentPage}"
                                        ng-click="setPage()">
                                        <a href ng-bind="n + 1">1</a>
                                    </li>
                                    <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                        <a href ng-click="nextPage()">Next »</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="content_main">
                              <div class="button_right">
                                 <li class="submenu">
                                        <a class="last_menu_icon" href="#">
                                            <input type="button" class="export_button" value="Export As Excel" ng-click="exportDataExcel()"/>
                                        </a>
                                        <!-- <ul class="level2">
                                            <li ><a herf="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_API_RUN_TRACKER.csv">As CSV<a/></li> 
                                            <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel()">As Excel</a></li>
                                            <li><a href="<?php echo base_url();?>MyParts/export" class="last_menu_icon">As Pdf<a/></li>
                                        </ul> -->
                                    </li>
                            </div>
                            <div class="table_main" id="api">
                                <div class="left_heading">
                                    <h4>Details for <span>{{responsecompetitorname}}</span></h4>
                                </div>
                                <table cellspacing="0" cellpadding="0" border="1" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th class="curser_pt2" ng-click="sortBy2('RunID')">Run ID</th>
                                            <th class="curser_pt2" ng-click="sortBy2('ListingFrom')">Listing From</th>
                                            <th class="curser_pt2" ng-click="sortBy2('ListingTo')">Listing To</th>
                                            <th class="curser_pt2" ng-click="sortBy2('Listings')">Listings</th>
                                            <th class="curser_pt2" ng-click="sortBy2('PageNo')">Page No</th>
                                            <th class="curser_pt2" ng-click="sortBy2('TotalPage')">Total Pages</th>
                                            <th class="curser_pt2" ng-click="sortBy2('RecordDate')">Run Date</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody ng-repeat="value in pagedItemssub[currentPagesub]">
                                    <!-- <tbody ng-repeat="value in datas"> -->
                                    <tr>
                                        <td>{{value.RunID}}</td>
                                        <td>{{value.ListingFrom}}</td>
                                        <td>{{value.ListingTo}}</td>
                                        <td>{{value.Listings}}</td>
                                        <td>{{value.PageNo}}</td>
                                        <td>{{value.TotalPage}}</td>
                                        <td>{{value.RecordDate}}</td>
                                    </tr>
                                    </tbody>
                                     <tr ng-show="result"><td colspan="11"><h1><center>{{result}}</center></h1></td></tr>
                                </table>
                                </div>
                                  
                                  <div class="pagination pull-right pagi_master">
                                    <ul>
                                        <li ng-class="{disabled: currentPagesub == 0}">
                                            <a href ng-click="prevPagesub()">« Prev</a>
                                        </li>
                                        <li ng-repeat="n in pagenossub | limitTo:5"
                                            ng-class="{active: n == currentPagesub}"
                                        ng-click="setPagesub()">
                                            <a href ng-bind="n + 1">1</a>
                                        </li>
                                        <li ng-class="{disabled: currentPagesub == pagedItemssub.length - 1}">
                                            <a href ng-click="nextPagesub()">Next »</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        </div>
        
        
          <?php $this->load->view('footer.php'); ?>
    </div>
    
    <script type="text/javascript" src="<?php echo  base_url();?>assets/config_css/demo/events.js"></script>
    <!-- <script type="text/javascript" src="datePicker.min.js"></script> -->
    <script type="text/javascript" src="<?php echo  base_url();?>assets/config_css/js/calendar.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/config_css/js/datePicker.js"></script>

<script>
$('#runto').click(function(e){ 
    if ($('#runfrm').val() != "") {
                  document.getElementById('runfrm'). style.borderColor = "";
                    }  
             });         
                    

    $('#btnsearch').click(function(e){ 
      if ($('#runfrm').val() == "" || $('#runto').val() == "") {
                 var runfrm=document.getElementById('runfrm').value;
                   if(runfrm == ""){
                        document.getElementById('runfrm'). style.borderColor = "red";
                        $("#msg_id").css("display","block");
                        //return false;
                    }
                    else
                     {
                       document.getElementById('runfrm').style.borderColor = ""; 
                    }
                     var runto=document.getElementById('runto').value;

                   if(runto == ""){
                        document.getElementById('runto'). style.borderColor = "red";
                        $("#msg_id").css("display","block");
                        return false;
                    }
                    else
                     {
                       document.getElementById('runto').style.borderColor = ""; 
                    }
                    e.preventDefault();
            }
     else
                {   
                    $("#msg_id").css("display","none");
                    document.getElementById('runfrm').style.borderColor = "";
                    document.getElementById('runto').style.borderColor = ""; 
            }
        });
</script>
  <script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script>



<!-- more demos on this page only useing calendar.js
<script type="text/javascript" src="demo/year.js"></script>
<script type="text/javascript" src="demo/month.js"></script>
<script type="text/javascript" src="demo/week.js"></script>
<script type="text/javascript" src="demo/flight.js"></script> -->
    
    <!-- <script src="<?php echo  base_url();?>assets/js/jquery-1.11.3.min.js"></script> -->
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>

        <script>

        var exportdata=[];
        var exportdata_exc = [];
        exportdata.push ({'RunID' : 'Run ID','ListingFrom' : 'Listing From','ListingTo' : 'Listing To','Listings' : 'Listings','PageNo' : 'Page No','TotalPage' : 'Total Pages','RecordDate':'Run Date'});

            var sortingOrder = 'CompetitorID'; 
            var app = angular.module('myApp', []);
            app.controller('myCtrl', function($scope,$http,$filter) {
                 var screenname = '<?php echo $screen;?>';
                   
                $scope.sortingOrder = sortingOrder;
                //$scope.sortingOrdersub=;
                $scope.reverse          = false;
                $scope.reversesub       = false;
                $scope.filteredItems    = [];
                $scope.filteredItemssub = [];
                $scope.groupedItems     = [];
                $scope.itemsPerPage     = 5;
                $scope.itemsPerPagesub  = 10;
                $scope.pagedItems       = [];
                $scope.pagedItemssub    = [];
                $scope.pagenos          = [];
                $scope.pagenossub       = [];
                $scope.currentPages     = 0;
                $scope.currentPagesub   = 0;
                $scope.iddd             ="";
                $scope.from             ='';
                $scope.to               ='';

                

                 $scope.competitorsids = function(id) {
                    $scope.iddd=id;
                    $scope.responsecompetitorname = id;

                  
                    var fromdate = $scope.from;
                    var todate   = $scope.to;
                    console.log(fromdate);
                    console.log(todate);
                   //console.log(id);
                     var c_id = id;
                     $scope.result =''
                   $http({                     
                        method  : 'POST',
                        url     : '<?php echo  base_url();?>index.php/API_Run_Tracker/comp_run_status',
                        data    : {'c_id':c_id,'fromdate':fromdate,'todate':todate},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                    .success(function(data) {
                             $scope.datas = data;
                                     if($scope.datas.length > 0){
                                        $scope.datas  = data;
                                        }else{
                                            $scope.result    = 'No Records found';
                                        }
                            $scope.datas.forEach(function (part) {
                                exportdata.push ({'RunID' : part.RunID,'ListingFrom' : "'"+part.ListingFrom,'ListingTo' : "'"+part.ListingTo,'Listings' : part.Listings,'PageNo' : part.PageNo,'TotalPage' : part.TotalPage,'RecordDate': "'"+part.RecordDate});
                                exportdata_exc.push ({'Run ID' : part.RunID,'Listing From' : part.ListingFrom,'Listing To' : part.ListingTo,'Listings' : part.Listings,'Page No' : part.PageNo,'Total Page' : part.TotalPage,'Run Date': part.RecordDate});
                            });
                   
                           $scope.ForCSVExport = exportdata;
                           $scope.ForExcelExport = exportdata_exc;

                        $scope.exportDataExcel = function () {
                        var mystyle = {
                            sheetid:  $scope.iddd,
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_API_RUN_TRACKER.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                        //alasql('SELECT * INTO XLSX("DIVE_MyParts.xlsx",{headers:true}) FROM ?',[$scope.ForExcelExport]);
                        };


                             // $scope.orderByField = 'CompetitorName';
                             //$scope.reverseSort = false;
                                          $scope.sortBy2 = function(propertyName) {
                                            $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                            $scope.reversesub = ($scope.sortKey === propertyName) ? !$scope.reversesub : false;
                                            $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortKey, $scope.reversesub);
                                            $scope.pagedItemssub = ""; 
                                            $scope.groupToPagessub();
                                        };
                             
                             
                                       var searchMatchsub = function (haystack, needle) {
                                            if (!needle) {
                                                return true;
                                            }
                                            if(haystack !== null){
                                                return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                            }
                                            //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                        };

                                        // init the filtered items
                                        $scope.mysearchsub = function () {
                                            $scope.filteredItemssub = $filter('filter')($scope.datas, function (item) {
                                                for(var attr in item) {
                                                
                                                   if (searchMatchsub(item[attr], $scope.query1))
                                                        return true;
                                                }
                                                return false;
                                            });
                                            // take care of the sorting order
                                            if ($scope.sortingOrdersub !== '') {
                                                $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                            }
                                            $scope.currentPagesub = 0;
                                            // now group by pages
                                            $scope.groupToPagessub();
                                        };

                                        $scope.myitemsearchsub= function () {
                                            $scope.filteredItemssub = $filter('filter')($scope.datas, function (item) {
                                                for(var attr in item) {
                                                   if (searchMatchsub(item[attr], $scope.itemquery))
                                                        return true;
                                                }
                                                return false;
                                            });
                                            // take care of the sorting order
                                            if ($scope.sortingOrdersub !== '') {
                                                $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                            }
                                            $scope.currentPagesub = 0;
                                            // now group by pages
                                            $scope.groupToPagessub();
                                        };

                                        // calculate page in place
                                        $scope.groupToPagessub = function () {
                                            $scope.pagedItemssub = [];
                                            
                                            for (var i = 0; i < $scope.filteredItemssub.length; i++) {
                                                if (i % $scope.itemsPerPagesub === 0) {
                                                    $scope.pagedItemssub[Math.floor(i / $scope.itemsPerPagesub)] = [ $scope.filteredItemssub[i] ];
                                                } else {
                                                    $scope.pagedItemssub[Math.floor(i / $scope.itemsPerPagesub)].push($scope.filteredItemssub[i]);
                                                }
                                            }
                                        };
                                        
                                        $scope.rangesub = function (start, end) {
                                            var ret = [];
                                            if (!end) {
                                                end = start;
                                                start = 0;
                                            }
                                            for (var i = start; i < end; i++) {
                                                ret.push(i);
                                            }
                                            $scope.pagenossub = ret;
                                            return ret;
                                        };
                                        
                                        $scope.prevPagesub = function () {
                                            if ($scope.currentPagesub > 0) {
                                                $scope.currentPagesub--;
                                            }
                                            $('#ckbCheckAll').trigger('click');
                                            $('#ckbCheckAll').trigger('click');
                                        };
                                        
                                        $scope.nextPagesub = function () {
                                            if ($scope.currentPagesub < $scope.pagedItemssub.length - 1) {
                                                $scope.currentPagesub++;
                                            }
                                            $('#ckbCheckAll').trigger('click');
                                            $('#ckbCheckAll').trigger('click');
                                        };
                                        
                                        $scope.setPagesub = function () {
                                            $scope.currentPagesub = this.n;
                                            $('#ckbCheckAll').trigger('click');
                                            $('#ckbCheckAll').trigger('click');
                                        };

                                        // functions have been describe process the data for display
                                        $scope.mysearchsub();

                                        // change sorting order
                                        $scope.sort_bysub = function(newSortingOrder) {
                                            if ($scope.sortingOrdersub == newSortingOrder)
                                                $scope.reversesub = !$scope.reversesub;

                                            $scope.sortingOrdersub = newSortingOrder;

                                            // icon setup
                                            $('th i').each(function(){
                                                // icon reset
                                                $(this).removeClass().addClass('icon-sort');
                                            });
                                            if ($scope.reversesub)
                                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                            else
                                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                                        };
                                        
                                        $scope.$watch('currentPagesub', function(pno,oldno){
                                          if ((pno+1)%5==0 && $scope.pagedItemssub.length > 5){
                                            var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                            $scope.rangesub(start, $scope.pagedItemssub.length);
                                          }
                                        });
                                        $scope.rangesub($scope.pagedItemssub.length);
 
                             })
                            .catch(function (err) {

                                  var StatusCode = err.status;
                                  var StatusText = err.statusText;
                                  var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                  var msg =err.status;
                             //failure msg(db error)
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                   data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                });                                        
                 }; 
                 
                 //competitorsearch
                 $scope.competitorsearch = function() {
                    var fromdate  = $('#runfrm').val();
                    var todate    = $('#runto').val();
                    $scope.from   = $('#runfrm').val();
                    $scope.to     = $('#runto').val();
                      $http({                     
                        method  : 'POST',
                        url     : '<?php echo  base_url();?>index.php/API_Run_Tracker/cron_status',
                        data    : {'fromdate':fromdate,'todate':todate},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        }).success(function(data) {
                            $scope.resultcompetitor = data;


                                //pagination part start 
                                    // *********************
                                      $scope.sortBy = function(propertyName) {
                                            $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                            $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                            $scope.pagedItems = ""; 
                                            $scope.groupToPages();
                                        };
                                       var searchMatch = function (haystack, needle) {
                                            if (!needle) {
                                                return true;
                                            }
                                            if(haystack !== null){
                                                return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                            }
                                            //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                        };

                                        // init the filtered items
                                        $scope.mysearch = function () {
                                            $scope.filteredItems = $filter('filter')($scope.resultcompetitor, function (item) {
                                                for(var attr in item) {
                                                if(attr == "CompetitorID") {
                                                   if (searchMatch(item[attr], $scope.query))
                                                        return true;
                                                }
                                            }
                                                return false;
                                            });
                                            // take care of the sorting order
                                            if ($scope.sortingOrder !== '') {
                                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                            }
                                            $scope.currentPage = 0;
                                            // now group by pages
                                            $scope.groupToPages();
                                        };

                                        $scope.myitemsearch = function () {
                                            $scope.filteredItems = $filter('filter')($scope.resultcompetitor, function (item) {
                                                for(var attr in item) {
                                                   if (searchMatch(item[attr], $scope.itemquery))
                                                        return true;
                                                }
                                                return false;
                                            });
                                            // take care of the sorting order
                                            if ($scope.sortingOrder !== '') {
                                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                            }
                                            $scope.currentPage = 0;
                                            // now group by pages
                                            $scope.groupToPages();
                                        };

                                        // calculate page in place
                                        $scope.groupToPages = function () {
                                            $scope.pagedItems = [];
                                            
                                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                                if (i % $scope.itemsPerPage === 0) {
                                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                                } else {
                                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                                }
                                            }
                                        };
                                        
                                        $scope.range = function (start, end) {
                                            var ret = [];
                                            if (!end) {
                                                end = start;
                                                start = 0;
                                            }
                                            for (var i = start; i < end; i++) {
                                                ret.push(i);
                                            }
                                            $scope.pagenos = ret;
                                            return ret;
                                        };
                                        
                                        $scope.prevPage = function () {
                                            if ($scope.currentPage > 0) {
                                                $scope.currentPage--;
                                            }
                                            $('#ckbCheckAll').trigger('click');
                                            $('#ckbCheckAll').trigger('click');
                                        };
                                        
                                        $scope.nextPage = function () {
                                            if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                                $scope.currentPage++;
                                            }
                                            $('#ckbCheckAll').trigger('click');
                                            $('#ckbCheckAll').trigger('click');
                                        };
                                        
                                        $scope.setPage = function () {
                                            $scope.currentPage = this.n;
                                            $('#ckbCheckAll').trigger('click');
                                            $('#ckbCheckAll').trigger('click');
                                        };

                                        // functions have been describe process the data for display
                                        $scope.mysearch();

                                        // change sorting order
                                        $scope.sort_by = function(newSortingOrder) {
                                            if ($scope.sortingOrder == newSortingOrder)
                                                $scope.reverse = !$scope.reverse;

                                            $scope.sortingOrder = newSortingOrder;

                                            // icon setup
                                            $('th i').each(function(){
                                                // icon reset
                                                $(this).removeClass().addClass('icon-sort');
                                            });
                                            if ($scope.reverse)
                                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                            else
                                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                                        };
                                        
                                        $scope.$watch('currentPage', function(pno,oldno){
                                          if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                            var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                            $scope.range(start, $scope.pagedItems.length);
                                          }
                                        });
                                        $scope.range($scope.pagedItems.length);

                                        if(data=='')
                                          {
                                            //alert('NO DATA TO DISPLAY');
                                             $http({                     
                                                 method  : 'POST',
                                                 url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                data    : 7
                                                })
                                                     .success(function(response) {
                                                        $('#msg').html(response);
                                                        $('.err_modal').css('display','block');
                                                        $('#msg').css('display','block');                  
                                                        $scope.isLoading = false;
                                                    });
                                          }
                        })
                        .catch(function (err) {
                                  var StatusCode = err.status;
                                  var StatusText = err.statusText;
                                  var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                              var msg =err.status;
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                         
                        })


                      
                 };
                   $scope.email = function() {
                    $scope.isLoading = true;
                        $http({                     
                        method  : 'POST',
                        url     : '<?php echo  base_url();?>index.php/API_Run_Tracker/email',
                        data    : {'emaildata':$scope.resultcompetitor},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        }).success(function(data) {
                            //$scope.isLoading = false;
                            //alert('email sent successfully');
                               $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 5
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                        })
                         .catch(function(err) {
                             //alert('email sent Problem please try again..');
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                })
                   };
                 
                 });
        </script>
       <script>
            //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
       </script>
<!-- <script  type="text/javascript">
        $(document).ready(function() {
            $("#button_upload").click(function(e) {
             var currentdate = new Date(); 
             var datetime = currentdate.getDate() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getFullYear() + ","  
                + currentdate.getHours() + "H-"  
                + currentdate.getMinutes() + "M-" 
                + currentdate.getSeconds() + "S";
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('api');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            a.href = data_type + ', ' + table_html;
            a.download = 'DIVE_API_RUN_TRACKER'+datetime+'.xls';
            a.click();
            e.preventDefault();
            });
        });
    </script>-->
</body>
</html>
                        
                        


           
           
        
 
            
