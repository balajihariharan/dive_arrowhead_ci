<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>     <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
        </title>
           <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/jquery.datepick.css"/>
</head>
<body class="main">
    <!-- <div class="container head_bg"> -->
   <?php $this->load->view('header.php'); ?>
    <div>
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                 <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="wrapper">
            
            <div class="content_main_all">
           <!--  <h1>
            <?php
           //if($this->session->userdata('validationmsg') && $this->session->userdata('validationmsg') != ''){
               //echo '<center><span style="color:red">'.$this->session->userdata('validationmsg').'</span></center>';
                //$this->session->unset_userdata('validationmsg');
           // }
            ?></h1>  -->
                <!--<div class="field_radio">
                    <input type="radio" class="field_chk"> Show that dont sell
                    <input type="radio" class="field_chk"> Show  my item
                </div>-->
                <div class="content_main">
                <!-- ng-submit="report_submit()" -->
                <form id="reportformform" action="<?php echo base_url();?>index.php/Reports/generate_report" method="post">
                    <div class="channel_main">
                        <label class="label_inner" id ="Competitors">Competitor <span style="color:red; font-size:16px;">*</span>    :</label>
                        <select id="services" name="competitorname" ng-model="competitorname" class="drop_down_med_cbr margin_top_10">
                            <option value="">Select</option>
                            <?php if(count($compidandname) >= 1){
                                foreach($compidandname as $value){ ?>
                                    <option value="<?php echo $value->CompID.','.$value->CompetitorName;?>"><?php echo $value->CompetitorName;?></option>
                                <?php } 
                                }
                            ?>
                        </select>
                    </div>
                    
                    <div class="channel_main">
                        <label class="label_inner" >Select Week <span style="color:red; font-size:16px;">*</span> :</label>
                        <!-- <select id="services" name="services" class="drop_down_med margin_top_10">
                            <option value="Break Parts">Date Picker (Weekno)</option>
                        </select> -->
                        <input placeholder="Select starting week" type="text"  class="input_picker" name="weekno" id="customWeekPicker" value="">
                        <!-- <select id="services" name="weekno" ng-model="weekno" class="drop_down_med margin_top_10">
                        <option value="">Select Week</option>
                           <?php for($i=1;$i<=52;$i++){ ?>
                            <option value="<?php echo $i;?>"><?php echo 'Weekno : '.$i;?></option>
                           <?php } ?>
                        </select> -->
                    </div>
                    <!-- <div class="channel_main">
                        <label class="label_inner_general_big">Select Report Generation Path</label>
                        <p class="drop_down_med1">
                            <input placeholder="635" type="file" >
                        </p>
                    </div> -->
                    <center> <div style=" display:none" class="error_msg_all" id="msg_id">Please select Competitor and week</div> </center>
                    <div class="channel_main">
                        <input type="submit" class="calculate_bill" id ="report" value="Generate Report" />
                    </div>
                  </form>
                </div>
                </div>
        </div>
        </div>
           <?php $this->load->view('footer.php'); ?>
    </div>
    <script src="<?php echo base_url().'assets/';?>js/jquery.min.1.11.0.js"></script>
    <script src="<?php echo  base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery.plugin.min.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery.datepick.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery.datepick.ext.js"></script>
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular-sanitize.js"></script>    
    <script>
      $('#customWeekPicker').click(function(e){ 
      if ($('#services').val() != ""){

          document.getElementById('services').style.borderColor = ""; 

      }
      });

      $('#report').click(function(e){ 

        if ($('#services').val() == "" || $('#customWeekPicker').val() == "") {

                 var Competitors=document.getElementById('services').value;
                   if(Competitors == ""){
                        document.getElementById('services').style.borderColor = "red";
                         $("#msg_id").css("display","block");
                        //return false;
                    }

                    else
                    {
                     document.getElementById('services').style.borderColor = "";   
                    }


                   var selectweek=document.getElementById ('customWeekPicker').value;
                   if(selectweek == ""){
                        document.getElementById('customWeekPicker').style.borderColor = "red";
                         $("#msg_id").css("display","block");
                        return false;
                    }  
                  
                    e.preventDefault();
      
            }
            else
            {

                document.getElementById('services').style.borderColor = "";
                document.getElementById('customWeekPicker').style.borderColor = "";   

                 
            }
        });
        </script>
        <script type="text/javascript">
            $(function() {
            		var today = new Date();
                    $('#customWeekPicker').datepick({ 
                        renderer: $.datepick.weekOfYearRenderer, 
                        calculateWeek: customWeek, 
                        firstDay: 1, 
                        showOtherMonths: true, 
                        showTrigger: '#calImg',
                        endDate: "today",
            			maxDate: today
                      }); 
                     
                    function customWeek(date) { 
                        return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1; 
                    }
            });
            $(window).bind('scroll', function () {
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
              $('#txtDate').datepicker({
                showWeek: true,
                firstDay: 1,
               onSelect: function(dateText) {        
                   $("#txtWeek").val($.datepicker.iso8601Week(new Date(dateText)));
                   $("#selectedDate").val(dateText);
               }
            });
           </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
</body>
</html>