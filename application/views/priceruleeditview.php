<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
          <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
   
    <style>
        .iframe_div{
            float:left;
            width:800px;
            margin:10px 5px;
            height:270px;
            overflow:auto;
        }
    </style>
</head>
<body class="main">
    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    
        <script>
            $(window).bind('scroll', function () {
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

                .title
                {
                    text-align: left;
                }
                .item
                {
                    text-align: left;
                }

            .itemIDCell {
                margin: 0;
            }

            .pricing_cal_new {
                display: inline-block;
                width: 116%;
                text-align: left;
                margin: 0 0 10px 0;
            }
        </style>
        
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                   <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
            <?php 
                if ($this->session->flashdata('errormsg') != '')
                {
                    echo '<span style="padding-left:40%; color:red; font-size:20px">'.$this->session->flashdata('errormsg').'</span>';
                }
            ?>
        </div>
        <div class="container">
            <div class="wrapper">
                    <div class="content_main_all">
                    <form id="new_rule_form" action="<?php echo base_url();?>Pricesmart/editsave" method="POST">
                        <div class="table_main">
                            <div class="table_footer">
                                <div class="rules_main">
                                    <div class="rules">
                                        <label>Rule ID</label>
                                        <div class="input_label_disable" name="ruleid" value="">
                                            <?php if(count($this->uri->segment(3)) <= 0){ ?>
                                            <input type="hidden" name="smart_ruleid" value="<?php echo strtotime(date("Y-m-d H:i:s"));?>">
                                            	<span><?php echo strtotime(date("Y-m-d H:i:s"));?></span>
                                            <?php }else{ ?>
                                            <input type="hidden" name="smart_ruleid" value="<?php echo $this->uri->segment(3);?>">
                                            	<span><?php echo $this->uri->segment(3);?></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="rules">
                                        <label>Rule Name</label>
                                        <?php if(count($this->uri->segment(3)) <= 0){ ?>
                                            <input type="text" name="smart_rulename" value="" style="min-width: 200px;padding: 5px 10px;color: #333;display: inline-block;border: 1px solid #ccc;font-size: 14px;">
                                         <?php }else { ?>
                                         		<input type="text" name="smart_rulename" value="<?php echo $selectedcatdata[0]->rule_name;?>" style="min-width: 200px;padding: 5px 10px;color: #333;display: inline-block;border: 1px solid #ccc;font-size: 14px;">
                                         <?php } ?>
                                    </div>
                                </div>


                                <div class="checking_cost">
                                    <p class="radio_normal">
                                        <input type="radio" class="lowest" name="smart_lowest" value="0" <?php if($selectedcatdata[0]->low_flag == 0){echo 'checked';}?>/>
                                        <label class="clr_light_blue">Wherever I'm Not the Lowest</label>
                                    </p>
                                    <p class="radio_normal">
                                        	<input type="radio" class="lowest" name="smart_lowest" value="1" <?php if($selectedcatdata[0]->low_flag == 1){echo 'checked';}?>/>   
                                            <label class="clr_light_blue">Wherever I'm the Lowest</label>
                                    </p>
                              </div>
                            </div>
                            <?php
                                if(count($this->uri->segment(3)) >=1){
                                    $selectid = array();
                                    $selectcompid = array();
                                    foreach($selectedcatid as $value){
                                        $selectid[] = $value->categoryid;
                                        $selectcompid[] = $value->compid;
                                    }
                                }
                            ?>

                            <div class="inclusion_main">
                                <h2>Inclusions (this will override Global)</h2>
                                    <div class="iframe_div">
                                    <h3 style="float:left;">Category / Competitor</h3>
                                    <h3 style="float:right;">Selected Category / Competitor</h3> 

                                    <select id="test-select" multiple="multiple" name="competitorcategory[]">
                                    <?php if(count($competitorcombo) >= 1){foreach($competitorcombo as $value){ ?>
                                        <option value="<?php echo $value->categoryid.','.$value->compid;?>" data-section="<?php echo $value->competitorname;?>" <?php if(count($this->uri->segment(3)) >=1){if(in_array($value->categoryid,$selectid) && in_array($value->compid,$selectcompid)){echo 'selected';}}?>><?php echo $value->category;?></option>
                                      <?php } } ?>
                                    </select>
                            </div>
                            <div class="pricing_config">
                                    <br><br>
                                    <div class="pricing_cal_new">
                                        <label>Set Threshold for Sale Price to be not less than </label>
                                            <input class="text_normal_small" type="text" name="smart_threshold" value="<?php echo $selectedcatdata[0]->cost_price_threshold;?>"/>
                                        <label class="clr_red"> %   of (Cost Price + Freight + Fees)</label>
                                    </div>
                                    <h3>Pricing Configuration</h3>
                                    <div class="pricing_cal">
                                    		<input type="radio" name="smart_inclusionpricing" value="e" <?php if($selectedcatdata[0]->our_price_operator == 'e'){echo 'checked';}?>>
                                        <label class="clr_light_blue font_size_14">Set our price = Competitor's Lowest Price</label>
                                    </div>
                                    <div class="pricing_cal">
	                                        <input type="radio" name="smart_inclusionpricing" value="0" <?php if($selectedcatdata[0]->our_price_operator != 'e'){echo 'checked';}?>>
                                        <label class="clr_light_blue font_size_14">Set our price = </label>
                                        
                                        	<input type="text" class="text_normal_small" name="smart_inclusionpricing_percentage" value="<?php echo $selectedcatdata[0]->our_price_value;?>" />
                                        <span>%</span>
                                        <select name="smart_inclusion_ltgt">
                                            <option value="g" <?php if($selectedcatdata[0]->our_price_operator == 'g'){echo 'selected';}?>>&gt;</option>
                                            <option value="l" <?php if($selectedcatdata[0]->our_price_operator == 'l'){echo 'selected';}?>>&lt;</option>
                                        </select>
                                        <span class="clr_light_blue font_size_14">Competitor's Lowest Price</span>
                                    </div>
                                </div>

                            <div class="inclusion_main">
                                <h2>Global [Global Rules are configured irresepective of the Rule ID above]</h2>

                                <div class="category">
                                    <h3>All Categories</h3>
                                </div>
                                <div class="competitor">
                                    <h3>All Competitors</h3>
                                </div>
                                <div class="pricing_config">
                                    <br>
                                    <div class="pricing_cal_new">
                                    <!-- marg_top -->
                                        <label>Set Threshold for Sale Price to be not less than </label>
                                            <input class="text_normal_small" id="threshold" type="text" name="global_threshold" value="<?php
                                                    if($selectedcatdata[0]->low_flag == 0){
                                                        echo $globaldata[0]->LowCPT;
                                                    }
                                                    else 
                                                    {
                                                        echo $globaldata[0]->NotLowCPT;
                                                    }
                                                    ?>"/>
                                        <label class="clr_red" align="center"> %   of (Cost Price + Freight + Fees)</label>
                                    </div>
                                    <div class="pricing_cal">
                                        	<input id="global_opp_e" type="radio" name="global_globalpricing" value="e" <?php 
                                                if(($selectedcatdata[0]->low_flag == 1 && $globaldata[0]->NotLow_Price_Operator == 'e') ||
                                                    ($selectedcatdata[0]->low_flag == 0 && $globaldata[0]->Low_Price_Operator == 'e'))
                                                    {
                                                        echo 'checked';
                                                    }
                                                ?>>
                                        <label class="clr_light_blue font_size_14">Set our price = Competitor's Lowest Price</label>
                                    </div>
                                    <div class="pricing_cal">
                                        	<input id="global_opp_ne" type="radio" name="global_globalpricing" value="0" <?php 
                                                if(($selectedcatdata[0]->low_flag == 1 && $globaldata[0]->NotLow_Price_Operator !== 'e') ||
                                                    ($selectedcatdata[0]->low_flag == 0 && $globaldata[0]->Low_Price_Operator !== 'e'))
                                                    {
                                                        echo 'checked';
                                                    }
                                                ?>>
                                        <label class="clr_light_blue font_size_14">Set our price = </label>
                                        	<input type="text" id="global_price" class="font_size_15 text_normal_small" name="global_globalpricing_percentage" value="<?php 
                                                    if($globaldata[0]->low_flag == 1){
                                                        echo $globaldata[0]->NotLow_Price_Value;
                                                    }
                                                    else 
                                                    {
                                                        echo $globaldata[0]->Low_Price_Value;
                                                    }
                                                ?>"/>                 
                                        <span>%</span>
                                        <select name="global_global_ltgt">
                                            <option id="global_opp_g" value="g" <?php 
                                                    if(($selectedcatdata[0]->low_flag == 1 && $globaldata[0]->NotLow_Price_Operator == 'g') ||
                                                        ($selectedcatdata[0]->low_flag == 0 && $globaldata[0]->Low_Price_Operator == 'g'))
                                                    {
                                                        echo 'selected';
                                                    }

                                                ?>>&gt;</option>
                                            <option id="global_opp_l" value="l" <?php 
                                                    if(($selectedcatdata[0]->low_flag == 1 && $globaldata[0]->NotLow_Price_Operator == 'l') ||
                                                        ($selectedcatdata[0]->low_flag == 0 && $globaldata[0]->Low_Price_Operator == 'l'))
                                                    {
                                                        echo 'selected';
                                                    }

                                                ?>>&lt;</option>
                                        </select>
                                        <span class="clr_light_blue font_size_14">Competitor's Lowest Price</span>
                                    </div>
                                </div>
                            </div>
                            <div class="save_icon">
                                <input type="submit" class="button_add rawlistdatasubmit" value="Save">
                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        
        
       <?php $this->load->view('footer.php'); ?>
    </div>
    
    <script src="<?php echo base_url();?>assets/js/jquery.tree-multiselect.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
    <script>
        var $select = $('#test-select');
        $select.treeMultiselect({ enableSelectAll: true, sortable: true, searchable: true });
</script>
<script>
    $(document).ready(function(){
        $(document).on('click','.lowest',function(){
            var value = $(this).val();
            var globalopp = '';
            if(value == 1){
                var glob_price = '<?php echo $globaldata[0]->NotLow_Price_Value;?>';
                $('#global_price').val(glob_price); 
                globalopp = "<?php echo $globaldata[0]->NotLow_Price_Operator;?>";
                var threshold = '<?php echo $globaldata[0]->NotLowCPT;?>';
                $('#threshold').val(threshold);
            }

            if(value == 0){
                $('#global_price').val('<?php echo $globaldata[0]->Low_Price_Value;?>');
                globalopp = "<?php echo $globaldata[0]->Low_Price_Operator;?>";
                var threshold = '<?php echo $globaldata[0]->LowCPT;?>';
                $('#threshold').val(threshold);

            }
            if (globalopp == 'g') { 
                $('#global_opp_g').attr("selected", "selected");
                $('#global_opp_l').removeAttr("selected");
                $('#global_opp_ne').prop('checked',true);
            }
            if(globalopp == 'l') {
                $('#global_opp_l').attr("selected", "selected");
                $('#global_opp_g').removeAttr("selected");
                $('#global_opp_ne').prop('checked',true);
            }
           if(globalopp !== 'g' && globalopp !== 'l'){
                $('#global_opp_e').prop('checked',true);
            }
        });
    });
</script>
</body>
</html>
