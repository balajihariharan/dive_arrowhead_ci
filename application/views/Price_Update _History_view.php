<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
     <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>



</head>
<body class="main" ng-controller="myController" ng-cloak>
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="images/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="images/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="images/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="images/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->

    <div>
       <!--  <script>
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script> -->
        <style type="text/css">
             .pagi_master li{
                list-style-type: none;
                display: inline-block;
                margin: 0 3px;
                background: #5D85B2;
                color: #fff;
                
                border-radius: 3px;
                cursor: pointer;
                    }

                    .pagi_master li a{
                            color: #fff;
                text-decoration: none;
                padding: 4px;
                    }
                .pagination ul>.active>a, .pagination ul>.active>span {
                color: #999999;
                cursor: default;
                }
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                   <?php echo "<h2>".$screen."</h2>" ;?>
                </div>
            </div>
        </div>
             <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
                <div class="content_main_all">
                    <div class="form_head">
                        <form ng-submit="price_history()">

                        <div class="field">
                            <div class="lable_normal">
                                From Date <span style="color:red; font-size:16px;">*</span> :

                            </div>
                            <p class="drop_down_med1">
                                <input type="text" class="date date-1"  id="runfrm" ng-model="fromdate" name="fromdate" placeholder="YYYY-MM-DD " />
                            </p>
                        </div>

                        <div class="field">
                            <div class="lable_normal">
                                 To Date <span style="color:red; font-size:16px;">*</span> :
                            </div>
                            <p class="drop_down_med1">
                                <input type="text" class="date date-1"  id="runto" ng-model="todate" name="todate" placeholder="YYYY-MM-DD"  />
                            </p>
                        </div>

                        <!-- id="btnsearch" -->
                        <div class="field1 margin_top_8"><input type="submit" class="button_search" id="btnsearch" name="btnsearch" value="Search"/></div>
                          
                    </form>
                    </div>
                    <center> <div style="color:red; display:none" id="msg_id">Please Select the Date</div> </center> 
                      
                        <div class="plan_list_view">
                            <table cellspacing="0" border="1" class="table price_smart_grid_new">
                                <thead>
                                    <tr>
                                        <th class="curser_pt" ng-click="sortBy('StoreName')">StoreName</th>
                                        <th class="curser_pt" ng-click="sortBy('ItemID')">My ItemIDs</th>
                                        <th class="curser_pt" ng-click="sortBy('SKU')">My SKU</th>
                                        <th class="curser_pt" ng-click="sortBy('MyPartNo')">MyPart No</th>
                                        <th class="curser_pt" ng-click="sortBy('OldPrice')">Old Price</th>
                                        <th class="curser_pt" ng-click="sortBy('NewPrice')">New Price</th>
                                        <th class="curser_pt" ng-click="sortBy('Status')">Status</th>
                                    </tr>
                                   <tr class="tab1table">
                                     <td>
                                            <div class="search">
                                              <input type="text" ng-model="StoreName" ng-change="mysearch()" class="inputsearch" placeholder="StoreName">
                                            </div> 
                                       </td>
                                       <td>
                                            <div class="search">
                                              <input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch" placeholder="My ItemIDs">
                                            </div> 
                                       </td>
                                       <td>
                                           <div class="search">
                                             <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch" placeholder="SKU">
                                           </div>
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="partquery" ng-change="mypartsearch()" class="inputsearch" placeholder="MyPart No">
                                           </div>
                                       </td>
                                       <td>
                                            <div class="search">
                                               <input type="text" ng-model="oldpricequery" ng-change="oldpricesearch()" class="inputsearch" placeholder="Old Price">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="search">
                                               <input type="text" ng-model="newpricequery" ng-change="newpricesearch()" class="inputsearch" placeholder="New Price">
                                            </div>
                                        </td>
                                        <td>
                                            <!-- <div class="search">
                                               <input type="text" ng-model="statusquery" ng-change="statussearch()" class="inputsearch" placeholder="Status">
                                            </div> -->
                                        </td>
                                      </tr>
                                </thead>
                                <tbody>
                                   <!--  <tr ng-repeat="history in pricehistory"> -->
                                     <tr ng-repeat="history in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                        <td>{{history.StoreName}}</td>
                                        <td>{{history.ItemID}}</td>
                                        <td>{{history.SKU}}</td>
                                        <td>{{history.MyPartNo}}</td>
                                        <td>{{history.OldPrice}}</td>
                                        <td>{{history.NewPrice}}</td>
                                        <td>{{history.Status}}</td>
                                    </tr>
                                    </tbody>
                            </table>

                            <!-- pagination ui part -->
                                <div class="pagination pull-right pagi_master">
                                    <ul>
                                        <li ng-class="{disabled: currentPage == 0}">
                                            <a href ng-click="prevPage()">« Prev</a>
                                        </li>
                                        <li ng-repeat="n in pagenos | limitTo:5"
                                            ng-class="{active: n == currentPage}"
                                        ng-click="setPage()">
                                            <a href ng-bind="n + 1">1</a>
                                        </li>
                                        <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                            <a href ng-click="nextPage()">Next »</a>
                                        </li>
                                    </ul>
                                </div>
                         <!-- pagination ui part end -->

                        </div>

                        </div>
                    </div>
                </div>
                
        </div>
        
        
          <?php $this->load->view('footer.php'); ?>
    </div>
    
    <script type="text/javascript" src="<?php echo  base_url();?>assets/config_css/demo/events.js"></script>
    <!-- <script type="text/javascript" src="datePicker.min.js"></script> -->
    <script type="text/javascript" src="<?php echo  base_url();?>assets/config_css/js/calendar.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/config_css/js/datePicker.js"></script>

<script>
$('#runto').click(function(e){ 
    if ($('#runfrm').val() != "") {
                  document.getElementById('runfrm'). style.borderColor = "";
                    }  
             });         
                    

    $('#btnsearch').click(function(e){ 
      if ($('#runfrm').val() == "" || $('#runto').val() == "") {
                 var runfrm=document.getElementById('runfrm').value;
                   if(runfrm == ""){
                        document.getElementById('runfrm'). style.borderColor = "red";
                        $("#msg_id").css("display","block");
                        //return false;
                    }
                    else
                     {
                       document.getElementById('runfrm').style.borderColor = ""; 
                    }
                     var runto=document.getElementById('runto').value;

                   if(runto == ""){
                        document.getElementById('runto'). style.borderColor = "red";
                        $("#msg_id").css("display","block");
                        return false;
                    }
                    else
                     {
                       document.getElementById('runto').style.borderColor = ""; 
                    }
                    e.preventDefault();
            }
     else
                {   
                    $("#msg_id").css("display","none");
                    document.getElementById('runfrm').style.borderColor = "";
                    document.getElementById('runto').style.borderColor = ""; 
            }
        });
</script>
  <script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
    <script>
         var sortingOrder = 'ItemID';
        var app = angular.module('myApp',[]);
        app.controller('myController',function($scope,$http,$filter){

                    $scope.sortingOrder = sortingOrder;
                    $scope.reverse = false;
                    $scope.filteredItems = [];
                    $scope.groupedItems = [];
                    $scope.itemsPerPage = 25;
                    $scope.pagedItems = [];
                    $scope.pagenos = [];
                    $scope.pricehistory = '';
                    $scope.currentPage = 0;
           

             //price_history
                 $scope.price_history = function() {
                    var fromdate = $('#runfrm').val();
                    var todate = $('#runto').val();
                      $http({                     
                        method  : 'POST',
                        url     : '<?php echo  base_url();?>index.php/Price_Update_History/get_price_history',
                        data    : {'fromdate':fromdate,'todate':todate},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        }).success(function(data) {
                            $scope.pricehistory = data;

                                      //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.pricehistory, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "ItemID") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.myskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.pricehistory, function (item) {
                                    for(var attr in item) {
                                    if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.mypartsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.pricehistory, function (item) {
                                    for(var attr in item) {
                                     if(attr == "MyPartNo") {
                                            if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.oldpricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.pricehistory, function (item) {
                                    for(var attr in item) {
                                     if(attr == "OldPrice") {
                                            if (searchMatch(item[attr], $scope.oldpricequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.newpricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.pricehistory, function (item) {
                                          for(var attr in item) {
                                            if(attr == "NewPrice") {
                                            if (searchMatch(item[attr], $scope.newpricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                           /* $scope.statussearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.pricehistory, function (item) {
                                          for(var attr in item) {
                                            if(attr == "Status") {
                                            if (searchMatch(item[attr], $scope.statusquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };*/

                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};
                            $scope.showLoader = false; //Loader Image 
                    
                                        if(data=='')
                                          {
                                            //alert('NO DATA TO DISPLAY');
                                             $http({                     
                                                 method  : 'POST',
                                                 url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                data    : 7
                                                })
                                                     .success(function(response) {
                                                        $('#msg').html(response);
                                                        $('.err_modal').css('display','block');
                                                        $('#msg').css('display','block');                  
                                                        $scope.isLoading = false;
                                                    });
                                          }
                        })
                        .catch(function (err) {
                              var msg =err.status;
                              //Error Log To DB
                             var screenname = '<?php echo $screen;?>';
                             var StatusCode = err.status;
                             var StatusText = err.statusText;
                             var ErrorHtml  = err.data; 
                           
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })  
                 };
        })
    </script>
        <script>
            //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
       </script> 
     
</body>
</html>
                        
                        


           
           
        
 
            
