<!DOCTYPE html>
<html ng-app="partsApp">
<html>
<head>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
           <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    
    <script src="<?php echo base_url().'assets/';?>Romaine/js/xlsx.full.min.js"></script>
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>
</head>
<style>
 .pagi_master li{
    list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
</style> 
<body class="main" ng-controller="partsCtrl" ng-cloak>
<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

   <!-- <div class="container head_bg"> -->


<?php $this->load->view('header.php'); ?>


    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="<?php echo base_url();?>assets/img/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="<?php echo base_url();?>assets/img/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="<?php echo base_url();?>assets/img/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="<?php echo base_url();?>assets/img/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->
    <div>
        <script>
            $(function () {
                /*$('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }*/
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
        

            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }
            .status_bar thead th:nth-child(3) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }
            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }


            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

                .close1 {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close1:hover,
                .close1:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                    display: none !important;
            }

            #my_file {
                display: none;
            }

             .temp_upload{
            padding: 6px 30px 6px 10px;
            margin: 0% 11% 0% -26%;
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            font-family: sans-serif, Verdana, Geneva;
            text-align: right;
            display: inline-block;
            color: #fff;
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
            font-size: 13px;
        } 


         .temp_upload_new{
            padding: 6px 30px 6px 10px;
           margin: 5% 1% 0% 0%;
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            font-family: sans-serif, Verdana, Geneva;
            text-align: right;
            display: inline-block;
            color: #fff;
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
            font-size: 13px;
        } 
            
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                      <?php echo "<h2>".$screen."</h2>" ;?>
           <span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
               <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">

            <div class="content_main_all">
             <div class="" style="width:90%; color: #2D8BD5;cursor:pointer;font-weight: bold;">
                          Default Margin (In %) : <input type="text" style="width:4%;" ng-model="margincount" class="ng-pristine ng-untouched ng-valid" value="">
                          <button class="button_add" ng-click="savedefaultmargin()">Save</button>
                         </div>
                <div class="button_right">
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="button_download" value="Export"/>
                            </a>
                            <ul class="level2">
                                <li>
                                    <a href="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_MyParts.csv">As CSV</a>
                                </li>
                                <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel()">As Excel</a></li>
                            </ul>
                        </li>
                </div>

                <div class="button_right">
                    <a href="#" class="last_menu_icon">
                                <input type="button" class="temp_upload" ng-click="import()" value="Import"/>
                    </a>
                </div>
                    <input type="file" id="my_file" file-reader="parts"/>
                     <div class="button_right">
                    <a href="#" class="last_menu_icon">
                                <input type="button" class="button_add_part" ng-click="addnewpart()" value="Add New Partno"/>
                    </a>
                </div>

                 <form id="mypartsform" ng-submit="onSubmit()">
                <div class="plan_list_view">
                    <table id="example" cellspacing="0" border="1" class="table price_smart_grid_new">
                        <thead>
                            <tr>
                                <th class="curser_pt" ng-click="sortBy('partno')">My Part #</th>
                                <th class="curser_pt" ng-click="sortBy('sku')">SKU</th>
                                <!--<th>Brand Name</th>-->
                                
                                <!--<th>Competitor Price</th>-->
                                <th class="curser_pt" ng-click="sortBy('cost_price')">Cost Price (in $)</th>
                                <th class="curser_pt" ng-click="sortBy('freight')">Freight (in $)</th>
                                <th class="curser_pt" ng-click="sortBy('margin')">Margin (in %)</th>
                                 <th class="curser_pt" ng-click="sortBy('MSPrice')">Minimum Selling Price(in $)</th>
                                <!-- <th class="curser_pt" ng-click="sortBy('my_price_ebay')"> My Price (eBay) (in $)</th> -->
                            </tr>
                            <tr>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text" ng-model="partquery" ng-change="partsearch()" class="inputsearch" placeholder="My Part"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="skuquery" ng-change="skusearch()" class="inputsearch" placeholder="SKU"/>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <!-- <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="mypricequery" ng-change="mypricesearch()" class="inputsearch" placeholder="My Price"/>
                                    </div>
                                </td> -->
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-click="change($index)" class="edit_type" ng-repeat="part in pagedItems[currentPage] | orderBy:propertyName:reverse">
                                <input type="hidden" class="input_table editable_input_blck" ng-model="part.flag"/>
                                <td>{{part.partno}}
                                    <input type="hidden" class="input_table editable_input_blck" ng-model="part.partno"/>
                                    <input type="hidden" class="input_table editable_input_blck" ng-model="part.flag"/>
                                </td>
                                <td>{{part.sku}}</td>
                                <td><input type="text" class="input_table editable_input_blck" ng-model="part.cost_price"/></td>
                                <td><input type="text" class="input_table editable_input_blck" ng-model="part.freight"/></td>
                                <td><input type="text" class="input_table editable_input_blck" ng-model="part.margin"/></td>
                                <td><input type="text" class="input_table editable_input_blck" ng-model="part.MSprice"/></td> 
                                <!-- <td>{{part.my_price_ebay}}</td> -->
                            </tr>

                        </tbody>
                    </table>
                    </div>

                    <?php
                         echo '<div id="popupform">
                                    <div id="Pnopopup" class="modal">

                                            <div class="pop_map_my_part">
                                               
                                                 <div class="pop_container" style="height: 191px;">
                                                        <div class="heading_pop_main">
                                                            <h4>

                                                                Import My Parts #
                                                            </h4>
                                                             <span class="close1">&times;</span>
                                                        </div>
                                                       <div class="pop_pad_all">
                                                           <div style="color:#009e47; text-align:center; font-size:17px">{{message}}</div>
                                                           <div style="color:#ff3333; text-align:center; font-size:17px">{{errormessage}}</div>
                                                        </div>

                                                        <div class="button_center">
                                                            <input type="hidden" id="ftype"/>
                                                        
                                                        <input type="text" id="fname" class="input_med" readonly/>
                                                        <button type="submit" id="popsavemain" class="temp_upload_new" ng-click="upload()">Upload</button><span style="font-size:16px; font-weight:bold;">(.csv)
                                                        </div>
                                                        <div class="channel_main">
                                                           
                                                        </div>

                                                        <div class="button_center padding_bottom_15">
                                                        <button type="submit" id="save_imp" class="button_download" ng-click="save_imp()">Save</button>
                                                        </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>';
                        ?>

    
                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>
                   
                        <div class="button_left">
                            <input class="button_add rawlistdatasubmit" type="submit" value="Save" ng-click="save()"/>
                        </div>
            </form>
        </div>
        
        </div>
    </div>
      <?php $this->load->view('footer.php'); ?>
      </div>
                        <div id="Pnopopup2" class="modal">
                            <div class="pop_map_my_part">
                               
                                 <div class="pop_container">
                                            <div class="heading_pop_main">
                                                <h4>
                                                    Add New Part Number#
                                                </h4>
                                                 <span class="close">&times;</span>
                                            </div>
                                    <div class ="pop_pad_all_add_new">
                                    <form id="saveme"  ng-submit= "savepartno()">
                                    <div class="channel_main">
                                        <label class="label_inner_general"  style="color:green; ">My Part <span style="color:red; font-size:16px;">*</span> </label>
                                            <p class="drop_down_med1">
                                                :  <input type="text" id="MyPart2" ng-model="newpartno2" class="input_med" name=" Part" value=""  />
                                            </p>
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general"  style="color:green; ">SKU <span style="color:red; font-size:16px;">*</span> </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id="SKU2" ng-change=block() ng-model="SKU2" name="SKU2" value=""  />
                                        </p>

                                      <div  class="error_msg_all"  style=" display:none;bottom: -7px;" id="msg_id">SKU Already Exist!</div>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general" style="color:green; ">Cost Price (in $)  </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id="CostPrice2" ng-model="CostPrice2" name="CostPrice2" value=""  />
                                        </p>
                                        <p class="resutnresponse"></p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general"  style="color:green; ">Freight (in $) </label>
                                        <p class="drop_down_med1">
                                           :  <input class="input_med" id="Freight2" ng-model="Freight2" name="Freight2" value="" />
                                        </p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general"  style="color:green; ">Fees (in $)</span> </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id= "Fees2" name= "Fees2" ng-model="Fees2"  value="" />
                                        </p>

                                    </div>
                                    
                                    <div class="save_icon_manage">
                                        <input type="submit"  value="Save" id="save2" class="button_add_part_save ">
                                    </div>
                                    </form>

                                </div>
                            </div>

                        </div>

        
  
    
    
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>
    <script type="text/javascript">
     $('#SKU2').click(function(e){
        var partno=document.getElementById('MyPart2').value;
                   if(partno != ""){
                        document.getElementById('MyPart2'). style.borderColor = "";
                       
                    }
         }); 

     $('#CostPrice2').click(function(e){
        var SKU2=document.getElementById('SKU2').value;
                   if(SKU2 != ""){
                        document.getElementById('SKU2'). style.borderColor = "";
                       
                    }
         });


    $('#save2').click(function(e){
          if ($('#MyPart2').val() == "" || $('#SKU2').val() == "") {
                 var partno=document.getElementById('MyPart2').value;
                   if(partno == ""){
                        document.getElementById('MyPart2'). style.borderColor = "red";
                       
                    }
                    else
                     {
                       document.getElementById('MyPart2').style.borderColor = ""; 
                    }

                  var sku=document.getElementById('SKU2').value;
                   if(sku == ""){
                        document.getElementById('SKU2').style.borderColor = "red";
                        $("#msg_id").css("display","none");
                        return false;
                    } 
                    e.preventDefault();           
                     }
            else
            {   $("#msg_id").css("display","none");
                document.getElementById('MyPart2').style.borderColor = "";
                document.getElementById('SKU2').style.borderColor = "";   
            
            }
        });
    </script>
    <script>
        //export part
        var exportdata=[];
        var exportdata_exc = [];
        exportdata.push ({'partno' : 'MyPart#','sku' : 'SKU','cost_price' : 'Cost Price (in $)','freight' : 'Freight (in $)','ebay_fees' : 'Fees (in $)','Margin (in %)' : 'margin','Minimum Selling Price':'Minimum Selling Price(in $)'});        
          
        //load data in grid            
        var sortingOrder = 'MyPartNo';
        var app = angular.module('partsApp', ['ngSanitize', 'ngCsv']);
            app.controller('partsCtrl', function($scope, $http, $timeout,$window,$filter) {
                $scope.isLoading=true;
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 10;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.parts = '';
                $scope.currentPage = 0;
                $scope.temp = '';
                $http.get("disp")
                .then(function (response) 
                {
                    $scope.isLoading=false;
                    $scope.parts = response.data;
                    //console.log($scope.parts);
                    $scope.margincount = response.data.margin;

                    $scope.parts.forEach(function (part) {
                        part.flag='N';
                    });

                    var exportdata=[];
                    var exportdata_exc = [];
                    exportdata.push ({'partno' : 'MyPart#','sku' : 'SKU','cost_price' : 'Cost Price (in $)','freight' : 'Freight (in $)','ebay_fees' : 'Fees (in $)','Margin (in %)' : 'Margin (in %)','Minimum Selling Price':'Minimum Selling Price(in $)'});       

                    $scope.parts.forEach(function (part) {
                        $scope.margincount = part.def_margin;
                        exportdata.push({'partno' : part.partno,'sku' : part.sku,'cost_price' : part.cost_price,'freight' : part.freight,'ebay_fees' : part.ebay_fees,'Margin (in %)' : part.margin,'Minimum Selling Price':part.MSprice});
                        exportdata_exc.push({'MyPart#' : part.partno,'SKU' : part.sku,'Cost Price (in $)' : part.cost_price,'Freight (in $)' : part.freight,'Fees (in $)' : part.ebay_fees,'Margin (in %)' : part.margin,'Minimum Selling Price':part.MSprice});
                    });
                   
                   $scope.ForCSVExport = exportdata;
                   $scope.ForExcelExport = exportdata_exc;


                    //pagination part start 
                    /*$scope.sortBy = function(propertyName) {
                        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                        $scope.propertyName = propertyName;
                    };*/

                    $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                            

                    var searchMatch = function (haystack, needle) {
                        if (!needle) {
                            return true;
                        }
                        if(haystack != ''){
                            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                        }
                    };

                    // init the filtered items
                    $scope.partsearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                for(var attr in item) {
                                    if(attr == 'partno'){
                                        if (searchMatch(item[attr], $scope.partquery))
                                            return true;
                                        }
                                }
                                return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };
                            
                    // calculate page in place
                    $scope.groupToPages = function () {
                        $scope.pagedItems = [];
                        for (var i = 0; i < $scope.filteredItems.length; i++) {
                            if (i % $scope.itemsPerPage === 0) {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                            } 
                            else {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);   
                            }
                        }
                    };

                    $scope.range = function (start, end) {
                        var ret = [];
                        if (!end) {
                            end = start;
                            start = 0;
                        }
                        for (var i = start; i < end; i++) {
                            ret.push(i);
                        }
                        $scope.pagenos = ret;
                        return ret;
                    };
                            
                    $scope.prevPage = function () {
                        if ($scope.currentPage > 0) {
                            $scope.currentPage--;
                        }
                    };
                            
                    $scope.nextPage = function () {
                        if ($scope.currentPage < $scope.pagedItems.length - 1) {
                            $scope.currentPage++;
                        }
                    };
                            
                    $scope.setPage = function () {
                        $scope.currentPage = this.n;
                    };

                    // functions have been describe process the data for display
                    $scope.partsearch();

                    // change sorting order
                    $scope.sort_by = function(newSortingOrder) {
                        if ($scope.sortingOrder == newSortingOrder)
                            $scope.reverse = !$scope.reverse;
                            $scope.sortingOrder = newSortingOrder;
                            // icon setup
                            $('th i').each(function(){
                                // icon reset
                                $(this).removeClass().addClass('icon-sort');
                            });
                            if ($scope.reverse)
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                            else
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                    };
                            
                    $scope.$watch('currentPage', function(pno,oldno){
                        if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                            var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                            $scope.range(start, $scope.pagedItems.length);
                        }
                    });

                    $scope.range($scope.pagedItems.length);
                 
                    $scope.skusearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            
                            for(var attr in item) {
                                 if(attr == 'sku'){
                                    if (searchMatch(item[attr], $scope.skuquery))
                                        return true;
                                    }
                                }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.mypricesearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            for(var attr in item) {
                                if(attr == 'my_price_ebay'){
                                    if (searchMatch(item[attr], $scope.mypricequery))
                                        return true;
                                    }
                            }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                           $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.change = function(index){
                        $scope.pagedItems[$scope.currentPage][index].flag = 'U';
                    };
                    $scope.block=function()
                    {
                          $("#msg_id").css("display","none");
                    }

                    $scope.savedefaultmargin = function(){
                         $scope.isLoading = true;
                         var margincount= $scope.margincount;
                        // console.log($scope.margincount);

                     
                      $http({                     
                            method  : 'POST',
                            url     : '<?php echo  base_url();?>index.php/MyParts/margincountsave',
                            data    : {'margincount':margincount},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                                .success(function(data) {
                                    $scope.isLoading = false;
                                    //alert("Successfully Updated");
                                     $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');  
                                                     $("#Pnopopup2").css('display','none');                
                                                    
                                                })
                                    //location.reload(true);
                                })

                             .catch(function(err) {
                                    $scope.isLoading = false;
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                         $("#Pnopopup2").css('display','none');                 
                                        
                                    });
                                })
                         }
                   
                    $scope.savepartno = function() {
                               var t;
                               var mypart     =$scope.newpartno2;
                               var SKU2       = $scope.SKU2;
                               var CostPrice2 = $scope.CostPrice2;
                               var Freight2   = $scope.Freight2;
                               var Fees2      = $scope.Fees2;
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                      for(var attr in item) {
                                        if(attr == 'sku'){
                                            if (searchMatch(item[attr], SKU2)){
                                                t=1;
                                            }
                                            }
                                        }
                                
                                });
                                if ((t==1)&&(SKU2!='')){
                                              $("#msg_id").css("display","block");
                                             return false;
                                }
                                                               
                               $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo  base_url();?>index.php/MyParts/savepartno',
                                    data    : {'mypart':mypart,'SKU2':SKU2,'CostPrice2':CostPrice2,'Freight2':Freight2,'Fees2':Fees2},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                    })
                                .success(function(data) {
                                
                                    //alert("Successfully Updated");
                                     $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');  
                                                     $("#Pnopopup2").css('display','none');                
                                                    
                                                })
                                    //location.reload(true);
                                })

                             .catch(function(err) {
                                 var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                         $("#Pnopopup2").css('display','none');                 
                                        
                                    });
                                })

                    }

                    $scope.save = function(){
                        $scope.isLoading = true; //Loader Image
                        //console.log($scope.finaldata);
                        
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/MyParts/save',
                                data    : { 'New_Content' : $scope.finaldata},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(data) {
                                console.log(data);
                                $scope.isLoading = false;
                                    //alert("Successfully Updated");
                                     $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                                    //location.reload(true);
                                })
                            .catch(function (err) {
                                 var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                        var msg = err.status;
                        console.log(err);
                         //failure msg
                            $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data    : msg
                            })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    //$("#Pnopopup").css("display", "none");                 
                                    $scope.isLoading = false;
                                });
                    }) 
                    };
                        
                    var finaldata = '';
                    $scope.$watch('pagedItems', function(newvalue,oldvalue) {
                        $scope.finaldata = newvalue;
                        //console.log($scope.finaldata);
                    }, true);

                    $scope.import = function(){
                        $scope.message = '';
                        $scope.errormessage = '';
                        $("#popupform").css("display", "block");
                        $("#Pnopopup").css("display", "block");
                    };

                    $scope.addnewpart = function(){
                        //$("#popupform2").css("display", "block");
                        $("#Pnopopup2").css("display", "block");
                    };

                    $scope.upload = function(){
                        document.getElementById('my_file').click();
                    };
                    
                    
                    $scope.save_imp = function(){
                    var type = document.getElementById('ftype').value;
                    console.log(type);
                    //|| type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    if (type == 'application/vnd.ms-excel')
                    {
                        console.log($scope.parts);
                        $scope.isLoading = true; //Loader Image
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/MyParts/imp_save',
                                    data    : { 'New_Content' : $scope.parts},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                })
                                .success(function(data) {
                                    $scope.isLoading = false;

                                    //alert("Successfully Updated");
                                                 $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg_id').html(response);
                                                    $('.err_modal1').css('display','block');
                                                    $('#msg1').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                                    $("#popupform").css("display", "none");
                                    $("#Pnopopup").css("display", "none");
                                    //location.reload(true);
                                        //$scope.message = "Successfully Updated";
                                })
                                .catch(function(err) {
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                var msg = err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                })

                    }
                    else
                    {
                       $scope.message = '';
                       $scope.errormessage = 'You are trying to Upload a Non-CSV File. Try again!!';
                    }
                    };
            
                    $scope.exportDataExcel = function () {
                        var mystyle = {
                            sheetid: 'My Parts',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_MyParts.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    };

            });   
        });

        app.directive('fileReader', function($timeout) {
            return {
                scope: {
                fileReader:"="
            },
            link: function(scope, element) {               
              $(element).on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                var name = files[0].name;
                var type = files[0].type;
                console.log(type);
                if (files.length) {
                  var r = new FileReader();
                  r.onload = function(e) {
                    var contents = e.target.result;
                    var resultvalue = [];
                    angular.forEach(contents.split('\n'), function(value, key) {
                        var result = value.split(",");
                            resultvalue.push({'partno' : result[0],'sku' : result[1],'cost_price' : result[2],'freight' : result[3],'ebay_fees' : result[4],'margin' : result[5],'MSprice' : result[6], 'flag' : 'I'});
                    });
                    scope.$apply(function () {
                        document.getElementById('fname').value = name;
                        document.getElementById('ftype').value = type;
                        scope.fileReader = resultvalue;
                        console.log(scope.fileReader);
                    });
                    };
                r.readAsText(files[0]);
                };
              });
            }
          };
      });
</script>
<script>
          $(document).on('click','.close1',function(){
          var name = document.getElementById('fname').value;
           if (name == '')
          {
            $("#Pnopopup").css("display", "none"); 
          }
          else
          {
            location.reload(true);
            $("#Pnopopup").css("display", "none");
          } 
        });


          //new popup msg close button
           $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
          location.reload(true);
      });


    $(document).on('click','.destroy2',function(){
    $(".err_modal").css("display", "none");     
    });

</script>
<script>
    $(document).on('click','.close',function(){
          $("#Pnopopup2").hide(); 
        });    
 


</script>
</body>
</html>
