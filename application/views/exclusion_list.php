<!DOCTYPE html>
<html ng-app="filterApp">
<html>
<head>
<title>
    <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
</title>
    <link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
   
    <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>   
    <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script>
    <script src="<?php echo base_url().'assets/';?>Romaine/js/xlsx.full.min.js"></script>
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>

    <style>
  ul,li { margin:0; padding:0; list-style:none;}
        .label { color:#000; font-size:16px;}
        .ckeckclass
        {
            margin:2px;
        }
        .divTable{
            float: left;
            width: 70px;
            height: 40px;
            border-right: 1px solid #e1e1e1;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .lastdivTable{
            float: left;
            width: 70px;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .paging-nav {
          text-align: right;
          padding-top: 2px;
        }

        .paging-nav a {
          margin: auto 1px;
          text-decoration: none;
          display: inline-block;
          padding: 1px 7px;
          background: #91b9e6;
          color: white;
          border-radius: 3px;
        }

        .paging-nav .selected-page {
          background: #187ed5;
          font-weight: bold;
        }
        .curser_pt {
            cursor: pointer;
            background: url("img/sort_icon.png") no-repeat 5px center;
            padding: 0 20px;
        }
        tfoot
        {
        text-align: center !important;
        display: table-row-group !important;
        }

        .paging-nav,
        #tblmasterresult {
         
          margin: 0 auto;
          font-family: Arial, sans-serif;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 20%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .myPnopopupsubclose{
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;   
        }
        .myPnopopupsubclose:hover,
        .myPnopopupsubclose:focus{
            color: black;
            text-decoration: none;
            cursor: pointer;   
        }

        div.absolute {
            position: absolute;
            top: 326px;
            left: 758px;
            width: 46px;
            height: 407px;
            cursor: pointer;
           
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        .modal-content {
            background-color: #fefefe;
            margin: 10% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 32%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #2d8bd5;
            text-decoration: none;
            cursor: pointer;
        }
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }
	</style>
    <style>
 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
</style> 
	</head>
<body class="main" ng-controller="filterCtrl">
<!-- loading gif-->
<!-- <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div> -->

            
               <?php $this->load->view('header.php'); ?>

                <div class="container header_bg_clr">
                    <div class="wrapper">
                        <div class="header_main">
                            <?php echo "<h2>".$screen."</h2>" ;?>

           <span style="padding-left:40%; color:red; font-size:14px"></span>
                        </div>
                    </div>
                </div>
                             <div id="msg" style="display: none;"></div>
        <div class="container">
        <div class="wrapper">
			
            <div class="content_main_all content_main_filter">
            <div class="buttons">
                <a href="<?php echo base_url();?>Filters/exclusion/"><button class="button_add" value="">ADD</button></a>
            </div>
               <div class="plan_list_view">
                <table cellspacing="0" border="1" class="table">
                    <thead>
                        <tr>
                            <th class="curser_pt" ng-click="sortBy('exclusion_id')">Filter Id</th>
                            <th class="curser_pt" ng-click="sortBy('exclusion_name')">Filter Description</th>
                            <th class="curser_pt">Active</th>
                            <th class="curser_pt" ng-click="sortBy('created_by')">Created By</th>
                            <th class="curser_pt" ng-click="sortBy('created_on')">Created On</th>
                            <th class="curser_pt">Action</th>
                         </tr>
                         <tr>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text" ng-model="excidquery" ng-change="exclusion_id_search()" class="
                                    " placeholder="Filter Id"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="excnamequery" ng-change="exclusion_name_search()" class="inputsearch" placeholder="Filter Description"/>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="crbyquery" ng-change="created_by_search()" class="inputsearch" placeholder="Created By"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="cronquery" ng-change="created_on_search()" class="inputsearch" placeholder="Created On"/>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                    </thead>
                    <tbody>
                         <tr ng-repeat="filterdata in pagedItems[currentPage] | orderBy:propertyName:reverse">
                                <td>{{filterdata.exclusion_id}}</td>
                                <td>{{filterdata.exclusion_name}}</td>
                                <td ng-if="filterdata.active_flag == '0'">
                                    <input type="checkbox" class="field_chk ex_list_checkbox" data-value="{{filterdata.exclusion_id}}" /></td>
                                <td ng-if="filterdata.active_flag == '1'">
                                    <input type="checkbox" class="field_chk ex_list_checkbox" data-value="{{filterdata.exclusion_id}}" checked /></td>
                                <td>{{filterdata.created_by}}</td>
                                <td>{{filterdata.created_on}}</td>
                                <td class="tbl_img">
                                <table class="table_inner">
                                    <tbody>
                                        <tr>
                                            <td><a href="<?php echo base_url();?>Filters/exclusion/{{filterdata.exclusion_id}}"><img src="<?php echo base_url().'assets/';?>img/edit.png"></a></td>
                                            <td><a href="#"><img src="<?php echo base_url().'assets/';?>img/delete_tbl.png" ng-click="delete_filter($event)" id= "{{filterdata.exclusion_id}}"></a></td>
                                        </tr>
                                </table>
                                </td>
                            </tr>
                    </tbody>
            
                </table>
                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>

                </div>

                </div>
            </div>
            </div>
    </div>
   <?php $this->load->view('footer.php'); ?>

    <script>
    $(document).on('click','.ex_list_checkbox',function(){
        var dataid = $(this).attr('data-value');
        var checkid = '';
            if($(this).prop("checked") == true){
              checkid = 1;  
            }
            else{
              checkid = 0;  
            }
            $.ajax({
                   url: 'exclusionlist_update',
                   type:'POST',
                   data: {'dataid':dataid,
                        'checkid':checkid},
                   success: function(data) {
                        //alert('Active Flag Update Successfully');
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                            data    : {'msgstatus':6},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                   },
                    error : function(xhr){
                          var msg =xhr.status;
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                    }
            });
    });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
    <script>
        var sortingOrder = 'ExclusionID';
        var app = angular.module('filterApp', []);
            app.controller('filterCtrl', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 10;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.fdata = '';
                $scope.currentPage = 0;
                $http.get("disp")
                .then(function (response) 
                {
                    $scope.fdata = response.data;
                    console.log(response.data);

                    $scope.sortBy = function(propertyName) {
                        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                        $scope.propertyName = propertyName;
                    };

                    var searchMatch = function (haystack, needle) {
                        if (!needle) {
                            return true;
                        }
                        if(haystack != ''){
                            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                        }
                    };

                    // init the filtered items
                    $scope.exclusion_id_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                                for(var attr in item) {
                                    if(attr == 'exclusion_id'){
                                        if (searchMatch(item[attr], $scope.excidquery))
                                        return true;
                                    }
                                }
                                return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };
                            
                    // calculate page in place
                    $scope.groupToPages = function () {
                        $scope.pagedItems = [];
                        for (var i = 0; i < $scope.filteredItems.length; i++) {
                            if (i % $scope.itemsPerPage === 0) {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                            } 
                            else {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);   
                            }
                        }
                    };

                    $scope.range = function (start, end) {
                        var ret = [];
                        if (!end) {
                            end = start;
                            start = 0;
                        }
                        for (var i = start; i < end; i++) {
                            ret.push(i);
                        }
                        $scope.pagenos = ret;
                        return ret;
                    };
                            
                    $scope.prevPage = function () {
                        if ($scope.currentPage > 0) {
                            $scope.currentPage--;
                        }
                    };
                            
                    $scope.nextPage = function () {
                        if ($scope.currentPage < $scope.pagedItems.length - 1) {
                            $scope.currentPage++;
                        }
                    };
                            
                    $scope.setPage = function () {
                        $scope.currentPage = this.n;
                    };

                    // functions have been describe process the data for display
                    $scope.exclusion_id_search();

                    // change sorting order
                    $scope.sort_by = function(newSortingOrder) {
                        if ($scope.sortingOrder == newSortingOrder)
                            $scope.reverse = !$scope.reverse;
                            $scope.sortingOrder = newSortingOrder;
                            // icon setup
                            $('th i').each(function(){
                                // icon reset
                                $(this).removeClass().addClass('icon-sort');
                            });
                            if ($scope.reverse)
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                            else
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                    };
                            
                    $scope.$watch('currentPage', function(pno,oldno){
                        if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                            var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                            $scope.range(start, $scope.pagedItems.length);
                        }
                    });

                    $scope.range($scope.pagedItems.length);
                 
                    $scope.exclusion_name_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                            for(var attr in item) {
                                if(attr == 'exclusion_name'){
                                    if (searchMatch(item[attr], $scope.excnamequery))
                                        return true;
                                    }
                                }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.created_by_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                            for(var attr in item) {
                                if(attr == 'created_by'){
                                    if (searchMatch(item[attr], $scope.crbyquery))
                                        return true;
                                    }
                                }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                           $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.created_on_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                            for(var attr in item) {
                             if(attr == 'created_on'){
                                if (searchMatch(item[attr], $scope.cronquery))
                                    return true;
                                }
                            }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                           $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                               $scope.delete_filter =function(e){
                            var del = e.target.getAttribute('id');

                             $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Filters/delete_filter',
                                    data    : { 'id' : del},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                })
                                .success(function(data) {
                                    //alert('Deleted Successfully');
                                             $http({                     
                                                         method  : 'POST',
                                                         url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                         data    : 4
                                                        })
                                                             .success(function(response) {
                                                                $('#msg').html(response);
                                                                $('.err_modal').css('display','block');
                                                                $('#msg').css('display','block');                  
                                                                $scope.showLoader = false;
                                                            });
                                    //location.reload();
                                })
                                 .catch(function (err) {
                                         var msg =err.status;
                                        //failure msg
                                                $http({                     
                                                 method  : 'POST',
                                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                                 data    : msg
                                                })
                                                     .success(function(response) {
                                                        $('#msg').html(response);
                                                        $('.err_modal').css('display','block');
                                                        $('#msg').css('display','block');                  
                                                        $scope.showLoader = false;
                                                    });
                                  })
                            }
                });
            });
    </script>

<script>
        //new popup msg close button
     $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            location.reload(true);
        });
</script>
</body>
</html>