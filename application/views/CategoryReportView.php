
<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    <link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_romaine.css" />
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script> 
    <script type="text/javascript" src="<?php echo  base_url();?>assets/demo/events.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/calendar.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/datePicker.js"></script>
    <script src="https://rawgithub.com/eligrey/FileSaver.js/master/FileSaver.js"></script>
   </head>


<body class="main" ng-controller="postController" ng-cloak>
    <!-- <div class="container head_bg"> -->
  <?php $this->load->view('header.php'); ?>
  <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
    <div>
        <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
    /*        .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }


            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }*/
.vertical_cen{
    vertical-align: middle;
}


            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            .edit_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 85px;
                font-size: 14px;
            }

             .delete_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 90px;
                font-size: 14px;
            }
            .table_main_new {
                width:100%;
                /*margin:50px 200px;*/
            }
            .table_main_new thead{
                background:#EEEEEE;
                
            }
           

            .table_main_new tbody tr td:last-child{
                 text-align: right;   
            }
            .table_main_new tbody tr td{
                color:#3e3e3e;
                font-size:14px;
                text-align:left ;
                text-transform:capitalize;
                line-height:20px;
            }

            .table_main_new_access th:nth-child(1) {
                width:450px;
            }
            .table_main_new_access th:nth-child(2) {
                border-right:none;
            }


               .table_main_new thead tr th {
                    color: #fff;
                    font-size: 15px;
                    background: #2D8BD5;
                    text-align: left;
                    padding: 10px;
                    line-height: 20px;
                }



                .nodata{
                    margin: 0 660px;
                    font-weight: bold;
                    color: black;
                    font-size: 25px;
                    display: inline-block;         
                }

                .back_button {
                    padding: 5px 31px 6px 24px;
                    float: right;
                    margin: -8px -2px -9px 8px;
                    font-size: 15px !important;
                    border: none;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    cursor: pointer;
                    font-family: sans-serif, Verdana, Geneva;
                    text-align: right;
                    display: inline-block;
                    color: #fff;
                    vertical-align: top;
                    background-color: #00886D;
                }

                .export_buttonnew {
                    padding: 6px 30px 7px 10px;
                    float: right;
                    margin: 9px 17px 1px 0px;
                    font-size: 15px !important;
                    border: none;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    cursor: pointer;
                    font-family: sans-serif, Verdana, Geneva;
                    text-align: right;
                    display: inline-block;
                    color: #fff;
                    vertical-align: top;
                    background: url('<?php echo base_url();?>assets/img/export_data1.png') no-repeat right 1px #00886D;
                }

                span.weeknew {
                    margin: 0 450px; 
                    font-weight: bold;
                    background: #5D85B2;
                    color: white;
                    padding: 5px 10px;
                    border-radius: 7px; 
                    text-decoration: none; 
                    display: inline-block; 
                    font-size: 22px;
                }

                .curser_pt_new {
                    cursor: pointer;
                    padding : 0 20px;
                    cursor: pointer !important;
                    }

    </style>
     <div class="container header_bg_clr">
        <div class="wrapper">
            <div class="header_main">
               <?php echo "<h2>".$screen."</h2>" ;?><span class="error_msg_head"></span>
            </div>
            </div>
    </div>     

       <!--  <div class="container"> -->
       <!--  <div class="wrapper"> -->
        <div class="content_main_all">
            <div class="display_inline_blk">
                <span class="weeknew ">Week : {{week}}</span>
                <span ng-if="catdisplay" class="weeknew ">Category : {{category}}</span>
            </div>
            <div class="form_head">
                <div class="field">
                    <div class="lable_normal">
                        Category Details &nbsp;&nbsp;
                    </div>
                        <select name="categoryname[]" class="select" ng-model="categoryname" id="categoryname">
                            <option value="">-Select-</option>
                            <?php if(count($compload) >= 1){
                              foreach($compload as $value){ ?>
                                <option style="color:black; " value="<?php echo $value['category'];?>"><?php echo $value['category'];?></option>  
                            <?php }
                            } ?>
                        </select>
                </div>
                <div class="field">
                    <input class="date date-1"  value="" id="myWeek" placeholder="YYYY-MM-DD" style="border-radius: 3px; padding: 4px 0px 4px 3px; width: 100px;color:black; font-weight: bold;"/>&nbsp;&nbsp;
                    <button value="Search" class="button_search_tab1" id="btnsearch" ng-click="GetData()">Display</button>
                </div>
                 <center> <div style=" display:none" class="error_msg_all" id="msg_id">Please select Category Details  and week</div> </center>
               <div class="button_right">
                    <input type="button" class="export_buttonnew" ng-click="exportData()" value="Export As Excel"/>
               </div>
    </div> 
                        <br><br>
                        <div class="table_main_new" id="exportable1">
                                <table style="width:100%;" border="2" cellpadding="0" cellspacing="0" align="center">
                                    <col>
                                          <colgroup span="2"></colgroup>
                                          <colgroup span="2"></colgroup>
                                              <thead>
                                          <tr>
                                            <th class="curser_pt" style="text-align: center; vertical-align: middle !important;" rowspan="2" ng-click="sortBy('Company')">Company</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">7 Days Sales (In $)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">7 Days % Of Total (In %) </th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Listing</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Sold [7 days]</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">% of Listing Sold (In %)</th>
                                            <th style="text-align: center;" colspan="2" scope="colgroup">Avg. Sale Price (In $)</th>
                                          </tr>
                                          <tr>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDaysSales')">This Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDaysSales_lw')">Last Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDAYPercentOFTOTAL')">This Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SevenDAYPercentOFTOTAL_lw')">Last Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('Listing')">This Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('Listing_lw')">Last Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SoldSevendays')">This Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('SoldSevendays_lw')">Last Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('PercentofListingSold')">This Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('PercentofListingSold_lw')">Last Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('AvgSalePrice')">This Wk</th>
                                            <th style="text-align: center;" class="curser_pt" scope="col" ng-click="sortBy('AvgSalePrice_lw')">Last Wk</th>
                                      </tr>
                                      </thead>
                                <tbody>

                                <tr ng-repeat="item in dashboarddata | orderBy:sortKey:reverse">

                                    <td class="curser_pt_new" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}" scope="row" ng-click="ReportTwo($event)" data-id="{{item.CompId}}" data-value="{{item.Company}}">{{item.Company}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDaysSales}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDaysSales_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDAYPercentOFTOTAL}}</td>
                                     <td style="text-align: right;"ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SevenDAYPercentOFTOTAL_lw}}</td>                                   
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.Listing}}</td>
                                    <td style="text-align: right;"ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.Listing_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SoldSevendays}}</td>
                                    <td style="text-align: right;"ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.SoldSevendays_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.PercentofListingSold}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.PercentofListingSold_lw}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.AvgSalePrice}}</td>
                                    <td style="text-align: right;" ng-style="item.CompId == sesdata && {'background-color':'#D24D57','color':'white'}">{{item.AvgSalePrice_lw}}</td>

                                </tr>
                                </tbody>
                            </table><br><br>
                            <span ng-if="dashboarddata.length == 0" class="weeknew">No Data</p>
                        </div>
                    </div>
                    </div></div>
                
            </div>
        </div>

         <?php $this->load->view('footer.php'); ?>
          <script>

          $('#myWeek').click(function(e){ 
           if ($('#categoryname').val() != ""){

          document.getElementById('categoryname').style.borderColor = ""; 

      }
      });
         $('#btnsearch').click(function(e){ 

        if ($('#categoryname').val() == "" || $('#myWeek').val() == "") {

                 var Competitors=document.getElementById('categoryname').value;
                   if(Competitors == ""){
                        document.getElementById('categoryname').style.borderColor = "red";
                         $("#msg_id").css("display","block");
                        //return false;
                    }

                    else
                    {
                     document.getElementById('categoryname').style.borderColor = "";   
                    }


                   var selectweek=document.getElementById ('myWeek').value;
                   if(selectweek == ""){
                        document.getElementById('myWeek').style.borderColor = "red";
                         $("#msg_id").css("display","block");
                        return false;
                    }  
                  
                    e.preventDefault();
      
            }
            else
            {

                document.getElementById('categoryname').style.borderColor = "";
                document.getElementById('myWeek').style.borderColor = "";   

                 
            }
        });
        </script>

<script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
</script>

<script type="text/javascript" src="<?php echo  base_url();?>assets/js/date.js"></script>
<script src="https://rawgithub.com/eligrey/FileSaver.js/master/FileSaver.js"></script>

<script>
Date.prototype.getWeek = function (dowOffset) {
    dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0;
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset;
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};

var cur_date = new Date();
var week = cur_date.getWeek();
var year = cur_date.getFullYear();
var month = cur_date.getMonth();
var mnth = month+1;
var day = cur_date.getDate();
var drilldate = year+'-'+mnth+'-'+day;

 var postApp = angular.module('postApp', []);
    postApp.controller('postController',function($scope,$http,$timeout){
                $scope.week = week;
                $scope.catdisplay = false;
                document.getElementById('exportable1').style.display = "none";
                
                //compid get from session
                $scope.sesdata = '<?php echo $this->session->userdata('compid')?>';

                $scope.sortBy = function(propertyName) {
                    $scope.sortKey = propertyName;
                    $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                };


                $scope.GetData = function(e){
                    $scope.isLoading = true;
                    document.getElementById('exportable1').style.display = "block";
                    var x = document.getElementById("myWeek").value;
                    var catname = document.getElementById("categoryname").value;
                    $scope.category = catname;
                    $scope.catdisplay = true;
                    if (x != '' && catname != '')
                    {
                        var year = x.slice(0,4);
                        var cur_date = new Date(x);
                        var week = cur_date.getWeek();
                        $scope.week = week;
                        $scope.year = year;
                        $scope.drilldate = x;
                        $http({                     
                            method  : 'POST',
                            url     : '<?php echo base_url();?>index.php/CategoryReport/CategoryReport_data',
                            data    : { 'year':year,'week': week,'catname' : catname},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                        .then(function(response) {
                            $scope.isLoading = false;
                            $scope.dashboarddata = response.data.dashboard_category;
                            console.log($scope.dashboarddata);
                        });
                    }
                    else if(x=='')
                    {
                      
                        $scope.isLoading = false;
                        $scope.catdisplay = false;
                        document.getElementById('exportable1').style.display = "none";
                    }
                    else
                    {
                       
                        $scope.isLoading = false;
                        $scope.catdisplay = false;
                        document.getElementById('exportable1').style.display = "none";
                    }
                };

                $scope.exportData = function () {
                    var blob = new Blob([document.getElementById('exportable1').innerHTML], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    });
                    saveAs(blob, "MSR_WEEK_"+$scope.week+".xls");
                }; 
    });
</script>
</html>