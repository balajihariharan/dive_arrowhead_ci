<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
          <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<body class="main">
    <!-- <div class="container head_bg"> -->
   <?php $this->load->view('header.php'); ?>
    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="images/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="images/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="images/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="images/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->
    <div>
        <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            .run_button {
    padding: 7px 4px 6px 7px;
    float: right;
    margin: -11px 2px 0px -3px;
    font-size: 15px !important;
    border: none;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    cursor: pointer;
    font-family: sans-serif, Verdana, Geneva;
    text-align: right;
    display: inline-block;
    color: #fff;
    vertical-align: top;
    background: url(../img/export_data1.png) no-repeat right 1px #00886D;
}
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?>
                    <span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
          <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
                
                    <div class="content_main_all">
                        
                        <a class="run_button" onclick="Recommendation()" style="cursor: pointer;">Run Price Recommendation</a>

                        <!-- <a class="button_right" target = '_blank' href="<?php echo base_url();?>Masterlist/index" style="cursor: pointer;">Run Price Recommendation</a> -->
                        <!--<div class="channel_main">
                            <div class="channel_all">
                                <label>Channel</label>
                                <select class="select_med">
                                    <option>PMT</option>
                                    <option>ebay</option>
                                    <option>Amazon</option>
                                    <option>3dcart</option>
                                    <option>Magento</option>
                                    <option>Big Commerce</option>
                                </select>
                                <span class="amazon_drop"><img src="images/amazon_logo.png"></span>
                            </div>
                           9px 25px 6px 7px;
                        </div>-->
                        <br>
                        <div class="table_main" align="center">
                            <table style="width:50%;"  border="1" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <?php  //echo count($rulelist);
                                    if(count($rulelistglobal) >= 1){
                                        foreach($rulelistglobal as $value){ ?>
                                            <tr>
                                                <td width="100px" style="color: #fff;font-size: 15px;background: #2D8BD5;">
                                                    <?php echo $value['rule_name_short'];?>
                                                </td>
                                                <td width="200px"><?php echo $value['rule_name'];?></td>
                                                <td width="200px" style="text-align: center;"><?php echo $value['synopsis'];?></td>
                                                <td width="80px">
                                                    <input type="checkbox" class="check_small" id="checkval" checked disabled/>
                                                </td>
                                            </tr>
                                    <?php }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="table_main" style="overflow-y: auto;"">
                            <form id="saveruleform" action="" method="post">
                            <table style="width:100%;overflow-x: scroll;" border="1" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Rule ID</th>
                                        <th>Rule Name</th>
                                        <th>Rule Hierarchy</th>
                                        <th>Synopsis</th>
                                        <th>Action</th>
                                        <th>Active</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  /*print_r($rulelist);*/
                                    if(count($rulelist) >= 1){
                                        foreach($rulelist as $value){ ?>

                                            <tr><td><input type="hidden" name="ruleid[]" value="<?php echo $value->rule_id;?>"><?php echo $value->rule_id;?></td>
                                                <td width="100px"><?php echo $value->rule_name;?></td>
                                                <td><input type="text" name="RuleHierarchy[]" class="hireachy" value="<?php echo $value->rule_hierarchy;?>"/></td>
                                                <td style="text-align: left;"><?php echo $value->synopsys;?></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td><a href="<?php echo base_url();?>Pricesmart/priceruleedit/<?php echo $value->rule_id;?>">Edit</a></td>
                                                            <td><a href="#" class="rowdelete" data-value="<?php echo $value->rule_id;?>">Delete</a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="80px"><input type="hidden" name="activeflag[]" value="<?php echo $value->active_flag;?>"><input type="checkbox" class="check_small" id="checkval" data-value="<?php echo $value->rule_id;?>" <?php if($value->active_flag == 1){echo 'checked';} ?>/></td>
                                            </tr>
                                    <?php }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            </form>
                        </div>


                        <div class="pricing_menu">
                            <ul>
                                <li>
                                    <a href="<?php echo base_url();?>Pricesmart/priceruleedit"> <input type="submit" value="Add Rule" class="button_add rawlistdatasubmit"></a>
                                </li>
                                <li>
                                    <input type="submit" value="Save Rule" class="button_add rawlistdatasubmit saverule">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
  <?php $this->load->view('footer.php'); ?>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
     
    </script>
          

    <script>
    $(document).on('click','.rowdelete',function(){
        var myid = $(this).attr('data-value');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>index.php/Pricesmart/price_rule_delete',
                data: {'myid':myid},
                success: function(response){
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                             data: {'msgstatus':4},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                    //location.reload();
                },
                error : function(xhr){
                     var msg =xhr.status;
                     var StatusCode = xhr.status;
                     var StatusText = xhr.statusText;
                     var ErrorHtml  = xhr.responseText; 
                     var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                    //alert('Please try again');
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
    })

    </script>
    <script>
        function Recommendation(){
           var url = "<?php echo base_url();?>Pricesmart/PriceRecomProcess";
           window.open(url, '_blank','width=1200,height=600');
        }
    </script>
    <script>
        $(document).ready(function(){
            $('input[id="checkval"]').click(function(){
                if($(this).prop("checked") == true)
                {
                    var ruleid = $(this).attr('data-value');
                    //alert(ruleid);
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url();?>index.php/Pricesmart/price_rule_check_upd',
                        data: {'ruleid':ruleid, 'flag' : '1'},
                        success: function(response){
                            //location.reload();
                             $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                             data: {'msgstatus':6},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                        },
                        error : function(xhr){
                             var msg =xhr.status;
                             var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                            //alert('Please try again');
                             $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                        }
                    }); 
                }
                else
                {
                    var ruleid = $(this).attr('data-value');
                    //alert(ruleid);
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url();?>index.php/Pricesmart/price_rule_check_upd',
                        data: {'ruleid':ruleid, 'flag' : '0'},
                        success: function(response){
                            //location.reload();
                             $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                              data: {'msgstatus':6},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                        },
                        error : function(xhr){
                             var msg =xhr.status;
                             var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                            //alert('Please try again');
                             $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                        }
                    });
                }   
            });
        });
 </script>
 <script>
         //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            location.reload();
        });
 </script>
 <script>
    $(document).on('click','.saverule',function(){
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>index.php/Pricesmart/pricerulesave',
            data: $("#saveruleform").serialize(),
            success: function(response){
                if(response == 'error'){
                    //alert('RuleHierarchy Should Be Unique Please Change the duplicate value');
                     $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                        data: {'xhrstatus':001},
                            success: function(response){
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block');
                        },
                    })
                }else{
                    $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                        data: {'msgstatus':3},
                            success: function(response){
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block');
                        },
                    })
                }
            },
            error : function(xhr){
                 var msg =xhr.status;
                 var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                 $.ajax({
                 type: 'POST',
                 url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                  data: {'xhrstatus':msg},
                    success: function(response){
                            $('#msg').html(response);
                            $('.err_modal').css('display','block');
                            $('#msg').css('display','block');
                    },
                })
            }
        });
    });
 </script>
</body>
</html>
