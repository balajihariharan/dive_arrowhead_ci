<!DOCTYPE html>
<html ng-app = "psmartApp">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
           <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <!-- <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script> -->
    <!-- <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>
   <!--  <script src="https://js.pusher.com/4.1/pusher.min.js"></script> --><!--pusher notification -->
    
</head>
<body class="main" ng-controller="psmartCntrl" ng-cloak>
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>


    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <!-- <div class="container"> -->
    <!--     <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="images/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="images/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="images/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="images/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    <!--   -->
    <div>
       <!--  <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script> -->
        <style type="text/css">
        .margin_right{
            margin-right: 10px;
        }
        .run_cron_start{
                float: left;
                margin-top: 10px;
                color: #06a206;
                font-size: 20px;
                font-weight: 600;
            }
            .run_cron_start p{
                display: inline-block;
            }
            .run_cron_status{
                float: left;
            }
            .run_cron_status ul li{
                float: left;
                padding: 0 30px;
                /*margin-top: 13px;*/
            }
            .run_cron_status ul li span.run_cron_sta_value{
                display: block;
                padding: 10px 0;
            }
            .run_cron_status ul li span.run_cron_sta_header{
                font-size: 18px;
                 font-weight: 600;
                 color: #2678b9;
            }
        .temp_upload{
            padding: 6px 30px 6px 10px;
            /*margin: 0% 11% 0% 10%;*/
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            font-family: sans-serif, Verdana, Geneva;
            text-align: right;
            display: inline-block;
            color: #fff;
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
            font-size: 13px;
        }
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(0) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            /*.status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }
*/
            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                    display: none !important;
            }
            .pagi_master li{
                list-style-type: none;
                display: inline-block;
                margin: 0 3px;
                background: #5D85B2;
                color: #fff;

                border-radius: 3px;
                cursor: pointer;
                }
                .bg_green{
                    background: #2d8bd5 none repeat scroll 0 0 !important;
                }

                .pagi_master li a{
                color: #fff;
                text-decoration: none;
                padding: 4px;
                }

                #my_file {
                display: none;
                }

                .level2 li input {
                    background: none;
                    padding: 5px 5px 5px 15px !important;
                    color: #fff !important;
                    line-height: 20px;
                    font-size: 15px;
                    text-align: left;
                    margin: 0px;
                    border: none !important;
                    display: block !important;
                    text-decoration: none;
                    cursor : pointer;
                }


                .temp_upload_new{
            padding: 6px 30px 6px 10px;
           margin: 5% 1% 0% 0%;
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            font-family: sans-serif, Verdana, Geneva;
            text-align: right;
            display: inline-block;
            color: #fff;
            background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
            font-size: 13px;
        } 

        .heading_pop {
    font-family: sans-serif, Verdana, Geneva;
    font-size: 14px;
    font-weight: 700;
    padding: 0px 0px 0px 8px;
    float: left;
    color: #696767;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 78px;
        background-color: #2C3E50;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
    }

    .tooltip1 .tooltiptext {
        visibility: hidden;
        width: 78px;
        background-color: #2C3E50;
        color: #fff;
        text-align: center;
        border-radius: 9px;
        padding: 3px 24px 3px 7px;
        position: absolute;
        margin: 17px -50px;
    }


    .tooltip1 .tooltiptext1 {
            visibility: hidden;
            width: 78px;
            background-color: #2C3E50;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 2px 20px 3px 4px;
            position: absolute;
            margin: 17px -50px;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .tooltip1:hover .tooltiptext {
        visibility: visible;
    }

    .tooltip1:hover .tooltiptext1 {
        visibility: visible;
    }

    .GreenCheck{
        background-color: green; /*#004d00*/
        width: 373px;
        height: 19px;
        color: white;
    }

    .RedCheck{
        background-color: #b30000; /*#b30000*/
        height: 19px;
        width:388px;
        margin-top: -18px;
        margin-left: 389px;
        color: white;
    }
   .GreyCheck{
        background-color: #808080; /*#b30000*/
        height: 19px;
        width:373px;
        /*margin-top: -18px;
        margin-left: 235px;*/
        color: white;
    }
    .OrangeCheck{
        background-color: #FF6600; /*#b30000*/
        height: 19px;
        width:388px;
        margin-top: -18px;
        margin-left: 391px;
        color: white;
    }
    .viewicon{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/trend_icon.png') no-repeat scroll 5px center;
                    padding: 3px 6px 0px 30px;
                    }
    .run_button {
    padding: 7px 4px 6px 7px;
    float: right;
    margin: 9px 2px 0px -3px;
    font-size: 15px !important;
    border: none;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    cursor: pointer;
    font-family: sans-serif, Verdana, Geneva;
    text-align: right;
    display: inline-block;
    color: #fff;
    vertical-align: top;
    background: url(../img/export_data1.png) no-repeat right 1px #00886D;
}
.cron_upload{
            margin: 0% 11% 0% 0%;
        }
/*******edit popup  start*****/
.pop_pad_all_add_new .channel_main{
                width:93%;
                margin: 0 auto;
                display: block;
                overflow: hidden;
            }
            .label_inner_general{
                font-weight: 600;
                width: 260px;
            }
            .pop_pad_all_add_new{
                padding: 20px;

            }
            .save_icon_manage{
                float: none;
            }

            .legendclass{
                color: #2C3E50;
                font-weight: bold;  
            }

            .fieldsetclass{
                /*border-color: #2C3E50;*/
                padding: 8px 10px;
                margin: 8px 0;
                border-radius: 2px;
                border: 1px solid #cccccc8a;
                box-shadow: 0px 1px 6px rgba(199, 199, 199, 0.54);
            }

            /*.filterclass{
                background: #dee7f8 none repeat scroll 0 0;
                border: 1px solid #cad8f3;
                border-radius: 6px;
                padding: 0 5px;
                float: left;
                font-size: 11px;
            }*/
             .filterclass{
                margin: 6px 0;
                padding: 0 5px;
                font-size: 13px;
            }
           /* .filterclass span{
                background: #dee7f8 none repeat scroll 0 0;
            }*/
            .filterclass p{
            background: #dee7f8 none repeat scroll 0 0;
            display: inline-block;
            border-radius: 6px;
             padding: 0 5px;
            }
            .filterclass strong{
                font-weight: 600;
            }

            .bold{
                font-weight: bold;
            }
            #searchResult_main{
             border: 1px solid #aaa;
            overflow-y: auto;
            width: 205px;
            position: absolute;
            margin-left: 83px;
            padding-bottom: 0px;
            min-height: 10px;
            max-height: 150px;
            }

            #searchResult_main li{
             background: white;
             padding: 4px;
            cursor: pointer;
                list-style: none;
                border-bottom: 1px solid #ededed;
                font-size: 14px;
            }

            #searchResult_main li:hover{
                background: #f1f1f1;
            }
            #searchResult{
                border: 1px solid #aaa;
                overflow-y: auto;
                width: 205px;
                position: absolute;
                /*margin-left: 365px;*/
                margin-left: 83px;
                padding-bottom: 0px;
                min-height: 10px;
                max-height: 150px;
                margin-top: 0px;
            }

            #searchResult li{
             background: white;
             padding: 4px;
            cursor: pointer;
                list-style: none;
                border-bottom: 1px solid #ededed;
                font-size: 14px;
            }

            #searchResult li:hover{
                background: #f1f1f1;
            }
             .closecrawler {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }
             .closecrawler:hover,
                .closecrawler:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }
                .cron_upload{     
            padding: 6px;     
            margin: 0% 11% 0% 0%;     
            border: none;       
            -webkit-border-radius: 3px;     
            -moz-border-radius: 3px;        
            border-radius: 3px;     
            cursor: pointer;        
            font-family: sans-serif, Verdana, Geneva;       
            text-align: right;      
            display: inline-block;      
            color: #fff; 
            background: linear-gradient(#009e47, #00713b);      
            /*background: url(<?php echo base_url();?>/assets/img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);*/      
            font-size: 13px;        
        }
/********edit popup end*******/


        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>

                </div>
                <div class="total_items">
                    <span>Last Run Date : <?php echo $rundate[0]->rundate;?>
            </div>
        </div>

                 <div id="msg" style="display: none;"></div>

          <!-- error handling popup-->
          <div class="progrss_confirm" >
         <div id="success" class="err_modal " style="display:none;">
             <div class="alert_box" class="popup" class="modal-content" >
                     <div class="fleld-alert success"> 

                           <div class="head_icon_green" style="background: #1376AF none repeat scroll">
                              <img src="<?php echo base_url();?>/assets/img/success_icon.png"  alt="Logout" title="Logout" class="destroy" />
                          </div>

                            <div class="alert alert-warning">
                             <strong ng-hide="msghide" style="font-size: 19px;padding:0px 0px">We are about to update the Price for the Selected Items on your eBay Store. Click "Yes", to confirm. Click "No" to Cancel.</strong><br>
                             <strong class="msgprgstatic content_dynamic"> in Progress..</strong>
                            <strong class="msgprogressitem"></strong>
                            <strong class="msgprogress"></strong>
                             <p></p>
                            </div>

                           <div class="alert_ok_btn butn_hide">
                              <button class="btn_ok_sty_green clck" ng-model="msghide" style="margin-left: -57px;">Yes</button>
                            </div>
                            <div class="alert_ok_btn butn_hide">
                              <button class="btn_ok_sty_green nonclck" style="margin: -16px 0 0px 26px;background-color:#b30000;left: 46%;">No</button>
                            </div>

                           </div>
                      </div>
                   </div>
                   </div>
                   <!-- -->
                 <!-- error handling popup end-->           
        
        <div class="container">
            <div class="wrapper">
            <div class="content_main_all">

               <!--  <a class="run_button" onclick="Recommendation()" style="cursor: pointer;">Run Price Recommendation</a> -->
                 <!--<div class="field_radio">
                    <input type="radio" class="field_chk"> Show that dont sell
                    <input type="radio" class="field_chk"> Show  my item
                </div>-->
                <div class="button_right">

                   
                     <li class="submenu">
                        
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="button_download" value="Export Complete List"/>
                            </a>
                            <ul class="level2">
                                <li>
                                    <a href="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_PriceSmart.csv">As CSV</a>
                                </li>    
                                <li>
                                    <form action="<?php echo base_url();?>PriceSmart/export_excel" method="post">
                                        <input type="hidden" name="flag" id="flaginput" value="a"/>
                                        <input type="submit" class="last_menu_icon" value="As Excel"/>
                                    </form> 
                                </li>
                            </ul>
                        </li>

                </div>

                <div class="button_right" id="exportsel">
                   
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="button_download" value="Export Selected"/>
                            </a>
                            <ul class="level2">
                                <li>
                                    <a href="#" class="last_menu_icon" ng-csv="ForCSVExport2" filename="DIVE_PriceSmart.csv">As CSV</a>
                                </li>    
                                <li>
                                    <form action="<?php echo base_url();?>PriceSmart/export_excel_selected" method="post">
                                        <input type="hidden" name="temp" id="my_hidden_input"/>
                                        <!-- <input type="hidden" name="exportvaldata[]" value="exportvaldata[]"/> -->
                                        <input type="submit" class="last_menu_icon" value="As Excel"/>
                                    </form> 
                                </li>
                            </ul>
                        </li>

                </div>

                <div class="button_right" id="exportsel">
                   
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="button_download" value="Export - Channel Advisor"/>
                            </a>
                            <ul class="level2">
                                <li>
                                    <a style="text-align: center;" href="#" class="last_menu_icon" ng-csv="ForCSVExport4" filename="DIVE_PriceSmart.csv">Complete List</a>
                                </li>    
                                <li id="selected">
                                    <a style="text-align: center;" href="#" class="last_menu_icon" ng-csv="ForCSVExport3" filename="DIVE_PriceSmart.csv">Selected List</a>
                                </li>
                            </ul>
                        </li>

                </div>
                
                <div class="button_right margin_right">
                    <a href="#" class="last_menu_icon">
                        <input type="button" class="temp_upload" ng-click="import()" value="Import"/>
                    </a>
                </div>

                 <div class="button_right margin_right">
                    <a href="#" class="last_menu_icon">
                        <input type="button" class="cron_upload" ng-click="Cron()" value="Start Run"/>
                    </a>
                </div>

                        <div id="run_cron" class="run_cron_start" style="display: none;"><p>Process Scheduled Successfully</p>
                           <!--  <div class="tooltip">
                                    <img style="vertical-align: middle;" src="<?php echo base_url();?>assets/img/refresh.png" ng-click="cron_status()">
                                    <span class="tooltiptext">Refresh</span>
                                </div> -->
                        </div>
                        <div class="run_cron_status" style="display: none;">
                            <ul>
                                <li><span class="run_cron_sta_header">Start Date</span>
                                    <span class="run_cron_sta_value">{{cron_sta_startdate}}</span>
                                </li>
                                <li>
                                    <span class="run_cron_sta_header">End Date</span>
                                    <span class="run_cron_sta_value">{{cron_sta_enddate}}</span>
                                </li>

                                <li>
                                    <span class="run_cron_sta_header">Status</span>
                                    <span class="run_cron_sta_value">{{cron_sta_progress}} out of {{cron_sta_total}}</span>
                                </li>
                                <!-- <li class="tooltip">
                                    <img style="margin-top: 9px;" src="<?php echo base_url();?>assets/img/refresh.png" ng-click="cron_status()">
                                    <span class="tooltiptext">Refresh</span>
                                </li> -->
                            </ul>
                            
                        </div>
                        <div id="run_cron_end"  class="run_cron_start" style="display: none;"><p>Process Completed Successfully</p></div>

               
                    <input type="file" id="my_file" file-reader="parts"/>

                <div class="plan_list_view">
                <div>

                    <div class="GreenCheck">
                        <input type="checkbox" id="green" ng-click="ShowSelected('green')" checked/>
                         I'm lonely Lowest: Rec.Price > Current Price 
                    </div>
                    
                    <div class="RedCheck">
                        <input type="checkbox" id="red" ng-click="ShowSelected('red')" checked/>
                      Not Lowest :Rec.Price < Current Price but losing
               </div>
               <div>
                    <div class="GreyCheck">
                        <input type="checkbox" id="grey" ng-click="ShowSelected('grey')" checked/>
                        I'm Lowest : Rec.Price = Current Price
                    </div>
                    <div class="OrangeCheck">
                        <input type="checkbox" id="orange" value="orange" ng-click="ShowSelected('orange')" checked/>
                        Not Lowest : Rec Pr < Current Price but Profitable
                    </div>
                </div>
                <br>
               <div class="lable_normal table_top_form"><input id="ckbCheckAll" ng-click="allselect()" type="checkbox" ng-model="checked"  />
                    Select All - Across Pages</div>
                    <div class="itm_lst"><p >Total Item Listed:</p><p style="font-size: 20px; color: #00886d;">{{totalitems}}</p></div>
                    <table cellspacing="0" border="2" class="table price_smart_grid" id="mytable">
                         <col>
                          <colgroup span="2"></colgroup>
                          </col>
                        <thead>
                            <tr>
                                <th rowspan="2">Sel.</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('MyPartNo')">My Part#</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('ItemId')">Item ID</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('SKU')">SKU</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('Title')">Title</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('Category')">Category</th>
                                <th colspan="8" scope="colgroup" style="text-align: center;">Price (In $)</th>
                                <th class="curser_pt" rowspan="2" ng-click="sortBy('RuleId')">Rule ID</th>
                              </tr>
                            <tr class="bg_green">
                                <th scope="col" class="curser_pt clr_lg_blue" ng-click="sortBy('Cost_Price')">Cost Price</th>
                                <th scope="col" class="curser_pt clr_lg_blue" ng-click="sortBy('Freight')">Freight 
                                <th scope="col" class="curser_pt clr_lg_blue" ng-click="sortBy('Fees')">Fees 
                                <th scope="col" class="curser_pt" ng-click="sortBy('MyPrice')">My Price</th>
                                <th scope="col" class="curser_pt" ng-click="sortBy('Comp_Low_Price')">Comp. L.Price</th>
                                <th scope="col" class="curser_pt" ng-click="sortBy('Comp_High_Price')">Comp. H.Price</th>
                                <th scope="col" class="curser_pt clr_lg_blue" ng-click="sortBy('TotalPrice')">Total Price</th>
                                <th scope="col" class="curser_pt clr_lg_blue" ng-click="sortBy('Recomend_Price')">Recommended Price</th>
                          </tr>
                          </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" ng-model="selectall[currentPage]" id="selectall" ng-click="selectallfunc()"/></td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text" ng-model="partquery" ng-change="partsearch()" class="inputsearch" placeholder="My Part#"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="itemquery" ng-change="itemsearch()" class="inputsearch" placeholder="Item Id"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="skuquery" ng-change="skusearch()" class="inputsearch" placeholder="SKU"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="titlequery" ng-change="titlesearch()" class="inputsearch" placeholder="Title"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="catquery" ng-change="catsearch()" class="inputsearch" placeholder="Category"/>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="r_pricequery" ng-change="r_pricesearch()" class="inputsearch" placeholder="Recommended Price"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="ruleidquery" ng-change="ruleidsearch()" class="inputsearch" placeholder="Rule ID"/>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody ng-repeat="(key,part) in pagedItems[currentPage] track by $index | orderBy:sortKey:reverse">
                             <tr class="edit_type checkflag_{{part.alert_flag}}" ng-hide="checkflag_{{part.alert_flag}}">   
                                <td id="{{part.ItemId}}">
                                    <input type="checkbox" element="{{part.ItemId}}" ng-checked="selectall[currentPage] || selectlist[part.ItemId] || checked" id="{{part.ItemId}}" ng-model="selectlist[part.ItemId]" ng-click="selectfunc($event)"/>
                                </td>
                                <td>{{part.MyPartNo}}</td>
                                <td class="curser_pt_new viewicon" data-id="{{part.ItemId}}" ng-click="SalesTrend($event)">{{part.ItemId}}</td>
                                <td>{{part.SKU}}</td>
                                <td>
                                    <a ng-click="redirectToLink1($event)" element="{{part.ItemId}}" style="cursor: pointer;">{{part.Title}}</a>
                                </td>
                                <td>{{part.Category}}</td>
                                <td>{{part.Cost_Price}}</td>
                                <td>{{part.Freight}}</td>
                                <td>{{part.Fees}}</td>
                                <td>{{part.MyPrice}}</td>
                                <td ng-if="part.Comp_Low_ItemId == ''">{{part.Comp_Low_Price}}</td>
                                <td class="tooltip1 curser_pt_new" ng-if="part.Comp_Low_ItemId != '' && part.RuleId != 'Auto Rawlist'">
                                <a ng-click="redirectToLink1($event)" element="{{part.Comp_Low_ItemId}}" style="cursor: pointer;">{{part.Comp_Low_Price}}</a>
                                <span class="tooltiptext1">{{part.Comp_Low_ItemId_Name}}</span>
                                </td>
                                <td class="tooltip1 curser_pt_new" ng-if="part.Comp_Low_ItemId != '' && part.RuleId == 'Auto Rawlist' ">
                                <a ng-click="redirectToLink1($event)" element="{{part.Comp_Low_ItemId_Name}}" style="cursor: pointer;">{{part.Comp_Low_Price}}</a>
                                <span class="tooltiptext1">{{part.Comp_Low_ItemId_Name}}</span>
                                </td>
                                <td class="tooltip1 curser_pt_new" ng-if="part.Comp_High_ItemId != '' && part.RuleId != 'Auto Rawlist'">
                                <a ng-click="redirectToLink1($event)" element="{{part.Comp_High_ItemId}}" style="cursor: pointer;">{{part.Comp_High_Price}}</a>
                                <span class="tooltiptext">{{part.Comp_High_ItemId_Name}}</span>
                                </td>
                                 <td class="tooltip1 curser_pt_new" ng-if="part.RuleId == 'Auto Rawlist'">N/A
                                </td>
                                <td ng-if="part.Comp_High_ItemId == ''">{{part.Comp_High_Price}}</td>
                                <td>{{part.TotalPrice}}</td> 
                                <td ng-if="part.alert_flag == 'g'">
                                    <input style="color: green; text-align: center; font-weight: bold; font-size: 15px;" type="text" class="input_table editable_input_blck" ng-model="part.Recomend_Price" ng-click="change(part.ItemId)"/>
                                </td>
                                <td ng-if="part.alert_flag == 'Y'">
                                    <input style="color: #808080; text-align: center; font-weight: bold; font-size: 15px;" type="text" class="input_table editable_input_blck" ng-model="part.Recomend_Price" ng-click="change(part.ItemId)"/>
                                </td>
                                <td ng-if="part.alert_flag == 'o'">
                                    <input style="color: #FF6600; text-align: center; font-weight: bold; font-size: 15px;" type="text" class="input_table editable_input_blck" ng-model="part.Recomend_Price" ng-click="change(part.ItemId)"/>
                                </td>
                                <td ng-if="part.alert_flag == 'r'"">
                                    <input style="color: red; text-align: center; font-weight: bold; font-size: 15px;" type="text" class="input_table editable_input_blck" ng-model="part.Recomend_Price" ng-click="change(part.ItemId)"/>
                                </td>    
                                <td ng-if="part.RuleId == 'N/A' || part.RuleId == 'GLOBAL-L' || part.RuleId == 'GLOBAL-NL'" style="font-weight: bold;">{{part.RuleId}}</td>
                                <td class="tooltip" ng-if="part.RuleId != 'N/A' && part.RuleId !='GLOBAL-L' && part.RuleId !='GLOBAL-NL' && part.RuleId != 'Auto Rawlist'">
                                    <a ng-click="redirectToLink($event)" element="{{part.RuleId}}" style="cursor: pointer;">{{part.RuleId}}</a>
                                    <span class="tooltiptext">{{part.RuleName}}</span>
                                </td>
                                 <td class="tooltip" ng-if="part.RuleId == 'Auto Rawlist'" ><a ng-click="editpopup($event)" dataitemid="{{part.ItemId}}"> {{part.RuleId}}</a>
                                </td>
                            </tr>
                        </tbody>
                   </table>

                  <?php
                         echo '<div id="popupform">
                                    <div id="Pnopopup" class="modal">

                                            <div class="pop_map_my_part">
                                               
                                                 <div class="pop_container" style="height: 191px;">
                                                        <div class="heading_pop_main">
                                                            <h4>

                                                                Import PriceSmart #
                                                            </h4>
                                                             <span class="close">&times;</span>
                                                        </div>
                                                       <div class="pop_pad_all">
                                                           <div style="color:#009e47; text-align:center; font-size:17px">{{message}}</div>
                                                           <div style="color:#ff3333; text-align:center; font-size:17px">{{errormessage}}</div>
                                                        </div>

                                                        <div class="button_center">
                                                            <input type="hidden" id="ftype"/>
                                                        
                                                        <input type="text" id="fname" class="input_med" readonly/>
                                                        <button type="submit" id="popsavemain" class="temp_upload_new" ng-click="upload()">Upload</button><span style="font-size:16px; font-weight:bold;">(.csv)
                                                        </div>
                                                        <div class="channel_main">
                                                           
                                                        </div>

                                                        <div class="button_center padding_bottom_15">
                                                        <button type="submit" id="save_imp" class="button_download" ng-click="save_imp()">Save</button>
                                                        </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>';
                        ?>

                        <div id="Pnopopupsalestrends" class="modal" style="display:none;">
                    <div class="modal-content" style="display:block; width:80%">
                       <div class="lable_normal table_top_form">
                       
                       <input id="showvalues" ng-model="checkboxModel.showvalues" ng-click="salestrendsshowvalues()" type="checkbox">Show values</div>
                        <span id="myPnopopupsubclose" ng-click="myPnopopupsubclose()" class="myPnopopupsubclose">&times;</span>
                    
                     <center><div id="NoRecord" style="display:none;">No Record Found</div></center>
                        <table class="status_bar" cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
                                <th style="width:8%;">Item ID</th>
                                <th style="width:6%; min-width:0px;">Qty. Sold (Since Inc.)</th>
                                <th style="width:6%; min-width:0px;">Qty. Sold (5 wk)</th>
                                <th style="width:6%; min-width:100px;">Total Sales (5 wk in $)</th>
                                <th style="width:20%;">5 wk Price (In $)</th>
                                <th style="width:20%;">5 wk Qty.Sold</th>
                            </tr>
                            </thead>
                            <tfoot>
                               <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td ng-repeat="weekno in week">
                                        <em>{{weekno.wkno1}}</em>
                                        <em>{{weekno.wkno2}}</em>
                                        <em>{{weekno.wkno3}}</em>
                                        <em>{{weekno.wkno4}}</em>
                                        <em>{{weekno.wkno5}}</em>
                                    </td>
                                    <td ng-repeat="weekno in week">
                                        <em>{{weekno.wkno1}}</em>
                                        <em>{{weekno.wkno2}}</em>
                                        <em>{{weekno.wkno3}}</em>
                                        <em>{{weekno.wkno4}}</em>
                                        <em>{{weekno.wkno5}}</em>
                                    </td>
                             </tr>
                             </tfoot>
                                <tbody>
                                <tr style="display: table-row;" ng-repeat="sales in saledata_inner">
                                    <td><span style="font-weight: bold;font-size: 15px">{{sales.itemid}} </span></td>
                                    <td class="alignment">
                                    <div class="tbl_txt">
                                     <div class="price_text">{{sales.qtysoldinc}}</div>
                                    </div>
                                    </td>
                                    <td class="alignment">
                                    <div class="tbl_txt">
                                     <div class="price_text">{{sales.qtysold}}</div>
                                    </div>
                                    </td>
                                    <td class="alignment">
                                        <div class="tbl_txt">
                                            <div class="price_text" style="margin-right:10%">{{sales.totalsales}}</div>
                                        </div>
                                    </td>
                                    <td>
                                    <div class="tbl_img">
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'u'" class="price_up"></div>
                                    </div>
                                    <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                            <div class="price_text">{{sales.pricevw1}}</div>
                                            <div class="price_text">{{sales.pricevw2}}</div>                              
                                            <div class="price_text">{{sales.pricevw3}}</div>
                                            <div class="price_text">{{sales.pricevw4}}</div>
                                            <div class="price_text">{{sales.pricevw5}}</div>
                                    </div>
                                    </td>
                                    <td>
                                    <div class="tbl_img">
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'u'" class="price_up"></div>

                                            <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                <div class="price_text">{{sales.qtysoldvw1}}</div>
                                                <div class="price_text">{{sales.qtysoldvw2}}</div>                              
                                                <div class="price_text">{{sales.qtysoldvw3}}</div>
                                                <div class="price_text">{{sales.qtysoldvw4}}</div>
                                                <div class="price_text">{{sales.qtysoldvw5}}</div>
                                            </div>
                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <br><br>
                                                     <p ng-if="saledata_inner.length == 0" class="nodata2">No Competitor is Mapped to this Item ID</p>
                                               </div>
                                            </div>

                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>     
                        </div>
                </div>
                <div class="button_left">
                        <!-- button_sync -->
                    <button value="Search" id="myselector" ng-click="save()" class="temp_upload_new" type="submit">Save</button>
                </div>
                <div class="button_left">
                     <input type="button" id="sync_ebay" class="temp_upload_new" ng-click="sync_ebay()" value="Sync With eBay" style="
                         margin: 3px 0px 0px 30px;">
                </div>
                <div class="button_left">
                     <input type="hidden" id="proceedtoebay" class="temp_upload_new" ng-click="proceedtoebay()" value="proceedtoebay" style="
                         margin: 3px 0px 0px 30px;">
                </div>
        </div>
        </div>
        <?php $this->load->view('footer.php'); ?>
    </div>
     <!--edit popup -->
   <div id="Pnopopup2" class="modal">
    <div class="pop_map_my_part conf_auto_rawlist">          
        <div class="pop_container">
            <div class="heading_pop_main">
                <h4>Edit</h4>
                <span class="close" ng-click="popupclose()">&times;</span>
            </div>
            <div class ="pop_pad_all_add_new" style="color: #666;">

                <fieldset class="fieldsetclass">
                    <legend class="legendclass" align="center">Item Specifics</legend>
                    <div>
                        <table class="pop_item_spec">
                            <tr>
                                <td style="font-weight: bold;">Item ID</td>
                                <td ng-bind="currentitemid" style="color: chocolate;font-weight: bold;"></td>
                                <td style="font-weight: bold;">SKU</td>
                                <td ng-bind="currentsku"></td>
                            </tr>
                            <!-- <tr>
                                <td style="font-weight: bold;">SKU</td>
                                <td ng-bind="currentsku"></td>
                            </tr> -->
                            <tr>
                                <td style="font-weight: bold;">Category</td>
                                <td ng-bind="currentcatid"></td>
                                <td style="font-weight: bold;">Title</td>
                                <td><a href="https://www.ebay.com/itm/{{currentitemid}}" target="blank">{{currenttitle}}</a></td>
                            </tr>
                            <!-- <tr>
                                <td style="font-weight: bold;">Title</td>
                                <td><a href="https://www.ebay.com/itm/{{currentitemid}}" target="blank">{{currenttitle}}</a></td>
                            </tr> -->
                        </table>
                    </div>
                </fieldset>
            
                 <div id="edit_mp_ip_opn" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #3966ad; border-radius: 5px; color: white;padding: 8px 0px 8px 0px;">MPN,OPN,IPN Not Available...</p></center>

                </div>
                 <div class="left_fieldset">
                    <fieldset class="fieldsetclass popup_keyword ">
                        <legend class="legendclass">Keywords</legend>
                        <div>
                            <label for="exckeyword">
                                OR (Any Words Any Order) :
                                <!-- ng-model="keywordsAND" -->
                            <textarea class="autosizeinput" id="orinput" width="100%" ng-model="keywordsor" value=""></textarea>
                                <!-- <input type="text" style="width:100%; margin-top: 10px;" maxlength="300" /> -->
                            </label>
                            
                            <label for="exckeyword">
                                AND (All Words Any Order) : 
                                <!-- ng-model="keywordsOR" -->
                                <input type="text" class="input_box_full" id="andinput" ng-model="keywordsand" value=""/>
                            </label>
                        </div>
                             <label class="exckeyword" for="exckeyword">
                             Exclude KeyWords : 
                                <input type="text" name="exckeyword" ng-model="keywordsmore" maxlength="300" value=""/>
                                (Comma Separated)
                            </label>
                            <input type="submit"  style="padding: 7px 24px 5px 6px;" ng-click="reset()" ng-disabled="reset_disable" class="button_search" value="Reset">
                            
                        </fieldset>
                     <div id="mp_ip_opn" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #3966ad; border-radius: 5px; font-size:14px; color: white;padding: 8px 0px 8px 0px;">MPN,OPN,IPN Not Available...<span>Do You Want to set Previous Keyword  <button class="mp_ip_yes" type="submit" ng-click="set_yes()">Yes</button><button class="mp_ip_no" type="submit" ng-click="set_none()">No</button> </span></p></center>

                    </div>


                    <fieldset class="fieldsetclass ship_loc">
                      <legend class="legendclass">Shipping & Location</legend>
                        <label for="freeship">
                            <input  type="checkbox" id="freeship" name="freeship" ng-model="freeshiping" element="Free Shipping" ng-true-value="'Free Shipping'" ng-false-value="''" value="1">Free Shipping</label>
                        
                        <label for="location">
                            <input  type="checkbox" id="location" name="location" element="Only US"  ng-model="location" ng-true-value="'Only US'" ng-false-value="''" value="1">Only US</label><br/>
                    </fieldset>
                    <fieldset class="fieldsetclass sort_order">
                      <legend class="legendclass">Sort Order</legend>
                            <label>Sort Order :</label>
                            <!-- $event,this.options[this.selectedIndex].getAttribute('itemvalue') -->
                                <select style="color: #505050;" ng-model="sortorder" ng-change="sortorderchange()" class="sortorderselect">
                                    <option value="0">--Select--</option>
                                    <option itemvalue="Best Match" value="12">Best Match</option>
                                    <option itemvalue="Time: ending soonest" value="1">Time: ending soonest</option>
                                    <option itemvalue="Time: newly listed" value="10">Time: newly listed</option>
                                    <option itemvalue="Price + Shipping: lowest first" value="15">Price + Shipping: lowest first</option>
                                    <option itemvalue="Price + Shipping: highest first" value="16">Price + Shipping: highest first</option>
                                    <option itemvalue="Distance: nearest first" value="7">Distance: nearest first</option>
                                </select>
                    </fieldset>

                    </div>
                    <div class="right_fieldset">
                         <fieldset class="fieldsetclass"> 
                            <legend class="legendclass">Category</legend>
                            <div>
                                <div class="cate_left">
                                <label>Exclude</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="loadcategory" id="normal_search_inc" ng-change="partnosearch_main()" ng-model="partnofirst" value="" autocomplete="off" >

                                 <ul id='searchResult_main' style="display: none;">
                                                        <li ng-click='setValue_main(result.category,result.CategoryID)' ng-repeat="result in suggestcat | filter : partnofirst" id="cat_inc" value="{{result.CategoryID}}" ><p ng-click="get_category(result.category,result.CategoryID,'Exclude')">{{result.category}}</p></li>
                                 </ul>
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="cate_right">
                                <label>Include</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="loadcategory" id="normal_search_exc" ng-change="partnosearch()" ng-model="partno" value="" autocomplete="off" > 
                                     <ul id='searchResult' style="display: none;">
                                                        <li ng-click='setValue(result.category,result.CategoryID)' ng-repeat="result in suggestcat | filter : partno" id="cat_exc"  value="{{result.CategoryID}}" ><p ng-click="get_category(result.category,result.CategoryID,'Include')">{{result.category}}</p></li>
                                    </ul>
                                
                            </div>
                            </div>
                        </fieldset>
                        <fieldset class="fieldsetclass popup_price">
                            <legend class="legendclass">Price</legend>
                                <label for='price' >Percentage</label>
                                ± <input type='text' value="" id="percent"  style="min-width:5%; max-width: 8%" ng-model="percent" ng-change="percentchange()" style=""/> <span style="font-size: 14px; font-weight: 600;">% of My Price [{{currentprice}}] </span><!-- {{currentprice}} -->
                                <!-- <br><br> -->
                                <label for='price' class="from_price">From</label> 
                                $ <input type='text' ng-model="minus" style="min-width:5%; max-width: 8%" name="minus"/> To 
                                $ <input type='text' ng-model="plus" style="min-width:5%; max-width: 8%" name="plus"/>   <!-- <input ng-click="clearpercent();" type="button" value="Clear"> -->
                        </fieldset>
                        <fieldset class="fieldsetclass">
                            <legend class="legendclass">Condition</legend>
                            <label for="connew">
                                <input type="checkbox" id="con_New" name="condition" ng-model="condition"  ng-true-value="'3'" ng-click="conditionchange($event)" element="New" ng-false-value="''" value="3" ng-checked="isChecked">
                                New
                            </label>
                            <label for="conused">
                                <input type="checkbox" id="con_Used" name="condition" ng-model="condition" ng-true-value="'4'" ng-false-value="''" ng-click="conditionchange($event)" element="Used" value="4" ng-checked="isChecked">
                                Used
                            </label>
                            <!-- <label for="remanu">
                                <input type="checkbox" id="con_Remanufactured" name="condition" ng-model="condition" ng-true-value="'Remanufactured'" ng-false-value="''" ng-click="conditionchange($event)" element="Remanufactured" value="2" >
                                Remanufactured
                            </label>
                            <label for="conused">
                                <input type="checkbox" id="con_parts" name="condition" ng-model="condition" ng-true-value="'For Parts Or Not Working'" ng-false-value="''" ng-click="conditionchange($event)" element="For Parts Or Not Working" value="1" >
                                For Parts Or Not Working
                            </label> -->
                            <label for="connc">
                                <input type="checkbox" id="con_notspecified" name="condition" ng-model="condition" ng-true-value="'10'" ng-false-value="''" ng-click="conditionchange($event)" element="Not Specified" value="10" ng-checked="isChecked">
                                Not Specified
                            </label>
                        </fieldset>
                    </div>
                <div class="left_fieldset">
                    
                </div>
                <div class="right_fieldset">
                    <fieldset class="fieldsetclass">
                        <legend class="legendclass">Sellers</legend>
                            <div>
                                <label style="display: block; margin-bottom: 7px;">
                                    Specific Sellers (Enter Seller's User Id)
                                </label>
                                <div class="cate_left">
                                <label for='includeseller'>Include</label>
                                <input type='text' id="comp_inc" ng-blur="get_competitor(comp_inc,'Include')" ng-model="comp_inc" value="" />
                                </div>
                                <div class="cate_right">
                                <label for='excludeseller'>Exclude</label> 
                                <input type='text' id="comp_exc" ng-blur="get_competitor(comp_exc,'Exclude')" ng-model="comp_exc" value=""/>  
                                                 
                                <br/>
                            </div>
                    </fieldset>

                </div>
                <div class="left_fieldset">
                    
                </div>
                 <div class="full_fieldset">
                    <fieldset class="fieldsetclass">
                        <legend class="legendclass" style="text-align:center;">Applied Filters</legend>
                        <div>
                            <!--  Forklifts,Solenoids -->
                            <li class="filterclass" ng-if="keywordsor != ''"><p><span class="bold" ng-if="keywordsor != ''">OR : </span>{{keywordsor}}
                                <span ng-click="eventclose($event)" datalabel="keywordsor" datavalue="{{keywordsor}}" ng-if="keywordsor != ''" style="cursor:pointer">&nbsp;&nbsp;x</span></p>
                            </li>
                            
                            <li class="filterclass" ng-if="keywordsand != ''"><p><span class="bold" ng-if="keywordsand != ''">AND : </span>{{keywordsand}}
                                <span ng-click="eventclose($event)" datalabel="keywordsand" datavalue="{{keywordsmore}}" ng-if="keywordsand != ''" style="cursor:pointer">&nbsp;&nbsp;x</span></p>
                            </li>
                            
                            <li class="filterclass" ng-if="keywordsmore != ''"><p><span class="bold" ng-if="keywordsmore != ''">KeyWord : </span>{{keywordsmore}}
                                <span ng-click="eventclose($event)" datalabel="keywords" datavalue="{{keywordsmore}}" ng-if="keywordsmore != ''" style="cursor:pointer">&nbsp;&nbsp;x</span></p>
                            </li>
                            
                            <!-- ng-hide="resultlabellength == 0 || resultlabellength == NULL" -->
                            <li class="filterclass" ng-model="resultlabellength"><p>
                                <!-- ng-hide="resultlabellength == 0 || resultlabellength == NULL" -->
                                <span class="bold" ng-model="resultlabellength">Category : </span>
                                <span ng-repeat="(key,catvalue) in resultcatlabel" cate-value="{{catvalue.id}}">{{catvalue.name}}<span ng-click="eventclose($event)" datalabel="category" datavalue="{{catvalue.id}}" style="cursor:pointer">&nbsp;x&nbsp;&nbsp;</span></span></p>
                            </li>
                            
                            <!-- <span ng-show="!$last">,</span>  ng-hide="pricevalue == 0"-->
                            <!--     -->
                            <li class="filterclass" ng-model="pricevalue" ng-hide="pricevalue == 0"><p><span class="bold" ng-hide="pricevalue == 0" ng-model="pricevalue">Price : </span><strong>From </strong>${{minus | currency : ''}} <strong>To</strong> ${{plus | currency : ''}}
                                <span ng-click="eventclose($event)" datalabel="pricelabel" style="cursor:pointer">&nbsp;&nbsp;x</span><p>
                            </li>
                            
                             <li class="filterclass" ng-if="resultcondition != NULL" ng-if="resultcondition != ''"><p><span class="bold" ng-if="resultcondition != ''">Condition :</span>{{resultcondition}}
                                <span ng-click="eventclose($event)" datalabel="conditionlabel" ng-if="resultcondition != ''" style="cursor:pointer">x</span></p>
                            </li>
                            
                            <li class="filterclass" ng-if="freeshiping || location != NULL"><p>
                                <span class="bold" ng-if="freeshiping == 'Free Shipping' || location == 'Only US'">
                                    Shipping &amp; Location :</span> {{freeshiping}}
                                    <span ng-click="eventclose($event)" datalabel="freeshiping" ng-if="freeshiping == 'Free Shipping'" style="cursor:pointer">x</span> 
                                    {{location}}
                                    <span ng-click="eventclose($event)" datalabel="location" ng-if="location == 'Only US'" style="cursor:pointer">x</span>
                                </p>
                            </li>
                            
                             <li class="filterclass" ng-model="resultcomplabellength" ng-hide="resultcomplabellength == 0"><p>
                                <span class="bold" ng-model="resultcomplabellength" ng-hide="resultcomplabellength == 0">Sellers :</span>
                                <span ng-repeat="(catkey,catvalue) in resultcomplabel" comp-value="{{catvalue.key}}">{{catvalue.key}}<span ng-click="eventclose($event)" datalabel="seller" datavalue="{{catvalue.key_id}}" style="cursor:pointer">&nbsp;x&nbsp;&nbsp;</span></span></p>
                            </li>

                            <li class="filterclass" ng-hide="resultsortorder == NULL"><p><span class="bold" ng-hide="resultsortorder == NULL">Sort Order :</span>{{resultsortorder}}
                                <span ng-click="eventclose($event)" datalabel="resultorder" datavalue="{{resultsortorder}}" style="cursor:pointer">x</span></p>
                            </li> 
                        </div>
                    </fieldset>  
                </div>
                    <div id="filtr_msg" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #1f6f1d;color: white; border-radius:5px;padding: 8px 0px 8px 0px;">Applied Filters Saved Successfully</p></center>
                    </div>
                        <!-- <hr size="2"> -->
                         <div class="conf_button_right" align="center">
                            <span>
                                <button type="submit" id="crwlbtn" style="padding: 7px 24px 5px 6px;" ng-click="crawlersearch()" class="button_search">API Search</button>
                            </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <span>
                                <button type="submit" id="testbtn" style="padding: 7px 24px 5px 6px;" ng-click="testsearch('')" class="button_search">eBay Search</button>
                            </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                           <span>
                               <button type="submit" style="padding: 7px 24px 5px 6px;"  ng-click="submititems()" class="button_add">Save Search</button>
                            </span>
                            <!-- <a href="">Clear options&nbsp;</a> -->
                        </div>

                </div>
            </div>
        </div>
    </div>
     <!--edit popup end-->
  <!--crawler popup-->
     <div id="Pnopopup3" class="modal">
    <div class="pop_map_my_part" style="margin:5% auto;width:80%;">          
        <div class="pop_container">
            <div class="heading_pop_main">
                <h4>API Search Results </h4>

                <h4>{{currentitemid}}</h4>
                <span class="closecrawler" id="close2" ng-click="exclude_check()">&times;</span>
                <span></span>
                <span><img src="{{pictureurl}}" style="max-width: 35px; border: 1px solid #c7e9ff; vertical-align: middle;"></span>
            </div>
            <div class ="pop_pad_all_add_new" style="color: #666;">
               <!--  <span style="font-weight: bold;">Image : <img src="{{pictureurl}}" style="max-width: 60px;"></span>
                <span style="color:#f44336">{{currentkeyword}}</span> -->
                <!--  <table cellspacing="0" border="1" class="table api_search_result" style="width:30%;">
                    <thead>
                        <th>My ItemID</th>
                        <th>Images</th>
                    </thead>
                    <tr>
                        <td width="50%">{{currentitemid}}</td>
                        <td style="text-align: center;"><img src="{{pictureurl}}" style="max-width: 60px;"></td>
                    </tr>
                </table> -->
                 <div id="tabs">
                <ul>
                    <li class="tabonebutton"><a href="#tabs-1">API Search</a></li>
                    <li class="tabtwobutton"><a href="#tabs-2">Show Exclude</a></li>
                </ul>
                 <div id="tabs-1">
                <div class="plan_list_view">
               <table cellspacing="0" border="1" class="table price_smart_grid_new">
                    <thead>
                        <tr>
                            <th>sel</th>
                            <th>Image</th>
                            <th>Seller</th>
                            <th>Title</th>
                            <th class="curser_pt" ng-click="sortBy2('CurrentPrice')">Current Price</th>
                            <th>Shipping Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat ="data in crawlurl | orderBy:propertyName2:reverse2">
                            <td><input type="checkbox" name="api_checkbox" element="{{data.itemId}}" id="{{data.itemId}}" ng-checked="data.excludeflag == 1" ng-model="api_checkbox[data.itemId]" ng-click="Api_checked($event)"></td>
                           <td width="50"><img src="{{data.galleryURL}}" style="max-width: 60px;"></td>
                            <td>{{data.sellerUserName}}</td>
                            <td><a ng-click="redirectToLink($event)" element="{{data.itemId}}" style="cursor: pointer;">{{data.title}}</a></td>
                            <td>{{data.CurrentPrice}}</td>
                            <td>{{data.ShippingPrice}}</td>
                        </tr>
                    </tbody>
                </table> 
                 <div id="exc_msg" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #1f6f1d;color: white; border-radius:5px;width: 100%;">Saved Successfully</p></center>
                    </div>
                <div align="center" style="margin-top:10px;">
                    <span><button type="submit" id="" style="padding: 7px 24px 5px 6px;"  class="button_search" ng-click="Api_save_exclude()">Save/Exclude</button></span>
                </div>
            </div>  
            </div>
             <div id="tabs-2" ">
                <div class="plan_list_view">
               <table cellspacing="0" border="1" class="table price_smart_grid_new api_search_table">
                    <thead>
                        <tr>
                            <th>sel</th>
                            <th>Image</th>
                            <th>Seller</th>
                            <th>Title</th>
                            <th class="curser_pt" ng-click="sortBy3('CurrentPrice')">Current Price</th>
                            <th>Shipping Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat ="data in crawlurl_exclude | orderBy:propertyName3:reverse3">
                             <td><input type="checkbox" name="api_checkbox_tab2" element="{{data.itemId}}" id="{{data.itemId}}" ng-checked="data.excludeflag == 1" ng-model="api_checkbox_tab2[data.itemId]" ng-click="Api_checked_tab2($event)"></td>
                            <td width="50"><img src="{{data.galleryURL}}" style="max-width: 60px;"></td>
                            <td>{{data.sellerUserName}}</td>
                            <td><a ng-click="redirectToLink($event)" element="{{data.itemId}}" style="cursor: pointer;">{{data.title}}</a></td>
                            <td>{{data.CurrentPrice}}</td>
                            <td>{{data.ShippingPrice}}</td>
                        </tr>
                    </tbody>
                </table>
                 <div id="exc_msg_tab2" style="display: none;padding: 5px 0 0px 0;">
                        <center><p style="background: #1f6f1d;color: white; border-radius:5px;width: 100%;">Saved Successfully</p></center>
                    </div>
                <div align="center" style="margin-top:10px;">
                    <span><button type="submit" id="" style="padding: 7px 24px 5px 6px;"  class="button_add" ng-click="Api_save_exclude_tab2()">Save/UnExclude</button></span>
                </div> 
            </div>  
            </div>           
        </div>
    </div>
</div>
</div>
</div>
 <!--crawler popup end-->

    

    <script>
        function Recommendation(){
           var url = "<?php echo base_url();?>Pricesmart/PriceRecomProcess";
           window.open(url, '_blank','width=1200,height=600');
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>

    <script>
        $(window).click(function(e) {
                $('.error_msg_all').css('display','none');
            });
    </script>
     <script>

          function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };
        var exportdata=[];
        var exportdata_exc = [];
        var exchangedata = [];
        exportdata.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
        exchangedata.push ({'Inventory Number':'Inventory Number','Buy It Now Price':'Buy It Now Price'});

        var sortingOrder = 'MyPartNo';
        var app = angular.module('psmartApp', ['ngSanitize', 'ngCsv']);



            app.controller('psmartCntrl', function($scope, $http, $timeout,$window,$filter,$attrs) {
                         
                $scope.isLoading=true;
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 20;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.parts = '';
                $scope.currentPage = 0;
                $scope.selectlist = [];
                $scope.finaldata = [];
                $scope.final_data = [];
                $scope.final_data_tab2 = [];
                $scope.api_checkbox_tab2   = [];
                $scope.api_checkbox    = [];
                $scope.allcolors ='';
                $scope.totalitems ='';
                

                   $http.get("<?php echo base_url();?>index.php/configIBR/suggest_categoryname")
                       .success(function (response) {
                        //$scope.isLoading =false;
                           $scope.suggestcat = response;
                          })
                $http.get("disp")
                .then(function (response) 
                {
                    $scope.isLoading=false;
                    $scope.allcolors = response.data;
                    $scope.populate( $scope.allcolors);
                });

                $scope.populate = function(data){
                    $scope.parts = data;
                    $scope.totalitems = $scope.parts.length;

                    $scope.parts.forEach(function (part) {
                        part.flag='N';
                    });

                    //console.log($scope.parts);

                    var exportdata=[];
                    var exchangedata = [];
                    exportdata.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                   exchangedata.push ({'Inventory Number':'Inventory Number','Buy It Now Price':'Buy It Now Price'});
                    $scope.parts.forEach(function (part) {
                        exportdata.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                        exchangedata.push ({'Inventory Number': part.SKU,'Buy It Now Price': part.Recomend_Price});
                    });
                    //console.log(exportdata);
                    //console.log(exchangedata);
                   
                   $scope.ForCSVExport = exportdata;
                   $scope.ForCSVExport4 = exchangedata;
                   
                    //pagination part start 
                    $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                    };

                    var searchMatch = function (haystack, needle) {
                        if (!needle) {
                            return true;
                        }
                        if(haystack !== null){                                       
                            return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                        }
                    };

                    // init the filtered items
                    $scope.partsearch = function () {

                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                             /*   for(var attr in item) {
                                    if (searchMatch(item[attr], $scope.partquery))
                                        return true;
                                }*/
                                 for(var attr in item) {
                                           if(attr == "MyPartNo") {
                                            if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                                return false;
                        });

                     

                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                       
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };
                            
                    // calculate page in place
                    $scope.groupToPages = function () {
                        $scope.pagedItems = [];

                        for (var i = 0; i < $scope.filteredItems.length; i++) {
                            //console.log(Math.floor(i / $scope.itemsPerPage));
                            if (i % $scope.itemsPerPage === 0) {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                            } 
                            else {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);   
                            }
                        }
                    };

                    $scope.range = function (start, end) {
                        var ret = [];
                        if (!end) {
                            end = start;
                            start = 0;
                        }
                        for (var i = start; i < end; i++) {
                            ret.push(i);
                        }
                        $scope.pagenos = ret;
                        return ret;
                    };
                            
                    $scope.prevPage = function () {
                        if ($scope.currentPage > 0) {
                            $scope.currentPage--;
                        }
                    };
                            
                    $scope.nextPage = function () {
                        if ($scope.currentPage < $scope.pagedItems.length - 1) {
                            $scope.currentPage++;
                        }
                    };
                        
                    $scope.setPage = function () {
                        $scope.currentPage = this.n;  
                    };

                    $scope.temp = function () {
                        angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                            if (value.ItemId == dummy[value.ItemId]){
                                document.getElementById(value.ItemId).checked = true;
                            } 
                        });
                    };
                    $scope.$watch('currentPage', function(pno,oldno){
                    if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                        var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                        $scope.range(start, $scope.pagedItems.length);
                      }
                    });

                    // functions have been describe process the data for display
                    $scope.partsearch();

                    // change sorting order
                    $scope.sort_by = function(newSortingOrder) {
                        if ($scope.sortingOrder == newSortingOrder)
                            $scope.reverse = !$scope.reverse;
                            $scope.sortingOrder = newSortingOrder;
                            // icon setup
                            $('th i').each(function(){
                                // icon reset
                                $(this).removeClass().addClass('icon-sort');
                            });
                            if ($scope.reverse)
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                            else
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                    };
                            
                 
                 
                    $scope.itemsearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            /*for(var attr in item) {
                                if (searchMatch(item[attr], $scope.itemquery))
                                    return true;
                            }*/
                            for(var attr in item) {
                                           if(attr == "ItemId") {
                                            if (searchMatch(item[attr], $scope.itemquery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.skusearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                           /* for(var attr in item) {
                                if (searchMatch(item[attr], $scope.skuquery))
                                    return true;
                            }*/
                             for(var attr in item) {
                                           if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.titlesearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            /*for(var attr in item) {
                                if (searchMatch(item[attr], $scope.titlequery))
                                    return true;
                            }*/
                             for(var attr in item) {
                                           if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                            return false;
                        });
                         if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.catsearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            /*for(var attr in item) {
                                if (searchMatch(item[attr], $scope.titlequery))
                                    return true;
                            }*/
                             for(var attr in item) {
                                           if(attr == "Category") {
                                            if (searchMatch(item[attr], $scope.catquery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                            return false;
                        });
                         if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.ruleidsearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            /*for(var attr in item) {
                                if (searchMatch(item[attr], $scope.titlequery))
                                    return true;
                            }*/
                             for(var attr in item) {
                                           if(attr == "RuleId") {
                                            if (searchMatch(item[attr], $scope.ruleidquery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                            return false;
                        });
                         if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.r_pricesearch = function () {
                        $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                            /*for(var attr in item) {
                                if (searchMatch(item[attr], $scope.r_pricequery))
                                    return true;
                            }*/
                            for(var attr in item) {
                                           if(attr == "Recomend_Price") {
                                            if (searchMatch(item[attr], $scope.r_pricequery)  > -1){
                                                return true;
                                            }
                                        }

                                    }
                            return false;
                        });

                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                           $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                        };




                    $scope.change = function(index){
                        $scope.parts.forEach(function (part) {
                                if (part.ItemId==index)
                                {
                                    part.flag='U';
                                }                                  
                            });
                    };

                    $scope.range($scope.pagedItems.length);
                }

                    $scope.save = function(){
                         $('.progrss_confirm').css('display','none');
                        $scope.isLoading = true; //Loader Image
                        var finaldata1=[];

                            $scope.finaldata.forEach(function (part) {
                                if (part.flag=='U')
                                {
                                    finaldata1.push(part)    
                                }
                                                                
                            });
                    
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/PriceSmart/save',
                                data    : { 'New_Content' : finaldata1},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(data) {
                                $scope.isLoading = false;
                                    //alert("Successfully Updated");
                                                $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                     data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                                    // location.reload(true);
                                })
                            .catch(function (err) {
                                var msg =err.status;
                                 //Error Log To DB
                                 var screenname = '<?php echo $screen;?>';
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                               
                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                         data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                 .success(function(response) {
                                                return true;
                                              });
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                });
                    };
                        
                    $scope.$watch('parts', function(newvalue,oldvalue) {
                        //console.log(newvalue);
                        $scope.finaldata = newvalue;
                    }, true);

                    $scope.import = function(){
                        $scope.message = '';
                        $scope.errormessage = '';
                        $("#popupform").css("display", "block");
                        $("#Pnopopup").css("display", "block");
                    };

                    $scope.upload = function(){
                        $scope.message = '';
                        $scope.errormessage = '';
                        document.getElementById('my_file').click();
                    };
                    
                    $scope.save_imp = function(){
                    var type = document.getElementById('ftype').value;
                    //|| type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    if (type == 'application/vnd.ms-excel')
                    {
                        //console.log($scope.parts);
                        if ($scope.parts.length != '')
                        {
                            $scope.isLoading = true; //Loader Image
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/PriceSmart/imp_save',
                                    data    : { 'New_Content' : $scope.parts},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                })
                                .success(function(data) {
                                    $scope.isLoading = false;

                                    //alert("Successfully Updated");
                                            $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.progrss_confirm').css('display','none');
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                                    $("#popupform").css("display", "none");
                                    $("#Pnopopup").css("display", "none");
                                    //location.reload(true);
                                     //$scope.message = "Successfully Updated";
                                })
                                 .catch(function(err) {
                                     var msg =err.status;
                                      //Error Log To DB
                                     var screenname = '<?php echo $screen;?>';
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                                   
                                     $http({                     
                                             method  : 'POST',
                                             url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                             headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                          })
                                     .success(function(response) {
                                                    return true;
                                                  });
                                //failure msg
                                    $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                      data    : msg
                                    })
                                    .success(function(response) {
                                        $('#msg').html(response);
                                        $('.progrss_confirm').css('display','none');
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                    });
                                }); 
                            }
                            else
                            {
                                $scope.message = '';
                                $scope.errormessage = 'Your File is Empty!!';
                            }
                        }
                        else
                        {
                           $scope.message = '';
                           $scope.errormessage = 'You are trying to Upload a Non-CSV File. Try again!!';
                        }
                    };


                    $scope.redirectToLink = function (obj) {
                       var url = "<?php echo base_url();?>/Pricesmart/priceruleedit/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                    };

                    $scope.redirectToLink1 = function (obj) {
                       var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                    };



                    $scope.exportDataExcel = function () {
                       var mystyle = {
                            sheetid: 'Price Smart',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                        };
                            alasql('SELECT * INTO XLS("DIVE_PriceSmart.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    };

        $scope.ShowSelected = function (alertflag) {
             $scope.parts_temp = $scope.parts;
              $scope.pagedItems = "";
              $scope.parts_checked ='';
                 $scope.parts_checked =[]; 
            if($("#green").is(":checked")){
                console.log('green');
                $scope.allcolors.forEach(function (part) {
                    if(part.alert_flag == 'g'){
                    $scope.parts_checked.push ({'MyPartNo' :part.MyPartNo,'ItemId': part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName,'RuleId':part.RuleId,'alert_flag':part.alert_flag,'flag':part.flag,'Comp_High_ItemId':part.Comp_High_ItemId,'Comp_High_ItemId_Name':part.Comp_High_ItemId_Name,'Comp_Low_ItemId':part.Comp_Low_ItemId,'Comp_Low_ItemId_Name':part.Comp_Low_ItemId_Name});
                }
               });   
            }
           if($("#red").is(":checked")){
             console.log('red');
               $scope.allcolors.forEach(function (part) {
                if(part.alert_flag == 'r'){
                    $scope.parts_checked.push ({'MyPartNo' :part.MyPartNo,'ItemId': part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName,'RuleId':part.RuleId,'alert_flag':part.alert_flag,'flag':part.flag,'Comp_High_ItemId':part.Comp_High_ItemId,'Comp_High_ItemId_Name':part.Comp_High_ItemId_Name,'Comp_Low_ItemId':part.Comp_Low_ItemId,'Comp_Low_ItemId_Name':part.Comp_Low_ItemId_Name});
                }
            });  
           }
           if($("#grey").is(":checked")){
            console.log('grey');
               $scope.allcolors.forEach(function (part) {
                if(part.alert_flag == 'Y'){
                    $scope.parts_checked.push ({'MyPartNo' :part.MyPartNo,'ItemId': part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName,'RuleId':part.RuleId,'alert_flag':part.alert_flag,'flag':part.flag,'Comp_High_ItemId':part.Comp_High_ItemId,'Comp_High_ItemId_Name':part.Comp_High_ItemId_Name,'Comp_Low_ItemId':part.Comp_Low_ItemId,'Comp_Low_ItemId_Name':part.Comp_Low_ItemId_Name});    
                }               
                });  
         }
           if($("#orange").is(":checked")){
             console.log('orange');
            $scope.allcolors.forEach(function (part) {
                if(part.alert_flag == 'o'){
                            $scope.parts_checked.push ({'MyPartNo' :part.MyPartNo,'ItemId': part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName,'RuleId':part.RuleId,'alert_flag':part.alert_flag,'flag':part.flag,'Comp_High_ItemId':part.Comp_High_ItemId,'Comp_High_ItemId_Name':part.Comp_High_ItemId_Name,'Comp_Low_ItemId':part.Comp_Low_ItemId,'Comp_Low_ItemId_Name':part.Comp_Low_ItemId_Name});
                 }
                });   
            }
        
                
                $scope.populate($scope.parts_checked);
                console.log($scope.parts_checked); 
                       /* if ($("#green").is(":checked"))
                        {
                            if ($("#red").is(":checked"))
                            {
                                document.getElementById('flaginput').value = 'a';
                                $scope.filteredItems = $filter('filter')($scope.parts);
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                $scope.groupToPages();

                                var exportdata2 = []; 
                                var exchangedata2 = [];

                                exportdata2.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                                exchangedata2.push ({'ItemId':'Item ID','Price':'Price(in $)'});

                                $scope.parts.forEach(function (part) {
                                        exportdata2.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                                        exchangedata2.push ({'ItemId': part.ItemId,'Price': part.Recomend_Price});
                                });
                                $scope.ForCSVExport = exportdata2;
                                $scope.ForCSVExport4 = exchangedata2;
                            }
                            else
                            {
                                document.getElementById('flaginput').value = 'g';
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                     for(var attr in item) {
                                                   if(attr == "alert_flag") {
                                                    if (searchMatch(item[attr], 'g')  > -1){
                                                        return true;
                                                    }
                                                }

                                            }
                                    return false;
                                });
                                 if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                $scope.groupToPages();

                                var exportdata2 = []; 
                                var exchangedata2 = [];

                                exportdata2.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                                exchangedata2.push ({'ItemId':'Item ID','Price':'Price(in $)'});

                                $scope.parts.forEach(function (part) {
                                    if (part.alert_flag == 'g')
                                    {
                                        exportdata2.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                                        exchangedata2.push ({'ItemId': part.ItemId,'Price': part.Recomend_Price});
                                    }
                                });

                                $scope.ForCSVExport = exportdata2;
                                $scope.ForCSVExport4 = exchangedata2;
                            }
                        }
                        else
                        {
                            if ($("#red").is(":checked"))
                            {
                                document.getElementById('flaginput').value = 'r';
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                     for(var attr in item) {
                                                   if(attr == "alert_flag") {
                                                    if (searchMatch(item[attr], 'r')  > -1){
                                                        return true;
                                                    }
                                                }

                                            }
                                    return false;
                                });
                                 if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                $scope.groupToPages();

                                var exportdata2 = [];
                                var exchangedata2 = [];

                                exportdata2.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                                exchangedata2.push ({'ItemId':'Item ID','Price':'Price(in $)'});

                                $scope.parts.forEach(function (part) {
                                    if (part.alert_flag == 'r')
                                    {
                                        exportdata2.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                                        exchangedata2.push ({'ItemId': part.ItemId,'Price': part.Recomend_Price});
                                    }
                                });
                                $scope.ForCSVExport = exportdata2;
                                $scope.ForCSVExport4 = exchangedata2;
                            }
                            else
                            {
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_pricesmart',
                                  data    : 21
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','none');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                var tem= "#"+alertflag;
                                $(tem).prop('checked',true);

                                var exportdata2 = []; 
                                var exchangedata2 = [];

                                exportdata2.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                                exchangedata2.push ({'ItemId':'Item ID','Price':'Price(in $)'});

                                $scope.parts.forEach(function (part) {
                                    if (part.alert_flag == alertflag.substring(0,1))
                                    {
                                        exportdata2.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                                        exchangedata2.push ({'ItemId': part.ItemId,'Price': part.Recomend_Price});
                                    }
                                });
                                $scope.ForCSVExport = exportdata2;
                                $scope.ForCSVExport4 = exchangedata2;
                            }
                        }*/

            


                       /* if($("#grey").is(":checked")){
                            $scope.checkflag_y = false;
                        }else{
                            $scope.checkflag_y = true;
                        }
                        if($("#green").is(":checked")){
                            $scope.checkflag_g = false;
                        }else{
                            $scope.checkflag_g = true;
                        }
                        if($("#red").is(":checked")){
                            $scope.checkflag_r = false;
                        }else{
                            $scope.checkflag_r = true;
                        }*/
                         $('.progrss_confirm').css('display','none');
                    };

                    var dummy = [];
                    $scope.selectfunc = function(obj){
                        $('#selectall').prop('checked',false);
                        $('#ckbCheckAll').prop('checked',false);
                        var id = obj.target.attributes.element.value;
                        if ($scope.selectlist[id])
                        {
                            dummy[id]=id ; 
                        }
                        else
                        {
                           delete dummy[id];
                        }
                        var temp = "";
                        for (var i in dummy) {
                               temp += dummy[i] + ", ";
                        }
                        document.getElementById('my_hidden_input').value = temp;
                        var exportdata2 = []; 
                        var exchangedata2 = [];

                        exportdata2.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                        exchangedata2.push ({'Inventory Number':'Inventory Number','Buy It Now Price':'Buy It Now Price'});

                        $scope.parts.forEach(function (part) {
                            if (dummy[part.ItemId] == part.ItemId)
                            {
                                exportdata2.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                                exchangedata2.push ({'Inventory Number': part.SKU,'Buy It Now Price': part.Recomend_Price});
                            }
                        });
                        $scope.ForCSVExport2 = exportdata2;
                        $scope.ForCSVExport3 = exchangedata2;
                    }

                    var selectallpg = [];
                    $scope.selectallfunc = function(){
                        if($("#selectall").is(":checked"))
                        {
                            angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                                $scope.selectlist[value.ItemId] = true;
                                selectallfun1(value.ItemId);
                            });
                        }
                        else{
                            angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                                $scope.selectlist[value.ItemId] = false;
                                selectallfun2(value.ItemId);
                            });
                        }
                        if (selectallpg[$scope.currentPage] != $scope.currentPage)
                        {
                            selectallpg[$scope.currentPage] = $scope.currentPage;
                        }
                        else
                        {
                             selectallpg[$scope.currentPage] = $scope.currentPage;
                        }
                        var exportdata2 = [];
                        var exchangedata2 = [];

                        exportdata2.push ({'MyPartNo' : 'My Part#','ItemId':'Item ID','SKU' : 'SKU','Title' : 'Title','Category' : 'Category','MyPrice' : 'My Price (eBay) (in $)','Comp_High_Price':'Competitor Highest Price (in $)','Comp_Low_Price':'Competitor Lowest Price (in $)','Cost_Price':'Cost Price (in $)','Freight':'Freight (in $)','Fees':'Fees (in $)','TotalPrice':'Total Price (in $)','Recomend_Price':'Recommended Price (in $)','RuleName':'Rule Name'});
                        exchangedata2.push ({'ItemId':'Item ID','Price':'Price(in $)'});

                        $scope.parts.forEach(function (part) {
                            if (dummy[part.ItemId] == part.ItemId)
                            {
                                exportdata2.push ({'MyPartNo' : "'"+part.MyPartNo,'ItemId': "'"+part.ItemId,'SKU' : part.SKU,'Title' : part.Title,'Category':part.Category,'MyPrice' : part.MyPrice,'Comp_High_Price': part.Comp_High_Price,'Comp_Low_Price':part.Comp_Low_Price,'Cost_Price': part.Cost_Price,'Freight': part.Freight,'Fees': part.Fees,'TotalPrice':part.TotalPrice,'Recomend_Price': part.Recomend_Price,'RuleName':part.RuleName});
                                exchangedata2.push ({'ItemId': part.ItemId,'Price': part.Recomend_Price});
                            }
                        });
                        $scope.ForCSVExport2 = exportdata2;
                        $scope.ForCSVExport3 = exchangedata2;
                    }

                    var selectallfun1 = function(id)
                    {
                        if (dummy[id] == id)
                        {
                            delete dummy[id];
                            dummy[id]=id ;
                        }
                        else
                        {
                            dummy[id]=id ;
                        }
                        var temp = "";
                        for (var i in dummy) {
                            temp += dummy[i] + ", ";
                        }
                        document.getElementById('my_hidden_input').value = temp;
                        /*console.log(temp);*/
                    }

                    var selectallfun2 = function(id)
                    {
                        delete dummy[id];
                        var temp = "";
                        for (var i in dummy) {
                               temp += dummy[i] + ", ";
                        }
                        document.getElementById('my_hidden_input').value = temp;
                        /*console.log(temp);*/
                    };

                    $scope.$watch('parts', function(newvalue,oldvalue) {
                        //console.log(newvalue);
                        $scope.finaldata = newvalue;
                    }, true);



                    $scope.save1 = function(){
                        var finaldata1=[];
                            $scope.finaldata.forEach(function (part) {
                                if (part.flag=='U')
                                {
                                    finaldata1.push(part)    
                                }
                                                                
                            });
                            $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/PriceSmart/save',
                                data    : { 'New_Content' : finaldata1},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .then(function(response) {
                                return true;
                        });
                    };

                     //work in progress
                    $scope.sync_ebay = function(){
                         $('.err_modal').css('display','block');   
                         $('.msgprgstatic').css('display','none');

                       /* $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/ebay_sync_confirm_msg'
                           
                            })
                             .success(function(response) {
                                $('#msg').html(response);
                                //$('.totcount').css('display','none');
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block'); 
                                //$scope.msghide = true;
                                //$scope.msgcountshow = false;                  
                                $scope.isLoading = false;
                         });*/
                    }

                       /* Pusher.logToConsole = true;
                        var pusher = new Pusher('15355bcfc5ff30a1fbca', {
                            cluster: 'ap2',
                          encrypted: true
                        });
                         var ip = "<?php echo $_SESSION['__ci_last_regenerate']; ?>";
                        var channel = pusher.subscribe('mynewchannel'+ip);
                        channel.bind('myevent'+ip, function(data) {
                        $scope.isLoading = false;
                            $('.msgprgstatic').css('display','none');
                            $('.msgprogress').html('');
                            $('.msgprogressitem').html('');
                            $('.msgprogressitem').append(data.itemid+'  of $'+data.startprice);
                            $('.msgprogress').append(data.message+' out of '+data.total+' in Progress');
                        });*/

                $scope.proceedtoebay = function(){
                    $('.progrss_confirm').css('display','block');
                    $('.msgprgstatic').css('display','block');
                    $scope.msghide =true;
                    //$scope.isLoading = true;
                    $scope.save1();
                    var sync_data = [];
                    var dummy = [];
                    $scope.finaldata.forEach(function (part) {
                        if ($scope.selectlist[part.ItemId] == true){
                            sync_data.push({'ItemId': part.ItemId,'Recomend_Price': part.Recomend_Price,'MyPartNo' : part.MyPartNo,'SKU' : part.SKU,'MyPrice' : part.MyPrice});
                        }
                    });
                //custom pusher using readystates
                var xhr = new XMLHttpRequest();  
                xhr.previous_text = '';
                xhr.onerror = function() { console.log('error occured');};               
                xhr.onreadystatechange = function(data){
                 $scope.new_response = '';
                try{
                    if (xhr.readyState == 3){
                         $scope.new_response = '';
                         $scope.new_response = xhr.responseText.substring(xhr.previous_text.length);
                    if (IsJsonString($scope.new_response)) {
                         //console.log('hi');
                        var result = JSON.parse( $scope.new_response);
                        $('.msgprgstatic').css('display','none');
                        $('.msgprogress').html('');
                        $('.msgprogressitem').html('');
                        $('.msgprogressitem').append('<span class="same_as_head">Updating Price of ItemID : </span>' + result.itemid +
                                '<br><span class="same_as_head"> with $</span>'+result.startprice+'<span class="same_as_head">(Old Price $</span>'+result.oldprice+'<span class="same_as_head">)</span>');
                        $('.msgprogress').append('<span class="same_as_head">Processing Item </span>' + result.runcount + '<span class="same_as_head"> of </span>'+result.total);
                        xhr.previous_text = xhr.responseText; 
                    }
                        } 
                    if(xhr.readyState == 4){
                        var request = '['+xhr.responseText.replace(/}{/g, "},{")+']'; 
                        $('.progrss_confirm').css('display','none');
                        var secondresult = JSON.parse(request);
                        var count        = JSON.parse(request).length-1;
                        var resultcout = secondresult[count];   
                          //count show (success msg)
                            $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ebay_sync_success_msg',
                                 data    : {'data':resultcout}
                                })
                                 .success(function(response) {
                                $('#msg').html(response);                                              
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block');                  
                                $scope.isLoading = false;                                             
                                })
                        }
                    }  //try end (catch)
                    catch(e){
                        var msg = xhr.status;
                       //failure msg
                          $http({                     
                           method  : 'POST',
                           url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                           data    : msg
                          })
                          .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block'); 
                                $("#Pnopopup").css("display", "none");                 
                                $scope.isLoading = false;
                          });
                      }  
                }

                xhr.open("POST", '<?php echo base_url();?>index.php/Pricesmart/sync_with_ebay', true);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.send(JSON.stringify({'sync_data' : sync_data}));
                 //custom pusher end...       
            }



                    $scope.SalesTrend = function(e){
                    $scope.isLoading = true;
                    var itemid = e.target.getAttribute('data-id');       
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/Pricesmart/SalesTrend',
                        data    : { 'itemid' : itemid},
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .then(function(response) {
                        $scope.isLoading = false;
                        $scope.saledata_inner = response.data.dashboard_drill_data;
                        $scope.week = response.data.week;
                        document.getElementById('Pnopopupsalestrends').style.display = "block";
                        /*console.log(response.data.dashboard_drill_data);*/
                    });
                };

                $scope.salestrendsshowvalues = function(){
                            if(event.target.checked == true){
                                $scope.salestrendsvalues = true;
                            }
                            else{
                                $scope.salestrendsvalues = false;
                            }
                };   

                $scope.allselect = function(){
                    if ($("#ckbCheckAll").is(":checked"))
                    {
                        document.getElementById('exportsel').style.display = "none";
                        document.getElementById('selected').style.display = "none";
                        var length = $scope.pagedItems.length;
                        var exchangedata2 = [];
                        $scope.parts.forEach(function (part) {
                            $scope.selectlist[part.ItemId] = true;
                        });
                        for (var i=0; i<=length ; i++)
                        {
                            $scope.selectall[i] = false;
                        }
                        $scope.ForCSVExport2 = [];
                        $scope.ForCSVExport3 = [];
                        document.getElementById('my_hidden_input').value = '';
                    }
                    else
                    {
                        document.getElementById('exportsel').style.display = "block";
                        document.getElementById('selected').style.display = "block";
                        var length = $scope.pagedItems.length;
                        $scope.parts.forEach(function (part) {
                            $scope.selectlist[part.ItemId] = false;
                        });
                        for (var i=0; i<=length ; i++)
                        {
                            $scope.selectall[i] = false;
                        }
                        $scope.ForCSVExport2 = [];
                        $scope.ForCSVExport3 = [];
                        document.getElementById('my_hidden_input').value = '';
                    }
                }
// config auto rawlist start
        $scope.keywordprevious = '';
        $scope.keywordspreviousand = '';
        $scope.keywordspreviousmore = '';
        $scope.set_yes = function(){
             //$scope.isLoading = true;
             $scope.keywordsor = $scope.s_or;
             $scope.keywordsand = $scope.s_and;
             $scope.keywordsmore = $scope.s_more;
             if($scope.keywordprevious != ''){
                $scope.keywordsor = $scope.keywordprevious;
                $scope.keywordsand = $scope.keywordspreviousand;
                $scope.keywordsmore = $scope.keywordspreviousmore;
             };
              if($scope.keywordsor == '' && $scope.keywordsand == '' && $scope.keywordsmore == '' ){
                 $('#edit_rest_mp_ip').show(); 
                  $('#mp_ip_opn').hide(); 
              }
             $('#mp_ip_opn').hide();  
        }
         $scope.set_none = function(){
            // $scope.isLoading = true;
            $scope.keywordprevious = $scope.s_or;
            $scope.keywordspreviousand = $scope.s_and;
            $scope.keywordspreviousmore = $scope.s_more;
             $scope.keywordsor = '';
             $scope.keywordsand = '';
             $scope.keywordsmore = '';
             $('#mp_ip_opn').hide();
        }
         //reset button
         $scope.reset = function(){
             $scope.isLoading = true;
             $scope.s_or = $scope.keywordsor;
             $scope.s_and = $scope.keywordsand;
             $scope.s_more = $scope.keywordsmore;
             $('#filtr_msg').hide(); 
             $http({                     
                     method  : 'POST',
                     url     : '<?php echo base_url();?>index.php/configIBR/editpopup_api_search',
                     data    : {'itemid' : $scope.currentitemid},
                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                    $scope.isLoading = false;
                    $scope.keywrd_mpn =response['sel_result'][0]['MPN'] == null ? '' : response['sel_result'][0]['MPN'];
                    $scope.keywrd_ipn =response['sel_result'][0]['IPN'] == null ? '' : response['sel_result'][0]['IPN'];
                    $scope.keywrd_opn =response['sel_result'][0]['OPN'] == null ? '' : response['sel_result'][0]['OPN'];
                    $scope.keywrd_def =response['def_keyword_res'][0]['Default_keywords'] == null ? '' : response['def_keyword_res'][0]['Default_keywords'];
                    if($scope.keywrd_mpn == '' && $scope.keywrd_ipn == '' &&  $scope.keywrd_opn == ''){
                        $('#mp_ip_opn').show();
                        $('#edit_mp_ip_opn').hide();
                    }
                    $scope.keywrd_mpn_opn_ipn =$scope.keywrd_mpn+' '+$scope.keywrd_ipn.trim()+' '+$scope.keywrd_opn+' '+$scope.keywrd_def;
                    $scope.keywordsor = $scope.keywrd_mpn_opn_ipn.replace(/,/g,"");
                    $scope.keywordsand = '';
                    //$scope.keywordsmore = '';
                     $scope.keywordsmore =response['def_keyword_res'][0]['Def_Exclude_keywords'] == null ? '' : response['def_keyword_res'][0]['Def_Exclude_keywords'];
                });
         }

          //get exclude  count
         $scope.exclude_check = function(){
            $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/configIBR/crawler_exclude_test',
                         data    : {'myitemid':$scope.currentitemid},
                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                        //console.log(response);
                        $scope.per_page_data = response.pagedata;
                        $scope.per_page_num = response.pagenumber;

                        if($scope.per_page_data >= 1){
                        $scope.per_page_data = $scope.per_page_data;
                        $scope.per_page_num = $scope.per_page_num;
                        }
                        else{
                            $scope.per_page_data =10;
                            $scope.per_page_num = $scope.per_page_num;
                        }
                        console.log($scope.per_page_data);
                    })
         }
    $scope.editpopup = function(evt) {
        // $scope.reset_disable =false;
             $('#edit_mp_ip_opn').hide();
            $scope.isLoading = true;
            //get exclude count
                $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/configIBR/crawler_exclude_test',
                             data    : {'myitemid':evt.target.getAttribute('dataitemid')},
                             headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                        })
                .success(function(response) {
                    //console.log(response);
                    $scope.per_page_data = response.pagedata;
                    $scope.per_page_num = response.pagenumber;

                    if($scope.per_page_data >= 1){
                    $scope.per_page_data = $scope.per_page_data;
                    $scope.per_page_num = $scope.per_page_num;
                    }
                    else{
                        $scope.per_page_data =10;
                        $scope.per_page_num = $scope.per_page_num;
                    }
                    console.log($scope.per_page_data);
                })
                //exclude count end
         $http({                     
             method  : 'POST',
             url     : '<?php echo base_url();?>index.php/Pricesmart/psmrt_edit_result',
             data    : {'itemid' : evt.target.getAttribute('dataitemid')},
             headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
            })
            .success(function(response) {
                $scope.isLoading = false;
               // console.log(response[0]['Keywords_AND']);
                 $scope.def_cat_id = response[0]['CategoryID'];
                 $scope.pictureurl =response[0]['pictureurl']
                 $scope.keywordsor  = response[0]['Keywords_OR'];
                 $scope.comp_inc = '';
                    $scope.comp_exc = '';
                    if(response[0]['Percent'] == '' || response[0]['Percent']  == 0){
                        $scope.pricevalue = 0;
                        $scope.percent  ='';
                       }
                       else if(response[0]['Percent']  != ''){
                        $scope.percent  = response[0]['Percent'];
                        $scope.pricevalue = 1;
                       }
                       if(response[0]['Priceform_from'] == 0){
                         $scope.minus  ='';
                       }
                        else{
                          $scope.minus  = response[0]['Priceform_from'];
                        }
                        if(response[0]['Priceform_to'] == 0){
                         $scope.plus  ='';
                        }
                        else{
                             $scope.plus  = response[0]['Priceform_to'];
                        }
                        //keywords editmode mpn/opn/ipn or keywordAND
                    
                       $("#Pnopopup2").css("display", "block");
                       var conditionvalues = {"3":"New","4":"Used","10":"Not Specified"}; 
                       var shippingval = {"1":"Free Shipping"};
                       var locationval = {"1":"Only US"};  
                       var sortorderval =  {"12":"Best Match","1":"Time: ending soonest","10":"Time: newly listed",
                                            "15":"Price + Shipping: lowest first","16":"Price + Shipping: highest first","7":"Distance: nearest first"};
                       mainresultvalue = [];
                       $scope.maincompetitorvalue = [];
                       $scope.currentitemid = response[0]['itemid'];
                       $scope.currentsku    = response[0]['sku'];
                       $scope.currentcatid  = response[0]['category'];
                       $scope.currenttitle  = response[0]['title'];
                       $scope.currentprice  = response[0]['price'];
                       $scope.keywordsand  = response[0]['Keywords_AND'];
                       $scope.keywordsmore  = response[0]['Exclude_keywords'];
                       $scope.resultcondition  = conditionvalues[response[0]['Conditions']];
                       $scope.condition  = response[0]['Conditions'];
                       if($scope.condition == 0){
                         $scope.condition = '0';
                        }
                      if($scope.condition == 10){
                        $scope.condition = '10';
                      }
                      if($scope.condition == 3){
                        $scope.condition = '3';
                      }
                      if($scope.condition == 4){
                        $scope.condition = '4';
                      }
                       $scope.freeshiping  = shippingval[response[0]['Shipping']];
                       $scope.location  = locationval[response[0]['Location']];
                       //$scope.sortorder  = evt.target.getAttribute('dataSort_order');
                       //$scope.resultsortorder =sortorderval[evt.target.getAttribute('dataSort_order')];
                        $scope.resultsortorder =sortorderval[response[0]['Sort_order']] ? sortorderval[response[0]['Sort_order']] : sortorderval[15];
                       $scope.sortorder =response[0]['Sort_order'] == 0 ? 15 : response[0]['Sort_order'];
                       $scope.resultsortordervalue =$scope.sortorder; 
                       //console.log($scope.sortorder) ;
                       $scope.cat_include  = response[0]['Category_Include'].split(',');
                       $scope.cat_exclude  = response[0]['Category_Exclude'].split(',');
                       $scope.comp_include  = response[0]['Sellers_Include'].split(',');
                       $scope.comp_exclude  = response[0]['Sellers_Exclude'].split(',');

                       //category & competitor label pushing in edit mode
                        if($scope.cat_include != '' || $scope.cat_exclude != ''){
                            $scope.categorymain = [];
                             for(catid in $scope.suggestcat){
                                $scope.categorymain[$scope.suggestcat[catid].CategoryID] = $scope.suggestcat[catid].category;
                           }
                                                 
                           for(catinid in $scope.cat_include){
                                    mainresultvalue[$scope.cat_include[catinid]] = $scope.categorymain[$scope.cat_include[catinid]];
                           }
                           for(catinid in $scope.cat_exclude){
                                    mainresultvalue[$scope.cat_exclude[catinid]] = '!='+$scope.categorymain[$scope.cat_exclude[catinid]];
                           } 
                       }

                     
                       //*****************************
                      if($scope.comp_include != '' || $scope.comp_exclude != '' ){
                               for(catinid in $scope.comp_include){
                                    if($scope.comp_include[catinid] != ''){
                                        $scope.maincompetitorvalue[$scope.comp_include[catinid]] = $scope.comp_include[catinid];
                                    }
                               }
                                for(catinid in $scope.comp_exclude){
                                    if($scope.comp_exclude[catinid] != ''){
                                        $scope.maincompetitorvalue[$scope.comp_exclude[catinid]] = '!='+$scope.comp_exclude[catinid];
                                    }
                               }
                       }

                        $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});
                        var compresponse = [];
                        for(key in $scope.maincompetitorvalue){
                            compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                        }
                        $scope.resultcomplabel = compresponse;  
                        if(compresponse.length == 0){
                            $scope.resultcomplabellength = 0;
                        }  
            });          
        }

         $scope.redirectToLink = function (obj) {
                        var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                        $window.open(url, '_blank','width=1200,height=600"');
                    }; 
            $scope.crawldata = 'data';
            $scope.crawlersearch = function(crawlerbutton){ 
                 $scope.currentguid = '';
                $http({                     
                     method  : 'POST',
                     url     : '<?php echo base_url();?>index.php/configIBR/getGUID',
                     data    : '',
                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                })
                .success(function(response) {
                    $scope.currentguid = response.replace('\r\n','');
                //var sendgui = Math.floor(new Date().valueOf() / 1000);
                //var sendgui = $('#api_hidden_id').val();
                var sendgui = $scope.currentguid;
                $scope.keyword_crawler = '';               
                $scope.key_exclude_crawler = '';
                $scope.categryid_crawler ='';
                $scope.highprice_crawler ='';
                $scope.condition_crawler = '';
                $scope.freeshipping_crawler ='';
                $scope.location_crawler ='';
                $scope.competitor_crawler ='';
                $scope.sortorder_crawler = '';
                $scope.Sellers_Include_exclude ='';
                $scope.excluderesultebay ='';
                $scope.includeresultebay ='';
                var includeebay = '';
                var excludeebay = '';
                var my_item_id = $scope.currentitemid;
                $k =0;
                $s =0;


                   //console.log($scope.keywordsand.replace(' ','%20'));
              //keywords condition
                 /*if($scope.keywordsand !='' || $scope.keywordsor !='' || $scope.keywordsmore !='' ){
                    $scope.keyword_crawler =$scope.keywordsor.replace(' ','%20')+' '+$scope.keywordsand; 
                }*/
                 // or,exclude | and,exclude | or | and 
                if($scope.keywordsor !='' && $scope.keywordsmore !=''){
                    $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_or_more = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawler = '('+$scope.keyword_or_more+')'+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');

                }
                 else if($scope.keywordsand !='' && $scope.keywordsmore !=''){
                     $scope.keyword_more = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' -');
                    $scope.keyword_and_more = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = $scope.keyword_and_more+'%20-'+$scope.keyword_more.replace(/\s/g,'%20');
                }
                else if($scope.keywordsor !=''){
                    $scope.keyword_crawl_or = $scope.keywordsor.replace(/\s/g,',%20');
                     $scope.keyword_crawler = '('+$scope.keyword_crawl_or+')';
                }
                 else if($scope.keywordsand !=''){
                    $scope.keyword_crawl_and = $scope.keywordsand.replace(/\s/g,'+');
                     $scope.keyword_crawler = $scope.keyword_crawl_and;
                }
                
                //both price
                 if($scope.minus !=''&& $scope.plus !=''){
                    
                    $scope.highprice_crawler = '&itemFilter('+$k+').name=MinPrice&itemFilter('+$k+').value='+$scope.minus+'&itemFilter('+$k+').paramName=Currency&itemFilter('+$k+').paramValue=USD&itemFilter('+(++$k)+').name=MaxPrice&itemFilter('+$k+').value='+$scope.plus+'&itemFilter('+$k+').paramName=Currency&itemFilter('+$k+').paramValue=USD';
                    $s++;
                }
                 //low price
                if($scope.minus !=''&& $scope.plus ==''){
                    $scope.highprice_crawler = '&itemFilter.name=MinPrice&itemFilter.value='+$scope.minus+'&itemFilter.paramName=Currency&itemFilter.paramValue=USD';
                }


                //high price
                if($scope.plus !='' && $scope.minus ==''){
                   $scope.highprice_crawler = '&itemFilter.name=MaxPrice&itemFilter.value='+$scope.plus+'&itemFilter.paramName=Currency&itemFilter.paramValue=USD';
                }

                 //location
                if($scope.location == undefined){
                    $scope.location_crawler = '';
                }
                else if($scope.location != ''){
                    //$scope.location_crawler = '&itemFilter(1).paramName=Currency&itemFilter(1).paramValue=USD';
                    //$scope.location_crawler = '&itemFilter.name=location&itemFilter.value=US';
                    $scope.location_crawler = '&LocatedIn=US';
                }
               

                 //free shipping
                if($scope.freeshiping == undefined ){
                    $scope.freeshipping_crawler ='' ;
                }
                else if($scope.freeshiping !=''){
                    $scope.freeshipping_crawler = '&itemFilter.name=FreeShippingOnly&itemFilter.value=true';
                }

                 //sort order
                if($scope.resultsortordervalue !=''){
                    var sort_ord_val =  {"12":"BestMatch","1":"EndTimeSoonest","10":"StartTimeNewest",
                                            "15":"PricePlusShippingLowest","16":"PricePlusShippingHighest","7":"DistanceNearest"};
                      var sotorder  = sort_ord_val[$scope.resultsortordervalue];                
                      //console.log(sotorder);
                     $scope.sortorder_crawler = '&sortOrder='+sotorder;
                }

                 //condition
                if($scope.condition == 0){
                    $scope.condition_crawler = '';
                }
                else if($scope.condition !=''){
                    $scope.condition_crawler ="";
                    if($s>0){
                           if($scope.condition == 3) $scope.condition_crawler = '&itemFilter('+(++$k)+').name=Condition&itemFilter('+($k)+').value='+1000;
                              else if($scope.condition == 4) $scope.condition_crawler = '&itemFilter('+(++$k)+').name=Condition&itemFilter('+($k)+').value='+3000;
                                   else if($scope.condition == 10) $scope.condition_crawler = '&itemFilter('+(++$k)+').name=Condition&itemFilter('+($k)+').value='+'';
                    $s++;
                    }
                    else{
                         if($scope.condition == 3) $scope.condition_crawler = '&itemFilter('+$k+').name=Condition&itemFilter('+$k+').value='+1000;
                             else if($scope.condition == 4) $scope.condition_crawler = '&itemFilter('+$k+').name=Condition&itemFilter('+$k+').value='+3000;
                                  else if($scope.condition == 10) $scope.condition_crawler = '&itemFilter('+$k+').name=Condition&itemFilter('+$k+').value='+'';
                    $s++;
                    }
                    
                }

                  //category include only   
                if($scope.resultcatlabel !=''){
                    var resultcategoryinclude = [];
                    var resultcategoryexclude = [];
                    var resultcatforebay = [];
                    for(val in $scope.resultcatlabel){
                        var sortvalue = $scope.resultcatlabel[val].name;
                        if(sortvalue[0] == '!' && sortvalue[1] == '='){
                            resultcategoryexclude.push(val);     
                        }
                        else{
                            resultcategoryinclude.push(val);
                        }
                    }
                   
                    //include category
                    if(resultcategoryinclude!=''){
                        if($s>0){
                        var include_mainurl  = '&itemFilter('+(++$k)+').name=Category%20';}
                        else{
                        var include_mainurl  = '&itemFilter('+$k+').name=Category%20'; }

                        //var include_mainurl = '&itemFilter(0).name=Category%20';
                        $j = 0;
                        for(key in resultcategoryinclude){
                            include_mainurl += '&itemFilter('+$k+').value('+$j+')='+resultcategoryinclude[key];
                            $j++;
                        }
                        $s++;
                     }
                    //exclude catergory
                    if(resultcategoryexclude!=''){
                        
                        if($s>0) {
                        var exclude_mainurl = '&itemFilter('+(++$k)+').name=ExcludeCategory%20';}
                        else {
                        var exclude_mainurl = '&itemFilter('+$k+').name=ExcludeCategory%20';}

                            $i = 0;
                            for(key in resultcategoryexclude){
                                exclude_mainurl += '&itemFilter('+$k+').value('+$i+')='+resultcategoryexclude[key];
                                $i++;
                            }
                            $s++;
                     }
                    if(resultcategoryexclude.length == 0){exclude_mainurl = '';}
                    if(resultcategoryinclude.length == 0){include_mainurl = '';}
                }

                 //competitor include and exclude
                 if($scope.resultcomplabel !=''){
                    var includeebay = [];
                    var excludeebay = [];
                    for(val in $scope.resultcomplabel){
                        var keysplitvalue = $scope.resultcomplabel[val].key;
                        if(keysplitvalue[0] == '!' && keysplitvalue[1] == '='){
                            excludeebay.push($scope.resultcomplabel[val].key_id);
                        }
                        else{
                            includeebay.push($scope.resultcomplabel[val].key_id);   
                        }
                    }
                    //var exclude_comp_mainurl = '&itemFilter(0).name=ExcludeSeller';
                    //var include_comp_mainurl = '&itemFilter(0).name=Seller%20';

                   
                    //include seller        
                    if(includeebay!= ''){
                             if($s>0){
                                var include_comp_mainurl = '&itemFilter('+(++$k)+').name=Seller%20';}
                             else{
                                var include_comp_mainurl = '&itemFilter('+$k+').name=Seller%20';}
                             $j = 0;
                                for(key in includeebay){
                                    include_comp_mainurl += '&itemFilter('+$k+').value('+$j+')='+includeebay[key];
                                    $j++;
                                }
                                $s++;
                        } 
                  //exclude seller
                    if(excludeebay!= ''){
                        if($s>0) {
                        var exclude_comp_mainurl = '&itemFilter('+(++$k)+').name=ExcludeSeller';}
                        else{
                            var exclude_comp_mainurl = '&itemFilter('+$k+').name=ExcludeSeller';}
                        $i = 0;
                            for(key in excludeebay){
                                exclude_comp_mainurl += '&itemFilter('+$k+').value('+$i+')='+excludeebay[key];
                                $i++;
                            }
                            $s++;
                     }
                }
                if(exclude_comp_mainurl == '' || exclude_comp_mainurl == undefined || excludeebay.length == 0){exclude_comp_mainurl = '';}
                if(include_comp_mainurl == '' || include_comp_mainurl == undefined || includeebay.length == 0){include_comp_mainurl = '';}

 
               $scope.crawlerurl = 'http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=APAEngin-AutoInve-PRD-5bffbb2e5-df0267de&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&outputSelector(0)=SellerInfo&outputSelector(1)=StoreInfo&paginationInput.pageNumber='+$scope.per_page_num+'&paginationInput.entriesPerPage='+$scope.per_page_data+'&keywords='+$scope.keyword_crawler+$scope.highprice_crawler+$scope.condition_crawler+$scope.freeshipping_crawler+include_mainurl+exclude_mainurl+exclude_comp_mainurl+include_comp_mainurl+$scope.sortorder_crawler+$scope.location_crawler;
               $scope.crawlerurl = $scope.crawlerurl.replace(/  | /g,'');
                   console.log($scope.crawlerurl); 

               //if($scope.crawldata == 'data'){
                 if(crawlerbutton == undefined){
                    $scope.isLoading =true;
                    $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/configIBR/crawler_url_search',
                         data    : {'url' : $scope.crawlerurl,'myitemid':my_item_id,
                                    'guid':sendgui},
                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                    .success(function(response) {
                        //console.log(response);
                            $scope.crawlurl = response.Non_Exclude_res;
                            $scope.crawlurl_exclude = response.Exclude_res;
                            $scope.crawlurl.forEach(function(part){
                                 if(part.excludeflag == 1){
                                    $scope.api_checkbox[part.itemId] = true;
                                }
                             })
                             $scope.crawlurl_exclude.forEach(function(part){
                                 if(part.excludeflag == 1){
                                    $scope.api_checkbox_tab2[part.itemId] = true;
                                }                               
                             })
                            $scope.isLoading = false;
                            $('#Pnopopup3').show();
                            $scope.propertyName2 = 'CurrentPrice';
                            $scope.reverse2 = true;
                            $scope.propertyName3 = 'CurrentPrice';
                            $scope.reverse3 = true;

                          $scope.sortBy2 = function(propertyName2) {
                            $scope.reverse2 = ($scope.propertyName2 === propertyName2) ? !$scope.reverse2 : false;
                            $scope.propertyName2 = propertyName2;
                          };
                           $scope.sortBy3 = function(propertyName3) {
                            $scope.reverse3 = ($scope.propertyName3 === propertyName3) ? !$scope.reverse3 : false;
                            $scope.propertyName3 = propertyName3;
                          };
                          });
               }
             });
            }
            $scope.testsearch = function(savebutton){ 
                //$scope.keywordsor = '';
                //$scope.keywordsand = '';
                $scope.keyword_test = '';               
                $scope.AND_OR_id ='';
                $scope.key_exclude_test = '';
                $scope.categryid_test ='';
                $scope.lowprice_test = '';
                $scope.highprice_test ='';
                $scope.condition_test = '';
                $scope.freeshipping_test ='';
                $scope.location_test ='';
                $scope.competitor_test ='';
                $scope.sortorder_test = '';
                $scope.Sellers_Include_exclude ='';
                $scope.excluderesultebay ='';
                $scope.includeresultebay ='';
               

                //keywords condition
                 if($scope.keywordsor !=''){
                    $scope.key_OR = $scope.keywordsor;
                    $scope.keyword_test = $scope.key_OR.replace(/\s/g, '+');
                    $scope.AND_OR_id =2;
                }
                else if($scope.keywordsand !=''){
                    $scope.key_AND = $scope.keywordsand;
                    $scope.keyword_test = $scope.key_AND.replace(/\s/g, '+');
                    $scope.AND_OR_id =1;
                }
                
               /*  if($scope.keywordsand !='' || $scope.keywordsor !=''){
                    $scope.key_AND = $scope.keywordsand+' '+$scope.keywordsor;
                    $scope.keyword_test = $scope.key_AND.replace(/\s/g, '+');
                    $scope.AND_OR_id =4;
                }*/

                //keywords condition exclude
                if($scope.keywordsmore !=''){
                     $scope.keyword_more_test = $scope.keywordsmore.replace(/\s*,\s*|\s+,/g, ' ');
                     $scope.key_exclude_test = $scope.keyword_more_test;
                } 

                //category include only
                if($scope.resultcatlabel !=''){
                    var resultcatforebay = [];
                    for(val in $scope.resultcatlabel){
                        var sortvalue = $scope.resultcatlabel[val].name;
                        if(sortvalue[0] != '!' && sortvalue[1] != '='){
                            resultcatforebay.push(val);   
                        }
                    }
                    $scope.categryid_test = resultcatforebay.toString();
                }

                //low price
                if($scope.minus !=''){
                    $scope.lowprice_test = $scope.minus;
                }

                //high price
                if($scope.plus !=''){
                     $scope.highprice_test = $scope.plus;
                }

                //condition
                if($scope.condition == 0){
                    $scope.condition_test = '';
                }
                else if($scope.condition !=''){
                     $scope.condition_test = $scope.condition;
                }
                
                //console.log($scope.freeshiping);
                //free shipping
                if($scope.freeshiping == undefined ){
                    $scope.freeshipping_test = 0;
                }
                else if($scope.freeshiping !=''){
                    $scope.freeshipping_test = 1;
                }

                //console.log($scope.location);
                //location
                if($scope.location == undefined){
                      $scope.location_test = 0;
                }
                else if($scope.location != ''){
                      $scope.location_test = 1;
                }
                //console.log($scope.location_test);

                //competitor include and exclude
                 if($scope.resultcomplabel !=''){
                    var includeebay = [];
                    var excludeebay = [];
                    for(val in $scope.resultcomplabel){
                        var keysplitvalue = $scope.resultcomplabel[val].key;
                        if(keysplitvalue[0] == '!' && keysplitvalue[1] == '='){
                            excludeebay.push($scope.resultcomplabel[val].key_id);
                        }
                        else{
                            includeebay.push($scope.resultcomplabel[val].key_id);   
                        }
                    }
                    $scope.includeresultebay = includeebay.toString();
                    $scope.excluderesultebay = excludeebay.toString();
                }

                if($scope.includeresultebay != ''){
                    $scope.Sellers_Include_exclude ='&_saslop=1&_sasl='+$scope.includeresultebay; 
                }
                else if($scope.excluderesultebay != ''){
                    $scope.Sellers_Include_exclude ='&_saslop=2&_sasl='+$scope.excluderesultebay; 
                }


                //sort order
                if($scope.resultsortordervalue !=''){
                     $scope.sortorder_test = $scope.resultsortordervalue;
                }

                //url formation
                $scope.url = 'https://www.ebay.com/sch/i.html?_nkw='+$scope.keyword_test+'&_in_kw='+$scope.AND_OR_id+'&_ex_kw='+$scope.key_exclude_test+'&_udlo='+$scope.lowprice_test+'&_udhi='+$scope.highprice_test+'&LH_ItemCondition='+$scope.condition_test+'&&LH_PrefLoc='+$scope.location_test+'&LH_FS='+$scope.freeshipping_test+'&_sop='+$scope.sortorder_test+'&_sacat='+$scope.categryid_test+$scope.Sellers_Include_exclude;
              console.log($scope.url);
                if(savebutton == ''){    
                    $window.open($scope.url, '_blank','width=1200,height=600"');
                }
            }

             //exclude competitor in API Search
               //for tab1
                var api_dummy =[];
                $scope.Api_checked = function(obj){
                    var item = obj.target.attributes.element.value;
                    if($scope.api_checkbox[item]){
                        api_dummy[item] = item;
                    }
                    else{
                        delete api_dummy[item];
                    }
                    var api_temp = "";
                    for(i in api_dummy){
                        api_temp += api_dummy[i] + "," ;
                       
                    }
                    // console.log(api_temp);
                    
                }
                 //for tab2
                var api_dummy_tab2 =[];
                $scope.Api_checked_tab2 = function(obj){
                    var item_tab2 = obj.target.attributes.element.value;
                    if($scope.api_checkbox_tab2[item_tab2]){
                        api_dummy_tab2[item_tab2] = item_tab2;
                    }
                    else{
                        delete api_dummy_tab2[item_tab2];
                    }
                    var api_temp_tab2 = "";
                    for(i in api_dummy_tab2){
                        api_temp_tab2 += api_dummy_tab2[i] + "," ;
                       
                    }
                     //console.log(api_temp_tab2);
                    
                }

                 $scope.$watch('crawlurl',function(newvalue,oldvalue){
                    $scope.final_data = newvalue;
                 },true)

                  $scope.$watch('crawlurl_exclude',function(newvalue,oldvalue){
                    $scope.final_data_tab2 = newvalue;
                   // console.log($scope.final_data_tab2);
                 },true)

                $scope.Api_save_exclude = function(){
                    var myitemid = $scope.currentitemid;
                    var final_data1 = [];
                   $scope.final_data.forEach(function(part){
                        if($scope.api_checkbox[part.itemId] == true){
                             final_data1.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':1});
                        }
                        if($scope.api_checkbox[part.itemId] == false){
                            final_data1.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':0});
                        }
                   })

                   console.log(final_data1);

                        $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/configIBR/api_save_exclude',
                                data    : { 'myitemid' : myitemid ,'data' : final_data1},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(data) {
                                $scope.isLoading = false;
                                 $('#exc_msg').show();
                                    //alert("Successfully Updated");
                                })
                            .catch(function (err) {
                                var msg =err.status;
                                 //Error Log To DB
                                 var screenname = '<?php echo $screen;?>';
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                               
                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                         data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                 .success(function(response) {
                                                //return true;
                                              });
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                         $('#Pnopopup2').css('display','none'); 
                                          $('#Pnopopup3').css('display','none'); 
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                });
                }

                $scope.Api_save_exclude_tab2 = function(){
                    console.log('trigger_tab2');
                    var myitemid_tab2 = $scope.currentitemid;
                    var final_data1_tab2 = [];
                   $scope.final_data_tab2.forEach(function(part){
                        if($scope.api_checkbox_tab2[part.itemId] == true){
                             final_data1_tab2.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':1});
                        }
                        if($scope.api_checkbox_tab2[part.itemId] == false){
                            final_data1_tab2.push({'itemId':part.itemId,'sellerUserName':part.sellerUserName,'title':part.title,'galleryURL':part.galleryURL,'exclude_flag':0});
                        }
                   })

                   //console.log(final_data1_tab2);

                        $http({                     
                                method  : 'POST',
                                url     : '<?php echo base_url();?>index.php/configIBR/api_save_exclude',
                                data    : { 'myitemid' : myitemid_tab2 ,'data' : final_data1_tab2},
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                            .success(function(data) {
                                $scope.isLoading = false;
                                $('#exc_msg_tab2').show();
                                    //alert("Successfully Updated");
                                })
                            .catch(function (err) {
                                var msg =err.status;
                                 //Error Log To DB
                                 var screenname = '<?php echo $screen;?>';
                                 var StatusCode = err.status;
                                 var StatusText = err.statusText;
                                 var ErrorHtml  = err.data; 
                               
                                 $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                         data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                         headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                 .success(function(response) {
                                                //return true;
                                              });
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                         $('#Pnopopup2').css('display','none'); 
                                          $('#Pnopopup').css('display','none'); 
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                });
                }
            $scope.submititems = function(){  
                    $scope.crawldata = 'nodata';
                    $scope.crawlersearch('crawlerbutton');
                    $scope.testsearch('savebutton');
                    $scope.isLoading =true;
                    $scope.default_flag = 1;
                     if($scope.condition == ''){
                        $scope.resultconditionvalue = '';
                    }
                    $scope.resultconditionvalue = $scope.condition;
                    //return false;
                    setTimeout(function() { 
                     $http({
                        method : 'POST',
                        url    : '<?php echo base_url(); ?>index.php/configIBR/save_key_search',
                        data   : {  'itemid':$scope.currentitemid,
                                    'keywords_and':$scope.keywordsand,
                                    'keywords_or':$scope.keywordsor,
                                    'keywords_exclude':$scope.keywordsmore,
                                    'catagory_id':$scope.resultcatlabel,
                                    'myprice':$scope.currentprice,
                                    'percentage':$scope.percent,
                                    'from_price':$scope.minus,
                                    'to_price':$scope.plus,
                                    'condition':$scope.resultconditionvalue,
                                    'shipping':$scope.freeshiping,
                                    'location':$scope.location,
                                    'comptitor':$scope.resultcomplabel,
                                    'sortorder':$scope.resultsortordervalue,
                                    'eBay_url':$scope.url,
                                    'crawlerurl':$scope.crawlerurl,
                                    'flag':$scope.default_flag,
                                    'catagoryid':$scope.def_cat_id
                                    },
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                     })
                    .success(function(data){
                        $scope.isLoading =false;
                       // $scope.get_category_result();
                        $('#filtr_msg').css('display','block');

                    })
                    .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
                     }, 5000);
                }
                 //suggest category combo for include  
                 $scope.setValue_main = function(index,arg){
                    $('#normal_search_inc').val(index);  
                    $('#searchResult_main').hide(); 
                    $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {}); 
                 }

                  $scope.partnosearch_main = function () {
                    if($scope.partnofirst !=''){
                        $('#searchResult_main').show();
                    }
                    else{
                         $('#searchResult_main').hide();
                    }
                      
                       /* $scope.filteredItems1 = $filter('filter')($scope.suggestcat, function (item) {
                              for(var attr in item) {
                                if(attr == "category") {
                                    if (searchMatch(item[attr], $scope.partnofirst)  > -1){
                                        return true;
                                    }
                                  }
                                }
                            return false;
                    });*/
                };

                //suggest category combo for exclude
                $scope.setValue = function(index){
                    $('#normal_search_exc').val(index);  
                    $('#searchResult').hide();  
                    $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});
                 }

                  $scope.partnosearch = function () {
                    if($scope.partno !=''){
                         $('#searchResult').show();
                    }
                    else{
                         $('#searchResult').hide();
                    }
                   
                               /* $scope.filteredItems1 = $filter('filter')($scope.suggestcat, function (item) {
                                      for(var attr in item) {
                                        if(attr == "category") {
                                            if (searchMatch(item[attr], $scope.partno)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });*/
                };

            //suggest category combo for exclude
                $scope.setValue = function(index){
                    $('#normal_search_exc').val(index);  
                    $('#searchResult').hide();  
                    $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});
                 }

                  $scope.partnosearch = function () {
                    if($scope.partno !=''){
                         $('#searchResult').show();
                    }
                    else{
                         $('#searchResult').hide();
                    }
                   
                               /* $scope.filteredItems1 = $filter('filter')($scope.suggestcat, function (item) {
                                      for(var attr in item) {
                                        if(attr == "category") {
                                            if (searchMatch(item[attr], $scope.partno)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });*/
                };


            //price calculation start
                $scope.price = $scope.currentprice;
                $scope.percentchange = function(){
                    if($scope.percent){
                      $scope.pricevalue = 1;
                      var currentprice  = $scope.currentprice;
                      var min_price     = $scope.currentprice / 100 * $scope.percent;
                      $scope.minus      = parseFloat(currentprice)-parseFloat(min_price);
                      $scope.plus       = parseFloat(currentprice)+parseFloat(min_price);
                    }
                    else{
                        $scope.minus = '';
                        $scope.plus = '';
                        $scope.pricevalue = 0;
                    }
                };


             // category label start     
                $scope.categorylabel       = [];
                $scope.get_category     = function(catname,catid,cattype){
                    if(cattype == 'Exclude'){
                        $scope.categorylabel[catid] = '!='+catname;
                    }
                    else{
                        $scope.categorylabel[catid] = catname;
                    }
                    angular.forEach($scope.categorylabel, function(value, key){
                        mainresultvalue[key] = value;
                    });
                   /* $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});*/
                    $scope.resultlabellength = 1;
                }

            // competitor label start 
            $scope.competitorlabel  =[];
            $scope.get_competitor =function(compname,comptype){
                if((comptype == 'Include' || comptype == 'Exclude') && compname != ''){
                    if(compname.indexOf(',') > -1){
                        if(comptype == 'Include'){
                            var mycomp = compname.split(',');
                            for(mycompkey in mycomp){
                                $scope.maincompetitorvalue[mycomp[mycompkey]] = mycomp[mycompkey];
                            }
                        }
                        else{
                            var mycomp = compname.split(',');
                            for(mycompkey in mycomp){
                                $scope.maincompetitorvalue[mycomp[mycompkey]] = '!='+mycomp[mycompkey];
                            }
                        }                     
                    }
                    else{
                        if(comptype == 'Include'){
                            console.log($scope.maincompetitorvalue);
                            $scope.maincompetitorvalue[compname] = compname; 
                        }
                        else{
                            $scope.maincompetitorvalue[compname] = '!='+compname;
                        }
                    }

                        var compresponse = [];
                        for(key in $scope.maincompetitorvalue){
                            if(key != ''){
                                compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                            }
                        }
                        $scope.resultcomplabel = compresponse;
                        $scope.resultcomplabellength = 1;
                }
            }

            $scope.eventclose = function(firstevent){

                     if(firstevent.target.attributes.datalabel.value == 'keywordsand'){
                        $scope.keywordsand = '';
                    }
                     if(firstevent.target.attributes.datalabel.value == 'keywordsor'){
                        $scope.keywordsor = '';
                    }
                    if(firstevent.target.attributes.datalabel.value == 'keywords'){
                        $scope.keywordsmore = '';
                    }
                    if(firstevent.target.attributes.datalabel.value == 'category'){
                        var index = mainresultvalue.indexOf(firstevent.target.attributes.datavalue.value);
                        //console.log(mainresultvalue);
                        if (index == -1) {
                            delete mainresultvalue[firstevent.target.attributes.datavalue.value];
                            $scope.resultcatlabel = mainresultvalue.reduce(function(result, item, index, array) {
                                                                      result[index] = {'name':item,'id':index};
                                                                      return result;
                                                             }, {});

                            if(Object.keys($scope.resultcatlabel).length == 0){
                                $scope.resultlabellength = 0;
                            }
                        }
                        //console.log(mainresultvalue);
                    }

                    if(firstevent.target.attributes.datalabel.value == 'pricelabel'){
                        $scope.percent = '';
                        $scope.minus = '';
                        $scope.plus = '';
                        $scope.pricevalue = 0;
                    }
                    if(firstevent.target.attributes.datalabel.value == 'conditionlabel'){
                        
                        $scope.resultcondition = '';
                        $scope.resultconditionvalue = '';
                        $scope.condition = false;
                    }
                    if(firstevent.target.attributes.datalabel.value == 'freeshiping'){
                        $scope.freeshiping = '';
                    }
                    if(firstevent.target.attributes.datalabel.value == 'location'){
                        $scope.location = '';
                    }

                    if(firstevent.target.attributes.datalabel.value == 'resultorder'){
                        $scope.resultsortorder      = '';
                        $scope.resultsortordervalue = 0;
                        $scope.resultsortorderlabel = 0;
                       // $scope.sortorder = 0;
                    }
                    if(firstevent.target.attributes.datalabel.value == 'seller'){
                        $scope.resultindex = $scope.maincompetitorvalue.indexOf(firstevent.target.attributes.datavalue.value);
                        if ($scope.resultindex == -1) {
                            delete $scope.maincompetitorvalue[firstevent.target.attributes.datavalue.value];
                            var compresponse = [];
                            for(key in $scope.maincompetitorvalue){
                                if(key != ''){
                                    compresponse.push({'key':$scope.maincompetitorvalue[key],'key_id':key});
                                }
                            }

                            $scope.resultcomplabel = compresponse;
                            //console.log($scope.maincompetitorvalue);
                            $scope.comp_inc = '';
                            $scope.comp_exc = '';
                            if(compresponse.length == 0){
                                $scope.resultcomplabellength = 1;
                            }
                        }
                    }
                    
                }


                $scope.resultconditionvalue = '';
                $scope.resultcondition = '';
                $scope.conditionchange = function(obj){
                    if(obj.target.checked){
                    $scope.resultcondition = obj.target.attributes.element.value;
                    $scope.resultconditionvalue = obj.target.attributes.value.value;
                     }      
                    else{       
                        $scope.resultcondition = '';        
                        $scope.resultconditionvalue = '';      
                    }
                }

                $scope.resultsortordervalue = '';
                $scope.sortorderchange = function(obj){
                    if($(".sortorderselect").find(':selected')){
                        $scope.resultsortorderlabel = 1;
                        $scope.resultsortorder = $(".sortorderselect").find(':selected').attr('itemvalue');
                        $scope.resultsortordervalue = $scope.sortorder;
                    }
                    else{
                        $scope.resultsortorder = '';
                        $scope.resultsortordervalue = '';

                    }
                    
                }
                $scope.popupclose = function(){
                    //$scope.get_category_result();
                    $("#Pnopopup2").hide(); 
                    $("#popup").hide(); 
                    $('#filtr_msg').hide();
                }


                // Cron Run functionality start
                $scope.Cron = function(){
                    $('#run_cron').show();
                     $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/configIBR/auto_raw_list_cron'
                            })
                                 .success(function(response) {    

                                });
                }

               /* $scope.cron_status = function(){
                 $http({                     
                         method  : 'POST',
                         url     : '<?php echo base_url();?>index.php/configIBR/cron_status_checker'
                        })
                             .success(function(response) {
                                console.log(response);
                             if(response.length == 0){
                                $("#run_cron > p").text("Please Wait..."); 
                                $scope.cron_sta_startdate = '';
                                $scope.cron_sta_enddate = '';
                                $scope.cron_sta_progress = '';
                                $scope.cron_sta_total = '';
                                $scope.cron_sta_status = '';
                             }
                             else{
                                $scope.cron_sta_startdate = response[0].start;
                                $scope.cron_sta_enddate = response[0].Ended;
                                $scope.cron_sta_progress = response[0].progress;
                                $scope.cron_sta_total = response[0].total;
                                $scope.cron_sta_status = response[0].status_field;
                                //console.log($scope.cron_sta_status);
                                if($scope.cron_sta_total == $scope.cron_sta_progress){
                                     $('#run_cron').hide();
                                    $('.run_cron_status').hide();
                                    $('#run_cron_end').show();
                                    setTimeout(function() {
                                        $("#run_cron_end").hide('blind', {}, 500)
                                    }, 10000);
                                }
                                else{
                                    $('#run_cron').hide();
                                    $('.run_cron_status').show();
                                }
                             }    
                        });
                }*/



        // config auto rawlist end
        //});
});


        app.directive('fileReader', function($timeout) {
            return {
                scope: {
                fileReader:"="
            },
            link: function(scope, element) {               
              $(element).on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                var name = files[0].name;
                var type = files[0].type;
                if (files.length) {
                  var r = new FileReader();
                  r.onload = function(e) {
                    var contents = e.target.result;
                    var resultvalue = [];
                    angular.forEach(contents.split('\n'), function(value, key) {
                        var result = value.split(",");
                        if (result.length != 1)
                        {
                            if (result[0] != '' && result[1] != '')
                            {
                                resultvalue.push({'MyPartNo' : result[0],'ItemId' : result[1],'SKU': result[2],'Title' : result[3],'Category':result[4],'MyPrice' : result[5],'Comp_High_Price' : result[6],'Comp_Low_Price' : result[7], 'Cost_Price':result[8],'Freight' : result[9],'Fees':result[10],'TotalPrice':result[11],'Recomend_Price':result[12],'RuleName':result[13],'flag' : 'I'});
                             }
                        }
                    });
                    scope.$apply(function () {
                        document.getElementById('fname').value = name;
                        document.getElementById('ftype').value = type;
                        scope.fileReader = resultvalue;
                    });
                    };
                r.readAsText(files[0]);
                };
              });
            }
          };
      });
</script>
<script>
          $(document).on('click','.close',function(){
          var name = document.getElementById('fname').value;
           if (name == '')
          {
            $("#Pnopopup").css("display", "none"); 
          }
          else
          {
            location.reload(true);
            $("#Pnopopup").css("display", "none");
          } 
        });
           //new popup msg close button
           $(document).on('click','.destroy',function(){
            $(".err_modal").css("display", "none");
            location.reload(true);
        });

           $(document).on('click','.destroy2',function(){
            $(".err_modal").css("display", "none");
            location.reload(true);
        });
           $(document).on('click','.clck',function(){
             $('.butn_hide').css('display','none');
            $('#proceedtoebay').trigger('click');
        });

            $(document).on('click','.nonclck',function(){
            $(".err_modal").css("display", "none");
            return false;
        });
</script>
<script>
    $(document).on('click','.myPnopopupsubclose',function(){
        $("#Pnopopupsalestrends").css("display", "none");
    });
</script>
<script>
        $(document).on('click','#close2',function(){
          $("#Pnopopup3").hide(); 
           $("#popup").hide(); 
            $('#exc_msg').hide();
         //location.reload(true); 
        });    
 </script>
<script type="text/javascript">
        Pusher.logToConsole = true;
        var pusher = new Pusher('26e79100d1fea27244e6', {
          encrypted: true
        });
        var channel = pusher.subscribe('schedulerprogressarrowhead');
        channel.bind('myevent', function(data) {          
            document.getElementById("run_cron").value = "";
            document.getElementById("run_cron").innerHTML = data.message;
        });
        Pusher.logToConsole = true;
        var pusher = new Pusher('26e79100d1fea27244e6', {
          encrypted: true
        });
        var channel = pusher.subscribe('mynewchannelarrowhead');
        channel.bind('eventarrow', function(data) {
            document.getElementById("run_cron").value = "";
            document.getElementById("run_cron").innerHTML = "Schedular Process Completed Successfully.";
            setTimeout(function () {
                document.getElementById('run_cron').style.display='none';
            }, 10000);
        });
</script>
<script>
     $(function () {
        $("#tabs").tabs();
    });
</script>
</body>
</html>
