<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
<body class="main" ng-app="myApp" ng-controller="myCtrl" ng-cloak>
    <?php $this->load->view('header.php'); ?>
    <div>
     <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
       
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
            .viewicon{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/edit.png') no-repeat scroll 5px center;
                    padding: 18px 6px 0px 28px;
                    }
            .viewicon2{
                    cursor: pointer !important;
                    background: rgba(0, 0, 0, 0) url('<?php echo base_url();?>/assets/img/delete_tbl.png') no-repeat scroll 53px center;
                    padding: 18px 6px 0px 28px;
            }
            .usagelimit{
                    margin: 0px 100px 0 0;
            }
            .button_add_e{
            padding: 3px 24px 3px 13px;
            margin: 0 10px 0 0;
            border: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            font-family: sans-serif, Verdana, Geneva;
            text-align: right;
            display: inline-block;
            color: #fff;
            vertical-align: top;
            background: url(../img/add1.png) no-repeat right -1px #00763C;
        }
        </style>
       
                <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="wrapper">
                <div class="content_main_all">
                  <div class="button_right" >
                                     <button value="Search" class="button_add" ng-click="newkey()">Create New Key</button>
                         </div>   
                         <div  class="button_right" style="width:90%; color: #2D8BD5;cursor:pointer;font-weight: bold;" >
                          Rawlist Search Items Count : <input type ="text" style="width:4%;"  ng-model ="count"/>
                          <button class="button_add_e" ng-click = "pagecountsave()">Save</button>
                         </div>   

                        <div class="content_main">
                             
                            <div class="table_main" id="api">
                                <div class="left_heading">
                                </div>
                                <div id="msg" style="display: none;"></div>
                                <table cellspacing="0" cellpadding="0" border="1" style="width:100%;" >
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;"">Active</th>
                                            <th style="width: 8%;">Key ID</th>
                                           <!--  <th style="width: 10%;">App ID </th> -->
                                            <th style="width: 10%;">Dev ID</th>
                                            <th style="width: 10%;">Cert ID</th>
                                            <th style="width: 8%;">Environment</th>
                                            <th style="width: 8%;">Usage Limit</th>
                                            <th style="width: 5%;">Action</th>

                                        </tr>
                                       
                                    </thead>
                                    
                                    <tbody  ng-model="datas" ng-repeat="value in datas">
                                    <tr>
                                        <td ng-if="value.Active_flag == '0'">
                                          <input type="checkbox" name="temp" class="field_chk ex_list_checkbox" data-value="{{value.KeyID}}"/></td>
                                        <td ng-if="value.Active_flag == '1'">
                                          <input type="checkbox" name="temp" class="field_chk ex_list_checkbox" data-value= "{{value.KeyID}}"  checked /></td>
                                        <!-- <td ng-click="editpopup(value.KeyID,1)" data-value="{{value.KeyID}}" style="cursor:pointer; text-align:justify;" class="viewicon" >{{value.KeyID}}</td> -->
                                        <td data-value="{{value.KeyID}}" style="cursor:pointer; text-align:justify;" >{{value.KeyID}}</td>
                                       <!--  <td style=" text-align:justify;">{{value.AppID}}</td> -->
                                        <td style="cursor:pointer; text-align: justify;">{{value.DevID}}</td>
                                        <td style="cursor:pointer; text-align: justify;">{{value.CertID}}</td>
                                        <td style="cursor:pointer;">{{value.Environment}}</td>
                                        <td ng-click="checkpopup(value.KeyID)"><span style="color: blue;cursor:pointer;"><u>Check</u></span></td>
                                        <td ng-click="editpopup(value.KeyID,2)" class="viewicon2"></td>
                                        
                                    </tr>
                                    </tbody>

                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        </div>
   <?php $this->load->view('footer.php'); ?>
   <div id="Pnopopup2" class="modal">
                            <div class="pop_map_my_part">
                               
                                 <div class="pop_container">
                                            <div class="heading_pop_main">
                                                <h4>
                                                   Add New Key ID#
                                                </h4>
                                                 <span class="close">&times;</span>
                                            </div>
                                    <div class ="pop_pad_all_add_new">
                                    <form id="saveme"  ng-submit= "savekey()">
                                    <div class="channel_main">
                                        <label class="label_inner_general" >Key ID <span style="color:red; font-size:16px;">*</span> </label>
                                            <p class="drop_down_med1" ng-if="!key">
                                                :  <input type="text" id="Key" ng-model="key" class="input_med" name=" Part" value="" required />
                                            </p>
                                            <p class="drop_down_med1" ng-if="key">
                                                :  <input class="input_lable_disabled" type="text" id="Key" ng-model="key" class="input_med" name=" Part" value="" readonly/>
                                            </p>
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general" >App ID <span style="color:red; font-size:16px;">*</span> </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id="AppID"  ng-model="AppID" name="SKU2" value="" required />
                                        </p>
                                    </div>
                                     <div class="channel_main">
                                        <label class="label_inner_general" >Dev ID <span style="color:red; font-size:16px;">*</span> </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id="DevID"  ng-model="DevID" name="SKU2" value="" required />
                                        </p>
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general" >Cert ID  <span style="color:red; font-size:16px;">*</span> </label></label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id="CertID " ng-model="CertID " name="CostPrice2" value="" required />
                                        </p>
                                        <p class="resutnresponse"></p>

                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general"  >Compatability <span style="color:red; font-size:16px;">*</span> </label> </label>
                                        <p class="drop_down_med1">
                                           :  <input class="input_med" id="Compatability" ng-model="Compatability" name="Freight2" value="" required/>
                                        </p>

                                     <div class="channel_main">
                                        <label class="label_inner_general"  >Server URL <span style="color:red; font-size:16px;">*</span> </label> </label>
                                        <p class="drop_down_med1">
                                           :  <input class="input_med" id="ServerURL" ng-model="ServerURL" name="Freight2" value="" required />
                                        </p>


                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general" >Site ID <span style="color:red; font-size:16px;">*</span> </label></span> </label>
                                        <p class="drop_down_med1">
                                            :  <input class="input_med" id= "SiteID" name= "SiteID" ng-model="SiteID"  value="" required/>
                                        </p>

                                    </div>
                                     <div class="channel_main">
                                        <label class="label_inner_general">Environment <span style="color:red; font-size:16px;">*</span> </label></label>
                                        <p class="drop_down_med1">
                                            :   <select class="drop_down_med"  id="Environment" ng-model="Environment" required>
                                            <option > - Select - </option>
                                            <option id="Sandbox"  ng-selected="Environment == Sandbox"  >    Sandbox</option>
                                            <option id="Production" ng-selected="Environment == Production"> Production</option>
                                        </select> <!-- ng-selected="Environment == Production" -->
                                        </p>
                                    </div>
                                    <div class="channel_main">
                                        <label class="label_inner_general" >Authentication Key<span style="color:red; font-size:16px;">*</span> :</label>
                                        <p class="drop_down_med1">
                                             <textarea  name="list" id= "Authentication" ng-model="Authentication" class="textarea" required></textarea> 
                                        </p>

                                    </div>
 
                                    <div class="save_icon_manage">
                                        <input type="submit"  value="Save" class="button_add_part_save ">
                                    </div>
                                    </form>

                                </div>
                            </div>

                        </div>


                                </div>
                            </div>

                        </div>
<div id="popup" class="modal">
<div class="pop_map_my_part">
 <div class="pop_container">
    <div class="heading_pop_main">
            <h4>
               Ebay Key Details#
            </h4>
             <span class="close">&times;</span>
        </div>
<div class ="pop_pad_all_add_new" style="padding: 23px;">
                                    
    <!-- $Remaining = $result->DailyHardLimit - $result->DailyUsage; -->
     <br><span style="font-weight:bold; margin: 0 114px 0 0;">Daily Limit</span>: {{dailylimit}}
     <br><span style="font-weight:bold; margin: 0 108px 0 0;">Used so far</span>: {{user1}}
     <br><span style="font-weight:bold; margin: 0 63px 0 0;">Remaining Today</span>: {{RemainingDaily}}
     <hr>

    <!--  $Remaining = $result->HourlyHardLimit - $result->HourlyUsage; -->
      <br><span style="font-weight:bold; margin: 0 102px 0 0;">Hourly Limit</span>: {{hourlylimit}}
      <br><span style="font-weight:bold; margin: 0 104px 0 0;">Used so far </span>: {{user2}}
      <br><span style="font-weight:bold; margin: 0 59px 0 0;">Remaining Hourly</span>: {{RemainingHourly}}
      <hr>

    <!-- $Remaining = $result->PeriodicHardLimit - $result->PeriodicUsage; -->
      <br><span style="font-weight:bold; margin: 0 90px 0 0;">Periodic Limit</span>: {{periodlimit}}
      <br><span style="font-weight:bold; margin: 0 109px 0 0;">Used so far</span>: {{user3}}
      <br><span style="font-weight:bold; margin: 0 10px 0 0;">Remaining in this period</span>: {{RemainingPeriodic}}
      <br><span style="font-weight:bold; margin: 0 48px 0 0;">Period Started from</span>: {{periodstart}}
      <br><span style="font-weight:bold; margin: 0 85px 0 0;">Current Period</span>: {{periodcurrent}}
      <hr>
    </div>
                                   
                                   
</div>

                         
    <script src="<?php  base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

</script>
 <script>
    $(document).on('click','.ex_list_checkbox',function(){

        var keyid = $(this).attr('data-value');
        var checkid = '';
            if($(this).prop("checked") == true){
              checkid = 1;  
            }
            else{
              checkid = 0;  
            }
            $.ajax({
                   url: 'checkbox_update',
                   type:'POST',
                   data: {'keyid':keyid,
                        'checkid':checkid},
                   success: function(data) {
                        //alert('Active Flag Update Successfully');
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                            data    : {'msgstatus':6},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                   },
                    error : function(response){
                          var screenname = '<?php echo $screen;?>'; 
                          var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                    }
            });
    });
</script>
<script >
 var app = angular.module('myApp', []);
            app.controller('myCtrl', function($scope,$http,$filter) {
                   var screenname = '<?php echo $screen;?>'; 
                //to get data from table
                         $http({
                                    method: 'POST',
                                    url     : '<?php echo  base_url();?>index.php/Ebay_Settings/getdata',
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).success(function(data) {
                                    console.log(data);
                                    $scope.datas = data;
                                    $scope.count = data[0].pagecount;
                                    console.log($scope.count);
                                    });  
                     //pagecount save
                     $scope.pagecountsave=function(){
                         $scope.isLoading = true;
                         var pagecount= $scope.count;
                         console.log($scope.count);

                     
                      $http({                     
                            method  : 'POST',
                            url     : '<?php echo  base_url();?>index.php/Ebay_Settings/pagecountsave',
                            data    : {'pagecount':pagecount},
                            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                            })
                                .success(function(data) {
                                    $scope.isLoading = false;
                                    //alert("Successfully Updated");
                                     $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');  
                                                     $("#Pnopopup2").css('display','none');                
                                                    
                                                })
                                    //location.reload(true);
                                })

                             .catch(function(err) {
                                    $scope.isLoading = false;
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                         $("#Pnopopup2").css('display','none');                 
                                        
                                    });
                                })
                         }


                      //popupdisplay       
                     $scope.newkey = function(){
                        $("#Pnopopup2").css("display", "block");
                               $scope.key            = '';
                               $scope.AppID          = '';
                               $scope.DevID          = '';
                               $scope.CertID         = '';
                               $scope.ServerURL      = '';
                               $scope.Compatability  = '';
                               $scope.SiteID         = '';
                               $scope.Environment    = '';
                               $scope.Authentication = '';
                      };  

                     //keysave
                     $scope.savekey = function() {
                        var keyval = $('#Key').val();
                               $scope.isLoading = true;
                               var key               = keyval;
                               //console.log(keyval);
                               var AppID             = $scope.AppID;
                               var DevID             = $scope.DevID;
                               var CertID            = $scope.CertID;
                               var ServerURL         = $scope.ServerURL;
                               var Compatability     = $scope.Compatability;
                               var SiteID            = $scope.SiteID;
                               var Environment       = $scope.Environment;
                               var Authentication    = $scope.Authentication;
                                                               
                               $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo  base_url();?>index.php/Ebay_Settings/savenewkey',
                                    data    : {'key':key,'AppID':AppID,'DevID':DevID,'CertID':CertID,'ServerURL':ServerURL,'Compatability':Compatability,'SiteID':SiteID,'Environment':Environment,'Authentication':Authentication},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                    })
                                .success(function(data) {
                                    $scope.isLoading = false;
                                    //alert("Successfully Updated");
                                     $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');  
                                                     $("#Pnopopup2").css('display','none');                
                                                    
                                                })
                                    //location.reload(true);
                                })

                             .catch(function(err) {
                                    $scope.isLoading = false;
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                         $("#Pnopopup2").css('display','none');                 
                                        
                                    });
                                })

                    }

                    //editkey
                    $scope.editpopup = function(param1,param2) {
                       $scope.isLoading = true;
                       var keyid = param1;
                       var param = param2;
                       console.log(param2);
                       $scope.Environment    = ''; 
                       //console.log(keyid);
                        if(param == 1)
                       {
                       $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo  base_url();?>index.php/Ebay_Settings/edit',
                                    data    : {'key':param1,'action':param2},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                    })
                       .success(function(response) {
                                $scope.isLoading = false;
                                $scope.keys            = response[0];
                                $scope.key             = $scope.keys.KeyID;
                                $scope.AppID           = $scope.keys.AppID;
                                $scope.DevID           = $scope.keys.DevID;
                                $scope.CertID          = $scope.keys.CertID;
                                $scope.ServerURL       = $scope.keys.ServerURL;
                                $scope.Compatability   = $scope.keys.Compatability;
                                $scope.SiteID          = $scope.keys.SiteID;
                                $scope.Environment     = $scope.keys.Environment;
                                $scope.Authentication  = $scope.keys.AuthKey;
                                 $("#Pnopopup2").css("display", "block");
                        })
                       .catch(function(err) {
                                $scope.isLoading = false;
                                  var StatusCode = err.status;
                                  var StatusText = err.statusText;
                                  var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                 var msg =err.status;
                            //failure msg
                            $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                            })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    $("#Pnopopup2").css('display','none');                 
                                    
                              });
                       })
                   }
          else
          {
                        $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo  base_url();?>index.php/Ebay_Settings/delete',
                                    data    : {'key':param1,'action':param2},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                    })
                       .success(function(data) {
                                    $scope.isLoading = false;
                                    //alert("Successfully Updated");
                                     $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                     data    : 4
                                                    })
                                                 .success(function(response) {
                                                    $('#msg').html(response);
                                                    $('.err_modal').css('display','block');
                                                    $('#msg').css('display','block');  
                                                     $("#Pnopopup2").css('display','none');                
                                                })
                                    //location.reload(true);
                                })
                        .catch(function(err) {
                                    $scope.isLoading = false;
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                         $("#Pnopopup2").css('display','none');                 
                                    });
                                })

          }
                   };

                   $scope.checkpopup = function(id) {
                    $scope.isLoading = true;
                       var keyid = id;
                       var key = [];
                        $scope.datas.forEach(function (data) {
                            if (data.KeyID == id){
                                key.push(data);
                            }
                        });
                        
                       $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo  base_url();?>index.php/Ebay_Settings/check',
                                    data    : {'key':key},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                    })
                       .success(function(data) {
                            $scope.isLoading = false;
                            $scope.details = data;
                            $scope.RemainingDaily = $scope.details['DailyHardLimit'] - $scope.details['DailyUsage'];
                            $scope.dailylimit = $scope.details['DailyHardLimit'];
                            $scope.user1 = $scope.details['DailyUsage'];
                            $scope.RemainingHourly = $scope.details['HourlyHardLimit'] - $scope.details['HourlyUsage'];
                            $scope.hourlylimit = $scope.details['HourlyHardLimit'];
                            $scope.user2 = $scope.details['HourlyUsage'];
                            $scope.RemainingPeriodic = $scope.details['PeriodicHardLimit'] - $scope.details['PeriodicUsage'];
                            $scope.periodlimit = $scope.details['PeriodicHardLimit'];
                            $scope.user3 = $scope.details['PeriodicUsage'];
                            $scope.periodstart = $scope.details['PeriodicStartDate'];
                            $scope.periodcurrent = $scope.details['ModTime'];
                            $("#popup").css("display", "block");  
                                    
                        })
                       .catch(function(err) {
                                    $scope.isLoading = false;
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                     var msg =err.status;

                                //failure msg
                                $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                     data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                        $("#Pnopopup2").css('display','none');                 
                                        
                                  });
                           })

                        };
                                       
                    });
</script>
<script>
    $(document).on('click','.close',function(){
          $("#Pnopopup2").hide(); 
           $("#popup").hide(); 
         //location.reload(true); 
        });    
 </script>
 <script>
          //new popup msg close button
           $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
          //location.reload(true);
      });
</script>
</body>
</html>
