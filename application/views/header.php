<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="<?php echo  base_url();?>assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/jquery-ui.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown1.css">
     <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/css/datePicker.css">

    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/demo.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/jquery.datepick.css"/> 
    <!-- following stylesheets are only for demos with calendar.js -->
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/month.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/week.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/flight.css">
    <link rel="stylesheet" href="<?php echo  base_url();?>assets/css/style_romaine.css" /> 
    <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.tree-multiselect.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/css/amazondive.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<style>
  ul,li { margin:0; padding:0; list-style:none;}
        .label { color:#000; font-size:16px;}
        .ckeckclass
        {
            margin:2px;
        }
        .divTable{
            float: left;
            width: 70px;
            height: 40px;
            border-right: 1px solid #e1e1e1;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .lastdivTable{
            float: left;
            width: 70px;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .paging-nav {
          text-align: right;
          padding-top: 2px;
        }

        .paging-nav a {
          margin: auto 1px;
          text-decoration: none;
          display: inline-block;
          padding: 1px 7px;
          background: #91b9e6;
          color: white;
          border-radius: 3px;
        }

        .paging-nav .selected-page {
          background: #187ed5;
          font-weight: bold;
        }
     /*   .curser_pt {
                cursor: pointer;
                background: url("../assets/img/sort_icon.png") no-repeat 5px center;
                padding: 0 20px;
            }*/
        tfoot
        {
        text-align: center !important;
        display: table-row-group !important;
        }

        .paging-nav,
        #tblmasterresult {
         
          margin: 0 auto;
          font-family: Arial, sans-serif;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 20%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .myPnopopupsubclose{
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;   
        }
        .myPnopopupsubclose:hover,
        .myPnopopupsubclose:focus{
            color: black;
            text-decoration: none;
            cursor: pointer;   
        }

        div.absolute {
            position: absolute;
            top: 326px;
            left: 758px;
            width: 46px;
            height: 407px;
            cursor: pointer;
           
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        .err_modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .err_modal1 {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
         .err_modal_sesion {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        .modal-content {
            background-color: #fefefe;
            margin: 10% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 32%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #2d8bd5;
            text-decoration: none;
            cursor: pointer;
        }
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }

        .pusheralert {
            position: fixed;
            left:  77%;
            width : 20%;
            height : 3%;
            padding: 20px;
            background-color: #4CAF50;
            color: white;
            top: 80%;
            z-index: 999;
            bottom : 0px;
        }

        .pusherclosebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .pusherclosebtn:hover {
            color: black;
        }
</style>
<body onload="set_interval()" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()"
        onscroll="reset_interval()"/> 
<div id="pusherresult" class="pusheralert" style="display:none;">
    <span class="pusherclosebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Scheduler </strong> Process Completed Successfully.
</div>
<div class="container head_bg">
        <div class="head">
                <div class="logo">
                    <a href='<?php echo base_url();?>DashBoard/index'> <img src="<?php echo base_url().'assets/';?>img/logo_12.png"></a>
                </div>
                <div class="nav nav_bg">
                <ul>
                    <li class="submenu_inner"><a class="last_menu_icon" href="<?php echo base_url();?>DashBoard/index"><img src="<?php echo base_url();?>assets/img/home.png">&nbsp Market Share - Dashboard</a></li>
                    <?php   
                            foreach($menu_data as $key => $value){
                                echo '<li class="submenu_inner"><a class="last_menu_icon" href="#"><img src="'.base_url().$icononly[$key].'"> '.$key.'</a>';
                                echo '<ul class="level3">';
                                if(is_array($value)){
        
                                    foreach($value as $subvalue){

                                            echo '<li><a href="'.base_url().$url[$subvalue].'"> '.$subvalue.'</a></li>';
                                    } 
                                }
                                echo '</ul></li>';
                            }
                            
                        ?>
                </ul>
        </div>
        
                <div class="headright">
                    <div class="login">
                        <a class="last_menu_icon" target="blank" href="http://192.168.100.234/apatechservice/(S(rpd4ubmhgxl0kqjd1ylnwrzl))/Master/FeedBackInfo.aspx?id=10131&ticketcreatedcustomer=<?php echo $this->session->userdata('username'); ?>">
                                <img src="<?php echo base_url();?>assets/img/feedback.png"/>
                        </a>
                        <p>Hi <?php echo $this->session->userdata('username') ?></p>
                        <a href="<?php echo base_url();?>Login/logout"><img src="<?php echo base_url().'assets/';?>img/logout1.png" alt="Logout" title="Logout" /></a>
                        <div>
                    <a href='<?php echo base_url();?>ChangePassword/index'><p style="font-size: 14px;">Change Password</p></a>
                    </div>
                    </div>

                </div>

                <div class="cbr_update">
            <p>Data As on : <?php echo $this->session->userdata('cbrdate') ?></p>
        </div>
            
        </div>
    </div>
     <div id="msg_seson" style="display: none;"></div>
    <div>
       <script>
        var timer = 0;
        function set_interval() {
          timer = setInterval("auto_logout()", 900000);
        }
        function reset_interval() {
          if (timer != 0) {
            clearInterval(timer);
            timer = 0;
            timer = setInterval("auto_logout()", 900000);
          }
        }

        function auto_logout() {
            //window.alert("Session Timeout.Please log in...!");
             $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/session_success_msg',
                            data : {'msgstatus':11},
                            
                                success: function(response){
                                        $('#msg_seson').html(response);
                                        $('.err_modal_sesion').css('display','block');
                                        $('#msg_seson').css('display','block');
                                },
                            })
         // window.location = "<?php echo base_url();?>Login/logout";
        }
         function Session_LogOut(){
            $(".err_modal_sesion").css("display", "none");
            window.location= "<?php echo base_url();?>Login/logout";
        }
    
        </script>
    <script>
        Pusher.logToConsole = true;
        var pusher = new Pusher('26e79100d1fea27244e6', {
          encrypted: true
        });
        var channel = pusher.subscribe('mynewchannelarrowhead');
        channel.bind('eventarrow', function(data) {        
            console.log(data.message);  
            document.getElementById("pusherresult").style.display = "block";
        });
    </script>


