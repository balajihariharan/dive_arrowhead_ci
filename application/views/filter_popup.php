<!DOCTYPE html>
<html ng-app="filterApp">
<html>
<head>
<title>DIVE : Filter</title>
    <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>   
<!--     <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script>
    <script src="<?php echo base_url().'assets/';?>Romaine/js/xlsx.full.min.js"></script> -->
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script><link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="<?php echo  base_url();?>assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown1.css">
     <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/css/datePicker.css">

    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/demo.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/jquery.datepick.css"/> 
    <!-- following stylesheets are only for demos with calendar.js -->
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/month.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/week.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/flight.css">
    <!-- <link rel="stylesheet" href="<?php echo  base_url();?>assets/price_css/style_romaine.css" />  -->
    <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery.tree-multiselect.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
.table tr, .table th, .table td{
    height: auto;
    padding: 6px 20px;
}
  ul,li { margin:0; padding:0; list-style:none;}
        .label { color:#000; font-size:16px;}
        .ckeckclass
        {
            margin:2px;
        }
        .divTable{
            float: left;
            width: 70px;
            height: 40px;
            border-right: 1px solid #e1e1e1;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .lastdivTable{
            float: left;
            width: 70px;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .paging-nav {
          text-align: right;
          padding-top: 2px;
        }

        .paging-nav a {
          margin: auto 1px;
          text-decoration: none;
          display: inline-block;
          padding: 1px 7px;
          background: #91b9e6;
          color: white;
          border-radius: 3px;
        }

        .paging-nav .selected-page {
          background: #187ed5;
          font-weight: bold;
        }
     /*   .curser_pt {
                cursor: pointer;
                background: url("../assets/img/sort_icon.png") no-repeat 5px center;
                padding: 0 20px;
            }*/
        tfoot
        {
        text-align: center !important;
        display: table-row-group !important;
        }

        .paging-nav,
        #tblmasterresult {
         
          margin: 0 auto;
          font-family: Arial, sans-serif;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 38%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .myPnopopupsubclose{
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;   
        }
        .myPnopopupsubclose:hover,
        .myPnopopupsubclose:focus{
            color: black;
            text-decoration: none;
            cursor: pointer;   
        }

        div.absolute {
            position: absolute;
            top: 326px;
            left: 758px;
            width: 46px;
            height: 407px;
            cursor: pointer;
           
        }
        body{
            background: none;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        .modal-content {
            background-color: #fefefe;
            margin: 10% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 38%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #2d8bd5;
            text-decoration: none;
            cursor: pointer;
        }
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }
}
</style>

   
	</head>
<body class="main" ng-controller="filterCtrl">
<!-- loading gif-->
<!-- <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div> -->

            
               

           
        <div class="container">
        <div class="wrapper">
			
            <div class="content_main_all content_main_filter">
           
               <div class="plan_list_view">




                <table cellspacing="0" border="1" class="table">
                    <thead>
                        <tr>
                            <th class="curser_pt" ng-click="sortBy('exclusion_id')">Filter Id</th>
                            <th class="curser_pt" ng-click="sortBy('exclusion_name')">Filter Description</th>
                            <th class="curser_pt">Active</th>
                        
                         </tr>
                         <tr>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text" ng-model="excidquery" ng-change="exclusion_id_search()" class="
                                    " placeholder="Filter Id"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="excnamequery" ng-change="exclusion_name_search()" class="inputsearch" placeholder="Filter Description"/>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                               
                            
                            </tr>
                    </thead>
                    <tbody>
                         <tr ng-repeat="filterdata in pagedItems[currentPage] | orderBy:propertyName:reverse">
                                <td>{{filterdata.exclusion_id}}</td>
                                <td>{{filterdata.exclusion_name}}</td>
                                <td ng-if="filterdata.active_flag == '0'">
                                    <input type="checkbox" class="field_chk ex_list_checkbox" data-value="{{filterdata.exclusion_id}}" /></td>
                                <td ng-if="filterdata.active_flag == '1'">
                                    <input type="checkbox" class="field_chk ex_list_checkbox" data-value="{{filterdata.exclusion_id}}" checked /></td>
                               
                              
                            </tr>
                    </tbody>
            
                </table>


                </div>
            </div>
            </div>
    </div>
   

    <script>
    $(document).on('click','.ex_list_checkbox',function(){
        var dataid = $(this).attr('data-value');
        var checkid = '';
            if($(this).prop("checked") == true){
              checkid = 1;  
            }
            else{
              checkid = 0;  
            }
            $.ajax({
                   url: 'exclusionlist_update',
                   type:'POST',
                   data: {'dataid':dataid,
                        'checkid':checkid},
                   success: function(data) {
                        alert('Active Flag Update Successfully');
                   },
                   error : function(data){
                        alert('Active Flag Update Successfully');
                   } 
            });
    });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
    <script>
        var sortingOrder = 'ExclusionID';
        var app = angular.module('filterApp', []);
            app.controller('filterCtrl', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 10;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.fdata = '';
                $scope.currentPage = 0;
                $http.get("disp")
                .then(function (response) 
                {
                    $scope.fdata = response.data;
                    console.log(response.data);

                    $scope.sortBy = function(propertyName) {
                        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                        $scope.propertyName = propertyName;
                    };

                    var searchMatch = function (haystack, needle) {
                        if (!needle) {
                            return true;
                        }
                        if(haystack != ''){
                            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                        }
                    };

                    // init the filtered items
                    $scope.exclusion_id_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                                for(var attr in item) {
                                    if(attr == 'exclusion_id'){
                                        if (searchMatch(item[attr], $scope.excidquery))
                                        return true;
                                    }
                                }
                                return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };
                            
                    // calculate page in place
                    $scope.groupToPages = function () {
                        $scope.pagedItems = [];
                        for (var i = 0; i < $scope.filteredItems.length; i++) {
                            if (i % $scope.itemsPerPage === 0) {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                            } 
                            else {
                                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);   
                            }
                        }
                    };

                    $scope.range = function (start, end) {
                        var ret = [];
                        if (!end) {
                            end = start;
                            start = 0;
                        }
                        for (var i = start; i < end; i++) {
                            ret.push(i);
                        }
                        $scope.pagenos = ret;
                        return ret;
                    };
                            
                    $scope.prevPage = function () {
                        if ($scope.currentPage > 0) {
                            $scope.currentPage--;
                        }
                    };
                            
                    $scope.nextPage = function () {
                        if ($scope.currentPage < $scope.pagedItems.length - 1) {
                            $scope.currentPage++;
                        }
                    };
                            
                    $scope.setPage = function () {
                        $scope.currentPage = this.n;
                    };

                    // functions have been describe process the data for display
                    $scope.exclusion_id_search();

                    // change sorting order
                    $scope.sort_by = function(newSortingOrder) {
                        if ($scope.sortingOrder == newSortingOrder)
                            $scope.reverse = !$scope.reverse;
                            $scope.sortingOrder = newSortingOrder;
                            // icon setup
                            $('th i').each(function(){
                                // icon reset
                                $(this).removeClass().addClass('icon-sort');
                            });
                            if ($scope.reverse)
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                            else
                                $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                    };
                            
                    $scope.$watch('currentPage', function(pno,oldno){
                        if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                            var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                            $scope.range(start, $scope.pagedItems.length);
                        }
                    });

                    $scope.range($scope.pagedItems.length);
                 
                    $scope.exclusion_name_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                            for(var attr in item) {
                                if(attr == 'exclusion_name'){
                                    if (searchMatch(item[attr], $scope.excnamequery))
                                        return true;
                                    }
                                }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.created_by_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                            for(var attr in item) {
                                if(attr == 'created_by'){
                                    if (searchMatch(item[attr], $scope.crbyquery))
                                        return true;
                                    }
                                }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                           $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                    $scope.created_on_search = function () {
                        $scope.filteredItems = $filter('filter')($scope.fdata, function (item) {
                            for(var attr in item) {
                             if(attr == 'created_on'){
                                if (searchMatch(item[attr], $scope.cronquery))
                                    return true;
                                }
                            }
                            return false;
                        });
                        // take care of the sorting order
                        if ($scope.sortingOrder !== '') {
                           $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                        }
                        $scope.currentPage = 0;
                        // now group by pages
                        $scope.groupToPages();
                    };

                               $scope.delete_filter =function(e){
                            var del = e.target.getAttribute('id');

                             $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Filters/delete_filter',
                                    data    : { 'id' : del},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                })
                                .success(function(data) {
                                    alert('Deleted Successfully');
                                    location.reload();
                                })
                    }
                });
            });
    </script>
</body>
</html>