
<div class="container" id="div3">
        <div class="wrapper">
      <div class="content_main_all">
<div class="plan_list_view" style="overflow: auto;">
                <div class="display_inline_blk">
                    <span class="week">Week : {{week}}</span>
                    <span class="week">Competitor Name : {{compname}}</span>
                    <span class="week">Category Name : {{catname}}</span>
                </div>
    <p class="display_inline_blk">
        <input type="button" class="back_button_rep3" ng-click="Back2()" value="Back"/>
    </p>

   <!--  <div class="button_right">       
            <input type="button" class="export_buttonnew" value="Export As Excel" ng-click="exportDataDrill3()"/>
    </div> -->
    <div style="width: 133px;" class="lable_normal table_top_form">
     <input id="showvalues" ng-model="checkboxModel.showvalues" ng-click="salestrendsshowvalues()" type="checkbox">
     Show values
     </div>
     <div style="margin-left: 7px;width: 316px;" class="lable_normal table_top_form">
          <input type="radio" id="radio1" name="mapped" ng-click="showitem(0)"/>
         <label style="margin: 0 0 0 0;" for="radio1">Show All Items</label>
        <input type="radio" id="radio2" name="mapped" ng-click="showitem(1)"/>
        <label style="margin: 0 0 0 0;" for="radio2">Show Only Mapped Items</label>
    </div>
      <div id="metrics" class="itm_lst"  ng-repeat="totsal in totalsales">
                      <div class="itm_lst">
                           <p>Total item listed:</p>
                           <img src="<?php echo base_url().'assets/';?>img/list.png">
                           <p style="font-size: 20px; color: #00886d;">{{totsal.TotalListings}}</p>
                      </div>
                       <div class="itm_lst">
                           <p>Total Sales Since Inception:</p>
                            <img src="<?php echo base_url().'assets/';?>img/doller.png">
                           <p style="font-size: 20px; color: #00886d;">{{totsal.TotalSales}}</p>
                      </div>
                       
      </div>

<br><br>
<div style="margin-left: 1px;width: 392px;;padding: 0px 2px 3px 0px;margin: 5px 2px 3px 7px;" id="div3_head" class="lable_normal table_top_form">
    <input type="radio" id="radio_lvl3_11" name="category_lvl3" ng-click="showcat(0)" checked/>
    <label style="margin: 0 0 0 0;" for="radio_lvl3_11">Market Share - Master List</label>
    <input type="radio" id="radio_lvl3_12" name="category_lvl3" ng-click="showcat(1)"/>
    <label style="margin: 0 0 0 0;" for="radio_lvl3_12">Market Share - Competitor</label>
</div>
<table cellspacing="0" border="1" class="status_bar">
    <thead>
        <tr>
            <th class="curser_pt" ng-click="sortBy('itemid')">Item ID #</th>
            <th class="curser_pt" ng-click="sortBy('sku')" >SKU</th>         
            <th class="curser_pt" ng-click="sortBy('mypartno')">My Part #</th>
            <th class="curser_pt" ng-click="sortBy('title')">Title</th>    
            <th class="curser_pt" ng-click="sortBy('price')">Current Price (In $)</th>
            <th class="curser_pt" ng-click="sortBy('qtysold')">Qty. Sold (5 wk)</th>
            <th class="curser_pt" ng-click="sortBy('totalsales')">Total Sales (5 wk) (In $)</th>
            <th class="curser_pt" ng-click="sortBy('pricevw1')">5 wk Price (In $)</th>
            <th class="curser_pt" ng-click="sortBy('qtysoldvw1')">5 wk Qty.Sold</th>  
        </tr>

    </thead>
     <tfoot>
      <tr>
                <td>
                  <div class="search"><input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch"></div>
                </td>
                <td>
                   <div class="search">
                     <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch">
                   </div>  
                </td>
                <td>
                   <div class="search">
                     <input type="text" ng-model="partquery" ng-change="mypartsearch()" class="inputsearch">
                   </div>    
                </td>
                <td>
                   <div class="search">
                     <input type="text" ng-model="titlequery" ng-change="mytitlesearch()" class="inputsearch">
                  </div>    
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td ng-repeat="totsal1 in totalsales">
                                <em title="{{totsal1.wk1dt}}">{{totsal1.wkno1}}</em>
                                <em title="{{totsal1.wk2dt}}">{{totsal1.wkno2}}</em>
                                <em title="{{totsal1.wk3dt}}">{{totsal1.wkno3}}</em>
                                <em title="{{totsal1.wk4dt}}">{{totsal1.wkno4}}</em>
                                <em title="{{totsal1.wk5dt}}">{{totsal1.wkno5}}</em>
                </td>
                <td ng-repeat="totsal2 in totalsales">
                                <em title="{{totsal2.wk1dt}}">{{totsal2.wkno1}}</em>
                                <em title="{{totsal2.wk2dt}}">{{totsal2.wkno2}}</em>
                                <em title="{{totsal2.wk3dt}}">{{totsal2.wkno3}}</em>
                                <em title="{{totsal2.wk4dt}}">{{totsal2.wkno4}}</em>
                                <em title="{{totsal2.wk5dt}}">{{totsal2.wkno5}}</em>
                </td>
     </tr>
    </tfoot>   
                                 <tbody>
                                  <tr ng-repeat="sales in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                           <td class="curser_pt_new trendicon" ng-if="compid == sesdata && sales.itemid_map_flag==1" ng-click="ReportFour($event)" data-id="{{sales.itemid}}" data-value="{{catid}}">{{sales.itemid}}</td>
                                            <td ng-if="compid == sesdata && sales.itemid_map_flag !=1">{{sales.itemid}}</td>
                                            <td ng-if="compid != sesdata">{{sales.itemid}}</td>
                                            <td>{{sales.sku}}</td>
                                            <td>{{sales.mypartno}}</td>
                                            <td>
                                                 <a ng-click="redirectToLink($event)" element="{{sales.itemid}}" style="cursor: pointer;">{{sales.title}}
                                                  </a>
                                            </td>

                                            <td class="alignment">{{sales.price}}</td>

                                            <td class="alignment">
                                                  <div class="tbl_txt">
                                                        <div class="price_text">{{sales.qtysold}}</div>
                                                  </div>
                                            </td>

                                            <td class="alignment">
                                                <div class="tbl_txt">
                                                    <div class="price_text" style="margin-right:10%">{{sales.totalsales}}</div>
                                                </div>
                                            </td>

                                            <td>
                                            <div class="tbl_img">
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'u'" class="price_up"></div>

                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'u'" class="price_up"></div>

                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'u'" class="price_up"></div>

                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'u'" class="price_up"></div>

                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'u'" class="price_up"></div>
                                            </div>
                                            <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                    <div class="price_text">{{sales.pricevw1}}</div>
                                                    <div class="price_text">{{sales.pricevw2}}</div>                              
                                                    <div class="price_text">{{sales.pricevw3}}</div>
                                                    <div class="price_text">{{sales.pricevw4}}</div>
                                                    <div class="price_text">{{sales.pricevw5}}</div>
                                            </div>
                                            </td>

                                            <td>
                                            <div class="tbl_img">
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'u'" class="price_up"></div>
                                                    
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'u'" class="price_up"></div>
                                                    
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'u'" class="price_up"></div>
                                                    
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'u'" class="price_up"></div>
                                                    
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == null" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'n'" class="price_wrong"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'e'" class="price_equal"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'd'" class="price_down"></div>
                                                    <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'u'" class="price_up"></div>

                                                    <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                        <div class="price_text">{{sales.qtysoldvw1}}</div>
                                                        <div class="price_text">{{sales.qtysoldvw2}}</div>                              
                                                        <div class="price_text">{{sales.qtysoldvw3}}</div>
                                                        <div class="price_text">{{sales.qtysoldvw4}}</div>
                                                        <div class="price_text">{{sales.qtysoldvw5}}</div>
                                                    </div>
                                            </div>
                                            </td>

                                        </tr>
                                       
                                    </tbody>  
                               </table>
                          </div>

            <div id="Pnopopupsalestrends" class="modal" style="display:none;">
                    <div class="modal-content" style="display:block; width:80%">
                       <div class="lable_normal table_top_form">
                       
                       <input id="showvalues" ng-model="checkboxModel.showvalues" ng-click="salestrendsshowvalues()" type="checkbox">Show values</div>
                        <span id="myPnopopupsubclose" ng-click="myPnopopupsubclose()" class="myPnopopupsubclose">&times;</span>
                    
                     <center><div id="NoRecord" style="display:none;">No Record Found</div></center>
                        <table id="MainContent_GV" cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
                                <th style="width:8%;">Competitor ID #</th>
                                <th style="width:8%;">Competitor Name</th>
                                <th style="width:8%;">Competitor SKU</th>
                                <th style="width:6%;">Current Price(in $)</th>
                                <th style="width:6%; min-width:0px;">Qty. Sold (5 wk)</th>
                                <th style="width:6%; min-width:100px;">Total Sales (5 wk in $)</th>
                                <th style="width:20%;">5 wk Price (In $)</th>
                                <th style="width:20%;">5 wk Qty.Sold</th>
                            </tr>
                            </thead>
                            <tfoot>
                               <tr>
                                    <td>
                                        <div class="search"></div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="search"></div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                   <!--  <td>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W5</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W4</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W3</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W2</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W1</em>
                                    </td>
                                    <td>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W5</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W4</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W3</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W2</em>
                                    <em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W1</em>
                                    </td> -->
                                     <td ng-repeat="totsal1 in totalsales">
                                                    <em title="{{totsal1.wk1dt}}">{{totsal1.wkno1}}</em>
                                                    <em title="{{totsal1.wk2dt}}">{{totsal1.wkno2}}</em>
                                                    <em title="{{totsal1.wk3dt}}">{{totsal1.wkno3}}</em>
                                                    <em title="{{totsal1.wk4dt}}">{{totsal1.wkno4}}</em>
                                                    <em title="{{totsal1.wk5dt}}">{{totsal1.wkno5}}</em>
                                    </td>
                                    <td ng-repeat="totsal2 in totalsales">
                                                    <em title="{{totsal2.wk1dt}}">{{totsal2.wkno1}}</em>
                                                    <em title="{{totsal2.wk2dt}}">{{totsal2.wkno2}}</em>
                                                    <em title="{{totsal2.wk3dt}}">{{totsal2.wkno3}}</em>
                                                    <em title="{{totsal2.wk4dt}}">{{totsal2.wkno4}}</em>
                                                    <em title="{{totsal2.wk5dt}}">{{totsal2.wkno5}}</em>
                                    </td>
                             </tr>
                             </tfoot>
                                <tbody>
                                <tr style="display: table-row;" ng-repeat="sales in saledata_inner">
                                    <td>{{sales.itemid}}</td>
                                    <td>{{sales.compname}}</td>
                                    <td>{{sales.sku}}</td>
                                    <td class="alignment">{{sales.price}}</td>
                                    <td class="alignment">
                                    <div class="tbl_txt">
                                     <div class="price_text">{{sales.qtysold}}</div>
                                    </div>
                                    </td>
                                    <td class="alignment">
                                        <div class="tbl_txt">
                                            <div class="price_text" style="margin-right:10%">{{sales.totalsales}}</div>
                                        </div>
                                    </td>
                                    <td>
                                    <div class="tbl_img">
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew1 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew2 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew3 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew4 == 'u'" class="price_up"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.pricew5 == 'u'" class="price_up"></div>
                                    </div>
                                    <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                            <div class="price_text">{{sales.pricevw1}}</div>
                                            <div class="price_text">{{sales.pricevw2}}</div>                              
                                            <div class="price_text">{{sales.pricevw3}}</div>
                                            <div class="price_text">{{sales.pricevw4}}</div>
                                            <div class="price_text">{{sales.pricevw5}}</div>
                                    </div>
                                    </td>
                                    <td>
                                    <div class="tbl_img">
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw1 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw2 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw3 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw4 == 'u'" class="price_up"></div>
                                            
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'n'" class="price_wrong"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'e'" class="price_equal"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'd'" class="price_down"></div>
                                            <div ng-hide="checkboxModel.showvalues" ng-if="sales.salesw5 == 'u'" class="price_up"></div>

                                            <div ng-show="checkboxModel.showvalues" class="tbl_txt">
                                                <div class="price_text">{{sales.qtysoldvw1}}</div>
                                                <div class="price_text">{{sales.qtysoldvw2}}</div>                              
                                                <div class="price_text">{{sales.qtysoldvw3}}</div>
                                                <div class="price_text">{{sales.qtysoldvw4}}</div>
                                                <div class="price_text">{{sales.qtysoldvw5}}</div>
                                            </div>
                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <br><br>
                                                    <div class="display_inline_blk align_center">
                                                        <p ng-if="saledata_inner.length == 0" class="nodata">No Competitor is Mapped to this Item ID</p>
                                                    </div>
                                               </div>
                                            </div>
                          
                    <div class="pagination pull-right pagi_master">
                        <ul>
                            <li ng-class="{disabled: currentPage == 0}">
                                <a href ng-click="prevPage()">« Prev</a>
                            </li>
                            <li ng-repeat="n in pagenos | limitTo:5"
                                ng-class="{active: n == currentPage}"
                            ng-click="setPage()">
                                <a href ng-bind="n + 1">1</a>
                            </li>
                            <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                <a href ng-click="nextPage()">Next »</a>
                            </li>
                        </ul>
                    </div>
          </div>
          </div></div>