<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
             <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
             <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>

</head>
<!-- <style>

 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
    .pagination ul>.active>a, .pagination ul>.active>span {
    color: #999999;
    cursor: default;
    }
    .alpha{
        "background-color" : "lightblue";
    }
  
</style>  -->
<body class="main" ng-controller="postController" ng-cloak>

<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
     <!-- <div class="container head_bg"> -->
   
    <?php $this->load->view('header.php'); ?>


<style type="text/css">
/*    div .ms-options{
        width:280%;
    }
*/
.slimtable-page-btn {
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    padding: 0 4px;
    border-radius: 3px;
    cursor:pointer;
}
.slimtable-page-btn:hover {
    background:#187ed5; 
}
.slimtable-page-btn.active {
    background:#187ed5; 
}
.slimtable-paging-btnsdiv{
    float:right;
}
/*.status_bar tr, .status_bar th, .status_bar td{
    width: 10%;
}*/

/*.status_bar thead th:nth-child(6){
    min-width: 600px;
    width: 600px;
    max-width: 600px;
}
.status_bar thead th:nth-child(2){
    min-width: 150px;
    width: 150px;
    max-width: 150px;
}
.status_bar thead th:nth-child(4){
    min-width: 150px;
    width: 150px;
    max-width: 150px;
}*/

ul,li { margin:0; padding:0; list-style:none;}
.label { color:#000; font-size:16px;}
.field1{
    display: inline-block;
    float: left;
    padding: 0 10px 0 0px;
}
.shwpopup
{
    display:none;
    width: 50%;
    margin: 50%;
    position: fixed;
    z-index: 1;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
    left: 0;
    top: 0;
}
.field{
    width:auto;
}
.ckeckclass
{
    margin:2px;
}
.divTable{
    float: left;
    width: 70px;
    height: 40px;
    border-right: 1px solid #e1e1e1;
    padding-top: 28px;
    padding-left: 7px;
    
}
.lastdivTable{
    float: left;
    width: 70px;
    padding-top: 28px;
    padding-left: 7px;
    
}
.paging-nav {
  text-align: right;
  padding-top: 2px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #5D85B2;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

tfoot
{
text-align: center !important;
display: table-row-group !important;
}

.paging-nav,
#tblrawresult {
 
  margin: 0 auto;
  font-family: Arial, sans-serif;
}

.select {
    padding: 2px;
    float: right;
    width: 155px;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}

.selectpage {
    padding: 2px;
    width: 76;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}



/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 10% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 30%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: #fff;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #2d8bd5;
    text-decoration: none;
    cursor: pointer;
}
.itemIDCell
{
    margin:0;
}

@media only screen and (max-width: 1500px) {
    
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }



/**********autocomplete suggestion***************/
#searchResult{
 /*list-style: none;
 display :block;
 padding: 0.1%;
 width: 6.9%;
 position: absolute;
 margin: 0;
 margin-left: 6.7%*/
   /* padding: 0;
    border: 1px solid black;
    max-height: 7.5em;
    overflow-y: auto;
    width: 8.1%;
    position: absolute;
    margin-left: 8.2%;*/
        border: 1px solid #aaa;
    overflow-y: auto;
    width: 172px;
    position: absolute;
    margin-left: 143px;
    padding-bottom: 0px;
    min-height: 10px;
    max-height: 150px;
}

#searchResult li{
 background: white;
 padding: 4px;
cursor: pointer;
    list-style: none;
    border-bottom: 1px solid #ededed;
}

#searchResult li:hover{
    background: #f1f1f1;
}
/**************/

input[type=radio] + label {
  color: #ccc;
  font-style: italic;
} 
input[type=radio]:checked + label {
  color: #f00;
  font-style: normal;
}
/*#competitorname option[value='? string:91 ?']{
    display: none;
}*/

</style>
    <div class="container header_bg_clr posi_rela">
        <div class="wrapper">
            <div class="header_main">
                <div class="head_menu">
             <?php echo "<h2>".$screen."</h2>" ;?><!-- <span style="padding-left:40%; color:red; font-size:14px"></span> -->
                </div>
            </div>
        </div>
    </div>  
    <div id="msg" style="display: none;"></div>
             
    </div> 
    
                     
 
    <div class="container">
        <div class="wrapper">
        <div class="content_main_all">
                     <div class="display_inline_blk">
                         <span class="week">Week : <?php echo date("W")-1; ?></span>
                    </div>
        <form id="compareform" ng-submit="submitForm()">
            <div class="form_head">
                <div class="field">

                    <div class="lable_normal" >
                        Competitor Details <span style="color:red; font-size:16px;">*</span>  : 
                    </div>

                      <select name="competitorname[]"  class="select" ng-model="competitorname" id="competitorname">
                            <option value="">-Select-</option>
                            <?php if(count($comptitornameandid) >= 1){
                              foreach($comptitornameandid as $value){ ?>
                                <option value="<?php echo $value->CompID;?>"><?php echo $value->CompetitorName;?></option>  
                            <?php }
                            } ?>
                      </select> 
                    
                </div>

                <div class="field">
                     <button value="Search" class="button_search" id="btnsearch" type="submit">Show</button>
                </div>
                <div class="field" >
                    <input type="radio" class="radiobt" ng-click="submitForm()" id="sell12" name="Sell" value="1" checked>
                    <label for="sell12">I Sell</label> 
                </div>
                <div class="field">
                    <input type="radio" class="radiobt" ng-click="dontsell()" id="dontsell12" name="Sell" value="0">
                    <label for="dontsell12">I Dont Sell</label>
                </div>
            </div>
             </form>
              <center>   <div  class="error_msg_all"  style=" display:none" id="msg_id">Please select Competitor</div> </center>
            <div class="button_right">
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                              <input type="button" style="margin: -5px 4px 0px 1px;" class="export_button" ng-click="exportDataExcel()" value="Export to Excel"/>
                            </a>
                      </li>
                 </div> 
        <div class="itm_lst">
        <p style="margin: 13px 33px 0px 23px;">Items Count:</p>
        <p style="font-size: 20px; color: #00886d;margin: 0 15px 0px -28px;">{{compcomparecount}}</p>
        </div>

 

           
            <div id="firstgrid" style="display: none;">
                <div class="plan_list_view">
                    <div class="clear"></div>


                    <!-- first table start-->
                        <table cellspacing="0" border="1" class="table price_smart_grid_new">
                                <thead>
                                    <tr>
                                        <th class="curser_pt" ng-click="sortBy('CompItemID')">Comp ItemID</th>
                                        <th class="curser_pt" ng-click="sortBy('CompSKU')">Comp SKU</th>
                                        <th class="curser_pt" ng-click="sortBy('Mypartno')">My Part#</th>
                                        <th class="curser_pt" ng-click="sortBy('MyItemID')">My ItemID</th>
                                        <th class="curser_pt" ng-click="sortBy('MySKU')">My SKU</th>
                                        <th class="curser_pt" ng-click="sortBy('MyTitle')" >My Title</th>
                                        <th class="curser_pt" ng-click="sortBy('MyPrice')">My Price(in $)</th>
                                        <th class="curser_pt" ng-click="sortBy('MyQtysold')">My Qty Sold (Last Wk)</th>
                                        <th class="curser_pt" ng-click="sortBy('CompPrice')">Comp Price(in $)</th>
                                        <th class="curser_pt" ng-click="sortBy('CompSold')">Comp Sold (Last Wk)</th>



                                    </tr>
                                   <tr class="tab1table">
                                       <td>
                                            <div class="search">
                                              <input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch" placeholder="Comp ItemID">
                                            </div> 
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="compskuquery" ng-change="compskusearch()" class="inputsearch" placeholder="Comp SKU">
                                            </div> 
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="Mypartnoquery" ng-change="Mypartnosearch()" class="inputsearch" placeholder="Mypartno">
                                            </div> 
                                       </td>
                                       <td>
                                          <div class="search">
                                              <input type="text" ng-model="MyItemIDquery" ng-change="MyItemIDsearch()" class="inputsearch" placeholder="MyItemID">
                                            </div> 
                                       </td>
                                       <td>
                                            <div class="search">
                                              <input type="text" ng-model="MySKUquery" ng-change="MySKUsearch()" class="inputsearch" placeholder="MySKU">
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="search">
                                              <input type="text" ng-model="MyTitlequery" ng-change="MyTitlesearch()" class="inputsearch" placeholder="MyTitle">
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="search">
                                              <input type="text" ng-model="MyPricequery" ng-change="MyPricesearch()" class="inputsearch" placeholder="MyPrice">
                                            </div> 
                                        </td>
                                         <td>
                                            <div class="search">
                                              <input type="text" ng-model="MyQtysoldquery" ng-change="MyQtysoldsearch()" class="inputsearch" placeholder="MyQtysold">
                                            </div> 
                                         </td>
                                         <td>
                                           <div class="search">
                                              <input type="text" ng-model="CompPricequery" ng-change="CompPricesearch()" class="inputsearch" placeholder="CompPrice">
                                            </div> 
                                         </td>
                                         <td>
                                           <div class="search">
                                              <input type="text" ng-model="CompSoldquery" ng-change="CompSoldsearch()" class="inputsearch" placeholder="CompSold">
                                            </div> 
                                         </td>
                                      </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                        <td>{{item.CompItemID}}</td>
                                        <td>{{item.CompSKU}}</td>
                                        <td>{{item.Mypartno}}</td>
                                        <td>{{item.MyItemID}}</td>
                                        <td>{{item.MySKU}}</td>
                                         <td id="title" headers="Title"><a ng-click="redirectToLink($event)" element="{{item.MyItemID}}" style="cursor: pointer;" >{{item.MyTitle}}</a></td>
                                    
                                        <td>{{item.MyPrice}}</td>
                                        <td>{{item.MyQtysold}}</td>
                                        <td>{{item.CompPrice}}</td>
                                        <td>{{item.CompSold}}</td>
                                    </tr>
                                </tbody>
                    </table>


                        <div class="pagination pull-right pagi_master">
                                <ul>
                                    <li ng-class="{disabled: currentPage == 0}">
                                        <a href ng-click="prevPage()">« Prev</a>
                                    </li>
                                    <li ng-repeat="n in pagenos | limitTo:5"
                                        ng-class="{active: n == currentPage}"
                                        ng-click="setPage()">
                                        <a href ng-bind="n + 1">1</a>
                                    </li>
                                    <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                        <a href ng-click="nextPage()">Next »</a>
                                    </li>
                                </ul>
                            </div>

                    <!-- first table end -->



                </div>
                </div>  
                
                     <!-- pagination ui part -->

                       

                
            
            <div id="secondgrid" style="display: none;">
                              <table id="location" cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
                                <th class="curser_pt" ng-click="sortBysub('itemid')">Item ID</th>
                                <th class="curser_pt" ng-click="sortBysub('sku')">SKU</th>
                                <th class="curser_pt" ng-click="sortBysub('category')">Category</th>
                                <th ng-click="sortBysub('title')">Title</th>
                                <th class="curser_pt" ng-click="sortBysub('price')">Price (in $)</th>
                                <th class="curser_pt" ng-click="sortBysub('Qtysold')">Qty Sold (Inc)</th>
                                <th class="curser_pt" ng-click="sortBysub('QuantitySold')">Qty Sold (Last Week)</th>
                            </tr>
                            <tr class="tab1table">
                                       <td>
                                            <div class="search">
                                              <input type="text" ng-model="querysub" ng-change="mysearchsub()" class="inputsearch" placeholder="ItemID">
                                            </div> 
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="subskuquery" ng-change="subskusearch()" class="inputsearch" placeholder="SKU">
                                            </div> 
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="subcategoryquery" ng-change="subcategorysearch()" class="inputsearch" placeholder="Category">
                                            </div> 
                                       </td>
                                       <td>
                                          <div class="search">
                                              <input type="text" ng-model="subtitlequery" ng-change="subtitlesearch()" class="inputsearch" placeholder="Title">
                                            </div> 
                                       </td>
                                       <td>
                                            <div class="search">
                                              <input type="text" ng-model="subpricequery" ng-change="subpricesearch()" class="inputsearch" placeholder="Price">
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="search">
                                              <input type="text" ng-model="subQtysoldquery" ng-change="subQtysoldsearch()" class="inputsearch" placeholder="Qtysold(inc)">
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="search">
                                              <input type="text" ng-model="subQuantitySoldquery" ng-change="subQuantitySoldsearch()" class="inputsearch" placeholder="QuantitySold(last wk)">
                                            </div> 
                                        </td>
                        
                                     
                                 </tr>
                    </thead>
                        <tr ng-repeat="items in pagedItemssub[currentPagesub] | filter:search | orderBy:sortKeysub:reversesub">
                                    <td>{{items.itemid}}</td>
                                    <td>{{items.sku}}</td>
                                    <td>{{items.category}}</td>
                                    <td>
                                    <a ng-click="redirectToLink($event)" element="{{items.itemid}}" style="cursor: pointer;">{{items.title}}
                                    </a></td>
                                    <td>{{items.price}}</td>
                                    <td>{{items.Qtysold}}</td>
                                    <td>{{items.QuantitySold}}</td>
                        
                    </tr>
                </table>

                        <div class="pagination pull-right pagi_master">
                                <ul>
                                    <li ng-class="{disabled: currentPagesub == 0}">
                                        <a href ng-click="prevPagesub()">« Prev</a>
                                    </li>
                                    <li ng-repeat="n in pagenossub | limitTo:5"
                                        ng-class="{active: n == currentPagesub}"
                                        ng-click="setPagesub()">
                                        <a href ng-bind="n + 1">1</a>
                                    </li>
                                    <li ng-class="{disabled: currentPagesub == pagedItemssub.length - 1}">
                                        <a href ng-click="nextPagesub()">Next »</a>
                                    </li>
                                </ul>
                            </div>



                        <div style="text-align:center;display: none">
                        <button class="button_add" value="" >Approve</button>
                    <!-- <input value="Approve" style="width:4%;background-color:#00763C;" /> -->
                    </div>
            </div>
        </div>
    </form>
</div>
</div>
</div>



                 <?php $this->load->view('footer.php'); ?>


<script>
    $('#btnsearch').click(function(e){ 
      if ($('#competitorname').val() == "") {
                 $("#msg_id").css("display","block");
                 var competitorname=document.getElementById('competitorname').value;
                   if(competitorname == ""){
                        document.getElementById('competitorname').style.borderColor = "red";
                       // return false;
                        e.preventDefault();
                    }
                    else
                    {
                       document.getElementById('competitorname').style.borderColor = "";
                    }
                   

            }
            else
            {   $("#msg_id").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                 
            }
        });
   /* $('#sell12').click(function(e){ 
      if ($('#competitorname').val() == "") {
                 $("#msg_id").css("display","block");
                 document.getElementById('competitorname').style.borderColor = "red";
                  e.preventDefault();
            }
            else
            {   $("#msg_id").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                
            }
        });*/
   /*  $('#dontsell12').click(function(e){ 
      if ($('#competitorname').val() == "") {
                 $("#msg_id").css("display","block");
                   document.getElementById('competitorname').style.borderColor = "red";
                  e.preventDefault(); 
            }
            else
            {   $("#msg_id").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                
            }
        });*/



</script>

<script>
             var sortingOrder = 'CompItemID';
             var sortingOrdersub = 'itemid';
            var postApp     = angular.module('postApp', []);
            postApp.controller('postController', function($scope, $http, $timeout,$window,$filter) {
                    $scope.sortingOrder = sortingOrder;
                    $scope.sortingOrdersub = sortingOrdersub;
                    $scope.reverse = false;
                    $scope.reversesub = false;
                    $scope.filteredItems = [];
                    $scope.filteredItemssub = [];
                    $scope.groupedItems = [];
                    $scope.groupedItemssub = [];
                    $scope.itemsPerPage = 25;
                    $scope.itemsPerPagesub = 25;
                    $scope.pagedItems = [];
                    $scope.pagedItemssub = [];
                    $scope.pagenos = [];
                    $scope.pagenossub = [];
                    $scope.compcompare = '';
                    $scope.compcompare_dontsell = '';
                    $scope.currentPage = 0;
                    $scope.currentPagesub = 0;


            //Click the button to open an new browser window that is 1200px wide and 600px tall
                 $scope.redirectToLink = function (obj) {
                       var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                    };


            $scope.submitForm =function(){
                var radio  = $("#compareform input[type='radio']:checked").val();
                if ($('#competitorname').val() == "") {
                           $("#msg_id").css("display","block");
                           document.getElementById('competitorname').style.borderColor = "red";
                           return false;
                    }
                else{   
                      $("#msg_id").css("display","none");
                      document.getElementById('competitorname').style.borderColor = "";
                    }
                $scope.isLoading =true;
                
                 if(radio == 0){
                    $scope.dontsell();
                    return false;
                 }
                 else
                 {
                $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Competitorcompare/Competitorcompare_result_sell',
                  data    : {'competitor':$scope.competitorname,'radio':radio},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    $('#firstgrid').css('display','block');
                    $('#secondgrid').css('display','none');
                     $scope.isLoading =false;
                    $scope.compcompare = data;
                    //console.log($scope.compcompare);
                    $scope.compcomparecount =$scope.compcompare.length;
                    var exportdata_exc = [];
                      $scope.compcompare.forEach(function (part) {                     
                            exportdata_exc.push({'CompItemID' : part.CompItemID,
                                                 'CompSKU':part.CompSKU,
                                                 'Mypartno' : part.Mypartno,
                                                 'MyItemID' : part.MyItemID,
                                                 'MySKU' : part.MySKU,
                                                 'MyTitle' : part.MyTitle,
                                                 'MyPrice (In $)': part.MyPrice,
                                                 'MyQtysold (last wk)':part.MyQtysold,
                                                'CompPrice(In $)' : part.CompPrice,
                                                 'CompSold (last wk)' : part.CompSold
                                               });

                        });
                                   $scope.ForExcelExport = exportdata_exc;
                                  
                                      //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                      for(var attr in item) {
                                        if(attr == "CompItemID") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.compskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                    for(var attr in item) {
                                    if(attr == "CompSKU") {
                                            if (searchMatch(item[attr], $scope.compskuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.Mypartnosearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Mypartno") {
                                            if (searchMatch(item[attr], $scope.Mypartnoquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.MyItemIDsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                    for(var attr in item) {
                                     if(attr == "MyItemID") {
                                            if (searchMatch(item[attr], $scope.MyItemIDquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.MySKUsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                          for(var attr in item) {
                                            if(attr == "MySKU") {
                                            if (searchMatch(item[attr], $scope.MySKUquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                            $scope.MyTitlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                          for(var attr in item) {
                                            if(attr == "MyTitle") {
                                            if (searchMatch(item[attr], $scope.MyTitlequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.MyPricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                          for(var attr in item) {
                                            if(attr == "MyPrice") {
                                            if (searchMatch(item[attr], $scope.MyPricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.MyQtysoldsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                          for(var attr in item) {
                                            if(attr == "MyQtysold") {
                                            if (searchMatch(item[attr], $scope.MyQtysoldquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                            
                             $scope.CompPricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                          for(var attr in item) {
                                            if(attr == "CompPrice") {
                                            if (searchMatch(item[attr], $scope.CompPricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.CompSoldsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compcompare, function (item) {
                                          for(var attr in item) {
                                            if(attr == "CompSold") {
                                            if (searchMatch(item[attr], $scope.CompSoldquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};
                            $scope.showLoader = false; //Loader Image 

                    $scope.exportDataExcel = function () {
                        //console.log('hio');
                        var mystyle = {
                            sheetid: 'Competitor Compare Reports I Sell',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_Competitor_Compare_Reports_I_Sell.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    };
                    

                  })
                  .catch(function (err) {
                       var msg =err.status;
                        //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                     
                                        $scope.isLoading = false;
                                    });
                         
                        })
                }//else condition end
            }

            $scope.dontsell =function(){
                    var radio  = $("#compareform input[type='radio']:checked").val();
                    if ($('#competitorname').val() == "") {
                           $("#msg_id").css("display","block");
                           document.getElementById('competitorname').style.borderColor = "red";
                           return false;
                         }
                   else{   
                          $("#msg_id").css("display","none");
                          document.getElementById('competitorname').style.borderColor = "";
                        }
                    $scope.isLoading =true;
                $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Competitorcompare/Competitorcompare_result_dontsell',
                  data    : {'competitor':$scope.competitorname,'radio':radio},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    //console.log(data);
                    $('#secondgrid').css('display','block');
                    $('#firstgrid').css('display','none');
                     $scope.isLoading =false;
                    $scope.compcompare_dontsell = data;
                    $scope.compcomparecount =$scope.compcompare_dontsell.length;
                    var exportdata_exc_sub = [];
                      $scope.compcompare_dontsell.forEach(function (part) {                     
                            exportdata_exc_sub.push({'itemid' : part.itemid,
                                                 'sku':part.sku,
                                                 'Category' : part.category,
                                                 'Title' : part.title,
                                                  'Price(In $)' : part.price,
                                                 'Qtysold (Inc)' : part.Qtysold,
                                                  'QuantitySold (last wk)' : part.QuantitySold
                                               });

                        });
                                   $scope.ForExcelExportsub = exportdata_exc_sub;
                                      //pagination part start 
                        // *********************

                         $scope.sortBysub = function(propertyName) {
                                $scope.sortKeysub = propertyName;   //set the sortKey to the param passed
                                $scope.reversesub = ($scope.sortKeysub === propertyName) ? !$scope.reversesub : false;
                                $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortKeysub, $scope.reversesub);
                                $scope.pagedItemssub = ""; 
                                $scope.groupToPagessub();
                            };
                                var searchMatchsub = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearchsub = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "itemid") {
                                            if (searchMatchsub(item[attr], $scope.querysub)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };
                             $scope.subskusearch = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "sku") {
                                            if (searchMatchsub(item[attr], $scope.subskuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };
                             $scope.subcategorysearch = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "category") {
                                            if (searchMatchsub(item[attr], $scope.subcategoryquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };
                             $scope.subtitlesearch = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "title") {
                                            if (searchMatchsub(item[attr], $scope.subtitlequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };
                             $scope.subpricesearch = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "price") {
                                            if (searchMatchsub(item[attr], $scope.subpricequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };
                             $scope.subQtysoldsearch = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "Qtysold") {
                                            if (searchMatchsub(item[attr], $scope.subQtysoldquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };
                             $scope.subQuantitySoldsearch = function () {
                                $scope.filteredItemssub = $filter('filter')($scope.compcompare_dontsell, function (item) {
                                      for(var attr in item) {
                                        if(attr == "QuantitySold") {
                                            if (searchMatchsub(item[attr], $scope.subQuantitySoldquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrdersub !== '') {
                                    $scope.filteredItemssub = $filter('orderBy')($scope.filteredItemssub, $scope.sortingOrdersub, $scope.reversesub);
                                }
                                $scope.currentPagesub = 0;
                                // now group by pages
                                $scope.groupToPagessub();
                            };


                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPagessub = function () {
                            $scope.pagedItemssub = [];
                            for (var i = 0; i < $scope.filteredItemssub.length; i++) {
                                if (i % $scope.itemsPerPagesub === 0) {
                                    $scope.pagedItemssub[Math.floor(i / $scope.itemsPerPagesub)] = [ $scope.filteredItemssub[i] ];
                                    } else {
                                    $scope.pagedItemssub[Math.floor(i / $scope.itemsPerPagesub)].push($scope.filteredItemssub[i]);
                                    //console.log('test');
                                }
                                }
                            };
                            
                            $scope.rangesub = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenossub = ret;
                                return ret;
                            };
                            
                            $scope.prevPagesub = function () {
                                if ($scope.currentPagesub > 0) {
                                    $scope.currentPagesub--;
                                }
                            };
                            
                            $scope.nextPagesub = function () {
                                if ($scope.currentPagesub < $scope.pagedItemssub.length - 1) {
                                    $scope.currentPagesub++;
                                }
                            };
                            
                            $scope.setPagesub = function () {
                                $scope.currentPagesub = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearchsub();

                            // change sorting order
                            $scope.sort_bysub = function(newSortingOrder) {
                                if ($scope.sortingOrdersub == newSortingOrder)
                                    $scope.reversesub = !$scope.reversesub;

                                $scope.sortingOrdersub = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reversesub)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPagesub', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItemssub.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.rangesub(start, $scope.pagedItemssub.length);
                              }
                            });
                            $scope.rangesub($scope.pagedItemssub.length);
                        //pagination part end
                            $scope.searchsub = {};
                            $scope.showLoader = false; //Loader Image 

                $scope.exportDataExcel = function () {
                        //console.log('hio');
                        var mystyle = {
                            sheetid: 'Competitor Compare Reports Dont Sell',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_Competitor_Compare_Reports_I_Dont_Sell.xls",?) FROM ?',[mystyle, $scope.ForExcelExportsub]);
                };

                  })
                  .catch(function (err) {
                       var msg =err.status;
                        //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                     
                                        $scope.isLoading = false;
                                    });
                         
                        })
                }
                  
            });
            
            
        </script>

        <script type="text/javascript">
      
        $(document).ready(function () {
               

            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
        

    </script>

    <script>
         $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");  
        });
    </script>

 </body>
</html>