<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <meta charset="utf-8"/>
    <title>Romaine Electric : Forgot Password</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_romaine.css" />
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
 </head>
<style>
.forget_pass_msg{
	font-style: italic;
}
.text_aligns{
	    margin: 30px 0px 0px 0px;
}
.btn_aligns{
    margin: 15px 0px 30px 115px;
}
.err_modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
  .forget_pass_msg p{
  	font-style: initial;
    padding: 10px 0;
    line-height: 22px;
  }
</style>
<body ng-controller="myController" ng-cloak>

<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>


<div class="container head_bg">
        <div class="head">
                <div class="logo">
                    <a href='<?php echo base_url();?>DashBoard/index'> <img src="<?php echo base_url().'assets/';?>img/dive_logo_small.jpg"></a>
                </div>
                <div class="nav nav_bg">
                <ul>    
                </ul>
        </div>
        </div>
    </div>

     		<div id="msg" style="display: none;"></div>			
    <div class="bg">
    
        <div class="container">

            <div class="wrapper">
                <div class="login_main" id="login_main">
                	
	                    <h1 style="font-size: 22px; color: #ec3a3a; font-family: 'Open Sans', sans-serif;">Forgot Password</h1>
		                    <div class="forget_pass_msg">
			                    <p>Please enter your email ID, to receive a link, to reset the password</p>
			                    <!-- <p>2.Dive will send a verification link to that email.</p> -->
		                    </div>

	                    <form ng-submit="emailsubmit()">
	                     	<div class="ps_main text_aligns" >
	                            <input name="email" type="email" placeholder="Email" ng-model="email" value="" class="ps_admin" required/>
	                        </div>
	                        <div class="btn_aligns">
	                            <button type="submit" class="submit_btn" style="background: #26a69a;">Submit</button>
	                        </div>
	                    </form>
                   
                </div>
            </div>
        </div>
    </div>
     <script src="<?php echo base_url().'assets/';?>js/jquery-1.11.3.min.js"></script>
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script>
    	 var app = angular.module('myApp',[]);
        app.controller('myController',function($scope,$http){

        	//get_email_id
        	$scope.emailsubmit = function(){
        		$scope.isLoading =true;
        		$http({
        			method : 'POST',
        			url    :'<?php echo base_url();?>index.php/ForgetPassword/check_email',
        			data   : {'emailid':$scope.email},
        			headers:{'Content-Type': 'application/x-www-form-urlencoded'}
        			})
        		.success(function(data){
        			$scope.emailresponse = data;
        			$scope.isLoading =false;
        			if(data != 'failure'){
        				//alert('Email send successfully');
        						$http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                     data    : 5
                                    })
                                 .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
        			}
        			else{
        				//alert('Email ID is not Available');
        				$http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : 002
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');                  
                                    $scope.isLoading = false;
                                });
        			}
        		})
        		 .catch(function (err) {
                                  var msg =err.status;
                             //failure msg(db error)
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block');                  
                                    $scope.isLoading = false;
                                });
                 });    

        	}

        });

    </script>
    <script>
            //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            window.location.replace('<?php echo base_url();?>');
        });
       </script>

</body>
</html>