<!DOCTYPE html>
<html ng-app="postApp">
<head>
<title><?php echo $title[0]->SellerName;?> :   <?php echo $screen;?>
</title>
</head>
<link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>

      <style>
    .excludemenu{color:red;}
    .generalexcludedynamic{color:#3498db;}
    .curser_pt {
                cursor: pointer;
                background: url(assets/img/sort_icon.png) no-repeat 5px center;
                padding: 0 20px;
            }
    </style>
    <div id="isLoading" style="display : none;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" style="margin-left:50%;margin-top:25%"/></div>
<body>
    
       <?php 
              $this->load->view('header.php'); 
       ?>

        

        <div class="container_bg header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?>
                 
                </div>
            </div>
        </div>
                      <div id="msg" style="display: none;"></div>
        
        <div class="container">
        <form id="competitorform" method="post"> 
           <!-- start -->
            <div class="wrapper">
            <div class="content_main_all">
                <div class="main_form_exclusion">
                <div class="field">
                    <div class="lable_normal_valign">
                        Filter Id
                    </div>
                    <div class="input_lable_disabled">
                        <?php 
                            if(count($this->uri->segment(3)) == 1){
                        ?>
                            <input type="hidden" name="exclustion_id" value="<?php echo $this->uri->segment(3);?>">
                            <span><?php echo $this->uri->segment(3);?></span>
                        <?php }else{ ?>
                            <input type="hidden" name="exclustion_id" value="Ex-<?php echo strtotime(date("Y-m-d H:i:s"));?>">
                            <span>Ex-<?php echo strtotime(date("Y-m-d H:i:s"));?></span>
                        <?php } ?>
                    </div>
                    <div class="jsonresult" style="padding-left:300%; display:none;width:100%;margin-top: -12.5%;"></div>
                </div>
                    <div class="field_config">
                        <div class="lable_normal">
                            Filter Description
                        </div>
                        <?php 
                            if(count($this->uri->segment(3)) == 1){
                        ?>
                        <?php if(count($fetch_exclusion) >= 1){?>
                        <input placeholder="Enter a name for the Filter" name="exclusion_name" type="text" class="input_main_exclu" value="<?php echo $fetch_exclusion[0]->exclusionname;?>">
                        <?php } ?>
                        <?php }else{ ?>
                            <input placeholder="Enter a name for the Filter" name="exclusion_name" type="text" class="input_main_exclu" value="">
                        <?php } ?>
                    </div>
                    </div>
                <div class="clear"></div>
                
                <?php //echo $this->input->get('id'); ?>
                
                <div id="accordion">
                    <h2 class="accordion_exclution">General</h2>
                    <div class="wrapper_inner">

                        <div class="catagory">
                            <div class="field_full">
                                <div class="lable_normal">
                                    Condition
                                </div>
                               <select class="select_full general_select" name="general_select_combo">
                               <option value="">Select</option>
                                <?php 
                                if(count($general_dropdown) > 1){
                                    foreach($general_dropdown as $generalkey => $generalvalue){ ?>
                                    <?php if(trim($generalvalue->meta_value) == 'Remanufactured'){ ?>
                                        <option value="<?php echo 'Remanufacture';?>"><?php echo $generalvalue->meta_value;?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo trim($generalvalue->meta_value);?>"><?php echo $generalvalue->meta_value;?></option>
                                     <?php } ?>
                                <?php } }?>
                                </select>
                                <label type="button" class="button_normal_onoff excludemenu generalexcludedyon" value="Search">Exclude</label>
                                <label type="button" class="button_normal_onoff includemenu generalexcludedyoff" style="display: none;" value="Search">Include</label>
                                <label class="switch">
                                    <input type="checkbox" id="general_exclude_id" name="general_exclude_id" value="0"/>
                                    <div class="slider round general_exclude"></div>
                                </label>
                            </div>
                            <div class="clear"></div>
                            <div class="textboxlist">
                                <div style="font-weight:bold;" class="textboxlist-autocomplete-placeholder">Below are the Inclusion list for General</div>
                                <ul class="textboxlist-bits general_label_li_append">
                                    
                                </ul>   
                                <div class="textboxlist-autocomplete" style="width: 400px;">
                                    <ul class="textboxlist-autocomplete-results">
                                    </ul>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="field">
                                <?php if(count($this->uri->segment(3)) == 1){?>
                                <input id="field_checked_value1" class="field_update_ver_chk" name="location_name" type="checkbox" value="<?php $fetch_exclusion[0]->location_us;?>" <?php if($fetch_exclusion[0]->location_us == 1){echo 'checked';}?>>
                                <?php }else{ ?>
                                <input id="field_checked_value1" class="field_update_ver_chk" name="location_name" type="checkbox" value="1" >
                                <?php } ?>
                                Location US
                            </div>
                            <div class="field">
                                <?php if(count($this->uri->segment(3)) == 1){?>
                                <input id="field_checked_value2" class="field_update_ver_chk" name="rated_name" type="checkbox" value="<?php echo $fetch_exclusion[0]->toprated;?>" <?php if($fetch_exclusion[0]->toprated == 1){echo 'checked';}?>>
                                <?php }else{ ?>
                                <input id="field_checked_value2" class="field_update_ver_chk" name="rated_name" type="checkbox" value="1">
                                <?php } ?>
                                Top Rated Only
                            </div>
                            <div class="field">
                                <?php if(count($this->uri->segment(3)) == 1){?>
                                <input id="field_checked_value3" class="field_update_ver_chk" name="shipping_name" type="checkbox" value="<?php echo $fetch_exclusion[0]->free_shipping;?>" <?php if($fetch_exclusion[0]->free_shipping == 1){echo 'checked';}?>>
                                <?php }else{ ?>
                                <input id="field_checked_value3" class="field_update_ver_chk" name="shipping_name" type="checkbox" value="1">
                                <?php } ?>
                                Free Shipping
                            </div>
                            <div class="field">
                                <?php if(count($this->uri->segment(3)) == 1){?>
                                <input id="field_checked_value4" class="field_update_ver_chk" name="active_only" type="checkbox" value="<?php echo $fetch_exclusion[0]->show_active;?>" <?php if($fetch_exclusion[0]->show_active == 1){echo 'checked';}?>>
                                <?php }else{ ?>
                                <input id="field_checked_value4" class="field_update_ver_chk" name="active_only" type="checkbox" value="1">
                                <?php } ?>
                                Show only Active
                            </div>
                        </div>

                    </div>
                    <h2 class="accordion_exclution">Part Details</h2>
                    <div class="wrapper_inner">
                        <div class="form_head">
   
                            <div class="field_12">
                                <input placeholder="Search for Part Number" type="text" class="input_left search_part_number">
                                <span class="or_small">(OR)</span>
                                <button value="Search" class="button_upload">Upload</button>
                                <label type="button" class="button_normal_onoff excludemenu partsexcludedyon" id="part_excbtn" value="Search">Exclude</label>
                                <label type="button" class="button_normal_onoff includemenu partsexcludedyoff" id="part_incbtn" style="display: none;" value="Search">Include</label>
                                <label class="switch">
                                    <input type="checkbox" id="parts_exclude_id" name="parts_exclude_value" value="0" />
                                    <div class="slider round parts_exclude"></div>
                                </label>
                            </div>
                            <!--<button value="add" class="button_add" type="submit">Add Part No</button>-->
                            <div class="clear"></div>
                            <div class="textboxlist">
                                <div style="font-weight:bold;" class="textboxlist-autocomplete-placeholder">Below are the Inclusion list for Part Number</div>
                                <ul class="textboxlist-bits parts_label_li_append">
                                    
                                </ul>
                                <div class="textboxlist-autocomplete" style="width: 400px;">
                                <ul class="textboxlist-autocomplete-results"></ul>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="accordian_parts_search_result">
                             </div>
                        </div>
                    </div>
                    <h2 class="accordion_exclution">Category</h2>

                    <div class="wrapper_inner">

                        <div class="catagory">
                            <div class="field_12">
                                <input placeholder="Search for Category" type="text" class="input_left search_category_number">
                                <label type="button" class="button_normal_onoff excludemenu categoryexcludedyon" id="excbtn" value="Search">Exclude</label>
                                <label type="button" class="button_normal_onoff includemenu categoryexcludedyoff" id="incbtn" style="display: none;" value="Search">Include</label>
                                <label class="switch">
                                     <input type="checkbox" id="category_exclude_id" name="category_exclude_value" value="0" />
                                     <div class="slider round category_exclude"></div>
                                </label>
                            </div>
                            
                            <div class="clear"></div>
                            <div class="textboxlist">
                                <div style="font-weight:bold;" class="textboxlist-autocomplete-placeholder">Below are the Inclusion list for Category</div>
                                <ul class="textboxlist-bits category_label_li_append">
                                    
                                </ul>
                                    <div class="textboxlist-autocomplete" style="width: 400px;">
                                    <ul class="textboxlist-autocomplete-results"></ul></div>
                            </div>
                            <div class="clear"></div>
                            <div class="accordian_category_search_result">
                            </div>
                        </div>
                    </div>
                    <h2 class="accordion_exclution">Brand</h2>
                    <div class="wrapper_inner">

                        <div class="catagory">
                            <div class="field_12">
                                <input placeholder="Search for Brands" type="text" class="input_left search_brand_name">
                                <label type="button" class="button_normal_onoff excludemenu brandexcludedyon" value="Search">Exclude</label>
                                <label type="button" class="button_normal_onoff includemenu brandexcludedyoff" style="display: none;" value="Search">Include</label>
                                <label class="switch">
                                    <input type="checkbox" id="brand_exclude_id" name="brand_exclude_value" value="0" />
                                     <div class="slider round brand_exclude"></div>
                                </label>
                            </div>
                            <div class="clear"></div>
                            <div class="textboxlist">
                                <div style="font-weight:bold;" class="textboxlist-autocomplete-placeholder">Below are the Inclusion list for Brands</div>
                                <ul class="textboxlist-bits brand_label_li_append">
                                
                                </ul>
                                    <div class="textboxlist-autocomplete" style="width: 400px;">
                                        <ul class="textboxlist-autocomplete-results"></ul>
                                    </div>
                            </div>
                            <div class="clear"></div>
                            <div class="accordian_Brand_search_result">
                             </div>
                        </div>

                    </div>
                    <h2 class="accordion_exclution">Title Like</h2>
                    <div class="wrapper_inner">
                        <div class="form_head" style="padding-bottom:20px;">
                            <div class="field_12">
                                <div class="lable_normal">
                                    Filter
                                </div>
                                <select class="select_drop accordian_title" name="accordian_title">
                                    <option value="Select" selected>Select</option>
                                    <option value="begins">Begins with</option>
                                    <option value="ends">Ends with</option>
                                    <option value="Contains">Contains</option>
                                    <option value="NotContains">Not Contains</option>
                                </select>
                                <input placeholder="Enter the Title" type="text" id="accordian_title_text" name="accordian_title_text" class="input_left">
                                <label type="button" class="button_normal_onoff excludemenu titleexcludedyon" value="Search">Exclude</label>
                                <label type="button" class="button_normal_onoff includemenu titleexcludedyoff" style="display: none;" value="Search">Include</label>
                                <label class="switch">
                                    <input type="checkbox" id="title_exclude_id" name="title_exclude_value" value="0" />
                                     <div class="slider round title_exclude"></div>
                                </label>
                                <i class="notes_title">Enter the title and hit enter to add inclusion or exclusion.</i>
                            </div>
                            <div class="clear"></div>
                            <div class="textboxlist">
                                <div style="font-weight:bold;" class="textboxlist-autocomplete-placeholder">Below are the Inclusion list for Title</div>
                                <ul class="textboxlist-bits accordian_title_li_append_result">
                                    
                                </ul>
                                <div class="textboxlist-autocomplete" style="width: 400px;">
                                    <ul class="textboxlist-autocomplete-results"></ul>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <h2 class="accordion_exclution">Competitor</h2>
                    <div class="wrapper_inner">
                        <div class="catagory">
                            <div class="field_12">
                                <input placeholder="Search for Competitor" id="accordian_competitor_search" type="text" class="input_left">
                                <label type="button" class="button_normal_onoff excludemenu competitorexcludedyon" id="comp_excbtn" value="Search">Exclude</label>
                                <label type="button" class="button_normal_onoff includemenu competitorexcludedyoff" id="comp_incbtn" style="display: none;" value="Search">Include</label>
                                <label class="switch">
                                    <input type="checkbox" id="competitor_exclude_id" name="competitor_exclude_value" value="0" />
                                     <div class="slider round competitor_exclude"></div>
                                </label>
                            </div>
                            <div class="clear"></div>
                            <div class="textboxlist">
                                <div style="font-weight:bold;" class="textboxlist-autocomplete-placeholder">Below are the Inclusion list for Competitor</div>
                                <ul class="textboxlist-bits accordian_competitor_li_append">
                                    
                                </ul>
                                <div class="textboxlist-autocomplete" style="width: 400px;">
                                    <ul class="textboxlist-autocomplete-results"></ul>
                                </div>
                                <div class="clear"></div>
                                <div class="accordian_competitor_search_result">
                                </div>
                            </div>
                    </div>
                </div>

        </div>
                <div class="button_left"> 
                <input type="submit" name="save_exclusion" class="button_apply save_exclusion_submit" value="Save">
                    <!-- <button type="submit" value="Search" class="button_apply save_exclusion_submit" name="save_exclusion">SAVE EXCLUSION</button> -->
                </div>
        </div>
        </div>
        <!-- end -->
        </form>

            <!-- <div class="popup">
                <div class="heading_strip_pop">
                    <p class="heading_pop">
                        Upload Partno
                    </p>
                    
                    
                </div>
                <div style="margin: 15px;">
                    <div class="field_12">
                        <div class="lable_normal">
                            Upload Part Number
                        </div>
                        <input class="browse_big" type="file">
                    </div>
                    <div class="button_center">
                        <button class="button_upload">Upload</button>
                        <button class="button_cancel">Cancel</button>
                    </div>
                </div>
            </div>
             -->
             <?php $this->load->view('footer.php'); ?>

    <script src="<?php echo base_url().'assets/';?>js/prettify.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>
    <script>
        $(function () {
            $("#accordion").accordion({
                collapsible: true,
                heightStyle: "content"
            });
        });
        $(document).ready(function () {
            $checks = $(":checkbox");
            $checks.on('change', function () {
                var string = $checks.filter(":checked").map(function (i, v) {
                    return this.value;
                }).get().join(" ");
                $('#field_results').val(string);
            });
        });
    </script>   
    <script>
        $(document).ready(function(){
            $(document).on('click','.general_exclude',function(){
                var id = $('#general_exclude_id').val();
                if(id == '0'){
                    $('.generalexcludedyon').hide();
                    $('.generalexcludedyoff').show();
                    $('#general_exclude_id').val('1');
                }
                else{
                    $('.generalexcludedyoff').hide();
                    $('.generalexcludedyon').show();
                    $('#general_exclude_id').val('0');
                }
            });
            $(document).on('click','.parts_exclude',function(){
                var id = $('#parts_exclude_id').val();
                if(id == '0'){
                    $('.partsexcludedyon').hide();
                    $('.partsexcludedyoff').show();   
                    $('#parts_exclude_id').val('1');
                }
                else{
                    $('.partsexcludedyoff').hide();
                    $('.partsexcludedyon').show();   
                    $('#parts_exclude_id').val('0');
                }
            });
            $(document).on('click','.category_exclude',function(){
                var id = $('#category_exclude_id').val();
                if(id == '0'){
                    $('.categoryexcludedyon').hide();
                    $('.categoryexcludedyoff').show();
                    $('#category_exclude_id').val('1');
                }
                else{
                     $('.categoryexcludedyoff').hide();
                     $('.categoryexcludedyon').show();
                    $('#category_exclude_id').val('0');
                }
            });
            $(document).on('click','.brand_exclude',function(){
                var id = $('#brand_exclude_id').val();
                if(id == '0'){
                     $('.brandexcludedyon').hide();
                     $('.brandexcludedyoff').show();
                    $('#brand_exclude_id').val('1');
                }
                else{
                    $('.brandexcludedyoff').hide();
                    $('.brandexcludedyon').show();
                    $('#brand_exclude_id').val('0');
                }
            });

            $(document).on('click','.title_exclude',function(){
                var id = $('#title_exclude_id').val();
                if(id == '0'){
                     $('.titleexcludedyon').hide();
                     $('.titleexcludedyoff').show();   
                    $('#title_exclude_id').val('1');
                }
                else{
                    $('.titleexcludedyoff').hide();
                    $('.titleexcludedyon').show();
                    $('#title_exclude_id').val('0');
                }
            });

            $(document).on('click','.competitor_exclude',function(){
                var id = $('#competitor_exclude_id').val();
                if(id == '0'){
                     $('.competitorexcludedyon').hide();
                     $('.competitorexcludedyoff').show();      
                    $('#competitor_exclude_id').val('1');
                }
                else{
                    $('.competitorexcludedyoff').hide();
                    $('.competitorexcludedyon').show();
                    $('#competitor_exclude_id').val('0');
                }
            });
        });
    </script>
    <script>  var selectedvalues = new Array();</script>
        <?php if(count($this->uri->segment(3)) == 1){?>
            <?php if(count($fetch_exclusion) >= 1){
                        if(count($fetch_exclusion[0]->general_cond) >= 1){?>
                            <?php foreach(explode(",",str_replace("'", '', $fetch_exclusion[0]->general_cond)) as $value){ ?>
                                   <script>  selectedvalues.push(<?php echo '"'.trim($value).'"';?>);</script>
                            <?php }
                        }
                    } 
           }
        ?>
<!-- script work start -->
    <script>
        var includearray = [];
        $(document).on('change','.general_select',function(){
            var optionvalue = $(this).val();
            var exclude = $('#general_exclude_id').val();
            var includeresult = '';
            if(exclude == '0'){
                includeresult = '!=';
            }
            if(optionvalue != ''){
                $('.general_label_li_append').html('');
                var general_replace = optionvalue.replace(/\!=|\"|\ |/g, '');
                includearray[general_replace] = includeresult+optionvalue;
                var result = '';
                for (var values in includearray) {
                    result += '<li class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+includearray[values]+'<span class="general_value_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="general_selected_value[]" value="'+includearray[values]+'"></li>'  
                }
                $('.general_label_li_append').html(result);
                console.log(includearray);
            }
        });

        $(document).on("click", ".general_value_delete", function() {
                var value = $(this).attr('data-value');
                var removeitem = value;
                for (var values in includearray) {
                    if(values == removeitem){
                        delete includearray[values];
                    }     
                }
                $(this).closest("li").remove();
        });
        var partarrayresult = [];
         $(document).on('keypress','.search_part_number',function(e){
                if (e.keyCode == 13) {
                    $('#isLoading').css('display','block');
                    e.preventDefault();
                    var searchtext = $('.search_part_number').val();
                    $.ajax({
                           url: '<?php echo base_url().'Filters/exclusion_search_partnumber'; ?>',
                           type: 'POST',
                           data: {'searchtext':searchtext},
                           success: function(data) {
                            $('#isLoading').css('display','none');
                            var json = $.parseJSON(data);
                            var resultappend = '';
                            $.each(json.result,function(key,value){
                                var pNo = value.partno.replace(/\"|\'|\!=|\&|\*|\-|\ |\,|\/|/g, ''); 
                                resultappend += '<div class="field"><input class="field_update_ver_chk accordian_parts_checked_value" id="'+pNo+'" data-value="'+pNo+'" data-value-result="'+value.partno+'" name="accordian_part_name[]" type="checkbox">'+value.partno+'</div>';
                                partarrayresult[pNo] = value.partno;
                            });
                            $('.accordian_parts_search_result').html(resultappend);  /*data-value-result="'+value.partno+'"*/
                           },
                           error : function(response){
                            $('#isLoading').css('display','none');
                            var screenname = '<?php echo $screen;?>'; 
                            console.log(screenname);
                          var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                           } 
                        });
                $('.search_part_number').val('');
                }
            });
            var selectpartsvalue = new Array();
            //part controls
            var partyselectarray = [];
            $(document).on('click','.accordian_parts_checked_value',function(){
                var datavalue = $(this).attr('data-value');
                var datavalueresult = $(this).attr('data-value-result');
                var exclude = $('#parts_exclude_id').val();
                var includeresult = '';
                if(exclude == '0'){
                    includeresult = '!=';
                }
                if ($(this).prop('checked')==true){ 
                    $('.parts_label_li_append').html('');
                    var partsdata_key = datavalue.replace(/\"|\'|\!=|\&|\*|\-|\ |\,|\/|/g, '');
                    partyselectarray[partsdata_key] = includeresult+datavalueresult;
                    $result = '';
                    for (var values in partyselectarray) {
                        var str = partyselectarray[values];
                        var regex = /[^\w\s]/gi;
                        var keepinclude = ''
                        if(regex.test(str) == true) {
                            keepinclude = '!=';
                        }
                        $result += '<li id="partsfilter_'+values+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+partyselectarray[values]+'<span class="accordian_parts_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="parts_selected_value[]" value="'+keepinclude+values+'"></li>';  
                    }
                    $('.parts_label_li_append').html($result);     
                }
                else{
                    var removeitem = datavalue;
                    for (var values in partyselectarray) {
                        if(values == removeitem){
                            delete partyselectarray[values];
                        }     
                    }
                    $('#partsfilter_'+datavalue).remove();
                }
            });

            $(document).on("click", ".accordian_parts_delete", function() {
                    var removeitem = $(this).attr('data-value');
                    for (var values in partyselectarray) {
                        if(values == removeitem){
                            delete partyselectarray[values];
                        }     
                    }
                    $(this).closest("li").remove();
                    $('#'+removeitem).attr('checked',false);
            });

    var searchcategoryarray = [];
         $(document).on('keypress','.search_category_number',function(e){
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#isLoading').css('display','block');
                    var searchtext = $('.search_category_number').val();
                    $.ajax({
                           url: '<?php echo base_url().'Filters/exclusion_search_categoryname';?>',
                           type: 'POST',
                           data: {'searchtext':searchtext},
                           success: function(data) {
                            $('#isLoading').css('display','none');
                            var json = $.parseJSON(data);
                            var resultappend = '';
                            $.each(json.result,function(key,value){
                              var filtercatid_key = value.category_id.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                               resultappend += '<div class="field"><input class="field_update_ver_chk accordian_category_checked_value" id="categoryfilter'+filtercatid_key+'" data-value="'+filtercatid_key+'" data-value-result="'+value.category+'" name="accordian_category_name[]" value="'+value.category_id+'" type="checkbox">'+value.category+'</div>';
                               searchcategoryarray[filtercatid_key] = value.category; 
                            });
                            $('.accordian_category_search_result').html(resultappend);
                           },
                           error : function(response){
                            $('#isLoading').css('display','none');
                            var screenname = '<?php echo $screen;?>'; 
                            console.log(screenname);
                          var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                           } 
                        });
                $('.search_part_number').val('');
                }
            });

            var selectpartsvalue_cat = new Array();
            //  category controls
            $(document).on('click','.accordian_category_checked_value',function(){
                var datavalue = $(this).attr('data-value');
                var datavalueresult = $(this).attr('data-value-result');
                var exclude = $('#category_exclude_id').val();
                var includeresult = '';
                if(exclude == '0'){
                    includeresult = '!=';
                }
                if($(this).prop('checked')==true){ 
                    $('.category_label_li_append').html('');
                    var filtercatid_key = datavalue.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                    selectpartsvalue_cat[filtercatid_key] = includeresult+datavalueresult;
                    var result = '';
                    for(var values in selectpartsvalue_cat) {
                        var str = selectpartsvalue_cat[values];
                        var regex = /[^\w\s]/gi;
                        var keepinclude = ''
                        if(regex.test(str) == true) {
                            keepinclude = '!=';
                        }
                        result += '<li class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable" id="selectedcategory_'+values+'">'+selectpartsvalue_cat[values]+'<span class="accordian_category_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="category_selected_value[]" value="'+keepinclude+values+'"></li>';  
                    }
                    $('.category_label_li_append').html(result);       
                }
                else{
                    var removeitem = datavalue.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                    for (var values in selectpartsvalue_cat) {
                        if(values == removeitem){
                            delete selectpartsvalue_cat[values];
                        }     
                    }
                    $('#selectedcategory_'+removeitem).remove();
                }
            });
            $(document).on("click", ".accordian_category_delete", function() {
                    var removeitem = $(this).attr('data-value').replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                    for (var values in selectpartsvalue_cat) {
                        if(values == removeitem){
                            delete selectpartsvalue_cat[values];
                        }     
                    }
                    $('#categoryfilter'+removeitem).attr('checked',false);
                    $(this).closest("li").remove();
            });
    
    // Brand controls
        var resultbrandarray = [];
         $(document).on('keypress','.search_brand_name',function(e){
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#isLoading').css('display','block');
                    var searchtext = $('.search_brand_name').val();
                    $.ajax({
                           url: '<?php echo base_url().'Filters/exclusion_search_brandname';?>',
                           type: 'POST',
                           data: {'searchtext':searchtext},
                           success: function(data) {
                            $('#isLoading').css('display','none');
                            var json = $.parseJSON(data);
                            var resultappend = '';
                            $.each(json.result,function(key,value){
                                if(value.brandname != ''){
                                    var filterbrandid_key = value.brandid.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                                    resultappend += '<div class="field"><input class="field_update_ver_chk accordian_brand_checked_value" id="filterbrandid_'+filterbrandid_key+'" data-value="'+filterbrandid_key+'" data-name="'+value.brandname+'" name="accordian_brand_name[]" value="'+value.brandid+'" type="checkbox">'+value.brandname+'</div>';
                                }
                            });
                            $('.accordian_Brand_search_result').html(resultappend);
                            $('.search_brand_name').val('');
                           },
                           error : function(response){
                            $('#isLoading').css('display','none');
                                                    var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                           }
                        });
                $('.search_part_number').val('');
                }
            });         
            $(document).on('click','.accordian_brand_checked_value',function(){
                    var datavalue = $(this).attr('data-value');
                    var dataname  = $(this).attr('data-name');
                    var exclude   = $('#brand_exclude_id').val();
                    var includeresult = '';
                    if(exclude == '0'){
                        includeresult = '!=';
                    }
                    var result = '';
                    $('.brand_label_li_append').html('');
                    if($(this).prop('checked')==true){ 
                        var selectfilterbrandid_key = datavalue.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                        resultbrandarray[selectfilterbrandid_key] = includeresult+dataname;
                        for (var values in resultbrandarray) {
                            var str = resultbrandarray[values];
                            var regex = /[^\w\s]/gi;
                            var keepinclude = ''
                            if(regex.test(str) == true) {
                                keepinclude = '!=';
                            }
                            result += '<li id="filterbrandlist_'+values+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+resultbrandarray[values]+'<span class="accordian_brand_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="brand_selected_value[]" value="'+keepinclude+values+'"></li>';  
                        }
                    }
                    else{
                            var removeitem = datavalue;
                            for (var values in resultbrandarray) {
                                if(values == removeitem){
                                    delete resultbrandarray[values];
                                }
                            }
                            for (var values in resultbrandarray) {
                            result += '<li id="filterbrandlist_'+values+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+resultbrandarray[values]+'<span class="accordian_brand_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="brand_selected_value[]" value="'+values+'"></li>';  
                            }
                            $('#filterbrandlist_'+datavalue).attr('checked',false);
                    }
                    $('.brand_label_li_append').append(result);
            });
            $(document).on("click", ".accordian_brand_delete", function() {
                    var datavalue = $(this).attr('data-value');
                    var removeitem = datavalue;
                        for (var values in resultbrandarray) {
                            if(values == removeitem){
                                delete resultbrandarray[values];
                            }     
                    }
                    $(this).closest("li").remove();
                    $('#filterbrandid_'+datavalue).attr('checked',false);
            });
    //title
            var titleresultarray = [];
                $(document).on('keypress','#accordian_title_text',function(e){
                    if (e.keyCode == 13) {
                        e.preventDefault();
                        var exclude = $('#title_exclude_id').val();
                        var includeresult = '';
                        if(exclude == 0){
                            includeresult = '!=';
                        }
                        var select_value = $('.accordian_title').val();
                        var text_field = $('#accordian_title_text').val();
                        var displaytext = '';
                        if(select_value == 'Select'){
                            alert('Kindly select the value');
                            $('#accordian_title_text').val("");
                            return false;
                        }
                        if(select_value == 'ends'){
                            titleresultarray[text_field] = includeresult+text_field+'%';
                            displaytext =   includeresult+text_field+'%'; 
                        }
                        if(select_value == 'begins'){
                            titleresultarray[text_field] = includeresult+'%'+text_field;
                            displaytext = includeresult+'%'+text_field; 
                        }
                        if(select_value == 'Contains'){
                            titleresultarray[text_field] = includeresult+'%'+text_field+'%';
                            displaytext = includeresult+'%'+text_field+'%'; 
                        }
                        if(select_value == 'NotContains'){
                            titleresultarray[text_field] = includeresult+text_field;
                            displaytext = includeresult+text_field; 
                        }
                        $('.accordian_title_li_append_result li').remove();
                        var result = '';
                        for (var values in titleresultarray) {
                                var temp_replace = titleresultarray[values].replace(/\%|/g, '');
                                var str = temp_replace;
                                var regex = /[^\w\s]/gi;
                                var keepinclude = ''
                                if(regex.test(str) == true) {
                                    keepinclude = '!=';
                                }
                                result += '<li class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+titleresultarray[values]+'<span class="accordian_title_delete" data-value="'+values+'">&nbsp;&nbsp;x</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="title_selected_value[]" value="'+titleresultarray[values]+'"></li>';
                        }

                        $('.accordian_title_li_append_result').append(result);
                        $('#accordian_title_text').val("");
                    }
                });
                $(document).on("click", ".accordian_title_delete", function() {
                    var datavalue = $(this).attr('data-value');
                    var removeitem = datavalue;
                    for (var values in titleresultarray) {
                        if(values == removeitem){
                                delete titleresultarray[values];
                        }    
                    }
                     $(this).closest("li").remove();
                }); 

    // competitor controls
  
            var compresultarray = [];
            $(document).on('keypress','#accordian_competitor_search',function(e){
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#isLoading').css('display','block');
                    var searchtext = $('#accordian_competitor_search').val();
                    $.ajax({
                           url: '<?php echo base_url().'Filters/exclusion_competitorname_search';?>',
                           type: 'POST',
                           data: {'searchtext':searchtext},
                           success: function(data) {
                            $('#isLoading').css('display','none');
                            var json = $.parseJSON(data);
                            var resultappend = '';
                                $.each(json.result,function(key,value){
                                    if(value.competitorname !== ''){
                                        var competitorid_key = value.compid.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                                        resultappend += '<div class="field"><input class="field_update_ver_chk accordian_competitor_checked_value" id="filtercompid_'+competitorid_key+'" data-value="'+competitorid_key+'" data-name="'+value.competitorname+'" name="accordian_competitor_name[]" value="'+value.compid+'" type="checkbox">'+value.competitorname+'</div>';
                                    }
                                });
                            $('.accordian_competitor_search_result').html(resultappend);
                           },
                           error : function(response){
                            $('#isLoading').css('display','none');
                            var screenname = '<?php echo $screen;?>'; 
                            console.log(screenname);
                          var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                           } 
                        });
                $('#accordian_competitor_search').val('');
                }
            });
            $(document).on('click','.accordian_competitor_checked_value',function(){
                    var datavalue = $(this).attr('data-value');
                    var dataname = $(this).attr('data-name');
                    var exclude = $('#competitor_exclude_id').val();
                    var includeresult = '';
                    if(exclude == '0'){
                        includeresult = '!=';
                    }
                    if($(this).prop('checked')==true){ 
                    $('.accordian_competitor_li_append').html('');
                    var selectcompetitorid_key = datavalue.replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                    compresultarray[selectcompetitorid_key] = includeresult+dataname;
                    var result = '';
                    for(var values in compresultarray){
                        var str = compresultarray[values];
                        var regex = /[^\w\s]/gi;
                        var keepinclude = ''
                        if(regex.test(str) == true) {
                            keepinclude = '!=';
                        }
                        result += '<li id="mycompid_'+values+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+compresultarray[values]+'<span class="accordian_competitor_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="competitor_selected_value[]" value="'+keepinclude+values+'"></li>';
                    }
                    $('.accordian_competitor_li_append').append(result);
                    }else{
                            var removeitem = datavalue;
                            for (var values in compresultarray) {
                                if(values == removeitem){
                                    delete compresultarray[values];
                                }     
                            }
                            $('#mycompid_'+datavalue).remove();
                    }
            });
            $(document).on("click", ".accordian_competitor_delete", function() {
                var data = $(this).attr('data-value').replace(/\"|\'|\&|\!=|\-|\ |\,|\/|\[|\]|\_|/g, '');
                $(this).closest("li").remove();
                var removeitem = data;
                for (var values in compresultarray) {
                    if(values == removeitem){
                        delete compresultarray[values];
                    }     
                }
                $('#filtercompid_'+data).attr('checked',false);
            });
    
    vars = window.location.href;
    var arrVars = vars.split("/");
    var postval = '';
    /*if(arrVars[3] == 'dive_romaine_ci' || arrVars[3] == 'Autocustoms' || arrVars[3] == 'Arrowhead' || arrVars[3] == 'Romainedemo'){*/
    if(arrVars[4] == 'Filters'){
    	postval = arrVars[6]
    }
    else{
    	postval = arrVars[7]
    }
    if(postval !== ""){
        $.ajax({
            type: "POST",
            url : "<?php echo base_url().'Filters/fetch_exclusion_frontend';?>",
            data: {'urlid':postval},
            success: function(data) {
                var json = $.parseJSON(data);
                //console.log(json);
                var resultappend = '';
                $.each(json.general_cond,function(key,values){
                    if(values !== ''){
                        var general_replace = values.replace(/\!=|\"|\ |/g, '');
                        resultappend += '<li class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+values+'<span class="general_value_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="general_selected_value[]" value="'+values+'"></li>';
                        includearray[general_replace] = values;
                    } 
                });
                $('.general_label_li_append').html(resultappend);

                //parts details
                var resultappend = '';
                $.each(json.part_filter,function(key,values){
                    if(values !== ''){
                        resultappend += '<li id="partsfilter_'+values+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+values+'<span class="accordian_parts_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="parts_selected_value[]" value="'+values+'"></li>';
                        var matchreplace = values.replace(/\"|\'|\&|\-|\ |\,|\/|\[|\]|\!=|\*|\_|/g, '');
                        partyselectarray[matchreplace] = values;
                    }
                });
                $('.parts_label_li_append').html(resultappend);

                  //category 
                  var result_cat = '';
                  $.each(json.category_filter,function(key,values){
                    if(values !== ''){
                        result_cat += '<li class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable" id="selectedcategory'+json.category_filter_id[key].replace(/\"|\ |\!=|/g, '')+'">'+values+'<span class="accordian_category_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="category_selected_value[]" value="'+json.category_filter_id[key]+'"></li>';  
                        selectpartsvalue_cat[json.category_filter_id[key].replace(/\"|\ |\!=|/g, '')] = values;
                    }
                  });
                  $('.category_label_li_append').html(result_cat);

                //work inprogress
                //brand
                var result_brand ='';
                $.each(json.brand_filter,function(key,values){
                        if(values !== ''){
                            result_brand += '<li id="filterbrandlist_'+values+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+values+'<span class="accordian_brand_delete" data-value="'+values+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="brand_selected_value[]" value="'+json.brand_filter_id[key]+'"></li>';  
                            resultbrandarray[json.brand_filter_id[key].replace(/\"|\ |\!=|/g, '')] = values;
                        }
                        });
                        $('.brand_label_li_append').append(result_brand);
                 //title
                var result_title ='';
                $.each(json.title_filter,function(key,values){
                        if(values !== ''){
                            result_title += '<li class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+values+'<span class="accordian_title_delete" data-value="'+values+'">&nbsp;&nbsp;x</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="title_selected_value[]" value="'+values+'"></li>';
                                titleresultarray[values.replace(/\"|\ |\!=|/g, '')] = values;
                        }
                  });
                   $('.accordian_title_li_append_result').append(result_title);
                   //competitor
                   var result_competitor = '';
                    $.each(json.competitor_filter,function(key,values){
                        if(values !== ''){
                         result_competitor += '<li id="mycompid_'+json.competitor_filter_id[key]+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+values+'<span class="accordian_competitor_delete" data-value="'+json.competitor_filter_id[key]+'">&nbsp;&nbsp;X</span><a class="textboxlist-bit-box-deletebutton" href="#"></a><input type="hidden" name="competitor_selected_value[]" value="'+json.competitor_filter_id[key]+'"></li>';
                             compresultarray[json.competitor_filter_id[key].replace(/\"|\ |\!=|/g, '')] = values;
                         }
                      });
                    $('.accordian_competitor_li_append').append(result_competitor);
           },
           error : function(data){
               var screenname = '<?php echo $screen;?>'; 
                            console.log(screenname);
                          var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
           } 
        });
    }
    </script>
<!-- script work end -->
    <script>
        $(document).on('click','.save_exclusion_submit',function(e){
             $('#isLoading').css('display','block');
            e.preventDefault();
            var formdata = $('#competitorform').serialize();
                $.ajax({
                    type: "POST",
                    url : "<?php echo base_url().'Filters/exclusion_submission';?>",
                    data: formdata,
                    success: function(data) {
                        //$('.jsonresult').css('display','block');
                        //$('.jsonresult').html(data);
                             $.ajax({
                                type: 'POST',
                                url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                data    : {'msgstatus':3},
                                    success: function(response){
                                         $('#isLoading').css('display','none');
                                            $('#msg').html(response);
                                            $('.err_modal').css('display','block');
                                            $('#msg').css('display','block');
                                    },
                                })
                   },
                   error : function(response){
                         // var msg =xhr.status;
                          //console.log(msg);
                         var screenname = '<?php echo $screen;?>'; 
                            console.log(screenname);
                          var msg =response.status;
                          // errorlog to db
                          var StatusCode = response.status;
                          var StatusText = response.statusText;
                          var ErrorHtml  = response.responseText; 
                            $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    
                                        success: function(response){
                                                return true;
                                        },
                            })
                          //console.log(msg);
                         $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                            data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                    }
                });
            });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
    <script>
        //new popup msg close button
     $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            //location.reload(true);
        });
    </script>
</body>
</html>