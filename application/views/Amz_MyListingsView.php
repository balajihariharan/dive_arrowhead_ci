<!DOCTYPE html>
<html ng-app="partsApp">
<html>
<head>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
           <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
     <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/css/amazondive.css">
      <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>   
   <!--  <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script> -->
    <!-- <script src="http://netsh.pp.ua/upwork-demo/1/js/typeahead.js"></script>  -->
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>    
</head>
<style>
.pagi_master li a{
    color: #fff;
    text-decoration: none;
    padding: 3px;
    background-color: chocolate;   
    border-radius: 7px; 
}
</style> 
<body class="main" ng-controller="partsCtrl" ng-cloak>
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

<?php $this->load->view('header.php'); ?>
    <div>
        <script>
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                    display: none !important;
            }

        th{
            color: rgba(0, 0, 0, 0.65);
            font-family: verdana;
            font-weight: bold;
            text-align: center;
            border-right: 1px solid rgba(0, 0, 0, 0.43);
        }

        .sort{
            cursor: pointer;
            background-image: url(<?php echo base_url();?>/assets/img/sort1.png) !important;
            padding: 10px 20px !important;
            background-position: 5px center !important;
            background-repeat: no-repeat !important;
        }

        td{
            font-family: verdana;
            color: rgba(0, 0, 0, 0.87);
        }

        .table{
            padding: 20px 0;
            text-align: center;
            width: 100%;
        }
        .table tr:nth-child(even){
            background : rgba(255, 193, 7, 0.09);
        }

        .expbuttoncss {
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            background-color: indianred;
            color: white;
        }

         .expbuttoncss:hover {
            background-color: #d21a1a;
            color: white;
        }   

        .alllabelcss{
            display: inline-block;
            padding: 3px 7px;
            cursor: pointer;
            font-family: verdana;
            border-radius: 10px;
        }

            
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main" style="width: 100%;">
                      <?php echo "<h2>".$screen."</h2>" ;?>
                      <div class="pricing_menu_manage amazonlogodiv">
                    <img class="amazonlogo" src="<?php echo base_url().'assets/';?>img/amz1.png"/>
                </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="wrapper">

            <div class="content_main_all" >
                <div class="total_items" style="font-family : verdana;font-size: 15px;">
                    <span>No. of ASINS Listed : </span>
                    <span style="font-weight: bold; color: #3F51B5;">{{TotalCount}}</span>
                </div>
                <label class="alllabelcss expbuttoncss" class="button_right" id="exportsel" >
                
                   
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                    <label class="alllabelcss expbuttoncss">
                    <img src="<?php echo base_url();?>/assets/img/download1.png" style="margin-right: 10px;"></img>
                    Bulk Export
                    </label>
                            </a>
                            <ul class="level2" style="background: #cd5c5c;">
                                <li>
                                    <a href="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_Amz_MyListings.csv">As CSV</a>
                                </li>
                                <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel()">As Excel</a></li>
                            </ul>
                        
                           
                        </li>

             
                </label>

             

                 <form id="mypartsform" ng-submit="onSubmit()">
                <div class="plan_list_view">
                    <table cellspacing="0" border="1" class="table price_smart_grid_new">
                        <thead>
                            <tr style="background : rgba(255, 153, 0, 0.55) none repeat scroll 0 0  ">
                                <th class="sort" ng-click="sortBy('ASIN')">ASIN #</th>
                                <th class="sort" ng-click="sortBy('Title')">Title</th>
                                <th class="sort" ng-click="sortBy('ListingID')">Listing ID</th>
                                <th class="sort" ng-click="sortBy('SKU')">SKU</th>
                                <th class="sort" ng-click="sortBy('ListPrice')">List Price (In $)</th>
                                <th class="sort" ng-click="sortBy('Quantity')">Quantity</th>
                                <th class="sort" ng-click="sortBy('InceptionDate')">Inception Date/Time</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text" ng-model="query" ng-change="mysearch()" class="inputsearch" placeholder="ASIN"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="titlequery" ng-change="titlesearch()" class="inputsearch" placeholder="Title"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="listidquery" ng-change="listidsearch()" class="inputsearch" placeholder="Listing ID"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="skuquery" ng-change="skusearch()" class="inputsearch" placeholder="SKU"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="listpricequery" ng-change="listpricesearch()" class="inputsearch" placeholder="List Price"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="quantityquery" ng-change="quantitysearch()" class="inputsearch" placeholder="Quantity"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="incepdatequery" ng-change="incepdatesearch()" class="inputsearch" placeholder="Inception Date/Time"/>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody">
                             <tr ng-repeat="part in pagedItems[currentPage] | orderBy:sortKey:reverse">
                                <td>{{part.ASIN}}</td>
                                <td style="max-width: 500px;">
                                    <a ng-click="redirectToLink($event)" element="{{part.ASIN}}" style="cursor: pointer;">{{part.Title}}</a>
                                </td>
                                <td>{{part.ListingID}}</td>
                                <td>{{part.SKU}}</td>
                                <td style="text-align: right;">{{part.ListPrice}}</td>
                                <td style="text-align: right;">{{part.Quantity}}</td>
                                <td>{{part.InceptionDate}}</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>

                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>
        </div>
        </div>
    </div>
      <?php $this->load->view('footer.php'); ?>
</div></div>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>
    <script>

        //load data in grid            
        var sortingOrder = 'ASIN';
        var app = angular.module('partsApp', ['ngSanitize', 'ngCsv']);
            app.controller('partsCtrl', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 25;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.parts = '';
                $scope.currentPage = 0;
                
                $scope.isLoading = true;
                $http.get("LoadData")
                .then(function (response) 
                {
                    $scope.isLoading = false;
                    $scope.parts = response.data;
                    console.log($scope.parts); 

                    $scope.TotalCount = $scope.parts.length;

                    var exportdata=[];
                    var exportdata_exc = [];
                    exportdata.push ({'ASIN' : 'ASIN#','Title' : 'Title','ListingID' : 'ListingID','SKU' : 'SKU','ListPrice' : 'ListPrice','Quantity':'Quantity','InceptionDate':'InceptionDate'});       
                    
                     angular.forEach($scope.parts, function(part) {
                        exportdata.push({'ASIN' :part.ASIN,'Title' :part.Title,'ListingID' : part.ListingID,'SKU' : part.SKU,'ListPrice' : part.ListPrice,'Quantity':part.Quantity,'InceptionDate':part.InceptionDate});
                        
                        exportdata_exc.push({'ASIN' :part.ASIN,'Title' :part.Title,'ListingID' : part.ListingID,'SKU' : part.SKU,'ListPrice' : part.ListPrice,'Quantity':part.Quantity,'InceptionDate':part.InceptionDate});
                    });

                   $scope.ForCSVExport = exportdata;
                   $scope.ForExcelExport = exportdata_exc;
                
                   
                                //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "ASIN") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.titlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                    if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             $scope.listidsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                    if(attr == "ListingID") {
                                            if (searchMatch(item[attr], $scope.listidquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.skusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                     if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.listpricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                     if(attr == "ListPrice") {
                                            if (searchMatch(item[attr], $scope.listpricequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.quantitysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                          for(var attr in item) {
                                            if(attr == "Quantity") {
                                            if (searchMatch(item[attr], $scope.quantityquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                            $scope.incepdatesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                          for(var attr in item) {
                                            if(attr == "InceptionDate") {
                                            if (searchMatch(item[attr], $scope.incepdatequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};
                    $scope.exportDataExcel = function () {
                             var mystyle = {
                                sheetid: 'Amz_MyListings',
                                headers: true,
                                column: { style: 'background:#a8adc4' },
                              };
                              alasql('SELECT * INTO XLS("DIVE_Amz_MyListings.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    };


            }); 

             $scope.redirectToLink = function (obj) {
                       var url = "http://www.amazon.com/dp/"+obj.target.attributes.element.value;
                       //$window.open(url, '_blank','width=1200,height=600');
                       $window.open(url, '_blank','width=1200,height=600,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no,top=100, left=200');
                    };   
  
        });
</script>
<script>
          $(document).on('click','.close1',function(){
          var name = document.getElementById('fname').value;
           if (name == '')
          {
            $("#Pnopopup").css("display", "none"); 
          }
          else
          {
            location.reload(true);
            $("#Pnopopup").css("display", "none");
          } 
        });


          //new popup msg close button
           $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
          location.reload(true);
      });


    $(document).on('click','.destroy2',function(){
    $(".err_modal").css("display", "none");     
    });

</script>
<script>
    $(document).on('click','.close',function(){
          $("#Pnopopup2").hide(); 
        });    
 


</script>
</body>
</html>
