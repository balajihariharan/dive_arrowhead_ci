<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
             <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
        
    </title>
   
</head>
<link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
  <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
<body class="main"  class="main" ng-app="myApp" ng-controller="myCtrl" ng-cloak>
    <?php $this->load->view('header.php'); ?>
    <div>
    <div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
      
        <style type="text/css">
        .edit_type
        {
            text-decoration: none;
            color: #1376AF;
            cursor: pointer;
            background-color: #fff;
            border: 1px solid #ccc;
            height: 27px;
            width: 85px;
            font-size: 14px;
        }
         .delete_type
        {
            text-decoration: none;
            color: #1376AF;
            cursor: pointer;
            background-color: #fff;
            border: 1px solid #ccc;
            height: 27px;
            width: 115px;
            font-size: 14px;
        }
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
             .edit_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 85px;
                font-size: 14px;
            }

             .delete_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 90px;
                font-size: 14px;
            }


            .buttonSave{
                padding: 6px 30px 6px 10px;
                margin: -28% 8% 0% 493%;
                border: none;
                border-radius: 3px;
                cursor: pointer;
                font-family: sans-serif, Verdana, Geneva;
                text-align: right;
                display: inline-block;
                color: #fff;
                vertical-align: top;
                background: url(../img/upload_green.png) no-repeat right -1px, linear-gradient(#009e47, #00713b);
                font-size: 13px;

            }
            
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                     <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>                
            
                 <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
                <div class="content_main_all">
                    <form>
                             <div class="field">
                             <div class="lable_normal" >
                             Category Details  <span style="color:red; font-size:16px;">*</span>  : 
                             </div>
                             <select  name="category[]"  ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" ng-model="category" id="category" class="select" ng-change="selectWeek()">
                             <option value="">-Select-</option>
                             </select>
                             </div>
                             </form>
                     <div class="button_right">

                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="export_button" ng-click="exportDataExcel()" value="Export"/>
                            </a>

                           
                        </li>
                </div>
                           <p id="metricslabel" style="    float: right; padding: 14px 2px;margin-right: 85px;margin-bottom: 0px;border-radius: 6px;">Items Count :<span id="metricsval" style="font-size: 26px; color: #00886d;">{{totalcount}}</span></p>
                        <div class="table_main">
                            <table style="width:100%;" border="1" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="curser_pt" ng-click="sortBy('ItemID')" style="text-align: center; vertical-align: middle; width: 8%;">Item ID</th>
                                        <th class="curser_pt" ng-click="sortBy('SKU')" style="text-align: center; vertical-align: middle; width: 6%;">SKU</th>
                                        <th class="curser_pt" ng-click="sortBy('MyPart')" style="text-align: center; vertical-align: middle;width: 6%;">My Part#</th>
                                        <th class="curser_pt" ng-click="sortBy('Category')" style="text-align: center; vertical-align: middle;">Category</th>
                                        <th class="curser_pt" ng-click="sortBy('Title')" style="text-align: center; vertical-align: middle;">Title</th>
                                        <th class="curser_pt" ng-click="sortBy('Quantity')" style="text-align: center; vertical-align: middle;width: 5%;">Quantity</th>
                                        <th class="curser_pt" ng-click="sortBy('Price')" style="text-align: center; vertical-align: middle;width: 6%;">Price (in $)</th>
                                        <th class="curser_pt" ng-click="sortBy('QtySold')" style="text-align: center; vertical-align: middle;width: 5%;">Qty Sold (Inc)</th>
                                     <!--    <th class="curser_pt" ng-click="sortBy('LastSold')" style="text-align: center; vertical-align: middle;width: 8%;">Last Sold On(Week No)</th>
 -->                                    </tr>
                                      <tr>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text" ng-model="ItemID" ng-change="mysearch()" class="inputsearch" placeholder="ItemID"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="SKU" ng-change="SKUsearch()" class="inputsearch" placeholder="SKU"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="MyPart" ng-change="MyPartsearch()" class="inputsearch" placeholder="MyPart"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="Category" ng-change="Categorysearch()" class="inputsearch" placeholder="Category"/>
                                                </div>
                                            </td>
                                             <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="Title" ng-change="Titlesearch()" class="inputsearch" placeholder="Title"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="Quantity" ng-change="Quantitysearch()" class="inputsearch" placeholder="Quantity"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="Price" ng-change="Pricesearch()" class="inputsearch" placeholder="Price"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="QtySold" ng-change="QtySoldsearch()" class="inputsearch" placeholder="QtySold"/>
                                                </div>
                                            </td>
                                            <!-- <td>
                                                <div class="search">
                                                <input class="inputsearch" type="text"  ng-model="LastSold" ng-change="LastSoldsearch()" class="inputsearch" placeholder="LastSold"/>
                                                </div>
                                            </td> -->
                                        </tr> 
                                </thead>



                                             <tbody>
                                    <tr ng-repeat="part in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                  <!--   <tr ng-repeat="part in compdata"> -->
                                        <td style="text-align: left;">{{part.ItemID}}</td>
                                        <td style="text-align: left;">{{part.SKU}}</td>
                                        <td style="text-align: left;">{{part.MyPart}}</td>
                                        <td style="text-align: left;">{{part.Category}}</td>
                                        <td style="text-align: left;">
                                            <a ng-click="redirectToLink($event)" element="{{part.ItemID}}" style="cursor: pointer;" >{{part.Title}}</a></td>
                                        <td style="text-align: left;">{{part.Quantity}}</td>
                                        <td style="text-align: left;">{{part.Price}}</td>
                                        <td style="text-align: left;">{{part.QtySold}}</td>
                                        <!-- <td style="text-align: center;">{{part.LastSold}}</td> -->
                                    </tr>
                                    </tbody>

                                </table>
                                
                                </div>
                                <div class="pagination pull-right pagi_master">
                                    <ul>
                                        <li ng-class="{disabled: currentPage == 0}">
                                            <a href ng-click="prevPage()">« Prev</a>
                                        </li>
                                        <li ng-repeat="n in pagenos | limitTo:5"
                                            ng-class="{active: n == currentPage}"
                                            ng-click="setPage()">
                                            <a href ng-bind="n + 1">1</a>
                                        </li>
                                        <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                            <a href ng-click="nextPage()">Next »</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

        
<?php $this->load->view('footer.php'); ?>

 <script >
var sortingOrder = 'ItemID';
 var app = angular.module('myApp', []);
            app.controller('myCtrl', function($scope,$http,$filter,$window) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 20;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.compdata = '';
                $scope.currentPage = 0;
                $scope.route = 0;

                $scope.isLoading = true;
                $scope.redirectToLink = function (obj) {
                       var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                       $window.open(url, '_blank','width=1200,height=600');
                    };

                   $http({   
                          method  : 'POST',
                          url     : '<?php echo base_url();?>index.php/ZeroInventory/getcategory',
                          data    : {'data' : '' },
                          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                         })

                    .success(function(data) {
                         $scope.isLoading = false;
                         $scope.subcategories = data;
                         console.log($scope.subcategories);
                          })
                    .catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                         .success(function(response) {
                                            return true;
                                });

                        //failure msg
                            $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data    : msg
                            })
                             .success(function(response) {
                                $('#msg').html(response);
                                $('.err_modal').css('display','block');
                                $('#msg').css('display','block');                  
                                $scope.isLoading = false;
                            });
                        })
              
              
                $scope.selectWeek = function(){
                 $scope.isLoading = true;
                 var category = $scope.category.Categoryid;
                  console.log($scope.category.Categoryid);
                      $http({
                        method: 'POST',
                        url     : '<?php echo  base_url();?>index.php/ZeroInventory/GetData',
                        data    : {'category' : $scope.category.Categoryid},
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                      })
                    .success(function(response){
                    var exportdata_exc = [];
                   console.log(response);
                    $scope.isLoading = false;
                    $scope.compdata = response.result;

                    $scope.totalcount =$scope.compdata.length;
                
                     $scope.compdata.forEach(function (part) {
                       
                            exportdata_exc.push({'ItemID' : part.ItemID,'SKU':part.SKU,'MyPart' : part.MyPart,'Category' : part.Category,'Title' : part.Title,'Quantity' : part.Quantity,'Price (in $)': part.Price,'QtySold (Inc)':part.QtySold});

                        });
                    $scope.ForExcelExport = exportdata_exc;
                  

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch  = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "ItemID") {
                                            if (searchMatch(item[attr], $scope.ItemID)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                            $scope.SKUsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                    if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.SKU)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                               $scope.MyPartsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "MyPart") {
                                            if (searchMatch(item[attr], $scope.MyPart)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.Categorysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Category") {
                                            if (searchMatch(item[attr], $scope.Category)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                           $scope.Titlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.Title)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                           $scope.Quantitysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Quantity") {
                                            if (searchMatch(item[attr], $scope.Quantity)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.Pricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "Price") {
                                            if (searchMatch(item[attr], $scope.Price)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.QtySoldsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "QtySold") {
                                            if (searchMatch(item[attr], $scope.QtySold)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.LastSoldsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.compdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "LastSold") {
                                            if (searchMatch(item[attr], $scope.LastSold)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };


                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};
                            $scope.showLoader = false; //Loader Image 

                         $scope.exportDataExcel = function () {
                        var mystyle = {
                            sheetid: 'ZeroInventory',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_ZeroInventory.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                    }; 

                }).catch(function (err) {
                         var msg =err.status;
                              //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                             $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                     headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                  })
                             .success(function(response) {
                                            return true;
                                          });

                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                        })
               }
               
      })
$(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
</script>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>

 
</body>
</html>       
            