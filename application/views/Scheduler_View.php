<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/css/index.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/config_css/css/datePicker.css"> -->

    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/demo.css">

    <!-- following stylesheets are only for demos with calendar.js -->
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/month.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/week.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>assets/demo/flight.css">
</head>
<body class="main" ng-app="myApp" ng-controller="myCtrl" ng-cloak>
    <!-- <div class="container head_bg"> -->


    <?php $this->load->view('header.php'); ?>

    <div>
     <!--    <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script> -->
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }

            .status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }
        </style>
        <div class="container header_bg_clr">
         <div id="msg" style="display: none;"></div>
            <div class="wrapper">
                <div class="header_main">
                    <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
       
                     <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
            <div class="content_main_all">
                <form id="savedata" method="post">
                     <center> <div class="error_msg_all" style=" display:none" id="msg_id">Please select the set schedule for</div> </center>
                        <div class="channel_main">
                            <div class="channel_all">

                                <label class="lable_normal">Set Schedule for<span style="color:red; font-size:16px;">*</span> :</label>
                                <!-- id="services" -->
                                <select ng-change="drop_down_med()" ng-model="servicevalue" name="services" id="empty"   class="drop_down_med margin_top_10">
                                    <option value=""   >Select</option>
                                    <option value="CBR">CBR</option>
                                    <option value="IBR">IBR</option>
                                <!--<option value="other">Other Services</option>-->
                                </select>
                            </div>
                            

                            <span class="returnresponse">
                            </span>
                        </div>


                    <div class="field_set">
                        <fieldset>
                            <p class="input_field">
                                <input type="radio" name="myschedules" ng-click="mydailyschedules()" value="daily" ng-checked="resultdaily_flag == 1"/>
                                <label class="label_inner_general" ng-click="mydailyschedules()" ng-model="mydailyschedules">Daily</label>
                                <input type="radio" name="myschedules" ng-click="myweeklyschedules()" value="weekly" ng-checked="resultweekly_flag == 1"/>
                                <label class="label_inner_general" ng-click="myweeklyschedules()" value="{{myweeklyschedules}}" ng-model="myweeklyschedules">Weekly</label> 
                                <input type="radio" name="myschedules" ng-click="mymonthlyschedules()" value="monthly" ng-checked="resultmonthly_flag == 1"/>
                                <label class="label_inner_general" ng-click="mymonthlyschedules()" ng-model="mymonthlyschedules">Monthly</label>
                            </p>

                            <p class="input_field">

                                <p> <span ng-show="startfrom">Start From</span>
                                <input type="text" class="date date-1" ng-show="startfrom"   ng-model="fromdate" name="fromdate" placeholder="YYYY-MM-DD" value="{{fromdate}}" required/>
                                </p> 

                                <div id="daily">
                                <input type="checkbox" name="sunday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resultsun == 1" value="{{resultsun}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Sun</label>

                                <input type="checkbox" name="monday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resultmon == 1" value="{{resultmon}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Mon</label>

                                <input type="checkbox" name="tuesday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resulttue == 1" value="{{resulttue}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Tue</label>

                                <input type="checkbox" name="wednesday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resultwed == 1" value="{{resultwed}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Wed</label>

                                <input type="checkbox" name="thursday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resultthu == 1" value="{{resultthu}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Thu</label>

                                <input type="checkbox" name="friday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resultfri == 1" value="{{resultfri}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Fri</label>

                                <input type="checkbox" name="saturday" ng-if="dailyschedule" class="check_box_sub" ng-checked="resultsat == 1" value="{{resultsat}}"/>
                                <label class="label_inner_sub" ng-if="dailyschedule">Sat</label>
                                </div>

                                </p>
                           <p> Repeat Until
                                <!-- <input type="text" class="date date-1" ng-model="repeatuntil" data-from="theFlight" id="strtfrm" ng-model="fromdate" name="fromdate" value="{{resultrepeat_until}}" required/> -->
                                <input type="text" class="date date-1" placeholder="YYYY-MM-DD" name="repeatutil" value="{{resultrepeat_until}}" required/>
                            </p> 
                            

                           <p>Time

                          
                           <input type="text" class="date date-1" data-type="time" placeholder="HH:MM:SS" name="repeattime" value="{{resultrepeattime}}" required/>
                            <!-- <input type="text" class="date date-1" data-from="theFlight" value="" data-type="time" placeholder="HH:MM:SS" value="{{resultrepeattime}}" required/> -->
                            </p>

                              <p class="save_icon_manage">
                            <input type="button" id="save" class="button_add rawlistdatasubmit" value="Save">
                        </p>
                            </div>
                            </fieldset>
                    </div>
                </form>
            </div>
        </div>
        
        
        
           <?php $this->load->view('footer.php'); ?>
    </div>
        <script type="text/javascript" src="<?php echo  base_url();?>assets/demo/events.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/calendar.js"></script>
    <script type="text/javascript" src="<?php echo  base_url();?>assets/js/datePicker.js"></script>
    <script src="<?php echo  base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
<script>
$(document).ready(function(){});
</script>

<script type="text/javascript">
    'use strict';


   
    function extend(a, b) { 
        for(var key in b) {
            if(!a.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
        return a;
    }


    var getTodayString = function() {
            var today = new Date();

            return today.getFullYear() + '-' +
                ((today.getMonth() + 1) + '').replace(/^(\d)$/, '0$1') + '-' +
                (today.getDate() + '').replace(/^(\d)$/, '0$1');
        };

    var options = {
            weekDays: ['Su', 'Mo', 'Tu', 'Wed', 'Thu', 'Fri', 'Sa'],
            months: ['january', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'],

            sundayBased: false,
            renderWeekNo: true,
            equalHight: true,
            events: events, 

            template: {
                row: '<td class=""><span class=""{{event}} data-day=\'{"day":"{{day}}", "month":"{{month}}", "year":"{{year}}"}\'>{{day-event}}{{today}}</span></td>',
           
                event: function(day, date, event) {
                    var text = [],
                        uuids = [],
                        someExtra = '';

                    for (var n = 0, m = event.length; n < m; n++) {
                        event[n].text && text.push('- ' + event[n].text);

                        uuids.push(event[n]._id);

                        if (event[n].extra) { 
                            someExtra = event[n].extra;
                        }
                    }
                    text = text.join("\n");

                    return text ? ' title="' + text + '"' +
                        ' data-uuids=\'[' + uuids.join(', ') + ']\'' +
                        (someExtra ? ' data-name="' + someExtra + '"' : '') : '';
                },
             
                today: function(day, date) {
                    return '<span class="today-icon">+</span>';
                },
           
                day: function(day, date, event) {
                    var length = event.length;

                    for (var n = length; n--; ) { // check if it's only a 'disabled' event
                        if (event[n].type && event[n].type === 'disabled') { // or event[n].disabled
                            length--;
                        }
                    }
                    if (length > 1) {
                        return day + '<span class="count-icon">' + length + '</span>';
                    }
                }
            },
     
            readValue: function(element) {
                if (!element.value && element.getAttribute('data-type') === 'time') { // initial time if empty
                    return new Date().toTimeString().split(' ')[0]; // triggers default behavior
                }

                return element.value; // triggers default behavior
            }
        },
     
        markRangeDays = function(container, element, toggled) {
            var calOptions = this.calendar.options,
                value_1 = +this.currentInput.value.split(' ')[0].replace(/-/g, ''),
                value_2 = this.currentPartner &&
                    +this.currentPartner.value.split(' ')[0].replace(/-/g, ''),
                dates = container.querySelectorAll( // get all displayed days
                    '.' + calOptions.prevMonthClass + ', ' +
                    '.' + calOptions.nextMonthClass + ', ' +
                    '.' + calOptions.currentMonthClass),
                data = '';

            if (this.isOpen && toggled !== undefined && value_2)  {
                for (var n = 0, m = dates.length; n < m; n++) {
                    if (data = dates[n].getAttribute(this.options.pickerAttribute)) {
                        data = +data.replace(/-(\d)(?=(?:-|$))/g, '0$1').replace(/-/g, '');
                        if (value_1 && value_2 && value_1 !== value_2 && (
                                (value_1 <= data && value_2 >= data) ||
                                (value_1 >= data && value_2 <= data))) {
                            dates[n].className += ' ' + (this.options.rangeClass || 'range') +
                            (value_1 === data || value_2 === data ? ' range-' +
                                (value_1 >= data && value_2 >= data? 'begin' : 'end') : '');
                        }
                    }
                }
            }
        },
       
        addButtons = function(container, element, toggled) {
            var _today = new Date(),
                today = _today.getFullYear() + '-' + (_today.getMonth() + 1) + '-' + _today.getDate(),
                date = this.date.year + '-' + +this.date.month + '-' + +this.date.day,
                isFrom = element.hasAttribute(this.options.rangeStartAttribute),
                isRange = this.currentPartner,
                value_2 = isRange && +this.currentPartner.value.split(' ')[0].replace(/-/g, ''),
                minDate = +(element.getAttribute(this.options.minDateAttribute) || this.options.minDate).replace(/-/g, ''),
                maxDate = +(element.getAttribute(this.options.maxDateAttribute) || this.options.maxDate).replace(/-/g, ''),
                _today = +today.replace(/-(\d)(?=(?:-|$))/g, '0$1').replace(/-/g, ''),
                todayPossible = (today !== date || !element.value) && minDate <= _today && _today <= maxDate && (
                    isRange ? (isFrom ? !value_2 || value_2 >= _today : !value_2 || value_2 <= _today) : true);

            this.isOpen && toggled !== undefined && container.insertAdjacentHTML('beforeend', // render buttons...
                '<div class="dp-footer">' +
                    '<button class="action clear"' + (this.currentInput.value ? '' : ' disabled') +
                        ' type="button">' + (this.options.clearText || 'clear') + '</button>' +
                    '<button class="action today"' + (todayPossible ? '' : ' disabled') +
                        ' data-picker="' + today + '" type="button">' + (this.options.todayText || 'today') + '</button>' +
                    '<button class="action close" type="button">' + (this.options.closeText || 'close') + '</button>' +
                '</div>');

            this._hasListeners = this._hasListeners || (function(_this) { // ...and add event listeners (once)
                container.addEventListener('click', function(e) {
                    var target = e.target,
                        className = target.className;

                    if (/action/.test(className)) {
                        if (/close/.test(className)) {
                            _this.toggle();
                        } else if (/clear/.test(className)) {
                            _this.currentInput.value = '';
                            _this.toggle(true);
                        } 
                    }
                });

                return true; // make this._hasListeners true for next call of renderCallback
            })(this);
        };




    // ------------------- DEMO for input.date-1 ------------------ //
    // Regular example that enables all kinds of different formats
    // and ranges and demonstrates the default possibilities of
    // datePicker... (except extra features defined in default options)
    // ------------------------------------------------------------ //
    window.myDatePicker_1 = new DatePicker('.date-1', options);
        

    // ------------------- DEMO for input.date-11 ----------------- //
    // Example with marked range days.
    // ------------------------------------------------------------ //
    // first set input fields to a minimum date
    [].map.call(document.querySelectorAll('.date-11'), function(elm, a) {
        elm.setAttribute('data-mindate', getTodayString());
    });

    window.myDatePicker_11 = new DatePicker('.date-11', extend({
        closeOnSelect: false,
        renderCallback: function(container, element, toggled) {
            markRangeDays.call(this, container, element, toggled);
            addButtons.call(this, container, element, toggled);

            return true; // triggers default behaviour
        }
    }, options));


    // ------------------- DEMO for input.date-111 ----------------- //
    // Example with different month / year selector (tables).
    // This is a bit more complex as we need to render years, months,
    // to install event listeners and to deal with min/max values...
    // ------------------------------------------------------------ //
    var hasClass = function(element, className) {
            return (' ' + element.className + ' ').indexOf(' ' + className + ' ') !== -1;
        },
        renderMonthsYears = function(monthYear, _this) {
            var isMonth = monthYear === 'dp-label-month',
                year = _this.tempYear - 4, // makes current the 5th in the table
                month = 1,
                getData = function(n, isMonth) {
                    var data = isMonth ? month++ : year++;

                    if (isMonth) {
                        data = +_this.date.year >= +_this.maxDate.year && data > +_this.maxDate.month ? '' :
                            +_this.date.year <= +_this.minDate.year && data < +_this.minDate.month ? '' : data;
                    } else {
                        if (!_this.hasMaxYear && data >= _this.maxDate.year) {
                            _this.hasMaxYear = true;
                        }
                        if (!_this.hasMinYear && data <= _this.minDate.year) {
                            _this.hasMinYear = true;
                        }
                        data = data <= _this.maxDate.year && data >= _this.minDate.year ? data : '';
                    }

                    return isMonth ? data === '' ? data : _this.options.months[data - 1] : data;
                },
                dataHTML = (function() {
                    var html = [];

                    _this.hasMinYear = _this.hasMaxYear = false;

                    for (var n = 0, m = 4; n < m; n++) {
                        for (var x = 0, y = 3; x < y; x++) {
                            html.push(_this.options.dataHTML.
                                replace(/class="(.*?)"/, function($1, $2) {
                                    return 'class="' + $2 + (
                                        isMonth && month === +_this.date.month ? ' current this-month' :
                                        !isMonth && year === +_this.date.year ? ' current this-year' : '') + '"';
                                }).
                                replace('{{data}}', getData(n * y + x, isMonth)).
                                replace('{{day}}', +_this.date.day).
                                replace('{{month}}', isMonth ? month - 1 : +_this.date.month).
                                replace('{{year}}', isMonth ? _this.date.year : year - 1 ));
                        }
                        html.push(_this.options.glueHTML);
                    }
                    html.pop(); // remove last <tr>

                    return html.join('');
                }());

            return _this.options.header.
                replace('{{month}}', _this.options.months[_this.date.month - 1]).
                replace('{{year}}', _this.date.year) +
                _this.options.yearsMonthsHTML.replace('{{data}}', dataHTML);
        };

    


    // ------------------- DEMO for input.date-2 ------------------ //
    // Example for hidden input field triggered by a button and
    // rendering a formatted value to a disabled input field.
    // ------------------------------------------------------------ //
    var getInput = function(element, hidden) {
            return element.parentNode.querySelector('input' + (hidden ? '[type="hidden"]' : ''));
        },
        changeValueFormat = function(value) { // some dummy function...
            var date = new Date(value + 'T00:00:00.000Z');
            return date.toDateString();
        };

    window.myDatePicker_2 = new DatePicker('.date-2', extend({
        /**
         * Initially render all values in input fields useing converted
         * values from hidden input fields.
         * 
         * @param  {ElementList} elements All elements datePicker is initialized to
         */
        initCallback: function(elements) {
            var input,
                hiddenInput;

            for (var n = elements.length; n--; ) {
                input = getInput(elements[n]);
                hiddenInput = getInput(elements[n], true);

                input.value = changeValueFormat(hiddenInput.value);
            }
        },
        renderCallback: function(container, element, toggled) {
            return getInput(element); // triggers default behaviour on certain element
        },
        renderValue: function(container, element, value) {
            getInput(element).value = changeValueFormat(value);
            return getInput(element, true); // triggers default behaviour on certain element
        },
        readValue: function(element) {
            var value = getInput(element, true).value;

            getInput(element).value = changeValueFormat(value);
            return value;
        }
    }, options));


    // ------------------- DEMO for input.date-3 ------------------ //
    // Example like the above but then year, month and day get
    // rendered in seperate fields.
    // ------------------------------------------------------------ //
    var getInputs = function(element, hidden) {
            return element.parentNode.querySelectorAll('input');
        };

    window.myDatePicker_3 = new DatePicker('.date-3', extend({
        renderCallback: function(container, element, toggled) {
            return getInputs(element)[0];
        },
        renderValue: function(container, element, value) {
            var inputs = getInputs(element),
                val = value.split('-');

            inputs[3].value = value;
            inputs[0].value = val[0];
            inputs[1].value = val[1]; // this.options.months[+val[1] - 1];
            inputs[2].value = val[2];
        },
        readValue: function(element) {
            var value = getInputs(element)[3].value;

            this.options.renderValue.call(this, undefined, element, value);
            return value;
        }
    }, options));


    // ------------------- DEMO for input.date-4 ------------------ //
    // Example with data from variables.
    // ------------------------------------------------------------ //
    var dates = window.dates = {
            'date-1': '2016-06-23',
            'date-2': '2016-07-26',
            'date-3': '2016-08-28'
        },
        dataAttr = 'data-date',
        changeValueFormat_de = function(value, options) {
            var parts = value.split(' ')[0].split('-'),
                date = new Date(parts[0], parts[1] - 1, parts[2] || 0);

            return options.weekDaysLong[date.getDay()] + ', ' + date.getDate() + '. ' +
                options.months[date.getMonth()] + ' ' + date.getFullYear();
        };

    window.myDatePicker_4 = new DatePicker('.date-4', extend({
        weekDaysLong:
            ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        initCallback: function(elements) {
            for (var n = elements.length; n--; ) {
                elements[n].value =
                    changeValueFormat_de(dates[elements[n].getAttribute(dataAttr)], this.options);
            }
        },
        renderValue: function(container, element, value) {
            dates[element.getAttribute(dataAttr)] = value;
            element.value = changeValueFormat_de(value, this.options);
        },
        readValue: function(element) {
            return dates[element.getAttribute(dataAttr)];
        }
    }, options));


    // ------------------- DEMO for input.date-5 ------------------ //
    // Example with less markup.
    // ------------------------------------------------------------ //
    window.myDatePicker_5 = new DatePicker('.date-5', extend({
        renderWeekNo: false,
        equalHight: false,
        datePickerClass: 'date-picker div-based',
        template: {
            start: function(month, year) { // rendering week days
                return '{{days}}'
            },
            daysOfWeek: '<div class="item">{{day}}</div>',
            colGlue: '',
            row: '<div class="item{{day-event}}">{{day}}</div>',
            end: function() {return ''},
            day: function(days, date) {
                if (date.getDay() === (this.options.sundayBased ? 0 : 1)) {
                    return ' cleared-day';
                }
                return ' ';
            }
        }
    }, options));


    // ------------------- DEMO for input.date-6 ------------------ //
    // Example with today, clear and close button (stays open).
    // ------------------------------------------------------------ //
    window.myDatePicker_6 = new DatePicker('.date-6', extend({
        renderWeekNo: true,
        closeOnSelect: false,

        clearText: '', // following are optional for different languages
        todayText: '',
        closeText: 'exit', // prints 'exit' in place of default 'close'

        renderCallback: function(container, element, toggled) {
            addButtons.call(this, container, element, toggled);

            return true; // triggers default behaviour
        }
    }, options));
</script>
<script>
      var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope,$http) {
          $scope.startfrom = false;
          $scope.dailyschedule = false;
          $scope.mydailyschedules = function(){
            console.log('i am running');
            $scope.dailyschedule = true;
            $scope.startfrom = false;
            document.getElementById('daily').style.display = "none";
          }

          $scope.myweeklyschedules = function(){
            document.getElementById('daily').style.display = "block";
            $scope.dailyschedule = true;
            $scope.startfrom = false;
          }

          $scope.mymonthlyschedules = function(){
            document.getElementById('daily').style.display = "none";
            $scope.dailyschedule = false;
            $scope.startfrom = true;
          }
          $scope.drop_down_med = function(){
            var selectvalue = $scope.servicevalue;
            $http({
                method: 'POST',
                url: '<?php echo base_url();?>index.php/Scheduler/get_data',
                data: {'Name' : $scope.servicevalue},
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                $scope.resultmon           = response.data[0].mon;
                $scope.resulttue           = response.data[0].tue;
                $scope.resultwed           = response.data[0].wed;
                $scope.resultthu           = response.data[0].thu;
                $scope.resultfri           = response.data[0].fri;
                $scope.resultsat           = response.data[0].sat;
                $scope.resultsun           = response.data[0].sun;
                if(response.data[0].daily_flag == 1){
                 $scope.dailyschedule = true;
                }
                if(response.data[0].weekly_flag == 1){
                 $scope.dailyschedule = true;   
                 $scope.startfrom = false;
                 $("#daily").css("display","none");
                }

                if(response.data[0].monthly_flag == 1){
                 $scope.dailyschedule = false;
                 $scope.startfrom = true;   
                }
                $scope.resultdaily_flag    = response.data[0].daily_flag;
                $scope.resultweekly_flag   = response.data[0].weekly_flag;
                $scope.resultmonthly_flag  = response.data[0].monthly_flag;
                $scope.resultrepeat_until  = response.data[0].repeat_until;
                $scope.resultrepeattime    = response.data[0].schedule_time;
                $scope.fromdate = response.data[0].starting_from;
            })
              .catch(function (err) {
                                  var msg = err.status;
                                   //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg(db error)
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                         
                        })
            $scope.$watch('resultrepeat_until',function(newdata){
                $scope.resultrepeat_until = newdata;
            });

          }
           $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
           location.reload(true);
            
        });
        });
</script>
 <script>
         
            
   

                      $("#save").click(function(e){
                      if ( $('#empty').val() == "") {
                      var empty=document.getElementById('empty').value;
                      if(empty == ""){
                         document.getElementById('empty').style.borderColor = "red";
                         $("#msg_id").css("display","block");
                                  return false;
                     }
                     else {
                         document.getElementById('empty').style.borderColor = "";
                         $("#msg_id").css("display","none"); 
                    }
                        }
                             
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'save',
                data: $("#savedata").serialize(),
                success: function(response){

                    //alert('Updated Successfully!!');
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                            data : {'msgstatus':3},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                }
                            })

                },
            
                 error : function(xhr){

                             var msg =xhr.status;
                             var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                            //alert('Please try again');
                             $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                        }
            })
        });
</script>
<script>
         //new popup msg close button
          $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
            
        });
 </script>
</body>
</html>
