<!DOCTYPE html>
<html ng-app="postApp">
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown1.css">
    <!-- <link rel="stylesheet" href="<?php echo  base_url();?>assets/price_css/style.css" />
    <link rel="stylesheet" href="<?php echo  base_url();?>assets/price_css/style_romaine.css" /> -->
    <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

         <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>
<head>
<title>   <?php echo $title[0]->SellerName;?> : <?php echo $screen;?></title>
        <style type="text/css">
            ul,li { margin:0; padding:0; list-style:none;}
        .label { color:#000; font-size:16px;}
        .ckeckclass
        {
            margin:2px;
        }
        .divTable{
            float: left;
            width: 70px;
            height: 40px;
            border-right: 1px solid #e1e1e1;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .lastdivTable{
            float: left;
            width: 70px;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .paging-nav {
          text-align: right;
          padding-top: 2px;
        }

        .paging-nav a {
          margin: auto 1px;
          text-decoration: none;
          display: inline-block;
          padding: 1px 7px;
          background: #91b9e6;
          color: white;
          border-radius: 3px;
        }

        .paging-nav .selected-page {
          background: #187ed5;
          font-weight: bold;
        }

        tfoot
        {
        text-align: center !important;
        display: table-row-group !important;
        }

        .paging-nav,
        #tblmasterresult {
         
          margin: 0 auto;
          font-family: Arial, sans-serif;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 20%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .myPnopopupsubclose{
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;   
        }
        .myPnopopupsubclose:hover,
        .myPnopopupsubclose:focus{
            color: black;
            text-decoration: none;
            cursor: pointer;   
        }

        div.absolute {
            position: absolute;
            top: 326px;
            left: 758px;
            width: 46px;
            height: 407px;
            cursor: pointer;
           
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        .modal-content {
            background-color: #fefefe;
            margin: 10% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 32%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #2d8bd5;
            text-decoration: none;
            cursor: pointer;
        }
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }
            table thead tr td span{
                color:#fff;
            }
            .activeplus { color: red; }
      .curser_pt {
        cursor: pointer;
        background: url(assets/img/sort_icon.png) no-repeat 5px center;
        padding: 0 20px;
      }
            .plan_list_view p {
    display: inline-block;
}
            .table_inner_head_acc {
                background: #555 !important;
                /*box-shadow: 0 0 10px 2px #95A5A6;*/
            }
            .table_inner_head_acc th{
                color:#555;
            }
            .table_inner_head_acc th:nth-child(4){
                width: 400px;
            }
            .accordion {
                cursor: pointer;
            }

            .status_bar th:nth-child(4){
                min-width: 400px;
            }
            .status_bar th:nth-child(5){
                min-width: 200px;
            }
            
.status_bar tbody tr:last-child {
    background: #ccc none repeat scroll 0 0 !important;
}
            .heart_beat {
                /*background: #7ED5E0;
                animation: beat .25s infinite alternate;
                transform-origin: center;
                background: none;*/
                margin-right: 5px;
            }
            @keyframes beat {
                to {
                    transform: scale(1.4);
                    box-shadow: 0px 0px 10px 2px #D24D57;
                    border-radius: 80%;
                }
            }
            .prev,
            .next,
            .current-page {
                margin: 0 5px;
                font-weight: bold;
                background: #5D85B2;
                color: white;
                padding: 0px 1px;
                border-radius: 3px;
                text-decoration: none;
                display: inline-block;
            }
            .ms-options{
                width: 700%
            }

    .status_bar tr, .status_bar th, .status_bar td {
    vertical-align: middle;
    height: 30px;
    min-width: 150px;
    line-height: 20px;
    width: 11%;
    word-break: break-all;
    max-width: 300px;
}
 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
          .pagination ul>.active>a, .pagination ul>.active>span {
    color: #999999;
    cursor: default;
  }
</style> 
</head>
<body ng-app="postApp" ng-controller="postController" ng-cloak>

        <?php $this->load->view('header.php'); ?>
      <!-- Loader Image -->
      <div ng-show="showLoader" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><!-- so this div containing img will be dislpayed only when the showLoader is equal to true-->
          <img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" style="margin-left:50%;margin-top:25%"> 
      </div>



    <!-- spinner old place -->
    <div class="container header_bg_clr">
        <div class="wrapper">
            <div class="header_main">
               <?php echo "<h2>".$screen."</h2>" ;?>
            </div>
        </div>
    </div>      
                <div id="msg" style="display: none;"></div>
    <div class="container">
        <div class="wrapper">
                    <ul>
                        <br/>    
                         <button id="btnExport" class="export_button" ng-click="exportDataExcel()">Export to Excel</button>
                    </ul>
        </div>
        </div>
        <div class="container" ng-cloak>
            <div class="wrapper">

                <div class="content_main_all">
                        <div class="plan_list_view" id="table_wrapper">
                            <table class="research status_bar" cellspacing="0" border="1">
                                <thead>
                                    <tr class="tab1subtable">
                                        <td class="curser_pt" ng-click="sortBy('MyPartNo')"><span>My Part #</span></td>
                                        <td class="curser_pt" ng-click="sortBy('MySKUs')"><span>My SKU</span></td>
                                        <td class="curser_pt" ng-click="sortBy('MyItems')"><span>My ItemIDs</span></td>
                                        <td class="curser_pt" ng-click="sortBy('CompItemID')"><span>Interchange</span></td>
                                        <td class="curser_pt" ng-click="sortBy('CompSKU')" style="background-color:#2C3E50;color:white;"><span>Competitor SKUs</span></td>
                                        <td class="curser_pt" ng-click="sortBy('CompPartNo')" style="background-color:#2C3E50;color:white;"><span>Competitor PartNo</span></td>
                                    </tr>
                                    <tr class="tab1table">
                                       <td>
                                            <div class="search">
                                              <input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch" placeholder="My Part#">
                                            </div> 
                                       </td>
                                       <td>
                                           <div class="search">
                                             <input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch" placeholder="SKU">
                                           </div>
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="itemquery" ng-change="myitemsearch()" class="inputsearch" placeholder="My Items">
                                           </div>
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="interquery" ng-change="intersearch()" class="inputsearch" placeholder="Interchange">
                                           </div>
                                       </td>
                                       <td>
                                           <div class="search">
                                              <input type="text" ng-model="compskuquery" ng-change="compskusearch()" class="inputsearch" placeholder="Competitor SKUs">
                                           </div>  
                                       </td>
                                       <td>
                                            <div class="search">
                                               <input type="text" ng-model="comppartquery" ng-change="comppartsearch()" class="inputsearch" placeholder="Competitor PartNo">
                                            </div>
                                        </td>
                                      </tr>
                                </thead>
                                <tbody>
                                        <tr ng-repeat="data in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                        <!-- <tr ng-repeat="data in interdata | filter:search| orderBy:propertyName:reverse "> -->
                                        <td>{{data.MyPartNo}}</td>
                                        <td>{{data.MySKUs}}</td>
                                
                                        <td>
                                           <div ng-repeat="elem in (data.MyItems | customSplitString)">
                                             <a ng-click="redirectToLink($event)" element="{{elem}}" style="cursor:pointer">{{ elem }}</a>
                                           </div>
                                        </td>
                                        <td>
                                          <!-- <div id="name" ng-repeat="(key,element) in (data.CompItemID | customSplitString)">
                                            <div ng-repeat="(subkey,subelement) in (data.CompName | customSplitString)">
                                                <a ng-if="key == subkey" ng-click="redirectToLink($event)" element="{{element}}" style="cursor:pointer" once-bind="{{subelement}} ({{element}})"></a>
                                            </div>
                                          </div> -->
                                         <div id="name" ng-repeat="(key,element) in (data.CompItemID.split(','))">
                                                <a ng-click="redirectToLink($event)" element="{{element}}" style="cursor:pointer">{{data.CompName.split(',')[key]}} ({{element}})</a>
                                          </div>
                                        </td>
                                        <td ng-if="data.CompSKU">{{data.CompSKU}}</td>
                                        <td ng-if="!data.CompSKU">N/A</td>
                                        <td ng-if="data.CompPartNo">{{data.CompPartNo}}</td>
                                        <td ng-if="!data.CompPartNo">N/A</td>
                                        </tr>


                                </tbody>
                            </table>
                            
               
                        </div>


                                      <!-- pagination ui part -->
                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>
                        </div>


                        <!-- <div style="min-height: 300px;">
                        </div>
 -->
</div>
                </div>
                
             <?php $this->load->view('footer.php'); ?>
        </div>


        

      <!--  <script src="<?php echo base_url().'assets/';?>js/jquery-1.11.3.min.js"></script>
         <script src="<?php echo base_url().'assets/';?>js/jquery.min.js"></script>
        <script src="<?php echo base_url().'assets/';?>js/jquery-ui.js"></script>
      <script src="<?php echo base_url().'assets/';?>js/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url().'assets/';?>js/portBox.min.js"></script>
        <script src="<?php echo base_url().'assets/';?>js/portBox.slimscroll.min.js"></script>
        <script src="<?php echo base_url().'assets/';?>js/prettify.js"></script> 
        <script src="<?php echo base_url().'assets/';?>js/dropdown_core.js"></script>
        <script src="<?php echo base_url().'assets/';?>js/dropdown_lob.js"></script>
        <script src="<?php echo base_url().'assets/';?>js/jquery.multiselect.js"></script> 
         <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular-sanitize.js"></script> -->

        <script>
            //new popup msg close button
         $(document).on('click','.destroy',function(){
             $(".err_modal").css("display", "none");   
        });
        </script>
       
         <script>
           var exportdata_excel = [];
              var sortingOrder = 'MyPartNo';
              var app = angular.module("postApp", []);
              app.controller('postController', function($scope, $timeout,$http,$window,$filter) {
                    $scope.sortingOrder = sortingOrder;
                    $scope.reverse = false;
                    $scope.filteredItems = [];
                    $scope.groupedItems = [];
                    $scope.itemsPerPage = 25;
                    $scope.pagedItems = [];
                    $scope.pagenos = [];
                    $scope.interdata = '';
                    $scope.currentPage = 0;

                 $scope.parseInt = parseInt; 
                 $scope.showLoader = true;  //Loader Image 
                    $http.get("<?php echo base_url();?>index.php/Interchange/interchange_get")
                               .success(function (response) {
                                   $scope.interdata = response;
                                  

                                    //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.interdata, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "MyPartNo") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.myskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.interdata, function (item) {
                                    for(var attr in item) {
                                    if(attr == "MySKUs") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.myitemsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.interdata, function (item) {
                                    for(var attr in item) {
                                     if(attr == "MyItems") {
                                            if (searchMatch(item[attr], $scope.itemquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.intersearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.interdata, function (item) {
                                    for(var attr in item) {
                                    if(attr == "CompItemID" || attr == "CompName") {
                                            if ((searchMatch(item[attr], $scope.interquery)  > -1) || (searchMatch(item[attr], $scope.interquery)  > -1)){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.compskusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.interdata, function (item) {
                                          for(var attr in item) {
                                            if(attr == "CompSKU") {
                                            if (searchMatch(item[attr], $scope.compskuquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.comppartsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.interdata, function (item) {
                                        for(var attr in item) {
                                            if(attr == "CompPartNo") {
                                            if (searchMatch(item[attr], $scope.comppartquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end

                              $scope.search = {};
                              $scope.showLoader = false; //Loader Image 
                    //currently working

                     /*<div id="name" ng-repeat="(key,element) in (data.CompItemID.split(','))">
                        <a ng-click="redirectToLink($event)" element="{{element}}" style="cursor:pointer">{{data.CompName.split(',')[key]}} ({{element}})</a>
                    </div>*/

                    //export to excel data render
                    $scope.interdata.forEach(function (part) {
                        var compnameandid = '';
                        var compskus = '';
                        var comppartnos = '';
                        if(part.CompItemID !== null){
                            angular.forEach(part.CompItemID.split(','), function(value, key){
                                  compnameandid +=  part.CompName+'('+value+')';
                            });
                        }
                        if(part.CompSKU !== null){
                            compskus = part.CompSKU;
                        }
                        if(part.CompPartNo !== null){
                            comppartnos = part.CompPartNo;
                        }
                        exportdata_excel.push({'MyPartNo' : part.MyPartNo,'My SKU' : part.MySKUs,'My ItemIDs' : part.MyItems,'Interchange' : compnameandid,'Competitor SKUs' : compskus,'Competitor PartNo':comppartnos});
                    });
                    $scope.ForExcelExport = exportdata_excel;
                   
                   })
                    .catch(function (err) {
                         var msg =err.status;
                          //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                        //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.showLoader = false;
                                    });
                     //$scope.isLoading = false;
                  })

                     //export to excel        
                    $scope.exportDataExcel = function () {
                        var mystyle = {
                            sheetid: 'Interchange',
                            headers: true,
                            column: { style: 'background:#a8adc4' },
                            };
                            alasql('SELECT * INTO XLS("DIVE_Interchange.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                        };   
                

                       
                
                    //Click the button to open an new browser window that is 1200px wide and 600px tall
                       $scope.redirectToLink = function (obj) {
                        var url = "http://www.ebay.com/itm/"+obj.target.attributes.element.value;
                        $window.open(url, '_blank','width=1200,height=600"');
                    };

                    });
                    

              app.filter('customSplitString', function() {
                    return function(input) {
                     var arr = input.split(',');
                      return arr;
                    };
                   });

              app.filter('unique', function() {
                     return function(collection, keyname) {
                        var output = [], 
                            keys = [];

               angular.forEach(collection, function(item) {
                      var key = item[keyname];
                        if(keys.indexOf(key) === -1) {
                             keys.push(key);
                             output.push(item);
                        }
                    });

                      return output;
                  };
              });



            </script>
         
        <!-- Export to Excel-->
        <!-- <script  type="text/javascript">
                  $(document).ready(function()  {

                  var currentdate = new Date(); 
                  var datetime = currentdate.getDate() + "/"
                          + (currentdate.getMonth()+1)  + "/" 
                          + currentdate.getFullYear() + "-"  
                          + currentdate.getHours() + ":"  
                          + currentdate.getMinutes() + ":" 
                          + currentdate.getSeconds();

                    $("#btnExport").click(function(e) {
                      e.preventDefault();
                      $('.tab1table').remove();
                      var data_type = 'data:application/vnd.ms-excel';
                      var table_div = document.getElementById('table_wrapper');
                      var table_html = table_div.outerHTML.replace(/ /g, '%20');
                      var a = document.createElement('a');
                       $('.tab1subtable').after(' <tr class="tab1table"><td><input type="text" class="inputsearch" placeholder="My Part #" ng-model="search.MyPartNo"></td><td><input type="text" class="inputsearch" placeholder="SKU" ng-model="search.MySKUs"></td><td><input type="text" class="inputsearch" placeholder="Listings" ng-model="search.MyItems"></td><td><input type="text" class="inputsearch" placeholder="Interchange" ng-model="search.MyID"></td><td><input type="text" class="inputsearch" placeholder="Competitor SKUs" ng-model="search.CompSKU"></td><td><input type="text" class="inputsearch" placeholder="Competitor PartNo" ng-model="search.CompPartNo"></td></tr>');
                      a.href = data_type + ', ' + table_html;
                      a.download = 'DIVE_Interchange_'+datetime+'.xls';
                      a.click();
                    });

               
                  });
             
         </script> -->
    <script type="text/javascript">
        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
</body>
</html>