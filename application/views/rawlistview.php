<!DOCTYPE html>
<html ng-app="postApp">
<head>
    <meta charset="utf-8"/>    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>


    </title>
             <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <link href="/assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon"/> 
    <!-- <link rel="stylesheet" href="<?php echo base_url().'assets/';?>Romaine/css/style.css"/>   -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/jquery-ui.css"/>
    <link rel="stylesheet" href="<?php echo base_url().'assets/';?>css/popup.css"/>
    <!-- <link rel="stylesheet" href="<?php echo  base_url();?>assets/price_css/style.css" /> --> 
    <link rel="stylesheet" href="<?php echo  base_url();?>assets/css/style_romaine.css" />
  <!--   <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">    --> 
    <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>   
   <!--  <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script> -->
    <!-- <script src="http://netsh.pp.ua/upwork-demo/1/js/typeahead.js"></script>  -->
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>    
    <!--  <script src="https://js.pusher.com/4.1/pusher.min.js"></script> --><!--pusher notification -->
</head>
<style>

 .pagi_master li{
                list-style-type: none;
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    
    border-radius: 3px;
    cursor: pointer;
        }

        .pagi_master li a{
                color: #fff;
    text-decoration: none;
    padding: 4px;
        }
    .pagination ul>.active>a, .pagination ul>.active>span {
    color: #999999;
    cursor: default;
    }
  
</style> 
<body class="main" ng-controller="postController" ng-cloak>
<?php


    function getGUID() {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid,20,12)
                .chr(125);// "}"
            return $uuid;
        }
    }
    $result = getGUID();
    $resultval = str_replace('{','',$result);
    $GUID = str_replace('}','',$resultval);
// echo $GUID;

    $datetime = date('Y-m-d H:i:s');
    $time = strtotime($datetime);
?>
<!-- loading gif-->
<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>
     <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
<style type="text/css">
    div .ms-options{
        width:280%;
    }
/*.status_bar td:nth-child(7),.status_bar th:nth-child(7){
    border:none;
    border-bottom:1px solid #e1e1e1;
    border-top:1px solid #e1e1e1;
    text-align:right;
    width:5px;
    min-width:5px;
    max-width:5px;
    font-size:13px;
    padding:0 2px !important;
}
.status_bar td:nth-child(8),.status_bar th:nth-child(8){
    text-align:right;
}*/

.slimtable-page-btn {
    display: inline-block;
    margin: 0 3px;
    background: #5D85B2;
    color: #fff;
    padding: 0 4px;
    border-radius: 3px;
    cursor:pointer;
}
.slimtable-page-btn:hover {
    background:#187ed5; 
}
.slimtable-page-btn.active {
    background:#187ed5; 
}
.slimtable-paging-btnsdiv{
    float:right;
}
/*.status_bar tr, .status_bar th, .status_bar td{
    width: 10%;
}*/

/*.status_bar thead th:nth-child(6){
    min-width: 600px;
    width: 600px;
    max-width: 600px;
}
.status_bar thead th:nth-child(2){
    min-width: 150px;
    width: 150px;
    max-width: 150px;
}
.status_bar thead th:nth-child(4){
    min-width: 150px;
    width: 150px;
    max-width: 150px;
}*/

ul,li { margin:0; padding:0; list-style:none;}
.label { color:#000; font-size:16px;}
.field1{
    display: inline-block;
    float: left;
    padding: 0 10px 0 0px;
}
.shwpopup
{
    display:none;
    width: 50%;
    margin: 50%;
    position: fixed;
    z-index: 1;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
    left: 0;
    top: 0;
}
.field{
    width:auto;
}
.ckeckclass
{
    margin:2px;
}
.divTable{
    float: left;
    width: 70px;
    height: 40px;
    border-right: 1px solid #e1e1e1;
    padding-top: 28px;
    padding-left: 7px;
    
}
.lastdivTable{
    float: left;
    width: 70px;
    padding-top: 28px;
    padding-left: 7px;
    
}
.paging-nav {
  text-align: right;
  padding-top: 2px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #5D85B2;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

tfoot
{
text-align: center !important;
display: table-row-group !important;
}

.paging-nav,
#tblrawresult {
 
  margin: 0 auto;
  font-family: Arial, sans-serif;
}

.select {
    padding: 2px;
    float: right;
    width: 155px;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}

.selectpage {
    padding: 2px;
    width: 76;
    border: 1px solid #bebcbc;
    border-radius: 2px;
    font-size: 12px;
    height: 29px !important;
    margin-bottom: 10px;
    color: #505050;
    background-color: #fff;
    font-family: sans-serif, Verdana, Geneva !important;
}



/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 10% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 30%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: #fff;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #2d8bd5;
    text-decoration: none;
    cursor: pointer;
}
.itemIDCell
{
    margin:0;
}

@media only screen and (max-width: 1500px) {
    
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }



/**********autocomplete suggestion***************/
#searchResult{
 /*list-style: none;
 display :block;
 padding: 0.1%;
 width: 6.9%;
 position: absolute;
 margin: 0;
 margin-left: 6.7%*/
   /* padding: 0;
    border: 1px solid black;
    max-height: 7.5em;
    overflow-y: auto;
    width: 8.1%;
    position: absolute;
    margin-left: 8.2%;*/
        border: 1px solid #aaa;
    overflow-y: auto;
    width: 172px;
    position: absolute;
    margin-left: 143px;
    padding-bottom: 0px;
    min-height: 10px;
    max-height: 150px;
}

#searchResult li{
 background: white;
 padding: 4px;
cursor: pointer;
    list-style: none;
    border-bottom: 1px solid #ededed;
}

#searchResult li:hover{
    background: #f1f1f1;
}
/**************/

/*#competitorname option[value='? string:91 ?']{
    display: none;
}*/

</style>
    <div class="container header_bg_clr posi_rela">
        <div class="wrapper">
            <div class="header_main">
                <div class="head_menu">
             <?php echo "<h2>".$screen."</h2>" ;?><!-- <span style="padding-left:40%; color:red; font-size:14px"></span> -->
                </div>
            </div>
        </div>
    </div>  
    <div id="msg" style="display: none;"></div>
             
    </div>
     <!-- error handling popup-->
          <div class="progrss_confirm" >
         <div id="success" class="err_modal1" style="display: none;">
             <div class="alert_box" class="popup" class="modal-content" >
                     <div class="fleld-alert success"> 

                           <div class="head_icon_green" style="background: #1376AF none repeat scroll">
                              <img src="<?php echo base_url();?>/assets/img/success_icon_blue.png"  alt="Logout" title="Logout" class="destroy" />
                          </div>

                            <div class="alert alert-warning" style="padding: 0% 0px;">
                            <!-- <strong  class="msgstatic content_dynamic" style="font-size: 19px;padding:0px 0px">Searching eBay for Competitors for the Keyword</strong> -->
                             <strong  class="msgprogress" style="font-size: 19px;padding:0px 0px"></strong>
                            <strong class="msgprogressitem" style="font-size: 19px;padding:0px 0px"></strong>
                            <strong  class="filterprogress" style="font-size: 19px;padding:0px 0px"></strong>
                           <!--  <strong class="msgprogress"></strong> -->
                           <!-- <button >Cancel</button> -->
                           <div class="alert_ok_btn butn_hide" id="cancel" style="display:none">
                              <button ng-click="cancelfun()" class="btn_ok_sty_green" style="background-color:rgba(179, 0, 0, 0.84);padding: 6px 25px;left: 40%;" >Stop</button>
                            </div>
                             <p></p>
                            </div>
                                
                                    

                          <!--  <div class="alert_ok_btn butn_hide">
                              <button class="btn_ok_sty_green clck" ng-model="msghide" style="margin-left: -57px;">Yes</button>
                            </div>
                            <div class="alert_ok_btn butn_hide">
                              <button class="btn_ok_sty_green nonclck" style="margin: -16px 0 0px 26px;background-color:#b30000;left: 46%;">No</button>
                            </div> -->

                           </div>
                      </div>
                   </div>
                   </div>
                   <!-- -->
                 <!-- error handling popup end-->   
    <div class="container">
        <div class="wrapper">
        <div class="content_main_all">
            <form id="rawlistform" ng-submit="submitForm()">
            <div class="form_head">
                <div class="field">
                    <div class="lable_normal">
                        Competitor Details <span style="color:red; font-size:16px;">*</span>  :  
                    </div>
                            
                    <select  name="competitorname[]" ng-options="opt.CompetitorName for opt in renderselectvalue track by opt.compid" ng-selected="selected"  class="select" ng-model="competitorname" id="competitorname" ng-click="freearray1()" ng-change="ngcompetitor()">
                             <option value="">--Select--</option>
                      </select> 
                </div>
                <div class="field">
                    <div class="lable_normal" >
                        Category Details  <span style="color:red; font-size:16px;">*</span>  : 
                    </div>
                  
                     <select  name="category[]" ng-options="::subcat.cat for subcat in subcategories track by ::subcat.Categoryid" ng-model="competitorcategory" id="compcat" class="select">
                      <option value="" ng-selected >-None-</option>
                      </select> 
                      <!--  <select  name="category[]" ng-options="subcat.cat for subcat in subcategories track by subcat.Categoryid" ng-model="competitorcategory" id="compcat" class="select">
                      <option value="">-None-</option>
                      </select> -->
                    
                </div>
                
                <div class="field1"><input value="Show" id="btnsearch" name="btnsearch" class="button_search_tab1" type="submit"/></div>
                <!-- <button value="Search" id="filterbtn" name="filterbtn" class="button_normal" type="submit" >Apply Filters</button>  -->
                <div class="filed_raw_check"><input id="applyfilter" ng-model="ischeckedpart" ng-click="applyfilter($event)" type="checkbox" value="1"/> Apply Filters</div>
                <input type="hidden" name="new_hidden_id" id="new_hidden_id" ng-model="new_hidden_id" value="<?php echo $GUID; ?>" />
               

             </form>
 
    

      
            <form id="searchform" class="raw_list_sec_form" ng-submit="submitForm()">
            <!-- <form id="searchform" ng-submit="searchForm()"> -->
                <div class="field">
                    <div class="lable_normal_sec" >
                    <input type="hidden" name="hidden_id" id="hidden_id" ng-model="hidden_id" value="">

                        Search For <span style="color:red; font-size:16px;">*</span>  : 
                    <input type="textbox" name="searchebay" id="searchebay" ng-model="searchebay" value="" placeholder=" Enter Title/MPN/IPN/OPN " ng-click="freearray()" >
                   </div>
                </div>
                <input type="hidden" name="mycheckbox" ng-model="mycheckbox" value=""/>
                <button value="Search" id="srchbt" name="srchbt" class="button_search" type="submit" >Search</button>
                &nbsp;&nbsp;&nbsp;
                <div class="lable_normal">
                    Items Count Per Search :  
                </div>
                <select class="selectpage" name="pagecount" ng-model="pagecount" id="pagecount">
                             <option value="">--Select--</option>
                             <option value="10">10</option>
                             <option value="25">25</option>
                             <option value="50">50</option>
                             <option value="75">75</option>
                             <option value="100">100</option>
                             <option value="0">All</option>
                </select>
                <!-- <div class="lable_normal" style="color: #28a1c0;font-size: 15px">
                    (Default : 25)  
                </div> -->
     <center> <div class="error_msg_all" style="display:none" id="ebay_msg_id">Please Enter Title/MPN/IPN/OPN </div></center> 
                 <center> <div class="error_msg_all"  style=" display:none" id="msg_id">Please select Competitor & Category</div> </center>    
                 <center> <div class="error_msg_all"  style=" display:none" id="invalidkeyword">No Matching Result. Try Different Keywords</div> </center>           
            </form>
     
            <div class="button_right">

                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                                <input type="button" class="export_button" ng-click="exportDataExcel()" value="Export As Excel"/>
                            </a>
                            <ul class="level2">

                                <!-- <li ><a herf="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_RawList.csv">As CSV<a/></li> -->    
                                <!-- <li><a ng-href="<?php echo base_url();?>Rawlist/export" class="last_menu_icon">As Pdf<a/></li> -->
                                <!-- <li><input type="button" ng-click="exportpdf()" class="last_menu_icon" value="Export"/></li> -->

                            </ul>
                        </li>
                </div>
    </div>
     
      <div style="color:#009e47; text-align:center; font-size:20px">{{message}}</div>
      <br/>
      <center><div class="addbtn" style="color:red; text-align:center; display:none">Please Select Atleast One</div></center>
        <!-- <form ng-submit="addtomasterlist()" id="addform" style="min-height:800px;"> -->
            <form ng-submit="addtomasterlist()" id="addform">
                <div class="plan_list_view">
                    <div class="clear"></div>

<div class="lable_normal table_top_form"><input id="ckbCheckAll" type="checkbox" ng-click="globalcheck()" ng-model="haschecked" />
                    Select All - Across Pages</div>

                    <div class="itm_lst" ng-repeat="incitem in inceptitem">
                           
                             <div class="itm_lst">
                                 <p>Total item listed:</p>
                                 <p style="font-size: 20px; color: #00886d;">{{incitem.TotalListings}}</p>
                            </div>
                             <div class="itm_lst"> 
                                 <p>Total Sales Since Inception:</p>
                                 <p style="font-size: 20px; color: #00886d;">${{incitem.TotalSales}}</p>
                            </div>
                            <div ng-if="incitem.InceptionDate" class="itm_lst">
                                 <p>Inception On:</p>
                                 <p style="font-size: 20px;color: #00886d;">{{incitem.InceptionDate}}</p>
                            </div>
                            <div ng-if="!incitem.InceptionDate" class="itm_lst">
                                 <p>Inception On:</p>
                                 <p style="font-size: 20px;color: #00886d;">N/A</p>
                            </div>
                        </div>

                    <table id="location" cellspacing="0" border="1" class="status_bar">
                        <thead>
                            <tr>
                                <th>Sel.</th>
                                <th class="curser_pt" ng-click="sortBy('CompetitorName')">Competitor</th>
                                <th class="curser_pt" ng-click="sortBy('ItemID')">Item ID</th>
                                <th class="curser_pt" ng-click="sortBy('SKU')">SKU</th>
                                <th ng-click="sortBy('mypartno')">My Part #</th>
                                <th class="curser_pt" ng-click="sortBy('Title')">Title</th>
                                <th class="curser_pt" ng-click="sortBy('CurrentPrice')">Price (in $)</th>
                                <th class="curser_pt" ng-click="sortBy('ListingDate')">Listing Date</th>
                                <th class="curser_pt" ng-click="sortBy('QuantitySold')">Qty.Sold (Inc)</th>
                                <th class="curser_pt" ng-click="sortBy('lastweek_Qtysold')">
                                    Qty.Sold (Last Wk)
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
        <tr>
         <td><input id="selpage" type="checkbox" ng-click="selectpage()" ng-model="selhaschecked" class="ckeckclass" /></td>
         <td><div class="search"><input type="text" ng-model="query" ng-change="mysearch()" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="itemquery" ng-change="myitemsearch()" class="inputsearch" Item ID"></div></td>
         <td><div class="search"><input type="text" ng-model="skuquery" ng-change="myskusearch()" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="partquery" ng-change="mypartsearch()" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="titlequery" ng-change="mytitlesearch()" class="inputsearch" ></div></td>
         <td></td>
         <td><div class="search"><input type="text" ng-model="pricequery" ng-change="mypricesearch()" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="listingquery" ng-change="listingsearch()" class="inputsearch"></div></td>
         <td></td>         
         </tr>

         <!-- <tr>
         <td><input id="selpage" type="checkbox" ng-click="selectpage()" ng-model="selhaschecked" class="ckeckclass" /></td>
      <!-- <td><div class="search"><input type="text" ng-model="search.CompetitorName" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="search" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="search.ItemID" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="search.SKU" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="search.mypartno" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="search.Title" class="inputsearch" ></div></td>
         <td></td>
         <td><div class="search"><input type="text" ng-model="search.ListingDate" class="inputsearch"></div></td>
         <td><div class="search"><input type="text" ng-model="search.QuantitySold" class="inputsearch"></div></td>
         <td></td>         
         </tr> -->
                         </tfoot>   
                                <tr ng-repeat="item in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                    <td><input type="checkbox" ng-model="item.selected" value="{{item.ItemID}}" name="addtomasterlist[]" id="{{item.ItemID}}" class="ckeckclass" ng-checked="item.Mlflag==1" ng-disabled="item.Mlflag==1" value=""></td>
                                    <td id="comp" headers="Competitor">{{item.CompetitorName}}</td>
                                    <td id="itemid" headers="Item ID">{{item.ItemID}}</td>
                                    <td id="sku" headers="SKU">{{item.SKU}}</td>
                                    <td id="partno" header="My Part#" ng-if="item.mypartno" class="mymodalmaintable" data-value="{{item.ItemID}}" my-partnumber="{{item.mypartno}}" my-compid="{{item.CompID}}" id="mypartnumber{{item.mypartno}}" ><a href="#" style="color: #0094DE;" id="updatedpartnumber{{item.ItemID}}" >{{item.mypartno}}</a></td>
                                    <td id="partno" headers="My Part #" ng-if="!item.mypartno" class="mymodalmaintable" data-value="{{item.ItemID}}" my-partnumber="{{item.mypartno}}" my-compid="{{item.CompID}}" id="mypartnumber{{item.mypartno}}" ><a href="#" style="color: #0094DE;" id="updatedpartnumber{{item.ItemID}}" >N/A</a></td>
                                    <td id="title" headers="Title"><a ng-click="redirectToLink($event)" element="{{item.ItemID}}" style="cursor: pointer;" >{{item.Title}}</a></td>
                                    </td>
                                    <td id="price" headers="Price(in $)">{{item.CurrentPrice}}</td>
                                    <td id="ldate" headers="Listing Date">{{item.ListingDate}}</td>
                                    <td id="qsinc" headers="Qty.Sold (Inc)">
                                    <!-- <span ng-if="item.CompetitorName.includes('IBR') && !item.QuantitySold" >N/A</span>
                                    <span ng-if="item.CompetitorName.includes('CBR') && !item.QuantitySold" >{{item.QuantitySold}}</span>
                                     -->{{item.QuantitySold}}
                                    </td>
                                    <td id="qslst" headers="Qty.Sold (Last Wk)">
                                        <span ng-if="item.CompetitorName.includes('[IBR]') && !item.lastweek_Qtysold" >N/A<br></span>
                                        <span ng-if="item.CompetitorName.includes('[CBR]') && !item.lastweek_Qtysold" >{{item.lastweek_Qtysold}}</span>
                                        <span ng-if="item.lastweek_Qtysold" >{{item.lastweek_Qtysold}}</span>
                                    </td>
                                </tr>
                        </table>
                </div>  
                
                     <!-- pagination ui part -->
                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>
                        </div>


                <div style="text-align:center;">
                    <input class="button_add" value="Add To Master List" type="submit" ng-hide="countChecked() == 0" />
                </div>
            </div>
            </form> 
<!-- popup start -->    
        <!-- ********************************** -->
                                    <form id="popupform" ng-submit="submitmainForm()">
                                        <div id="Pnopopup" class="modal">
                                               <div class="pop_map_my_part">
                                                
                                                 <!-- <center><div class="successupdate" style="display:none;">Update Successfully</div></center> -->
                                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                                 <center><div class="pnum" style="display:none;">Please Enter Part Number</div></center>
                                                 
                                                 <div class="pop_container">
                                                            <div class="heading_pop_main">
                                                                <h4>
                                                                    Map my Part #
                                                                </h4>
                                                                <span id="myPnopopup{{item.ItemID}}" class="close">&times;</span>
                                                            </div>
                                                            <div class="pop_pad_all">
                                                                <div class="field_12">
                                                                    <div class="lable_normal">
                                                                        <label class="normal_label">Item ID</label>
                                                                        <label id="displayitemid"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="field_12">
                                                                                    <div class="lable_normal">
                                                                                      <label class="normal_label">My Part Number </label>
                                                    <input type="hidden" id="displaycompidhidden" name="maincompetitorid" ng-model="mainpopup.maincompetitorid" value=""/>
                                                    <input type="text" id="runpartnumber" name="maincompitemid" ng-change="partnosearch()" ng-model="searchpart.partno" value="" autocomplete="off"/>
                                                           <!--  <input type="text" id="runpartnumber" ng-model="searchpart"/>  ng-if="searchresultload"-->

                                                    <ul id='searchResult'>
                                                       <li ng-click='setValue(result.partno)' ng-repeat="result in suggest_partno | filter : searchpart" >{{ result.partno }}</li>
                                                    </ul>

                                                    <input type="hidden" name="mainmypartnumber" ng-model="mainpopup.maincompetitoritemid" id="displayitemidhidden" value=""/>
                                                    

                                                            <div class="button_center">
                                                                    <img style="display:none;" class="ajaxloader" src='<?php echo base_url().'assets/';?>img/ajax-loader.gif'/>
                                                                    <button type="submit" id="popsavemain" class="button_upload">Save</button>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                            </div>
                                                </div>
                                               </div>
                                            </div>
                                    </form>


                                     <div id="Filterpopup" class="modal">
                                               <div class="pop_apply_filter">
                                                
                                                
                                                <div class="pop_container">
                                                <div class="heading_pop_main">
                                                    <h4>Select Filters</h4>
                                                    <span id="filterclose" class="close">&times;</span>
                                                </div>
                                                    <!-- <table id="location" class="status_bar" cellspacing="0" border="1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Sel.</th>
                                                                            <th class="curser_pt">Filter Id</th>
                                                                            <th class="curser_pt">Filter Name</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tfoot>
                                                                       <tr>
                                                                            <td><input class="ckeckclass" type="checkbox"></td>
                                                                            <td>
                                                                              <div class="search"><input class="inputsearch_pop" type="text"></div>
                                                                            </td>
                                                                            <td>
                                                                               <div class="search">
                                                                               <input class="inputsearch_pop" type="text">
                                                                               </div>
                                                                            </td>
                                                                     </tr>
                                                                     </tfoot>   
                                                                            <tbody>
                                                                            <tr>
                                                                                <td><input class="ckeckclass" checked="checked" type="checkbox"></td>
                                                                                <td>F0001</td>
                                                                                <td>Filter 1</td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td><input class="ckeckclass" checked="checked" type="checkbox"></td>
                                                                                <td>F0001</td>
                                                                                <td>Filter 1</td>
                                                                            </tr>
                                                                    </tbody></table>

                                                                    <div class="pop_up_go">
                                                                        <button class="btn_go_green">go</button>
                                                                      </div> -->
                                                 <iframe src = "<?php echo base_url();?>Filters/filter_popup" width = "100%" height = "220">
                                                </iframe>
                                                </div>
                                               
                                                 
                                            </div>

                                     </div>
        </div>
    </div> 

    
   

                <?php $this->load->view('footer.php'); ?>
 </body>
</html>


<script>

 $('#compcat').click(function(e){ 
      if ($('#competitorname').val() != ""){
        $("#invalidkeyword").css("display","none");
          document.getElementById('competitorname').style.borderColor = ""; 
          }
         });
$('#btnsearch').click(function(e){ 
    $("#ebay_msg_id").css("display","none");
    $("#invalidkeyword").css("display","none");
                document.getElementById('searchebay').style.borderColor = "";
    
      
      if ($('#competitorname').val() == "" || $('#compcat').val() == "") {
        $("#invalidkeyword").css("display","none");
                 var competitorname=document.getElementById('competitorname').value;
                   if(competitorname == ""){
                        document.getElementById('competitorname'). style.borderColor = "red";
                        $("#msg_id").css("display","block");
                        //return false;
                    }
                    else
                     {
                       document.getElementById('competitorname').style.borderColor = ""; 
                       $("#invalidkeyword").css("display","none");
                    }

                  var compcat=document.getElementById('compcat').value;
                   if(compcat == ""){
                        document.getElementById('compcat').style.borderColor = "red";
                        $("#invalidkeyword").css("display","none");
                        $("#msg_id").css("display","block");
                        return false;
                    }  
                   /* $("#msg_id").css("display","block");
                    $('.addbtn').css('display','none');
                    $("#ebay_msg_id").css("display","none");
                    $("#success_msg").css("display","none");*/
                    e.preventDefault();
                  
     
            }
            else
            {   $("#msg_id").css("display","none");
          $("#invalidkeyword").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                document.getElementById('compcat').style.borderColor = "";   
                 /* $("#msg_id").css("display","none");
                    $('.addbtn').css('display','none');
                    $("#ebay_msg_id").css("display","none");
                    $("#success_msg").css("display","none");*/
            }
        });

 $('#srchbt').click(function(e){ 
                $("#msg_id").css("display","none");
                $("#invalidkeyword").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                document.getElementById('compcat').style.borderColor = ""; 
      
            if ($('#searchebay').val() == "" ) {
                    var searchebay=document.getElementById('searchebay').value;
                    $("#invalidkeyword").css("display","none");
                   if(searchebay == ""){
                        document.getElementById('searchebay').style.borderColor = "red";
                        $("#ebay_msg_id").css("display","block");
                        return false;
                    }

                 
                   /* $("#msg_id").css("display","block");
                    $('.addbtn').css('display','none');
                    $("#ebay_msg_id").css("display","none");
                    $("#success_msg").css("display","none");*/
                    e.preventDefault();
                  
     
            }
            else
            {   $("#ebay_msg_id").css("display","none");
          $("#invalidkeyword").css("display","none");
                document.getElementById('searchebay').style.borderColor = "";

             
                 /* $("#msg_id").css("display","none");
                    $('.addbtn').css('display','none');
                    $("#ebay_msg_id").css("display","none");
                    $("#success_msg").css("display","none");*/
            }
        });
 $('#add_msg_hide').click(function(e){ 
              $("#ebay_msg_id").css("display","none");
              $("#invalidkeyword").css("display","none");
                document.getElementById('searchebay').style.borderColor = "";
                $("#msg_id").css("display","none");
                document.getElementById('competitorname').style.borderColor = "";
                document.getElementById('compcat').style.borderColor = ""; 
            });

</script>

<script>
    $('#searchebay').keypress(function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        $('#srchbt').trigger('click');
        //alert('hi');
    }
});
</script>

<script>
    $(document).ready(function() {
        $(document).on('click','.mymodalmaintable',function(){
            var datavalue = $(this).attr("data-value");
            var partno = $(this).attr('my-partnumber');
            var compid =$(this).attr('my-compid');
            $('#searchResult').hide();
            $('#runpartnumber').val(partno);  
            $("#Pnopopup").css("display", "block");
            $('#displayitemid').text(datavalue);
            $('#displayitemidhidden').val(datavalue);
            $('#displaycompidhidden').val(compid);
            /*$('#mainpartnumber').val(partno);*/
        });

          $(document).on('click','.close',function(){
          var idvalue = $(this).attr("id");
          $("#Pnopopup").css("display", "none");
          $('.pnum').css("display", "none");
          $('#searchResult').css("display", "none");
          $('.successupdate').hide();
          $('.failureupdate').hide(); 
          $('#runpartnumber').val('');     
          $("#Filterpopup").css("display", "none");
        });

        $('#applyfilter').click(function() {
        if(!$(this).is(':checked'))
        {
             $("#Filterpopup").css("display", "none");
        }
        else{
             $("#Filterpopup").css("display", "block");
        }
        });
        $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
        });
        $(document).on('click','.destroy2',function(){
          $(".err_modal").css("display", "none");
          $("#Pnopopup").css("display", "block");   
        });
    });
 </script>

<script>

  function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };

         //export partqueryt
        /*var compname = document.getElementById("comp").headers;
        var itemid = document.getElementById("itemid").headers;
        var sku = document.getElementById("sku").headers;
        var partno = 'My Part#';
        var title = document.getElementById("title").headers;
        var price = document.getElementById("price").headers;
        var ldate = document.getElementById("ldate").headers;
        var qsinc = document.getElementById("qsinc").headers;
        var qslst = document.getElementById("qslst").headers;*/
        
        var exportdata=[];
        var exportdata_exc = [];
        exportdata.push ({'CompetitorName' : 'Competitor','ItemID' : 'Item ID','SKU' : 'SKU','mypartno' : 'My Part#','Title' : 'Title','CurrentPrice' : 'Price (in $)','ListingDate':'Listing Date','QuantitySold':'Qty.Sold (Inc)','lastweek_Qtysold':'Qty.Sold (Last Wk)'});
        
            //var rawlistitemresult = '';
            var sortingOrder = 'CompetitorName';
            var postApp     = angular.module('postApp', ['ngSanitize', 'ngCsv']);

            postApp.controller('postController', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 25;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.currentPage = 0;
                $scope.searchresult = [];
                
                
                //Click the button to open an new browser window that is 1200px wide and 600px tall
                $scope.redirectToLink = function(obj){
                var url ="http://www.ebay.com/itm/" +obj.target.attributes.element.value;
                $window.open(url,'_blank','width=1200,height=600')
                };


                //category loading function
                $scope.ngcompetitor = function() {
                    $scope.competitorcategory="";
                    if($scope.competitorname == undefined){
                        return false;
                    }
                    else{
                        $scope.isLoading = true; //Loader Image
                    var value = $('#applyfilter').is(":checked") ? 1 : 0;
                    var key = 'applyfilter';
                    
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Rawlist/competitor_category',
                  data    : {'competitor':$scope.competitorname.compid, 'chkvalue':value, 'chkkey':key},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    $scope.isLoading = false; //Loader Image
                    //$scope.suggest_partno = data['partno_result'];
                    //$scope.filteredItems1 = $scope.suggest_partno;
                    var categorydata = [];
                    angular.forEach(data, function(value, key){
                        if(value.Category != ''){
                            categorydata.push({'Category' : value.Category, 'Categoryid' : value.Categoryid, 'cat' : value.cat});
                        }
                    });
                    $scope.subcategories = categorydata;
                  })
                  .catch(function (err) {
                        
                         var screenname = '<?php echo $screen;?>';
                         var msg = err.status;
                         //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        })
                    }
                };

                //search part no in popup window 
               /* $scope.searchpartno =function() {
                    if($scope.searchpart == ''){
                        $scope.searchresultload = false;
                        return false;
                    }
                    $http({
                        method  : 'POST',
                        url     : '<?php echo base_url();?>index.php/Rawlist/search_partno',
                        data    : {'searchpartno':$scope.searchpart},
                        header  : {'Content-Type': 'application/x-www-form-urlencoded'} 
                    })
                  .success(function(data) {
                    $scope.searchresultload = true;
                    $scope.searchresult = data;
                  })
                   .catch(function (err) {
                         var msg = err.status; 
                             //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                  data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                      
                                        $scope.isLoading = false;
                                    });
                        })
                }*/
                /*if($scope.searchpart == ''){
                        $scope.searchresultload = false;
                        return false;
                    }*/
                // Set value to search box
                $scope.setValue = function(index){
                    $('#runpartnumber').val(index);  
                    //$scope.searchresultload = true;
                    $('#searchResult').hide();   
                 }

                 $scope.partnosearch = function () {
                  $('#searchResult').show();  
                                $scope.filteredItems1 = $filter('filter')($scope.suggest_partno, function (item) {
                                      for(var attr in item) {
                                        if(attr == "PartNo") {
                                            if (searchMatch(item[attr], $scope.searchpart.partno)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                };
                 
                /*  $scope.searchForm = function() {
                     $scope.isLoading = true; //Loader Image
                      $http({                     
                      method  : 'POST',
                      url     : '<?php echo base_url();?>index.php/Rawlist/search_keywords_ebay',
                      data    :  angular.element('#searchform').serializeArray(),
                      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                     })
                      .success(function(data) {
                          //  $('.status_bar').html(data);
                          $scope.isLoading = false;
                          $scope.rendersearchresult = data;
                         // $('#location'+data.Competitor).html(data.Competitor);
                            
                      })
                        .catch(function (err) {
                          
                      });
                }; */

                    if($scope.ischeckedpart == "undefined"){
                        console.log($scope.ischeckedpart);
                    }

                    /*var ischeck = '';
                    $scope.$watch('ischeckedpart',function(){
                        console.log($scope.ischeckedpart);
                        if($scope.ischeckedpart == "undefined"){
                            console.log('i am success');
                        }
                        //console.log($scope.ischeckedpart);
                        /*if($scope.ischeckedpart){
                            $scope.mycheckbox = 1;
                        }
                        else{
                            $scope.mycheckbox = 0;
                        }
                    });*/

                  /*  //pusher
                     Pusher.logToConsole = true;
                        var pusher = new Pusher('15355bcfc5ff30a1fbca', {
                            cluster: 'ap2',
                          encrypted: true
                        });
                        var ip = "<?php echo $_SESSION['__ci_last_regenerate']; ?>";
                        //console.log(ip);
                        var channel = pusher.subscribe('myebaychannel'+ip);
                        channel.bind('myebayevent'+ip, function(data) {
                        $scope.isLoading = false; 
                            $('.msgstatic').css('display','none');     
                            $('.msgprogress').html('');
                            $('.msgprogress').append('<span class="content_dynamic">Searching eBay for Competitors for the Keyword</span>'+data.message);
                            if(data.itemId[0] != ''){
                                $('.msgprogressitem').html('');
                                $('.msgprogress').html('');    
                                $('.msgprogressitem').append('<span class="content_dynamic">Retrieving Items Specifics for </span> <br> <span class="same_as_head">ItemID :</span>'+data.itemId+'<br> <span class="same_as_head">Competitor :</span>'+data.sellerName+'<br>['+data.count+' of '+data.total+']');    
                            }
                            if(data.filtermsg != ''){
                               $('.filterprogress').html('');
                              $('.filterprogress').append(data.filtermsg);

                            }                           
                        });
                         //pusher end*/


                $scope.ischeckedpart = true;
                $scope.ischeckedpart = false;

                //rawlist competitor search and ebay search function
                $scope.submitForm = function() {                 
                  $scope.secd_response = [];
                  $scope.inceptitem = '';
                  $scope.rawlistitem ='';
                  $('.msgprogress').html('');  
                  $('.msgprogressitem').html('');
                  var temp_search = '';
                  var ebay_url = '';
                  var sendgui = $('#new_hidden_id').val();

                  toString = sendgui.toString(),
                  toConcat = sendgui + "";
                  //console.log(toConcat);
                  var ebay_data = '';
                  var value = $('#applyfilter').is(":checked") ? 1 : 0;
                  var key = 'applyfilter';
                  var btn = $(document.activeElement).val();
                  //console.log(btn);
                  if(btn == 'Search') {
                      temp_search = 1;
                     $('.err_modal1').css('display','block');
                     $('.msgprogress').append('<span class="content_dynamic">eBay Search Process Is Initializing</span><img src="<?php echo base_url().'assets/';?>img/loader.gif" style="width:15%"/>');
                    ebay_url   = '<?php echo base_url();?>index.php/Rawlist/search_keywords_ebay';
                    ebay_data  =  {'first':$scope.ischeckedpart,'second':angular.element('#searchform').serializeArray()};
                  }
                  if(btn == 'Show'){
                      temp_search = 2;
                      $scope.isLoading = true; //Loader Image
                        ebay_url  = '<?php echo base_url();?>index.php/Rawlist/rawlist_details_control',
                        ebay_data = {'competitor':$scope.competitorname.compid,'category':$scope.competitorcategory.Categoryid,'chkvalue':value, 'chkkey':key, 'passgui':toConcat};
                  }
                  if(btn != 'Search' && btn != 'Show') {
                      temp_search = 1;
                     $('.err_modal1').css('display','block');
                    ebay_url   = '<?php echo base_url();?>index.php/Rawlist/search_keywords_ebay';
                    ebay_data  =  {'first':$scope.ischeckedpart,'second':angular.element('#searchform').serializeArray()};
                  }
                  $scope.message = '';    

                  // custom pusher start
               // try
                //{
                  var xhr = new XMLHttpRequest();  
                  xhr.previous_text = '';
                  xhr.onerror = function() { console.log('error occured'); };               
                  xhr.onreadystatechange = function(data){
                    $scope.new_response = '';
                    try{
                    if(xhr.readyState == 3){
                        $scope.new_response = '';
                        $timeout(function(){
                         if(temp_search == 1){
                          $scope.new_response = xhr.responseText.substring(xhr.previous_text.length);
                         if (IsJsonString($scope.new_response)) {
                          var result = JSON.parse($scope.new_response);
                          $scope.secd_response = result;
                            /*console.log(result);*/
                              if(result.itemId == '' ){
                                $('.err_modal1').css('display','block');
                                $('.msgprogress').html('');
                                 $('.msgprogress').append('<span class="content_dynamic">Searching eBay For Competitors For The Keyword</span>'+result.sellerName);
                              }
                              else{
                                $scope.keyid = result.keyid;
                                $scope.checkboxval = result.checkboxval;
                                $('#cancel').css('display','block');
                                /*console.log($scope.keyid);
                                console.log($scope.checkboxval);*/
                                $('.msgprogress').html('');
                                    $('.msgprogress').html('');
                                    $('.msgprogress').append('<span class="content_dynamic"><span class="same_as_head">Total Pages : </span>'+result.totalPages+'<br><span class="same_as_head">Total Entries : </span>'+result.totalEntries+'<br><br>Retrieving Items Specifics For, </span><span class="same_as_head">Page Number : </span>['+result.pageNumber+' of '+result.totalpagecount+']<br> <span class="same_as_head">Item ID : </span>'+result.itemId+'<br> <span class="same_as_head">Competitor : </span>'+result.sellerName+'<br><span class="same_as_head">Retrieving Page '+result.pageNumber+' : </span>['+result.runningcount+' of '+result.totcount+']');
                              }
                              if(result.filtermsg != ''){
                                $('.msgprogress').html('');
                                $('.msgprogressitem').html('');
                               $('.filterprogress').html('');
                              $('.filterprogress').append(result.filtermsg);

                            }  
                          xhr.previous_text = xhr.responseText;
                          }
                          }
                        },1);
                      } 
                     if(xhr.readyState == 4 ){
                      //console.log('XHR State 4');  
                      var request = xhr.responseText.replace(/}{/g, "},{"); 
                        $('.err_modal1').css('display','none');
                        $('#cancel').css('display','none');
                        /*if(temp_search == 2){
                          console.log('Temp Search = 2');
                          console.log(xhr.responseText);
                            $scope.new_response = xhr.responseText;

                            var result = JSON.parse($scope.new_response);
                            console.log(result);
                            $scope.secd_response = result;
                         }*/

                          $timeout(function() {
                            $scope.inceptitem = '';
                            $scope.rawlistitem ='';
                            $scope.ForExcelExport =[];
                            var exportdata_exc=[];   
                            //console.log('Inside Timeout');
                            var npos = xhr.responseText.indexOf("first");
                            //console.log(xhr.responseText.substring(npos-2));
                            //console.log('Break');
                          //$resultstring = xhr.responseText.substring(npos-2);
                          //var rawitemdetail = JSON.parse($resultstring);
                          //$scope.inceptitem = rawitemdetail.itemdetail;
                          //$scope.rawlistitem =  rawitemdetail.rawdetail;
                          var rawitemdetail = JSON.parse('['+request+']');                         
                          $scope.inceptitem = rawitemdetail[rawitemdetail.length-1].itemdetail;
                          $scope.rawlistitem =  rawitemdetail[rawitemdetail.length-1].rawdetail;  
                          //console.log(rawitemdetail);
                            /*if ($scope.rawlistitem.length == 0)
                            {
                              $scope.pagedItems = '';
                              $("#invalidkeyword").css("display","block");
                              return false;

                            }*/
                            //rawlistitemresult =  $scope.secd_response.rawdetail;
                            //console.log($scope.rawlistitem);
                            //console.log('raw detail');
                            //console.log($scope.secd_response.rawdetail);
                            $scope.orderByField = 'CompetitorName';
                            $scope.reverseSort = false;
                            //console.log('Start process');
                            //console.log($scope.rawlistitem);
                            //console.log('End process');

                            if($scope.rawlistitem != undefined){
                              $scope.rawlistitem.forEach(function (part) {
                                  if (part.mypartno != ''){
                                  exportdata.push({'CompetitorName' : part.CompetitorName,'ItemID' : part.ItemID,'SKU' : part.SKU,'mypartno' : 'N/A','Title' : part.Title,'CurrentPrice' : part.CurrentPrice,'ListingDate':part.ListingDate,'QuantitySold':part.QuantitySold,'lastweek_Qtysold':part.lastweek_Qtysold});
                                  exportdata_exc.push({'Competitor' : part.CompetitorName,'Item ID' : "'"+part.ItemID,'SKU' : part.SKU,'My Part#' : 'N/A','Title' : part.Title,'Price (in $)' : part.CurrentPrice,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.QuantitySold,'Qty.Sold (Last Wk)':part.lastweek_Qtysold});
                                  }
                                  else
                                  {
                                      exportdata_exc.push({'Competitor' : part.CompetitorName,'Item ID' : "'"+part.ItemID,'SKU' : part.SKU,'My Part#' : part.mypartno,'Title' : part.Title,'Price (in $)' : part.CurrentPrice,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.QuantitySold,'Qty.Sold (Last Wk)':part.lastweek_Qtysold});
                                  }
                              });             
                   
                               $scope.ForCSVExport = exportdata;
                               $scope.ForExcelExport = exportdata_exc; 
                               /*  $scope.exportpdf = function() {
                                  var ebay_url = '';
                                  var ebay_data = '';
                                  var value = $('#applyfilter').is(":checked") ? 1 : 0;
                                  var key = 'applyfilter';

                                  if(btn == 'Search') {
                                    ebay_url   = '<?php echo base_url();?>index.php/Rawlist/search_keywords_ebay';
                                    ebay_data  =  angular.element('#searchform').serializeArray();
                                  }
                                  if(btn == 'Show'){
                                  ebay_url  = '<?php echo base_url();?>index.php/Rawlist/export',
                                  ebay_data = {'competitor':$scope.competitorname.compid,'category':$scope.competitorcategory.Categoryid,'chkvalue':value, 'chkkey':key};
                                    }
                                    $scope.message = '';    
                                    $http({                     
                                        method  : 'POST',
                                        url     : ebay_url,
                                        data    : ebay_data,
                                        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                  };*/

                                          //pagination part start 
                                  // *********************

                                  $scope.sortBy = function(propertyName) {
                                      $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                      $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                      $scope.pagedItems = ""; 
                                      $scope.groupToPages();
                                  };

                                  var searchMatch = function (haystack, needle) {
                                      if (!needle) {
                                          return true;
                                      }
                                      if(haystack !== null){                                       
                                              return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                      }
                                      };

                                       // *****************init the filtered items*****************
                                     $scope.mysearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                     if(attr == "CompetitorName") {
                                                      if (searchMatch(item[attr], $scope.query)  > -1){
                                                          return true;
                                                      }
                                                  }

                                              }
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                       $scope.categorysearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                  
                                                  if(attr == "Category") {
                                                      if (searchMatch(item[attr], $scope.categoryquery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                       $scope.myitemsearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                 
                                                  if (attr == "ItemID") {
                                                      if (searchMatch(item[attr], $scope.itemquery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                       $scope.mypartsearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                
                                                  if(attr == "mypartno") {
                                                      if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                       $scope.myskusearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                
                                                  if(attr == "SKU") {
                                                      if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                       $scope.mytitlesearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                 
                                                   if(attr == "Title") {
                                                      if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                       $scope.mypricesearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                  if(attr == "CurrentPrice") {
                                                      if (searchMatch(item[attr], $scope.pricequery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };

                                      $scope.listingsearch = function () {
                                          $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                              for(var attr in item) {
                                                  if(attr == "ListingDate") {
                                                      if (searchMatch(item[attr], $scope.listingquery)  > -1){
                                                          return true;
                                                      }
                                                  }
                                              }
                                              return false;
                                          });
                                          // take care of the sorting order
                                          if ($scope.sortingOrder !== '') {
                                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                          }
                                          $scope.currentPage = 0;
                                          // now group by pages
                                          $scope.groupToPages();
                                      };
                                      
                                      // calculate page in place
                                      $scope.groupToPages = function () {
                                      $scope.pagedItems = [];
                                      for (var i = 0; i < $scope.rawlistitem.length; i++) {
                                          if (i % $scope.itemsPerPage === 0) {
                                              $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                              } else {
                                              $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                          }
                                          }
                                      };
                    
                                      $scope.range = function (start, end) {
                                          var ret = [];
                                          if (!end) {
                                              end = start;
                                              start = 0;
                                          }
                                          for (var i = start; i < end; i++) {
                                              ret.push(i);
                                          }
                                          $scope.pagenos = ret;
                                          return ret;
                                      };
                    
                                      $scope.prevPage = function () {
                                          if ($scope.currentPage > 0) {
                                              $scope.currentPage--;
                                               $('#ckbCheckAll').trigger('click');
                                                $('#ckbCheckAll').trigger('click');
                                          }
                                      };
                    
                                      $scope.nextPage = function () {
                                          if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                              $scope.currentPage++;
                                               $('#ckbCheckAll').trigger('click');
                                                $('#ckbCheckAll').trigger('click');
                                          }
                                      };
                                      
                                      $scope.setPage = function () {
                                          $scope.currentPage = this.n;
                                          $('#selpage').prop('checked',false);
                                           
                                      };

                                      // functions have been describe process the data for display
                                      $scope.mysearch();

                                      // change sorting order
                                      $scope.sort_by = function(newSortingOrder) {
                                          if ($scope.sortingOrder == newSortingOrder)
                                              $scope.reverse = !$scope.reverse;

                                          $scope.sortingOrder = newSortingOrder;

                                          // icon setup
                                          $('th i').each(function(){
                                              // icon reset
                                              $(this).removeClass().addClass('icon-sort');
                                          });
                                          if ($scope.reverse)
                                              $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                          else
                                              $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                                      };
                                      
                                      $scope.$watch('currentPage', function(pno,oldno){
                                        if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                          var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                          $scope.range(start, $scope.pagedItems.length);
                                        }
                                      });
                                      $scope.range($scope.pagedItems.length);
                                  //pagination part end

                                      $scope.search = {};
                                      $scope.isLoading = false;

                                        $scope.exportDataExcel = function () {
                                             var mystyle = {
                                                sheetid: 'Rawlist',
                                                headers: true,
                                                column: { style: 'background:#a8adc4' },
                                                };
                                                alasql('SELECT * INTO XLS("DIVE_RawList.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                                        };   
                                  }
                                  }, 0);//timeout end
                          $scope.isLoading = false;
                  }
                }  //try end (catch)
                      catch(e){
                        var msg = xhr.status;
                               //failure msg
                                  $http({                     
                                   method  : 'POST',
                                   url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                   data    : msg
                                  })
                                  .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                        $("#Pnopopup").css("display", "none");                 
                                        $scope.isLoading = false;
                                  });
                      }                      
                };   
                xhr.open("POST", ebay_url, true);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.send(JSON.stringify(ebay_data));

                $scope.cancelfun = function(){
                  xhr.abort();
                     $('.err_modal1').css('display','block');
                     $('.msgprogress').html('');
                     $('.msgprogress').append('<span class="content_dynamic">Aborting eBay Search Process</span><img src="<?php echo base_url().'assets/';?>img/loader.gif" style="width:15%"/>');
                     $http({                     
                      method  : 'POST',
                      url     : '<?php echo base_url();?>index.php/Rawlist/canceldataget',
                      data    :  {'keyid': $scope.keyid,'applyfilter_checkbox':$scope.checkboxval},
                      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                     })
                    .success(function(response) {
                        $scope.inceptitem = response.itemdetail;
                        $scope.rawlistitem =  response.rawdetail;
                        $('.err_modal1').css('display','none');
                        $scope.ForCSVExport = '';
                        $scope.ForExcelExport =  '';
                        var exportdata = [];
                        var exportdata_exc = [];
                            if($scope.rawlistitem != undefined)
                            {
                                $scope.rawlistitem.forEach(function (part) {
                                    if (part.mypartno != '')
                                    {
                                        exportdata.push({'CompetitorName' : part.CompetitorName,'ItemID' : part.ItemID,'SKU' : part.SKU,'mypartno' : 'N/A','Title' : part.Title,'CurrentPrice' : part.CurrentPrice,'ListingDate':part.ListingDate,'QuantitySold':part.QuantitySold,'lastweek_Qtysold':part.lastweek_Qtysold});
                                        exportdata_exc.push({'Competitor' : part.CompetitorName,'Item ID' : "'"+part.ItemID,'SKU' : part.SKU,'My Part#' : 'N/A','Title' : part.Title,'Price (in $)' : part.CurrentPrice,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.QuantitySold,'Qty.Sold (Last Wk)':part.lastweek_Qtysold});
                                    }
                                    else
                                    {
                                        exportdata_exc.push({'Competitor' : part.CompetitorName,'Item ID' : "'"+part.ItemID,'SKU' : part.SKU,'My Part#' : part.mypartno,'Title' : part.Title,'Price (in $)' : part.CurrentPrice,'Listing Date':part.ListingDate,'Qty.Sold (Inc)':part.QuantitySold,'Qty.Sold (Last Wk)':part.lastweek_Qtysold});
                                    }
                                  });             
                              }
                             $scope.ForCSVExport = exportdata;
                             $scope.ForExcelExport = exportdata_exc; 
                                 
                         //pagination part start 
                          // *********************
                          $scope.sortBy = function(propertyName) {
                              $scope.sortKey = propertyName;   //set the sortKey to the param passed
                              $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                              $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                              $scope.pagedItems = ""; 
                              $scope.groupToPages();
                          };

                          var searchMatch = function (haystack, needle) {
                              if (!needle) {
                                  return true;
                              }
                              if(haystack !== null){                                       
                                      return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                              }
                          };

                               // *****************init the filtered items*****************
                          $scope.mysearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                             if(attr == "CompetitorName") {
                                              if (searchMatch(item[attr], $scope.query)  > -1){
                                                  return true;
                                              }
                                          }

                                      }
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                            };

                            $scope.categorysearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                          
                                          if(attr == "Category") {
                                              if (searchMatch(item[attr], $scope.categoryquery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                          };

                           $scope.myitemsearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                         
                                          if (attr == "ItemID") {
                                              if (searchMatch(item[attr], $scope.itemquery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                          };

                          $scope.mypartsearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                        
                                          if(attr == "mypartno") {
                                              if (searchMatch(item[attr], $scope.partquery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                          };

                          $scope.myskusearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                        
                                          if(attr == "SKU") {
                                              if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                          };

                          $scope.mytitlesearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                         
                                           if(attr == "Title") {
                                              if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                          };

                          $scope.mypricesearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                          if(attr == "CurrentPrice") {
                                              if (searchMatch(item[attr], $scope.pricequery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                          };

                          $scope.listingsearch = function () {
                                  $scope.filteredItems = $filter('filter')($scope.rawlistitem, function (item) {
                                      for(var attr in item) {
                                          if(attr == "ListingDate") {
                                              if (searchMatch(item[attr], $scope.listingquery)  > -1){
                                                  return true;
                                              }
                                          }
                                      }
                                      return false;
                                  });
                                  // take care of the sorting order
                                  if ($scope.sortingOrder !== '') {
                                      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                  }
                                  $scope.currentPage = 0;
                                  // now group by pages
                                  $scope.groupToPages();
                              };
                              
                              // calculate page in place
                              $scope.groupToPages = function () {
                              $scope.pagedItems = [];
                              for (var i = 0; i < $scope.rawlistitem.length; i++) {
                                  if (i % $scope.itemsPerPage === 0) {
                                      $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                      } else {
                                      $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                  }
                                  }
                          };
            
                          $scope.range = function (start, end) {
                                  var ret = [];
                                  if (!end) {
                                      end = start;
                                      start = 0;
                                  }
                                  for (var i = start; i < end; i++) {
                                      ret.push(i);
                                  }
                                  $scope.pagenos = ret;
                                  return ret;
                          };
            
                          $scope.prevPage = function () {
                              if ($scope.currentPage > 0) {
                                  $scope.currentPage--;
                                   $('#ckbCheckAll').trigger('click');
                                    $('#ckbCheckAll').trigger('click');
                              }
                          };
        
                          $scope.nextPage = function () {
                              if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                  $scope.currentPage++;
                                   $('#ckbCheckAll').trigger('click');
                                    $('#ckbCheckAll').trigger('click');
                              }
                          };
                              
                          $scope.setPage = function () {
                              $scope.currentPage = this.n;
                              $('#selpage').prop('checked',false);
                               
                          };

                          // functions have been describe process the data for display
                          $scope.mysearch();

                          // change sorting order
                          $scope.sort_by = function(newSortingOrder) {
                              if ($scope.sortingOrder == newSortingOrder)
                                  $scope.reverse = !$scope.reverse;

                              $scope.sortingOrder = newSortingOrder;

                              // icon setup
                              $('th i').each(function(){
                                  // icon reset
                                  $(this).removeClass().addClass('icon-sort');
                              });
                              if ($scope.reverse)
                                  $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                              else
                                  $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                          };
                          
                          $scope.$watch('currentPage', function(pno,oldno){
                            if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                              var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                              $scope.range(start, $scope.pagedItems.length);
                            }
                          });

                          $scope.range($scope.pagedItems.length);
                          //pagination part end

                          $scope.search = {};
                          $scope.isLoading = false;

                          $scope.exportDataExcel = function () {
                             var mystyle = {
                                sheetid: 'Rawlist',
                                headers: true,
                                column: { style: 'background:#a8adc4' },
                              };
                              alasql('SELECT * INTO XLS("DIVE_RawList.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                          };   
                         $scope.isLoading = false;
                      }); 
                }
                
                 // custom pusher end
              /*} //try end for onreadystate function
              catch(e){
                  var msg = xhr.status;
                         //failure msg
                            $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data    : msg
                            })
                            .success(function(response) {
                                  $('#msg').html(response);
                                  $('.err_modal').css('display','block');
                                  $('#msg').css('display','block'); 
                                  $("#Pnopopup").css("display", "none");                 
                                  $scope.isLoading = false;
                            });
                }  */
            };  

              //show and ebay search end
                        

                if($scope.rawlistitem !== 'undefined'){
                        if($scope.haschecked){
                            //$('.ckeckclass').prop('checked',true);
                            angular.forEach($scope.rawlistitem, function(value, key){
                                    if(value.Mlflag == 1){
                                        $('#'+value.ItemID).prop('checked',true);
                                    }
                                    else{
                                        $('.ckeckclass').prop('checked',true);
                                    }
                            });
                        }
                        else{
                            angular.forEach($scope.rawlistitem, function(value, key){
                                    if(value.Mlflag == 0){
                                        $('#'+value.ItemID).prop('checked',false);
                                    }
                            });
                        }
                    }
                //global check for all checkboxes function 

                $scope.globalcheck = function(){
                    if($scope.rawlistitem !== 'undefined'){
                        if($scope.haschecked){
                            //$('.ckeckclass').prop('checked',true);
                            angular.forEach($scope.rawlistitem, function(value, key){
                                    if(value.Mlflag == 1){
                                        $('#'+value.ItemID).prop('checked',true);
                                    }
                                    else{
                                        $('.ckeckclass').prop('checked',true);
                                    }
                            });
                        }
                        else{
                            angular.forEach($scope.rawlistitem, function(value, key){
                                    if(value.Mlflag == 0){
                                        $('#'+value.ItemID).prop('checked',false);
                                    }
                            });
                        }
                        $('#selpage').prop('checked',false);
                    }
                }
                $scope.selectpage = function(){
                    if($scope.selhaschecked){
                        angular.forEach($scope.rawlistitem, function(value, key){
                                if(value.Mlflag == 1){
                                    $('#'+value.ItemID).prop('checked',true);
                                }
                                else{
                                    $('.ckeckclass').prop('checked',true);
                                }
                        });
                    }
                    else{
                        angular.forEach($scope.rawlistitem, function(value, key){
                                    if(value.Mlflag == 0){
                                        $('#'+value.ItemID).prop('checked',false);
                                    }
                            });
                    }
                }

                 //popup save button function
                 var timeoutinstance;
                $scope.submitmainForm = function() {
                $('.failureupdate').hide();
                $('.pnum').hide();
                  
                var partarray ='';
                angular.forEach($scope.suggest_partno, function(data){       
                 if(data.partno == $('#runpartnumber').val())
                 {
                   partarray = data.partno;
                 } 
                });  
                console.log(partarray);
                if ($('#runpartnumber').val() == '')
                {
                    partarray ='';
                }
                 if (partarray.length == 0 && $('#runpartnumber').val() != '')
                 {    
                   console.log('not matched');
                   partarray = '';
                        //return false;
                           $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg_configcbr',
                                  data    : 003
                                })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    $("#Pnopopup").css("display", "none");                 
                                    $scope.isLoading = false;
                                });
                  }    
                  if (partarray.length != 0 || $('#runpartnumber').val()== '')
                  {
                   $scope.isLoading = true; //Loader Image
                      $http({                     
                      method  : 'POST',
                      url     : '<?php echo base_url();?>index.php/Rawlist/competitor_mypartnumber_update',
                      data    :  {'items':angular.element('#popupform').serializeArray(),'partnum':partarray},
                      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                     })
                      .success(function(data) {
                             if(data != 'Failure')
                             {
                                $scope.isLoading = false;
                                $('#runpartnumber').val('');  
                                $('#updatedpartnumber'+data.itemid).html(data.mypartno);
                                //$('.successupdate').show();
                                     $http({                     
                                             method  : 'POST',
                                             url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                             data    : 3
                                            })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('#Pnopopup').css('display','none');
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                  
                                                $scope.isLoading = false;
                                            })
                             } 
                             else{
                                $scope.isLoading = false;
                                $('#updatedpartnumber').val();
                                $('.failureupdate').show(); 
                             }
                      })
                     .catch(function (err) {
                        
                         var msg = err.status;
                        //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                         //failure msg
                            $http({                     
                             method  : 'POST',
                             url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data    : msg
                            })
                                 .success(function(response) {
                                    $('#msg').html(response);
                                    $('.err_modal').css('display','block');
                                    $('#msg').css('display','block'); 
                                    $("#Pnopopup").css("display", "none");                 
                                    $scope.isLoading = false;
                                });
                    })   
                   }
                };

                            $http({                     
                              method  : 'POST',
                              url     : '<?php echo base_url();?>index.php/Rawlist/compload',
                              data    : {'chkvalue':0, 'chkkey':'applyfilter'},
                              headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                             })
                                .success(function(data) {
                                   console.log(data);
                                    $scope.suggest_partno =data.part;
                                    $scope.filteredItems1 = $scope.suggest_partno;
                                    $scope.subcategories = '';
                                    $scope.isLoading = false;
                                    $scope.maincompetitor = data;
                                    var selectvalue=[];
                                   angular.forEach(data.comp, function(value, key){
                                        selectvalue.push({'compid' :value.CompID,'CompetitorName':value.CompetitorName});
                                   });
                                   $scope.renderselectvalue = selectvalue;
                                   $scope.firstselect = false;
                                   $scope.secondselect = true;
                                   $scope.onloadshow = false;                  
                                   $scope.onloadhide = true;
                                })

                                .catch(function (err) {
                                     var screenname = '<?php echo $screen;?>';
                                     var msg = err.status;
                                    //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                                     //failure msg
                                        $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                          data    : msg
                                        })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');                     
                                                $scope.isLoading = false;
                                            });
                                })

                    $scope.onloadshow = true;
                    $scope.selectcommon = true;
                    $scope.applyfilter =function(evt){
                        //$window.alert('Hai!');
                        $scope.competitorname="";
                        $scope.pagedItems = "";
                        $scope.pagenos = '';
                        var key = 'applyfilter';
                        var value = $('#applyfilter').is(":checked") ? 1 : 0;
                        if($('#applyfilter').is(":checked") == true){
                            var result = {'chkvalue':value, 'chkkey':key};
                        }else{
                            var result =  {'chkvalue':value, 'chkkey':key};
                        }
                            $scope.pagedItems = '';
                            $scope.inceptitem = '';
                            $scope.isLoading = true; //Loader Image
                              $http({                     
                              method  : 'POST',
                              url     : '<?php echo base_url();?>index.php/Rawlist/compload',
                              data    : result,
                              headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                             })
                                .success(function(data) {
                                   $scope.suggest_partno =data.part;
                                 $scope.filteredItems1 = $scope.suggest_partno;
                                    $scope.pagedItems = "";
                                    $scope.pagenos = '';
                                    $scope.subcategories = '';
                                    $scope.isLoading = false;
                                    $scope.maincompetitor = data;
                                    var selectvalue=[];
                                   angular.forEach(data.comp, function(value, key){
                                        selectvalue.push({'compid' :value.CompID,'CompetitorName':value.CompetitorName});
                                   });
                                   $scope.renderselectvalue = selectvalue;
                                   $scope.firstselect = false;
                                   $scope.secondselect = true;   
                                   $scope.onloadshow = false;                  
                                   $scope.onloadhide = true;
                                })
                                 .catch(function (err) {
                                   var screenname = '<?php echo $screen;?>';
                                    var msg = err.status;
                                    //Error Log To DB
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                                     //failure msg
                                        $http({                     
                                         method  : 'POST',
                                         url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                         data    : msg
                                        })
                                             .success(function(response) {
                                                $('#msg').html(response);
                                                $('.err_modal').css('display','block');
                                                $('#msg').css('display','block');
                                                $("#Pnopopup").css("display", "none");                      
                                                $scope.isLoading = false;
                                            });
                         
                                 })
                            /*}
                            else{
                                $scope.onloadshow = true;                  
                                $scope.onloadhide = false;
                            }*/
                    };

                    //intially disabled the AddtoMasterList button
                 $scope.countChecked = function(){
                    var count = 0;
                    angular.forEach($scope.rawlistitem, function(value){
                         if (value.selected || $("#selpage").is(':checked') || $("#ckbCheckAll").is(':checked')) count++;
                    });
                    return count;
                 }

                     $scope.freearray = function(){
                        $scope.competitorname = '';
                        $scope.competitorcategory = '';
                        $scope.temp = 1;
                     }

                      $scope.freearray1 = function(){
                        $scope.temp = 0;
                     }

                 //Add to masterlist function
                  $scope.addtomasterlist = function() {
                    $('.err_modal1').css('display','none');
                     //$('.progrss_confirm').css('display','none');
                        $('.addbtn').hide();
                         $('#success_msg').hide();
                        $('#msg_id').hide();
                        $('#ebay_msg_id').hide();
                      //************************

                          if ($scope.temp != 1)
                          {
                             $scope.compid = $scope.competitorname.compid;
                          }

                          else
                          {
                              $scope.compid = 0;
                          }

                     var globalitem = '';
                     if($scope.haschecked){
                        globalitem = {'itemid':$scope.rawlistitem};
                     }
                     else{
                        globalitem = {'competitor':$scope.compid,'itemid':angular.element('#addform').serializeArray()};
                     }

                    if($('input[type=checkbox]:checked').length == 0){
                     
                         $('.addbtn').css('display','inline-block');
                          $("#msg_id").css("display","none");
                         return false;
                    }
                    else{
                         $('.addbtn').css('display','none');
                    }
                    //$scope.message = false;
                  $scope.isLoading = true; //Loader Image
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Rawlist/add_to_masterlist',
                  data    : globalitem,
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                     .success(function(data) {

                                            if(data !== 'failure')
                                            {
                                                $scope.string = data;

                                               /* $scope.message="Items Successfully Added to Masterlist.";*/
                                                //$scope.isLoading = false;
                                            $http({                     
                                               method  : 'POST',
                                               url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                               data     : 1
                                              })
                                               .success(function(response) {
                                                  $('#msg').html(response);
                                                  $('.err_modal').css('display','block');
                                                  $('#msg').css('display','block');                  
                                                  $scope.isLoading = false;
                                              });
                                            }
                                            else{
                                                 $scope.message="Please Try Again";
                                                 $scope.isLoading = false;
                                            }
                                          })

                                 .catch(function (err) {
                                    console.log(err.data);
                                    //var htmlmsg = err.data;
                                   //Error Log To DB
                         var screenname = '<?php echo $screen;?>';
                         var StatusCode = err.status;
                         var StatusText = err.statusText;
                         var ErrorHtml  = err.data; 
                       
                         $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                 data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                 headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                              })
                         .success(function(response) {
                                        return true;
                                      });
                                    //failure msg(db error)
                                    $http({                     
                                     method  : 'POST',
                                     url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                     data    : msg
                                    })
                                         .success(function(response) {
                                            $('#msg').html(response);
                                            $('.err_modal').css('display','block');
                                            $('#msg').css('display','block');
                                            $("#Pnopopup").css("display", "none");                      
                                            $scope.isLoading = false;
                                        });
                         
                                })
                                   $scope.temp = '';
                         };


                
                /*$scope.sortBy = function(propertyName) {
                    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                    $scope.propertyName = propertyName;
                };*/
            });
            
            
        </script>

        <script type="text/javascript">
        /* $('#applyfilter').on('change', function() {
            var value = this.checked ? 1 : 0;
            var key = 'applyfilter';
            alert(value);
        }) */
        
        /* $('#applyfilter').on('change', function() {
                    var value = $('#applyfilter').is(":checked") ? 1 : 0;
                    var key = 'applyfilter';
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Rawlist/index',
                  data    : {'chkvalue':value, 'chkkey':key},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    console.log(data);
                    //return false;*
                   // $scope.subcategories = data;
                  
                  });
               }); */

        $(document).ready(function () {
            $('#hidden_id').val('<?php echo $GUID; ?>');
           // $('#new_hidden_id').val('<?php echo $GUID; ?>');
            /*$('#applyfilter').on('change', function() {
                
                    $scope.isLoading = true; //Loader Image
                    var value = $('#applyfilter').is(":checked") ? 1 : 0;
                    var key = 'applyfilter';
                  $.(ajax){
                      
                  });
                  $http({                     
                  method  : 'POST',
                  url     : '<?php echo base_url();?>index.php/Rawlist/index',
                  data    : {'chkvalue':value, 'chkkey':key},
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                 })
                  .success(function(data) {
                    /*console.log(data);
                    return false;
                    $scope.isLoading = false; //Loader Image
                   // $scope.subcategories = data;
                  
                  });*/
               

            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
        

    </script>
   <script>
     
   </script>