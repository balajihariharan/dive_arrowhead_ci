<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <title>Arrowhead : Login</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_romaine.css" />
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    </head>
<body>
    <div class="bg">
        <!--<div class="header_login">
            <div class="logo_main">
                <img src="img/logo.png">
            </div>
        </div>-->
        <div class="container">
            <div class="wrapper">
                <div class="login_main" id="login_main">

                    <h1>Login</h1>
                    <form action="<?php echo base_url();?>Login/login" class="form_admin" method="post">
                       <?php 
                       if ($this->session->flashdata('errormsg') != '')
                       {
                         echo '<span style="padding-left:30%; color:red; font-size:15px">'.$this->session->flashdata('errormsg').'</span>';
                        }
                         ?>
                        <p class="user_name_main">
                            <input name="loginid" type="text" placeholder="Login Id" class="ur_name" required />
                        </p>
                        <p class="ps_main">
                            <input name="password" type="password" placeholder="Password" class="ps_admin" required />
                        </p>
                        <p>
                            <!--<input type="submit" value="Submit" class="submit_btn"/>
                            <input type="submit" value="Forget Password" class="forget_btn"/>-->
                            <button type="submit" class="submit_btn">Submit</button>
                        </p>
                        
                    </form>
                        <p>
                            <a href='<?php echo base_url();?>ForgetPassword/index'><button type="submit" class="forget_btn" style="margin: 0px 0px 0px 0px;">Forget Password</button></a>
                        </p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>