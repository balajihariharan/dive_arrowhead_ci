<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
             <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<body class="main">
    <!-- <div class="container head_bg"> -->
    <?php $this->load->view('header.php'); ?>
    <!-- <div class="container">
    <div>
            <div class="nav nav_bg">
                <div class="wrapper">
                    <ul>

                        <li><a href="/Romaine/Rawlist"  class="current"  >
                        <img src="img/filter1.png">&nbsp Raw List</a></li>

                        <li><a href="/Romaine/Masterlist"  >
                        <img src="img/filter1.png">&nbsp Master List</a></li>

                        <li><a href="/Romaine/Competitoranalysis"  >
                        <img src="img/doller.png">&nbsp Sales Trends</a></li>
                        <li><a href="http://apatechnology.com/codeigniter_romaine/"  >
                        <img src="img/doller.png">&nbsp Competitor Analysis</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
    
      <div>
        <script>
            $(function () {
                $('#customWeekPicker').datepick({
                    renderer: $.datepick.weekOfYearRenderer,
                    calculateWeek: customWeek, firstDay: 1,
                    showOtherMonths: true, showTrigger: '#calImg'
                });
                function customWeek(date) {
                    return Math.floor(($.datepick.dayOfYear(date) - 1) / 7) + 1;
                }
            });
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
            .status_bar tr, .status_bar th, .status_bar td {
                width: 10%;
            }


            /*.status_bar thead th:nth-child(6) {
                min-width: 600px;
                width: 600px;
                max-width: 600px;
            }

            .status_bar thead th:nth-child(2) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }

            .status_bar thead th:nth-child(4) {
                min-width: 150px;
                width: 150px;
                max-width: 150px;
            }*/

            ul, li {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            .label {
                color: #000;
                font-size: 16px;
            }

            .field1 {
                display: inline-block;
                float: left;
                padding: 0 10px 0 0px;
            }

            .shwpopup {
                display: none;
                width: 50%;
                margin: 50%;
                position: fixed;
                z-index: 1;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                left: 0;
                top: 0;
            }

            .field {
                width: auto;
            }

            .ckeckclass {
                margin: 2px;
            }

            .divTable {
                float: left;
                width: 70px;
                height: 40px;
                border-right: 1px solid #e1e1e1;
                padding-top: 28px;
                padding-left: 7px;
            }

            .lastdivTable {
                float: left;
                width: 70px;
                padding-top: 28px;
                padding-left: 7px;
            }

            .paging-nav {
                text-align: right;
                padding-top: 2px;
            }

                .paging-nav a {
                    margin: auto 1px;
                    text-decoration: none;
                    display: inline-block;
                    padding: 1px 7px;
                    background: #5D85B2;
                    color: white;
                    border-radius: 3px;
                }

                .paging-nav .selected-page {
                    background: #187ed5;
                    font-weight: bold;
                }

            tfoot {
                text-align: center !important;
                display: table-row-group !important;
            }

            .paging-nav,
            #tblrawresult {
                margin: 0 auto;
                font-family: Arial, sans-serif;
            }


            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 30%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }

            .itemIDCell {
                margin: 0;
            }

            .edit_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 85px;
                font-size: 14px;
            }

             .delete_type{
                text-decoration: none;
                color: #1376AF;
                cursor: pointer;
                background-color: #fff;
                border: 1px solid #ccc;
                height: 27px;
                width: 90px;
                font-size: 14px;
            }
        </style>
        <div id="msg" style="display: none;"></div>
                <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
                      <?php echo "<h2>".$screen."</h2>" ;?><span style="padding-left:40%; color:red; font-size:14px"></span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="wrapper">
                
                    <div class="content_main_all">
                        
                        <!--<div class="channel_main">
                            <div class="channel_all">
                                <label>Channel</label>
                                <select class="select_med">
                                    <option>PMT</option>
                                    <option>ebay</option>
                                    <option>Amazon</option>
                                    <option>3dcart</option>
                                    <option>Magento</option>
                                    <option>Big Commerce</option>
                                </select>
                                <span class="amazon_drop"><img src="img/amazon_logo.png"></span>
                            </div>
                           
                        </div>-->

                        <div class="pricing_menu_manage">
                            <ul>
                                <li>
                                    <input type="submit" value="Add Role" id="addrole" class="button_add rawlistdatasubmit addpop"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="table_main">
                            <table style="width:100%;" border="1" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Role ID</th>
                                        <th>Role Name</th>
                                        <th>Role Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <?php 
                                    echo '<tbody>';
                                    foreach ($role as $value)
                                    {
                                        echo '<tr><td>'.$value['roleid'].'</td>
                                        <td>'.$value['rolename'].'</td>
                                        <td>'.$value['roledesc'].'</td>
                                        <td>
                                           <input type="submit" class="edit_type" id='.$value['roleid'].' value="Edit"/>
                                           <input type="submit" class="delete_type" id='.$value['roleid'].' value="Delete"/>
                                        </td>
                                        </tr>';
                                    }
                                    echo '</tbody>';
                                ?>
                            </table>
                        </div>
                    <!--  <form id="popupform"> -->
                        <?php
                         echo '
                                        <div id="Pnopopup" class="modal">
                                               <div class="pop_map_my_part">
                                               
                                                <center><div class="successupdate" style="display:none;">Update Successfully</div></center>
                                                 <center><div class="failureupdate" style="display:none;">Update Failed Please try again</div></center>
                                                 <div class="pop_container">
                                                            <div class="heading_pop_main">
                                                                <h4>
                                                                    Role Updates
                                                                </h4>
                                                                 <span class="close">&times;</span>
                                                            </div>
                                                            <div class ="pop_pad_all_add_new">
                                                                <div class="channel_main">
                                                                    <label class="label_inner_general">Role ID</label>
                                                                    <p class="drop_down_med1">
                                                                        <input type="text" class="label_disabled" name="roleid" id="roleid" value="" readonly/>
                                                                    </p>

                                                               <div class="channel_main">
                                                                    <label class="label_inner_general">Role Name :<span style="color:red; font-size:16px;">*</span></label>
                                                                    <p class="drop_down_med1">
                                                                        <input type="text" class="input_med" name="rolename" id="rolename" value="" />
                                                                    </p>
                                                                </div>


                                                                <div class="channel_main">
                                                                    <label class="label_inner_general">Role Description :<span style="color:red; font-size:16px;">*</span></label>
                                                                    <p class="drop_down_med1">
                                                                        <textarea id="roledesc" name="roledesc" value="" >
                                                                        </textarea>                 
                                                                    </p>
                                                                </div>

                                                            <div class="button_center">
                                                                    <img style="display:none;" class="ajaxloader" src="'.base_url().'assets/img/ajax-loader.gif"/>
                                                                    
                                                                    <button type="submit" id="popsavemain" class="button_add_part_save  myeditpart" 
                                                                    style="margin: 0;">Save</button>
                                                                
                                                                </div>
                                                            </div>  
                                                        </div>
                                                            </div>
                                                </div>
                                               </div>
                                            </div>
                                    ';
                        ?>
                    </div>
                
            </div>
        </div>
        <?php $this->load->view('footer.php'); ?>
        </div>
    </footer>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

</script>
<script>
        $(".edit_type").click(function()
        { 
            var id = $(this).attr('id');
            $("#Pnopopup").css("display", "block");
            $("#Pnopopup").css("display", "block");
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>Manage_Role/edit',
                data: { id : id },
                success : roleedit
                ,
                error : function(xhr){
                    var msg =xhr.status;
                     var StatusCode = xhr.status;
                     var StatusText = xhr.statusText;
                     var ErrorHtml  = xhr.responseText; 
                     var screenname = '<?php echo $screen;?>';

                     $.ajax({
                     type: 'POST',
                     url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                     data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                    
                     success: function(response){
                     return true;
                    },
                })
                   /* alert('Oops something wrong');*/
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }
            });
        });
       var roleedit = function (data){
                    var roledata = $.parseJSON(data);
                    document.getElementById('roleid').value = roledata.result.roleid;
                    document.getElementById('rolename').value = roledata.result.rolename;
                    document.getElementById('roledesc').value = roledata.result.roledesc;
                }
</script>
<script>

       $(document).on('click','.myeditpart',function(){
             if ($('#rolename').val() == "" || $('#roledesc').val() == "") {
                 var rolename=document.getElementById('rolename').value;
                       if(rolename == ""){
                            document.getElementById('rolename').style.borderColor = "red";
                            //return false;
                             }
                        else {
                           document.getElementById('rolename').style.borderColor = ""; 
                        }
                    var roledesc=document.getElementById('roledesc').value;
                        if(roledesc == ""){
                            document.getElementById('roledesc').style.borderColor = "red";
                            return false;
                        }
                          else {
                           document.getElementById('roledesc').style.borderColor = ""; 
                 
                        }

                        if(rolename == ""){
                            document.getElementById('rolename').style.borderColor = "red";
                            return false;
                             }
                            
                        
                       
                        e.preventDefault();
                }
             else{
                     document.getElementById('rolename').style.borderColor = ""; 
                     document.getElementById('roledesc').style.borderColor = ""; 
                }
               
                
            var rid = $('#roleid').val();
            var rname = $('#rolename').val();
            var rdesc = $('#roledesc').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>Manage_Role/save',
                data: { rid : rid, 
                        rname : rname, 
                        rdesc : rdesc},
                success : function(){
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                            data:{'msgstatus':3},
                                success: function(response){
                                   $('#Pnopopup').css('display','none');
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })

                   /* $('.successupdate').show();
                    location.reload(true);*/
                },
                error : function(xhr){
                        var msg =xhr.status;
                        var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                      $.ajax({

                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                             data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                    
                    
                }
            });
        });
</script>
<script>

        $(".delete_type").click(function()
        {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>Manage_Role/delete',
                data: { id : id },
                success : function(){
                 $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                             data : {'msgstatus':4},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                /*location.reload(true);*/
                },
                error : function(xhr){
                      var msg =xhr.status;
                      var StatusCode = xhr.status;
                             var StatusText = xhr.statusText;
                             var ErrorHtml  = xhr.responseText; 
                             var screenname = '<?php echo $screen;?>';

                             $.ajax({
                             type: 'POST',
                             url: '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDBAjax',
                             data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname' : screenname},
                            
                             success: function(response){
                             return true;
                            },
                        })
                     $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                              data: {'xhrstatus':msg},
                                success: function(response){
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');
                                },
                            })
                }

            });
            
        });
</script>
<script>
        $("#addrole").click(function()
        {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url();?>Manage_Role/addrole',
                data: { id : id },
                success : roleid
            });
        });
       var roleid = function (data){
                    var roledata = $.parseJSON(data);
                    $("#popupform").css("display", "block");
                     $("#Pnopopup").css("display", "block");
                    document.getElementById('roleid').value = roledata.result.roleid;
                    document.getElementById('rolename').value = "";
                    document.getElementById('roledesc').value = "";

                }
</script>
<script>
          $(document).on('click','.close',function(){
          $("#Pnopopup").css("display", "none"); 
          location.reload(true);
        });
</script>
<script>
$(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
           location.reload(true);
            
        });
/*new popup reload*/
</script>
</html>