<!DOCTYPE html>
<html ng-app="partsApp">
<html>
<head>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
     <script src="<?php echo base_url().'assets/';?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url().'assets/';?>js/jquery-ui.min.js"></script>   
   <!--  <script src="<?php echo base_url().'assets/';?>Romaine/js/jquery.min.1.11.0.js"></script> -->
    <!-- <script src="http://netsh.pp.ua/upwork-demo/1/js/typeahead.js"></script>  -->
    <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
    <script src= "https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.4/angular-sanitize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.5/xlsx.core.min.js"></script>    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
           <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
    </title>
</head>
<style>
.pagi_master li a{
    color: #fff;
    text-decoration: none;
    padding: 3px;
    background-color: chocolate;
    border-radius: 7px;    
}
</style> 
<body class="main" ng-controller="partsCtrl" ng-cloak>

<div ng-if="isLoading" style="display : block;position : fixed;z-index: 100;background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"><img src='<?php echo base_url().'assets/';?>img/loader.gif' width="58px" height="58px" ng-if="isLoading" style="margin-left:50%;margin-top:25%"/></div>

<?php $this->load->view('header.php'); ?>

    <div>
        <script>
            $(window).bind('scroll', function () {
                //alert('hi');
                if ($(window).scrollTop() > 10) {
                    $('.footer_main').addClass('relative');
                } else {
                    $('.footer_main').removeClass('relative');
                }
            });
        </script>
        <style type="text/css">
        

            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                    display: none !important;
            }

            

        th{
            color: rgba(0, 0, 0, 0.65);
            font-family: verdana;
            font-weight: bold;
            text-align: center;
            border-right: 1px solid rgba(0, 0, 0, 0.43);
        }

        .sort{
            cursor: pointer;
            background-image: url(<?php echo base_url();?>/assets/img/sort1.png) !important;
            padding: 10px 20px !important;
            background-position: 5px center !important;
            background-repeat: no-repeat !important;
        }

        .nonsort{
            padding: 10px 20px !important;
            background-position: 5px center !important;
            background-repeat: no-repeat !important;
        }

        td{
            font-family: verdana;
            color: rgba(0, 0, 0, 0.87);
        }

        .table{
            padding: 20px 0;
            text-align: center;
            width: 100%;
        }
        .table tr:nth-child(even){
            background : rgba(255, 193, 7, 0.09);
        }

        input[type="file"] 
        {
            display: none;
        }
        
        .alllabelcss{
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
            font-family: verdana;
            border-radius: 10px;
        }

        .impbuttoncss {
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
             background-color: rgb(106, 123, 87);
             color: white;
        }

        .expbuttoncss {
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            background-color: indianred;
            color: white;
        }

        .syncbuttoncss {
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            background-color: rgba(63, 81, 181, 0.82);
            color: white;
        }

        .impbuttoncss:hover {
            background-color: #4CAF50; /* Green */
            color: white;
        }

        .expbuttoncss:hover {
            background-color: #d21a1a; /* Green */
            color: white;
        }

        .syncbuttoncss:hover {
            background-color: rgba(7, 39, 216, 0.82); /* Green */
            color: white;
        }
         .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

                .close:hover,
                .close:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }
                .closecrawler {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }
           .closecrawler:hover,
                .closecrawler:focus {
                    color: #2d8bd5;
                    text-decoration: none;
                    cursor: pointer;
                }
        .close1 {       
            color: #aaa;        
            float: right;       
            font-size: 28px;        
            font-weight: bold;      
        }       
                .close1:hover,      
                .close1:focus {     
                    color: #2d8bd5;     
                    text-decoration: none;      
                    cursor: pointer;        
                }       
                #my_file {      
                display: none;      
            }
            
        </style>
        <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main" style="width: 100%;">
                      <?php echo "<h2>".$screen."</h2>" ;?>
                      <div class="pricing_menu_manage" style="margin: 0px 7px 0px 0px;">
                    <img style="width: 17%;float: right;" src="<?php echo base_url().'assets/';?>img/amz1.png"/>
                </div>
                </div>
            </div>
        </div>
               <div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">

            <div class="content_main_all" >
                <div class="total_items" style="font-family : verdana;font-size: 15px;">
                    <span>No. of ASINS Listed : </span>
                    <span style="font-weight: bold; color: #3F51B5;">{{TotalCount}}</span>
                </div>
               <label for="my_file" class="alllabelcss impbuttoncss">
                    <img src="<?php echo base_url();?>/assets/img/upload.png" style="margin-right: 10px;"></img>Bulk Import (CSV)
                </label>
                <input type="file" ng-click="import()"  id="my_file" file-reader="parts"/>
               
                <div class="button_right" id="exportsel" style="float: left;margin: -9px 0 0px 0px;">
                   
                     <li class="submenu">
                            <a class="last_menu_icon" href="#">
                    <label class="alllabelcss expbuttoncss">
                    <img src="<?php echo base_url();?>/assets/img/download1.png" style="margin-right: 10px;"></img>
                    Bulk Export
                    </label>
                            </a>
                            <ul class="level2" style="background: #cd5c5c;">
                                <li>
                                    <a href="#" class="last_menu_icon" ng-csv="ForCSVExport" filename="DIVE_AmazonAutoExplore.csv">As CSV</a>
                                </li>
                                <li><a href="#" class="last_menu_icon" ng-click="exportDataExcel()">As Excel</a></li>
                            </ul>
                        
                           
                        </li>

                </div>
                
                 <form id="mypartsform" ng-submit="onSubmit()">
                <div class="plan_list_view">
                    <div class="lable_normal table_top_form" style="background-color: rgba(139, 69, 19, 0.72);"><input id="ckbCheckAll" ng-click="globalselect()" type="checkbox" ng-model="checked"  />
                    Select All - Across Pages</div>
                    <table cellspacing="0" border="1" class="table price_smart_grid_new">
                        <thead>
                            <tr style="background : rgba(255, 153, 0, 0.55) none repeat scroll 0 0  ">
                                <th rowspan="2"></th>
                                <th rowspan="2" class="sort" ng-click="sortBy('ASIN')">ASIN #</th>
                                <th rowspan="2" class="sort" ng-click="sortBy('Title')">Title</th>
                                <th rowspan="2" class="sort" ng-click="sortBy('SKU')">SKU</th>
                                <th colspan="4" style="border-bottom-color :  rgba(0, 0, 0, 0.43)">Price (In $)</th>
                                <th rowspan="2" class="sort" ng-click="sortBy('SearchKeyWord')">Profit<br>(In $)</th>
                                <th rowspan="2">Action</th>
                            </tr>
                            <tr style="background : rgba(255, 153, 0, 0.55) none repeat scroll 0 0  ">
                                <th class="sort" ng-click="sortBy('ListPrice')">List</th>
                                <th class="sort" ng-click="sortBy('CurrentPrice')">Current</th>
                                <th class="sort" ng-click="sortBy('CompLowPrice')">Comp. Low</th>
                                <th class="sort" ng-click="sortBy('CompLowPrice')">Recommended</th>
                            </tr>
                            <tr>
                                <td>
                                     <input id="selectall" type="checkbox"  ng-click="selectcurrentpagefunc()"  ng-model="selectall[currentPage]" class="ckeckclass" />
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text" ng-model="query" ng-change="mysearch()" class="inputsearch" placeholder="ASIN"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="titlequery" ng-change="titlesearch()" class="inputsearch" placeholder="Title"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="skuquery" ng-change="skusearch()" class="inputsearch" placeholder="SKU"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="listpricequery" ng-change="listpricesearch()" class="inputsearch" placeholder="List Price"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="currentpricequery" ng-change="currentpricesearch()" class="inputsearch" placeholder="Current Price"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="complowpricequery" ng-change="complowsearch()" class="inputsearch" placeholder="Comp Low Price"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="rpricequery" ng-change="recompricesearch()" class="inputsearch" placeholder="Recommended Price"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="search">
                                    <input class="inputsearch" type="text"  ng-model="profitquery" ng-change="profitsearch()" class="inputsearch" placeholder="Profit"/>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                             <tr ng-repeat="part in pagedItems[currentPage] | filter:search | orderBy:sortKey:reverse">
                                <td>
                                     <input type="checkbox" element="{{part.ASIN}}" ng-checked="selectall[currentPage] || selectlist[part.ASIN] || checked" id="{{part.ASIN}}" ng-model="selectlist[part.ASIN]" ng-click="selectfunc($event)"/>
                                </td>
                                <td>{{part.ASIN}}</td>
                                <td style="max-width: 500px;">
                                    <a ng-click="redirectToLink($event)" element="{{part.ASIN}}" style="cursor: pointer;">{{part.Title}}</a>
                                </td>
                                <td>{{part.SKU}}</td>
                                <td style="text-align: center;">{{part.ListPrice}}</td>
                                <td style="text-align: center;">{{part.CurrentPrice}}</td>
                                <td style="text-align: center;">
                                    <a ng-click="redirectToLink($event)" element="{{part.ASIN}}" style="cursor: pointer;">{{part.CompLowPrice}}</a></td>
                                <td>
                                    <input type="text" style="color: green;text-align: center;font-weight: bold;" value="{{part.RecommendPrice}}" />
                                </td>
                                    <td style="text-align: center;">{{part.Profit}}</td>
                                <td><img style="margin-left: 20px;cursor: pointer;" src="<?php echo base_url();?>/assets/img/searchamz.png" ng-click="AmzSearch(part.Keywords)"/></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>

                  
                        <div class="pagination pull-right pagi_master">
                            <ul>
                                <li ng-class="{disabled: currentPage == 0}">
                                    <a href ng-click="prevPage()">« Prev</a>
                                </li>
                                <li ng-repeat="n in pagenos | limitTo:5"
                                    ng-class="{active: n == currentPage}"
                                ng-click="setPage()">
                                    <a href ng-bind="n + 1">1</a>
                                </li>
                                <li ng-class="{disabled: currentPage == pagedItems.length - 1}">
                                    <a href ng-click="nextPage()">Next »</a>
                                </li>
                            </ul>
                        </div>

                        <div style="margin-top: 20px;">
                            <label class="alllabelcss syncbuttoncss">
                    <img src="<?php echo base_url();?>/assets/img/sync.png" style="margin-right: 2px;"></img>
                    Sync With Amazon
                </label>
                        </div>
            </form>
        </div>

        
        </div>
    </div>
      <?php $this->load->view('footer.php'); ?>
     </div>
    <div id="popupform1">
                <div id="Pnopopup1" class="modal">

                        <div class="pop_map_my_part">
                           
                             <div class="pop_container" style="height: 191px;">
                                    <div class="heading_pop_main">
                                        <h4>

                                            Import AmazonAutoExplore#
                                        </h4>
                                         <span class="close1">&times;</span>
                                    </div>
                                   <div class="pop_pad_all">
                                       <div style="color:#009e47; text-align:center; font-size:17px">{{message}}</div>
                                       <div style="color:#ff3333; text-align:center; font-size:17px">{{errormessage}}</div>
                                    </div>

                                    <div class="button_center">
                                        <input type="hidden" id="ftype"/>
                                    
                                    <input type="text" id="fname" class="input_med" readonly/>
                                    <button type="submit" id="popsavemain" class="temp_upload_new" ng-click="upload()">Upload</button><span style="font-size:16px; font-weight:bold;">(.csv)
                                    </div>
                                    <div class="channel_main">
                                       
                                    </div>

                                    <div class="button_center padding_bottom_15">
                                    <button type="submit" id="save_imp" class="button_download" ng-click="save_imp()">Save</button>
                                    </div>
                            </div>  
                        </div>
                    </div>
                </div>

        
  
    
    
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });
    </script>
    <script>

        //load data in grid            
        var sortingOrder = 'ASIN';
        var app = angular.module('partsApp', ['ngSanitize', 'ngCsv']);
            app.controller('partsCtrl', function($scope, $http, $timeout,$window,$filter) {
                $scope.sortingOrder = sortingOrder;
                $scope.reverse = false;
                $scope.filteredItems = [];
                $scope.groupedItems = [];
                $scope.itemsPerPage = 25;
                $scope.pagedItems = [];
                $scope.pagenos = [];
                $scope.parts = '';
                $scope.currentPage = 0;
                $scope.selectlist=[];
                
                $scope.isLoading = true;
                $http.get("LoadData")
                .then(function (response) 
                { 
                    $scope.isLoading = false;
                    $scope.parts = response.data;
                    console.log($scope.parts);
                    $scope.TotalCount = $scope.parts.length;
                    $scope.parts.forEach(function (part) {
                        part.flag='N';
                    });
                    $scope.upload = function(){
                      document.getElementById('my_file').click();
                    };

                    var exportdata=[];
                    var exportdata_exc = [];
                    exportdata.push ({'ASIN' : 'ASIN#','Title' : 'Title','SKU' : 'SKU','ListPrice' : 'ListPrice','CurrentPrice' : 'CurrentPrice','CompLowPrice' : 'CompLowPrice','RecommendPrice' : 'RecommendPrice','Profit' : 'Profit'});       
                    
                     angular.forEach($scope.parts, function(part) {
                        exportdata.push({'ASIN' :part.ASIN,'Title' :part.Title,'SKU' : part.SKU,'ListPrice' :part.ListPrice,'CurrentPrice' :part.CurrentPrice,'CompLowPrice' : part.CompLowPrice,'RecommendPrice' : part.RecommendPrice,'Profit' : part.Profit});
                        
                        exportdata_exc.push({'ASIN' :part.ASIN,'Title' :part.Title,'SKU' : part.SKU,'ListPrice' :part.ListPrice,'CurrentPrice' :part.CurrentPrice,'CompLowPrice' : part.CompLowPrice,'RecommendPrice' : part.RecommendPrice,'Profit' : part.Profit});
                    });

                   $scope.ForCSVExport = exportdata;
                   $scope.ForExcelExport = exportdata_exc;
                 
                

                                 //pagination part start 
                        // *********************

                         $scope.sortBy = function(propertyName) {
                                $scope.sortKey = propertyName;   //set the sortKey to the param passed
                                $scope.reverse = ($scope.sortKey === propertyName) ? !$scope.reverse : false;
                                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortKey, $scope.reverse);
                                $scope.pagedItems = ""; 
                                $scope.groupToPages();
                            };
                                var searchMatch = function (haystack, needle) {
                                if (!needle) {
                                    return true;
                                }
                                 if(haystack !== null){
                                    return haystack.toString().toLowerCase().indexOf(needle.toString().toLowerCase());
                                }
                                    //return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                                };

                            // *****************init the filtered items*****************
                            $scope.mysearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    /*for(var attr in item) {
                                        if (searchMatch(item[attr], $scope.query))
                                            return true;
                                    }*/
                                      for(var attr in item) {
                                        if(attr == "ASIN") {
                                            if (searchMatch(item[attr], $scope.query)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.titlesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                    if(attr == "Title") {
                                            if (searchMatch(item[attr], $scope.titlequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                              $scope.skusearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                     if(attr == "SKU") {
                                            if (searchMatch(item[attr], $scope.skuquery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.listpricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                    for(var attr in item) {
                                     if(attr == "ListPrice") {
                                            if (searchMatch(item[attr], $scope.listpricequery)  > -1){
                                                return true;
                                            }
                                          }
                                        }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.currentpricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                          for(var attr in item) {
                                            if(attr == "CurrentPrice") {
                                            if (searchMatch(item[attr], $scope.currentpricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                            $scope.complowsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                          for(var attr in item) {
                                            if(attr == "CompLowPrice") {
                                            if (searchMatch(item[attr], $scope.complowpricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             $scope.recompricesearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                          for(var attr in item) {
                                            if(attr == "RecommendPrice") {
                                            if (searchMatch(item[attr], $scope.rpricequery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };
                             $scope.profitsearch = function () {
                                $scope.filteredItems = $filter('filter')($scope.parts, function (item) {
                                          for(var attr in item) {
                                            if(attr == "Profit") {
                                            if (searchMatch(item[attr], $scope.profitquery)  > -1){
                                                return true;
                                            }
                                        }
                                    }
                                    return false;
                                });
                                // take care of the sorting order
                                if ($scope.sortingOrder !== '') {
                                    $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                                }
                                $scope.currentPage = 0;
                                // now group by pages
                                $scope.groupToPages();
                            };

                             
                             // *****************finish the filtered items*****************

                            
                            // calculate page in place
                            $scope.groupToPages = function () {
                            $scope.pagedItems = [];
                            for (var i = 0; i < $scope.filteredItems.length; i++) {
                                if (i % $scope.itemsPerPage === 0) {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                    } else {
                                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                                }
                                }
                            };
                            
                            $scope.range = function (start, end) {
                                var ret = [];
                                if (!end) {
                                    end = start;
                                    start = 0;
                                }
                                for (var i = start; i < end; i++) {
                                    ret.push(i);
                                }
                                $scope.pagenos = ret;
                                return ret;
                            };
                            
                            $scope.prevPage = function () {
                                if ($scope.currentPage > 0) {
                                    $scope.currentPage--;
                                }
                            };
                            
                            $scope.nextPage = function () {
                                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                    $scope.currentPage++;
                                }
                            };
                            
                            $scope.setPage = function () {
                                $scope.currentPage = this.n;
                            };

                            // functions have been describe process the data for display
                            $scope.mysearch();

                            // change sorting order
                            $scope.sort_by = function(newSortingOrder) {
                                if ($scope.sortingOrder == newSortingOrder)
                                    $scope.reverse = !$scope.reverse;

                                $scope.sortingOrder = newSortingOrder;

                                // icon setup
                                $('th i').each(function(){
                                    // icon reset
                                    $(this).removeClass().addClass('icon-sort');
                                });
                                if ($scope.reverse)
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-up');
                                else
                                    $('th.'+new_sorting_order+' i').removeClass().addClass('icon-chevron-down');
                            };
                            
                            $scope.$watch('currentPage', function(pno,oldno){
                              if ((pno+1)%5==0 && $scope.pagedItems.length > 5){
                                var start = pno > oldno ? pno : (pno - 4 ? pno - 4 : 0);
                                $scope.range(start, $scope.pagedItems.length);
                              }
                            });
                            $scope.range($scope.pagedItems.length);
                        //pagination part end
                            $scope.search = {};

                   $scope.exportDataExcel = function () {
                             var mystyle = {
                                sheetid: 'AmazonAutoExplore',
                                headers: true,
                                column: { style: 'background:#a8adc4' },
                              };
                              alasql('SELECT * INTO XLS("DIVE_AmazonAutoExplore.xls",?) FROM ?',[mystyle, $scope.ForExcelExport]);
                      };

                      $scope.import = function(){     
                        //console.log('hi');      
                        $scope.message = '';        
                        $scope.errormessage = '';       
                        $("#popupform1").css("display", "block");       
                        $("#Pnopopup1").css("display", "block");        
                    };
                    $scope.save_imp = function(){
                    var type = document.getElementById('ftype').value;
                
                    if (type == 'application/vnd.ms-excel')
                    {
                        console.log($scope.parts);
                        $scope.isLoading = true; //Loader Image
                            $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Amz_AutoExplore/imp_save',
                                    data    : { 'New_Content' : $scope.parts},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                })
                                .success(function(data) {
                                    $scope.isLoading = false;
                                                 $http({                     
                                                     method  : 'POST',
                                                     url     : '<?php echo base_url();?>index.php/Errorhandling/success_msg',
                                                      data    : 3
                                                    })
                                                 .success(function(response) {
                                                    $('#msg_id').html(response);
                                                    $('.err_modal1').css('display','block');
                                                    $('#msg1').css('display','block');                  
                                                    $scope.isLoading = false;
                                                })
                                    $("#popupform").css("display", "none");
                                    $("#Pnopopup").css("display", "none");
                                    //location.reload(true);
                                        //$scope.message = "Successfully Updated";
                                })
                                .catch(function(err) {
                                     var StatusCode = err.status;
                                     var StatusText = err.statusText;
                                     var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .success(function(response) {
                                        return true;
                                      });
                                var msg = err.status;
                                //failure msg
                                $http({                     
                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                                     .success(function(response) {
                                        $('#msg').html(response);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block');                  
                                        $scope.isLoading = false;
                                    });
                                })

                    }
                    else
                    {
                       $scope.message = '';
                       $scope.errormessage = 'You are trying to Upload a Non-CSV File. Try again!!';
                    }
                }

            });  
        


                    $scope.redirectToLink = function (obj) {
                       var url = "http://www.amazon.com/dp/"+obj.target.attributes.element.value;
                       //$window.open(url, '_blank','width=1200,height=600');
                       $window.open(url, '_blank','width=1200,height=600,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no,top=100, left=200');
                    };   

                    $scope.AmzSearch = function(keyword){
                        var url = "http://www.amazon.com/s/?field-keywords="+keyword;
                        $window.open(url, '_blank','width=1200,height=600,toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no,top=100, left=200');
                    };   



            $scope.globalselect = function(){
                    if ($("#ckbCheckAll").is(":checked"))
                    {
                        var length = $scope.pagedItems.length;
                        $scope.parts.forEach(function (part) {
                            $scope.selectlist[part.ASIN] = true;
                        });
                        for (var i=0; i<=length ; i++)
                        {
                            $scope.selectall[i] = false;
                        }
                    }
                    else
                    {
                        var length = $scope.pagedItems.length;
                        $scope.parts.forEach(function (part) {
                            $scope.selectlist[part.ASIN] = false;
                        });
                        for (var i=0; i<=length ; i++)
                        {
                            $scope.selectall[i] = false;
                        }
                    }
                }

                 var selectallpg = [];
                   $scope.selectcurrentpagefunc = function(){
                            if($("#selectall").is(":checked"))
                            {
                                angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                                    $scope.selectlist[value.ASIN] = true;
                                     
                                    selectallfun1(value.ASIN);
                                });
                            }
                            else{
                                angular.forEach($scope.pagedItems[$scope.currentPage], function(value, key){
                                    $scope.selectlist[value.ASIN] = false;
                                    selectallfun2(value.ASIN);
                                });
                            }
                          
                            if (selectallpg[$scope.currentPage] != $scope.currentPage)
                            {
                                selectallpg[$scope.currentPage] = $scope.currentPage;
                            }
                            else
                            {
                                 selectallpg[$scope.currentPage] = $scope.currentPage;
                            }
                            
                   }

                   

                    var selectallfun1 = function(id){
                                if (dummy[id] == id)
                                {
                                    delete dummy[id];
                                    dummy[id]=id ;
                                }
                                else
                                {
                                    dummy[id]=id ;
                                }
                                var temp = "";
                                for (var i in dummy) {
                                    temp += dummy[i] + ", ";
                                }
                               
                             }

                            var selectallfun2 = function(id){
                                delete dummy[id];
                                var temp = "";
                                for (var i in dummy) {
                                       temp += dummy[i] + ", ";
                                }
                               
                            };
                            //single checkbox select
                             var dummy = [];
                            $scope.selectfunc = function(obj){
                                $('#selectall').prop('checked',false);
                                $('#ckbCheckAll').prop('checked',false);
                                var id = obj.target.attributes.element.value;
                                if ($scope.selectlist[id])
                                {
                                    dummy[id]=id ; 
                                }
                                else
                                {
                                   delete dummy[id];
                                }
                                var temp = "";
                                for (var i in dummy) {
                                       temp += dummy[i] + ", ";
                                }
                                                
                            }

                             $scope.$watch('parts', function(newvalue,oldvalue) {
                            //console.log(newvalue);
                            $scope.finaldata = newvalue;
                          }, true);
                          


        });
//directive for import excel
        app.directive('fileReader', function($timeout) {
                return {
                scope: {
                fileReader:"="
                },
                link: function(scope, element) {               
                $(element).on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                var name = files[0].name;
                var type = files[0].type;
                if (files.length) {
                var r = new FileReader();
                r.onload = function(e) {
                var contents = e.target.result;
               
                var resultvalue = [];
                
                     angular.forEach(contents.split('\n'), function(value, key) {
                        var result = value.split(",");
                        console.log(value);
                        resultvalue.push({'ASIN' : result[0],'Title' : result[1],'SKU' : result[2], 'ListPrice' :  result[3], 'CurrentPrice' :  result[4], 'CompLowPrice' :  result[5], 'RecommendPrice' :  result[6], 'Profit' :  result[7]});
                          });
                     console.log(resultvalue);
                     scope.$apply(function () {
                        document.getElementById('fname').value = name;
                        document.getElementById('ftype').value = type;
                        scope.fileReader = resultvalue;
                        
                    });
                    };
                r.readAsText(files[0]);
                };
              });
            }
          };
      });
</script>

<script>
          $(document).on('click','.close1',function(){
          var name = document.getElementById('fname').value;
           if (name == '')
          {
            $("#popupform1").css("display", "none"); 
          }
          else
          {
            location.reload(true);
            $("#popupform1").css("display", "none");
          } 
        });


          //new popup msg close button
           $(document).on('click','.destroy',function(){
          $(".err_modal").css("display", "none");
          location.reload(true);
      });


    $(document).on('click','.destroy2',function(){
    $(".err_modal").css("display", "none");     
    });

</script>
<script>
    $(document).on('click','.close',function(){
          $("#Pnopopup2").hide(); 
        });    
 


</script>
</body>
</html>
