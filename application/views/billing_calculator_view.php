<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
         <?php echo $title[0]->SellerName;?> : <?php echo $screen;?>
   
       </title>
    <link href="<?php echo base_url().'assets/';?>img/favicon.ico" type="image/x-icon" rel="icon"/>
   
    <script src="/ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
</head>

    <body class="main" ng-app="myApp" ng-controller="myCtrl" ng-cloak>
      <?php $this->load->view('header.php'); ?>
            <div class="container header_bg_clr">
            <div class="wrapper">
                <div class="header_main">
         <?php echo "<h2>".$screen."</h2>" ;?>
           <!--  "<p style='color:red;'>".$ip['cityName']."</p>"; -->
      <!--  <h2> </h2><span style="padding-left:40%; color:red; font-size:14px"></span> -->
                </div>
            </div>
        </div>
         
<div id="msg" style="display: none;"></div>
        <div class="container">
            <div class="wrapper">
            <div class="content_main_all">
                <form id = "bill">
                <div class="channel_main">
                        <label class="label_inner_general_big">Enter no. of Competitors <span style="color:red; font-size:16px;">*</span>  : </label>
                        <p class="drop_down_med1">
                            <input type="text" ng-model="no_of_comp" name="no_of_comp" id="no_of_comp" value= "" class="input_med" required>
                        </p>


                    </div>
                    <div class="channel_main">
                        <label class="label_inner_general_big">Competitor # with > 25K listings <span style="color:red; font-size:16px;">*</span>  : </label>
                        <p class="drop_down_med1">
                            <input type="text" ng-model="compwith" ng-blur="compwithblur()" name="compwith" id="compwith" value= "" class="input_med">
                            <span ng-if="errorMessage">{{ errorMessage }}</span>
                        </p>
                   <center> <div class="error_msg_all" style="color:red; display:none" id="msg_id2">Competitors with 25k listings must be less than the competitor</div> </center>
                     <center> <div  class="error_msg_all" style=" display:none" id="msg_id">Please fill all the details</div> </center>
                    </div>

                    <div class="channel_main">
                        <label class="label_inner_general_big">CBR / IBR Schedule <span style="color:red; font-size:16px;">*</span>  : </label>
                        <select ng-model="services" name="services" class="drop_down_med margin_top_10" id="services" >
                        <option value=""   >Select</option>
                         <?php 
                          foreach ($role as $value)
                          {
                            echo '<option id="'.$value['id'].'" value="'.$value['id'].'">'.$value['ibr_cbr_schedule'].'</option>';
                            
                          }
                            ?>
                        </select>

                    </div>
                   
                    
                    <div class="channel_main">
                     
                    <!-- id="calculate" -->
                        <input type="button"  id ="billing" ng-click="calculate()" class="calculate_bill" value="Calculate Billing" />
                      
                       
                    </div>

                    <div class="channel_main">
                        <label class="label_inner_general_big">Estimated Billing (in $)</label>
                        <p class="drop_down_med1">
                            <input type="text" id="estimate" class="label_disabled align_right" value="{{result}}/month" readonly/>
                        </p>

                    </div>
               

                  
                </div>
                </div>
           </div>
           </div>

        </form>
       
        
        
            <div class="container">
                <div class="footer_main">
                    <div class="wrapper">
                        <div class="footer_logo">
                            <img src="<?php echo base_url();?>assets/img/footer_logo_dive.png">
                        </div>
                        <p class="flt_left margin_top_10">
                            <a href="#" id="lnkEmailAddress">Powered By APA Engineering</a>
                            <img src="<?php echo base_url();?>assets/img/apa_logo.png" class="apa_footer">
                            <a>Copyrights &copy; 2017</a>
                        </p>
                        <p class="flt_right margin_top_10">
                            <a><img src="<?php echo base_url();?>assets/img/twitter.png"></a>
                            <a><img src="<?php echo base_url();?>assets/img/facebook.png"></a>
                            <a><img src="<?php echo base_url();?>assets/img/mail_footer.png"></a>
                        </p>
                    </div>
                </div>
            </div>

    
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".submenu").hover(function () {
                $('.level2', this).not('.in .level2').stop(true, true).addClass('active');

            },
                function () {
                    $('.level2', this).not('.in .level2').stop(true, true).removeClass('active');
                }
            );
            $(".submenu_inner").hover(function () {
                $('.level3', this).not('.in .level3').stop(true, true).addClass('active');

            },
                function () {
                    $('.level3', this).not('.in .level3').stop(true, true).removeClass('active');
                }
            );

        });

    </script>
    <script type="text/javascript">
        
 $('#billing').click(function(e){

        
                    if ( $('#no_of_comp').val() == ""||$('#compwith').val() == ""||$('#services').val() == "") {
                        var no_of_comp=document.getElementById('no_of_comp').value;
                           if(no_of_comp == ""){
                                document.getElementById('no_of_comp').style.borderColor = "red";
                                 $("#msg_id").css("display","block");
                                  //return false;
                                 }
                            else {
                                 document.getElementById('no_of_comp').style.borderColor = "";
                                 $("#msg_id").css("display","none"); 
                            }

                        var compwith=document.getElementById('compwith').value;
                           if(compwith == ""){
                                   document.getElementById('compwith').style.borderColor = "red";
                                   $("#msg_id").css("display","block");
                                  //return false;  
                                 }
                             else {
                                 document.getElementById('compwith').style.borderColor = ""; 
                                $("#msg_id").css("display","none");
                                  
                            } if(no_of_comp == ""){
                                document.getElementById('no_of_comp').style.borderColor = "red";
                                 $("#msg_id").css("display","block");
                                  //return false;
                                 }

                         var services=document.getElementById('services').value;
                           if(services == ""){
                                document.getElementById('services').style.borderColor = "red";
                                 $("#msg_id").css("display","block");
                               
                                 }
                            else {
                                 document.getElementById('services').style.borderColor = "";
                                 $("#msg_id").css("display","none"); 
                            }if(no_of_comp == ""){
                                document.getElementById('no_of_comp').style.borderColor = "red";
                                 $("#msg_id").css("display","block");
                                  //return false;
                            } if(compwith == ""){
                                   document.getElementById('compwith').style.borderColor = "red";
                                   $("#msg_id").css("display","block");
                                  //return false;  
                                 }


                        
                           return false;
                        e.preventDefault();
                }
            else{
                     $("#msg_id").css("display","none");
                     document.getElementById('no_of_comp').style.borderColor = ""; 
                     document.getElementById('compwith').style.borderColor = "";
                     document.getElementById('services').style.borderColor = "";
                     
                }
                    
         });
      
    </script>
      <script>

      var app = angular.module('myApp', []);
        app.controller('myCtrl', function($scope,$http) { 
         var screenname = '<?php echo $screen;?>'; 
          var no_of_comp = '';
          var compwith;
          var services;
               

           no_of_comp = parseInt($('#no_of_comp').val());
           compwith = parseInt($('#compwith').val());
           $scope.calculate = function(){
            if(parseInt($('#no_of_comp').val()) == parseInt($('#compwith').val()) || parseInt($('#compwith').val()) < parseInt($('#no_of_comp').val())){
            
                $http({
                    method: 'POST',
                    url: '<?php echo base_url();?>index.php/Billing_calculator/insert',
                    data: {'no_of_comp' : $scope.no_of_comp,'compwith' : $scope.compwith,'id':$scope.services},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function (response) {
                    angular.forEach(response.data.result[0], function(value, key) {
                        $scope.result = value;
                        $scope.errorMessage='';
                    });
                }).catch(function (err) {

                    
                               var msg =err.status;
                                 var StatusCode = err.status;
                                  var StatusText = err.statusText;
                                  var ErrorHtml  = err.data; 
                          
                                  //Error Log To DB
                                $http({                     
                                    method  : 'POST',
                                    url     : '<?php echo base_url();?>index.php/Errorhandling/ErrorLogToDB',
                                    data    : {'StatusCode' : StatusCode, 'StatusText' : StatusText, 'ErrorHtml' : ErrorHtml,'screenname':screenname},
                                    headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
                                      })
                                       .then(function(response) {
                                        return true;
                                      });
                                //failure msg
   
                             $http({     

                                 method  : 'POST',
                                 url     : '<?php echo base_url();?>index.php/Errorhandling/failure_msg',
                                 data    : msg
                                })
                       .then(function(response) {
                                        //console.log(response.data);
                                        $('#msg').html(response.data);
                                        $('.err_modal').css('display','block');
                                        $('#msg').css('display','block'); 
                                         //$("#Pnopopup2").css('display','none');                 
                                        
                                    });

                });  
            }
            else{
                console.log('error part');
               if ( $('#no_of_comp').val() == ""||$('#compwith').val() == ""||$('#services').val() == "")
               {
                  $("#msg_id2").css("display","none");
               }
               else
               {
                 $("#msg_id2").css("display","block");
               }
              
            }
          }
      
        });


</script>
<script >
    $(document).on('click','.destroy',function(){
    $(".err_modal").css("display", "none");
    $('#msg').css('display','none');      
    });
</script>
</body>
</html>
