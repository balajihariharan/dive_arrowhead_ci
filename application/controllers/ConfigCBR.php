<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/libraries/EBaySession.php';

class ConfigCBR extends CI_Controller
{
	public function __construct()
	{
    	parent::__construct();
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->model('ConfigCBR_Model');
   		$this->load->model('PricesmartModel');
   		 $this->load->library('Screenname');
  	}
	public function index()
	{
		if($this->session->userdata('loginid'))
		{
			$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
	    $mainmenu_data['icononly']   = $getmenu['icononly'];
	    $mainmenu_data['url']        = $getmenu['url'];
	    $mainmenu_data['screen']     = $title;
	    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
	     $ref =$this->ConfigCBR_Model->get_title();
	    $mainmenu_data['title']= $ref;
		$this->load->view('ConfigCBR_View',$mainmenu_data);
	}
	}
		   

	public function GetData()
	{
        $data = json_decode(file_get_contents("php://input"), true);
        $ret= $this->ConfigCBR_Model->GetData();
        $header = $this->ConfigCBR_Model->GetDataHeader();;
        $result = array();
        foreach ($ret as $value) {
        	$result[] = array(
        		'CompID'=>round($value->CompID,2),
        		'CompetitorName'=>$value->CompetitorName,
        		'Route'=>$value->Route,
        		'InceptionDate'=>$value->InceptionDate,
        		'TotalListing'=>round($value->TotalListing,2),
        		'ActiveListing'=>round($value->ActiveListing,2),
        		'FirstListingDate'=>$value->FirstListingDate,
        		'ProcessFlag'=>$value->ProcessFlag
        		);
        }
        $data['result'] = $result;
        $data['header'] = $header;
        echo json_encode($data);
   	}

	public function Save()
	{
		$ebaysetting_result = $this->PricesmartModel->get_ebaysettings_detail();
		if(empty($ebaysetting_result)){
			$userToken = 'AgAAAA**AQAAAA**aAAAAA**YiJ4WQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AClYOgC5KLpwidj6x9nY+seQ**hNUDAA**AAMAAA**hxd7fhvH7dJ2dV6QjUk34cTCJxIYnV+QPCUQkVolgGtfVWZRySLH46HLJLoMqMcL3JoqFbKC2+ZI/Q+xtmduqakPHHljVHY+xr6vu7TcVsrkVHkuHrBZPZlEUzZnq9dOxcIWiyqqEkYp9az0AesWHXwGPLcSM473Nxm1jhIFUvxHBIwYrlXZqgv2ZDwVMiPwg/Np5it3TIyS27eb++sGsGO9/r0qH5f5ai8n5nVzsrVtDZzyzEFM43HlMLECoLYVsF9KurOCIS46vQVHDXp+GYg0N0o1Tt83/uqmoy55CPr3THY3nagUoB+jsb5trU6N7mnJf6sM1lX/dOclH9WPnGBZYss+KzDDHESlRvJCTVON0LadM3nJlqJMmfnPMArxdFf0RERwvmPLawqKYriWM6/kqYPGE4cQEI492Q9FEwCOcTD1AR4ZKW+sQuzfY/IQjfbzAQgvi/CGOkFUQKKu20plXhQjiIe2zVzKP1hrlXNX0hRiHz3k7W1nr66ifGwwg0ZJsuOn1ddkbWKkSDzYZQfXvDkBqu3B4Nkige4PFyKFLhTlbtXIP88opgHwviobULB4YG0wvQMGWPYahkgqpWhyh6ClC8AlkbGgQGDBoNKFCP9ljFspGK9+/YzsTfvOfysIBtVqPPtcEhf10erdNIYbozTwzlkj1TGdWM4SB9DD/EIx/O8QOKcB/WyBXKBmET3LcUKTJVquRIhWKmH846e1BQT+n0hQUDj8Tg4HSQ6V2x8hBsMr3xx5s0dMwVmS';
			$DevID ='bf69e9-f39-4d0-93f-4f358f1d';
			$AppID ='Tamil-get-PRD-bb7edec1b-5b89e35c';
			$CertID ='PRD-b7edec1b74c5-aade-4be-ac19-5f79 ';
			$ServerURL ='https://api.ebay.com/ws/api.dll';
			$Compatability =001;
			$siteID =0;
		}
		else{
			$userToken=$ebaysetting_result[0]['AuthKey'];
			$DevID =$ebaysetting_result[0]['DevID'];
			$AppID =$ebaysetting_result[0]['AppID'];
			$CertID =$ebaysetting_result[0]['CertID'];
			$ServerURL =$ebaysetting_result[0]['ServerURL'];
			$Compatability =$ebaysetting_result[0]['Compatability'];
			$siteID =$ebaysetting_result[0]['SiteID'];
		}
		$data = json_decode(file_get_contents("php://input"), true);	
		$compname  = $data['Compname'];
		$temp =0;       
        $query = $this->db->query("Select CompetitorName from competitor where DeleteFlag=0;");
   		$result = $query->result();
        foreach ($result as $key => $value) {
        	$temp0 = str_replace(' ', '', $compname);
        	$temp1 = trim($temp0);
			$temp2 = strtolower($value->CompetitorName); 
			$temp3 = strtolower($temp1);
			if ( $temp2 == $temp3)
			{
				$temp = 1;	
			}
   		}

   		if ($temp != 1)
   		{
	        $todate = date("Y-m-d");
			$d=strtotime("-1 day");
			$fromdate = date("Y-m-d",$d);	

			$ModTimeFrom = $fromdate.'T00:00:00';
			$ModTimeTo = $todate.'T00:00:00';
			$compatabilityLevel = 535;
			$verb = 'GetStore';

			$requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>';
			$requestXmlBody .= '<GetStoreRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
  			$requestXmlBody .= '<RequesterCredentials>';
	        $requestXmlBody .= '<eBayAuthToken>'.$userToken.'</eBayAuthToken>';
		    $requestXmlBody .= '</RequesterCredentials>';
		    $requestXmlBody .= '<UserID>'.$temp3.'</UserID>';
		    $requestXmlBody .=  '</GetStoreRequest>';

			//Create a new eBay session with all details pulled in from included keys.php
			$session = new eBaySession($userToken, $DevID, $AppID, $CertID, $ServerURL, $compatabilityLevel, $siteID, $verb);

			//send the request and get response
			$responseXml = $session->sendHttpRequest($requestXmlBody);
			$responseDoc = new DomDocument();
			$responseDoc->loadXML($responseXml);
			
			$xml = simplexml_load_string($responseXml);

			$status = $xml->Ack;

			if ($status == 'Success')
			{
				str_replace('T',' ',$xml->Store->LastOpenedTime);
				$inception = explode('.',str_replace('T',' ',$xml->Store->LastOpenedTime));
				$inceptiondate = $inception[0];
				$username      =  $this->session->userdata('username');
	            $roleid        =  $this->session->userdata('roleid');
				$result = $this->ConfigCBR_Model->Save($temp3,$inceptiondate,$username,$roleid);
			}
			else
			{
				$inceptiondate= 0;
			}
		}
		else
		{
			$inceptiondate = 1;
			//$result = $this->ConfigCBR_Model->Save($compname,$inceptiondate);
		}
		echo json_encode($inceptiondate);
	}

    public function Checkbox_Update()
    {   $username      =  $this->session->userdata('username');
	    $roleid        =  $this->session->userdata('roleid');
		$data = json_decode(file_get_contents("php://input"), true);	
		$competitor_listing_update 	= $this->ConfigCBR_Model->Checkbox_Update($data['compid'],$data['key'],$username,$roleid);
    	return true;
    }

    public function Delete()
    {
		$data = json_decode(file_get_contents("php://input"), true);	
		$competitor_listing_update 	= $this->ConfigCBR_Model->Delete($data['compid']);
    	return true;
    }

    public function Refresh()
    {
		$competitor_listing_update 	= $this->ConfigCBR_Model->Refresh();
    	return true;
    }
}
