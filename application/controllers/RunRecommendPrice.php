<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RunRecommendPrice extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->database(); 
        $this->load->model('RunRecommendPriceModel');
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 300);
    }

    public function index()
    {   
        $this->load->view('RunRecommendPriceView');
    }

    public function runfunc()
    {
        $this->RunRecommendPriceModel->index();
        return true;
    }
}
