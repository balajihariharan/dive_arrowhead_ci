<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interchange extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database(); 
   		$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->library('Screenname');
   		ini_set('memory_limit', '256M');
	}

	  public function index(){
    
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
	
        $ref =$this->CompetitorModel->get_title();
        $mainmenu_data['title']= $ref;
		$this->load->view('interchange',$mainmenu_data);
		
}

	public function interchange_get(){
		$intechange_result 							= $this->CompetitorModel->interchange_part_model();
		$intechange_category = array();
			foreach($intechange_result as $value){
				$intechange_category[] = array('MyItems'=>$value->MyItems,
										  'MySKUs'=>$value->MySKUs,
										  'MyPartNo'=>$value->MyPartNo,
										  'CompSKU'=>$value->CompSKU,
										  'CompPartNo'=>$value->CompPartNo,
										  'CompName'=>$value->CompName,
										  'CompItemID'=>$value->CompItemID
										 );

			}
		/*$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($intechange_category));*/
		echo json_encode($intechange_category);
	}
}