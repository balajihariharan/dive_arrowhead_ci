<?php
class CategoryReport extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
    	$this->load->model('CategoryReport_Model');
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->library('Screenname');
  	}
	  public function index(){
		$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
	/*$mainmenu_data['compdata']= $result;*/
	$result2=$this->CategoryReport_Model->index();
	$mainmenu_data['compload']= $result2;
	
	 $ref =$this->CategoryReport_Model->get_title();
    $mainmenu_data['title']= $ref;
	$this->load->view('CategoryReportView',$mainmenu_data);
	}
    

	public function CategoryReport_data(){
		$data = json_decode(file_get_contents("php://input"), true);
		$year = $data['year'];
		$week = $data['week'];
		$catname = $data['catname'];
		$dashboard_result = $this->CategoryReport_Model->getdata($week,$year,$catname);
		$dashboard_category = array();
		foreach($dashboard_result as $value){
			$dashboard_category[] = array('CompId'=>$value['compid'],
									  'Company'=>$value['Company'],
									  'SevenDaysSales'=>(float)$value['7DaysSales'],
									  'SevenDaysSales_lw'=>round($value['7DaysSalesLstWk'],2),
									  'SevenDAYPercentOFTOTAL'=>round($value['7DAYPercentOFTOTAL'],2),
									  'SevenDAYPercentOFTOTAL_lw'=>round($value['7DAYPercentOFTOTALLstWk'],2),
									  'Listing'=>round((float)$value['Listing'],2),
									  'Listing_lw'=>round((float)$value['ListingLstWk'],2),
									  'SoldSevendays'=>round((float)$value['Sold7days'],2),
									  'SoldSevendays_lw'=>round((float)$value['Sold7daysLstWk'],2),
									  'PercentofListingSold'=>round((float)$value['PercentofListingSold'],2),
									  'PercentofListingSold_lw'=>round((float)$value['PercentofListingSoldLstWk'],2),
									  'AvgSalePrice'=>(float)$value['AvgSalePrice'],
									  'AvgSalePrice_lw'=>(float)$value['AvgSalePriceLstWk']);
		}
		$data['dashboard_category'] = $dashboard_category;
		echo json_encode($data);
	}
}
?>
