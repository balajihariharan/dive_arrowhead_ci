<?php
class under_cons extends CI_Controller
{
	public function __construct()
	{
    	parent::__construct();
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
  	}
	public function index()
	{
	if($this->session->userdata('loginid')){
		$mainmenu =  $this->MenuModel->get_menulist();
		$filter = array();
        foreach($mainmenu as $resultvalue){
            $array = explode("/",$resultvalue->submenuurl);
            $filter[] = $array[0];
        }
        if(in_array($this->uri->segment(1),$filter)){
		$unique = array();
		foreach($mainmenu as $mainkey => $mainvalue){
			$unique[] = $mainvalue;
		}
		$selfunique = array();
		$selfunique1 = array();
		$icon = array();
		foreach($unique as $value){
			if($value->MenuID == $value->submenuparentid){
				$selfunique[$value->submenuitem] = $value->MenuItem;
			}
			else{
				$selfunique1[] = $value->MenuItem;
			}
		}
		$resultvalue   = [];
		$unique = array_unique($selfunique);
		foreach($unique as $search) {
			$found = array_keys($selfunique, $search);
			if(count($found) > 1) {
				$resultvalue[$search] = $found;
			}
			else{
				$resultvalue[$search] = $found;
			}
		}
		$resultvalue1 = array();
		$resultvalue2 = array();
		foreach($selfunique1 as $value){
			foreach($resultvalue as $val){
				if(!in_array($value, $val))
				{
					$resultvalue1[$value] = $value;
				}
				if(in_array($value, $val))
				{
					$resultvalue2[$value] = $value;
				}
			}
		}
		$result = array_diff($resultvalue1,$resultvalue2);
		$mainmenu_data['menu_data'] 	= array_merge($resultvalue,$result);
		$icononly = array();
		$urlonly = array();
		foreach($mainmenu as $meinkey => $value){
			foreach($mainmenu_data['menu_data'] as $subkey => $subvalue){
				$icononly[$value->MenuItem] = $value->Icon;
				$urlonly[$value->submenuitem]  = $value->submenuurl;
			}
		}
		$mainmenu_data['icononly'] 	= $icononly;
		$mainmenu_data['url'] 	= $urlonly;
		$this->load->view('under_cons_view',$mainmenu_data);
		}
        else{
            redirect('/Login/index','refresh');    
        }
	}
	else
	{
		redirect('/Login/index','refresh');
	}

}
}
