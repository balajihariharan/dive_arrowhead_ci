<?php
class Login extends CI_Controller
{
	public function __construct()
	{
    	parent::__construct();
    	$this->load->model('Login_Model');
    	$this->load->library('session');
		//$this->load->helper('url');
  	}
	public function index()
	{
		$this->load->view('Login_View');

	}
	public function login()
	{
		$loginid=$this->input->post('loginid');
		$password=md5($this->input->post('password'));
		$result=$this->Login_Model->login($loginid,$password);
		$username= $result['username'];
		$roleid= $result['roleid'];
		$compid=$result['compid'];
		$date=$result['cbrdate'];
		$pagecount=$result['pagecount'];
		$cbrdate = date("d-m-Y", strtotime($date));
		if (empty($result))
		{
		     $errormsg = 'Incorrect Login Id Or Password';
             $this->session->set_flashdata('errormsg',$errormsg);
             redirect('Login/index/');
			/*echo 'Incorrect Login Id Or Password';
			$this->index();*/
			
		}
		else{
		$session_data = array(
        	'username'  => $username,
        	'roleid'     => $roleid,
        	'loginid' => $loginid,
        	'compid' => $compid,
        	'cbrdate' => $cbrdate,
        	'pagecount'=>$pagecount
		 );
	/*	 echo "<pre>";
		 print_r($session_data);
		 exit;*/
		$this->session->set_userdata($session_data);
		redirect('/DashBoard/index', 'refresh');
		}
		
	}
   /* public function screenname(){
    	$scr=$this->Login_Model->screen();
  
             $session_data = array(
        	'username'  => $username,
        	'roleid'     => $roleid,
        	'loginid' => $loginid,
        	'compid' => $compid
		 );
		 /*echo "<pre>";
		 print_r($session_data);
		 exit;
		$this->session->set_userdata($session_data);
		redirect('/DashBoard/index', 'refresh');

    }*/


	public function logout(){
		$session_data = array(
        	'username'  => '',
        	'roleid'     => '',
        	'loginid' => '',
        	'compid' => '',
        	'cbrdate' => '',
        	'pagecount'=>''

		 );
		$this->session->unset_userdata($session_data);
		$this->session->sess_destroy();
		redirect('/Login/index', 'refresh');
	}
}
?>