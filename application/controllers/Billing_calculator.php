<?php
class Billing_calculator extends CI_controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('Billing_calculator_model');
    $this->load->model('CompetitorModel');
    $this->load->model('MenuModel');
    $this->load->library('Screenname');
  }
      public function index(){
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $ret =$this->Billing_calculator_model->get_rolename();
    $mainmenu_data['role']=$ret;
    
    $ref =$this->Billing_calculator_model->get_title();
    $mainmenu_data['title']= $ref;

    $this->load->view('billing_calculator_view',$mainmenu_data);
    }
   

  public function insert()
  { 
    $data = json_decode(file_get_contents("php://input"), true);
    $id = $data['id'];
    $no_of_comp = $data['no_of_comp'];
    $compwith = $data['compwith'];
    $res=$this->Billing_calculator_model->save($compwith,$no_of_comp,$id);
    echo json_encode(array('result'=>$res));
  }
}
?> 