<?php
class Manage_Access extends CI_controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('Manage_Access_Model');
    $this->load->model('CompetitorModel');
    $this->load->model('MenuModel');
    $this->load->library('Screenname');
  }
    public function index(){
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
            $ret =$this->Manage_Access_Model->get_rolename();
            $mainmenu_data['role']=$ret;
            $ref =$this->Manage_Access_Model->get_title();
            $mainmenu_data['title']= $ref;
            $this->load->view('Manage_Access_View',$mainmenu_data);
 
        }
        
  public function get_screendata(){
    $roleid = $_POST['roleid'];
 
    $res=$this->Manage_Access_Model->get_screendata($roleid);
   
    echo json_encode($res);

  
  }
  public function save(){
     $screennamed = '';
    if(count($_POST['selected_screen']) >= 1){
      $selectroleid = $_POST['selectrole'];
      $inactive_flagresult = array_diff($_POST['selected_screen'],$_POST['active_flag']);
      $active_flagresult = $_POST['active_flag'];
      $username=  $this->session->userdata('username');
      //$roleid =   $this->session->userdata('roleid');
      $screenids = $this->Manage_Access_Model->save_model();
      $inactive_result = array();
      foreach($screenids as $mainvalue){
          foreach($inactive_flagresult as $value){
            if($mainvalue->screenname == $value){
              $inactive_result[] = $mainvalue->screenid;
            }
        }
      }
      $active_result = array();
      foreach($screenids as $mainvalue){
          foreach($active_flagresult as $value){
            if($mainvalue->screenname == $value){
              $active_result[] = $mainvalue->screenid;
            }
        }
      }
      $result = $this->Manage_Access_Model->get_save_screendata($selectroleid,$inactive_result,$active_result,$username);      
    }
  }
}
?>