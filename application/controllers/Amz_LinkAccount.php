<?php
defined ('BASEPATH') OR exit('No direct script access allowed');

class Amz_LinkAccount extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
        $this->load->model('Manage_User_Model'); 
        $this->load->model('MenuModel'); 
        $this->load->library('Screenname');   
    }
    public function index(){   
        $getmenu    = $this->screenname->getscreen();
        $router     =& load_class('Router', 'core');
        $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
        $title      = '';
        foreach($getmenu['url'] as $key => $value){
          if($value == $indexroute){
            $title = $key;
          }
        }
        $mainmenu_data['icononly']   = $getmenu['icononly'];
        $mainmenu_data['url']        = $getmenu['url'];
        $mainmenu_data['screen']     = $title;
        $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    	$ref =$this->Manage_User_Model->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('Amz_LinkAccountView',$mainmenu_data);
    }
 }
?>
        
        
        
