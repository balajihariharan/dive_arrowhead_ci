<?php
/*defined('BASEPATH') OR exit('No direct script access allowed');*/
/*require_once 'application/libraries/EBaySession.php';
require_once 'application/libraries/keys.php';
require_once(APPPATH.'libraries/FPDF Plugin/fpdf.php');*/ //Export Plugin
//require_once 'application/libraries/pusher/vendor/autoload.php';//pusher

class UnsoldItem extends CI_Controller {

	  public function __construct(){
  		parent::__construct();
   		 $this->load->database();
   		 $this->load->helper( 'Metrics_helper' ); 
   		 $this->load->model('UnsoldItemModel');
		   $this->load->library('keysnew');
		   $this->load->library('Screenname');
   		 $this->load->model('MenuModel');
       ini_set('memory_limit', '256M');
      ini_set('max_execution_time', 0);
	}

	  public function index()
	{
    if($this->session->userdata('loginid'))
    {
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $ref =$this->UnsoldItemModel->get_title();
    $mainmenu_data['title']= $ref;
    $this->load->view('UnsoldItemView',$mainmenu_data);
      }
  }
    
    public function getcategory(){

    $data = json_decode(file_get_contents("php://input"), true);
    $data =$this->UnsoldItemModel->get_tab1_categorycombo();
    echo json_encode($data);
       

     }
   


    public function GetData()
      {

    $data = json_decode(file_get_contents("php://input"), true);
    
    $ret=$this->UnsoldItemModel->weekdata($data['Name'],$data['category']);
     $result = array();
        foreach ($ret as $value) {

          $result[] = array(
            'ItemID'=>$value->ItemID,
            'SKU'=>$value->SKU,
            'MyPart'=>$value->Partno,
            'Category'=>$value->category,
            'Title'=>$value->Title,
            'Quantity'=>round($value->Quantity,2),
            'Price'=>round($value->Price,2),
            'QtySold'=>round($value->QtySold),
          /*  'LastSold'=>0*/
            );
        }
    $data['result'] = $result;
    echo json_encode($data);
     
   

      }
  }