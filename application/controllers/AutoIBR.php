<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'application/libraries/EBaySession.php';
require_once 'application/libraries/keys.php';
require_once(APPPATH.'libraries/FPDF Plugin/fpdf.php');
//require_once('keys.php');
require_once 'application/libraries/EBaySession.php';
//require_once('DBConnect.php');
//require_once('email.php');

class AutoIBR extends CI_Controller {

	  public function __construct(){
  	   parent::__construct();
   		 $this->load->database();
   		 $this->load->helper( 'Metrics_helper' ); 
   		 $this->load->model('AutoIBRModel');
		   $this->load->library('keysnew');
		   $this->load->library('Screenname');
		   $this->load->model('CompetitorModel');
   		 $this->load->model('MenuModel');
   		ini_set('memory_limit', '256M');
   		ini_set('max_execution_time', 30000);
	  }

	  public function index(){	
  	  $getmenu = $this->screenname->getscreen();
      $router =& load_class('Router', 'core');
      $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
      $title = '';
        foreach($getmenu['url'] as $key => $value){
          if($value == $indexroute){
            $title = $key;
          }
        }
		  $mainmenu_data['icononly']  = $getmenu['icononly'];
	    $mainmenu_data['url']       = $getmenu['url'];
	    $mainmenu_data['screen']    = $title;
	    $mainmenu_data['menu_data'] = $getmenu['menu_data'];
	    $ref =$this->AutoIBRModel  -> get_title();
      $mainmenu_data['title']     = $ref;
      $mainmenu_data['ShowMasked']= $ref[0]->ShowMasked;
      $this->load->view('Auto_IBR_View',$mainmenu_data);
		}

   
  public function competitor_category(){
     $categories   = $this->AutoIBRModel->get_competitor_category();
    echo json_encode( $categories);
  }
  public function getcategorydetails(){
    $data = json_decode(file_get_contents("php://input"), true);
    $Categoryid = $data['Categoryid'];
    $categorydetails   = $this->AutoIBRModel->getcategorydetails($Categoryid);
        foreach ($categorydetails as $value) {
          $result[] = array(
            'ItemID'        =>$value->itemid,
            'SKU'           =>$value->sku,
            'Title'         =>$value->title,
            'CurrentPrice'  =>round($value->price,2),
            'costprice'     =>round($value->costprice,2),
            'freight'       =>round($value->freight,2),
            'CompLowPrice'  =>round($value->CompLowPrice,2),
            'CompItemID'    =>$value->CompItemID,
            'RecommendedPrice'=>round($value->RecommendedPrice,2),
            'Profit'        => round((($value->RecommendedPrice)-($value->costprice + $value->freight)),2),
            'ApproveFlag'   =>$value->ApproveFlag
          );
        }
    $data['result'] = $result;
    echo json_encode($data);
  }
  
  public function gotoebay(){ 
    $data    = json_decode(file_get_contents("php://input"), true);
    $ItemID  = $data['ItemID'];
    $ebayurl = $this->AutoIBRModel->geturl($ItemID);
    $url     = $ebayurl[0]['ebay_url'];
    echo $url;
  }

  public function save(){
    $data = json_decode(file_get_contents("php://input"), true);
    $username      =  $this->session->userdata('username');
    $roleid        =  $this->session->userdata('roleid');
    $result = $data['data'];
    print_r($data);
    
    foreach ($result as $value) 
    {
      $ItemID           = $value['ItemID'];
      $RecommendedPrice = $value['RecommendedPrice'];
      $ApproveFlag      = $value['ApproveFlag'];
      $this->AutoIBRModel->save($ItemID,$RecommendedPrice,$ApproveFlag);
    }
  }


}