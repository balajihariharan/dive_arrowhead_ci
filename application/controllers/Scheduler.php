<?php
defined ('BASEPATH') OR exit('No direct script access allowed');
class Scheduler extends CI_Controller
{
  public function __construct(){
    parent::__construct();
    $this->load->model('Scheduler_Model');
    $this->load->model('CompetitorModel');
    $this->load->model('MenuModel');
     $this->load->library('Screenname');
  }
  public function index(){
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
          $ref =$this->Scheduler_Model->get_title();
        $mainmenu_data['title']= $ref;

        $this->load->view('Scheduler_View',$mainmenu_data);
       
  }   
  public function get_data(){
    $data = json_decode(file_get_contents("php://input"), true);
    $name = $data['Name'];
    $ret=$this->Scheduler_Model->get_time($name);

   /* echo"<pre>";
    print_r($name);
    exit;*/

    echo json_encode($ret);
       }
  public function save(){  

    $apppath = APPPATH;
    $app = explode("\\",$apppath);
    $path = ''.$app[0].' \ '.$app[1].' \ '.$app[2].'';
    //taskname
    $task=$this->Scheduler_Model->tasksched();
    $AccountName = $task->AccountName;
    $AccountID = $task->AccountID;

    $weekly_days  = array();//to store the days selected
    $services     = $_POST['services'];
    $daily_flag   = '';
    $weekly_flag  = '';
    $monthly_flag = '';
    if(isset($_POST['myschedules']) && $_POST['myschedules'] != ''){
        if($_POST['myschedules'] == 'daily')  { $daily_flag   = 1;} else{ $daily_flag = 0;}
        if($_POST['myschedules'] == 'weekly') { $weekly_flag  = 1;} else{ $weekly_flag = 0;}
        if($_POST['myschedules'] == 'monthly'){ $monthly_flag = 1;} else{ $monthly_flag = 0;}
    }
    $startingfrom = '';
      if(isset($_POST['fromdate']))   { $startingfrom = $_POST['fromdate'];}
      if(isset($_POST['sunday']))     { $sunday     = 1;  $weekly_days[] ='SUN';}   else{$sunday    = 0;}
      if(isset($_POST['monday']))     { $monday     = 1;  $weekly_days[] ='MON';}   else{$monday    = 0;}
      if(isset($_POST['tuesday']))    { $tuesday    = 1;  $weekly_days[] ='TUE';}   else{$tuesday   = 0;}
      if(isset($_POST['wednesday']))  { $wednesday  = 1;  $weekly_days[] ='WED';}   else{$wednesday = 0;}
      if(isset($_POST['thursday']))   { $thursday   = 1;  $weekly_days[] ='THU';}   else{$thursday  = 0;}
      if(isset($_POST['friday']))     { $friday     = 1;  $weekly_days[] ='FRI';}   else{$friday    = 0;}  
      if(isset($_POST['saturday']))   { $saturday   = 1;  $weekly_days[] ='SAT';}   else{$saturday  = 0;}
      if(isset($_POST['repeatutil'])) { $repeatutil = $_POST['repeatutil'];}
      if(isset($_POST['repeattime'])) { $repeattime = $_POST['repeattime'];}
    $username =  $this->session->userdata('username');
    $roleid   =  $this->session->userdata('roleid');
    $repeatuntil = explode("-",$repeatutil);
    $repeatime   = explode(":",$repeattime);
    
    //schedulding task 
    if($_POST['myschedules'] == 'daily'){
        
        $command = 'SchTasks /Create /SC DAILY /TN  "'.$AccountName.'" /TR "'.$path.'\ '.$AccountName.'PG\CompetitorCrawler.php" /ST '.$repeatime[0].':'.$repeatime[1];
        print_r($command);
        $output  = shell_exec($command);
        echo "<pre>$output</pre>";
         if($output == 'WARNING: The task name "'.$AccountName.'" already exists. Do you want to replace it (Y/N)? ' ){

           //DELETE THE EXISTSING TASK
               $command2 = 'SchTasks /Delete /F /TN "'.$AccountName.'"';
               echo  $command2;
               $output2 = shell_exec($command2);
               
           //CREATING NEW TASK AFTER DELETING
            $command = 'SchTasks /Create /SC DAILY /TN  "'.$AccountName.'" /TR "'.$path.'\ '.$AccountName.'PG\CompetitorCrawler.php" /ST '.$repeatime[0].':'.$repeatime[1];
              print_r($command);
              $output  = shell_exec($command);
              echo "<pre>$output</pre>";
            }

     }
    if($_POST['myschedules'] == 'weekly'){
     
        $command = 'SchTasks /Create /SC WEEKLY /D '.implode(",",$weekly_days).' /TN "'.$AccountName.'" /TR "'.$path.'\ '.$AccountName.'PG\CompetitorCrawler.php" /ST '.$repeatime[0].':'.$repeatime[1];
        $output  = shell_exec($command);
        echo $command;
         //echo "<pre>$output</pre>";
        if($output == 'WARNING: The task name "'.$AccountName.'" already exists. Do you want to replace it (Y/N)? ' ){

              //DELETE THE EXISTSING TASK
              $command2 = 'SchTasks /Delete /F /TN "'.$AccountName.'"';
               echo  $command2;
              $output2 = shell_exec($command2);

              //CREATING NEW TASK AFTER DELETING
              $command = 'SchTasks /Create /SC WEEKLY /D '.implode(",",$weekly_days).' /TN "'.$AccountName.'" /TR "'.$path.'\ '.$AccountName.'PG\CompetitorCrawler.php" /ST '.$repeatime[0].':'.$repeatime[1];
              $output  = shell_exec($command);
              echo "<pre>$output</pre>";
        }
        //echo "<pre>$output</pre>";
      }
     if($_POST['myschedules'] == 'monthly'){

        $command = 'SchTasks /Create /SC MONTHLY /D '.$repeatuntil[2].' /TN "'.$AccountName.'"  /TR "'.$path.'\ '.$AccountName.'PG\CompetitorCrawler.php" /ST '.$repeatime[0].':'.$repeatime[1];
        echo $command;
        $output  = shell_exec($command);
        echo "<pre>$output</pre>";
        if($output == 'WARNING: The task name "'.$AccountName.'" already exists. Do you want to replace it (Y/N)? ' ){

              //DELETE THE EXISTSING TASK
              $command2 = 'SchTasks /Delete /F /TN "'.$AccountName.'"';
               echo  $command2;
              $output2 = shell_exec($command2);

              //CREATING NEW TASK AFTER DELETING
              $command = 'SchTasks /Create /SC MONTHLY /D '.$repeatuntil[2].' /TN "'.$AccountName.'"  /TR "'.$path.'\ '.$AccountName.'PG\CompetitorCrawler.php" /ST '.$repeatime[0].':'.$repeatime[1];
              echo $command;
              $output  = shell_exec($command);
              echo "<pre>$output</pre>";
        }

      }

      $result = $this->Scheduler_Model->dive_scheduler_save($services,$daily_flag,$weekly_flag,$monthly_flag,$startingfrom,$sunday,$monday,$tuesday,$wednesday,$thursday,$friday,$saturday,$repeattime,$repeatutil,$username, $roleid );
      if($result == NULL){
       echo 'success';
      }
  }
}
 ?>

