<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/libraries/EBaySession.php';
require_once 'application/libraries/keys.php';
require_once 'application/libraries/pusher/vendor/autoload.php';//pusher

class ConfigIBR extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Config_IBR_model');
      $this->load->model('MenuModel');
      $this->load->model('Ebaysettings_Model');
       $this->load->library('Screenname');
    }
  public function index()
  {
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
     $ref                         =$this->Ebaysettings_Model->get_title();
    $mainmenu_data['title']      = $ref;
     $this->load->view('configIBR',$mainmenu_data);
    /*echo "<pre>";
    print_r($mainmenu_data);
    exit;*/
    }

  public function suggest_categoryname(){
    $competitor_category     = $this->Config_IBR_model->search_category_name();
    echo json_encode($competitor_category);
  }
 public function getstorename(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->getstorename();
    echo json_encode($data_result);
  }
  public function getcategory(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->get_categorycombo($data['CompID']);
    echo json_encode($data_result);
  }
  public function categorycombo_result(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->categorycombo_result_model($data['category'],$data['CompID']);
    $result1 = array();

    foreach($data_result as $value){
        $result1[] = array('itemid'=>$value->itemid,
                    'categoryid'=>$value->categoryid,
                    'sku'=>$value->sku,
                    'title'=>(string)($value->title),
                    'price'=>(float)(round($value->price,2)),
                    'Keywords_AND'=>(string)($value->Keywords_AND),
                    'Keywords_OR'=>(string)($value->Def_Key_words),
                    'Exclude_keywords'=>(string)($value->Exc_Key_words),
                    'Category_Include'=>(string)$value->Category_Include,
                    'Category_Exclude'=>(string)$value->Category_Exclude,
                    'my_price'=>(float)(round($value->my_price,2)),
                    'Percent'=>(float)(round($value->Percent,2)),
                    'Priceform_from'=>(float)(round($value->Priceform_from,2)),
                    'Priceform_to'=>(float)(round($value->Priceform_to,2)),
                    'Conditions'=>(float)($value->Conditions),
                    'Shipping'=>(float)($value->Shipping),
                    'Location'=>(float)($value->Location),
                    'HandlingTime'=>(float)($value->HandlingTime),
                    'TopRated'=>(float)($value->TopRated),
                    'Titleincludeflag'=>(float)($value->Titleincludeflag),
                    'Sort_order'=>(float)($value->Sort_order),
                    'Sellers_Include'=>(string)($value->Sellers_Include),
                    'Sellers_Exclude'=>(string)($value->Sellers_Exclude),
                    'Lastrun_id'=>(float)($value->Lastrun_id),
                    'Active_flag'=>(float)($value->Active_flag),
                    'ebay_url'=>(string)($value->ebay_url),
                    'crawler_url'=>(string)($value->crawler_url),
                    'category'=>(string)($value->category),
                    'MPN'=>$value->MPN,
                    'IPN'=>$value->IPN,
                    'OPN'=>$value->OPN,
                    'pictureurl'=>$value->pictureurl,
                    'Exclude_key_validate'=>(string)($value->Exclude_keywords),
                    'Keywords_OR_validate'=>(string)($value->Keywords_OR),
                    'compid'=>$value->compid,
                    'storename'=>$value->storename,
                    'AppID'=>$value->AppID
                   );
      }
  /* echo "<pre>";
    print_r($data_result);
    exit;*/
    echo json_encode($result1);
  }
  public function unprocessedcombo_result(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->unprocessedcombo_result($data['CompID']);
    $result1 = array();

    foreach($data_result as $value){
        $result1[] = array('itemid'=>$value->itemid,
                    'categoryid'=>$value->categoryid,
                    'sku'=>$value->sku,
                    'title'=>(string)($value->title),
                    'price'=>(float)(round($value->price,2)),
                    'Keywords_AND'=>(string)($value->Keywords_AND),
                    'Keywords_OR'=>(string)($value->Def_Key_words),
                    'Exclude_keywords'=>(string)($value->Exc_Key_words),
                    'Category_Include'=>(string)$value->Category_Include,
                    'Category_Exclude'=>(string)$value->Category_Exclude,
                    'my_price'=>(float)(round($value->my_price,2)),
                    'Percent'=>(float)(round($value->Percent,2)),
                    'Priceform_from'=>(float)(round($value->Priceform_from,2)),
                    'Priceform_to'=>(float)(round($value->Priceform_to,2)),
                    'Conditions'=>(float)($value->Conditions),
                    'Shipping'=>(float)($value->Shipping),
                    'Location'=>(float)($value->Location),
                    'HandlingTime'=>(float)($value->HandlingTime),
                    'TopRated'=>(float)($value->TopRated),
                    'Titleincludeflag'=>(float)($value->Titleincludeflag),
                    'Sort_order'=>(float)($value->Sort_order),
                    'Sellers_Include'=>(string)($value->Sellers_Include),
                    'Sellers_Exclude'=>(string)($value->Sellers_Exclude),
                    'Lastrun_id'=>(float)($value->Lastrun_id),
                    'Active_flag'=>(float)($value->Active_flag),
                    'ebay_url'=>(string)($value->ebay_url),
                    'crawler_url'=>(string)($value->crawler_url),
                    'category'=>(string)($value->category),
                    'MPN'=>$value->MPN,
                    'IPN'=>$value->IPN,
                    'OPN'=>$value->OPN,
                    'pictureurl'=>$value->pictureurl,
                    'Exclude_key_validate'=>(string)($value->Exclude_keywords),
                    'Keywords_OR_validate'=>(string)($value->Keywords_OR),
                    'compid'=>$value->compid,
                    'storename'=>$value->storename

                );
      }
  /* echo "<pre>";
    print_r($data_result);
    exit;*/
    echo json_encode($result1);
  }
    public function skucombo_result(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->skucombo_result($data['SKU'],$data['CompID']);
    $result1 = array();
    foreach($data_result as $value){
        $result1[] = array('itemid'=>$value->itemid,
                    'categoryid'=>$value->categoryid,
                    'sku'=>$value->sku,
                    'title'=>(string)($value->title),
                    'price'=>(float)(round($value->price,2)),
                    'Keywords_AND'=>(string)($value->Keywords_AND),
                    'Keywords_OR'=>(string)($value->Def_Key_words),
                    'Exclude_keywords'=>(string)($value->Exc_Key_words),
                    'Category_Include'=>(string)$value->Category_Include,
                    'Category_Exclude'=>(string)$value->Category_Exclude,
                    'my_price'=>(float)(round($value->my_price,2)),
                    'Percent'=>(float)(round($value->Percent,2)),
                    'Priceform_from'=>(float)(round($value->Priceform_from,2)),
                    'Priceform_to'=>(float)(round($value->Priceform_to,2)),
                    'Conditions'=>(float)($value->Conditions),
                    'Shipping'=>(float)($value->Shipping),
                    'Location'=>(float)($value->Location),
                    'HandlingTime'=>(float)($value->HandlingTime),
                    'TopRated'=>(float)($value->TopRated),
                    'Titleincludeflag'=>(float)($value->Titleincludeflag),
                    'Sort_order'=>(float)($value->Sort_order),
                    'Sellers_Include'=>(string)($value->Sellers_Include),
                    'Sellers_Exclude'=>(string)($value->Sellers_Exclude),
                    'Lastrun_id'=>(float)($value->Lastrun_id),
                    'Active_flag'=>(float)($value->Active_flag),
                    'ebay_url'=>(string)($value->ebay_url),
                    'crawler_url'=>(string)($value->crawler_url),
                    'category'=>(string)($value->category),
                    'MPN'=>$value->MPN,
                    'IPN'=>$value->IPN,
                    'OPN'=>$value->OPN,
                    'pictureurl'=>$value->pictureurl,
                    'Exclude_key_validate'=>(string)($value->Exclude_keywords),
                    'Keywords_OR_validate'=>(string)($value->Keywords_OR),
                    'storename'=>$value->storename,
                    'compid'=>$value->compid
                  

                );
      }
    echo json_encode($result1);
  }


  public function save_key_search(){
    $data = json_decode(file_get_contents("php://input"), true);
    // echo "<pre>";
    // print_r($data);
    // exit;
    $username=  $this->session->userdata('username');
    $splitcategory_exclude = array();
    $splitcategory_include = array();  
    if(isset($data['catagory_id'])){
      foreach($data['catagory_id'] as $key => $value){
        if($data['catagory_id'][$key]['name']){
          $symbol = $data['catagory_id'][$key]['name'];
          if($symbol[0] == '!' && $symbol[1] == '='){
            $splitcategory_exclude[$data['catagory_id'][$key]['id']] = $data['catagory_id'][$key]['id'];
          }
          else{
            $splitcategory_include[$data['catagory_id'][$key]['id']] = $data['catagory_id'][$key]['id'];
          }
        }
      }
      $splitcategory_exclude = implode(',', $splitcategory_exclude);
      $splitcategory_include = implode(',', $splitcategory_include);
    }
    else{
      $splitcategory_exclude = '';
      $splitcategory_include = '';  
    }

    $sellerexclude = array();
    $sellerinclude = array();
    if(isset($data['comptitor'])){
      foreach($data['comptitor'] as $key => $value){
        $symobol = $data['comptitor'][$key]['key'];
          if($symobol[0] == '!' && $symobol[1] == '='){
            $sellerexclude[] = substr($data['comptitor'][$key]['key'],2);
          }
          else{
            $sellerinclude[] = $data['comptitor'][$key]['key'];
          }
      }
      $sellerexclude = implode(',', $sellerexclude);
      $sellerinclude = implode(',', $sellerinclude);
    }
    else{
      $sellerexclude = '';
      $sellerinclude = '';
    }

    if(isset($data['itemid'])) {$itemid = $data['itemid'];}else{$itemid = '';}
    if(isset($data['sku'])) {$sku = $data['sku'];}else{$sku = '';}
    if(isset($data['keywords_and'])) {$keywords_and = $data['keywords_and'];}else{$keywords_and = '';}
    if(isset($data['keywords_or'])) {$keywords_or = $data['keywords_or'];}else{$keywords_or = '';}
    if(isset($data['keywords_exclude'])) {$keywords_exclude = $data['keywords_exclude'];}else{$keywords_exclude = '';}
    if(isset($data['myprice'])) {$myprice = $data['myprice'];}else{$myprice = '';}
    if(isset($data['percentage'])) {$percentage = $data['percentage'];}else{$percentage = '';}
    if(isset($data['from_price'])) {$from_price = $data['from_price'];}else{$from_price = '';}
    if(isset($data['to_price'])) {$to_price = $data['to_price'];}else{$to_price = '';}
    if(isset($data['condition'])) {$condition = $data['condition'];}else{$condition = '';}
    if(isset($data['shipping']) && $data['shipping'] != ''){$shipping = 1;}else{$shipping = 0;}
    if(isset($data['location']) && $data['location'] != '') {$location = 1;}else{$location = 0;}
    if(isset($data['handlingtime']) && $data['handlingtime'] != ''){$handlingtime = 1;}else{$handlingtime = 0;}
    if(isset($data['toprated']) && $data['toprated'] != ''){$TopRated = 1;}else{$TopRated = 0;}
    if(isset($data['sortorder'])){$sortorder = $data['sortorder'];}else{$sortorder = '';}
    if(isset($data['eBay_url'])){$ebay_url = $data['eBay_url'];}else{$ebay_url = '';}
    if(isset($data['crawlerurl'])){$crawlerurl = $data['crawlerurl'];}else{$crawlerurl = '';}
    if(isset($data['flag'])){$def_flag = $data['flag'];}else{$def_flag = '';}
    if(isset($data['catagoryid'])){$def_cat_id = $data['catagoryid'];}else{$def_cat_id = '';}
   // print_r($data['titleinclude']);
    if(isset($data['titleinclude']) && $data['titleinclude'] == 1 ){ $Titleincludeflag = 1;}
    if(isset($data['titleinclude'])&& $data['titleinclude'] == 0) { $Titleincludeflag = 0;}
    if(isset($data['updateflag'])&& $data['updateflag'] == 0) { $UpdateFlag = 0;}
    if(isset($data['updateflag'])&& $data['updateflag'] == 1) { $UpdateFlag = 1;}
    if(isset($data['Compid'])){$compid= $data['Compid'];}else{$compid = '';}


    

    //$ebay_url = $data['eBay_url'];



    $savedata   = $this->Config_IBR_model->save_key_search_model($compid,$itemid,$sku,$keywords_and,$keywords_or,$keywords_exclude,
                                      $splitcategory_include,$splitcategory_exclude,$myprice,$percentage,$from_price,$to_price,$condition,$shipping,$location,$handlingtime,$TopRated,$sellerinclude,$sellerexclude,$sortorder
                                      ,$ebay_url,$username,$crawlerurl,$def_flag,$def_cat_id,$Titleincludeflag,$UpdateFlag);
    echo json_encode($savedata);
  }

  public function imp_save(){
    $data = json_decode(file_get_contents("php://input"), true);
    $result = $data['New_Content'];
    foreach ($result as $value)
        {
      if ($value['ItemID'] != '' && $value['ItemID'] != 'ItemID')
      {
          $ItemID    = $value['ItemID'];
          $Keywords_AND = $value['Keywords_AND'];
          $Keywords_OR = $value['Keywords_OR'];
          $Exclude_keywords = $value['Exclude_keywords'];
          $Category_Include = $value['Category_Include'];
          $Category_Exclude = $value['Category_Exclude'];
          $Percent = $value['Percent'];
          $my_price = $value['my_price'];
          $Priceform_from = $value['Priceform_from'];
          $Priceform_to = $value['Priceform_to'];
          $Conditions = $value['Conditions'];
          $Shipping = $value['Shipping'];
          $Location = $value['Location'];
          $Sellers_Include = $value['Sellers_Include'];
          $Sellers_Exclude = $value['Sellers_Exclude'];
          $Sort_order = $value['Sort_order'];
          $urlSort_order = '';
          
         
         
          
         
         if($Priceform_from == '' && $Priceform_to == ''){
          $min_price        = $my_price / 100 * $Percent;
          $Priceform_from      = floatval($my_price)-floatval($min_price);
          $Priceform_to       = floatval($my_price)+floatval($min_price);
           }

           
          //$keyword_AND_OR_id
          if($Keywords_AND!=''){
          $urlKeywords_and     = str_replace('|' , ',' , $Keywords_AND);
          }else{$urlKeywords_and='';}
          if($Keywords_OR !=''){
          $Keywordsor     = str_replace('|' , ' ' , $Keywords_OR);
          $urlKeywords_or = '('.str_replace(' ' , ',' , $Keywordsor).')';
          }else{$urlKeywords_or ='';}
          if($Exclude_keywords !=''){
          $urlExclude_keywords  ='-'.str_replace('|' , '-' , $Exclude_keywords);
          }else{$urlExclude_keywords='';}

           if($Sellers_Include !=''){
            $result = explode("|",$Sellers_Include);

            $urlSellersInclude = '&itemFilter(0).name=Seller%20';
            foreach($result as $key=>$value){
             $urlSellersInclude .=  "&itemFilter(0).value(".$key.")=".$value;
            }
             }else{$urlSellersInclude='';}
              
            if($Sellers_Exclude !=''){
            $result = explode("|",$Sellers_Exclude);

            $urlSellersExclude = '&itemFilter(0).name=SellersExclude%20';
            foreach($result as $key=>$value){
             $urlSellersExclude .=  "&itemFilter(0).value(".$key.")=".$value;
            }
             }else{$urlSellersExclude='';}

             if($Category_Include !=''){
            $result = explode("|",$Category_Include);

            $urlCategory = '&itemFilter(0).name=Category%20';
            foreach($result as $key=>$value){
             $urlCategory .=  "&itemFilter(0).value(".$key.")=".$value;
            }
             }else{$urlCategory='';}
             
             if($Category_Exclude !=''){
            $result = explode("|",$Category_Exclude);

            $urlCategoryExclude = '&itemFilter(0).name=ExcludeCategory%20';
            foreach($result as $key=>$value){
             $urlCategoryExclude .=  "&itemFilter(0).value(".$key.")=".$value;
            }
             }else{$urlCategoryExclude='';}

            if($Conditions !='')
            {
             $urlConditions = '&itemFilter(0).name=Condition&itemFilter(0).value='.$Conditions;
            }
            else{$urlConditions='';}
            if($Location !='')
            {
             $Location = '&itemFilter.name=location&itemFilter.value='.$Location;
            }
            else{$Location='';}
            if($Shipping !='')
            {
             $Shipping = '&itemFilter.name=FreeShippingOnly&itemFilter.value'.$Shipping;
            }
            else{$Shipping='';}
            
            if($Priceform_from !='')
            {
             $urlPriceform_from = '&itemFilter(1).name=MinPrice&itemFilter(1).value='.$Priceform_from;
            }
            else{$urlPriceform_from='';}

            if($Priceform_to !='')
            {
             $urlPriceform_to = '&itemFilter(1).name=MaxPrice&itemFilter(1).value='.$Priceform_to;
            }
            else{$urlPriceform_to='';}

          if($Sort_order == 1)
            {
              $urlSort_order ="Time: ending soonest";
            }
          elseif($Sort_order == 10)
            {
              $urlSort_order = "Time: newly listed";
            }
          elseif($Sort_order ==15)
            {
              $urlSort_order = "Price + Shipping: lowest first";
            }
          elseif($Sort_order ==16)
            {
              $urlSort_order ="Price + Shipping: highest first";
            }
          elseif($Sort_order ==7)
            {
              $urlSort_order ="Distance: nearest first";
            }
          elseif($Sort_order ==12)
            {
              $urlSort_order ="Best Match";
            } 
            
         if($urlSort_order !='')
            {
             $urlSort = 'itemFilter.name=sortOrder&itemFilter.value='.$urlSort_order;
            }
            else{$urlSort_order='';}
          
          $crawler_url = "http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=APAEngin-AutoInve-PRD-5bffbb2e5-df0267de&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&keywords"."=".$urlKeywords_and."".$urlKeywords_or."".$urlExclude_keywords."&outputSelector(0)=SellerInfo&outputSelector(1)=StoreInfo&paginationInput.pageNumber=1&paginationInput.entriesPerPage=5".$urlCategoryExclude.$urlCategory.$urlConditions.$urlSellersExclude.$urlSellersInclude.$Location.$Shipping.$urlPriceform_from.$urlPriceform_to.$urlSort;       
            if($Keywords_AND !=''){
               $Keywords_AND = str_replace('|', '+',$Keywords_AND);
               $keywordAND_OR_id =3;
             }

            elseif($Keywords_OR !=''){
              
              $Keywords_AND = str_replace('|', '+' ,$Keywords_OR);
              $keywordAND_OR_id = 4;
          }
                

           $ebay_url = "https://www.ebay.com/sch/i.html?_nkw=".$Keywords_AND."&_in_kw=".$keywordAND_OR_id."&_ex_kw=".$Exclude_keywords."&LH_ItemCondition=".$Conditions."&_udlo=".$Priceform_from."&_udhi=".$urlPriceform_to."&_saslop=1&_sasl=".$Sellers_Include."&_saslop=2&_sasl=".$Sellers_Exclude."&&LH_PrefLoc=".$Location."&LH_FS=".$Shipping."&_sop=".$Sort_order."&_sacat=".$Category_Include;


           if($Conditions=="New")
            {
              $Conditions = '3';
            }
           elseif($Conditions=="used")
            {
              $Conditions = '4';
            }
           elseif($Conditions=="Not Specified")
           {
            $Conditions = '10';
           }
           if($Keywords_AND !=''){
              $Keywords_AND = str_replace('+', ',',$Keywords_AND);
                     }
            if($Keywords_OR !=''){
              $Keywords_OR = str_replace('|', ',',$Keywords_OR);
                     }
          if($Sellers_Include !=''){
              $Sellers_Include = str_replace('|', ',',$Sellers_Include);
                     }
          if($Sellers_Exclude !=''){
             $Sellers_Exclude = str_replace('|', ',',$Sellers_Exclude);
                     }
          if($Category_Include !=''){
             $Category_Include = str_replace('|', ',',$Category_Include);
                     }
        if($Category_Exclude !=''){
             $Category_Exclude = str_replace('|', ',',$Category_Exclude);
                     }
        if($Exclude_keywords !=''){
             $Exclude_keywords = str_replace('|', ',',$Exclude_keywords);
                     }

          
           if($Shipping==True){$Shipping=1;}else{$Shipping=0;}
           if($Location=='us'){$Location=1;}else{$Location=0;}
          $this->Config_IBR_model->save($ItemID,$Keywords_AND ,$Keywords_OR ,$Exclude_keywords ,$Category_Include,$Category_Exclude,$my_price,
          $Percent,$Priceform_from,$Priceform_to,$Conditions,$Shipping,$Location,$Sellers_Include,$Sellers_Exclude,$Sort_order,$ebay_url,$crawler_url);
         

      }
     }
    $this->index();
  }
   
   public function delete_filter(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->delete_filter_model($data['itemid']);
    echo json_encode($data_result);
   }   

    public function flag_update_filter(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->Config_IBR_model->flag_update_model($data['itemid'],$data['flag']);
    echo json_encode($data_result);
   }

   public function crawler_exclude_test(){
      $data = json_decode(file_get_contents("php://input"), true);
      $my_item_ids = $data["myitemid"];
      $selqry = "SELECT Comp_ItemID FROM dive_config_ibr_save_exclude WHERE MyItemID =".$my_item_ids;

      $result_query = $this->db->query($selqry);
      $selqry_res= $result_query->result();
      $request_number ='';
      (count($selqry_res) >= 1) ? $request_number = count($selqry_res)+10 : $request_number = count($selqry_res); 
      $result = [];
      (count($request_number) <= 100) ? 
      $result = array('pagenumber' => 1, 'pagedata' => $request_number) : 
      $result =  array('pagedata' => $request_number % 100, 'pagenumber' => (($request_number - $pagedata) / 100)+1);
      $data['pagedata'] = $result['pagedata'];
      $data['pagenumber'] = $result['pagenumber'] ;
      echo json_encode($data);
   }

     public function crawler_url_search(){
      $data = json_decode(file_get_contents("php://input"), true);
      $url = str_replace(' ', '', $data["url"]);
      $my_itemid = $data["myitemid"];
      $guid = $data['guid'];
      if(isset($data['handlingtime']) && $data['handlingtime'] != ''){$handlingtimeflag = 1;}else{$handlingtimeflag = 0;}
      if(isset($data['toprated']) && $data['toprated'] != ''){$topratedflag = 1;}else{$topratedflag = 0;}


      /*$selqry = "SELECT Comp_ItemID FROM dive_config_ibr_save_exclude WHERE MyItemID =".$my_itemid;

      //$exist_itemid = array();
      $result_query = $this->db->query($selqry);
      $selqry_res= $result_query->result();

      if(count($selqry_res) >= 1){
        $requested_data = count($selqry_res) + 10;
        $url = str_replace('&paginationInput.entriesPerPage=10', '&paginationInput.entriesPerPage='.$requested_data, $url);
      }*/

      $connection = curl_init();
      curl_setopt($connection, CURLOPT_URL, $url);
      curl_setopt($connection, CURLOPT_POST, 0);
      curl_setopt($connection, CURLOPT_HEADER, 0);
      curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($connection);
      $xml = simplexml_load_string($response);

       if(count($xml->searchResult->item) >= 1){
        foreach ($xml->searchResult->item as $key => $itemvalue) {
                        $itemId          = (string)$itemvalue->itemId;
                        $title           = (string)$itemvalue->title;
                        $sellerUserName  = (string)$itemvalue->sellerInfo->sellerUserName;
                        $currentPrice    = (string)$itemvalue->sellingStatus->currentPrice;
                        $ShippingPrice   = (string)$itemvalue->shippingInfo->shippingServiceCost;
                        $galleryURL      = (string)$itemvalue->galleryURL;

                        $shipType = $itemvalue->shippingInfo->shippingType;
                        $handlingTime = $itemvalue->shippingInfo->handlingTime;
                        
                        $MultiShipFlag = 0;
                        $FreeShipFlag = 0;
                        $TopRatedFlag = 0;

                        if ($shipType == 'Calculated' || $shipType == 'CalculatedDomesticFlatInternational' || $shipType == 'FlatDomesticCalculatedInternational'){
                          $MultiShipFlag = 1;
                        }

                        if ($shipType == 'Flat' || $shipType == 'Free' || $shipType == 'FreePickup'){
                          $FreeShipFlag = 1;
                        }

                        if ($itemvalue->topRatedListing == 'true'){
                          $TopRatedFlag = 1;
                        }

                        $title = preg_replace('/\\\\/', '', $title);
                        $title = str_replace("'", "\'" ,$title);
                        $title = str_replace('"', '\"' ,$title);

            $insertQry = "INSERT into config_ibr_api_search(guid, my_itemid, itemId, title, sellerUserName, CurrentPrice, ShippingPrice, galleryURL,ShipType,HandlingTime,MultiShipFlag,FreeShipFlag,TopRatedFlag)
          values('".$guid."', '".$my_itemid."', '".$itemId."', '".$title."', '".$sellerUserName."', '".$currentPrice."', '".$ShippingPrice."','".$galleryURL."','".$shipType."',".$handlingTime.",".$MultiShipFlag.",".$FreeShipFlag.",".$TopRatedFlag.")";
          $exc_insrt_qry = $this->db->query($insertQry);
        }
       }

      $non_exclude_res = $this->Config_IBR_model->api_search_non_exclude($guid,$my_itemid,$handlingtimeflag,$topratedflag);
      $exclude_res= $this->Config_IBR_model->api_search_exclude($my_itemid);
       $itemmatched = array();
       $itemunmatched = array();
       $comp_analysis_mapped_items = array();
       /*echo "<pre>";
       print_r($non_exclude_res);
       exit;*/


      foreach ($non_exclude_res as $show_value) {
        $CurrentPrice = round($show_value->price,2);
        $ShippingPrice = round($show_value->ShippingPrice,2);
        $TotalPrice = $CurrentPrice + $ShippingPrice;
        if($show_value->flag == 1){
         $comp_analysis_mapped_items[] = array('itemId'=>$show_value->compitem,
                                                'title'=>$show_value->title,
                                                'sellerUserName'=>$show_value->sellerUserName,
                                                'CurrentPrice'=> $CurrentPrice,
                                                'ShippingPrice'=> $ShippingPrice,
                                                'TotalPrice'=>$TotalPrice,
                                                'galleryURL'=> $show_value->galleryURL,
                                                'MultiShipFlag' => $show_value->MultiShipFlag,
                                                'flag'=>$show_value->flag
                                                );
        }
        else{
          $itemunmatched[] = array('itemId'=>$show_value->compitem,
                                'title'=>$show_value->title,
                                'sellerUserName'=>$show_value->sellerUserName,
                                'CurrentPrice'=> $CurrentPrice,
                                'ShippingPrice'=> $ShippingPrice,
                                'TotalPrice'=>$TotalPrice,
                                'galleryURL'=>$show_value->galleryURL,
                                'MultiShipFlag' => $show_value->MultiShipFlag,
                                'flag'=>$show_value->flag
                                );
        }
      }
      foreach ($exclude_res as $exclude_value) {
        $CurrentPrice = round($exclude_value->currentPrice,2);
        $ShippingPrice = round($exclude_value->ShippingPrice,2);
        $TotalPrice = $CurrentPrice + $ShippingPrice;
         $itemmatched[] = array('itemId'=>$exclude_value->Comp_Itemid,
                                'title'=>$exclude_value->title,
                                'CurrentPrice'=> $CurrentPrice,
                                'ShippingPrice'=> $ShippingPrice,
                                'TotalPrice'=>$TotalPrice,
                                'galleryURL'=>$exclude_value->galleryURL,
                                'MultiShipFlag' => $show_value->MultiShipFlag,
                                'excludeflag'=>$exclude_value->exclude_flag
                                );
      }
      $Api_Result['Non_Exclude_res'] = $itemunmatched;
      $Api_Result['Exclude_res'] = $itemmatched;
      $Api_Result['comp_analysis_mapped_items'] = $comp_analysis_mapped_items;

          /*$compfildterid = array();
          if(count($selqry_res)>=1){
            foreach($selqry_res as $compkey => $compvalue){
                $compfildterid[$compvalue->Comp_ItemID] = $compvalue->Comp_ItemID;
            }
          }
          $itemmatched = array();
          $itemunmatched = array();

          if(count($xml->searchResult->item) >= 1){
              foreach($xml->searchResult->item as $key => $itemvalue) {
                if(in_array((string) $itemvalue->itemId,$compfildterid)){
 
                    $itemunmatched[] = array('itemId'=>(string)$itemvalue->itemId,
                                            'title'=>(string)$itemvalue->title,
                                            'sellerUserName'=>(string)$itemvalue->sellerInfo->sellerUserName,
                                            'CurrentPrice'=>round((string)$itemvalue->sellingStatus->currentPrice,2),
                                            'ShippingPrice'=>round((string)$itemvalue->shippingInfo->shippingServiceCost,2),
                                            'galleryURL'=>(string)$itemvalue->galleryURL,
                                             'excludeflag'=>1
                                            );
                }
                else{
                   $itemmatched[] = array('itemId'=>(string)$itemvalue->itemId,
                                            'title'=>(string)$itemvalue->title,
                                            'sellerUserName'=>(string)$itemvalue->sellerInfo->sellerUserName,
                                            'CurrentPrice'=>round((string)$itemvalue->sellingStatus->currentPrice,2),
                                            'ShippingPrice'=>round((string)$itemvalue->shippingInfo->shippingServiceCost,2),
                                            'galleryURL'=>(string)$itemvalue->galleryURL,
                                             'excludeflag'=>0
                                            );
                }
              }
          }*/
   
      echo json_encode($Api_Result);
  }


  public function editpopup_api_search(){
    $data = json_decode(file_get_contents("php://input"), true);
    $itemid=$data['itemid'];
    $deleteqry = "DELETE FROM dive_ibr_item_specifics WHERE ItemID=".$itemid;
    $this->db->query($deleteqry);
    $ipn_meta_data   = $this->Config_IBR_model->ipn_meta_data_model();
     error_reporting(E_ALL);
        $compatabilityLevel = 535;    // eBay API version
        $devID = 'bf69e9-f39-4d0-93f-4f358f1d';   // these prod keys are different from sandbox keys
        $appID = 'Tamil-get-PRD-bb7edec1b-5b89e35c';
        $certID = 'PRD-b7edec1b74c5-aade-4be-ac19-5f79';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**YiJ4WQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AClYOgC5KLpwidj6x9nY+seQ**hNUDAA**AAMAAA**hxd7fhvH7dJ2dV6QjUk34cTCJxIYnV+QPCUQkVolgGtfVWZRySLH46HLJLoMqMcL3JoqFbKC2+ZI/Q+xtmduqakPHHljVHY+xr6vu7TcVsrkVHkuHrBZPZlEUzZnq9dOxcIWiyqqEkYp9az0AesWHXwGPLcSM473Nxm1jhIFUvxHBIwYrlXZqgv2ZDwVMiPwg/Np5it3TIyS27eb++sGsGO9/r0qH5f5ai8n5nVzsrVtDZzyzEFM43HlMLECoLYVsF9KurOCIS46vQVHDXp+GYg0N0o1Tt83/uqmoy55CPr3THY3nagUoB+jsb5trU6N7mnJf6sM1lX/dOclH9WPnGBZYss+KzDDHESlRvJCTVON0LadM3nJlqJMmfnPMArxdFf0RERwvmPLawqKYriWM6/kqYPGE4cQEI492Q9FEwCOcTD1AR4ZKW+sQuzfY/IQjfbzAQgvi/CGOkFUQKKu20plXhQjiIe2zVzKP1hrlXNX0hRiHz3k7W1nr66ifGwwg0ZJsuOn1ddkbWKkSDzYZQfXvDkBqu3B4Nkige4PFyKFLhTlbtXIP88opgHwviobULB4YG0wvQMGWPYahkgqpWhyh6ClC8AlkbGgQGDBoNKFCP9ljFspGK9+/YzsTfvOfysIBtVqPPtcEhf10erdNIYbozTwzlkj1TGdWM4SB9DD/EIx/O8QOKcB/WyBXKBmET3LcUKTJVquRIhWKmH846e1BQT+n0hQUDj8Tg4HSQ6V2x8hBsMr3xx5s0dMwVmS';          

    $siteID = 0;
    $verb = 'GetItem';

    ///Build the request Xml string
    $requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
    $requestXmlBody .= '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
    $requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
    $requestXmlBody .= "<IncludeItemSpecifics>true</IncludeItemSpecifics>";
    $requestXmlBody .= "<ItemID>$itemid</ItemID>";
    $requestXmlBody .= "<IncludeItemCompatibilityList>true</IncludeItemCompatibilityList>";
    $requestXmlBody .= "<IncludeTaxTable>true</IncludeTaxTable>";
    $requestXmlBody .= "<DetailLevel>ReturnAll</DetailLevel>";
    $requestXmlBody .= '</GetItemRequest>';

    $session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);
    //send the request and get response
    $responseXml = $session->sendHttpRequest($requestXmlBody);
    //Xml string is parsed and creates a DOM Document object
    $responseDoc = new DomDocument();
    $responseDoc->loadXML($responseXml);
    $xml = simplexml_load_string($responseXml);
    $filterxml = array();
       /*foreach($xml->Item->SellingStatus as $value){
      $Rslt = (array)$value;
      }
      $CurrentPrice = $Rslt['CurrentPrice'];
      $updatequery = "UPDATE `competitor_master_list` SET `price`= ".$CurrentPrice." WHERE `itemid` = ".$itemid."";
      $this->db->query($updatequery);*/
    //echo $CurrentPrice;
    //exit;
    
      foreach ($xml->Item->ItemSpecifics->NameValueList as $value) {
        if(count($value->Value) > 1){
          $objectarray = (array) $value->Value; 
          $slice = implode(' ', $objectarray);
          $filterxml[str_replace(" ",'_',$value->Name)] = $slice;
        }
        else{
          $slice = (array) $value->Value; 
          $filterxml[str_replace(" ",'_',$value->Name)] = $slice[0];
        }
      }
    $filteripn = array();
    foreach($ipn_meta_data as $value){
      $filteripn[str_replace(" ","_",$value->IPN_Name)] = $value->IPN_ID;
    }

    //result case
    $filterresult = array();
     $OPN ='';
    $mpn = '';

    /*
    print_r($xml->Item->ItemSpecifics);*/
    foreach($filteripn as $key => $value){
      if(array_key_exists($key, $filterxml)){
      array_key_exists('Manufacturer_Part_Number', $filterxml) ? $mpn = $filterxml['Manufacturer_Part_Number'] : $mpn = '';
      array_key_exists('Other_Part_Number', $filterxml) ? $OPN = $filterxml['Other_Part_Number'] : $OPN = '';

          $insertquery = "INSERT into dive_ibr_item_specifics(ItemID, IPN_ID, IPN_Value, MPN, OPN) values('".$itemid."', '".$value."', '".$filterxml[$key]."', '".$mpn."','".$OPN."')";  
        $this->db->query($insertquery);
      }
      else{
        array_key_exists('Manufacturer_Part_Number', $filterxml) ? $mpn = $filterxml['Manufacturer_Part_Number'] : $mpn = '';
        array_key_exists('Other_Part_Number', $filterxml) ? $OPN = $filterxml['Other_Part_Number'] : $OPN = '';
          $insertquery = "INSERT into dive_ibr_item_specifics(ItemID, IPN_ID, IPN_Value, MPN, OPN) values('".$itemid."', '', '', '".$mpn."','".$OPN."')";  
        $this->db->query($insertquery);  
      }
    }

    $selectqry = "SELECT MPN,GROUP_CONCAT(IPN_Value SEPARATOR ' ') as IPN,OPN FROM dive_ibr_item_specifics WHERE ItemID =".$itemid;
    $sel_exec = $this->db->query($selectqry);
    $result['sel_result'] = $sel_exec->result();

    $result['def_keyword_res']= $this->Config_IBR_model->reset_exclude_def_keyword($itemid);
      echo json_encode($result);
  }
   public function auto_raw_list_cron(){
    $truqry = "DELETE  FROM config_ibr_cron_run_status";
    $this->db->query($truqry);
    /* $apppath = APPPATH;
     $app = explode("\\",$apppath);*/
     $path = getcwd().'/PG/Searchkeywords';
     $bat_name = 'Auto_Rawlist_ahstag';


      //Generating Batch File For Instance Creation
      $my_file =$path.'/scripts/batchscripts/'.$bat_name.'.bat';
      $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
      $data = 'php '.$path.'/Searchkeywords.php Register InstanceCreation '.$bat_name;
      fwrite($handle, $data);

      //Generating VB File For Instance Creation
      $my_file = $path.'/scripts/vbscripts/'.$bat_name.'.vbs';
      $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);  
      $data = 'Set WshShell = CreateObject("WScript.Shell")
      WshShell.Run chr(34) & "'.$path.'/scripts/batchscripts/'.$bat_name.'.bat" & Chr(34), 0
      Set WshShell = Nothing';
      fwrite($handle, $data);
      $result = $this->ScheduleInstanceCreation($bat_name,$path,$my_file);
  }

  public function ScheduleInstanceCreation($bat_name,$path,$my_file){
        
        $command2 = 'SchTasks /Delete /F /TN "'.$bat_name.'"';
        $output2 = shell_exec($command2);
        
        date_default_timezone_set("Asia/Kolkata");
        $date = date('Y-m-d H:i:s');
        $currentDate = strtotime($date);
        $futureDate = $currentDate+(62);
        $formatDate = date("Y-m-d H:i:s", $futureDate);
        $finaltime = explode(' ',$formatDate);

        $command = 'SchTasks /Create /SC ONCE /TN '.$bat_name.' /TR '.$my_file.' /ST '.$finaltime[1];
        $output  = shell_exec($command);
        
        if($output == 'WARNING: The task name "'.$bat_name.'" already exists. Do you want to replace it (Y/N)? ' )
        {
           //DELETE THE EXISTSING TASK
           $command2 = 'SchTasks /Delete /F /TN "'.$bat_name.'"';
           $output2 = shell_exec($command2);      

          //CREATING NEW TASK AFTER DELETING
           $command = 'SchTasks /Create /SC ONCE /TN '.$bat_name.' /TR '.$my_file.' /ST '.$finaltime[1];
           $output  = shell_exec($command);
        } 
  }

 /* public function cronpusherstatus(){
      $ci =  &get_instance();
      $ci->load->model('Config_IBR_model');
      $config_ibr_cron_run_status   = $ci->Config_IBR_model->config_ibr_cron_run_status_tablecheck();
      $result = $config_ibr_cron_run_status;
      if(count($result) != 0){
        if($result[0]->progress == $result[0]->total){
          $options = array('encrypted' => false);
          $pusher = new Pusher\Pusher(
          '26e79100d1fea27244e6',
          'cf47f5a111cad6290a96',
          '197221',
          $options
        );
        $data['message'] = 'Pusher Received one';
        $pusher->trigger('mynewchannel', 'myevent', $data);
        }
        else{
          $this->cronpusherstatus();
        }
      }
      else{
         $this->cronpusherstatus();
      }
  }*/

  public function cron_status_checker(){
    $status_checker   = $this->Config_IBR_model->get_cron_status_checker();
    echo json_encode($status_checker);
  }
  public function api_save_exclude(){
    $data = json_decode(file_get_contents("php://input"), true);
    $username      =  $this->session->userdata('username');
    $result = $data['data'];
    $myitemid = $data['myitemid'];
        foreach ($result as $value) 
        {
          $itemId           = $value['itemId'];
          $sellerUserName   = $value['sellerUserName'];
          $titleitem        = str_replace("'","\'",$value['title']);
          $title            = $titleitem;
          $galleryURL       = $value['galleryURL'];
          $exclude_flag     = $value['exclude_flag'];
          $status = $this->Config_IBR_model->get_api_save_exclude($myitemid,$itemId,$sellerUserName,$title,$exclude_flag,$username);
        }
    echo json_encode($status);
  }

  public function creategetGUID() {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid,20,12)
                .chr(125);// "}"
            return $uuid;
        }
    }

    public function getGUID(){
      $result = $this->creategetGUID();
      $resultval = str_replace('{','',$result);
      $GUID = str_replace('}','',$resultval);
      echo $GUID;
      //exit;
    }


    public function getSKUCount(){
      $data = json_decode(file_get_contents("php://input"), true);
      $selqry = "SELECT count(SKU) as 'Count' FROM competitor_master_list WHERE sku ='".$data['sku']."' and compid = (select compid from dive_config_meta_data)";
      $result_query = $this->db->query($selqry);
      $selqry_res= $result_query->result();
      echo $selqry_res[0]->Count;
    }
     public function bulk_export(){
       $query   = $this->Config_IBR_model->bulkexport();
       echo json_encode($query);
    }

    public function selected_cron(){
       $data = json_decode(file_get_contents("php://input"), true);
       $result_update = $this->db->query("UPDATE dive_config_ibr_filter SET `selected_flag` = 0");
        
        if (empty($data['selectedcron'])){
               $status = $this->Config_IBR_model->unselectedcron();
          }
        else{
               //print_r($data['selectedcron']);
              foreach($data['selectedcron'] as $key => $value){
          $itemId           = $value['ItemId'];
          $status = $this->config_IBR_model->selectedcron($value['ItemId'],$value['compid']);
            }

    }
  }
    public function selected_all_cron(){


       $status = $this->Config_IBR_model->unselectedcron();
    }
    public function cron_for_newitm(){
       $data = json_decode(file_get_contents("php://input"), true);
       $compid = $data['compid'];
       $result_update = $this->db->query("UPDATE dive_config_ibr_filter SET `selected_flag` = 0");
       $status = $this->Config_IBR_model->cron_for_newitm($compid);
    }

    public function all_item(){
       $result_update = $this->db->query("UPDATE dive_config_ibr_filter SET `selected_flag` = 0"); 
       $result_update = $this->db->query("UPDATE dive_config_ibr_filter SET `selected_flag` = 1");
     } 

    public function limited_item(){
     $data = json_decode(file_get_contents("php://input"), true);
     $status = $this->Config_IBR_model->cron_for_limiteditm($data['Fromlimit'],$data['Tolimit']);
   } 
}
?>