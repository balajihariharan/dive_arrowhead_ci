<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API_Run_Tracker extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database();
   		$this->load->model('API_Run_Tracker_model');
      $this->load->model('RawlistModel');
      $this->load->model('CompetitorModel');
      $this->load->model('MenuModel');
      $this->load->library('email');
      $this->load->library('Screenname'); 
   		}

	public function index()
	{	
 $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
         $ref =$this->API_Run_Tracker_model->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('API_Run_Tracker_view', $mainmenu_data);
        
  
	}
 public function cron_status()
     { 
       $data = json_decode(file_get_contents("php://input"), true);
       $runfrm = $data['fromdate'];
       $runto= $data['todate'];
       $data = $this->API_Run_Tracker_model->cron_state($runfrm, $runto);
      /* echo "<pre>";
       print_r($data);
       exit;*/
       $cron_data = array();
       foreach($data as $value){
        $cron_data[] = array('CompetitorID'=>$value->CompetitorID,
                      'CronStatus'=>$value->CronStatus,
                      'Error'=>$value->Error,
                      'RecordCount'=>round($value->RecordCount,2),
                      'recordDate'=>$value->recordDate,
                      'TotalCalls'=>round($value->TotalCalls,2),
                      'StartDate'=>$value->StartDate,
                      'EndDate'=>$value->EndDate,
                      'LastPageNo' =>round($value->LastPageNo),
                      'TotalPage'=>round($value->TotalPage)
                     );

      }
       echo json_encode($cron_data);
     }
 public function comp_run_status()
 {  $data    = json_decode(file_get_contents("php://input"), true);
     $c_name = $data['c_id'];
     $runfrm1 = $data['fromdate'];
     $runto1  = $data['todate'];
     $data = $this->API_Run_Tracker_model->Run_status($c_name,$runfrm1, $runto1);

     $run_status_data = array();
     foreach($data as $value){
      $run_status_data[] = array('TotalPage'=>round($value->TotalPage,2),
                      'ListingFrom'=>$value->ListingFrom,
                      'ListingTo'=>$value->ListingTo,
                      'PageNo'=>round($value->PageNo,2),
                      'RecordDate'=>$value->RecordDate,
                      'RunID'=>round($value->RunID,2),
                      'Listings'=>round($value->Listings,2)
                     );
     }
     echo json_encode($run_status_data);
}

public function email()
{ 
  $data1 = $this->API_Run_Tracker_model->e_mail();
   $data = json_decode(file_get_contents("php://input"), true);
   $filter_date = array();
   foreach($data['emaildata']  as $datevalue){
    $filter_date[] =  $datevalue['recordDate'];
   }
   $unique_date = array_unique($filter_date);
   $emaildata = array();
   foreach($unique_date as $uniquedatevalue){
        foreach($data['emaildata'] as $value){
          if($uniquedatevalue == $value['recordDate']){
            $emaildata[$value['recordDate']][] = array($value['CompetitorID'],
                                 $value['recordDate'],
                                 $value['TotalCalls'],
                                 $value['RecordCount'],
                                 $value['Error'],
                                 $value['CronStatus'],
                                 $value['StartDate'],
                                 $value['EndDate']
                                );
          }
        }
   }

   
  $mail='';
  $mail.='<html><body>';
  $mail .= '<h1  style="color: #f40;"><strong>DIVE API Run Tracker</strong></h1>';
  
  foreach($emaildata as $key=>$resultvalue){
     $key1 = $resultvalue;
     $mail.='<p>Run Date :'.$key1[0][1].'</p>' ;
    $mail .= '<table rules="all" border="1" cellpadding="10">';
   
    $mail.=' <thead>
                    <tr>
                        <th><strong>Competitor</strong></th>
                        <th><strong>Total Calls</strong></th>
                        <th><strong>Record Count</strong></th>
                        <th><strong>Cron Status</strong></th>
                        <th><strong>Start Date</strong></th>
                        <th><strong>End Date</strong></th>
                        
                    </tr>
              </thead>' ; 
                   

    foreach ($key1 as $value) {
                              
       if ($value[1] == $key)
       {
         
          $mail.='<tr "style"="border:1px solid #000;">
            <td style=”padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px;”>'.$value[0].'</td>
            <td style=”padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px;”>'.$value[2].'</td>
            <td style=”padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px;”>'.$value[3].'</td>
            <td style=”padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px;”>'.$value[5].'</td>
            <td style=”padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px;”>'.$value[6].'</td>
            <td style=”padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px;”>'.$value[7].'</td>
            </tr>';           
       }
    
  }
  $mail.='</table><br><br><br>';

  }
  
 $mail.='</body></html>';


 foreach($data1 as $key=>$email){
     $this->email->from($email['formmail'], 'DIVE'); 
     $this->email->to($email['too']);
     $this->email->cc($email['cc']);
     $this->email->bcc($email['bcc']);
     $this->email->subject('Dive CBR Report'); 
     $this->email->set_mailtype("html");
     $this->email->message($mail);
     $this->email->send(); 
}
}



}

?>
       