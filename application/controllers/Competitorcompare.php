<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'application/libraries/EBaySession.php';
require_once 'application/libraries/keys.php';
require_once(APPPATH.'libraries/FPDF Plugin/fpdf.php'); //Export Plugin
//require_once 'application/libraries/pusher/vendor/autoload.php';//pusher

class Competitorcompare extends CI_Controller {

	  public function __construct(){
  		parent::__construct();
   		$this->load->database();
   		 $this->load->helper( 'Metrics_helper' ); 
   		$this->load->model('AutoIBRModel');
		$this->load->library('keysnew');
		$this->load->library('Screenname');
		$this->load->model('CompetitorcompareModel');
   		$this->load->model('MenuModel');
   		ini_set('memory_limit', '256M');
   		ini_set('max_execution_time', 30000);
	}

	public function index()
	{	

		$getmenu = $this->screenname->getscreen();
	    $router =& load_class('Router', 'core');
	    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
	    $title = '';
		    foreach($getmenu['url'] as $key => $value){
		      if($value == $indexroute){
		        $title = $key;
		      }
		    }
					
		$mainmenu_data['icononly']  = $getmenu['icononly'];
	    $mainmenu_data['url']       = $getmenu['url'];
	    $mainmenu_data['screen']     = $title;
	    $mainmenu_data['menu_data'] = $getmenu['menu_data'];

	    $ref =$this->AutoIBRModel->get_title();
	    $mainmenu_data['comptitornameandid'] = $this->CompetitorcompareModel->get_competitor();
        $mainmenu_data['title']= $ref;
        $mainmenu_data['ShowMasked']= $ref[0]->ShowMasked;
		$this->load->view('Compcompare_view',$mainmenu_data);
	}

  public function Competitorcompare_result_sell(){
    $data = json_decode(file_get_contents("php://input"), true);
	//$competitor_compare_result= [];
    $competitor_compare_result = $this->CompetitorcompareModel->get_competitor_compare_result_sell($data['competitor'], $data['radio']);
	$competitor_compare = array();
	foreach ($competitor_compare_result as $value) {
		$competitor_compare[] =array('CompItemID'=>$value->CompItemID,
									 'CompSKU'=>$value->CompSKU,
									 'Mypartno'=>$value->Mypartno,
									 'MyItemID'=>$value->MyItemID,
									 'MySKU'=>$value->MySKU,
									 'MyTitle'=>$value->MyTitle,
									 'MyPrice'=>round($value->MyPrice,2),
									 'MyQtysold'=>round($value->MyQtysold,2),
									 'CompPrice'=>round($value->CompPrice,2),			
									 'CompSold'=>round($value->CompSold,2)		
									 );			
	}
	/*echo "<pre>";
	print_r($competitor_compare);
	exit;*/
	echo json_encode($competitor_compare);
	
	}

public function Competitorcompare_result_dontsell(){
    $data = json_decode(file_get_contents("php://input"), true);
	//$competitor_compare_result= [];
    $competitor_compare_result = $this->CompetitorcompareModel->get_competitor_compare_result_dontsell($data['competitor'], $data['radio']);
	$competitor_compare = array();
	foreach ($competitor_compare_result as $value) {
		$competitor_compare[] =array('itemid'=>$value->itemid,
									 'sku'=>$value->sku,
									 'category'=>$value->category,
									 'title'=>$value->title,
									 'price'=>round($value->price,2),
									 'Qtysold'=>round($value->Qtysold,2),
									 'QuantitySold'=>round($value->LastweekQtySold,2)					
									 );			
	}
	/*echo "<pre>";
	print_r($competitor_compare);
	exit;*/
	echo json_encode($competitor_compare);
	
	}
	
  }