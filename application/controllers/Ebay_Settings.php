<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/libraries/EBaySession.php';

class Ebay_Settings extends CI_Controller
{
	public function __construct()
	{
    	parent::__construct();
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->model('Ebaysettings_Model');
   		 $this->load->library('Screenname');
  	}
	public function index()
	{
	 $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $ref                         = $this->Ebaysettings_Model->get_title();
    $mainmenu_data['title']      = $ref;
		$this->load->view('ebay_settings_view',$mainmenu_data);
		}
      

public function savenewkey()
	{
		$data = json_decode(file_get_contents("php://input"), true);

		
		       $key              = $data['key'];
		       $AppID            = $data['AppID'];
		       $devID            = $data['DevID'];
		       $CertID           = $data['CertID'];
		       $ServerURL        = $data['ServerURL'];
		       $Compatability    = $data['Compatability'];
	           $SiteID           = $data['SiteID'];
		       $Environment      = $data['Environment'];
               $Authentication   = $data['Authentication'];
		       $username         =  $this->session->userdata('username');
		       $roleid           =   $this->session->userdata('roleid');

		$out       = $this->Ebaysettings_Model->savenewpart($key,$AppID,$devID,$CertID,$ServerURL,$Compatability,$SiteID,$Environment,$Authentication,$username, $roleid);
		//return $out;
	}

	public function getdata(){
        $data = json_decode(file_get_contents("php://input"), true);
        $ret= $this->Ebaysettings_Model->get_data();
        echo json_encode($ret);
   }

   public function edit(){
        $data = json_decode(file_get_contents("php://input"), true);
        $key = $data['key'];
        $action =$data['action'];
        $ret = $this->Ebaysettings_Model->edit($key,$action);
        echo json_encode($ret);
   }
   public function delete(){
        $data = json_decode(file_get_contents("php://input"), true);
        $key = $data['key'];
        $action =$data['action'];
        $ret = $this->Ebaysettings_Model->delete($key,$action);
        //echo json_encode($ret);
   }

   public function checkbox_update(){
		$competitor_listing_update 	= $this->Ebaysettings_Model->checkbox_update_model($_POST['keyid'],$_POST['checkid']);
	}
	public function pagecountsave(){
		$data = json_decode(file_get_contents("php://input"), true);
		$pagecount=$data['pagecount'];
		$ret = $this->Ebaysettings_Model->pagecountsavemodel($pagecount);
	}

	public function check(){
		$data = json_decode(file_get_contents("php://input"), true);
		$userToken = $data['key'][0]['AuthKey'];
		$devID = $data['key'][0]['DevID'];
		$appID = $data['key'][0]['AppID']; 
		$certID = $data['key'][0]['CertID'];
		$serverUrl = $data['key'][0]['ServerURL'];
		$compatabilityLevel = $data['key'][0]['Compatability'];
		$siteID = $data['key'][0]['SiteID'];

		//the call being made:
		$verb = 'GetApiAccessRules';

		///Build the request Xml string
		$requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
		$requestXmlBody .= '<GetApiAccessRulesRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
		$requestXmlBody .= '</GetApiAccessRulesRequest>';
	   
		//Create a new eBay session with all details pulled in from included keys.php
		$session = new eBaySession($userToken,$devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);
		
		//send the request and get response
		$responseXml = $session->sendHttpRequest($requestXmlBody);
		if(stristr($responseXml, 'HTTP 404') || $responseXml == '')
			die('<P>Error sending request');
		
		//Xml string is parsed and creates a DOM Document object
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($responseXml);
		$resxml = simplexml_load_string($responseXml);
		$result = $resxml->ApiAccessRule[0];
		echo json_encode($result);
		
		}
     
}
