<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterlist extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database();
        $this->load->helper( 'Metrics_helper' );
   		$this->load->model('MasterlistModel');
   		$this->load->model('RawlistModel');
   		$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->library('Screenname');
   		ini_set('memory_limit', '512M');
	}

	  public function index(){	
			$getmenu     = $this->screenname->getscreen();
		    $router      =& load_class('Router', 'core');
		    $indexroute  =  $router->fetch_class().'/'.$router->fetch_method();
		    $title       = '';
    foreach($getmenu['url'] as $key => $value){
                if($value == $indexroute){
                $title = $key;
            }
          }
    $mainmenu_data['icononly']           = $getmenu['icononly'];
    $mainmenu_data['url']                = $getmenu['url'];
    $mainmenu_data['screen']             = $title;
    $mainmenu_data['menu_data']          = $getmenu['menu_data'];
	$metrics_result                      = $this->MasterlistModel->get_metrics_model();
	$mainmenu_data['comptitornameandid'] = $this->MasterlistModel->get_competitor_model();
    $temp                                = $metrics_result[0]['TotalSales'];
    $metrics_result[0]['TotalSales']     = test_method($temp);
    $mainmenu_data['metrics']            = $metrics_result;
    $ref                                 =$this->MasterlistModel->get_title();
    $mainmenu_data['title']              = $ref;
    $this->load->view('masterlistview',$mainmenu_data);
	    }
		
	public function competitor_category(){
		$data = json_decode(file_get_contents("php://input"), true);
		$result['partno_result'] 	= $this->MasterlistModel->search_partno_model();
		$competitor_category_result = [];
		if ($data['competitor']){
			$result['competitor_category_result']	= $this->MasterlistModel->get_competitor_category($data['competitor']);
		}
		echo json_encode($result);
        }


	public function masterlist_details_control(){
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
			$competitor_category_result 	= $this->MasterlistModel->Masterlist_details_model($data['competitor'],$data['category']);	
			$competitor_category_result1 	= $this->MasterlistModel->get_item_detail_model($data['competitor'],$data['category']);
			$competitor_category = array();
			foreach($competitor_category_result as $value){
				$competitor_category[] = array('ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'Category'=>$value->Category,
										  'CategoryID'=>$value->CategoryID,
										  'price'=>round($value->price,2),
										  'Qtysold'=>round($value->Qtysold,2),
										  'lastweekQtysold'=>round($value->lastweek_Qtysold,2),
										  'TotalSales'=>round($value->TotalSales,2),
										  'ItemURL'=>$value->ItemURL,
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,
										  'compid'=>round($value->compid),										  
										  'ListingDate'=>$value->ListingDate
										 );
			}
			$data['masterdetail'] 			= $competitor_category;
			$temp= $competitor_category_result1[0]['TotalSales'];
	        $competitor_category_result1[0]['TotalSales'] = test_method($temp);
			$data['itemdetail'] 			= $competitor_category_result1;
			

            }
		echo json_encode($data);

	

	}

	public function search_partno(){
		$data = json_decode(file_get_contents("php://input"), true);
		echo json_encode($partno_result);
	}

		public function competitor_mypartnumber_update(){

		$data = json_decode(file_get_contents("php://input"), true);
		
		 $username=  $this->session->userdata('username');
	    $roleid=  $this->session->userdata('roleid');
		$competitor_result 	= $this->MasterlistModel->competitor_mypartnumber_update_model($data['items'][0]['value'],$data['partnum'],$data['items'][2]['value'],$username,$roleid);
		if($competitor_result != 'Failure'){
			echo json_encode($competitor_result);
		}
		else{
			echo 'failure';
		}
	}

    	public function remove_from_masterlist(){
		$data = json_decode(file_get_contents("php://input"),true);
		if($data['itemid']){
			$competitor_master_result		= $this->MasterlistModel->remove_from_masterlist_model($data['itemid'],$data['competitor']);
		}
			echo json_encode($competitor_master_result);
		/*if($competitor_master_result !== 'failure'){
			echo $competitor_master_result;
		}
		else{
			echo "failure";
		}*/
		
		}

		public function exportexcelcomplete(){
			$result=$this->MasterlistModel->getcompletelist();
			$competitor_category = array();
			foreach($result as $value){
				$competitor_category[] = array('ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'Category'=>$value->Category,
										  'price'=>round($value->price,2),
										  'Qtysold'=>round($value->Qtysold,2),
										  'lastweekQtysold'=>round($value->lastweek_Qtysold,2),
										  'TotalSales'=>round($value->TotalSales,2),
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,									  
										  'ListingDate'=>$value->ListingDate
										 );
			}
			echo json_encode($competitor_category);
		}

}
