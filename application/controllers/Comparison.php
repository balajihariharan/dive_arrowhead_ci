<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comparison extends CI_Controller {

  public function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->helper( 'Metrics_helper' );
      $this->load->library('Screenname'); 
        $this->load->model('CompetitorModel');
      $this->load->model('MenuModel');
      ini_set('memory_limit', '256M');
  }


    public function usersubmenu()
  {
    $sendval = $_POST['menuid'];
    $getsubmenu = $this->MenuModel->get_submenulist($sendval);
    $getsubmenu_data['submenu_data']    = $getsubmenu;
  }

  public function exclusionlist(){
    $competitor_exclusion           = $this->CompetitorModel->get_exclusion();
    $competitor_data['exlusion_data']     = $competitor_exclusion;
      
  

     
    $this->load->view('exclusion_list',$competitor_data);
    

  }

  public function exclusion(){
    if(count($this->uri->segment(2)) == 1){
      $fetch_exclusion      = $this->CompetitorModel->fetch_exclusion($this->uri->segment(3));
      $competitor_exclusion     = $this->CompetitorModel->exclusion_general_lookup();
      $data['general_dropdown']   = $competitor_exclusion;
      $data['fetch_exclusion']    = $fetch_exclusion;
      $this->load->view('exclusion',$data);

    }
    else{
      $competitor_exclusion     = $this->CompetitorModel->exclusion_general_lookup();
      $data['general_dropdown']   = $competitor_exclusion;
      $this->load->view('exclusion',$data);
    }
  } 

  public function exclusion_competitorname_search(){
    $searchtext         = $_POST['searchtext'];
    $competitor_exclusion     = $this->CompetitorModel->exclusion_competitor_name($searchtext);
    echo json_encode(array('result'=>$competitor_exclusion));
  }
  
  public function exclusion_search_partnumber(){
    $searchtext         = $_POST['searchtext'];
    $competitor_exclusion     = $this->CompetitorModel->search_part_number($searchtext);
    echo json_encode(array('result'=>$competitor_exclusion));
  }
  
  public function exclusion_search_categoryname(){
    $searchtext         = $_POST['searchtext'];
    $competitor_exclusion     = $this->CompetitorModel->search_category_name($searchtext);
    echo json_encode(array('result'=>$competitor_exclusion));
  }
  
  public function exclusion_search_brandname(){
    $searchtext         = $_POST['searchtext'];
    $competitor_exclusion     = $this->CompetitorModel->search_brand_name($searchtext);
    echo json_encode(array('result'=>$competitor_exclusion));
  }

  public function exclusion_submission(){
    if(count($_POST) >= 1){
      if(isset($_POST['exclustion_id'])){$exclusion_id = $_POST['exclustion_id'];}else{$exclusion_id = NULL;}
      if(isset($_POST['exclusion_name'])){$exclusion_name = $_POST['exclusion_name'];}else{$exclusion_name = "NULL";}
      if(isset($_POST['general_select_combo'])){$general_select_combo = $_POST['general_select_combo'];}else{$general_select_combo = NULL;}
      if(isset($_POST['location_name'])){ $location_name = 1;}else{$location_name = 0;}
      if(isset($_POST['rated_name'])){  $rated_name = 1;}else{$rated_name = 0;}
      if(isset($_POST['shipping_name'])){ $shipping_name = 1;}else{$shipping_name = 0;}
      if(isset($_POST['active_only'])){ $active_only = 1;}else{$active_only = 0;}
      if(isset($_POST['parts_selected_value'])){$parts_selected_value = "'".implode(',',$_POST['parts_selected_value'])."'";}else{$parts_selected_value = NULL;}
      if(isset($_POST['general_selected_value'])){$general_selected_value = "'" . implode ( "', '", $_POST['general_selected_value'] ) . "'";}else{$general_selected_value = NULL;}
      if(isset($_POST['category_selected_value'])){$category_selected_value = "'".implode(',',$_POST['category_selected_value'])."'";}else{$category_selected_value = NULL;}
      if(isset($_POST['brand_selected_value'])){$brand_selected_value = "'".implode(',',$_POST['brand_selected_value'])."'";}else{$brand_selected_value = NULL;}      
      if(isset($_POST['title_selected_value'])){$title_selected_value = "'".implode(',',$_POST['title_selected_value'])."'";}else{$title_selected_value = NULL;}
      if(isset($_POST['competitor_selected_value'])){$competitor_selected_value = "'".implode(',',$_POST['competitor_selected_value'])."'";}else{$competitor_selected_value = NULL;}
    }
    
    $exclusion_save = $this->CompetitorModel->save_exclusion($exclusion_id,$exclusion_name,$general_selected_value,$location_name,$active_only,
             $rated_name,$shipping_name,$parts_selected_value,$category_selected_value,$brand_selected_value,$title_selected_value,
         $competitor_selected_value);
    if($exclusion_save){
      echo 'Exculsion Saved Successfully';
    }
    else{
      echo 'Exculsion Saving Problem Please Try again';
    }
  }

  public function exclusionlist_update(){
    $competitor_listing_update        = $this->CompetitorModel->$this->CompetitorModel->exclusionlist_update_model($_GET['dataid'],$_GET['checkid']);
    if($competitor_listing_update){
      return "success";
    }
    else{
      return "failure";
    }
  }

  public function index()
  { 
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
      $mainmenu_data['icononly']       = $getmenu['icononly'];
      $mainmenu_data['url']            = $getmenu['url'];
      $mainmenu_data['screen']         = $title;
      $mainmenu_data['menu_data']      = $getmenu['menu_data'];
    $metrics_result            = $this->CompetitorModel->metrics_model();
    $competitor_result             = $this->CompetitorModel->get_competitor();
    $category_combo            = $this->CompetitorModel->get_tab1_categorycombo();
    $mainmenu_data['data']         = $competitor_result;
    $temp                            = $metrics_result[0]['TotalSales'];
      $metrics_result[0]['TotalSales'] = test_method($temp);
    $mainmenu_data['metrics']    = $metrics_result;
    $mainmenu_data['category_combo'] = $category_combo;
        $ref                             =$this->CompetitorModel->get_title();
        $mainmenu_data['title']          = $ref;
  
  
    $this->load->view('Comparison_view',$mainmenu_data);
    }
        
  

  public function index_get(){
    $data = json_decode(file_get_contents("php://input"), true);
    if (isset($data['mypartno'])) {$mypartnoval = $data['mypartno'];} else {$mypartnoval = '';}
    $competitor_listing_result        = $this->CompetitorModel->get_competitor_listings($data['categoryids'],$mypartnoval,1,1);
    $competitor_listing = array();
    $competitor_listing_sublist = array();
    foreach($competitor_listing_result['first'] as $d=> $mainvalue){
      $competitor_listing[$d] = array('MyID'=>$mainvalue->MyID,
                      'MySKU'=>$mainvalue->MySKU,
                      'MyTitle'=>$mainvalue->MyTitle,
                      'MyCategory'=>$mainvalue->MyCategory,
                      'MyPrice'=>round($mainvalue->MyPrice,2),
                      'MyQuantitySold'=>(float)$mainvalue->MyQuantitySold,
                      'mypartno'=>($mainvalue->mypartno == NULL) ? 'NA' : $mainvalue->mypartno,
                      'lastweek_Qtysold'=>(float)$mainvalue->lastweek_Qtysold,
                      'CompLowPrice'=>(float)$mainvalue->CompLowPrice,
                      'CompHighPrice'=>(float)$mainvalue->CompHighPrice,
                      'CompCount'=>(float)$mainvalue->CompCount
                     );
        foreach($competitor_listing_result['second'] as $key => $subvalue){
          if($mainvalue->MyID == $key){
            foreach($subvalue as $val){
                $competitor_listing_sublist[$mainvalue->MyID][] = array(
                                'MyID1'=>$val->MyID,
                                'CompetitorItemID1'=>$val->CompetitorItemID,
                                'CompetitorName1'=>$val->CompetitorName,
                                'CompetitorSKU1'=>$val->CompetitorSKU,
                                'CompetitorTitle1'=>$val->CompetitorTitle,
                                'CompetitorCategory1'=>$val->CompetitorCategory,
                                'CompetitorPrice1'=>(float)$val->CompetitorPrice,
                                'CompetitorQuantitySold1'=>($val->CompetitorQuantitySold !== NULL) ? (float) $val->CompetitorQuantitySold : 0,
                                'mypartno1'=>$val->mypartno,
                                'lastweek_Qtysold1'=>(float)$val->lastweek_Qtysold
                                );
            }
          }
        }
    }
    $result = array('first'=>$competitor_listing,'second'=>$competitor_listing_sublist);
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($result));
  }

  public function competitor_category(){
    $data = json_decode(file_get_contents("php://input"), true);
    $competitor_category_result = [];
    if ($data['competitor']){
      $competitor_category_result   = $this->CompetitorModel->get_competitor_category($data['competitor']);
    }
    echo json_encode($competitor_category_result);
  }
  public function category_listing(){
    $competitor_result = $this->CompetitorModel->get_competitor_listings();
  }

  public function competitor_category_listing_showitem(){
    $data = json_decode(file_get_contents("php://input"), true);
    $competitor_result = [];
    if ($data){

      $competitor_result  = $this->CompetitorModel->get_competitor_category_showitem($data['tabs'],$data['competitor'],$data['category']);
    }
    echo json_encode($competitor_result);
  }

  public function competitor_mypartnumber_update(){
    $data = json_decode(file_get_contents("php://input"), true);
    $username=  $this->session->userdata('username');
      $roleid=  $this->session->userdata('roleid');
    $competitor_result  = $this->CompetitorModel->competitor_mypartnumber_update_model($data[0]['value'],$data[2]['value'],$data[1]['value'],$data[3]['value'],$username,$roleid);
    if($competitor_result){
      echo 'success';
    }
    else{
      echo 'failure';
    }
  }

  public function iamlowest(){
    $data = json_decode(file_get_contents("php://input"), true);

    if (isset($data['mypartno'])) {$mypartnoval = $data['mypartno'];} else {$mypartnoval = '';}
    
    $competitor_listing_result        = $this->CompetitorModel->get_competitor_listings($data['categoryids'],$mypartnoval, $data['iamlowest'],$data['iamnotlowest']);
    $competitor_listing = array();
    $competitor_listing_sublist = array();
    foreach($competitor_listing_result['first'] as $d=> $mainvalue){
      $competitor_listing[$d] = array('MyID'=>$mainvalue->MyID,
                      'MySKU'=>$mainvalue->MySKU,
                      'MyTitle'=>$mainvalue->MyTitle,
                      'MyCategory'=>$mainvalue->MyCategory,
                      'MyPrice'=>round($mainvalue->MyPrice,2),
                      'MyQuantitySold'=>round($mainvalue->MyQuantitySold),
                      'mypartno'=>($mainvalue->mypartno == NULL) ? 'NA' : $mainvalue->mypartno,
                      'lastweek_Qtysold'=>round($mainvalue->lastweek_Qtysold),
                      'CompLowPrice'=>round($mainvalue->CompLowPrice,2),
                      'CompHighPrice'=>round($mainvalue->CompHighPrice,2),
                      'CompCount'=>round($mainvalue->CompCount)
                     );
        foreach($competitor_listing_result['second'] as $key => $subvalue){
          if($mainvalue->MyID == $key){
            foreach($subvalue as $val){
              if($val->CompetitorQuantitySold == NULL){
                $qunatirysoldlast = 0; 
              }
              else{
                $qunatirysoldlast = (int)$val->CompetitorQuantitySold;
              }
              $competitor_listing_sublist[$mainvalue->MyID][] = array(
                                'MyID'=>$val->MyID,
                                'CompetitorItemID'=>$val->CompetitorItemID,
                                'CompetitorName'=>$val->CompetitorName,
                                'CompetitorSKU'=>$val->CompetitorSKU,
                                'CompetitorTitle'=>$val->CompetitorTitle,
                                'CompetitorCategory'=>$val->CompetitorCategory,
                                'CompetitorPrice'=>round($val->CompetitorPrice,2),
                                'CompetitorQuantitySold'=>$qunatirysoldlast,
                                'mypartno'=>round($val->mypartno),
                                'lastweek_Qtysold'=>round($val->lastweek_Qtysold)
                                );
            }
          }
        }
    }
    $result = array('first'=>$competitor_listing,'second'=>$competitor_listing_sublist);
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($result));
  }

  public function salestrends(){
    $data = json_decode(file_get_contents("php://input"), true);
    $sales_trend_result         = $this->CompetitorModel->dive_comp_analysis_item_sales_trend_model(
                          $data['dataname'],
                          $data['datacompid'],
                          $data['datasku'],
                          $data['datalist']
                          );
    if($sales_trend_result){
      echo json_encode($sales_trend_result);
    }
    else{
      echo 'failure';
    }
  }
  public function search_partno(){
    //$data = json_decode(file_get_contents("php://input"), true);
    /*echo "<pre>";
    print_r($data);
    exit;*/
    $partno_result  = $this->CompetitorModel->search_partno_model();
    /*echo "<pre>";
    print_r($partno_result);
    exit;*/
    echo json_encode($partno_result);
  }

}

        