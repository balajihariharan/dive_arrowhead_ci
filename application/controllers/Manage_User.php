<?php
defined ('BASEPATH') OR exit('No direct script access allowed');
class Manage_User extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
        $this->load->model('Manage_User_Model'); 
        $this->load->model('CompetitorModel');
        $this->load->model('MenuModel'); 
        $this->load->library('Screenname');   
    }
      public function index(){   
        $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    	$nresult            = $this->Manage_User_Model->index();
        $result             = $this->Manage_User_Model->display();
        $mainmenu_data['id']         = $result;
        $mainmenu_data['role']       = $nresult;
         $ref =$this->Manage_User_Model->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('Manage_User_View',$mainmenu_data);
        }
        
    

    /* public function sample(){
        //$loginid = $_POST['loginid'];
        //$result             = $this->Manage_User_Model->display();
        /*$loginuserid = array();
        if(count($result) >= 1){
            foreach($result as $value){
                $loginuserid[] = $value['loginid'];
            }
        }
      

     }
     public function user_exists(){
        $loginid = $_POST['loginid'];
        $result             = $this->Manage_User_Model->display();
        $loginuserid = '';
        if(count($result) >= 1){
            foreach($result as $value){
                $loginuserid[] = $value['loginid'];
            }
        }
        $successresult = '';
        foreach($loginuserid as $getval){
            if($getval == $_POST['loginid']){
               $successresult = "success"; 
            }
        }
        echo json_encode(array('result'=>$successresult));
     }*/

     public function insert(){ 
        $userid = $_POST['uid'];
        $username = $_POST['uname'];
        $loginid = $_POST['lid'];
        $emailid = $_POST['eid'];
        $password = md5($_POST['pass']);
        $roleid = $_POST['rname'];
        $username1=  $this->session->userdata('username');
        $this->Manage_User_Model->insert_mod($userid,$username,$loginid,$emailid,$password,$roleid,$username1);
     }

    public function edit(){
        if(isset($_POST['id']))
        {
            $roleid = $_POST['id'];
            $result=$this->Manage_User_Model->edit_select($roleid);
            foreach($result as $item) 
            {
                echo json_encode(array('result'=>$item));
            }
        }       
    }
    public function adduser(){
        $result=$this->Manage_User_Model->get_userid();
        foreach($result as $item) 
        {
                echo json_encode(array('result'=>$item));
        }
     }
    public function delete(){
        if(isset($_POST['id']))
        {
            $userid= $_POST['id'];
            $this->Manage_User_Model->delete($userid);
        }   
    }
}      
?>
        
        
        
