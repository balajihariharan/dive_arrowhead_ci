<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errorhandling extends CI_Controller {

   public function __construct(){
      parent::__construct();
      $this->load->helper('log4php');
    }

     public function ErrorLogToDB(){
      $data = json_decode(file_get_contents("php://input"), true);
      $this->db->query("CALL dive_save_error('".$data['StatusCode']."','".$data['StatusText']."','".base64_encode($data['ErrorHtml'])."',
        '".$data['screenname']."')");
      return true;
    }
    public function ErrorLogToDBAjax(){
      $this->db->query("CALL dive_save_error('".$_POST['StatusCode']."','".$_POST['StatusText']."','".base64_encode($_POST['ErrorHtml'])."',
        '".$_POST['screenname']."')");
      return true;
    }
    
  public function failure_msg(){
    if(isset($_POST['xhrstatus'])){
      $status = ($_POST['xhrstatus']);
    }
    else{
       $status = json_decode(file_get_contents("php://input"), true);
    }
   
    if($status == 500){
      $errormsg = "Internal Server Error";

    }
    elseif($status == 404){
       $errormsg = "File Not Found";
        log_error($errormsg);
    }

    elseif($status == 001){
       $errormsg = "Rule Hierarchy Should Be Unique. Please Change the Duplicate value";
    }
    elseif($status == 21){
       $errormsg = "You Can't Unselect Both Checkbox";

    }
    elseif($status == 002){
       $errormsg = "Email ID is not Available";

    }
    else{
        $errormsg = "Something Went Wrong";
         log_error($errormsg);
    }
    
    $imgpath =  base_url()."assets/" .'img/error_icon.png';

    $msg = '<div id="msg_id" class="err_modal" >
                       <div class="alert_box" class="popup" class="modal-content" >
                        <div class="fleld-alert failure">

                        <div class="head_icon">
                            <img src="'.$imgpath.'" alt="Logout" title="Logout"  />
                        </div>

                          <div class="alert alert-warning">
                           <strong>'.$errormsg.'</strong>
                           <p> </p>
                          </div>

                          <div class="alert_ok_btn">
                            <button class="btn_ok_sty destroy">ok</button>
                          </div>

                        </div>
                    </div>
                    </div>';
         echo $msg;
         
  }

  public function success_msg(){
  if(isset($_POST['msgstatus'])){
      $status = ($_POST['msgstatus']);
    }
    else{
       $status = json_decode(file_get_contents("php://input"), true);
    }
    
    if($status == 1){
      $successmsg = "Item Successfully Added To Masterlist";
    }
    elseif($status == 2){
       $successmsg = "Item Successfully Removed";
    }
    elseif($status == 3){
       $successmsg = "Successfully Updated";
    }
     elseif($status == 4){
       $successmsg = "Deleted Record Successfully";
    }
     elseif($status == 5){
       $successmsg = "Email Send Successfully";
    }
     elseif($status == 6){
       $successmsg = "Active Flag Update Successfully";
    }
    elseif($status == 7){
       $successmsg = "No Data to Display";
    }
     elseif($status == 8){
       $successmsg = "No Records Found";
    }
    elseif($status == 9){
       $successmsg = "Password Changed Successfully";
    }
    elseif($status == 10){
       $successmsg = "Applied Filters Cleared Successfully";
    }
    else{
        $successmsg = "Success!";
    }


    $imgpath =  base_url()."assets/" .'img/success_icon.png';

    $msg = '<div id="success" class="err_modal">
           <div class="alert_box" class="popup" class="modal-content" >
                   <div class="fleld-alert success"> 

                         <div class="head_icon_green">
                            <img src="'.$imgpath.'"  alt="Logout" title="Logout" class="destroy" />
                        </div>

                          <div class="alert alert-warning">
                           <strong>'.$successmsg.'</strong>
                           <p></p>
                          </div>

                          <div class="alert_ok_btn">
                            <button class="btn_ok_sty_green destroy">ok</button>
                          </div>

                         </div>
                    </div>
                 </div>';
         echo $msg; 
  }

  public function success_msg_config_cbr(){
    $data = json_decode(file_get_contents("php://input"), true);
    $status = substr($data['msgstatus'], 1, -1);
    $compname = $data['compname'];
    $successmsg = 'Competitor Added To CBR List';
    $successmsg0 = 'Competitor Name : ';
    $successmsg1 = 'eBay Registration Date/Time : ';
    $imgpath =  base_url()."assets/" .'img/success_icon.png';

    $msg = '<div id="success" class="err_modal">
           <div class="alert_box" class="popup" class="modal-content" style="width: 414px;line-height: 20px;">
                   <div class="fleld-alert success"> 

                         <div class="head_icon_green">
                            <img src="'.$imgpath.'"  alt="Logout" title="Logout" class="destroy" />
                        </div>

                          <div class="alert alert-warning" style="padding: 8% 12px;">
                           <strong style="color: green;font-size: 27px;padding: 0px 0px 25px 0px;">'.$successmsg.'</strong>
                           <strong style="color:darkblue">'.$successmsg0.'<br></strong><span style="display: initial;color:brown";font-size:23px>'.$compname.
                           '</span><br><br><strong style="color:darkblue;">'.$successmsg1.'<br></strong><span style="display: initial;color:brown;font-size:23px">'.$status.
                           '</span><p></p>
                          </div>

                          <div class="alert_ok_btn">
                            <button class="btn_ok_sty_green destroy">Ok</button>
                          </div>

                         </div>
                    </div>
                 </div>';
         echo $msg; 
  }

  public function failure_msg_pricesmart(){
    $errormsg = "You Can't Unselect Both Checkbox";
    
    $imgpath =  base_url()."assets/" .'img/error_icon.png';

    $msg = '<div id="msg_id" class="err_modal" >
                       <div class="alert_box" class="popup" class="modal-content" >
                        <div class="fleld-alert failure">

                        <div class="head_icon">
                            <img src="'.$imgpath.'" alt="Logout" title="Logout"  />
                        </div>

                          <div class="alert alert-warning">
                           <strong>'.$errormsg.'</strong>
                           <p> </p>
                          </div>

                          <div class="alert_ok_btn">
                            <button class="btn_ok_sty destroy2">Ok</button>
                          </div>

                        </div>
                    </div>
                    </div>';
         echo $msg;
         
  }


  public function failure_msg_configcbr(){
    if(isset($_POST['msgstatus'])){
      $status = ($_POST['msgstatus']);
    }
    else{
       $status = json_decode(file_get_contents("php://input"), true);
    }
    if($status == 1){
      $errormsg = "Competitor Does Not Exist in eBay";
    }
    elseif($status == 003){
       $errormsg = "Invalid PartNo";

    }
    else{
       $errormsg = "Competitor Already Exists";
    }
    
    $imgpath =  base_url()."assets/" .'img/error_icon.png';

    $msg = '<div id="msg_id" class="err_modal" >
                       <div class="alert_box" class="popup" class="modal-content" >
                        <div class="fleld-alert failure">

                        <div class="head_icon">
                            <img src="'.$imgpath.'" alt="Logout" title="Logout"  />
                        </div>

                          <div class="alert alert-warning">
                           <strong>'.$errormsg.'</strong>
                           <p> </p>
                          </div>

                          <div class="alert_ok_btn">
                            <button class="btn_ok_sty destroy2">Ok</button>
                          </div>

                        </div>
                    </div>
                    </div>';
         echo $msg;
         
  }

   public function failure_msg_partno_map(){
   $status = json_decode(file_get_contents("php://input"), true);

    if($status == 003){
       $errormsg = "Invalid PartNo";
    }
    
    $imgpath =  base_url()."assets/" .'img/error_icon.png';

    $msg = '<div id="msg_id" class="err_modal" >
                       <div class="alert_box" class="popup" class="modal-content" >
                        <div class="fleld-alert failure">

                        <div class="head_icon">
                            <img src="'.$imgpath.'" alt="Logout" title="Logout"  />
                        </div>

                          <div class="alert alert-warning">
                           <strong>'.$errormsg.'</strong>
                           <p> </p>
                          </div>

                          <div class="alert_ok_btn">
                            <button class="btn_ok_sty destroy3">Ok</button>
                          </div>

                        </div>
                    </div>
                    </div>';
         echo $msg;
         
  }

  public function ebay_sync_success_msg(){
      $data = json_decode(file_get_contents("php://input"), true);
      $total = $data['data']['totalcount'];
      $success =$data['data']['successcount'];
      $failure =$data['data']['failurecount'];
      $ebayfee =$data['data']['ebaysyncfee'];
      $successmsg ="eBay Sync Successfully completed";     

      $imgpath =  base_url()."assets/" .'img/success_icon.png';

      $msg = '<div id="success" class="err_modal">
             <div class="alert_box" class="popup" class="modal-content" >
                     <div class="fleld-alert success"> 

                           <div class="head_icon_green">
                              <img src="'.$imgpath.'"  alt="Logout" title="Logout" class="destroy" />
                          </div>

                            <div class="alert alert-warning">
                             <strong>'.$successmsg.'</strong><br>
                             <p>Total Items Selected : '.$total.'</p><br>
                             <p>Success : '.$success.'</p><br>
                             <p>Failure : '.$failure.'</p><br>
                             <p>eBay Fees Incurred : '.$ebayfee.'</p>
                            </div>

                            <div class="alert_ok_btn">
                              <button class="btn_ok_sty_green destroy2">ok</button>
                            </div>

                           </div>
                      </div>
                   </div>';
           echo $msg; 

  }


  /*public function ebay_sync_confirm_msg(){
      $imgpath =  base_url()."assets/" .'img/success_icon.png';

       $confirmmsg = 'We are about to Update the Price for the Selected Items on your eBay Store.<br><br>Click "Yes", to Confirm.  Click "No" to Cancel.';
      
      $msg = '<div id="success" class="err_modal">
             <div class="alert_box" class="popup" class="modal-content" >
                     <div class="fleld-alert success"> 

                           <div class="head_icon_green" style="background: #1376AF none repeat scroll ">
                              <img src="'.$imgpath.'"  alt="Logout" title="Logout" class="destroy" />
                          </div>

                            <div class="alert alert-warning">

                             <strong style="font-size: 19px;padding:0px 0px">'.$confirmmsg.'</strong><br>
                             <p></p>
                            </div>

                            <div class="alert_ok_btn">
                              <button class="btn_ok_sty_green clck" ng-click="proceedtoebay()" style="margin-left: -57px;">Yes</button>
                            </div>
                            <div class="alert_ok_btn">
                              <button class="btn_ok_sty nonclck" style="margin: -16px 0 0px 26px;background-color:#b30000;left: 46%;">No</button>
                            </div>

                           </div>
                      </div>
                   </div>';
           echo $msg; 

  }*/
  
}

  