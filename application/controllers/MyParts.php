<?php
require_once(APPPATH.'libraries/FPDF Plugin/fpdf.php'); //Export Plugin

class MyParts extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
    	$this->load->model('MyParts_Model');
    	$this->load->model('RawlistModel');
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		 $this->load->library('Screenname');
  	}
	public function index(){
	 $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
		$category_combo 		= $this->RawlistModel->get_rawlist_competitorcombo();
		$mainmenu_data['compidandname'] 	= $category_combo;
	
		 $ref =$this->MyParts_Model->get_title();
        $mainmenu_data['title']= $ref;
        
        /*echo "<pre>";
        print_r($mainmenu_data);
        exit;
		*/$this->load->view('MyParts_View',$mainmenu_data);
		//$this->load->view('MyParts_View')
	      }
	public function disp(){
		$data = json_decode(file_get_contents("php://input"), true);
		$result=$this->MyParts_Model->index($data['CompID']);
		$result1 = array();
		foreach($result as $value){
				$result1[] = array('partno'=>$value->partno,
									  'sku'=>$value->sku,
									  'cost_price'=>(float)(round($value->cost_price,2)),
									  'freight'=>(float)(round($value->freight,2)),
									  'ebay_fees'=>(float)(round($value->ebay_fees,2)),
									  'margin' => (float)(round($value->margin,2)),
									  'MSprice' => (float)(round($value->MSprice,2)),
									  'def_margin' => (float)(round($value->def_margin,2)),
									  'my_price_ebay'=>(float)(round($value->my_price_ebay,2))
								);
			}
		echo json_encode($result1);
	}
	public function save(){

		$data = json_decode(file_get_contents("php://input"), true);
		$result = $data['New_Content'];
		$username=  $this->session->userdata('username');
		$roleid =   $this->session->userdata('roleid');
		foreach ($result as $value) 
		{
			foreach ($value as $val)
			{
				
						$partno = $val['partno'];
						$sku = $val['sku'];
						$cost_price = round($val['cost_price'],2);
						$freight = $val['freight'];
						$ebay_fees = $val['ebay_fees'];
						$margin = $val['margin'];
						$my_price_ebay = $val['my_price_ebay'];
						$MSprice = $val['MSprice'];
						$this->MyParts_Model->save($partno,$sku,$cost_price,$freight,$ebay_fees,$margin,$my_price_ebay,$MSprice,$username,$roleid);
			
			}
		}
		//$this->index();
	}

	public function imp_save(){
		$data = json_decode(file_get_contents("php://input"), true);
		$result = $data['New_Content'];
		$username=  $this->session->userdata('username');
		$roleid =   $this->session->userdata('roleid');

		foreach ($result as $value) 
		{
			if ($value['flag'] == 'I' && $value['partno'] != '' && $value['partno'] != 'My Part#' && $value['partno'] != 'MyPart#'
				&& $value['sku'] != '' && $value['sku'] != 'SKU')
			{
					$partno = str_replace("'","",$value['partno']);
					$sku = str_replace("'","",$value['sku']);
					$cost_price = $value['cost_price'];
					$freight = $value['freight'];
					$ebay_fees = $value['ebay_fees'];
					$my_price_ebay = 0;
					$margin = $val['margin'];
					$MSprice = $val['MSprice'];
					$this->MyParts_Model->save($partno,$sku,$cost_price,$freight,$ebay_fees,$margin,$my_price_ebay,$MSprice,$username,$roleid);
			}
		}
		$this->index();
	}
	
	public function export()
  	{
		$result=$this->MyParts_Model->index();
		$heading=['My Part#','SKU','Cost Price (In $)','Freight (In $)','Fees (In $)','My Price(eBay) (In $)'];
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',18);
		$pdf->Cell(70);
		$pdf->Cell(40,10,'My Parts Informations');
		$pdf->Ln(10);		
		$pdf->SetFont('Arial','B',9);
		foreach($heading as $column_heading)
			{
				$pdf->Cell(32,12,$column_heading,1);
			}
		foreach($result as $row) {
			$pdf->SetFont('Arial','',9);	
			$pdf->Ln();
			foreach($row as $column)
				$pdf->Cell(32,12,$column,1);
		}
		$pdf->Output();
	}
	public function savepartno()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		
		$freight=0;
		$Fees= 0;
		$CostPrice=0;
		$Margin =0;
		//Array ( [mypart] => df [SKU2] => vZSD [CostPrice2] => sc [Freight2] => scfss [Fees2] => cfsc )
		if(isset($data['mypart']))       {$mypart    = $data['mypart'];}
		if(isset($data['SKU2'] ))        { $sku      = $data['SKU2'];}
		if(isset( $data['CostPrice2']))  {$CostPrice = $data['CostPrice2'];}
	    if(isset($data['Freight2']))     {$freight   = $data['Freight2'];}
		if(isset($data['Fees2']))        {$Fees      = $data['Fees2'];}
		if(isset($data['Margin2']))        {$Margin      = $data['Margin2'];}
		$username=  $this->session->userdata('username');
		$roleid =   $this->session->userdata('roleid');

		$out       = $this->MyParts_Model->savenewpart($mypart,$sku,$CostPrice,$freight,$Fees,$Margin,$username,$roleid);
		return $out;
	}

	public function margincountsave(){
		$data = json_decode(file_get_contents("php://input"), true);
		$margincount=$data['margincount'];
		$ret = $this->MyParts_Model->margincountsavemodel($margincount);


	}
	public function getstorename(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->MyParts_Model->getstorename();
    echo json_encode($data_result);
  }
}
?>