<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/FPDF Plugin/fpdf.php'); //Export Plugin
require_once 'application/libraries/EBaySession.php';
require_once 'application/libraries/pusher/vendor/autoload.php';
 


class Pricesmart extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database(); 
   		$this->load->model('PricesmartModel');
   		$this->load->model('RawlistModel');
   		$this->load->library('excel');
   		$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->library('Screenname');
   		ini_set('memory_limit', '256M');
   		ini_set('max_execution_time', 0);
	}

	public function pricesmartindex()
	{	
	$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $ref =$this->PricesmartModel->get_title();
    $mainmenu_data['title']= $ref;
    $rundate = $this->PricesmartModel->lastrundate();
    $mainmenu_data['rundate'] = $rundate;
    $this->load->view('pricesmartview',$mainmenu_data);
	}

	        
		
		
	
		public function disp(){
			$flag = 'a';
			$category_result=$this->PricesmartModel->get_item_detail($flag);
			$competitor_category = array();
			foreach($category_result as $value){
					$competitor_category[] = array('MyPartNo'=>$value->mypartno,
											  'ItemId'=>$value->itemid,
											  'SKU'=>$value->sku,
											  'Title'=>$value->title,
											  'Category'=>$value->category,
											  'MyPrice'=>round($value->my_price,2),
											  'Comp_High_Price'=>round($value->comp_high_price,2),
											  'Comp_Low_Price'=>round($value->comp_low_price,2),
											  'Comp_Low_ItemId_Name'=>$value->lowcompitemid,
											  'Comp_High_ItemId_Name'=>$value->highcompitemid,
											  'Comp_Low_ItemId'=>rtrim(substr($value->lowcompitemid,strpos($value->lowcompitemid,'(')+1),')'),
											  'Comp_High_ItemId'=>rtrim(substr($value->highcompitemid,strpos($value->highcompitemid,'(')+1),')'),
											  'Cost_Price'=>round($value->cost_price,2),
											  'Freight'=>round($value->freight,2),
											  'Fees'=>round($value->fees,2),
											  'Recomend_Price'=>round($value->recomended_price,2),
											  'RuleId'=>$value->rule_id,
											  'RuleName'=>$value->rule_name,
											  'TotalPrice'=>$value->total_price,
											  'alert_flag'=>$value->alert_flag,
											  'Floorprice1'=>$value->Floorprice1,
											  'Floorprice2'=>$value->Floorprice2,
											  'margin'=>$value->margin,
											  'priceflag' => $value->priceflag
											 );
			}
			

			echo json_encode($competitor_category);
			
		}

	public function pricerule(){	
	if($this->session->userdata('loginid')){
			 $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
		$result = $this->PricesmartModel->get_price_list();
		$mainmenu_data['rulelist'] = $result;
		$result1 = $this->PricesmartModel->get_price_list_global();
		$res = array();
		foreach ($result1 as $key=>$value) {
			if ($value->low_flag == 1)
			{
				if ($value->our_price_operator == 'g')
				{
					$res[] = array('rule_name'=>"Whenever I'm the Lowest",
									'rule_name_short'=>"GLOBAL-L",
						  			'synopsis' =>"<span style='font-weight:bold;'>Cost Price Threshold :</span> ".$value->cost_price_threshold."%<br><span style='font-weight:bold;'>Set Our Price :</span>".$value->our_price_value.'% > CLP');
				}
				if ($value->our_price_operator == 'l')
				{
					$res[] = array('rule_name'=>"Whenever I'm the Lowest",
									'rule_name_short'=>"GLOBAL-L",
						  			'synopsis' =>"<span style='font-weight:bold;'>Cost Price Threshold :</span> ".$value->cost_price_threshold."%<br><span style='font-weight:bold;'>Set Our Price :</span>".$value->our_price_value.'% < CLP');
				}
				if ($value->our_price_operator == 'e')
				{
					$res[] = array('rule_name'=>"Whenever I'm the Lowest",
									'rule_name_short'=>"GLOBAL-L",
						  			'synopsis' =>"<span style='font-weight:bold;'>Cost Price Threshold :</span> ".$value->cost_price_threshold."%<br><span style='font-weight:bold;'>Set Our Price :</span> = CLP");
				}
			}
			
			else
			{
				if ($value->our_price_operator == 'g')
				{
					$res[] = array('rule_name'=>"Whenever I'm Not the Lowest",
								'rule_name_short'=>"GLOBAL-NL",
						  		'synopsis' =>"<span style='font-weight:bold;'>Cost Price Threshold :</span> ".$value->cost_price_threshold."%<br><span style='font-weight:bold;'>Set Our Price :</span> ".$value->our_price_value.'% > CLP');
				}
				if ($value->our_price_operator == 'l')
				{
					$res[] = array('rule_name'=>"Whenever I'm Not the Lowest",
								'rule_name_short'=>"GLOBAL-NL",
						  		'synopsis' =>"<span style='font-weight:bold;'>Cost Price Threshold :</span> ".$value->cost_price_threshold."%<br><span style='font-weight:bold;'>Set Our Price :</span> ".$value->our_price_value.'% > CLP');
				}
				if ($value->our_price_operator == 'e')
				{
					$res[] = array('rule_name'=>"Whenever I'm Not the Lowest",
								'rule_name_short'=>"GLOBAL-NL",
						  		'synopsis' =>"<span style='font-weight:bold;'>Cost Price Threshold :</span> ".$value->cost_price_threshold."%<br><span style='font-weight:bold;'>Set Our Price :</span> = CLP");
				}
			}
		}
		$mainmenu_data['rulelistglobal'] = $res;
        $ref =$this->PricesmartModel->get_title();
        $mainmenu_data['title']= $ref;
		$this->load->view('priceruleview',$mainmenu_data);
		}
        else{
            redirect('/Login/index','refresh');    
        }
	}
	
	
	public function onloadruleview(){
		$result = $this->PricesmartModel->get_price_list();
		echo json_encode($result);	
	}

	public function priceruleedit(){
		$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    $flag=0;
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
        $flag = 1;
      }
    }
    if ($flag == 0)
    {
    	foreach($getmenu['zeroflag_url'] as $key => $value){
      	if($value == $indexroute){
        	$title = $getmenu['zeroflag_name'][$key];
      		}
    	}	
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
     $ref                         =$this->PricesmartModel->get_title();
    $mainmenu_data['title']      = $ref;

		$param = '';
		$mainmenu_data['competitorcombo'] 		= $this->PricesmartModel->get_comp_cat_name($param);

		if($this->uri->segment(3) == NULL){
			$param = '';
			$mainmenu_data['selectedcatid'] 		= $this->PricesmartModel->get_comp_cat_name($param);
			$mainmenu_data['selectedcatdata'] 		= $this->PricesmartModel->get_globaldata($param);
			$mainmenu_data['globaldata'] 			= $this->PricesmartModel->get_globaldata($param);	
		}
		else{
			$param = '';
			$mainmenu_data['selectedcatid'] 		= $this->PricesmartModel->get_comp_cat_name($this->uri->segment(3));
			$mainmenu_data['selectedcatdata'] 		= $this->PricesmartModel->get_globaldata($this->uri->segment(3));
			$mainmenu_data['globaldata'] 		= $this->PricesmartModel->get_globaldata($param);
		}
		
		$this->load->view('priceruleeditview',$mainmenu_data);
		}
        

	public function editsave(){
		$username      =  $this->session->userdata('username');
		$roleid        =  $this->session->userdata('roleid');
		if(isset($_POST['competitorcategory']) || !isset($_POST['competitorcategory'])){
			$compselected = '';
			$categselected = '';
			$first = true;
			if(isset($_POST['competitorcategory'])){
				foreach($_POST['competitorcategory'] as $value){
					$explode = explode(',',$value);

					if ($first) {
						$categselected = '(' . $explode[0] . ')';
						$compselected = '(' . $explode[1] . ')';
						$first = false;
					} else  {
						$categselected .= ', ' . '(' . $explode[0] . ')';
						$compselected .= ', ' . '(' . $explode[1] . ')';
					}	
				}
			}
			else{
				$errormsg = 'Select atleast one Inclusion Competitor/Category';
				$this->session->set_flashdata('errormsg',$errormsg);
				redirect('Pricesmart/priceruleedit/'.$smartruleid);
			}

			$smartruleid 				= $_POST['smart_ruleid'];
			$smart_rulename 			= $_POST['smart_rulename'];
			$lowflag 		  			= $_POST['smart_lowest'];
			$costprice_threshold 		= $_POST['smart_threshold'];
			$ourpricevalue 	  			= $_POST['smart_inclusionpricing_percentage'];
			$costprice_threshold_g		= $_POST['global_threshold'];
			$globalpricevalue_g 	  	= $_POST['global_globalpricing_percentage'];
			
			
			/*Specific Rule ID Info*/
			if(isset($_POST['smart_inclusionpricing']) && $_POST['smart_inclusionpricing'] == 'e'){
				$ourpriceoperator 			= 'e';	
			}
			else{
				$ourpriceoperator 			= $_POST['smart_inclusion_ltgt'];
			}

			
			/*Global Rule ID Info*/
			if(isset($_POST['global_globalpricing']) && $_POST['global_globalpricing'] == 'e'){
				$ourpriceoperator_g 			= 'e';	
			}
			else{
				$ourpriceoperator_g 			= $_POST['global_global_ltgt'];
			}

			
			$result = $this->PricesmartModel->save_pricesmart($smartruleid, $smart_rulename, $lowflag, $costprice_threshold, $ourpricevalue,$ourpriceoperator, $costprice_threshold_g, $globalpricevalue_g, $ourpriceoperator_g, $compselected, $categselected,$username,$roleid);

			redirect('Pricesmart/pricerule');
		}
	}

	public function combo_load(){
		$comp_cat_combo = $this->PricesmartModel->get_comp_cat_name();
		echo json_encode($comp_cat_combo);
	}


	public function imp_save(){
		$data = json_decode(file_get_contents("php://input"), true);
		$result = $data['New_Content'];
		$username      =  $this->session->userdata('username');
		$roleid        =  $this->session->userdata('roleid');
		foreach ($result as $value) 
		{
			if ($value['ItemId'] != '')
			{
					$itemid= str_replace("'","",$value['ItemId']);
					$rprice = $value['Recomend_Price'];
					$this->PricesmartModel->save($itemid,$rprice,$username,$roleid);			
			}
		}
		return true;
	}


	public function save(){
		$data = json_decode(file_get_contents("php://input"), true);
		$username      =  $this->session->userdata('username');
		$roleid        =  $this->session->userdata('roleid');
		$result = $data['New_Content'];
		foreach ($result as $value) 
		{
			$itemid= $value['ItemId'];
			$rprice = $value['Recomend_Price'];
			$this->PricesmartModel->save($itemid,$rprice,$username,$roleid);
		}
		return true;
	}

	/*public function export()
  	{
		$result=$this->PricesmartModel->get_item_detail();
		$heading=['My Part#','Item Id','SKU','Title','My Price (in $)','Comp. High Price(in $)','Comp Low Price(in $)','Cost Price (in $)','Freight (in $)','Fees (in $)','Recommended Price (in $)'];
		$pdf = new FPDF();
		$pdf->AddPage('L');
		$pdf->SetFont('Arial','B',18);
		$pdf->Cell(70);
		$pdf->Cell(40,10,'Price Smart Informations');
		$pdf->Ln(10);		
		$pdf->SetFont('Arial','B',9);
		foreach($heading as $column_heading)
			{
				$pdf->Cell(25,10,$column_heading,1);
			}
		foreach($result as $row) {
			$pdf->SetFont('Arial','',9);	
			$pdf->Ln();
			foreach($row as $key=>$column)
				if ($key!='alert_flag')
				{
					if ($key == 'Title')
					{
						$temp=WordWrap($column,25);
						$pdf->Cell(200,5,$temp,1);
					}
					else{
						$pdf->Cell(25,10,$column,1);
					}	
				}
		}
		$pdf->Output();
	}*/

   public function export_excel(){
  		$category_result=$this->PricesmartModel->get_item_detail($_POST['flag']);
		$competitor_category = array();
		foreach($category_result as $value){
			if ($value->rule_id == 'GLOBAL-L' || $value->rule_id == 'GLOBAL-NL')
			{
				$competitor_category[] = array('MyPartNo'=>$value->mypartno,
											  'ItemId'=>$value->itemid,
											  'SKU'=>$value->sku,
											  'Title'=>$value->title,
											  'Category'=>$value->category,
											  'MyPrice'=>round($value->my_price,2),
											  'Comp_High_Price'=>round($value->comp_high_price,2),
											  'Comp_Low_Price'=>round($value->comp_low_price,2),
											  'Cost_Price'=>round($value->cost_price,2),
											  'Freight'=>round($value->freight,2),
											  'Fees'=>round($value->fees,2),
											  'TotalPrice'=>round($value->total_price,2),
											  'Recomend_Price'=>round($value->recomended_price,2),
											  'RuleId'=>$value->rule_id,
											  'RuleName'=>$value->rule_id,
											  'alert_flag'=>$value->alert_flag 
											 );
			}
			else
			{
				$competitor_category[] = array('MyPartNo'=>$value->mypartno,
										  'ItemId'=>$value->itemid,
										  'SKU'=>$value->sku,
										  'Title'=>$value->title,
										  'Category'=>$value->category,
										  'MyPrice'=>round($value->my_price,2),
										  'Comp_High_Price'=>round($value->comp_high_price,2),
										  'Comp_Low_Price'=>round($value->comp_low_price,2),
										  'Cost_Price'=>round($value->cost_price,2),
										  'Freight'=>round($value->freight,2),
										  'Fees'=>round($value->fees,2),
										  'TotalPrice'=>round($value->total_price,2),
										  'Recomend_Price'=>round($value->recomended_price,2),
										  'RuleId'=>$value->rule_id,
										  'RuleName'=>$value->rule_name,
										  'alert_flag'=>$value->alert_flag 
										 );
			}
		}
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Pricesmart');
		$this->excel->getActiveSheet()->setCellValue('A1', 'MyPart#');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Item ID');
		$this->excel->getActiveSheet()->setCellValue('C1', 'SKU');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Title');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Category');
		$this->excel->getActiveSheet()->setCellValue('F1', 'My Price (eBay) (in $)');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Competitor Highest Price (in $)');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Competitor Lowest Price (in $)');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Cost Price (in $)');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Freight (in $)');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Fees (in $)');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Total Price (in $)');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Recommended Price (in $)');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Rule Name');
		$styleArray1 = array(
	    	'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => 'FF0000'),
	        'size'  => 13,
	        'name'  => 'Verdana'
    	));
    	$styleArray2 = array(
	    	'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => '006600'),
	        'size'  => 13,
	        'name'  => 'Verdana'
    	));
    	$styleArray3 = array(
      'font'  => array(
         'bold'  => true,
         'color' => array('rgb' => '808080'),
         'size'  => 13,
         'name'  => 'Verdana'
     ));
     $styleArray4 = array(
      'font'  => array(
         'bold'  => true,
         'color' => array('rgb' => 'FF6600'),
         'size'  => 13,
         'name'  => 'Verdana'
     ));

    	$headerArray = array(
	    	'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => 'FFFFFF'),
	        'size'  => 13,
	        'name'  => 'Verdana'
    	));

		$this->excel->getActiveSheet()->getStyle("A1:N1")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => '#FFC0CB')));

    	for($col = ord('A'); $col <= ord('N'); $col++){ 
        	$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
        	$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    	}
    	for($col = 'A'; $col <= 'N'; $col++) {
		    $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}

		$row = 2;
		    if(count($competitor_category)>=1){
		    	foreach($competitor_category as $key=>$excelvalue){
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $excelvalue['MyPartNo']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "'".$excelvalue['ItemId']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $excelvalue['SKU']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $excelvalue['Title']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $excelvalue['Category']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $excelvalue['MyPrice']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $excelvalue['Comp_High_Price']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $excelvalue['Comp_Low_Price']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $excelvalue['Cost_Price']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $excelvalue['Freight']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $excelvalue['Fees']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $excelvalue['TotalPrice']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row,$excelvalue['Recomend_Price']);
		    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row,$excelvalue['RuleName']);
		    		if($excelvalue['alert_flag'] == 'r')
		    		{
                 		$this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray1);
            		}
            		if($excelvalue['alert_flag'] == 'g')
            		{
                 		$this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray2);
            		}
            		if($excelvalue['alert_flag'] == 'y')
              {
                   $this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray3);
              }
              if($excelvalue['alert_flag'] == 'o')
              {
                   $this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray4);
              }
		           	if($key%2 !== 0)
		            {
		              $this->excel->getActiveSheet()->getStyle("A".$row.':N'.$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'FFC0CB')));
		            }
		    		$row++;
		    	}
		    }
		    $filename = 'DIVE_Pricesmart_Complete.xls';
		    header('Content-Type: application/vnd.ms-excel'); 
		    header('Content-Disposition: attachment;filename="'.$filename.'"');
		    header('Cache-Control: max-age=0'); 
		    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		    $objWriter->save('php://output');
		    redirect('Pricesmart/pricesmartindex');
		}

		public function pricerulesave(){
			$username      =  $this->session->userdata('username');
		    $roleid        =  $this->session->userdata('roleid');
			$ruleid        = $_POST['ruleid'];
			$RuleHierarchy = $_POST['RuleHierarchy'];
			$fullcount     = count($RuleHierarchy);
			$uniquecount   = count(array_unique($RuleHierarchy));
			if ($fullcount == $uniquecount) {
				$activeflag = $_POST['activeflag'];
				$this->PricesmartModel->price_rule_save($ruleid,$RuleHierarchy,$activeflag,$username,$roleid);	
			}
			else
			{
				echo 'error';
				/*$this->session->set_userdata('validationmsg', 'RuleHierarchy Should Be Unique Please Change the duplicate value');
				*///redirect ('Pricesmart/pricesmartindex');

			}
		}

		public function price_rule_delete(){
			$result = $this->PricesmartModel->price_ruleid_delete($_POST['myid']);
		}

	public function PriceRecomProcess(){
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    $flag=0;
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
        $flag = 1;
      }
    }
    if ($flag == 0)
    {
    	foreach($getmenu['zeroflag_url'] as $key => $value){
      	if($value == $indexroute){
        	$title = $getmenu['zeroflag_name'][$key];
      		}
    	}	
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
	
    $ref                         =$this->PricesmartModel->get_title();
    $mainmenu_data['title']      = $ref;
	$this->load->view('RunRecommendPriceView', $mainmenu_data);
		}

		public function price_rule_check_upd(){
			$username      =  $this->session->userdata('username');
		    $roleid        =  $this->session->userdata('roleid');
			$result = $this->PricesmartModel->price_checked_updata($_POST['ruleid'],$_POST['flag'],$username,$roleid);
		}

		public function export_excel_selected(){
			$itemidlist = array();
			$itemidlist = explode(', ',$_POST['temp']);
  			foreach ($itemidlist as $value) {
  				$category_result=$this->PricesmartModel->exportchecked($value);		
  				foreach($category_result as $value){
  					if ($value->rule_id == 'GLOBAL-L' || $value->rule_id == 'GLOBAL-NL')
					{
						$competitor_category[] = array('MyPartNo'=>$value->mypartno,
													  'ItemId'=>$value->itemid,
													  'SKU'=>$value->sku,
													  'Title'=>$value->title,
													  'Category'=>$value->category,
													  'MyPrice'=>round($value->my_price,2),
													  'Comp_High_Price'=>round($value->comp_high_price,2),
													  'Comp_Low_Price'=>round($value->comp_low_price,2),
													  'Cost_Price'=>round($value->cost_price,2),
													  'Freight'=>round($value->freight,2),
													  'Fees'=>round($value->fees,2),
													  'TotalPrice'=>round($value->total_price,2),
													  'Recomend_Price'=>round($value->recomended_price,2),
													  'RuleId'=>$value->rule_id,
													  'RuleName'=>$value->rule_id,
													  'alert_flag'=>$value->alert_flag 
													 );
					}
					else
					{
						$competitor_category[] = array('MyPartNo'=>$value->mypartno,
												  'ItemId'=>$value->itemid,
												  'SKU'=>$value->sku,
												  'Title'=>$value->title,
												  'Category'=>$value->category,
												  'MyPrice'=>round($value->my_price,2),
												  'Comp_High_Price'=>round($value->comp_high_price,2),
												  'Comp_Low_Price'=>round($value->comp_low_price,2),
												  'Cost_Price'=>round($value->cost_price,2),
												  'Freight'=>round($value->freight,2),
												  'Fees'=>round($value->fees,2),
												  'TotalPrice'=>round($value->total_price,2),
												  'Recomend_Price'=>round($value->recomended_price,2),
												  'RuleId'=>$value->rule_id,
												  'RuleName'=>$value->rule_name,
												  'alert_flag'=>$value->alert_flag 
												 );
					}
				}
  			}
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Pricesmart');
			$this->excel->getActiveSheet()->setCellValue('A1', 'MyPart#');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Item ID');
			$this->excel->getActiveSheet()->setCellValue('C1', 'SKU');
			$this->excel->getActiveSheet()->setCellValue('D1', 'Title');
			$this->excel->getActiveSheet()->setCellValue('E1', 'Category');
			$this->excel->getActiveSheet()->setCellValue('F1', 'My Price (eBay) (in $)');
			$this->excel->getActiveSheet()->setCellValue('G1', 'Competitor Highest Price (in $)');
			$this->excel->getActiveSheet()->setCellValue('H1', 'Competitor Lowest Price (in $)');
			$this->excel->getActiveSheet()->setCellValue('I1', 'Cost Price (in $)');
			$this->excel->getActiveSheet()->setCellValue('J1', 'Freight (in $)');
			$this->excel->getActiveSheet()->setCellValue('K1', 'Fees (in $)');
			$this->excel->getActiveSheet()->setCellValue('L1', 'Total Price (in $)');
			$this->excel->getActiveSheet()->setCellValue('M1', 'Recommended Price (in $)');
			$this->excel->getActiveSheet()->setCellValue('N1', 'Rule Name');
			$styleArray1 = array(
		    	'font'  => array(
		        'bold'  => true,
		        'color' => array('rgb' => 'FF0000'),
		        'size'  => 13,
		        'name'  => 'Verdana'
	    	));
	    	$styleArray2 = array(
		    	'font'  => array(
		        'bold'  => true,
		        'color' => array('rgb' => '006600'),
		        'size'  => 13,
		        'name'  => 'Verdana'
	    	));
	    	$styleArray3 = array(
	      'font'  => array(
	         'bold'  => true,
	         'color' => array('rgb' => '808080'),
	         'size'  => 13,
	         'name'  => 'Verdana'
	     ));
     $styleArray4 = array(
      'font'  => array(
         'bold'  => true,
         'color' => array('rgb' => 'FF6600'),
         'size'  => 13,
         'name'  => 'Verdana'
     ));

	    	$headerArray = array(
		    	'font'  => array(
		        'bold'  => true,
		        'color' => array('rgb' => 'FFFFFF'),
		        'size'  => 13,
		        'name'  => 'Verdana'
	    	));

			$this->excel->getActiveSheet()->getStyle("A1:N1")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => '#FFC0CB')));

	    	for($col = ord('A'); $col <= ord('N'); $col++){ 
	        	$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
	        	$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	    	}
	    	for($col = 'A'; $col <= 'N'; $col++) {
			    $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}

			$row = 2;
			    if(count($competitor_category)>=1){
			    	foreach($competitor_category as $key=>$excelvalue){
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $excelvalue['MyPartNo']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "'".$excelvalue['ItemId']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $excelvalue['SKU']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $excelvalue['Title']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $excelvalue['Category']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $excelvalue['MyPrice']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $excelvalue['Comp_High_Price']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $excelvalue['Comp_Low_Price']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $excelvalue['Cost_Price']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $excelvalue['Freight']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $excelvalue['Fees']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $excelvalue['TotalPrice']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row,$excelvalue['Recomend_Price']);
			    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row,$excelvalue['RuleName']);
			    		if($excelvalue['alert_flag'] == 'r')
			    		{
	                 		$this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray1);
	            		}
	            		if($excelvalue['alert_flag'] == 'g')
	            		{
	                 		$this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray2);
	            		}
	            		if($excelvalue['alert_flag'] == 'y')
              {
                   $this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray3);
              }
              if($excelvalue['alert_flag'] == 'o')
              {
                   $this->excel->getActiveSheet()->getStyle("M".$row)->applyFromArray($styleArray4);
              }
			           	if($key%2 !== 0)
			            {
			              $this->excel->getActiveSheet()->getStyle("A".$row.':N'.$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'FFC0CB')));
			            }
			    		$row++;
			    	}
			    }
			    $filename = 'DIVE_Pricesmart_Selected.xls';
			    header('Content-Type: application/vnd.ms-excel'); 
			    header('Content-Disposition: attachment;filename="'.$filename.'"');
			    header('Cache-Control: max-age=0'); 
			    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			    $objWriter->save('php://output');
			    redirect('Pricesmart/pricesmartindex');
  		}

  		public function SalesTrend(){
		$data = json_decode(file_get_contents("php://input"),true);
		$competitor_category_result = [];
		$competitor_category_result = $this->PricesmartModel->get_salestrends($data['itemid']);
		$competitor_category_result1 = $this->PricesmartModel->get_week();
		$competitor_category = array();
		foreach($competitor_category_result as $value){
			$competitor_category[] = array('itemid' => $value->itemid,
												'sku' => $value->sku,
												'title' => $value->title,
												'mypartno' => $value->mypartno,
												'compname' => $value->compname,
												'category' => $value->category,
												'subcat' => $value->subcat,
												'pricew1' => $value->pricew1,
												'pricew2' => $value->pricew2,
												'pricew3' => $value->pricew3,
												'pricew4' => $value->pricew4,
												'pricew5' => $value->pricew5,
												'salesw1' => $value->salesw1,
												'salesw2' => $value->salesw2,
												'salesw3' => $value->salesw3,
												'salesw4' => $value->salesw4,
												'salesw5' => $value->salesw5,
												'pricevw1' => round($value->pricevw1, 2),
												'pricevw2' => round($value->pricevw2, 2),
												'pricevw3' => round($value->pricevw3, 2),
												'pricevw4' => round($value->pricevw4, 2),
												'pricevw5' => round($value->pricevw5, 2),
												'qtysoldvw1' => $value->qtysoldvw1,
												'qtysoldvw2' => $value->qtysoldvw2,
												'qtysoldvw3' => $value->qtysoldvw3,
												'qtysoldvw4' => $value->qtysoldvw4,
												'qtysoldvw5' => $value->qtysoldvw5,
												'price' => (float)round($value->price, 2),
												'qtysold' => (float)$value->qtysold,
												'qtysoldinc' => (float)$value->qtysoldinc,
												'totalsales' => round($value->totalsales, 2),
												'itemURL' => $value->itemURL,
												'listingdate' => $value->listingdate,
												'compid' => $value->compid
										 );
		}
        $data['dashboard_drill_data'] = $competitor_category;
        $data['week'] = $competitor_category_result1;
		echo json_encode($data);
	}

	public function sync_with_ebay(){
		$ebaysetting_result = $this->PricesmartModel->get_ebaysettings_detail();
		$data = json_decode(file_get_contents("php://input"),true);
		/*echo "<pre>";
		print_r($ebaysetting_result[0]['AuthKey']);
		exit;*/
		$createdby =$this->session->userdata('username');
		if(empty($ebaysetting_result)){
			$AuthKey='AgAAAA**AQAAAA**aAAAAA**YiJ4WQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj';
			$DevID ='bf69e9-f39-4d0-93f-4f358f1d';
			$AppID ='Tamil-get-PRD-bb7edec1b-5b89e35c';
			$CertID ='PRD-b7edec1b74c5-aade-4be-ac19-5f79 ';
			$ServerURL ='https://api.ebay.com/ws/api.dll';
			$Compatability =001;
			$SiteID =0;
		}
		else{
			$AuthKey=$ebaysetting_result[0]['AuthKey'];
			$DevID =$ebaysetting_result[0]['DevID'];
			$AppID =$ebaysetting_result[0]['AppID'];
			$CertID =$ebaysetting_result[0]['CertID'];
			$ServerURL =$ebaysetting_result[0]['ServerURL'];
			$Compatability =$ebaysetting_result[0]['Compatability'];
			$SiteID =$ebaysetting_result[0]['SiteID'];
		}
		$successcount = 0;
		$ebaysyncfee = 0;
		$totalcount = 0;
		$summary = array();
		$userToken = $AuthKey;
		$devID = $DevID;   // these prod keys are different from sandbox keys
        $appID = $AppID;
        $certID = $CertID;
        $serverUrl = $ServerURL;
        $compatabilityLevel = $Compatability;
        //$countoftot = count($data['sync_data']);
        $runcount = 0;
        $itemid = '';
        $startprice = '';
		foreach ($data['sync_data'] as $key => $value) 
		{
			$recordDate = $startDate = date('Y-m-d H:i:s');
			$accountid = 'PG001';
			$siteID = $SiteID;
			$verb = 'ReviseFixedPriceItem';

			$itemid = $value['ItemId'];
			$startprice = $value['Recomend_Price'];
			$mypart = $value['MyPartNo'];
			$mysku = $value['SKU'];
			$oldprice = $value['MyPrice'];
			

			///Build the request Xml string
			$requestXmlBody  = '<?xml version="1.0" encoding="utf-8" ?>';
			$requestXmlBody .= '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
			$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
			$requestXmlBody .= "<ErrorLanguage>en_US</ErrorLanguage>";
			$requestXmlBody .= "<WarningLevel>High</WarningLevel>";
			$requestXmlBody .= "<Item>";
			$requestXmlBody .= "<ItemID>$itemid</ItemID>";
			$requestXmlBody .= "<StartPrice>$startprice</StartPrice>";	
			$requestXmlBody .= " </Item>";
			$requestXmlBody .= '</ReviseFixedPriceItemRequest>';

			//Create a new eBay session with all details pulled in from included keys.php
			$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);

			//send the request and get response
			$responseXml = $session->sendHttpRequest($requestXmlBody);
			$responseDoc = new DomDocument();
			$responseDoc->loadXML($responseXml);
			
			$xml = simplexml_load_string($responseXml);

			//insert data into psmart_price_update_history table
			$pstatus = $xml->Ack;

			$insertqry = "INSERT INTO psmart_price_update_history (ItemID, SKU, MyPartNo, OldPrice, NewPrice, Status, Created_By, Created_On) 
								values ('".$itemid."', '".$mysku."', '".$mypart."', '".$oldprice."', '".$startprice."', '".$pstatus."', '".$createdby."', NOW()) ";
			
			$result_query = $this->db->query($insertqry);
			$res_update   =   $this->PricesmartModel->updateprice($itemid,$startprice);
			
			//insert data end

			if ($xml->Ack == 'Success')
			{
				//$this->PricesmartModel->updateprice($itemid,$startprice);
				foreach ($xml->Fees->Fee as $mainkey => $mainvalue) {
					if ($mainvalue->Fee != 0.0)
					{
						$name = (array) $mainvalue->Name->{0};
						$fee = (array) $mainvalue->Fee->{0};
						$rdate = (array) $xml->EndTime->{0};
						
						$summary[] = array(
								'ItemID' => $itemid,
								'Price' => $startprice,
								'Status' => 'S',
								'Message' => '',
								'FeesHead' => $name[0],
								'Fees' => $fee[0],
								'Recorddate' => $rdate[0],
								'Created_By' => $this->session->userdata('username'));
						$ebaysyncfee += $fee[0];
						$this->db->insert('psmart_ebay_sync_fees', array(
																		'ItemID' => $itemid,
																		'Price' => $startprice,
																		'Status' => 'S',
																		'Message' => '',
																		'FeesHead' => $name[0],
																		'Fees' => $fee[0],
																		'Created_By' => $this->session->userdata('username'),
																		'Created_On' => $rdate[0]
																	));
					}
				}
				$successcount++;
				 $runcount =$runcount;
				  $total =count($data['sync_data']);
				  $itemid =$itemid;
				  $startprice =$startprice;
				  $runcount++;
				  /*$buffer = array('total' => $total,'runcount' => $runcount,
						 	  'itemid' => $itemid,'startprice' => $startprice,'oldprice'=>$oldprice);
				echo json_encode($buffer).PHP_EOL; 
			    ob_flush();
				flush();*/

				echo json_encode(array('total' => $total,'runcount' => $runcount,'itemid' => $itemid,'startprice' => $startprice,'oldprice'=>$oldprice)); 
			    ob_get_contents();
				flush();
				ob_flush();
				 
			}
			else
			{
				  $runcount =$runcount;
				  $total =count($data['sync_data']);
				  $itemid =$itemid;
				  $startprice =$startprice;
				  $runcount++;
				 /* $buffer = array('total' => $total,'runcount' => $runcount,
						 	  'itemid' => $itemid,'startprice' => $startprice,'oldprice'=>$oldprice);
				echo json_encode($buffer).PHP_EOL; 
			    ob_flush();
				flush();*/

					echo json_encode(array('total' => $total,'runcount' => $runcount,'itemid' => $itemid,'startprice' => $startprice,'oldprice'=>$oldprice)); 
			    ob_get_contents();
				flush();
				ob_flush();
				  
			}
			$totalcount += 1;  
		}

			$failurecount = $totalcount - $successcount; 
			echo json_encode(array('totalcount'=>$totalcount,'ebaysyncfee'=>$ebaysyncfee,'successcount'=>$successcount,'failurecount'=>$failurecount));
	}

	 public function psmrt_edit_result(){
    $data = json_decode(file_get_contents("php://input"), true);
    $data_result =$this->PricesmartModel->psmrt_edit_result_model($data['itemid']);
    $result1 = array();
    foreach($data_result as $value){
        $result1[] = array('itemid'=>$value->itemid,
                    'sku'=>$value->sku,
                    'title'=>(string)($value->title),
                    'price'=>(float)(round($value->price,2)),
                    'Keywords_AND'=>(string)($value->Keywords_AND),
                    'Keywords_OR'=>(string)($value->Keywords_OR),
                    'Exclude_keywords'=>(string)($value->Exclude_keywords),
                    'Category_Include'=>(string)$value->Category_Include,
                    'Category_Exclude'=>(string)$value->Category_Exclude,
                    'my_price'=>(float)(round($value->my_price,2)),
                    'Percent'=>(float)(round($value->Percent,2)),
                    'Priceform_from'=>(float)(round($value->Priceform_from,2)),
                    'Priceform_to'=>(float)(round($value->Priceform_to,2)),
                    'Conditions'=>(float)($value->Conditions),
                    'Shipping'=>(float)($value->Shipping),
                    'Location'=>(float)($value->Location),
                    'Sort_order'=>(float)($value->Sort_order),
                    'Sellers_Include'=>(string)($value->Sellers_Include),
                    'Sellers_Exclude'=>(string)($value->Sellers_Exclude),
                    'Lastrun_id'=>(float)($value->Lastrun_id),
                    'Active_flag'=>(float)($value->Active_flag),
                    'ebay_url'=>(string)($value->ebay_url),
                    'crawler_url'=>(string)($value->crawler_url),
                    'category'=>(string)($value->category),
                    'pictureurl'=>(string)($value->pictureurl),
                    'CategoryID'=>(string)($value->CategoryID),
                    'Titleincludeflag'=>(int)($value->Titleincludeflag),
                    'TopRated'=>(int)($value->TopRated),
                    'HandlingTime'=>(int)($value->HandlingTime)

                );
      }
  /* echo "<pre>";
    print_r($data_result);
    exit;*/
    echo json_encode($result1);
  }
}

