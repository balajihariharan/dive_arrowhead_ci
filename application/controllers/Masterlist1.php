<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterlist extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database();
        $this->load->helper( 'Metrics_helper' );
   		$this->load->model('MasterlistModel');
   		$this->load->model('RawlistModel');
   		$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		ini_set('memory_limit', '256M');
	}

	public function index()
	{	
		if($this->session->userdata('loginid')){
		$mainmenu =  $this->MenuModel->get_menulist();
		$filter = array();
        foreach($mainmenu as $resultvalue){
            $array = explode("/",$resultvalue->submenuurl);
            $filter[] = $array[0];
        }
        if(in_array($this->uri->segment(1),$filter)){
		$unique = array();
		foreach($mainmenu as $mainkey => $mainvalue){
			$unique[] = $mainvalue;
		}
		$selfunique = array();
		$selfunique1 = array();
		$icon = array();
		foreach($unique as $value){
			if($value->MenuID == $value->submenuparentid){
				$selfunique[$value->submenuitem] = $value->MenuItem;
			}
			else{
				$selfunique1[] = $value->MenuItem;
			}
		}
		$resultvalue   = [];
		$unique = array_unique($selfunique);
		foreach($unique as $search) {
			$found = array_keys($selfunique, $search);
			if(count($found) > 1) {
				$resultvalue[$search] = $found;
			}
			else{
				$resultvalue[$search] = $found;
			}
		}
		$resultvalue1 = array();
		$resultvalue2 = array();
		foreach($selfunique1 as $value){
			foreach($resultvalue as $val){
				if(!in_array($value, $val))
				{
					$resultvalue1[$value] = $value;
				}
				if(in_array($value, $val))
				{
					$resultvalue2[$value] = $value;
				}
			}
		}
		$result = array_diff($resultvalue1,$resultvalue2);

		$mainmenu_data['menu_data'] 	= array_merge($resultvalue,$result);
		$icononly = array();
		$urlonly = array();
		foreach($mainmenu as $meinkey => $value){
			foreach($mainmenu_data['menu_data'] as $subkey => $subvalue){
				$icononly[$value->MenuItem] = $value->Icon;
				$urlonly[$value->submenuitem]  = $value->submenuurl;
			}
		}
		$mainmenu_data['icononly'] 	= $icononly;
		$mainmenu_data['url'] 	= $urlonly;
		$metrics_result = $this->MasterlistModel->get_metrics_model();
		$mainmenu_data['comptitornameandid'] = $this->MasterlistModel->get_competitor_model();

		$temp= $metrics_result[0]['TotalSales'];
	    $metrics_result[0]['TotalSales'] = test_method($temp);
	    $mainmenu_data['metrics'] = $metrics_result;
        $this->load->view('masterlistview',$mainmenu_data);
	    }
		else
	   {

		redirect('/Login/index', 'refresh');
	   }
	   }
	   else
	   {

		redirect('/Login/index', 'refresh');
	  }
	}

	public function competitor_category(){
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
			$competitor_category_result 	= $this->MasterlistModel->get_competitor_category($data['competitor']);
		}
		echo json_encode($competitor_category_result);
			/*echo "<pre>";
		print_r($data);
		exit;
	*/


	}


	public function masterlist_details_control(){
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
			$competitor_category_result 	= $this->MasterlistModel->Masterlist_details_model($data['competitor'],$data['category']);	
			$competitor_category_result1 	= $this->MasterlistModel->get_item_detail_model($data['competitor'],$data['category']);
			/*echo "<pre>";
		            print_r($competitor_category_result1);
		            exit;*/


			
	        		
            
			$competitor_category = array();
			foreach($competitor_category_result as $value){
				$competitor_category[] = array('ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'Category'=>$value->Category,
										  'CategoryID'=>$value->CategoryID,
										  'price'=>round($value->price,2),
										  'Qtysold'=>round($value->Qtysold,2),
										  'lastweekQtysold'=>round($value->lastweek_Qtysold,2),
										  'TotalSales'=>round($value->TotalSales,2),
										  'ItemURL'=>$value->ItemURL,
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,
										  'compid'=>round($value->compid),										  
										  'ListingDate'=>$value->ListingDate
										 );
			}
			$data['masterdetail'] 			= $competitor_category;
			$temp= $competitor_category_result1[0]['TotalSales'];
	        $competitor_category_result1[0]['TotalSales'] = test_method($temp);
			$data['itemdetail'] 			= $competitor_category_result1;
			

            }
		echo json_encode($data);

	

	}

	public function search_partno(){
		$data = json_decode(file_get_contents("php://input"), true);
		/*echo "<pre>";
		print_r($data);
		exit;
		$partno_result 	= $this->MasterlistModel->search_partno_model($data['searchpartno']);
		/*echo "<pre>";
		print_r($partno_result);
		exit;*/
		echo json_encode($partno_result);
	}

		public function competitor_mypartnumber_update(){

		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_result 	= $this->MasterlistModel->competitor_mypartnumber_update_model($data[0]['value'],$data[1]['value'],$data[2]['value']);
		if($competitor_result != 'Failure'){
			echo json_encode($competitor_result);
		}
		else{
			echo 'failure';
		}
	}

    	public function remove_from_masterlist(){
		$data = json_decode(file_get_contents("php://input"),true);
		if($data['itemid']){
			$competitor_master_result		= $this->MasterlistModel->remove_from_masterlist_model($data['itemid']);
		}
			echo json_encode($competitor_master_result);
		/*if($competitor_master_result !== 'failure'){
			echo $competitor_master_result;
		}
		else{
			echo "failure";
		}*/
		
		}

		public function exportexcelcomplete(){
			$result=$this->MasterlistModel->getcompletelist();
			$competitor_category = array();
			foreach($result as $value){
				$competitor_category[] = array('ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'Category'=>$value->Category,
										  'price'=>round($value->price,2),
										  'Qtysold'=>round($value->Qtysold,2),
										  'lastweekQtysold'=>round($value->lastweek_Qtysold,2),
										  'TotalSales'=>round($value->TotalSales,2),
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,									  
										  'ListingDate'=>$value->ListingDate
										 );
			}
			echo json_encode($competitor_category);
		}

}
