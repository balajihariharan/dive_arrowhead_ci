<?php
	defined ('BASEPATH') OR exit('No direct script access allowed');
	class Email_Config extends CI_Controller
	{
		public function __construct() {
			parent::__construct();
			$this->load->model('Email_Configmodel');     
			$this->load->model('CompetitorModel');
			$this->load->model('MenuModel');
			$this->load->library('Screenname');
		}

		public function index() { 
			$getmenu    = $this->screenname->getscreen();
			$router     =& load_class('Router', 'core');
			$indexroute =  $router->fetch_class().'/'.$router->fetch_method();
			$title      = '';
			foreach($getmenu['url'] as $key => $value) {
				if($value == $indexroute) {
					$title = $key;
				}
			}
			$mainmenu_data['icononly']   = $getmenu['icononly'];
			$mainmenu_data['url']        = $getmenu['url'];
			$mainmenu_data['screen']     = $title;
			$mainmenu_data['menu_data']  = $getmenu['menu_data'];
			$result                      = $this->Email_Configmodel->email_config_get();
			$mainmenu_data['load_data']  = $result;
			$ref                         =$this->Email_Configmodel->get_title();
			$mainmenu_data['title']      = $ref;
			$this->load->view('email_configview',$mainmenu_data);
		}
		public function insert() { 
			$data = json_decode(file_get_contents("php://input"), true);
			$smtpserver = $data['smtpserver'];
			$username = $data['username'];
			$password = $data['password'];
			$formmail = $data['formmail'];
			$to = $data['to'];
			$cc = $data['cc'];
			$bcc = $data['bcc'];
			$username =  $this->session->userdata('username');
			$roleid =  $this->session->userdata('roleid');
			$this->Email_Configmodel->dive_email_config_insert( $smtpserver, $username, $password, $formmail, $to, $cc, $bcc, $roleid);
			return true;
		}
	}
?>