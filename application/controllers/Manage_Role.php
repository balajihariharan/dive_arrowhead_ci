<?php
class Manage_Role extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
    	$this->load->model('Manage_Role_Model');
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->library('Screenname');
  	}
	   public function index(){
		$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
		$result=$this->Manage_Role_Model->index();
		$mainmenu_data['role']=$result;
		 $ref =$this->Manage_Role_Model->get_title();
        $mainmenu_data['title']= $ref;
		$this->load->view('Manage_Role_View',$mainmenu_data);
		
        
		
	}
	public function edit(){
		if(isset($_POST['id']))
		{
    		$roleid = $_POST['id'];
		 	$result=$this->Manage_Role_Model->edit_select($roleid);
			foreach($result as $item) 
			{
    			echo json_encode(array('result'=>$item));
  			}
		}		
	}
	public function save(){
		$roleid = $_POST['rid'];
		$rolename = $_POST['rname'];
		$roledesc = $_POST['rdesc'];
		$username=  $this->session->userdata('username');
		$this->Manage_Role_Model->save($roleid,$rolename,$roledesc,$username);
	}

	public function delete(){
		if(isset($_POST['id']))
		{
    		$roleid = $_POST['id'];
		 	$this->Manage_Role_Model->delete($roleid);
		}	
	}
	public function addrole(){
		$result=$this->Manage_Role_Model->get_roleid();
		foreach($result as $item) 
		{
    			echo json_encode(array('result'=>$item));
  		}
	}
}