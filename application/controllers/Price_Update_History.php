<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price_Update_History extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database();
   		$this->load->model('Price_Update_History_model');
      $this->load->model('MenuModel');
      $this->load->library('Screenname');
   		}

	public function index()
	{	
  $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $ref =$this->Price_Update_History_model->get_title();
    $mainmenu_data['title']= $ref;
        
        $this->load->view('Price_Update _History_view', $mainmenu_data);
        }
        

   public function get_price_history(){ 
       $data = json_decode(file_get_contents("php://input"), true);
       $runfrm = $data['fromdate'];
       $runto= $data['todate'];
       $data = $this->Price_Update_History_model->price_history_model($runfrm, $runto);

       $price_data = array();
       foreach($data as $value){
        $price_data[] = array('ItemID'=>$value->ItemID,
                      'StoreName'=>$value->StoreName,
                      'SKU'=>$value->SKU,
                      'MyPartNo'=>$value->MyPartNo,
                      'OldPrice'=>round($value->OldPrice,2),
                      'NewPrice'=>round($value->NewPrice,2),
                      'Status'=>$value->Status
                     );

      }
       echo json_encode($price_data);
     }

}

?>
       