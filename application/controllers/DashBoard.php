<?php
class DashBoard extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
    	$this->load->model('DashBoard_Model');
    	$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->model('SalestrendsModel');
   		$this->load->helper('Metrics_helper');
   		 $this->load->library('Screenname');
   		 ini_set('memory_limit', '256M');
  	}
	public function index(){
		$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    $flag=0;
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
        $flag = 1;
      }
    }
    if ($flag == 0)
    {
    	foreach($getmenu['zeroflag_url'] as $key => $value){
      	if($value == $indexroute){
        	$title = $getmenu['zeroflag_name'][$key];
      		}
    	}	
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
		
		$ref =$this->DashBoard_Model->get_title();
        $mainmenu_data['title']= $ref;
      
        $this->load->view('DashBoard_View',$mainmenu_data);
	}
   

	public function DashBoard_data(){
		$data = json_decode(file_get_contents("php://input"), true);
		$year = $data['year'];
		$week = $data['week'];
		$dashboard_result = $this->DashBoard_Model->getdata($week,$year);
		$dashboard_category = array();
		
			foreach($dashboard_result as $value){
				$dashboard_category[] = array('CompId'=>$value['compid'],
										  'Company'=>$value['Company'],
										  'SevenDaysSales'=>(float)$value['7 Days Sales'],
										  'SevenDaysSales_lw'=>(float)$value['7 Days Sales [Lst Wk]'],
										  'SevenDAYPercentOFTOTAL'=>round($value['7 DAY % OF TOTAL'],2),
										  'SevenDAYPercentOFTOTAL_lw'=>round($value['7 DAY % OF TOTAL [Lst Wk]'],2),
										  'Listing'=>round($value['Listing'],2),
										  'Listing_lw'=>round($value['Listing [Lst Wk]'],2),
										  'SoldSevendays'=>round($value['Sold [7 days]'],2),
										  'SoldSevendays_lw'=>round($value['Sold [7 days] [Lst Wk]'],2),
										  'PercentofListingSold'=>round($value['% of Listing Sold'],2),
										  'PercentofListingSold_lw'=>round($value['% of Listing Sold [Lst Wk]'],2),
										  'AvgSalePrice'=>(float)$value['Avg. Sale Price'],
										  'AvgSalePrice_lw'=>(float)$value['Avg. Sale Price [Lst Wk]']);
			}
		$data['dashboard_category'] = $dashboard_category;
		echo json_encode($data);
	}

	public function DashBoard_data2(){
		$data = json_decode(file_get_contents("php://input"), true);
		$year = $data['year'];
		$week = $data['week'];
		$dashboard_result = $this->DashBoard_Model->getdata_complete($week,$year);
		$dashboard_category = array();	
			foreach($dashboard_result as $value){
				$dashboard_category[] = array('CompId'=>$value['compid'],
										  'Company'=>$value['Company'],
										  'SevenDaysSales'=>(float)$value['7 Days Sales'],
										  'SevenDaysSales_lw'=>(float)$value['7 Days Sales [Lst Wk]'],
										  'SevenDAYPercentOFTOTAL'=>round($value['7 DAY % OF TOTAL'],2),
										  'SevenDAYPercentOFTOTAL_lw'=>round($value['7 DAY % OF TOTAL [Lst Wk]'],2),
										  'Listing'=>round($value['Listing'],2),
										  'Listing_lw'=>round($value['Listing [Lst Wk]'],2),
										  'SoldSevendays'=>round($value['Sold [7 days]'],2),
										  'SoldSevendays_lw'=>round($value['Sold [7 days] [Lst Wk]'],2),
										  'PercentofListingSold'=>round($value['% of Listing Sold'],2),
										  'PercentofListingSold_lw'=>round($value['% of Listing Sold [Lst Wk]'],2),
										  'AvgSalePrice'=>(float)$value['Avg. Sale Price'],
										  'AvgSalePrice_lw'=>(float)$value['Avg. Sale Price [Lst Wk]']);
			}
		$data['dashboard_category'] = $dashboard_category;
		echo json_encode($data);
	}

	public function ReportTwo(){
		$data = json_decode(file_get_contents("php://input"), true);
		$year = $data['year'];
		$week = $data['week'];
		$compid = $data['compid'];
		$dashboard_result = $this->DashBoard_Model->getdatadrill2($week,$year,$compid);
		$dashboard_category = array();
		foreach($dashboard_result as $value){
			$dashboard_category[] = array('CategoryId'=>$value['category_id'],
									  'Category'=>$value['Category'],
									  'SubCategory'=>$value['SubCategory'],
									  'MappedItems'=>(float)$value['MappedItems'],
									  'SevenDaysSales'=>(float)$value['7DaysSales'],
									  'SevenDaysSales_lw'=>(float)$value['7DaysSalesLstWk'],
									  'SevenDAYPercentOFTOTAL'=>round($value['7DAYPercentOFTOTAL'],2),
									  'SevenDAYPercentOFTOTAL_lw'=>round($value['7DAYOFPercentTOTALLstWk'],2),
									  'Listing'=>round((float)$value['Listing'],2),
									  'Listing_lw'=>round((float)$value['ListingLstWk'],2),
									  'SoldSevendays'=>round((float)$value['Sold7days'],2),
									  'SoldSevendays_lw'=>round((float)$value['Sold7daysLstWk'],2),
									  'PercentofListingSold'=>round((float)$value['PercentofListingSold'],2),
									  'PercentofListingSold_lw'=>round((float)$value['PercentofListingSoldLstWk'],2),
									  'AvgSalePrice'=>(float)$value['AvgSalePrice'],
									  'AvgSalePrice_lw'=>(float)$value['AvgSalePriceLstWk']);
		}
		$data['dashboard_category'] = $dashboard_category;
		echo json_encode($data);
	}

	public function ReportTwo2(){
		$data = json_decode(file_get_contents("php://input"), true);
		$year = $data['year'];
		$week = $data['week'];
		$compid = $data['compid'];
		$dashboard_result = $this->DashBoard_Model->getdatadrill2_complete($week,$year,$compid);
		$dashboard_category = array();
		foreach($dashboard_result as $value){
			$dashboard_category[] = array('CategoryId'=>$value['category_id'],
									  'Category'=>$value['Category'],
									  'SubCategory'=>$value['SubCategory'],
									  'SevenDaysSales'=>(float)$value['7DaysSales'],
									  'SevenDaysSales_lw'=>(float)$value['7DaysSalesLstWk'],
									  'SevenDAYPercentOFTOTAL'=>round($value['7DAYPercentOFTOTAL'],2),
									  'SevenDAYPercentOFTOTAL_lw'=>round($value['7DAYOFPercentTOTALLstWk'],2),
									  'Listing'=>round((float)$value['Listing'],2),
									  'Listing_lw'=>round((float)$value['ListingLstWk'],2),
									  'SoldSevendays'=>round((float)$value['Sold7days'],2),
									  'SoldSevendays_lw'=>round((float)$value['Sold7daysLstWk'],2),
									  'PercentofListingSold'=>round((float)$value['PercentofListingSold'],2),
									  'PercentofListingSold_lw'=>round((float)$value['PercentofListingSoldLstWk'],2),
									  'AvgSalePrice'=>(float)$value['AvgSalePrice'],
									  'AvgSalePrice_lw'=>(float)$value['AvgSalePriceLstWk']);
		}
		$data['dashboard_category'] = $dashboard_category;
		echo json_encode($data);
	}


	public function ReportThree(){
		$data = json_decode(file_get_contents("php://input"),true);
		$competitor_category_result = [];
		$competitor_category_result = $this->SalestrendsModel->get_salestrends_dashboard($data['compid'],$data['catid'],$data['date']);
		$competitor_category_result1 = $this->SalestrendsModel->get_totsal_detail_dashboard($data['compid'],$data['catid'],$data['date']);
		$competitor_category = array();
		foreach($competitor_category_result as $value){
			$competitor_category[] = array('itemid' => $value->itemid,
												'sku' => $value->sku,
												'title' => $value->title,
												'mypartno' => $value->mypartno,
												'category' => $value->category,
												'itemid_map_flag' =>$value->itemid_map_flag,
												'pricew1' => $value->pricew1,
												'pricew2' => $value->pricew2,
												'pricew3' => $value->pricew3,
												'pricew4' => $value->pricew4,
												'pricew5' => $value->pricew5,
												'salesw1' => $value->salesw1,
												'salesw2' => $value->salesw2,
												'salesw3' => $value->salesw3,
												'salesw4' => $value->salesw4,
												'salesw5' => $value->salesw5,
												'pricevw1' => round($value->pricevw1, 2),
												'pricevw2' => round($value->pricevw2, 2),
												'pricevw3' => round($value->pricevw3, 2),
												'pricevw4' => round($value->pricevw4, 2),
												'pricevw5' => round($value->pricevw5, 2),
												'qtysoldvw1' => $value->qtysoldvw1,
												'qtysoldvw2' => $value->qtysoldvw2,
												'qtysoldvw3' => $value->qtysoldvw3,
												'qtysoldvw4' => $value->qtysoldvw4,
												'qtysoldvw5' => $value->qtysoldvw5,
												'price' => (float)round($value->price, 2),
												'qtysold' => (float)$value->qtysold,
												'totalsales' => round($value->totalsales, 2),
												'itemURL' => $value->itemURL,
												'listingdate' => $value->listingdate,
												'compid' => $value->compid
										 );
		}
		$temp= $competitor_category_result1[0]['TotalSales'];
	    $competitor_category_result1[0]['TotalSales'] = test_method($temp);
	  	$data['dashboard_drill_data_header'] = $competitor_category_result1;
        $data['dashboard_drill_data'] = $competitor_category;
		echo json_encode($data);
	}

	public function ReportFour(){
		$data = json_decode(file_get_contents("php://input"),true);
		$competitor_category_result = [];
		$competitor_category_result = $this->SalestrendsModel->get_salestrends_dashboard2($data['itemid'],$data['date']);
		$competitor_category = array();
		foreach($competitor_category_result as $value){
			$competitor_category[] = array('itemid' => $value->itemid,
												'sku' => $value->sku,
												'title' => $value->title,
												'mypartno' => $value->mypartno,
												'compname' => $value->compname,
												'category' => $value->category,
												'pricew1' => $value->pricew1,
												'pricew2' => $value->pricew2,
												'pricew3' => $value->pricew3,
												'pricew4' => $value->pricew4,
												'pricew5' => $value->pricew5,
												'salesw1' => $value->salesw1,
												'salesw2' => $value->salesw2,
												'salesw3' => $value->salesw3,
												'salesw4' => $value->salesw4,
												'salesw5' => $value->salesw5,
												'pricevw1' => round($value->pricevw1, 2),
												'pricevw2' => round($value->pricevw2, 2),
												'pricevw3' => round($value->pricevw3, 2),
												'pricevw4' => round($value->pricevw4, 2),
												'pricevw5' => round($value->pricevw5, 2),
												'qtysoldvw1' => $value->qtysoldvw1,
												'qtysoldvw2' => $value->qtysoldvw2,
												'qtysoldvw3' => $value->qtysoldvw3,
												'qtysoldvw4' => $value->qtysoldvw4,
												'qtysoldvw5' => $value->qtysoldvw5,
												'price' => (float)round($value->price, 2),
												'qtysold' => (float)$value->qtysold,
												'totalsales' => round($value->totalsales, 2),
												'itemURL' => $value->itemURL,
												'listingdate' => $value->listingdate,
												'compid' => $value->compid
										 );
		}
        $data['dashboard_drill_data'] = $competitor_category;
		echo json_encode($data);
	}
}
?>
