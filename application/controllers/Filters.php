<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filters extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database(); 
   		$this->load->model('CompetitorModel');
   		$this->load->model('RawlistModel');
   		$this->load->model('MenuModel');
   		ini_set('memory_limit', '256M');
   		 $this->load->library('Screenname');
	}

	  public function exclusionlist(){
	 $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }

    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
         $ref =$this->CompetitorModel->get_title();
        $mainmenu_data['title']= $ref;
		$this->load->view('exclusion_list',$mainmenu_data);
		
		 }

	public function disp(){
		$result=$this->CompetitorModel->get_exclusion();
		echo json_encode($result);
	}

	public function exclusion(){
	$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    $flag = 0;
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
        $flag = 1;
      }
    }
    if ($flag == 0)
    {
    	foreach($getmenu['zeroflag_url'] as $key => $value){
      	if($value == $indexroute){
        	$title = $getmenu['zeroflag_name'][$key];
      		}
    	}	
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
        $ref =$this->CompetitorModel->get_title();
        $mainmenu_data['title']= $ref;

		if(count($this->uri->segment(2)) == 1) {
			$fetch_exclusion 					= $this->CompetitorModel->fetch_exclusion($this->uri->segment(3));
			$competitor_exclusion 				= $this->CompetitorModel->exclusion_general_lookup();
			$mainmenu_data['general_dropdown']  = $competitor_exclusion;
			$mainmenu_data['fetch_exclusion']  	= $fetch_exclusion;
			$this->load->view('exclusion',$mainmenu_data);	
		}
		else{
			$competitor_exclusion 				= $this->CompetitorModel->exclusion_general_lookup();
			$mainmenu_data['general_dropdown']  = $competitor_exclusion;
			$this->load->view('exclusion',$mainmenu_data);
		}
	   }	

	public function fetch_exclusion_frontend(){
		if(isset($_POST) && $_POST !== ''){
			$fetch_exclusion 					= $this->CompetitorModel->fetch_exclusion($_POST['urlid']);
			$mainmenu_data['fetch_exclusion']  	= $fetch_exclusion;
			/*echo "<pre>";
			print_r($fetch_exclusion);
			exit;*/
			$exclusionname = $fetch_exclusion[0]->exclusionname;
			$location_us   = $fetch_exclusion[0]->location_us;
			$show_active   = $fetch_exclusion[0]->show_active;
			$toprated      = $fetch_exclusion[0]->toprated;
			$free_shipping = $fetch_exclusion[0]->free_shipping;
			$arr 		   = str_replace("'", "",$fetch_exclusion[0]->general_cond);
			$general_cond  = explode(",",$arr);
			$arr 		   = str_replace("'", "",$fetch_exclusion[0]->part_filter);
			$part_filter   = explode(",",$arr);
			$arr 		   = str_replace("'", "",$fetch_exclusion[0]->category_filter);
			$category_filter = explode(",",$arr);
			$arr 			 = str_replace("'", "",$fetch_exclusion[0]->brand_filter);
			$brand_filter 	 = explode(",",$arr);
			$arr 			 = str_replace("'", "",$fetch_exclusion[0]->title_filter);
			$title_filter 	 = explode(",",$arr);
			$arr 			 = str_replace("'", "",$fetch_exclusion[0]->competitor_filter);
			$competitor_filter = explode(",",$arr);
			$arr 			 = str_replace("'", "",$fetch_exclusion[0]->category_filter_id);
			$category_filter_id = explode(",",$arr);
			$arr 			 = str_replace("'", "",$fetch_exclusion[0]->brand_filter_id);
			$brand_filter_id = explode(",",$arr);
			$arr 			 = str_replace("'", "",$fetch_exclusion[0]->competitor_filter_id);
			$competitor_filter_id = explode(",",$arr);
			echo json_encode($result_fetch_exclusion = array('exclusionname'=> $exclusionname,
																		'general_cond' 		 => $general_cond,
																		'location_us'  		 => $location_us,
																		'show_active'  		 => $show_active,
																		'toprated' 	   		 => $toprated,
																		'free_shipping'		 => $free_shipping,
																		'part_filter'  		 => $part_filter,
																		'category_filter' 	 => $category_filter,
																		'category_filter_id' => $category_filter_id,
																		'brand_filter' 		 => $brand_filter,
																		'brand_filter_id'	 => $brand_filter_id,
																		'title_filter' 		 => $title_filter,
																		'competitor_filter'  => $competitor_filter,
																		'competitor_filter_id' => $competitor_filter_id)
																		);
		}
	}

	public function exclusion_competitorname_search(){
		$searchtext 				= $_POST['searchtext'];
		$competitor_exclusion 		= $this->CompetitorModel->exclusion_competitor_name($searchtext);
		echo json_encode(array('result'=>$competitor_exclusion));
	}
	
	public function exclusion_search_partnumber(){
		$searchtext 				= $_POST['searchtext'];
		$competitor_exclusion 		= $this->CompetitorModel->search_part_number($searchtext);
		echo json_encode(array('result'=>$competitor_exclusion));
	}
	
	public function exclusion_search_categoryname(){
		$searchtext 				= $_POST['searchtext'];
		$competitor_exclusion 		= $this->CompetitorModel->search_category_name($searchtext);
		echo json_encode(array('result'=>$competitor_exclusion));
	}
	
	public function exclusion_search_brandname(){
		$searchtext 				= $_POST['searchtext'];
		$competitor_exclusion 		= $this->CompetitorModel->search_brand_name($searchtext);
		echo json_encode(array('result'=>$competitor_exclusion));
	}

	public function exclusion_submission() {
	
		if (count($_POST) > 0) {
			if (isset($_POST['exclustion_id']))  { 	$exclusion_id = trim($_POST['exclustion_id']); 	} else {  $exclusion_id = ""; }
			if (isset($_POST['exclusion_name'])) {  $exclusion_name = trim($_POST['exclusion_name']); } else { $exclusion_name = ""; }
			if (isset($_POST['general_select_combo'])) { $general_select_combo = trim($_POST['general_select_combo']); } else { $general_select_combo = ""; }
			if (isset($_POST['location_name'])) { $location_name = 1; } else { $location_name = 0; }
			if (isset($_POST['rated_name'])) {	$rated_name = 1; } else { $rated_name = 0; }
			if (isset($_POST['shipping_name'])) {	$shipping_name = 1; } else { $shipping_name = 0; }
			if (isset($_POST['active_only'])) {	$active_only = 1; } else { $active_only = 0; }
			if (isset($_POST['parts_selected_value'])) { $parts_selected_value =  array_map('trim', $_POST['parts_selected_value']); } else { $parts_selected_value = ""; }
			if (isset($_POST['general_selected_value'])) { $general_selected_value = array_map('trim', $_POST['general_selected_value']); } else { $general_selected_value = ""; }
			if (isset($_POST['category_selected_value'])) { $category_selected_value =  array_map('trim', $_POST['category_selected_value']); } else { $category_selected_value = ""; }
			if (isset($_POST['brand_selected_value'])) { $brand_selected_value = array_map('trim', $_POST['brand_selected_value']);  } else { $brand_selected_value = ""; }			
			if (isset($_POST['title_selected_value'])) { $title_selected_value =  array_map('trim', $_POST['title_selected_value']); } else { $title_selected_value = ""; }
			if (isset($_POST['competitor_selected_value'])) { $competitor_selected_value = array_map('trim', $_POST['competitor_selected_value']); } else { $competitor_selected_value = ""; }
			
			if ($parts_selected_value != '')      $parts_selected_value = "'".implode("','", $parts_selected_value)."'";
			if ($general_selected_value != '')    $general_selected_value = "'" . implode("','", $general_selected_value). "'";
			if ($category_selected_value != '')   $category_selected_value = "'".implode("','", $category_selected_value)."'";
			if ($brand_selected_value != '')      $brand_selected_value = "'".implode("','", $brand_selected_value)."'";
			if ($title_selected_value != '')      $title_selected_value = "'".implode("','", $title_selected_value)."'";
			if ($competitor_selected_value != '') $competitor_selected_value = "'".implode("','", $competitor_selected_value)."'";
			
			
		}
		$username=  $this->session->userdata('username');
		$roleid =   $this->session->userdata('roleid');

		
		$exclusion_save = $this->CompetitorModel->save_exclusion($exclusion_id,$exclusion_name,$general_selected_value,$location_name,$active_only,
		         $rated_name,$shipping_name,$parts_selected_value,$category_selected_value,$brand_selected_value,$title_selected_value,
				 $competitor_selected_value,$username,$roleid);
		if($exclusion_save == 'success') {
			echo 'Exculsion Saved Successfully';
		} else {
			echo 'Exculsion Saving Problem Please Try again';
		}
	}

	public function exclusionlist_update(){
		$competitor_listing_update 	= $this->CompetitorModel->exclusionlist_update_model($_POST['dataid'],$_POST['checkid']);
		/*if($competitor_listing_update){
			return true;
		}
		else{
			return false;
		}*/
	}

	public function index()
	{	
		$metrics_result 							= $this->CompetitorModel->metrics_model();
		$competitor_result 							= $this->CompetitorModel->get_competitor();
		$category_combo 							= $this->CompetitorModel->get_tab1_categorycombo();
		$competitor_data['data'] 					= $competitor_result;
		$competitor_data['metrics']					= $metrics_result;
		$competitor_data['category_combo']			= $category_combo;
		$this->load->view('competition',$competitor_data);
	}

	public function index_get(){
		$data = json_decode(file_get_contents("php://input"), true);
		if (isset($data['categoryids'])) {$categoryids = $data['categoryids'];} else {$categoryids = '';}
		if (isset($data['iamlowestfirst'])) {$iamlowestfirst = $data['iamlowestfirst'];} else {$iamlowestfirst = 0;}
		if (isset($data['iamlowestsecond'])) {$iamlowestsecond = $data['iamlowestsecond'];} else {$iamlowestsecond = 0;}
		if (isset($data['mypartno'])) {$mypartnoval = $data['mypartno'];} else {$mypartnoval = '';}
		$competitor_listing_result 				= $this->CompetitorModel->get_competitor_listings($categoryids,$mypartnoval,$iamlowestfirst,$iamlowestsecond);
		$competitor_listing = array();
		$competitor_listing_sublist = array();
		foreach($competitor_listing_result['first'] as $d=> $mainvalue){
			$competitor_listing[$d] = array('MyID'=>round($mainvalue->MyID),
										  'MySKU'=>$mainvalue->MySKU, 
										  'MyTitle'=>$mainvalue->MyTitle,
										  'MyCategory'=>$mainvalue->MyCategory,
										  'MyPrice'=>round($mainvalue->MyPrice,2),
										  'MyQuantitySold'=>round($mainvalue->MyQuantitySold,2),
										  'mypartno'=>($mainvalue->mypartno == NULL) ? 'NA' : $mainvalue->mypartno,
										  'lastweek_Qtysold'=>round($mainvalue->lastweek_Qtysold),
										  'CompLowPrice'=>round($mainvalue->CompLowPrice,2),
										  'CompHighPrice'=>round($mainvalue->CompHighPrice,2),
										  'CompCount'=>round($mainvalue->CompCount)
										 );
			
			
				foreach($competitor_listing_result['second'] as $key => $subvalue){
					if($mainvalue->MyID == $key){
						foreach($subvalue as $val){
								$competitor_listing_sublist[$mainvalue->MyID][] = array(
																'MyID1'=>$val->MyID,
																'CompetitorItemID1'=>$val->CompetitorItemID,
																'CompetitorName1'=>$val->CompetitorName,
																'CompetitorSKU1'=>$val->CompetitorSKU,
																'CompetitorTitle1'=>$val->CompetitorTitle,
																'CompetitorCategory1'=>$val->CompetitorCategory,
																'CompetitorPrice1'=>round($val->CompetitorPrice,2),
																'CompetitorQuantitySold1'=>($val->CompetitorQuantitySold !== NULL) ? (float) $val->CompetitorQuantitySold : 0,
																'mypartno1'=>$val->mypartno,
																'lastweek_Qtysold1'=>(float)$val->lastweek_Qtysold
																);
						}
					}
				}
		}
		$result = array('first'=>$competitor_listing,'second'=>$competitor_listing_sublist);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
	}

	public function competitor_category(){
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
			$competitor_category_result 	= $this->CompetitorModel->get_competitor_category($data['competitor']);
		}
		echo json_encode($competitor_category_result);
	}
	public function category_listing(){
		$competitor_result = $this->CompetitorModel->get_competitor_listings();
	}

	public function competitor_category_listing_showitem(){

		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_result = [];
		if ($data){

			$competitor_category_result = $this->CompetitorModel->get_competitor_category_showitem($data['tabs'],$data['competitor'],$data['category']);
			$competitor_category = array();
			foreach($competitor_category_result as $value){
				$competitor_category[] = array('ITemID'=>round($value->ITemID),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'Category'=>$value->Category,
										  'SellerName'=>$value->SellerName,
										  'Price'=>round($value->Price,2),
										  'QuantitySold'=>round($value->QuantitySold,2)  
										 );
		}
		}
		echo json_encode($competitor_category);
	}

	public function competitor_mypartnumber_update(){
		$data = json_decode(file_get_contents("php://input"), true);
		$username=  $this->session->userdata('username');
	    $roleid=  $this->session->userdata('roleid');
		$competitor_result 	= $this->CompetitorModel->competitor_mypartnumber_update_model($data[0]['value'],$data[2]['value'],$data[1]['value'],$data[3]['value'],$username,$roleid);

		
		if($competitor_result != 'Failure'){
			echo json_encode($competitor_result);
		}
		else{
			echo 'failure';
		}
	}

	public function iamlowest(){
		$data = json_decode(file_get_contents("php://input"), true);	
		if (isset($data['categoryids'])) {$categoryids = $data['categoryids'];} else {$categoryids = '';}
		if (isset($data['iamlowestfirst'])) {$iamlowestfirst = $data['iamlowestfirst'];} else {$iamlowestfirst = 0;}
		if (isset($data['iamlowestsecond'])) {$iamlowestsecond = $data['iamlowestsecond'];} else {$iamlowestsecond = 0;}
		if (isset($data['mypartno'])) {$mypartnoval = $data['mypartno'];} else {$mypartnoval = '';}
		$competitor_listing_result 				= $this->CompetitorModel->get_competitor_listings($categoryids,$mypartnoval,$iamlowestfirst,$iamlowestsecond);
		$competitor_listing = array();
		$competitor_listing_sublist = array();
		foreach($competitor_listing_result['first'] as $d=> $mainvalue){
			$competitor_listing[$d] = array('MyID'=>$mainvalue->MyID,
										  'MySKU'=>$mainvalue->MySKU,
										  'MyTitle'=>$mainvalue->MyTitle,
										  'MyCategory'=>$mainvalue->MyCategory,
										  'MyPrice'=>round($mainvalue->MyPrice,2),
										  'MyQuantitySold'=>round($mainvalue->MyQuantitySold),
										  'mypartno'=>($mainvalue->mypartno == NULL) ? 'NA' : $mainvalue->mypartno,
										  'lastweek_Qtysold'=>round($mainvalue->lastweek_Qtysold),
										  'CompLowPrice'=>round($mainvalue->CompLowPrice,2),
										  'CompHighPrice'=>round($mainvalue->CompHighPrice,2),
										  'CompCount'=>round($mainvalue->CompCount)
										 );
				foreach($competitor_listing_result['second'] as $key => $subvalue){
					if($mainvalue->MyID == $key){
						foreach($subvalue as $val){
							if($val->CompetitorQuantitySold == NULL){
								$qunatirysoldlast = 0; 
							}
							else{
								$qunatirysoldlast = (int)$val->CompetitorQuantitySold;
							}
							$competitor_listing_sublist[$mainvalue->MyID][] = array(
																'MyID'=>$val->MyID,
																'CompetitorItemID'=>$val->CompetitorItemID,
																'CompetitorName'=>$val->CompetitorName,
																'CompetitorSKU'=>$val->CompetitorSKU,
																'CompetitorTitle'=>$val->CompetitorTitle,
																'CompetitorCategory'=>$val->CompetitorCategory,
																'CompetitorPrice'=>round($val->CompetitorPrice,2),
																'CompetitorQuantitySold'=>$qunatirysoldlast,
																'mypartno'=>round($val->mypartno),
																'lastweek_Qtysold'=>round($val->lastweek_Qtysold)
																);
						}
					}
				}
		}
		$result = array('first'=>$competitor_listing,'second'=>$competitor_listing_sublist);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
	}

	public function salestrends(){
		$data = json_decode(file_get_contents("php://input"), true);
		//echo $passcompid;
		//exit;

		$sales_trend_result 				= $this->CompetitorModel->dive_comp_analysis_item_sales_trend_model(
												  $data['dataname'],
												  $data['datacompid'],
												  $data['datasku'],
												  $data['datalist']
												  );
		if($sales_trend_result){
			echo json_encode($sales_trend_result);
		}
		else{
			echo 'failure';
		}
	}

	public function delete_filter(){
		$data = json_decode(file_get_contents("php://input"), true);
		$id = $data['id'];
		$result = $this->CompetitorModel->get_delete_filter($id);
		return true;
	}
	public function filter_popup(){
		$result=$this->CompetitorModel->get_exclusion();
		$this->load->view('filter_popup',$result);
		//echo json_encode($result);
	}
}
