<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'application/libraries/EBaySession.php';
require_once 'application/libraries/keys.php';
require_once(APPPATH.'libraries/FPDF Plugin/fpdf.php'); //Export Plugin
//require_once 'application/libraries/pusher/vendor/autoload.php';//pusher

class Rawlist extends CI_Controller {

	  public function __construct(){
  		parent::__construct();
   		$this->load->database();
   		$this->load->helper( 'Metrics_helper' ); 
   		$this->load->model('RawlistModel');
		$this->load->library('keysnew');
		$this->load->library('Screenname');
		$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		ini_set('memory_limit', '256M');
   		ini_set('max_execution_time', 30000);
	}

	  public function index()
	{	

	 $getmenu = $this->screenname->getscreen();
    $router =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
				
		$mainmenu_data['icononly']  = $getmenu['icononly'];
	    $mainmenu_data['url']       = $getmenu['url'];
	    $mainmenu_data['screen']     = $title;
	    $mainmenu_data['menu_data'] = $getmenu['menu_data'];

		$category_combo 		= $this->RawlistModel->get_rawlist_competitorcombo();
		$mainmenu_data['compidandname'] 	= $category_combo;
	    $ref =$this->RawlistModel->get_title();
      /*  $mainmenu_data['title']= $ref;*/
        $mainmenu_data['title']= $ref;
        $mainmenu_data['ShowMasked']= $ref[0]->ShowMasked;
  

        $partno_result 	= $this->RawlistModel->search_partno_model();
		$this->load->view('rawlistview',$mainmenu_data);
		}
        
	
	  
	
	public function compload() {
		
		$data = json_decode(file_get_contents("php://input"), true);
		$result 	= $this->RawlistModel->search_partno_model();
		$competitor_result 	= $this->RawlistModel->get_competitor($data['chkvalue'], $data['chkkey']);
		$merget_object = array();
		foreach($competitor_result as $key => $value){
			$merget_object[] = array('CompID'=>$value->CompID,'CompetitorName'=>$value->CompetitorName); 
		}
		$data['comp'] = $merget_object;
		$data['part'] = $result;
		echo json_encode($data);
		//echo json_encode($merget_object);
	}
	
	public function competitor_category(){
		$data = json_decode(file_get_contents("php://input"), true);
		//$result['partno_result'] 	= $this->RawlistModel->search_partno_model();
		$competitor_category_result = [];
		if ($data['competitor']){
			$competitor_category_result 	= $this->RawlistModel->get_competitor_category($data['competitor'], $data['chkvalue'], $data['chkkey']);
		}
		echo json_encode($competitor_category_result);
		
	}


	public function rawlist_details_control() {
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
			 $competitor_category_result1 	= $this->RawlistModel->raw_item_detail_model($data['competitor'],$data['category'], $data['chkvalue'],$data['passgui']);
			 $competitor_category_result 	= $this->RawlistModel->rawlist_details_model($data['competitor'],$data['category'], $data['chkvalue'],$data['passgui']);
			//$competitor_category_result1 	= $this->RawlistModel->raw_item_detail_model($data['competitor'],$data['category'], $data['chkvalue']);
			//$competitor_category_result 	= $this->RawlistModel->rawlist_details_model($data['competitor'],$data['category'], $data['chkvalue']);
			$competitor_category = array();
		    $temp= $competitor_category_result1[0]['TotalSales'];
	        $competitor_category_result1[0]['TotalSales'] = test_method($temp);

			foreach($competitor_category_result as $value){
				$competitor_category[] = array('ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'category_in'=>$value->category,
										  'CategoryID'=>$value->CategoryID,
										  'CurrentPrice'=>round($value->CurrentPrice,2),
										  'QuantitySold'=>round($value->QuantitySold,2),
										  'lastweek_Qtysold'=>round($value->lastweek_Qtysold),
										  'TotalSales'=>round($value->TotalSales),
										  'ItemURL'=>$value->ItemURL,
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,
										  'CompID'=>round($value->CompID),
										  'Mlflag'=>$value->Mlflag,
										  'ListingDate'=>$value->ListingDate
										 );
			}
	
            $data['rawdetail'] 				= $competitor_category;
            $data['itemdetail'] 			= $competitor_category_result1;
            }
		echo json_encode($data);
	}

	/*public function search_partno(){
		$data = json_decode(file_get_contents("php://input"), true);
        $partno_result 	= $this->RawlistModel->search_partno_model($data['searchpartno']);
		echo json_encode($partno_result);
	}*/

	public function competitor_mypartnumber_update(){

		$data = json_decode(file_get_contents("php://input"), true);
		/*echo "<pre>";
		print_r($data['items'][0]['value']);
		exit;*/
	    $username=  $this->session->userdata('username');
		$roleid=  $this->session->userdata('roleid');
		$competitor_result 	= $this->RawlistModel->competitor_mypartnumber_update_model($data['items'][0]['value'],$data['partnum'],$data['items'][2]['value'],$username,$roleid);
		if($competitor_result != 'Failure'){
			echo json_encode($competitor_result);
		}
		else{
			echo 'failure';
		}
	}

	public function add_to_masterlist(){
		$competitor = $itemid = '';
		$data = json_decode(file_get_contents("php://input"),true);
		/*if(!isset($data['competitor'])){
			$competitor = array();
			$itemid = array();
			foreach($data['itemid'] as $value){
				$competitor[] = $value['CompID'];
				$itemid[] = array('value'=>$value['ItemID']);
			}
			$competitor_master_result		= $this->RawlistModel->add_to_masterlist_model($competitor[0], $itemid);
			echo json_encode($competitor_master_result);
		}*/
		//else{
			if(isset($data['competitor'])) $competitor = $data['competitor'];
			if(isset($data['itemid']))	   $itemid = $data['itemid'];
			$username=  $this->session->userdata('username');
			$roleid=  $this->session->userdata('roleid');
			$competitor_master_result		= $this->RawlistModel->add_to_masterlist_model($competitor, $itemid,$username,$roleid);
			echo json_encode($competitor_master_result);
		//}	
	}
	
	public function search_keywords_ebay() {		
		$this->keysnew->keys_function();
		$pagecount_result = $this->RawlistModel->pagecountmodel();
		$pagecount = $pagecount_result[0]['pagecount'];
		/*echo "<pre>";
		print_r($pagecount);
		exit;*/
		//$pagecount = 50;

		$data = json_decode(file_get_contents("php://input"),true);
		$applyfilter_checkbox = '';
		if($data['first'] == ''){
			$applyfilter_checkbox = 0;
		}
		else{
			$applyfilter_checkbox = 1;
		}
		// echo $applyfilter_checkbox;
		// print_r($data['second']);
		// exit;
		$allflag = 0;
		if ($data['second'][3]['value'] != '')
		{
			$pagecount = $data['second'][3]['value'];
		}
		if ($data['second'][3]['value'] == '0')
		{
			$pagecount = 101;
			$allflag = 1;
		}

		$entrynodb = '';
		if($data['second'][0]['value'] != '')
		{
			$id = trim($data['second'][1]['value']);
			$keyid = trim($data['second'][0]['value']);

			if ($keyid != '' ) {
				$res1 = $this->RawlistModel->checkebaydetails($keyid,$id);
				$runflag = $res1[0]['flag'];
				$entrynodb = $res1[0]['entrycount'];
				if ($runflag != 1)
				{	
					$truqry = "DELETE FROM dive_ebay_search_details WHERE keyvalue = '".$keyid."' ";
					$this->db->query($truqry);
				}
				if ($runflag == 1)
				{	
					if ($entrynodb < $pagecount)
					{
						$truqry = "DELETE FROM dive_ebay_search_details WHERE keyvalue = '".$keyid."' ";
						$this->db->query($truqry);
						$runflag = 0;
					}
					else
					{
						$runflag = 1;
					}
				}
			}
			
			
			
			if($id != '') {
				$strval = str_replace(" ", "+", $id);
				$strval = str_replace("&", "%26", $strval);
				$strval = str_replace(",", "%2C", $strval);
				$strval = str_replace("%", "%25", $strval);
			}
			
			$d = array('totcount' => '','runningcount' => '','itemId' => '','sellerName' => $id,'filtermsg' => '');
			echo json_encode(array('totcount' => '','runningcount' => '','itemId' => '','sellerName' => $id,'filtermsg' => ''));
			//.PHP_EOL 
		    ob_get_contents();
			flush();
			ob_flush();

			/*//pusher
			 $options = array(
				 		'cluster' => 'ap2',
					    'encrypted' => false
					  );
				 $pusher = new Pusher\Pusher(
					    '15355bcfc5ff30a1fbca',
					    '648a414fcf973c4d2523',
					    '382129',
					     $options
					  );
				  $data['message'] = $id;
				  $pusher->trigger('myebaychannel'.$_SESSION['__ci_last_regenerate'], 'myebayevent'.$_SESSION['__ci_last_regenerate'], $data);
			 //pusher end*/
				  /*echo "<pre>";
				  print_r(ob_get_contents());
				  exit;*/

			
            
			if ($runflag != 1)
			{
				if($id != '') {
					$strval = str_replace(" ", "+", $id);
					$strval = str_replace("&", "%26", $strval);
					$strval = str_replace(",", "%2C", $strval);
					$strval = str_replace("%", "%25", $strval);
				}
				error_reporting(E_ALL);  // Turn on all errors, warnings and notices for easier debugging
				//Seller Authorization Token
				$userAuthToken = 'AgAAAA**AQAAAA**aAAAAA**IriZWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ABmIOgCpmAog+dj6x9nY+seQ**P3gDAA**AAMAAA**dU7O4i8uhM7pzhMWjeixhzipmJX/pC3dELPQwS55dCQI+Cc4g+mH+0llOKAOKz/XCfFP6mUp/VH17VS1Okc70ygtu5czc5DKKgLWyocV9oeRpxLGwLS4SuQUXhPoDwTIt8uZw3N48QVvEHk5svvxTiQUoTCZN2K0FpIMzCDAyV87fsAe9XimqfG/mwVZkWG5MAgcyuUgislZj7j744sStlLLzxsk/NjJaLQnY+e1PqRsIZNrp/s7gWSFnYkeks4plpwM+SV+Co3vt1kG5oX7YCmfD44vtFQ8Qqc3XYty99WOoCFvDjpcWZkq55S89JA4dlJB1Jr8FE6r8uLkxNiRzGyYlfT5LfGshAFX15dJT5h6VeOO+VAUKQn+JVjoMSrHcmZkddO9+2yVCHEMRKYPS2C3r20xxz2iD0fBdj6QnL/Ds0E7YsEFljsVgR+KYMlGJm0rTaZCPFm7+D+UidQpgtFFeFV5OWJjCo1KnppLtAJfBZ0UgngIQDBli4AljSclV88qb32B8nJK5Fdekchx3vrJYqLsdsDUTxCk1Nkp4awFj7/iLDOpfm1M7wE5h4+vjGu+bsknGkNMB6nvJPNiGu26vcBMSW4bebckO3feLFNyk7PtUKkk1IMrIb6U66ZP4ctQ+FtIwOEj3U/C341M71bfpnR+U/wSO5HUPgLuK9vY/Z5qrCExBophwbDw1LHEcc0u/CufsTOgIT9i7nYNTpy0YyGoEx+P+GN1jZPykQjhurwP1vgHWt43JOw0+imI';

				//	$keywords = "Roll Up Ford F-250 F-350 6.75";
				//Endpoint URL to Use
				$nextpageflag = 1;
				for ($page = 1; $nextpageflag!= 0 ;$page++)
				{
					$url = 'http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=APAEngin-AutoInve-PRD-5bffbb2e5-df0267de&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD&keywords='.$strval.'&paginationInput.pageNumber='.$page.'&paginationInput.entriesPerPage='.$pagecount;

					//initialise a CURL session
					$connection = curl_init();
					//Set Endpoint URL 
					curl_setopt($connection, CURLOPT_URL, $url);

					//Set Method as GET; Bydefault it is Get, So it is OK if you do not set it if the call is Get call
					curl_setopt($connection, CURLOPT_HTTPGET, true);

					//Create Array of Headers
					$headers = array();
					$headers[] = $userAuthToken;
					$headers[] = 'X-EBAY-GLOBAL-ID:EBAY-US';

					//set the headers using the array of headers
					curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);

					//set it to return the transfer as a string from curl_exec
					curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);

					//stop CURL from verifying the peer's certificate
					curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

					//Send the Request
					$response = curl_exec($connection);
					$resultxml = simplexml_load_string($response);
					
					/*echo "<pre>";
					print_r($resultxml);
					exit;*/
					$responseDoc = new DomDocument();
					$responseDoc->loadXML($response);
					//close the connection
					curl_close($connection);
				
					//Check If Response is OK
					if (stristr($response, 'HTTP 404') || $response == '')
					die('<P>Error sending request, Please check the Request');
					$VAL = $resultxml->searchResult;
					$cndfv = $VAL['count'];
					$res = $VAL->item;

					$totalPages = (string)$resultxml->paginationOutput->totalPages;
					$totalEntries = (string) $resultxml->paginationOutput->totalEntries;
					$pageNumber = (string) $resultxml->paginationOutput->pageNumber;

					$compatabilityLevel = 535;
					$devID = '098ea620-f6a0-4a98-9d26-855a804c572d';   // these prod keys are different from sandbox keys
					$appID = 'APAEngin-AutoInve-PRD-5bffbb2e5-df0267de';
					$certID = 'PRD-bffbb2e55907-d466-4dbd-915f-694c';
					//set the Server to use (Sandbox or Production)
					$serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
					//the token representing the eBay user to assign the call with
					$userToken = 'AgAAAA**AQAAAA**aAAAAA**IriZWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ABmIOgCpmAog+dj6x9nY+seQ**P3gDAA**AAMAAA**dU7O4i8uhM7pzhMWjeixhzipmJX/pC3dELPQwS55dCQI+Cc4g+mH+0llOKAOKz/XCfFP6mUp/VH17VS1Okc70ygtu5czc5DKKgLWyocV9oeRpxLGwLS4SuQUXhPoDwTIt8uZw3N48QVvEHk5svvxTiQUoTCZN2K0FpIMzCDAyV87fsAe9XimqfG/mwVZkWG5MAgcyuUgislZj7j744sStlLLzxsk/NjJaLQnY+e1PqRsIZNrp/s7gWSFnYkeks4plpwM+SV+Co3vt1kG5oX7YCmfD44vtFQ8Qqc3XYty99WOoCFvDjpcWZkq55S89JA4dlJB1Jr8FE6r8uLkxNiRzGyYlfT5LfGshAFX15dJT5h6VeOO+VAUKQn+JVjoMSrHcmZkddO9+2yVCHEMRKYPS2C3r20xxz2iD0fBdj6QnL/Ds0E7YsEFljsVgR+KYMlGJm0rTaZCPFm7+D+UidQpgtFFeFV5OWJjCo1KnppLtAJfBZ0UgngIQDBli4AljSclV88qb32B8nJK5Fdekchx3vrJYqLsdsDUTxCk1Nkp4awFj7/iLDOpfm1M7wE5h4+vjGu+bsknGkNMB6nvJPNiGu26vcBMSW4bebckO3feLFNyk7PtUKkk1IMrIb6U66ZP4ctQ+FtIwOEj3U/C341M71bfpnR+U/wSO5HUPgLuK9vY/Z5qrCExBophwbDw1LHEcc0u/CufsTOgIT9i7nYNTpy0YyGoEx+P+GN1jZPykQjhurwP1vgHWt43JOw0+imI';
	
					if(count($res) > 0) {
						$runcount = 1;

						foreach($res as $resultval) {
							$subtitle = '';
							$itemId = $resultval->itemId;
							$title = $resultval->title;
							$topRatedListing = $resultval->topRatedListing;
							$categoryId = $resultval->primaryCategory->categoryId;
							$categoryName = $resultval->primaryCategory->categoryName;
							$shippingType = $resultval->shippingInfo->shippingType;
							if(isset($resultval->subtitle)) $subtitle = $resultval->subtitle;
							$siteID = 0;
							//the call being made:
							$verb = 'GetItem';
							
							///Build the request Xml string
							$requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
							$requestXmlBody .= '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
							$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
							$requestXmlBody .= "<IncludeItemSpecifics>true</IncludeItemSpecifics>";
							$requestXmlBody .= "<ItemID>$itemId</ItemID>";
							$requestXmlBody .= '</GetItemRequest>';
							
							//Create a new eBay session with all details pulled in from included keys.php
							$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);
							
							//send the request and get response
							$responseXml = $session->sendHttpRequest($requestXmlBody);
							if(stristr($responseXml, 'HTTP 404') || $responseXml == '')
								die('<P>Error sending request');
							
							//Xml string is parsed and creates a DOM Document object
							$responseDoc = new DomDocument();
							$responseDoc->loadXML($responseXml);
							$xml = simplexml_load_string($responseXml);
						/*	echo "<pre>"; print_r($xml); 
							exit();*/
							$resultArray = $xml->Item;
							$Quantity = $resultArray->Quantity;
							$SKU = ''; $NameValueList = array();
							if(isset($resultArray->SKU)) $SKU = $resultArray->SKU;
							$StartTime = $resultArray->ListingDetails->StartTime;
							$EndTime = $resultArray->ListingDetails->EndTime;
							$sellerName = $resultArray->Seller->UserID;
							$SellerLevel = $resultArray->Seller->SellerInfo->SellerLevel;
							$TopRatedSeller = $resultArray->Seller->SellerInfo->TopRatedSeller;
							$ConverCurPrice = $resultArray->SellingStatus->ConvertedCurrentPrice;
							$CurrentPrice = $resultArray->SellingStatus->CurrentPrice;
							$MinimumToBid = $resultArray->SellingStatus->MinimumToBid;
							$QuantitySold = $resultArray->SellingStatus->QuantitySold;
							$startdate = $resultArray->ListingDetails->StartTime;
							$datetm = str_replace("T"," ",$startdate);
							$datetm = str_replace(".000Z","",$datetm);
							$listedon = DateTime::createFromFormat('Y-m-d H:i:s',$datetm);
							$listingdate = $listedon->format('Y-m-d H:m:s');
							$ListingStatus = $resultArray->SellingStatus->ListingStatus;
							$ViewItemURL = $resultArray->ListingDetails->ViewItemURL;
							
							$shipping = $servicecost = '';
							$itemcountry = $resultval->country;
							$itemLocation = $resultval->location;
							$ConditionID = $resultval->condition->conditionId;
							$ConditionDisplayName = $resultval->condition->conditionDisplayName;
							
							$ShippingService = $resultArray->ShippingDetails->ShippingServiceOptions;

							if (count($ShippingService) > 0) {
								$servicecost  = $ShippingService->ShippingServiceCost;
								$shipping = $ShippingService->FreeShipping;
							} else {
								$servicecost = $ShippingService->ShippingServiceCost;
							}
							
							if ($servicecost > 0) {
								$shipping = 'false';
							} else {
								$shipping = 'True';
							}
				
							$itemLocation = str_replace("'", "\'" ,$itemLocation);
							$itemLocation = str_replace('"', '\"' ,$itemLocation);
							
							$SKU = str_replace("'", "\'" ,$SKU);
							$SKU = str_replace('"', '\"' ,$SKU);
						
							$categoryName = str_replace("'", "\'" ,$categoryName);
							$categoryName = str_replace('"', '\"' ,$categoryName);
							
							$title = str_replace("'", "\'" ,$title);
							$title = str_replace('"', '\"' ,$title);
							
						    $displayname = substr("$sellerName",0,1)."*****". substr("$sellerName",-1).substr (rand(),0,2);
							
							$insertqry = "INSERT INTO dive_ebay_search_details (Competitor,DisplayName, ItemID, sku, Title, Category, CategoryID, Quantity, CurrentPrice, QuantitySold, ListingStatus,
											ItemURL, ListingDate, country, location, freeshipping, ShippingCost, ConditionName, ConditionID, SellerLevel, SearchKeyword, keyvalue, RecordDate) 
											values ('".$sellerName."','".$displayname."', '".$itemId."', '".$SKU."', '".$title."', '".$categoryName."', '".$categoryId."', '".$Quantity."', '".$CurrentPrice."',
											'".$QuantitySold."', '".$ListingStatus."', '".$ViewItemURL."', '".$listingdate."', '".$itemcountry."', '".$itemLocation."', '".$shipping."', 
											'".$servicecost."', '".$ConditionDisplayName."', '".$ConditionID."',	'".$SellerLevel."', '".$id."', '".$keyid."', NOW())";
							/*//pusher
							 $options = array(
								 		'cluster' => 'ap2',
									    'encrypted' => false
									  );
								 $pusher = new Pusher\Pusher(
									    '15355bcfc5ff30a1fbca',
									    '648a414fcf973c4d2523',
									    '382129',
									     $options
									  );
								    $data['total'] = count($res);
								    $data['count'] = $runcount;
								   $data['itemId'] = (array)$itemId;
								    $data['sellerName'] = (array)$sellerName;
								    $runcount++;
								 $pusher->trigger('myebaychannel'.$_SESSION['__ci_last_regenerate'], 'myebayevent'.$_SESSION['__ci_last_regenerate'], $data);
							 //pusher end*/

							 $totcount = count($res);
							 $runningcount = $runcount;
							 $itemId = (array)$itemId ;

							/* $sellerName = (array)$sellerName;*/
							$ref =$this->RawlistModel->get_title();
						      /*  $mainmenu_data['title']= $ref;*/
						        
						     $ShowMasked= $ref[0]->ShowMasked;
							 if ($ShowMasked == 1)
	                          {
	                          
	                          	$sellerName = (array)$displayname;
	                          }
	                          else
	                          {
	                          	
	                          	$sellerName = (array)$sellerName;
	                          }
	                         /* echo"<pre>";
	                          print_r($sellerName);
	                          exit;*/
							 
	                          $runcount++;


							 //currently working
							 //$data = array('totcount' => $totcount,'runningcount' => $runningcount,
							 	//'itemId' => $itemId[0],'sellerName' => $sellerName[0],'filtermsg' => '');
							 //print_r(array($totcount,$runningcount,$itemId[0],$sellerName[0]));
	                         if ($allflag  != 1)
	                         {
							 	$totalpage = $pageNumber;
							 }
							 else
							 {
							 	$totalpage = $totalPages;	
							 }

							 echo json_encode(array('totcount' => $totcount,'runningcount' => $runningcount,
							 	'itemId' => $itemId[0],'sellerName' => $sellerName[0],'filtermsg' => '','totalPages' => $totalPages,'totalEntries' => $totalEntries, 'totalpagecount' => $totalpage,'pageNumber' => $pageNumber,'keyid'=> $keyid,'checkboxval' => $applyfilter_checkbox));
							 //.PHP_EOL
						     ob_get_contents();
							 flush();
							 ob_flush();
							 $result_query = $this->db->query($insertqry);
						}
					}
					if ($allflag != 1 || $pageNumber ==  $totalpage || $pagecount == 100)
					{
						$nextpageflag = 0;
					}
				}
					if($applyfilter_checkbox == 1){
						/*$filtermsg ='Applying selected Filters';
						$d = array('totcount' => '','runningcount' => '',
						 	'itemId' => '','sellerName' => '','filtermsg' => $filtermsg);
						 echo json_encode($d).PHP_EOL;
					     ob_flush();
						 flush();*/
					}
			}			
		}

		$search_result_header 	= $this->RawlistModel->rawlist_search_header($keyid,$applyfilter_checkbox);
		$search_result 	= $this->RawlistModel->rawlist_search_details($keyid,$applyfilter_checkbox);
		$typeconvert = array();
		if(count($search_result) >= 1){
			$runcount=1;
			foreach($search_result as $value){
				$typeconvert[] = array(
										  'ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'category'=>$value->category,
										  'CategoryID'=>$value->CategoryID,
										  'CurrentPrice'=>round($value->CurrentPrice,2),
										  'QuantitySold'=>round($value->QuantitySold,2),
										  'lastweek_Qtysold'=>round($value->lastweek_Qtysold),
										  'TotalSales'=>round($value->TotalSales),
										  'ItemURL'=>$value->ItemURL,
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,
										  'CompID'=>round($value->CompID),
										  'Mlflag'=>$value->Mlflag,
										  'ListingDate'=>$value->ListingDate
									  );
				/*if($applyfilter_checkbox == 1){
					$options = array(
								 		'cluster' => 'ap2',
									    'encrypted' => false
									  );
								 $pusher = new Pusher\Pusher(
									    '15355bcfc5ff30a1fbca',
									    '648a414fcf973c4d2523',
									    '382129',
									     $options
									  );
								    $data['filtermsg'] = 'Applying selected Filters';
									$pusher->trigger('myebaychannel'.$_SESSION['__ci_last_regenerate'], 'myebayevent'.$_SESSION['__ci_last_regenerate'], $data);
					}*/
					
			}	
		}
		 $data['itemdetail'] 			= $search_result_header;
		 $data['rawdetail'] 				= $typeconvert;	
		 echo json_encode($data);	
		//echo json_encode(array('itemdetail'=> $search_result_header,'rawdetail'=> $typeconvert));
		//echo $result;
	}


	public function canceldataget()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		$search_result_header 	= $this->RawlistModel->rawlist_search_header($data['keyid'],$data['applyfilter_checkbox']);
		$search_result 	= $this->RawlistModel->rawlist_search_details($data['keyid'],$data['applyfilter_checkbox']);
		$typeconvert = array();
		if(count($search_result) >= 1){
			$runcount=1;
			foreach($search_result as $value){
				$typeconvert[] = array(
										  'ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'Title'=>$value->Title,
										  'category'=>$value->category,
										  'CategoryID'=>$value->CategoryID,
										  'CurrentPrice'=>round($value->CurrentPrice,2),
										  'QuantitySold'=>round($value->QuantitySold,2),
										  'lastweek_Qtysold'=>round($value->lastweek_Qtysold),
										  'TotalSales'=>round($value->TotalSales),
										  'ItemURL'=>$value->ItemURL,
										  'mypartno'=>$value->mypartno,
										  'CompetitorName'=>$value->CompetitorName,
										  'CompID'=>round($value->CompID),
										  'Mlflag'=>$value->Mlflag,
										  'ListingDate'=>$value->ListingDate
									  );
				/*if($applyfilter_checkbox == 1){
					$options = array(
								 		'cluster' => 'ap2',
									    'encrypted' => false
									  );
								 $pusher = new Pusher\Pusher(
									    '15355bcfc5ff30a1fbca',
									    '648a414fcf973c4d2523',
									    '382129',
									     $options
									  );
								    $data['filtermsg'] = 'Applying selected Filters';
									$pusher->trigger('myebaychannel'.$_SESSION['__ci_last_regenerate'], 'myebayevent'.$_SESSION['__ci_last_regenerate'], $data);
					}*/
					
			}	
		}
		 $data['itemdetail'] 			= $search_result_header;
		 $data['rawdetail'] 				= $typeconvert;
		 echo json_encode($data);
	}

	public function export() {
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
			$competitor_category_result 	= $this->RawlistModel->rawlist_details_model($data['competitor'],$data['category'], $data['chkvalue'], $data['chkkey']);
			$competitor_category_result1 	= $this->RawlistModel->raw_item_detail_model($data['competitor'],$data['category'], $data['chkvalue'], $data['chkkey']);	
			foreach($competitor_category_result as $value){
				$competitor_category[] = array(
										  'CompetitorName'=>$value->CompetitorName,
										  'ItemID'=>round($value->ItemID,2),
										  'SKU'=>$value->SKU,
										  'mypartno'=>$value->mypartno,
										  'Title'=>$value->Title,
										  'CurrentPrice'=>round($value->CurrentPrice,2),
										  'ListingDate'=>$value->ListingDate,
										  'QuantitySold'=>round($value->QuantitySold,2),
										  'lastweek_Qtysold'=>round($value->lastweek_Qtysold)
										 );
			}
		}
		$heading=['Competitor','Item ID','SKU','My Part#','Title','Price (In $)','Listing Date','Qty.Sold (Inc)','Qty.Sold (Last Wk)'];
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',9);		
		foreach($heading as $column_heading)
			{
				$pdf->Cell(32,12,$column_heading,1);
			}
		foreach($competitor_category as $row) {
			$pdf->SetFont('Arial','',9);	
			$pdf->Ln();
			foreach($row as $column)
				$pdf->Cell(32,12,$column,1);
		}
		$pdf->Output();
	}
}

