<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AmazonResearch extends CI_Controller {

	public $parameters = array();

	public function __construct()
	{
  		parent::__construct();
   		$this->load->database();
   		$this->load->helper( 'Metrics_helper' );
   		$this->load->library('Screenname');
   		$this->load->model('AmazonResearchModel');
		$this->load->model('MenuModel');
		$this->load->library('Screenname');
		$this->load->library('AmazonConfig');
   		ini_set('memory_limit', '256M');
   		ini_set('max_execution_time', 30000);
   	}

	public function index()
	{	
		$getmenu = $this->screenname->getscreen();
	    $router =& load_class('Router', 'core');
	    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
	    $title = '';
	    foreach($getmenu['url'] as $key => $value){
	      if($value == $indexroute){
	        $title = $key;
	      }
	    }		
		$mainmenu_data['icononly']  = $getmenu['icononly'];
	    $mainmenu_data['url']       = $getmenu['url'];
	    $mainmenu_data['screen']     = $title;
	    $mainmenu_data['menu_data'] = $getmenu['menu_data'];

		$ref =$this->AmazonResearchModel->get_title();
        $mainmenu_data['title']= $ref;
        $mainmenu_data['ShowMasked']= $ref[0]->ShowMasked;
        $this->load->view('AmazonResearchView',$mainmenu_data);
	}

	public function SearchAmazon()
	{
		$data = json_decode(file_get_contents("php://input"), true);
        $SearchResults = $this->amazonconfig->GetSearchKeywordResult($data['Keyword']);
        $Result = array();
        $count = 0;
        foreach ($SearchResults->Product as $value) {
			$Result[] = array('ASIN'=>(string)$value->Identifiers->MarketplaceASIN->ASIN,
							  'ListPrice'=>(float)$value->AttributeSets->ItemAttributes->ListPrice->Amount,
							  'Model'=>(string)$value->AttributeSets->ItemAttributes->Model,
							  'PartNo'=>(string)$value->AttributeSets->ItemAttributes->PartNumber,
							  'Category'=>(string)$value->AttributeSets->ItemAttributes->ProductGroup,
							  'Competitor'=>(string)$value->AttributeSets->ItemAttributes->Publisher,
							  'Title'=>(string)$value->AttributeSets->ItemAttributes->Title,
							  'ImageURL'=>(string)$value->AttributeSets->ItemAttributes->SmallImage->URL
							);
			$count +=1;
		}
		$Result['ItemCount'] = $count;
		echo json_encode($Result);
	}
}

