<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

  public function __construct(){
      parent::__construct();
      $this->load->database(); 
      $this->load->model('ReportsModel');
      $this->load->library('excel');
      $this->load->library('session');
      $this->load->model('CompetitorModel');
      $this->load->model('MenuModel');
      $this->load->library('Screenname');
      ini_set('memory_limit', '-1');
      set_time_limit(0);
  }

    public function index()
  { 
      $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
        $competitor_combo     = $this->ReportsModel->get_reports_competitorcombo();
        $mainmenu_data['compidandname']  = $competitor_combo;
       
         $ref =$this->ReportsModel->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('reportsview',$mainmenu_data);
        }
        

  public function generate_report(){
  	$convert = explode(",",$_POST['competitorname']);
    if($convert[0] == '' && $_POST['weekno'] == ''){
        $this->session->set_userdata('validationmsg', 'Kindly Select Competitor Name and Week No');
        redirect('Reports/index');
        return false;
    }
    else if($_POST['competitorname'] == ''){
        $this->session->set_userdata('validationmsg', 'Kindly Select Competitor Name');
        redirect('Reports/index');
        return false;
    }
    else if($_POST['weekno'] == ''){
        $this->session->set_userdata('validationmsg', 'Kindly Select Week No');
        redirect('Reports/index');
        return false;
    }

    $date = new DateTime($_POST['weekno']);
    $week1date         = $date->format('Y-m-d');
    $weeknumber        = $date->format('W');
    $weekdate        = $week1date;
   	
   	$datesplit = $_POST['weekno'];
  	$result = explode("/",$datesplit);
	$postdate = $result[2]."/".$result[0]."/".$result[1]; 
    $currentWeekNumber = date('W');
    $first             = date('W',strtotime($result[2]."/".$result[0]."/".$result[1]." - 7 day"));
    $second            = date('W',strtotime($result[2]."/".$result[0]."/".$result[1]." - 14 day"));
    $third             = date('W',strtotime($result[2]."/".$result[0]."/".$result[1]." - 21 day"));
    $fourth            = date('W',strtotime($result[2]."/".$result[0]."/".$result[1]." - 28 day"));
    $date 		         = strtotime($date->format('d-M-Y'));
   	$firstdate         = strtotime("-0 day", $date);
   	$seconddate        = strtotime("-7 day", $date);
   	$thirddate         = strtotime("-14 day", $date);
   	$fourthdate        = strtotime("-21 day", $date);
   	$fifthdate         = strtotime("-28 day", $date);
   	$sixthdate         = strtotime("-35 day", $date);
   	$firestdate        = date('d-M', $firstdate);
   	$seconddate        = date('d-M', $seconddate);
   	$thirddate         = date('d-M', $thirddate);
   	$fourthdate        = date('d-M', $fourthdate);
   	$fifthdate         = date('d-M', $fifthdate);
   	$sixthdate         = date('d-M', $sixthdate);

    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('CBR Report ');
    $this->excel->getActiveSheet()->setCellValue('A1', 'Country Excel Sheet');
    $this->excel->getActiveSheet()->setCellValue('A1', 'ItemId');
    $this->excel->getActiveSheet()->setCellValue('B1', 'SKU');
    $this->excel->getActiveSheet()->setCellValue('C1', 'Title');
    $this->excel->getActiveSheet()->setCellValue('D1', 'Category');
    $this->excel->getActiveSheet()->setCellValue('E1', 'CurrentPrice (in $) as on Week '.$currentWeekNumber);
    $this->excel->getActiveSheet()->setCellValue('F1', 'ItemURL');
    $this->excel->getActiveSheet()->setCellValue('G1', 'QTY Sold Since Inc as on Week'.$currentWeekNumber);
    /*$this->excel->getActiveSheet()->setCellValue('H1', 'QTY Sold '.$seconddate);
    $this->excel->getActiveSheet()->setCellValue('I1', 'QTY Sold '.$thirddate);
    $this->excel->getActiveSheet()->setCellValue('J1', 'QTY Sold '.$fourthdate);
    $this->excel->getActiveSheet()->setCellValue('K1', 'QTY Sold '.$fifthdate);
    $this->excel->getActiveSheet()->setCellValue('L1', 'QTY Sold '.$sixthdate);*/
    $this->excel->getActiveSheet()->setCellValue('H1', 'Week '.$weeknumber.' '.'('.$firestdate.')');
    $this->excel->getActiveSheet()->setCellValue('I1', 'Week '.$first.' '.'('.$seconddate.')');
    $this->excel->getActiveSheet()->setCellValue('J1', 'Week '.$second.' '.'('.$thirddate.')');
    $this->excel->getActiveSheet()->setCellValue('K1', 'Week '.$third.' '.'('.$fourthdate.')');
    $this->excel->getActiveSheet()->setCellValue('L1', 'Week '.$fourth.' '.'('.$fifthdate.')');
    $this->excel->getActiveSheet()->getStyle("A1:G1")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'b76e79')));
    $this->excel->getActiveSheet()->getStyle("H1:L1")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'c4d79b')));
    for($col = ord('A'); $col <= ord('L'); $col++){ 
        $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    }
    $report     = $this->ReportsModel->report_generate_model($convert[0],$weekdate);
    if(count($report) >= 1){
        $exceldata="";
        $row = 2;
        $swap = '';
        foreach ($report as $key => $value){

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, ' '.$value['ItemID'].' ');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $value['SKU']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $value['Title']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $value['Category']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $value['CurrentPrice']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $value['ItemURL']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $value['QW6']);
            /*$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $value['QW4']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $value['QW3']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $value['QW2']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $value['QW1']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $value['QW0']);*/
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $value['Week5']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $value['Week4']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $value['Week3']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $value['Week2']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $value['Week1']);

            if($key%2 !== 0)
            {
              $this->excel->getActiveSheet()->getStyle("A".$row.':L'.$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'FFC0CB')));
            }
            if($value['Week5'] > 0){
                 $this->excel->getActiveSheet()->getStyle("H".$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'c4d79b')));
                 //1a6600                                     
            }
            if($value['Week4'] > 0){
                 $this->excel->getActiveSheet()->getStyle("I".$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'c4d79b')));
            }
            if($value['Week3'] > 0){
                 $this->excel->getActiveSheet()->getStyle("J".$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'c4d79b')));
            }
            if($value['Week2'] > 0){
                 $this->excel->getActiveSheet()->getStyle("K".$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'c4d79b')));
            }
            if($value['Week1'] > 0){
                 $this->excel->getActiveSheet()->getStyle("L".$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'c4d79b')));
            }
            $row++;
        }

        $date = new DateTime($_POST['weekno']);
        $filename = 'CBR_'.$convert[1].'_'.$date->format('d').'_'.$date->format('M').'_'.$date->format('Y').'.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8' );
        //header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');                      
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
        $objWriter->save('php://output');
    }else{
         //$this->session->set_userdata('validationmsg', 'Excel Sheet Population Problem please try Again.');
         redirect('Reports/index');
         return false;
    }
  }
}
