<?php
defined ('BASEPATH') OR exit('No direct script access allowed');

class Amz_ConfigExplore extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
        $this->load->model('Manage_User_Model'); 
        $this->load->model('MenuModel'); 
        $this->load->model('Amz_ConfigExploreModel');  
        $this->load->library('Screenname');   
        $this->load->database();
        $this->load->library('AmazonConfig');
    }

    public function index(){   
        $getmenu    = $this->screenname->getscreen();
        $router     =& load_class('Router', 'core');
        $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
        $title      = '';
        foreach($getmenu['url'] as $key => $value){
          if($value == $indexroute){
            $title = $key;
          }
        }
        $mainmenu_data['icononly']   = $getmenu['icononly'];
        $mainmenu_data['url']        = $getmenu['url'];
        $mainmenu_data['screen']     = $title;
        $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    	$ref =$this->Manage_User_Model->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('Amz_ConfigExploreView',$mainmenu_data);
    }

    public function LoadData(){
        $result= $this->Amz_ConfigExploreModel->LoadData();
         $res_data = array();
       foreach($result as $value){
        $res_data[] = array('ASIN'=>$value->ASIN,
                      'Title'=>$value->Title,
                      'SKU'=>$value->SKU,
                      'ListPrice'=>round($value->ListPrice,2),
                      'CurrentPrice'=>round($value->CurrentPrice,2),
                      'Keywords'=>$value->Keywords
                     );

      }
        echo json_encode($res_data);
    }
     public function ApiSearch()
    {
        $data = json_decode(file_get_contents("php://input"), true);

       //GetSearchKeywordResult
        $SearchResult = $this->amazonconfig->GetSearchKeywordResult($data['Keyword']);
        if(count($SearchResult) > 0){
           $Keyword_res = array(); 
        foreach ($SearchResult->Product as $value) {
          $Keyword_res[]= (string)$value->Identifiers->MarketplaceASIN->ASIN;
        }

        
       
        //GetPriceResult
        $SearchPriceResult = $this->amazonconfig->GetPriceResult($Keyword_res);
        $Price_res = array(); 
        foreach ($SearchPriceResult->GetCompetitivePricingForASINResult as $value) {
          $Price_res[(string)$value->Product->Identifiers->MarketplaceASIN->ASIN]= array(
            'LandedPrice'=>(float)$value->Product->CompetitivePricing->CompetitivePrices->CompetitivePrice->Price->LandedPrice->Amount,
            'ASIN'=>(string)$value->Product->Identifiers->MarketplaceASIN->ASIN,
            'Shipping'=>(float)$value->Product->CompetitivePricing->CompetitivePrices->CompetitivePrice->Price->Shipping->Amount
            );
                    
        }

        //Get Competitor Cost & Shipping Price and Over all SearchResult Insert into dive_amz_cron_details(table)
        $Result_grid = array();
        foreach ($SearchResult->Product as  $search_value) {          
          $count = 0;
          foreach ($Price_res as $pr_key => $pr_value) { 
            if($search_value->Identifiers->MarketplaceASIN->ASIN == $pr_key){
                  $CategoryRank = '';
                  $OverallCategoryRank = '';
                if(count($search_value->SalesRankings->SalesRank) > 1){
                    $OverallCategoryRank = (string)$search_value->SalesRankings->SalesRank[0]->Rank;
                    $CategoryRank = (string)$search_value->SalesRankings->SalesRank[1]->Rank;
                }
                else if(count($search_value->SalesRankings->SalesRank) == 1){
                  $CategoryRank = (string)$search_value->SalesRankings->SalesRank[0]->Rank;
                  $OverallCategoryRank = '';
                }
                    $Result_grid[] = array(
                                        'ASIN'=>(string)$search_value->Identifiers->MarketplaceASIN->ASIN,
                                        'List_Price' =>round($search_value->AttributeSets->ItemAttributes->ListPrice->Amount,2),
                                        'PartNo'=>(string)$search_value->AttributeSets->ItemAttributes->PartNumber,
                                        'Category'=>(string)$search_value->AttributeSets->ItemAttributes->ProductGroup,
                                        'Competitor'=>(string)$search_value->AttributeSets->ItemAttributes->Brand,
                                        'Title'=>(string)$search_value->AttributeSets->ItemAttributes->Title,
                                        'ImageURL'=>(string)$search_value->AttributeSets->ItemAttributes->SmallImage->URL,
                                        'Current_Price'=>round($pr_value['LandedPrice'],2),
                                        'OverallCategoryRank'=>round($OverallCategoryRank),
                                        'CategoryRank'=>round($CategoryRank)
                                      );
            }
             $count +=1;
          }
        }
        echo json_encode(array(array('resultdata'=>$Result_grid),array('resultcount'=>$count)));
      }
    }
     public function imp_save(){
        $data = json_decode(file_get_contents("php://input"), true);
        $result = $data['New_Content'];
      /*  echo"<pre>";
        print_r($result);
        exit;*/
        foreach ($result as $value) {
            if ($value['ASIN'] != 'ASIN#' && $value['ASIN'] != 'ASIN' && $value['ASIN'] != '' && $value['ListPrice'] != 'ListPrice'
                && $value['SKU'] != '' && $value['SKU'] != 'SKU')
            {
           
                $ASIN = $value['ASIN'];
                $Title= $value['Title'];
                $SKU  = $value['SKU'];
                $ListPrice= $value['ListPrice'];
                $CurrentPrice= $value['CurrentPrice'];
                $Keywords = str_replace('"', '', $value['Keywords']);
                $this->Amz_ConfigExploreModel->save($ASIN,$Title,$SKU,$ListPrice,$CurrentPrice,$Keywords);
               
        }
    }
     $this->index();
}
 public function SaveSearch(){
     $data = json_decode(file_get_contents("php://input"), true);
     $Keyword  = $data['Keyword'];
     $ASIN  = $data['ASIN'];
     $this->Amz_ConfigExploreModel->SaveSearch($ASIN,$Keyword);
      

 }
 }
?>
        
        
        
