<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salestrends extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database();
   		$this->load->helper( 'Metrics_helper' ); 
   		$this->load->model('SalestrendsModel');
   		$this->load->model('RawlistModel');
   		$this->load->model('CompetitorModel');
   		$this->load->model('MenuModel');
   		$this->load->library('Screenname');
   		ini_set('memory_limit', '256M');
	}

	  public function index()
	{	
	$getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $mainmenu_data['comptitornameandid'] = $this->SalestrendsModel->get_competitor_model();
	$ref =$this->SalestrendsModel->get_title();
    $mainmenu_data['title']= $ref;
	$this->load->view('salestrendsview',$mainmenu_data);
		
       
	}

	public function competitor_category(){
		$data = json_decode(file_get_contents("php://input"), true);
		$competitor_category_result = [];
		if ($data['competitor']){
		$competitor_category_result 	= $this->SalestrendsModel->get_competitor_category($data['competitor']);

		


	     
		}
		echo json_encode($competitor_category_result);

		

	}

	public function salestrends_detail_control(){
		$data = json_decode(file_get_contents("php://input"),true);
		$date_array = explode("/",$data['reportdate']);
		$var_day = $date_array[0]; //day seqment
		$var_month = $date_array[1]; //month segment
		$var_year = $date_array[2]; //year segment
		$new_date_format = "$var_year-$var_day-$var_month";
		$competitor_category_result = [];
		if($data['competitor']){
		  	$competitor_category_result		= $this->SalestrendsModel->get_salestrends_detail_model($data['competitor'],$data['category'],$new_date_format);
		  	$competitor_category_result1	= $this->SalestrendsModel->get_totsal_detail_model($data['competitor'],$data['category'],$new_date_format);
            $competitor_category = array();
			foreach($competitor_category_result as $value){
				$competitor_category[] = array('itemid' => $value->itemid,
												'sku' => $value->sku,
												'title' => $value->title,
												'mypartno' => $value->mypartno,
												'category' => $value->category,
												'pricew1' => $value->pricew1,
												'pricew2' => $value->pricew2,
												'pricew3' => $value->pricew3,
												'pricew4' => $value->pricew4,
												'pricew5' => $value->pricew5,
												'salesw1' => $value->salesw1,
												'salesw2' => $value->salesw2,
												'salesw3' => $value->salesw3,
												'salesw4' => $value->salesw4,
												'salesw5' => $value->salesw5,
												'pricevw1' => round($value->pricevw1, 2),
												'pricevw2' => round($value->pricevw2, 2),
												'pricevw3' => round($value->pricevw3, 2),
												'pricevw4' => round($value->pricevw4, 2),
												'pricevw5' => round($value->pricevw5, 2),
												'qtysoldvw1' => $value->qtysoldvw1,
												'qtysoldvw2' => $value->qtysoldvw2,
												'qtysoldvw3' => $value->qtysoldvw3,
												'qtysoldvw4' => $value->qtysoldvw4,
												'qtysoldvw5' => $value->qtysoldvw5,
												'price' => round($value->price, 2),
												'qtysold' => round($value->qtysold),
												'totalsales' => round($value->totalsales, 2),
												'itemURL' => $value->itemURL,
												'listingdate' => $value->listingdate,
												'compid' => $value->compid
										 );

			}
	
            $temp= $competitor_category_result1[0]['TotalSales'];
	        $competitor_category_result1[0]['TotalSales'] = test_method($temp);
		  	$data['totsaldetail'] 			=   $competitor_category_result1;
            $data['salestrenddetail'] 			= $competitor_category;
		
		}
		echo json_encode($data);
	}

}