<?php
defined ('BASEPATH') OR exit('No direct script access allowed');

class Amz_AutoExplore extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
        $this->load->model('Manage_User_Model'); 
        $this->load->model('MenuModel'); 
         $this->load->model('Amz_AutoExplore_Model'); 
        $this->load->library('Screenname');   
        $this->load->database();
    }

    public function index(){   
        $getmenu    = $this->screenname->getscreen();
        $router     =& load_class('Router', 'core');
        $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
        $title      = '';
        foreach($getmenu['url'] as $key => $value){
          if($value == $indexroute){
            $title = $key;
          }
        }
        $mainmenu_data['icononly']   = $getmenu['icononly'];
        $mainmenu_data['url']        = $getmenu['url'];
        $mainmenu_data['screen']     = $title;
        $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    	$ref =$this->Manage_User_Model->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('Amz_AutoExploreView',$mainmenu_data);
    }

    public function LoadData(){
        $ret= $this->Amz_AutoExplore_Model->load_data_model();
        $result_data = array();
        foreach ($ret as $value) {
            $result_data[] = array(
                'ASIN'=>$value->ASIN,
                'Title'=>$value->Title,
                'SKU'=>$value->SKU,
                'ListPrice'=>round($value->ListPrice,2),
                'Keywords'=>$value->Keywords,
                'CurrentPrice'=>round($value->CurrentPrice,2),
                'CompLowPrice'=>round($value->CompLowPrice,2),
                'RecommendPrice'=>round($value->RecommendPrice,2),
                'Profit'=>round($value->Profit,2)
                );
        }
     echo json_encode($result_data);
    }
    public function imp_save(){
        $data = json_decode(file_get_contents("php://input"), true);
        $result = $data['New_Content'];

        foreach ($result as $value) {
            if ($value['ASIN'] != 'ASIN#' && $value['Title'] != '' && $value['Title'] != 'Title' && $value['ListPrice'] != 'ListPrice'
                && $value['SKU'] != '' && $value['SKU'] != 'SKU')
            {
           
                $ASIN= $value['ASIN'];
                $Title= $value['Title'];
                $SKU= $value['SKU'];
                $ListPrice= $value['ListPrice'];
                $CurrentPrice= $value['CurrentPrice'];
                $CompLowPrice= $value['CompLowPrice'];
                $RecommendPrice= $value['RecommendPrice'];
                $Profit= $value['Profit'];
                $this->Amz_AutoExplore_Model->save($ASIN,$Title,$SKU,$ListPrice,$CurrentPrice,$CompLowPrice,$RecommendPrice,$Profit);
               
        }
    }
     $this->index();
}
}

?>
        