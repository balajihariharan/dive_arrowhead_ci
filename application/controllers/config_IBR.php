<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class config_IBR extends CI_Controller {

  public function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('MenuModel');
      $this->load->model('config_IBR_model');
       $this->load->library('Screenname');
        

      }
	public function index()
	{	
     $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
     $ref =$this->config_IBR_model->get_title();
        $mainmenu_data['title']= $ref;
    $this->load->view('config_IBR_view', $mainmenu_data);
     }
        
  public function compload() {
    $data = json_decode(file_get_contents("php://input"), true);
    $competitor_result  = $this->config_IBR_model->get_competitor($data['comp']);
    $data = array();
    foreach($competitor_result as $key => $value){
      $data[] = array('CompID'=>$value->CompID,'CompetitorName'=>$value->CompetitorName); 
    }
    echo json_encode($data);
  }

   public function get_data(){
        $data = json_decode(file_get_contents("php://input"), true);
        $ret= $this->config_IBR_model->get_datas();
        echo json_encode($ret);
   }


   public function competitor_category(){
    
    $data = json_decode(file_get_contents("php://input"), true);
    if ($data['competitor']){
      $categories   = $this->config_IBR_model->get_competitor_category($data['competitor']);
    }
    
    echo json_encode( $categories);
  }
  public function save(){  
    $data = json_decode(file_get_contents("php://input"), true);
    print_r($data);
    $compname = $data['compname'];
    $f1_flag=$data['firsttop_flag'];
    $s_flag=$data['secondtop_flag'];
    $list_flag= $data['listitems_flag'];
    $cat_flag= $data['Categories_flag'];
      
      if($f1_flag == 1)
        $firsttop =  $data['firsttop']; 
      else 
        $firsttop='';

     
      if($s_flag == 1)
        $secondtop =  $data['secondtop']; 
      else
        $secondtop ='';

      if($data['allitems'] == 1){
          $allitems =  $data['allitems'];
        }
        
        if($list_flag == 1){
          $listitems =  $data['listitems'];
                  }
        
      
      
      if( $cat_flag==1){

          $selectedcat =  $data['selectedcat'];
           }
     $username=  $this->session->userdata('username');
     $roleid =   $this->session->userdata('roleid');

           

      $result  = $this->config_IBR_model->get_ibr_insert($compname,$firsttop, $secondtop,$allitems, $listitems,
      $selectedcat, $f1_flag,$s_flag,$list_flag,$cat_flag,$username,$roleid);
     
      
      return $result;
      
      
  }
}
?>