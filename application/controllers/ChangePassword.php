<?php
class ChangePassword extends CI_Controller
{
	public function __construct()
	{
    	parent::__construct();
    	$this->load->model('ChangePassword_Model');
    	$this->load->library('session');
		//$this->load->helper('url');
  	}
	public function index()
	{
		$this->load->view('change_password_view');

	}
	public function change_password(){
		$data = json_decode(file_get_contents("php://input"),true);
		$pass = md5($data['passwrd']);
		$loginid = $data['loginid'];
		 $username      =  $this->session->userdata('username');
		$result = $this->ChangePassword_Model->change_password_model($pass,$loginid,$username);
		echo json_encode($result);
		$session_data = array(
        	'username'  => '',
        	'roleid'     => '',
        	'loginid' => '',
        	'compid' => '',
        	'cbrdate' => ''
		 );
		$this->session->unset_userdata($session_data);
		$this->session->sess_destroy();
		redirect('/Login/index', 'refresh');
		

	}
}