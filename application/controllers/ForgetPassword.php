<?php
class ForgetPassword extends CI_Controller
{
	public function __construct()
	{
    	parent::__construct();
    	$this->load->model('ForgetPassword_Model');
    	$this->load->library('session');
    	 $this->load->library('email');
		//$this->load->helper('url');
  	}
	public function index()
	{
		$this->load->view('Forget_Password_View');

	}
	public function check_email(){
		$data = json_decode(file_get_contents("php://input"),true);
		$emailid = $data['emailid'];
		$result = $this->ForgetPassword_Model->check_email_model($emailid);

		//mail
		
		$mail='';
		$mail.='<html><body>';
		$mail .= '<h1  style="color: #FF0000;"><img src="http://apaengineering.com/client_logos/dive_logo_small.jpg"></h1>';
		$mail .= '<table rules="all" border="1" cellpadding="10">';
   
	   	$mail.=' <thead style="height: 80px;font-size: 22px;text-align: left;color: #fff;">
	                    <tr style="background-color: rgb(65, 132, 243);">
	                        <th><strong>DIVE - Password Reset</strong></th>   
	                    </tr>
	              </thead>' ;
	        
		if(!empty($result)){
			$loginid = $result[0]['loginid'];
			$username = $result[0]['username'];
			echo json_encode($result);
			$url = base_url().'index.php/ChangePassword/index/'.$loginid;
			//$mail =base_url().'index.php/ChangePassword/index/'.$loginid;
			$mail.='<tr style="border:1px solid #000;"><td style="padding: 10px 20px 0px 20px; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px;  line-height: 18px; height: 200px; width: 600px;background: none repeat scroll 0 0 #f3f3f3;"><h3 style="
    		margin: 0 0 15px 0px;"><h2>Dear : '.$username.'</h2></h3>You have recently asked to reset the password for this DIVE account<br><p style="    border: 2px solid #4184f3; border-radius: 25px;width: 40%; padding: 0 50px 0 15px;">Login ID : '.$loginid.'</p><br>To Reset your password, click the link below<br><br><a href='.$url.'>Reset Password</a><br><br>Cheers<br>Team DIVE</td></tr>';
	        $mail.='</table><br><br><br>';
	        $mail.='</body></html>';
			$this->email->from('tech@apaengineering.com', 'DIVE - Reset Password'); 
		    $this->email->to($emailid);
		    $this->email->cc('meiyanathan.j@apaegineering.com');
		    $this->email->bcc('balaji.h@apaengineering.com');
		    $this->email->subject('DIVE - Reset Password'); 
		    $this->email->set_mailtype("html");
		    $this->email->message($mail);
		    $this->email->send();
			//echo "success";
		}
		else{
			echo 'failure';
		}
		//echo json_encode($result);
	}
}