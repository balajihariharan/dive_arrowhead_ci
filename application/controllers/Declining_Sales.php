<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Declining_Sales extends CI_Controller {

	public function __construct(){
  		parent::__construct();
   		$this->load->database();
   		$this->load->model('Declining_Sales_Model');
      $this->load->model('MenuModel');
      $this->load->library('Screenname');
   		}

	public function index()
	{	
    $getmenu    = $this->screenname->getscreen();
    $router     =& load_class('Router', 'core');
    $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
    $title      = '';
    foreach($getmenu['url'] as $key => $value){
      if($value == $indexroute){
        $title = $key;
      }
    }
    $mainmenu_data['icononly']   = $getmenu['icononly'];
    $mainmenu_data['url']        = $getmenu['url'];
    $mainmenu_data['screen']     = $title;
    $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    $ref =$this->Declining_Sales_Model->get_title();
    $mainmenu_data['title']= $ref;
        
    $this->load->view('Declining_Sales_view', $mainmenu_data);
  }
  public function getcategory(){

    $data = json_decode(file_get_contents("php://input"), true);
    $data =$this->Declining_Sales_Model->get_tab1_categorycombo();
    echo json_encode($data);
  }

  public function Declining_Sales_detail()
  {
    $data = json_decode(file_get_contents("php://input"), true);
    $declining_result = $this->Declining_Sales_Model->get_declining_sales($data['category']);  
    $dec_sal_detail = array();
          foreach ($declining_result as $value) {
                            $dec_sal_detail[] = array('ItemID'=>$value->ItemID,
                              'SKU'=>$value->SKU,
                              'Title'=>$value->Title,
                              'mypartno'=>$value->mypartno,
                              'Category'=>$value->Category,
                              'quantity'=>$value->quantity,
                              'price'=>round($value->price,2),
                              'qtysold_inc'=>$value->qtysold_inc,
                              'qtysoldvw1'=>$value->qtysoldvw1,
                              'qtysoldvw2'=>$value->qtysoldvw2,
                              'qtysoldvw3'=>$value->qtysoldvw3,
                              'qtysoldvw4'=>$value->qtysoldvw4,
                              'qtysoldvw5'=>$value->qtysoldvw5,
                              'salesw1'=>$value->salesw1,
                              'salesw2'=>$value->salesw2,
                              'salesw3'=>$value->salesw3,
                              'salesw4'=>$value->salesw4,
                              'salesw5'=>$value->salesw5
                              );    
          }
          echo json_encode($dec_sal_detail);
  }
        
}