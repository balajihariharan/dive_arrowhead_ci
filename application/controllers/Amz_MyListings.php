<?php
defined ('BASEPATH') OR exit('No direct script access allowed');

class Amz_MyListings extends CI_Controller
{
	public function __construct(){
    	parent::__construct();
        $this->load->model('Manage_User_Model'); 
        $this->load->model('MenuModel'); 
        $this->load->library('Screenname');   
        $this->load->database();
    }

    public function index(){   
        $getmenu    = $this->screenname->getscreen();
        $router     =& load_class('Router', 'core');
        $indexroute =  $router->fetch_class().'/'.$router->fetch_method();
        $title      = '';
        foreach($getmenu['url'] as $key => $value){
          if($value == $indexroute){
            $title = $key;
          }
        }
        $mainmenu_data['icononly']   = $getmenu['icononly'];
        $mainmenu_data['url']        = $getmenu['url'];
        $mainmenu_data['screen']     = $title;
        $mainmenu_data['menu_data']  = $getmenu['menu_data'];
    	$ref =$this->Manage_User_Model->get_title();
        $mainmenu_data['title']= $ref;
        $this->load->view('Amz_MyListingsView',$mainmenu_data);
    }

    public function LoadData(){
        $CI = &get_instance();
        $this->db3 = $CI->load->database('db3', TRUE);
        $data = $this->db3->query("SELECT * from dive_amz_mylistings;");
        $result = $data->result();

        $result_data = array();
        foreach ($result as $value) {
            $result_data[] = array(
                'ASIN'=>$value->ASIN,
                'Title'=>$value->Title,
                'ListingID'=>$value->ListingID,
                'SKU'=>$value->SKU,
                'ListPrice'=>round($value->ListPrice,2),
                'Quantity'=>round($value->Quantity),
                'InceptionDate'=>$value->InceptionDate
                );
        }
        echo json_encode($result_data);
    }
 }
?>
        
        
        
