<!DOCTYPE html>
<html ng-app="postApp">
<head>
<title>DIVE : Interchange</title>
    <link href="<?php echo base_url().'assets/';?>/img/favicon.ico" type="image/x-icon" rel="icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/';?>css/dropdown1.css">
    <link href="<?php echo base_url().'assets/';?>css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css">
            table thead tr td span{
                color:#fff;
            }
            .activeplus { color: red; }
			.curser_pt {
				cursor: pointer;
				background: url(assets/img/sort_icon.png) no-repeat 5px center;
				padding: 0 20px;
			}
            .plan_list_view p {
    display: inline-block;
}
            .table_inner_head_acc {
                background: #555 !important;
                /*box-shadow: 0 0 10px 2px #95A5A6;*/
            }
            .table_inner_head_acc th{
                color:#555;
            }
            .table_inner_head_acc th:nth-child(4){
                width: 400px;
            }
            .accordion {
                cursor: pointer;
            }

            .status_bar th:nth-child(4){
                min-width: 400px;
            }
            .status_bar th:nth-child(5){
                min-width: 200px;
            }
            
.status_bar tbody tr:last-child {
    background: #ccc none repeat scroll 0 0 !important;
}
            .heart_beat {
                /*background: #7ED5E0;
                animation: beat .25s infinite alternate;
                transform-origin: center;
                background: none;*/
                margin-right: 5px;
            }
            @keyframes beat {
                to {
                    transform: scale(1.4);
                    box-shadow: 0px 0px 10px 2px #D24D57;
                    border-radius: 80%;
                }
            }
            .prev,
            .next,
            .current-page {
                margin: 0 5px;
                font-weight: bold;
                background: #5D85B2;
                color: white;
                padding: 0px 1px;
                border-radius: 3px;
                text-decoration: none;
                display: inline-block;
            }
            .ms-options{
                width: 700%
            }

           
        ul,li { margin:0; padding:0; list-style:none;}
        .label { color:#000; font-size:16px;}
        .ckeckclass
        {
            margin:2px;
        }
        .divTable{
            float: left;
            width: 70px;
            height: 40px;
            border-right: 1px solid #e1e1e1;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .lastdivTable{
            float: left;
            width: 70px;
            padding-top: 28px;
            padding-left: 7px;
            
        }
        .paging-nav {
          text-align: right;
          padding-top: 2px;
        }

        .paging-nav a {
          margin: auto 1px;
          text-decoration: none;
          display: inline-block;
          padding: 1px 7px;
          background: #91b9e6;
          color: white;
          border-radius: 3px;
        }

        .paging-nav .selected-page {
          background: #187ed5;
          font-weight: bold;
        }

        tfoot
        {
        text-align: center !important;
        display: table-row-group !important;
        }

        .paging-nav,
        #tblmasterresult {
         
          margin: 0 auto;
          font-family: Arial, sans-serif;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 20%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .myPnopopupsubclose{
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;   
        }
        .myPnopopupsubclose:hover,
        .myPnopopupsubclose:focus{
            color: black;
            text-decoration: none;
            cursor: pointer;   
        }

        div.absolute {
            position: absolute;
            top: 326px;
            left: 758px;
            width: 46px;
            height: 407px;
            cursor: pointer;
           
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        .modal-content {
            background-color: #fefefe;
            margin: 10% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 32%; /* Could be more or less, depending on screen size */
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #2d8bd5;
            text-decoration: none;
            cursor: pointer;
        }
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
        }
		.status_bar tr, .status_bar th, .status_bar td {
    vertical-align: middle;
    height: 30px;
    min-width: 150px;
    line-height: 20px;
    width: 11%;
    word-break: break-all;
    max-width: 300px;
}
</style>
</head>

<!-- <div class="container head_bg">
        <div class="wrapper">
            <div class="head">
                <div class="logo">
                    <a href="arrowhead.html"> <img height="50px" src="<?php echo base_url().'assets/';?>img/logo.png"></a>
                </div>
                <div class="nav nav_bg">
                <ul>
                  <li><a class="first" href="http://192.168.100.227/Romaine/Rawlist"><img src="<?php echo base_url().'assets/';?>img/raw_list.png">&nbspRaw List</a></li>
                    <li><a class="second" href="http://192.168.100.227/Romaine/Masterlist"><img src="<?php echo base_url().'assets/';?>img/filter1.png">&nbspMaster List</a></li>
                    <li><a class="third last_menu_icon" href="<?php echo base_url();?>"><img src="<?php echo base_url().'assets/';?>img/competitor_analysis.png">&nbspCompetitor Analysis</a></li>
                    <li><a class="fourth" href="http://192.168.100.227/Romaine/Salestrends"><img src="<?php echo base_url().'assets/';?>img/sales_trends.png">&nbsp Sales Trends</a></li>
                    <li><a class="fifth" href="<?php echo base_url();?>welcome/exclusionlist"><img src="<?php echo base_url().'assets/';?>img/filter_menu.png">&nbsp Filters</a></li>
                    <li><a class="fifth" href="<?php echo base_url();?>Interchange/index"><img src="<?php echo base_url().'assets/';?>img/interchange_menu.png">&nbsp Interchange</a></li>
                </ul>
        </div>
                <div class="headright">
                    <div class="login">
                        <p>Welcome User</p>
                        <a href="#"><img src="<?php echo base_url().'assets/';?>img/logout.png" />&nbsp;Logout</a>
                    </div>

                </div>
            </div>
        </div>
    </div> -->