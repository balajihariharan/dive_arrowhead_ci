<?php
	echo "<pre>";

	require_once('/../pusher/vendor/autoload.php');
	require_once('eBaySessionShopping.php');
	require_once('DBConnect.php');
	require_once('Email.php');
	ini_set('max_execution_time', 0);

	ob_implicit_flush(true);

	//Set Last Run DateTime As Present Time
	date_default_timezone_set("Asia/Kolkata");

	$Options = array('encrypted' => false);
    $Pusher = new Pusher\Pusher(
      '26e79100d1fea27244e6',
      'cf47f5a111cad6290a96',
      '197221',$Options
    );	

	$MainDB = MainDBConnect();
	$MailObj = new Email();

	//Process Start Mail
	$MailObj->SendMail(array("Subject" => 'Started Successfully',
							 "Status"  => 'Success',
							 "Query"   => '',
							 "CustomerFlag" => 1,
    						 "Message" => 'Auto Pricer Process Started Successfully.'
    						)
					   );

	//Truncate Run Status Table
	$TrunQry ="TRUNCATE table config_ibr_cron_run_status";
	if (!$MainDB->query($TrunQry)){						
		$MailObj->SendMail(array("Subject" => 'Truncate Query Error - Run Status Table', 
								 "Status"  => 'Error',
								 "Query"   => $TrunQry,
								 "CustomerFlag" => 0,
								 "Message" => $MainDB->error
								)
							);
		die('Error Thrown : '.$MainDB->error);
	}


	//Truncate Output Table
	$TrunQry = "TRUNCATE table competitor_search_item_details";
	if (!$MainDB->query($TrunQry)){							
		$MailObj->SendMail(array("Subject" => 'Truncate Query Error - Search Item Details Table',
								 "Query"   => $TrunQry,
								 "Status"  => 'Error',
								 "CustomerFlag" => 0,
								 "Message" => $MainDB->error));
		die('Error Thrown : '.$MainDB->error);
	}
	
	
	//Fetch Input Data
	$KeysQry = "SELECT AppID FROM dive_ebaysettings WHERE Active_flag = 1";
	$KeysQryRes = $MainDB->query($KeysQry);
	$KeyCount = mysqli_num_rows($KeysQryRes);

	if($KeyCount == 1){
		while($Result = $KeysQryRes->fetch_assoc()){
			$AppID = trim($Result['AppID']);
		}	
	}
	else{
		$MailObj->SendMail(array("Subject" => 'Get eBay Credentials Error', 
								 "Query"   => '',
								 "Status"  => 'Error',
								 "CustomerFlag" => 1,
    							 "Message" => 'No eBay Credentials Are Active'
    							)
						  );
		die('No eBay Credentials Are Active');
	}
	


	$SelQry = "SELECT ItemID,REPLACE(crawler_url,'ToBeReplaced','".$AppID."') AS CrawlerURL FROM dive_config_ibr_filter WHERE Active_flag = 1 AND selected_flag = 1";
	$Res = $MainDB->query($SelQry);
	$ResCount = mysqli_num_rows($Res);

	if ($ResCount == 0){
		$Data['message'] = 'Process Completed';
	    $Pusher->trigger('mynewchannelarrowhead', 'eventarrow', $Data);
		$MailObj->SendMail(array("Subject" => 'Completed Successfully',
								 "Query"   => '',
								 "Status"  => 'Success',
								 "CustomerFlag" => 1,
    							 "Message" => 'There Is No Item To Continue The Auto Pricer Process'
    							)
						  );
	    die('There Is No Item In Table To Continue The Process');
	}

	$ItemIDResult = array();
	$URLResult = array();

	while($ItemIDRes = $Res->fetch_assoc()) {
		$ItemIDResult[] = $ItemIDRes['ItemID'];
		$URLResult[] = array('ItemID' => $ItemIDRes['ItemID'],
							 'CrawlerURL' => $ItemIDRes['CrawlerURL'],
							 );
	}

	$ItemIDResult = array_chunk($ItemIDResult,20);




	/*   GetMultipleItems Process   */	
	$MailObj->SendMail(array("Subject" => 'Current Price Update Process Started Successfully',
							 "Query"   => '',
							 "Status"  => 'Success',
							 "CustomerFlag" => 1, 
    						 "Message" => 'Title and Price Update For My Items Process Started Successfully'
    						)
					  );

	$Verb = 'GetMultipleItems';
	$Session = new eBaySessionShopping($AppID,$Verb);

	foreach ($ItemIDResult as $ItemIDS) {
		$RequestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
	    $RequestXmlBody .= '<GetMultipleItemsRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
	    //$RequestXmlBody .= "<IncludeSelector>Details</IncludeSelector>";
	    $RequestXmlBody .= "<ItemID>".implode("</ItemID><ItemID>", $ItemIDS)."</ItemID>";
	    $RequestXmlBody .= '</GetMultipleItemsRequest>';

		$ResponseXml = $Session->sendHttpRequest($RequestXmlBody);
	    $XMLResult = simplexml_load_string($ResponseXml);

	    if ($XMLResult->Ack == 'Success'){
	    	foreach ($XMLResult->Item  as $Items) {
	    		$ItemID = $Items->ItemID;
	    		$Title = $Items->Title;
	    		$Title = str_replace("'", "\'" ,$Title);
                $Title = str_replace('"', '\"' ,$Title);
	    		$CurrentPrice = $Items->ConvertedCurrentPrice;
	    		/*$SKU = $Items->SKU;
	    		$SKU = str_replace("'", "\'" ,$SKU);
                $SKU = str_replace('"', '\"' ,$SKU);*/
	    		
	    		if($CurrentPrice != ''){
	    			$Updry = "UPDATE competitor_master_list 
				    		  SET Price = ".$CurrentPrice.",
		                          Title = '".$Title."',
		                          Modified_By = 'Autopricer',
		                          Modified_On = NOW()
				       		  WHERE ItemID = '".$ItemID."' 
				       		  AND CompID = (SELECT CompID FROM dive_config_meta_data)";

				    if (!$MainDB->query($Updry)){							
						$MailObj->SendMail(array("Subject" => 'Update My Item Detail Error', 
												 "Query"   => $Updry,
												 "Status"  => 'Error',
												 "CustomerFlag" => 0,
												 "Message" => $MainDB->error));
						die('Error Thrown : '.$MainDB->error);
					}
				}
     	 	
	    	}
	    }
	    else{
	    	$Message = $XMLResult->Errors->LongMessage;
	    	$MailObj->SendMail(array("Subject" => 'eBay Get Price Error', 
	    							 "Query"   => $RequestXmlBody,
									 "Status"  => 'Error',
									 "CustomerFlag" => 1, 
									 "Message" => $Message));
			die("eBay Get Price Error : ".$Message);
	    }
	}

	$MailObj->SendMail(array("Subject" => 'Current Price Update Process Completed Successfully', 
							 "Query"   => '',
							 "Status"  => 'Success',
							 "CustomerFlag" => 1,
    						 "Message" => 'Title and Price Update For My Items Process Completed Successfully'
    						)
					  );




	/*   SearchKeyWord Process   */	
	$MailObj->SendMail(array("Subject" => 'SearchKeyWord Process Started Successfully',
							 "Query"   => '',
							 "Status"  => 'Success',
							 "CustomerFlag" => 1, 
    						 "Message" => 'SearchKeyWord Process For Finding Competitor Price Started Successfully'
    						)
					  );

	$Total = count($URLResult);
	$Progress = 1;

	foreach ($URLResult as $URLRes) {
		$MyItemID = $URLRes['ItemID'];
		$CrawlerURL = $URLRes['CrawlerURL'];
		$StartDate = date('d-m-Y H:i:s');

		echo "Current ItemID : ".$MyItemID."<br>";

		//Update The Current Status
	 	$CronIns = "INSERT into config_ibr_cron_run_status(ItemID, start, ended, progress,total,status_field)
						VALUES ('".$MyItemID."', '".$StartDate."', '', '".$Progress."', '" .$Total."','In Progress')";
		if (!$MainDB->query($CronIns)){							
			$MailObj->SendMail(array("Subject" => 'Insert Run Status Error',
									 "Query"   => $CronIns,
									 "Status"  => 'Error',
									 "CustomerFlag" => 0, 
									 "Message" => $MainDB->error));
			die('Error Thrown : '.$MainDB->error);
		}

		$Connection = curl_init();
		curl_setopt($Connection, CURLOPT_URL, $CrawlerURL);
		curl_setopt($Connection, CURLOPT_HTTPGET, true);
		curl_setopt($Connection, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($Connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($Connection, CURLOPT_SSL_VERIFYHOST, 0);

		$Response = curl_exec($Connection);
		$XMLResult = simplexml_load_string($Response);
		curl_close($Connection);
		
		if ($XMLResult->ack == 'Success'){
			$TotalRes = (int)$XMLResult->searchResult['count'];
			if ($TotalRes > 0){
				foreach ($XMLResult->searchResult->item as $SearchRes) {
					$SellerName = (string)$SearchRes->sellerInfo->sellerUserName;
					$ItemID = (string)$SearchRes->itemId;
					$Title = (string)$SearchRes->title;
					$Title = preg_replace('/\\\\/', '', $Title);
					$Title = str_replace("'", "\'" ,$Title);
					$Title = str_replace('"', '\"' ,$Title);
					$CategoryID = (string)$SearchRes->primaryCategory->categoryId;
					$Category = (string)$SearchRes->primaryCategory->categoryName;
					$Category = str_replace("'", "\'" ,$Category);
					$Category = str_replace('"', '\"' ,$Category);
					$CurrentPrice = (string)$SearchRes->sellingStatus->currentPrice;
					$ListingStatus = (string)$SearchRes->sellingStatus->sellingState;
					$ViewItemURL = (string)$SearchRes->viewItemURL;
					$Startdate = (string)$SearchRes->listingInfo->startTime;
					$DateTime = str_replace("T"," ",$Startdate);
					$DateTime = str_replace(".000Z","",$DateTime);
					if($DateTime != '') {
						$ListingOn = DateTime::createFromFormat('Y-m-d H:i:s',$DateTime);
						$ListingDate = $ListingOn->format('Y-m-d H:i:s');
					}
					$Country = (string)$SearchRes->country;
					$Location = (string)$SearchRes->location;
					$Location = str_replace("'", "\'" ,$Location);
					$Location = str_replace('"', '\"' ,$Location);
					$ConditionID = (string)$SearchRes->condition->conditionId;
					$ConditionName = (string)$SearchRes->condition->conditionDisplayName;
					$ShippingCost = (string)$SearchRes->shippingInfo->shippingServiceCost;
					$ShipType = $SearchRes->shippingInfo->shippingType;
					
					$HandlingTime = '';
					$HandlingTime = $SearchRes->shippingInfo->handlingTime;
					
					$MultiShipFlag = 0;
					$FreeShipFlag = 0;
					$TopRatedFlag = 0;

					if ($ShipType == 'Calculated' || $ShipType == 'CalculatedDomesticFlatInternational' || $ShipType == 'FlatDomesticCalculatedInternational'){
						$MultiShipFlag = 1;
					}

					if ($ShipType == 'Flat' || $ShipType == 'Free' || $ShipType == 'FreePickup'){
						$FreeShipFlag = 1;
					}

					if ($SearchRes->topRatedListing == 'true'){
						$TopRatedFlag = 1;
					}

					$CompLowPrice = $CurrentPrice + $ShippingCost;

					$SearchQry = "CALL dive_autopricer_search_item_details_insert('".$MyItemID."','".$ItemID."','".$Title."','".$CategoryID."','".$Category."','".$CurrentPrice."','".$ListingStatus."','".$SellerName."','".$ViewItemURL."','".$Country."','".$Location."','".$ListingDate."','".$ShippingCost."','".$ShipType."','".$HandlingTime."','".$CompLowPrice."','".$ConditionID."','".$ConditionName."',".$MultiShipFlag.",".$FreeShipFlag.",".$TopRatedFlag.")";

					if (!$MainDB->query($SearchQry)) {							
						$MailObj->SendMail(array("Subject" => 'Insert Into Search Item Detail Error', 
												 "Query"   => $SearchQry,
												 "Status"  => 'Error',
												 "CustomerFlag" => 0, 
												 "Message" => $MainDB->error));
						die('Error Thrown : '.$MainDB->error);
					}
				}
			}
		}
		else{
	    	$Message = $XMLResult->errorMessage->error->message;
	    	$MailObj->SendMail(array("Subject" => 'eBay SearchKeyWord Error', 
	    							 "Query"   => "My ItemID : ".$MyItemID."\n\n\n".$CrawlerURL,
									 "Status"  => 'Error',
									 "CustomerFlag" => 1,
									 "Message" => $Message));
			die("eBay SearchKeyWord Error : ".$Message);
	    }

	    $EndDate = date('d-m-Y H:i:s');
		$CronUpdQry = " UPDATE config_ibr_cron_run_status 
						 SET Ended = '".$EndDate."',
						 	 status_field= 'Completed' 
						 WHERE ItemID = '".$MyItemID."' ";
		if (!$MainDB->query($CronUpdQry)) {							
			$MailObj->SendMail(array("Subject" => 'Update Run Status Error',
									 "Query"   => $CronUpdQry,
									 "Status"  => 'Error',
									 "CustomerFlag" => 0,
									 "Message" => $MainDB->error));
			die('Error Thrown : '.$MainDB->error);
		}
		$Progress++;
	}

	$MailObj->SendMail(array("Subject" => 'SearchKeyWord Process Ended Successfully',
							 "Query"   => '',
							 "Status"  => 'Success',
							 "CustomerFlag" => 1, 
    						 "Message" => 'SearchKeyWord Process For Finding Competitor Price Ended Successfully'
    						)
					  );

	$MailObj->SendMail(array("Subject" => 'Price Recommendation Process Started Successfully',
							 "Query"   => '',
							 "Status"  => 'Success',
							 "CustomerFlag" => 1, 
    						 "Message" => 'Price Recommendation Process Based On Competitor Price Started Successfully'
    						)
					  );

	$AfterCronQry = "CALL dive_config_autorawlist_after_cron()";
	if (!$MainDB->query($AfterCronQry)) {							
		$MailObj->SendMail(array("Subject" => 'Price Recommendation Process Error',
								 "Query"   => $AfterCronQry,
								 "Status"  => 'Error',
								 "CustomerFlag" => 0,
								 "Message" => $MainDB->error));
		die('Error Thrown : '.$MainDB->error);
	}

	$MailObj->SendMail(array("Subject" => 'Price Recommendation Process Ended Successfully',
							 "Query"   => '',
							 "Status"  => 'Success',
							 "CustomerFlag" => 1, 
    						 "Message" => 'Price Recommendation Process Based On Competitor Price Ended Successfully'
    						)
					  );

	$MailObj->SendMail(array("Subject" => 'Completed Successfully',
							 "Status"  => 'Success',
							 "Query"   => '',
							 "CustomerFlag" => 1,
    						 "Message" => 'Auto Pricer Process Completed Successfully.'
    						)
					   );
	$Data['message'] = 'Process Completed ';
    $Pusher->trigger('mynewchannelarrowhead', 'eventarrow', $Data);
?>