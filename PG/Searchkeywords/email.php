<?php
	class Email  
	{
		function SendMail($Input) {
			$Instance = 'AH-Staging';
			$Subject = $Instance." - Auto Pricer Process - ".$Input['Subject'];
			$Message = $Input['Message'];
			$Query = $Input['Query'];
			$Status = $Input['Status'];
			$CustomerFlag = $Input['CustomerFlag'];
			$From  = "tech@apaengineering.com";

			if ($CustomerFlag){
				$To = "vaithi.d@apaengineering.com";
				$CC = "tamilselvan.s@apaengineering.com,balaji.h@apaengineering.com";
			}
			else{
				$To = "vaithi.d@apaengineering.com,tamilselvan.s@apaengineering.com,balaji.h@apaengineering.com";
			}
			
			
			$HTMLMessage = '<html><body style="width: 800px;margin:0px auto; padding:0px;"><table style="background-color: #f8f8f8; display: inline-block; border-right: none; width: 780px; margin:0px auto" cellpadding="0" cellspacing="0"><tr><td align="left" style="background: #fff; padding:20px 2px 14px 0px;"><a href="http://apaengineering.com/"><img src="https://apaengineering.com/wp-content/uploads/2017/07/logo-uai-258x37.png"></a><a href="#" style="float: right;" style="text-align:center;"><img src="http://apaengineering.com/dive.jpg" width="100px"></a></td></tr><tr style=" width:100%;"><td style="padding:0px;"><table style="  border-top:10px solid #4e545a; border-bottom:none; border-collapse: collapse; padding: 0px 0 0 10px; border-spacing: 0px; background: #e2e2e2; width: 780px;" width="100%" cellpadding="0" cellspacing="0"><tr><td style="font-family: MyriadPro; font-size: 15px; color: #333; padding: 10px;">Hi Team,</td></tr><tr>';

			if ($Status != 'Error'){
				$HTMLMessage .= '<td style="font-family: MyriadPro; font-size: 15px; color: green; padding: 10px;font-weight:bold;">'.$Message.'</td>';
			}
			else{
				$HTMLMessage .= '<td style="font-family: MyriadPro; font-size: 15px; color: red; padding: 10px;font-weight:bold;">'.$Message.'</td>';	
			}

			$HTMLMessage .= '</tr></table></td></tr><tr><td style="font-size: 13px; color: #0e30ec; padding: 10px;"><span style="font-weight: bold;">Note : </span>This is a System Generated Mail. Please Do Not Reply Back To This Email.</td> </tr></table></body></html>';

			$Boundary = md5($Instance);

			//Headers
			$Headers = "MIME-Version: 1.0\r\n"; 
	        $Headers .= "From: APA-AP : Alerts <".$From.">\r\n";
	        $Headers .= "CC: ".$CC."\r\n";
	        $Headers .= "Content-Type: multipart/mixed; boundary = $Boundary\r\n\r\n"; 

	        //Body
	        $Body = "--$Boundary\r\n";
	        $Body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	        $Body .= "Content-Transfer-Encoding: base64\r\n\r\n"; 
	        $Body .= chunk_split(base64_encode($HTMLMessage)); 

	        if ($Status == 'Error'){
				//Attachment
				$Message = "Error Description : \n\n".$Message;
	            $Query = "Error Triggered By : \n\n".$Query;
	            $Content = chunk_split(base64_encode($Message."\n\n\n\n\n\n".$Query));
				$ErrorFileName  = $Instance.'_AutoPricer_Error';
				$ErrorFileType = 'text/plain';
		        $Body .= "--$Boundary\r\n";
		        $Body .="Content-Type: $ErrorFileType; name=".$ErrorFileName."\r\n";
		        $Body .="Content-Disposition: attachment; filename=".$ErrorFileName."\r\n";
		        $Body .="Content-Transfer-Encoding: base64\r\n";
		        $Body .="X-Attachment-Id: ".rand(1000,99999)."\r\n\r\n"; 
		        $Body .= $Content;
		    }

	        $SendMail = @mail($To, $Subject, $Body, $Headers);
		    if($SendMail){       
		        echo "Sent Mail !!";
		    }
		    else{
		        echo "Could Not Send Mail !!";  
		    }
		}	
	}
	
?>


