<?php

class eBaySessionShopping
{
	private $appID;
	private $verb;
	
	public function __construct($applicationID,$callName){
		$this->appID = $applicationID;
		$this->verb = $callName;
	}

	public function sendHttpRequest($requestBody){
		$headers = $this->buildEbayHeaders();
		$connection = curl_init();
		$serverUrl = 'http://open.api.ebay.com/shopping';
		curl_setopt($connection, CURLOPT_URL, $serverUrl);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($connection, CURLOPT_POST, 1);
		curl_setopt($connection, CURLOPT_POSTFIELDS, $requestBody);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($connection);
		curl_close($connection);
		return $response;
	}
	
	private function buildEbayHeaders(){
		$headers = array (
			'X-EBAY-API-VERSION: 647',
			'X-EBAY-API-APP-ID: '.$this->appID,
			'X-EBAY-API-CALL-NAME: '.$this->verb,			
			'X-EBAY-API-SITEID: 0',
			'X-EBAY-API-REQUEST-ENCODING: XML',			
			'X-EBAY-API-RESPONSE-ENCODING: XML',
			'Content-Type: text/xml'
		);
		return $headers;
	}
}

?>