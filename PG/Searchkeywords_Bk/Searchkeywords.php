<?php
	require_once('/../pusher/vendor/autoload.php');
	require_once('keys.php');
	require_once('eBaySession.php');
	require_once('DBConnect.php');
	require_once('email.php');
	ob_implicit_flush(true);

	$Instance = 'CompleteTractor';
	$MailFlag = 'Yes';

	//Dedicated DB Connect
	$MainDB = MainDBConnect();
	//Central DB Connect
	//$CentralDB = CentralDBconnect();
	
	//Mail Function
	$MailObj = new objmail();

	$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Process Started Successfully', 
    							 "sendmail" => $MailFlag, 
    							  "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer processing program started successfully.'
    							)
						  );

	//Truncate Run Status Table
	$TrunQry ="TRUNCATE table config_ibr_cron_run_status";
	if (!$MainDB->query($TrunQry)) 
	{							
		$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Truncate Query Error - Run Status Table', 
									 "sendmail" => $MailFlag, 
									 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
		die('<P>Error thrown : ' .  $mysqli->error);
	}



	//Truncate Output Table
	$TrunQry = "TRUNCATE table competitor_search_item_details";
	if (!$MainDB->query($TrunQry)) 
	{							
		$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Truncate Query Error - Search Item Details Table', 
									 "sendmail" => $MailFlag, 
									 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
		die('<P>Error thrown : ' .  $mysqli->error);
	}
	

	//Set Last Run DateTime As Present Time
	date_default_timezone_set("Asia/Kolkata");
    $LastRunDate = date('Y-m-d H:i:s');
	$UpdQry = " UPDATE dive_config_ibr_filter 
				SET Lastrundate = '".$LastRunDate."' 
				AND Active_flag = 0 ";
	//$MainDB->query($UpdQry);

	
	//Fetch Input Data
	$Appidqry = "SELECT AppId FROM dive_ebaysettings WHERE Active_flag =1";
	$Appidqryres = $MainDB->query($Appidqry);
	$Appid = $Appidqryres->fetch_assoc();
	$Appid = $Appid['AppId'];
	$SelQry = "SELECT REPLACE(crawler_url,'ToBeReplaced','".$Appid."') AS crawler_url,itemid,categoryid 
			   FROM dive_config_ibr_filter 
			   WHERE Active_flag = 1 AND selected_flag = 1";
	$SelQry2 = "SELECT crawler_url,itemid,categoryid 
			   FROM dive_config_ibr_filter 
			   WHERE Active_flag = 1 ";
			   //AND Percent = 33
	$Res = $MainDB->query($SelQry);
	$res2 = $MainDB->query($SelQry2);
	$Result = array();
	while($row = $Res->fetch_assoc()) 
	{
		$Result[] = $row;
	}

 
	//If No Input, End Process With Pusher Notification
	if (count($Result) == 0)
	{
		$options = array('encrypted' => false);
	    $pusher = new Pusher\Pusher(
	      '26e79100d1fea27244e6',
	      'cf47f5a111cad6290a96',
	      '197221',$options
	    );	
		$data['message'] = 'Process Completed ';
	    $pusher->trigger('mynewchannelarrowhead', 'eventarrow', $data);
	    $MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Process Completed Successfully', 
    							 	 "sendmail" => $MailFlag, 
    							  	  "msg" => 'There Is No Item In Table To Continue The Process'
    								 )
						  	   );
	    exit;
	}
    
	//Pusher Notifications
	/*if (count($Result) == $res2->num_rows){
	$options = array('encrypted' => false);
      $pusher = new Pusher\Pusher(
      '26e79100d1fea27244e6',
      'cf47f5a111cad6290a96',
      '197221',
      $options
    );
    $data['message'] = 'Scheduled In Progress:Running For All Items';
   // print_r($data);
    $pusher->trigger('schedulerprogressarrowhead', 'myevent', $data);
 }
*/
 /*if (count($Result) != $res2->num_rows){
	$options = array('encrypted' => false);
      $pusher = new Pusher\Pusher(
      '26e79100d1fea27244e6',
      'cf47f5a111cad6290a96',
      '197221',
      $options
    );
    $data['message'] = 'Scheduled In Progress:Running For Selected Items';
    // print_r($data);
    $pusher->trigger('schedulerprogressarrowhead', 'myevent', $data);
 }*/
	$start_Date = date('d-m-Y H:i:s');
	$total = count($Result);
	$balncecomp = 1;
	$progress_count = 1;

	$keyquery = " SELECT `AppID`,`DevID`,`CertID`,`ServerURL`,`Compatability`,`SiteID`,`AuthKey` FROM  `dive_ebaysettings` WHERE `Active_flag` =1";
	$keyqueryres = $MainDB->query($keyquery);
	print_r($keyqueryres);
	while($resultset = $keyqueryres->fetch_assoc()) 
	{
		$keyResult[] = $resultset;
	}
	//print_r($keyResult[0]);
	//exit;
	//$Appid = $Appidqryres->fetch_assoc();


	if(count($Result) > 0)
	{
		foreach($Result as $reset)
		{         
			$itemidcrawl = $reset['itemid'];
            $compatabilityLevel = $keyResult[0]['Compatability'];
	        $devID = $keyResult[0]['DevID'];
	        $appID = $keyResult[0]['AppID'];
	        $certID = $keyResult[0]['CertID'];
	        $serverUrl = $keyResult[0]['ServerURL'];
	        $userToken1 = $keyResult[0]['AuthKey'];
		    $siteID = 0;
		    $verb = 'GetItem';

		    $requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
		    $requestXmlBody .= '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		    $requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken1</eBayAuthToken></RequesterCredentials>";
		    $requestXmlBody .= "<IncludeItemSpecifics>true</IncludeItemSpecifics>";
		    $requestXmlBody .= "<ItemID>$itemidcrawl</ItemID>";
		    $requestXmlBody .= '</GetItemRequest>';

		    $session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);
		    $responseXml = $session->sendHttpRequest($requestXmlBody);
		    $responseDoc = new DomDocument();
		    $responseDoc->loadXML($responseXml);
		    $xml1 = simplexml_load_string($responseXml);

		    if (stristr($responseXml, 'exceeded usage limit') || $responseXml == '')
		    {
				$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Exceeded Usage Limit Error - GetItem',
											 "sendmail" => $MailFlag,
											  "msg" => $responseXml));
				die('<P>Error Thrown : ' .  $mysqli->error);
			}
		    
		    foreach($xml1->Item as $resultvalue)
		    {
				$titlenew =(string)$resultvalue->Title;
				$titlenew = str_replace("'", "\'" ,$titlenew);
                $titlenew = str_replace('"', '\"' ,$titlenew);
				$SKUnew = (string)$resultvalue->SKU;
			}
		
		    foreach($xml1->Item->SellingStatus as $value2)
		    {
		      	$Rslt = (array)$value2;
		    }
		    
		    $CurrentPricenew = $Rslt['CurrentPrice'];
		    

		    //Update Current Price, SKU and Title For My Item
		     if( $CurrentPricenew != ''){
		    $updatequery = "UPDATE competitor_master_list 
		    				SET price= ".$CurrentPricenew.",
                               	SKU   = '".$SKUnew."',
                                title = '".$titlenew."',
                                Modified_By ='Autopricer',
                                Modified_On = now()
		       				WHERE itemid = ".$itemidcrawl." 
		       				AND compid = (SELECT compid FROM dive_config_meta_data)";   
		    if (!$MainDB->query($updatequery)) 
		    {							
				$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Update My Item Detail Error', 
											 "sendmail" => $MailFlag, 
											 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
				die('<P>Error Thrown : ' .  $mysqli->error);
			}
		}
     	 	

     	 	//Update The Current Status
		 	$cron_insert = "INSERT into config_ibr_cron_run_status(itemid, start, ended, progress,total,status_field)
							VALUES ('".$reset['itemid']."', '".$start_Date."', 'In Progress', '" . $progress_count. "', '" . $total. "','In Progress')";
			if (!$MainDB->query($cron_insert)) 
			{							
				$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Insert Run Status Error', 
											 "sendmail" => $MailFlag, 
											 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
				die('<P>Error Thrown : ' .  $mysqli->error);
			}
			
			
			$url = $reset['crawler_url'];
			$location = 'USA';
			$remaineComp = $total - $balncecomp;	
			
			$runid = 0;
            $lastrunid = 1;
			$lastrunid = $lastrunid + 1;
			$runonce = 1;
			$siteID = 0;
		
			$userAuthToken = 'AgAAAA**AQAAAA**aAAAAA**IriZWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ABmIOgCpmAog+dj6x9nY+seQ**P3gDAA**AAMAAA**dU7O4i8uhM7pzhMWjeixhzipmJX/pC3dELPQwS55dCQI+Cc4g+mH+0llOKAOKz/XCfFP6mUp/VH17VS1Okc70ygtu5czc5DKKgLWyocV9oeRpxLGwLS4SuQUXhPoDwTIt8uZw3N48QVvEHk5svvxTiQUoTCZN2K0FpIMzCDAyV87fsAe9XimqfG/mwVZkWG5MAgcyuUgislZj7j744sStlLLzxsk/NjJaLQnY+e1PqRsIZNrp/s7gWSFnYkeks4plpwM+SV+Co3vt1kG5oX7YCmfD44vtFQ8Qqc3XYty99WOoCFvDjpcWZkq55S89JA4dlJB1Jr8FE6r8uLkxNiRzGyYlfT5LfGshAFX15dJT5h6VeOO+VAUKQn+JVjoMSrHcmZkddO9+2yVCHEMRKYPS2C3r20xxz2iD0fBdj6QnL/Ds0E7YsEFljsVgR+KYMlGJm0rTaZCPFm7+D+UidQpgtFFeFV5OWJjCo1KnppLtAJfBZ0UgngIQDBli4AljSclV88qb32B8nJK5Fdekchx3vrJYqLsdsDUTxCk1Nkp4awFj7/iLDOpfm1M7wE5h4+vjGu+bsknGkNMB6nvJPNiGu26vcBMSW4bebckO3feLFNyk7PtUKkk1IMrIb6U66ZP4ctQ+FtIwOEj3U/C341M71bfpnR+U/wSO5HUPgLuK9vY/Z5qrCExBophwbDw1LHEcc0u/CufsTOgIT9i7nYNTpy0YyGoEx+P+GN1jZPykQjhurwP1vgHWt43JOw0+imI';

			$connection = curl_init();
			curl_setopt($connection, CURLOPT_URL, $url);
			curl_setopt($connection, CURLOPT_HTTPGET, true);
			$headers = array();
			$headers[] = $userAuthToken;
			$headers[] = 'X-EBAY-GLOBAL-ID:EBAY-US';
			curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

			$response = curl_exec($connection);
			$resultxml = simplexml_load_string($response);
			$responseDoc = new DomDocument();
			$responseDoc->loadXML($response);
			curl_close($connection);

			if (stristr($responseXml, 'exceeded usage limit') || $responseXml == '')
			{
				$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Exceeded Usage Limit Error - SearchKeyWord',
											 "sendmail" => $MailFlag,
											  "msg" => $responseXml));
				die('<P>Error Thrown : ' .  $mysqli->error);
			}
			
			$myItemID = $reset['itemid'];
			echo "\n My Item ID : " . $myItemID;
			$categoryid = $reset['categoryid'];
			
			$count = (int)$resultxml->searchResult['count'];

			if ($count > 0)
			{
				foreach($resultxml->searchResult->item as $itemkey => $itemvalue)
				{
					$shipping = $shippingcost = '';
					$userid = (string)$itemvalue->sellerInfo->sellerUserName;
					$itemID = (string)$itemvalue->itemId;
					$title = (string)$itemvalue->title;
					$category = (string)$itemvalue->primaryCategory->categoryName;
					$categoryid =  $categoryid;
					$currentprice = (string)$itemvalue->sellingStatus->currentPrice;
					$ListingStatus = (string)$itemvalue->sellingStatus->sellingState;
					$ViewItemURL = (string)$itemvalue->viewItemURL;
					$listingdate = '';//
					$startdate = (string)$itemvalue->listingInfo->startTime;
					$datetm = str_replace("T"," ",$startdate);
					$datetm = str_replace(".000Z","",$datetm);
					if($datetm != '') {
						$listedon = DateTime::createFromFormat('Y-m-d H:i:s',$datetm);
						$listingdate = $listedon->format('Y-m-d H:i:s');
					}
					$itemcountry = (string)$itemvalue->country;
					$itemLocation = (string)$itemvalue->location;
					$ConditionID = (string)$itemvalue->condition->conditionId;
					$ConditionDisplayName = (string)$itemvalue->condition->conditionDisplayName;
					
					$shippingcost = (string)$itemvalue->shippingInfo->shippingServiceCost;
					$shipType = $itemvalue->shippingInfo->shippingType;
					$handlingtime ='';
					$handlingTime = $itemvalue->shippingInfo->handlingTime;
					
					$MultiShipFlag = 0;
					$FreeShipFlag = 0;
					$TopRatedFlag = 0;

					if ($shipType == 'Calculated' || $shipType == 'CalculatedDomesticFlatInternational' || $shipType == 'FlatDomesticCalculatedInternational'){
						$MultiShipFlag = 1;
					}

					if ($shipType == 'Flat' || $shipType == 'Free' || $shipType == 'FreePickup'){
						$FreeShipFlag = 1;
					}

					if ($itemvalue->topRatedListing == 'true'){
						$TopRatedFlag = 1;
					}

					$category = str_replace("'", "\'" ,$category);
					$category = str_replace('"', '\"' ,$category);
					
					$itemLocation = str_replace("'", "\'" ,$itemLocation);
					$itemLocation = str_replace('"', '\"' ,$itemLocation);
					
					$title = preg_replace('/\\\\/', '', $title);
					$title = str_replace("'", "\'" ,$title);
					$title = str_replace('"', '\"' ,$title);

					$itemexistsqry = "select 1 from competitor_search_item_details where itemID='".$itemID."' and myitemid = '" . $myItemID . "'";
					$existsresult = $MainDB->query($itemexistsqry);	
					$result = $existsresult->fetch_row();

					if($result[0] == 1){
						$updateQry ="UPDATE competitor_search_item_details 
									 SET userid = '".$userid."' , 
										 ItemID = '".$itemID."' , 
										 Title = '".$title."' , 
										 CategoryID = '".$categoryid."' , 
										 Category = '".$category."' , 
										 CurrentPrice = '".$currentprice."' , 
										 ListingStatus = '".$ListingStatus."' , 
										 ItemURL = '".$ViewItemURL."' , 
										 ListingDate = '".$listingdate."' , 
										 RecordDate = now() , 
										 WeekNo = WEEK(NOW()) , 
										 YearNo = YEAR(NOW()) , 
										 Country = '".$itemcountry."' , 
										 Location = '".$itemLocation."' , 
										 ConditionID = '".$ConditionID."' , 
										 ConditionName = '".$ConditionDisplayName."' , 
										 ShipType = '".$shipType."' ,
										 ShippingCost = '".$shippingcost."' ,
										 FreeShipFlag = ".$FreeShipFlag." ,
										 MultiShipFlag = ".$MultiShipFlag." ,
										 TopRatedFlag = ".$TopRatedFlag." , 
										 HandlingTime = '".$handlingTime."' ,
										 CompLowPrice = '".($currentprice +$shippingcost)."' , 
										 RecommendedPrice = '".($currentprice +$shippingcost)."'
										 WHERE itemID = '" . $itemID . "' 
										 AND myitemid = '" . $myItemID . "'";	
						if (!$MainDB->query($updateQry)) {							
							$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Update Search Item Detail Error', 
														 "sendmail" => $MailFlag, 
														 "msg" => ''.$updateQry.'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'.$myItemID.''));
							die('<P>Error Thrown : ' .  $mysqli->error);
						} 
					}
					else{
						$insertQry = "INSERT INTO competitor_search_item_details(userid, MyItemid, ItemID, Title, Category, CategoryID, CurrentPrice, ListingStatus, ItemURL,ListingDate, RecordDate, WeekNo, YearNo, Country, Location, ConditionID, ConditionName, ShipType,ShippingCost,FreeShipFlag, MultiShipFlag, TopRatedFlag, HandlingTime, CompLowPrice,RecommendedPrice)
									VALUES('".$userid."', '".$myItemID."', '".$itemID."', '".$title."', '".$category."', '".$categoryid."', '".$currentprice."','".$ListingStatus."','".$ViewItemURL."','".$listingdate."', now(), WEEK(NOW()), YEAR(NOW()), '".$itemcountry."','".$itemLocation."', '".$ConditionID."', '".$ConditionDisplayName."','".$shipType."' , '".$shippingcost."', ".$FreeShipFlag." ,".$MultiShipFlag." ,".$TopRatedFlag." ,'".$handlingTime."','".($currentprice +$shippingcost)."','".($currentprice +$shippingcost)."')";
						if (!$MainDB->query($insertQry)) {							
							$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Insert Into Search Item Detail Error', 
														 "sendmail" => $MailFlag, 
														 "msg" =>''.$insertQry.'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'.$myItemID.''));
							die('<P>Error Thrown : ' .$mysqli->error);
						} 
					}
				}
				$endDate = date('d-m-Y H:i:s');
				$cron_uptqry = " UPDATE config_ibr_cron_run_status 
								 SET ended = '".$endDate."',
								 	 status_field= 'Completed' 
								 WHERE itemid = '".$myItemID."' ";
				if (!$MainDB->query($cron_uptqry)) {							
					$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Update Run Status Error', 
												 "sendmail" => $MailFlag, 
												 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
					die('<P>Error Thrown : ' .  $mysqli->error);
				}
				$progress_count++;
			}	
		}	
	}
   	
   	$spcall = "CALL  dive_config_autorawlist_after_cron()";
    if (!$MainDB->query($spcall)) {							
		$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer After Cron Error', 
									 "sendmail" => $MailFlag, 
									 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
		die('<P>Error Thrown : ' .  $mysqli->error);
	}


    $spcalltwo = "CALL  psmart_price_recomendation_process_ibr()";
    if (!$MainDB->query($spcalltwo)){							
		$MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Price Recommendation Error', 
									 "sendmail" => $MailFlag, 
									 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended With Error.'));
		die('<P>Error Thrown : ' .  $mysqli->error);
	}

	unset($MainDB);
	//unset($CentralDB);
	
	$options = array('encrypted' => false);
      $pusher = new Pusher\Pusher(
      '26e79100d1fea27244e6',
      'cf47f5a111cad6290a96',
      '197221',
      $options
    );	
	$data['message'] = 'Process Completed ';
    $pusher->trigger('mynewchannelarrowhead', 'eventarrow', $data);

    $MailObj->mailfunction(array("subject" => $Instance.' - Auto Pricer Process Successfully Completed', 
								 "sendmail" => $MailFlag, 
								 "msg" => 'This is an automated mailer to intimate that the '.$Instance.' Auto Pricer Process Ended Successfully.'));
?>
