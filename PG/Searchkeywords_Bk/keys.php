<?php
/*  � 2013 eBay Inc., All Rights Reserved */ 
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
    //show all errors - useful whilst developing
    error_reporting(E_ALL);

    // these keys can be obtained by registering at http://developer.ebay.com
    
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = 'bb002fe6-899e-402a-922e-8f2963808b11';   // these prod keys are different from sandbox keys
        $appID = 'Sivaranj-dive-PRD-85d8a3c47-37b732aa';
        $certID = 'PRD-5d8a3c47bf6c-21c4-49d4-9538-6bfc';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**NMBpWg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ADk4ekDZGGpwidj6x9nY+seQ**Xx4EAA**AAMAAA**rXYZtOesTfYd+Dqp2wCm4jC3x/lBIAvzw2k7ZjKwKE2oxRRhMItAMznPCAh7uDn7brN+U2sErDgIgYSlflSE+pFqRBuIfEDrtFtW18Z9IQOV3kzrS2DwDl2bSXXRIsx6oHdhy54mzqOu1DV5spQQNnoQ2AxMrMw0blMeteSgoWY7LI5dII05ps6j87bzifGKPu7UsksihyLmpuVEo9KrF7S1/+YI+jF8uwxzvgRF4lQ7LXuYT4JCSI8LzTwZM3Y86ci7dX+OcJ8F24fzsOGkDYhGqa11Va/ICR2heMuhLN2AJTIepIsazMxElSEQG7nUgtMWYO5eb+WRT2vHm445A1U1D4objbDU3V6iEN3AwVmqkv8QBaV4+/96/KJfquNrfaksGLoT+3F/xEGqnk0PNpyhDiYTbZWqNBrG61wUPxh4YKjTFz8181w8numIOIV+tUKdhenRbNhM9FUK6wwp23ZeXmcgs9WqO8BD6uP+rHMyoUQRNVzRxDy6Vh8e+YJtbG/kUQtxX4uEoyoG1OfIKKS69pQOb6ZTbkMJGMU/XuvOaM2fnFliKQa/wSEvPhXT4JsNwHmb04FOr4bE9hcnLRQFwDXIITiG+Ggf2D9QETddbH5gGJM8gOq8Ssh+zwuMPEqx/sTW5Z7EDpNAw2002LDMH0YR4SDt4HKAXgY7wDtLI60rgiZO0ig0wUaLJ4SMoY5Kfl9UqTcB7Kh0hgG38tpSCrLuXzVv7afMNG5rgiomEX0TvAvjbRwqH+I7jiKd';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
    
    
?>