<?php
require_once('AmazonConfig.php');
require_once('DBConnect_amz.php');

ob_implicit_flush(true);

$mysqli = openDBconnect();

//Update RunFlag = 0  in dive_amz_config_explore(table)
/*$Up_date_Query = "UPDATE dive_amz_config_explore SET RunFlag = 0";
$Update_Query_Res = $mysqli->query($Up_date_Query);*/
$deletePreviousweekdata = "delete from dive_amz_cron_details where WeekNo = (WEEK(NOW())-1)";
$mysqli->query($deletePreviousweekdata);
$runflag = "UPDATE  dive_amz_config_explore SET   RunFlag = 0 WHERE ApproveFlag = 1";
$mysqli->query($runflag);


$SelQuery = "SELECT ASIN,Keywords FROM  dive_amz_config_explore WHERE RunFlag = 0 AND ApproveFlag = 1";

$ConfigObject = new AmazonConfig;

$SelQueryRes = $mysqli->query($SelQuery);

while($row = $SelQueryRes->fetch_assoc()) 
{
	$ASIN = $row['ASIN'];
	$Keyword = $row['Keywords'];
	/*$Exclusionselqry = "SELECT ASIN FROM dive_amz_configexplore_exclude WHERE myASIN ='".$row['ASIN']."'";
	$exist_ASIN = array();
	$result_query = $mysqli->query($Exclusionselqry);

	$compasinid = array();
	      if($result_query->num_rows>=1){
            while($resrow = $result_query->fetch_assoc()){
                $compasinid[] = $resrow['ASIN'];
            }
          }*/
          
	$SearchResult = $ConfigObject->GetSearchKeywordResult($Keyword);
	
	$Keyword_res = array();	
	foreach ($SearchResult->Product as $value) {
		$Keyword_res[]= (string)$value->Identifiers->MarketplaceASIN->ASIN;
	}

	//GetPriceResult
	$SearchPriceResult = $ConfigObject->GetPriceResult($Keyword_res);
	$Price_res = array();	
	foreach ($SearchPriceResult->GetCompetitivePricingForASINResult as $value) {

		if(isset($value->Product->CompetitivePricing->CompetitivePrices->CompetitivePrice->Price)){
          $Landed_Price = $value->Product->CompetitivePricing->CompetitivePrices->CompetitivePrice->Price->LandedPrice->Amount;
          $Shipping_Price =$value->Product->CompetitivePricing->CompetitivePrices->CompetitivePrice->Price->Shipping->Amount;
	      }
	    else{
	      	 $Landed_Price = '';
	          $Shipping_Price = '';
	     }
		$Price_res[(string)$value->Product->Identifiers->MarketplaceASIN->ASIN]= array(
			'LandedPrice'=>$Landed_Price,
			'ASIN'=>(string)$value->Product->Identifiers->MarketplaceASIN->ASIN,
			'Shipping'=>$Shipping_Price
			);
							
	}
/*exit;*/
	//Get Competitor Cost & Shipping Price and Over all SearchResult Insert into dive_amz_cron_details(table)
	foreach ($SearchResult->Product as  $search_value) {
		foreach ($Price_res as $pr_key => $pr_value) {
			if($search_value->Identifiers->MarketplaceASIN->ASIN == $pr_key){
				 $CategoryRank = '';
                 $OverallCategoryRank = '';
                 $Cate_gory='';
                 $Overall_Catgory ='';
                if(count($search_value->SalesRankings->SalesRank) > 1){
                	$Overall_Catgory = (string)$search_value->SalesRankings->SalesRank[0]->ProductCategoryId;
                    $OverallCategoryRank = (string)$search_value->SalesRankings->SalesRank[0]->Rank;
                    $Cate_gory = (string)$search_value->SalesRankings->SalesRank[1]->ProductCategoryId;
                    $CategoryRank = (string)$search_value->SalesRankings->SalesRank[1]->Rank;
                }
                else if(count($search_value->SalesRankings->SalesRank) == 1){
                  $Cate_gory = (string)$search_value->SalesRankings->SalesRank[0]->ProductCategoryId;
                  $CategoryRank = (string)$search_value->SalesRankings->SalesRank[0]->Rank;
                  $Overall_Catgory = '';
                  $OverallCategoryRank = '';
                }
				$CompASIN = (string)$search_value->Identifiers->MarketplaceASIN->ASIN;
				$CompName = (string)$search_value->AttributeSets->ItemAttributes->Publisher;
				$CompTitle = (string)$search_value->AttributeSets->ItemAttributes->Title;
				$CompTitle =str_replace("'",'',$CompTitle);
				$CompListPrice = $search_value->AttributeSets->ItemAttributes->ListPrice->Amount;
				$CompPartNo = (string)$search_value->AttributeSets->ItemAttributes->PartNumber;
				$CompImageURL = (string)$search_value->AttributeSets->ItemAttributes->SmallImage->URL;
				$OverAllCatID = $Overall_Catgory;
				$OverAllRank = $OverallCategoryRank;
				$RankCatID =$Cate_gory;
				$RankCat = $CategoryRank;
				$CompIncDate = (string)$search_value->AttributeSets->ItemAttributes->ReleaseDate;
				$CompCurrentPrice=$pr_value['LandedPrice'];
				$CompShippingPrice=$pr_value['Shipping'];
				
               //if(!in_array($CompASIN,$compasinid)){
                    $itemexistsqry = "SELECT 1 from dive_amz_cron_details where ASIN='".$ASIN."' and CompASIN='".$CompASIN."' ";
					$existsresult = $mysqli->query($itemexistsqry);	
					$result = $existsresult->fetch_row();
					if($result[0] == 1){

								$updateQry ="UPDATE dive_amz_cron_details 
															SET WeekNo = WEEK(NOW()) , 
																YearNo = YEAR(NOW()) , 
																ASIN = '".$ASIN."' , 
																Keywords = '".$Keyword."' , 
																CompName = '".$CompName."' , 
																CompASIN = '".$CompASIN."' , 
																CompTitle = '".$CompTitle."' , 
																CompListPrice = '".$CompListPrice."' , 
																CompCurrentPrice = '".$CompCurrentPrice."' , 
																CompShippingPrice = '".$CompShippingPrice."' , 
																CompPartNo = '".$CompPartNo."' , 
																CompImageURL = '".$CompImageURL."' , 
																CompIncDate = '".$CompIncDate."' , 
																OverAllCatID = '".$OverAllCatID."' , 
																OverAllRank = '".$OverAllRank."' , 
																RankCatID = '".$RankCatID."', 
																RankCat = '".$RankCat."' 
																WHERE ASIN = '".$ASIN."'
																and  CompASIN='".$CompASIN."'";	
								$mysqli->query($updateQry);

					}
					else{
				//insert lowest price competitor data in dive_amz_cron_details
			$insertquery = "INSERT into dive_amz_cron_details(WeekNo, YearNo, ASIN, Keywords, CompName, CompASIN, CompTitle, CompListPrice, CompCurrentPrice, CompShippingPrice, CompPartNo, CompImageURL,CompIncDate, OverAllCatID, OverAllRank, RankCatID,RankCat) values(WEEK(NOW()),YEAR(NOW()),'".$ASIN."', '".$Keyword."', '".$CompName."', '".$CompASIN."','".$CompTitle."','".$CompListPrice."','".$CompCurrentPrice."','".$CompShippingPrice."','".$CompPartNo."','".$CompImageURL."','".$CompIncDate."','".$OverAllCatID."','".$OverAllRank."','".$RankCatID."','".$RankCat."')";

				$result_query = $mysqli->query($insertquery);
				/*echo $ASIN.'=>'."Inserted successfully for".'=>'. $CompASIN.' Price => '.$CompCurrentPrice.' Shipping =>'.$CompShippingPrice."<br>";*/
				if ($result_query === FALSE) 
	            {
	               echo "Inserted Error: " . $insertquery . "<br>" . $mysqli->error. " <br>";
	            } 
                }
				//update flag in dive_amz_auto_explore
				$updatequery = "UPDATE dive_amz_config_explore SET RunFlag = 1 WHERE ASIN ='".$ASIN."' " ;
				$result_update = $mysqli->query($updatequery);
				if ($result_update === FALSE) 
	            {
	               echo "UPDATED Error: " . $updatequery . "<br>" . $mysqli->error. " <br>";
	            } 
	         //}
			}
		}
	}
	
} //while loop end
$Spcall = "CALL dive_amz_after_cron(WEEK(NOW()),YEAR(NOW()))";
$mysqli->query($Spcall);

/*
	1. Call GetSearchKeywordResult
	2. Extract All ASINS From the Result
	3. Form ASIN List Query
	4. Call GetPriceResult
	5. Get ASIN,Landing Price and Shipping Price 
	6. Get Competitor Cost & Shipping Price and Over all SearchResult Insert into dive_amz_cron_details(table)

	*/



	//Get Lowest Price Result
	/*$check_low_price = '';
	foreach($Price_res as $key => $value){
		if(in_array(min(array_column($Price_res,'LandedPrice')),$value)){
			$check_low_price = $key;
		}
	}


	$minval_ASIN = $Price_res[$check_low_price]['ASIN'];
	$CompCurrentPrice=$Price_res[$check_low_price]['LandedPrice'];
	$CompShippingPrice=$Price_res[$check_low_price]['Shipping'];

	foreach ($SearchResult->Product as $comp) {
		if($comp->Identifiers->MarketplaceASIN->ASIN == $minval_ASIN){
			$CompASIN = (string)$comp->Identifiers->MarketplaceASIN->ASIN;
			$CompName = (string)$comp->AttributeSets->ItemAttributes->Brand;
			$CompTitle = (string)$comp->AttributeSets->ItemAttributes->Title;
			$CompListPrice = $comp->AttributeSets->ItemAttributes->ListPrice->Amount;
			$CompPartNo = (string)$comp->AttributeSets->ItemAttributes->PartNumber;
			$CompImageURL = (string)$comp->AttributeSets->ItemAttributes->SmallImage->URL;
			$OverAllCatID = (string)$comp->SalesRankings->SalesRank[0]->ProductCategoryId;
			$OverAllRank = (string)$comp->SalesRankings->SalesRank[0]->Rank;
			$RankCatID =(string)$comp->SalesRankings->SalesRank[1]->ProductCategoryId;
			$RankCat = (string)$comp->SalesRankings->SalesRank[1]->Rank;
			$CompIncDate = (string)$comp->AttributeSets->ItemAttributes->ReleaseDate;

			//insert lowest price competitor data in dive_amz_cron_details
			$insertquery = "INSERT into dive_amz_cron_details(WeekNo, YearNo, ASIN, Keywords, CompName, CompASIN, CompTitle, CompListPrice, CompCurrentPrice, CompShippingPrice, CompPartNo, CompImageURL,CompIncDate, OverAllCatID, OverAllRank, RankCatID,RankCat) values(WEEK(NOW()),YEAR(NOW()),'".$ASIN."', '".$Keyword."', '".$CompName."', '".$CompASIN."','".$CompTitle."','".$CompListPrice."','".$CompCurrentPrice."','".$CompShippingPrice."','".$CompPartNo."','".$CompImageURL."','".$CompIncDate."','".$OverAllCatID."','".$OverAllRank."','".$RankCatID."','".$RankCat."')";

			$result_query = $mysqli->query($insertquery);
			echo $ASIN."Inserted successfully";

			//update flag in dive_amz_auto_explore
			$updatequery = "UPDATE dive_amz_auto_explore SET RunFlag = 1 WHERE ASIN ='".$ASIN."' " ;
			$result_update = $mysqli->query($updatequery);
			
		}
	
	}	
	exit;*/
	/*
	1. Call GetSearchKeywordResult
	2. Extract All ASINS From the Result
	3. Form ASIN List Query
	4. Call GetPriceResult
	5. Get ASIN,Landing Price and Shipping Price of Lowest One

	*/








?>