//Name Of Instance
var InstanceName = 'Dive_Arrowhead_Staging';
//Dependency Packages
const shell = require('shelljs');
var Promise = require('promise');
var JsonQuery = require('json-query');
var Request = require('request');
var ParseString = require('xml2js').parseString;
var Pusher = require('pusher');
var Async = require('async');
var MySql = require('mysql');
var Mongoose = require('mongoose');
var Mailer = require('nodemailer');
var AutoMsg = '<span style="color:blue;"><span style="font-weight:bold;">Note :</span> This is a System Generated Mail. Please Do Not Reply Back To This Email ID.</span>'

//Pusher Credentials
var PusherAppID   = '500271'; var PusherKey = '678afec375584fd8c3ea'; 
var PusherSecret  = '21acf1b92781db5f30df'; var PusherCluster = 'ap2'; var PusherEncryption = 'true';

//MySQL DB Credentials
var MySQLHost = 'localhost'; var MySQLDBName = 'arrowhead';
var MySQLUser = 'root'; var MySQLPass = 'apadive457';

//Mongo DB Credentials
var MongoHost = 'localhost'; var MongoPort = '27017'; var MongoDBName = 'arrowhead';
var MongoDBUrl = "mongodb://"+MongoHost+":"+MongoPort+"/"+MongoDBName;

//Mail Credentials
var Service = 'gmail'; var MailID = 'tech@apaengineering.com'; var MailPass = 'E8mZA9r82aa';
var To = 'lamp-dive@googlegroups.com';

//Create Pusher Object
global.PusherObj = new Pusher({
    appId: PusherAppID,key: PusherKey,secret: PusherSecret,cluster: PusherCluster,encrypted: PusherEncryption
});

//Create MyQL Object
global.MySQLCon = MySql.createConnection({
    host: MySQLHost, user: MySQLUser,password: MySQLPass,database: MySQLDBName
});
MySQLCon.connect(function(err) {
    if (err) throw err;
    //console.log("*** MySQL DB Connection Successful !! ***");
});

global.AppID = global.DevID = global.CertID = global.ServerURL = global.CompatibilityLevel = global.SiteID = global.AuthToken = '';
//Get eBay Credentials
var Query = "SELECT AppID,DevID,CertID,ServerURL,Compatability,SiteID,AuthKey FROM  dive_ebaysettings WHERE Active_flag =1";

MySQLCon.query(Query, function (err, result, fields) {
    if(err){
        ErrorSendMail('Select Data Keys From MySQL DB Error',Query,err)
        setTimeout(function () {
            process.exit();
        }, 5000);
    }
    if (result.length == 0){
        ErrorSendMail('No Keys Are Active Error');
        setTimeout(function () {
            process.exit();
        }, 5000);
    }
    Async.eachSeries(result, function iterator(result2, callback) {
        global.AppID = result2.AppID; global.DevID = result2.DevID; global.CertID = result2.CertID;
        global.AuthToken = result2.AuthKey ; global.ServerURL = result2.ServerURL;
        global.CompatibilityLevel = result2.Compatability; global.SiteID = result2.SiteID;
    });
});

setTimeout(function () {
//Create MongoDB Object
Mongoose.connect(MongoDBUrl,function(){
    Mongoose.connection.db.dropDatabase();
});

Mongoose.connect(MongoDBUrl);
global.MongoDBCon = Mongoose.connection;
MongoDBCon.on('error', console.error.bind(console, 'Connection Error:'));

//Schemes and Models
var GetItemResultScheme = Mongoose.Schema({
    MyItemID: Number,
    Title: String,
    SKU: String,
    Price: Number
});                            
global.GetItemResultModel = Mongoose.model('GetItem', GetItemResultScheme, 'GetItemResult');

var SearchResultScheme = Mongoose.Schema({
    MyItemID: String,
    ItemID: String,
    Title: String,
    GalleryURL: String,
    CategoryID : String,
    Category : String,
    SellerName : String,
    Price : String,
    ItemURL : String,
    ListingStatus : String,
    Country : String,
    Location : String,
    ListingDate : String,
    ShippingCost : String,
    ShipType : String,
    CompLowPrice : Number,
    HandlingTime : String,
    ConditionID : String,
    ConditionName : String,
    MultiShipFlag : String,
    FreeShipFlag : String,
    TopRatedFlag : String
});
global.SearchResultModel = Mongoose.model('SearchResult', SearchResultScheme, 'SearchResults');

var SearchURLScheme = Mongoose.Schema({
    eBayURL: String,
    MyItemID : String
});                    
var SearchURLModel = Mongoose.model('SearchURL', SearchURLScheme, 'GetItemResult');

//Create Pusher Object
PusherObj.trigger('schedulerprogressarrowhead', 'myevent', {
    "message": "Process Started Successfully"
});

MongoDBCon.once('open', function() {
    //console.log("*** Mongo DB Connection Successful !! ***");
    SendMail('Auto Pricer Process Started','Auto Pricer Process Started Successfully');
    return new Promise(function(resolve, reject) {
        //console.log(AppID);
        var Query = "SELECT REPLACE(crawler_url,'ToBeReplaced','"+global.AppID+"') AS crawler_url,itemid,categoryid FROM dive_config_ibr_filter WHERE Active_flag = 1  and selected_flag =1";
        MySQLCon.query(Query, function (err3, result3, fields) {
            if(err3){
                ErrorSendMail('Fetch Filters From MySQL DB Error',Query,err3)
                setTimeout(function () {
                    reject(err3);
                }, 10000);
            }
            
            //console.log("*** MySQL DB Fetch Filters To MongoDB Successful !! ***");
            var FilterScheme = Mongoose.Schema({
                crawler_url: String,
                itemid: Number,
                categoryid: Number
            });              
            var FilterModel = Mongoose.model('Filters', FilterScheme, 'ItemFilters');
            var mainresultcount = result3.length;
            var mscount = mainresultcount;

            console.log('Main Result : ' +result3.length);

            FilterModel.collection.insert(result3, function (err, docs) {
                if (err) reject(err);

                //console.log("*** Filters Inserted To MongoDB ***");
                    
                Async.eachSeries(result3, function iterator(input, CallBack1) {
                    global.MyItemID = input.itemid; global.CatID = input.categoryid;
                    global.SearchURL = input.crawler_url;
                    
                    console.log('Currrent ItemID : '+MyItemID);

                    var requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
                    requestXmlBody += '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
                    requestXmlBody += "<RequesterCredentials><eBayAuthToken>"+AuthToken+"</eBayAuthToken></RequesterCredentials>";
                    requestXmlBody += "<IncludeItemSpecifics>true</IncludeItemSpecifics>";
                    requestXmlBody += "<ItemID>"+MyItemID+"</ItemID>";
                    requestXmlBody += '</GetItemRequest>';

                    var PostData = {
                        url: ServerURL,
                        method: 'POST',
                        headers: {
                        'Content-Type': 'application/xml',
                        'X-EBAY-API-COMPATIBILITY-LEVEL'  : CompatibilityLevel,
                        'X-EBAY-API-DEV-NAME' : DevID,
                        'X-EBAY-API-APP-NAME' : AppID,
                        'X-EBAY-API-CERT-NAME' : CertID,
                        'X-EBAY-API-CALL-NAME' : 'GetItem',
                        'X-EBAY-API-SITEID' : SiteID,
                        },
                        body: requestXmlBody,
                    };

                    Request(PostData, function (error, response, body) {
                        if(error){
                            ErrorSendMail('eBay Request Error',PostData,error)
                            setTimeout(function () {
                                reject(error);
                            }, 5000);
                        }
                        
                        ParseString(response.body, function (err, result) {
                            if(err){
                                ErrorSendMail('Parsing eBay Response Error',err,err)
                                setTimeout(function () {
                                    reject(err);
                                }, 5000);
                            }

                            var Status = result.GetItemResponse.Ack.toString();
                            
                            if (Status != 'Success'){                                
                                var ItemResult = JsonQuery('Errors', {data: result.GetItemResponse}).value;
                                var Error = JsonQuery('LongMessage', {data: ItemResult}).value.toString();
                                ErrorSendMail('eBay Get Item Error',JSON.stringify(result),requestXmlBody);
                                setTimeout(function () {
                                    process.exit();
                                }, 5000);                                
                            }
                            else{
                                var ItemResult = JsonQuery('Item', {data: result.GetItemResponse}).value;
                                var Title = SKU = '';
                                var Price = 0;
                                Title = Validation((JsonQuery('Title', {data: ItemResult})).value).toString();
                                Title = Title.replace(/'/g,"\\'");
                                Title = Title.replace(/"/g, '\\"');
                                SKU = Validation((JsonQuery('SKU', {data: ItemResult})).value).toString();
                                SKU = SKU.replace(/'/g,"\\'");
                                SKU = SKU.replace(/"/g, '\\"');
                                PriceRes = Validation((JsonQuery('SellingStatus', {data: ItemResult})).value);
                                PriceRes = Validation((JsonQuery('CurrentPrice', {data: PriceRes})).value);
                                Price = parseFloat(Validation((JsonQuery('_', {data: PriceRes})).value));
                                
                                var GetItemResult = ({
                                    'MyItemID' : MyItemID,
                                    'Title' : Title,
                                    'SKU' : SKU,
                                    'Price': Price
                                });
                                
                                GetItemResultModel.collection.insert(GetItemResult, function (err, docs) {
                                    if (err){
                                        ErrorSendMail('Mongo Insert GetItem Result Error',err,GetItemResult);
                                        setTimeout(function () {
                                            reject(err);
                                        }, 5000);     
                                    }
                                    //console.log("\n--- GetItem Inserted For "+MyItemID+" ---");
                                   
                                    var PostData = {
                                        url: SearchURL
                                    };
                                    
                                    Request(PostData, function (error, response, body) {
                                        if(error) reject(error);
                                        
                                        ParseString(response.body, function (err, result) {
                                            if(err) reject(err);

                                            var Status = result.findItemsByKeywordsResponse.ack.toString();
                                            
                                            if (Status != 'Success'){
                                                var ItemResult = JsonQuery('Errors', {data: result.findItemsByKeywordsResponse}).value;
                                                console.log(result);
                                                var Error = JsonQuery('LongMessage', {data: ItemResult}).value.toString();
                                                ErrorSendMail('eBay SearchKeyWord Error',JSON.stringify(result),requestXmlBody);
                                                setTimeout(function () {
                                                    process.exit();
                                                }, 5000);       
                                            }
                                            
                                            var SearchURL = Validation((JsonQuery('itemSearchURL', {data: result.findItemsByKeywordsResponse}).value)).toString();
                                            var SearchResult = JsonQuery('searchResult', {data: result.findItemsByKeywordsResponse}).value;
                                            
                                            var eBayURL = ({
                                                'eBayURL' : SearchURL
                                            });

                                            var Query = {'MyItemID': MyItemID};
                                            
                                            SearchURLModel.findOneAndUpdate(Query, eBayURL, {upsert:true}, function(err, doc){
                                                if (err){
                                                    ErrorSendMail('Mongo Insert Search URL Error',err,eBayURL+Query);
                                                    setTimeout(function () {
                                                        reject(err);
                                                    }, 5000);     
                                                }
                                                
                                                //console.log("--- SearchKeyWord eBay URL Updated For "+MyItemID+" ---");

                                                var SearchResultItems = JsonQuery('item', {data: SearchResult}).value;
                                                Async.eachSeries(SearchResultItems, function iterator(element, Callback2) {
                                                    var ItemID = Title = SKU = GalleryURL = CategoryID = Category = ListingStatus = '';
                                                    var SellerName = ItemURL = Country = Location = ListingDate = ShipType = '';
                                                    var ConditionID = ConditionName = RecordDate = '';
                                                    var CompLowPrice = MultiShipFlag = FreeShipFlag = TopRatedFlag = Price = ShippingCost = HandlingTime = 0 ;
                                                    
                                                    ItemID = Validation(element.itemId.toString());
                                                    Title = Validation(element.title).toString();
                                                    Title = Title.replace(/'/g,"\\'");
                                                    Title = Title.replace(/"/g, '\\"');
                                                    GalleryURL = Validation(element.galleryURL).toString();
                                                    CategoryID = CatID;
                                                    Category = Validation((JsonQuery('categoryName', {data: element.primaryCategory}).value)).toString();
                                                    Category = Category.replace(/'/g,"\\'");
                                                    Category = Category.replace(/"/g, '\\"');
                                                    PriceRes = Validation((JsonQuery('currentPrice', {data: element.sellingStatus})).value);
                                                    PriceRes.forEach(function(ele){
                                                        Price = parseFloat(Validation(ele._));   
                                                    });
                                                    ListingStatus = Validation((JsonQuery('sellingState', {data: element.sellingStatus}).value)).toString();
                                                    SellerName = Validation((JsonQuery('sellerUserName', {data: element.sellerInfo}).value)).toString();
                                                    ItemURL = Validation(element.viewItemURL).toString();
                                                    Country = Validation(element.country).toString();
                                                    Location = Validation(element.location).toString();
                                                    Location = Location.replace(/'/g,"\\'");
                                                    Location = Location.replace(/"/g, '\\"');
                                                    ListingDate = Validation((JsonQuery('startTime', {data: element.listingInfo}).value)).toString();
                                                    ListingDate = ListingDate.replace("T", " ");  
                                                    ListingDate = Validation(ListingDate.replace(".000Z", ""));
                                                    ShippingCostRes = (JsonQuery('shippingServiceCost', {data: element.shippingInfo}).value);
                                                    ShippingCostRes.forEach(function(ele){
                                                        ShippingCost = parseFloat(Validation(ele._));   
                                                    });
                                                    ShipType = Validation((JsonQuery('shippingType', {data: element.shippingInfo}).value)).toString();
                                                    HandlingTime = Validation((JsonQuery('handlingTime', {data: element.shippingInfo}).value)).toString();
                                                    CompLowPrice = Validation(parseFloat(Price)+parseFloat(ShippingCost));
                                                    ConditionID = Validation((JsonQuery('conditionId', {data: element.condition}).value)).toString();
                                                    ConditionName = Validation((JsonQuery('conditionDisplayName', {data: element.condition}).value)).toString();
                                                
                                                    if(ShipType == 'Calculated' || ShipType == 'CalculatedDomesticFlatInternational' || ShipType == 'FlatDomesticCalculatedInternational'){
                                                    MultiShipFlag = 1;
                                                    }
                                                
                                                    if(ShipType == 'Flat' || ShipType == 'Free' || ShipType == 'FreePickup'){
                                                    FreeShipFlag = 1;
                                                    }
                                                
                                                    if(element.topRatedListing.toString() == 'true'){
                                                    TopRatedFlag = 1;
                                                    }

                                                    var SearchResultEach = ({
                                                        MyItemID : MyItemID, ItemID: ItemID,Title: Title,GalleryURL: GalleryURL,
                                                        CategoryID : CategoryID,Category : Category,SellerName : SellerName,
                                                        Price : Price,ItemURL : ItemURL,ListingStatus : ListingStatus,
                                                        Country : Country,Location : Location,ListingDate : ListingDate,
                                                        ShippingCost : ShippingCost,ShipType :ShipType,CompLowPrice : CompLowPrice,
                                                        HandlingTime : HandlingTime,ConditionID : ConditionID,ConditionName : ConditionName,
                                                        MultiShipFlag : MultiShipFlag,FreeShipFlag : FreeShipFlag,TopRatedFlag : TopRatedFlag
                                                    });     
                                                    
                                                    SearchResultModel.collection.insert(SearchResultEach, function (err, docs) {
                                                        if (err){
                                                            ErrorSendMail('Mongo Insert Search Result Error',err,SearchResultEach);
                                                            setTimeout(function () {
                                                                reject(err);
                                                            }, 5000);     
                                                        }
                                                        Callback2();
                                                    });
                                                });
                                                mainresultcount--;
                                                console.log('Current Running Result'+mainresultcount+' Out of '+mscount);
                                                if(mainresultcount == 0){
                                                    AfterCron();
                                                }
                                                CallBack1();
                                            });
                                        })
                                    })
                                });
                            }
                        })
                    })
                });
            });
        });
    });
});
}, 100)

//Validation Function
function Validation(Data){
    if (typeof Data == 'undefined') {
        return '';
    }
    else{
      return Data;
    }
}

function ErrorSendMail(subject,filedata,err){
    //Create Mail Object
    var Transporter = Mailer.createTransport({
        service: 'gmail',
        auth: {
        user: 'tech@apaengineering.com',
        pass: 'E8mZA9r82aa'
        }
    });  

    var MailConfig = {
        from    : 'APA - AutoPricer : Alert tech@apaengineering.com',
        to      : 'tamilselvan.s@apaengineering.com',  //lamp-dive@googlegroups.com
        //to          : 'hemanand.r@apaengineering.com',
        cc      : '',
        bcc     : '',
        subject   : subject,
        html    : err+"<br><br><br>"+AutoMsg,
        attachments : [{'filename': 'ErrorLog.txt', 'content': filedata+"\n\n\n\n"+err}]
    };

    Transporter.sendMail(MailConfig, function(error, info){
        if (error) {
            console.log('Email Send Error !!'+error);
        } 
        else {
            console.log('Email Sent Successfully !!');
        }
    });
}

function SendMail(subject,msg){
    //Create Mail Object
    var Transporter = Mailer.createTransport({
        service: 'gmail',
        auth: {
        user: 'tech@apaengineering.com',
        pass: 'E8mZA9r82aa'
        }
    });  

    var MailConfig = {
        from    : 'APA - AutoPricer : Alert tech@apaengineering.com',
        to      : 'tamilselvan.s@apaengineering.com',  //lamp-dive@googlegroups.com
        //to          : 'hemanand.r@apaengineering.com',
        cc      : '',
        bcc     : '',
        subject   : subject,
        html    : msg+"<br><br><br>"+AutoMsg
    };

    Transporter.sendMail(MailConfig, function(error, info){
        if (error) {
            console.log('Email Send Error !!'+error);
        } 
        else {
            console.log('Email Sent Successfully !!');
        }
    });
}

function AfterCron(){
    setTimeout(function () {
        return new Promise(function(resolve, reject) {
            SearchResultModel.find({},'',function(err,docs){
                if (err){
                    ErrorSendMail('Mongo Get Search Result Error',err,SearchResultModel);
                    setTimeout(function () {
                        reject(err);
                    }, 5000); 
                }
                var mainresultcount = docs.length;
                var Query = "TRUNCATE TABLE competitor_search_item_details";
                MySQLCon.query(Query, function (err1, result1, fields) {
                    if(err1){
                        ErrorSendMail('Insert Search Items Error',Query,err1)
                        setTimeout(function () {
                            reject(err1);
                        }, 5000);  
                    }
                    Async.eachSeries(docs, function iterator(input, callback) {
                        var Query = "Call dive_autopricer_search_item_details_insert ('"+input.MyItemID+"','"+input.ItemID+"', '"+input.Title+"', '"+input.GalleryURL+"', '"+input.CategoryID+"', '"+input.Category+"', "+input.Price+", '"+input.ListingStatus+"', '"+input.SellerName+"', '"+input.ItemURL+"', '"+input.Country+"', '"+input.Location+"', '"+input.ListingDate+"', "+input.ShippingCost+", '"+input.ShipType+"', '"+input.HandlingTime+"', "+input.CompLowPrice+", '"+input.ConditionID+"', '"+input.ConditionName+"', "+input.MultiShipFlag+", "+input.FreeShipFlag+","+input.TopRatedFlag+")";
                        MySQLCon.query(Query, function (err2, result2, fields) {
                            if(err2) {
                                console.log(Query);
                                ErrorSendMail('Insert Search Items Error',Query,err2)
                                setTimeout(function () {
                                    reject(err2);
                                }, 5000);    
                            }
                            console.log('Inserted Successfully');
                            mainresultcount--;

                            if(mainresultcount == 0){
                                GetItemResultModel.find({},'',function(err,docs){
                                    if (err) throw err;
                                    var subresultcount = docs.length;
                                    Async.eachSeries(docs, function iterator(input, callback1) {
                                        var Query = "UPDATE competitor_master_list set price = '"+input.Price+"', Title = '"+input.Title+"',sku = '"+input.SKU+"' where itemid = '"+input.MyItemID+"'";
                                        MySQLCon.query(Query, function (err2, result2, fields) {
                                            if(err2) {
                                                console.log(Query);
                                                ErrorSendMail('Update GetItem Result Error',Query,err2)
                                                setTimeout(function () {
                                                    reject(err2);
                                                }, 5000);    
                                            }
                                            console.log('Updated Successfully');
                                            subresultcount--;
                                            if(subresultcount == 0){
                                                var Query = "call dive_config_autorawlist_after_cron()";
                                                MySQLCon.query(Query, function (err, result, fields) {
                                                    if(err) {
                                                        console.log(Query);
                                                        ErrorSendMail('After Cron Error',Query,err)
                                                        setTimeout(function () {
                                                            reject(err);
                                                        }, 5000);    
                                                    }
                                                    console.log('After Process Finished Successfully');
                                                    SendMail('Auto Pricer Process Ended','Auto Pricer Process Ended Successfully');
                                                    PusherObj.trigger('mynewchannelarrowhead', 'eventarrow', {
                                                        "message": "Scheduled Process has been Completed ."
                                                    });
                                                });
                                            }
                                            callback1();
                                        });
                                    });
                                });
                            }
                            callback();
                        });
                    });
                });
            });
        });
    }, 10000);
}