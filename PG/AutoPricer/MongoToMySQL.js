//Name Of Instance
var InstanceName = 'Dive_Arrowhead_Staging';

//Dependency Packages
var Promise = require('promise');
var Async = require('async');
var MySql = require('mysql');
var Mongoose = require('mongoose');

//MySQL DB Credentials
var MySQLHost = 'localhost'; var MySQLDBName = 'arrowhead';
var MySQLUser = 'root'; var MySQLPass = 'apadive457';

//Mongo DB Credentials
var MongoHost = 'localhost'; var MongoPort = '27017'; var MongoDBName = 'arrowhead';
var MongoDBUrl = "mongodb://"+MongoHost+":"+MongoPort+"/"+MongoDBName;

//Create MyQL Object
var MySQLCon = MySql.createConnection({
    host: MySQLHost, user: MySQLUser,password: MySQLPass,database: MySQLDBName
});
MySQLCon.connect(function(err) {
    if (err) throw err;
    console.log("*** MySQL DB Connection Successful !! ***");
});

//Create MongoDB Object
Mongoose.connect(MongoDBUrl);
var MongoDBCon = Mongoose.connection;
MongoDBCon.on('error', console.error.bind(console, 'Connection Error:'));


//Schemes and Models
var SearchResultScheme = Mongoose.Schema({
    MyItemID: String,
    ItemID: String,
    Title: String,
    GalleryURL: String,
    CategoryID : String,
    Category : String,
    SellerName : String,
    Price : String,
    ItemURL : String,
    ListingStatus : String,
    Country : String,
    Location : String,
    ListingDate : String,
    ShippingCost : String,
    ShipType : String,
    CompLowPrice : Number,
    HandlingTime : String,
    ConditionID : String,
    ConditionName : String,
    MultiShipFlag : String,
    FreeShipFlag : String,
    TopRatedFlag : String
});
var SearchResultModel = Mongoose.model('SearchResult', SearchResultScheme, 'SearchResults');

var GetItemResultScheme = Mongoose.Schema({
    MyItemID: Number,
    Title: String,
    SKU: String,
    Price: Number
});                            
var GetItemResultModel = Mongoose.model('GetItem', GetItemResultScheme, 'GetItemResult');


MongoDBCon.once('open', function() {
    console.log("*** Mongo DB Connection Successful !! ***");
    
    return new Promise(function(resolve, reject) {
        SearchResultModel.find({},'',function(err,docs){
            if (err) throw err;
            var mainresultcount = docs.length;
            var Query = "TRUNCATE TABLE competitor_search_item_details";
            MySQLCon.query(Query, function (err1, result1, fields) {
                if(err1) throw err1;
                Async.eachSeries(docs, function iterator(input, callback) {
                    var Query = "Call dive_autopricer_search_item_details_insert ('"+input.MyItemID+"','"+input.ItemID+"', '"+input.Title+"', '"+input.GalleryURL+"', '"+input.CategoryID+"', '"+input.Category+"', "+input.Price+", '"+input.ListingStatus+"', '"+input.SellerName+"', '"+input.ItemURL+"', '"+input.Country+"', '"+input.Location+"', '"+input.ListingDate+"', "+input.ShippingCost+", '"+input.ShipType+"', '"+input.HandlingTime+"', "+input.CompLowPrice+", '"+input.ConditionID+"', '"+input.ConditionName+"', "+input.MultiShipFlag+", "+input.FreeShipFlag+","+input.TopRatedFlag+")";
                    MySQLCon.query(Query, function (err2, result2, fields) {
                        if(err2) {
                            console.log(Query);
                            throw err2;
                        }
                        console.log('Inserted Successfully');
                        mainresultcount--;

                        if(mainresultcount == 0){
                            GetItemResultModel.find({},'',function(err,docs){
                                if (err) throw err;
                                var subresultcount = docs.length;
                                Async.eachSeries(docs, function iterator(input, callback1) {
                                    var Query = "UPDATE competitor_master_list set price = '"+input.Price+"', Title = '"+input.Title+"',sku = '"+input.SKU+"' where itemid = '"+input.MyItemID+"'";
                                    MySQLCon.query(Query, function (err2, result2, fields) {
                                        if(err2) {
                                            console.log(Query);
                                            throw err2;
                                        }
                                        console.log('Updated Successfully');
                                        subresultcount--;
                                        if(subresultcount == 0){
                                            var Query = "call dive_config_autorawlist_after_cron()";
                                            MySQLCon.query(Query, function (err, result, fields) {
                                                if(err) {
                                                    console.log(Query);
                                                    throw err;
                                                }
                                                console.log('Price Recommendation Process Finished Successfully');
                                            });
                                        }
                                        callback1();
                                    });
                                });
                            });
                        }
                        callback();
                    });
                });
            });
        });
    });      
});